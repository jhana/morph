package util.fncs;

/**
 * 
 * @todo?? remove eq semantics? Call BinPredicate? But both args are of the same type
 * @author jirka
 */
//@Deprecated /* use the code in util instead */
public interface Eq<T> { // todo implemnts BinPredicate<T,T>
    boolean equals(T a, T b);
}
