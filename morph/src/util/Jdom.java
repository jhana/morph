package util;

import java.io.IOException;
import java.io.Reader;
import lombok.Cleanup;
import morph.util.Cols;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import util.io.IO;
import util.io.XFile;

/**
 *
 * @author j
 */
public final class Jdom {
    public static Document readXml(final XFile aFile) throws IOException {
        System.out.println("xml file: " + aFile);
        @Cleanup Reader r = IO.openLineReader(aFile);
        try {
            SAXBuilder builder = new SAXBuilder(false);
            builder.setExpandEntities(false);
            return builder.build(r);
        } catch (JDOMException e) {
            throw new RuntimeException("Error while parsing the XML structure.", e);
        }
    }     
    
    public static Iterable<Element> elements(Document aDoc, String aElement) {
        return Cols.<Element>toIterable( aDoc.getDescendants(new org.jdom2.filter.ElementFilter(aElement)) );
    }

}
