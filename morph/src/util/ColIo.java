package util;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.purl.net.jh.utilx.Cols;
import org.purl.net.jh.utilx.IException;
import util.col.MultiMap;
import util.err.FormatError;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;

/**
 * Basic utilities for simple reading of collections from a file. Drop once util2 library is used.
 * 
 * @todo move here other functions from IO and elsewhere
 * @todo create reader builder (configuration comments, skip empty lines, etc).
 * @todo combine with EzReader
 * @author jirka
 */
public class ColIo {
    // source
    protected final XFile file;
    protected final Reader reader;

    //
    protected LineReader r;
    protected String line;
    
    public ColIo(XFile aFile) {
        file = aFile;
        reader = null;
    }

    public ColIo(String aSrcString) {
        file = null;
        reader = new StringReader(aSrcString);
    }
    
    /** Override to filter or preprocess lines, etc. */
    protected void readLine() throws IOException {
        line = r.readNonEmptyLine();
    }
    
    /** Whitespace separated key and values */
    public Map<String,String> readInMap() throws IOException {
        return readInMap(Pattern.compile("(\\S+)\\s+(\\S+)\\s*"));
    }
    
    /**
     * Reads in multimap from a file.
     * The first token is the key the remaining tokens are the values.
     *
     * @param aFile
     * @param aKeyValPattern pattern with two groups specifying the key and the value
     * @return
     * @throws IOException
     * @todo 
     */
    public Map<String,String> readInMap(final Pattern aKeyValPattern) throws IOException {
        final Map<String,String> map = new HashMap<String, String>();

        try {
            open();
            //r.configureSplitting(aSeparator, 2, "Incorrect format - requires: key values");

            for(;;) {
                readLine();
                if (line == null) break;
                
                Matcher m = aKeyValPattern.matcher(line);
                if (!m.matches()) throw new FormatError("Error [%d] -- the line does not match the key-value pattern\n%s", r.getLineNumber(), line);
                if (m.groupCount() != 2) throw new IException("The pattern %s should have two groups, it has %d.", aKeyValPattern, m.groupCount());

                map.put(m.group(1),m.group(2));
            }

            return map;
        }
        finally {
            close();
        }
    }

    public MultiMap<String,String> readInMultiMap(final Pattern aSeparator) throws IOException {
        final MultiMap<String,String> map = new MultiMap(); //  XCols.newMultiHashHashMap<String,String>();

        try {
            open();
            r.configureSplitting(aSeparator, 2, "Incorrect format - requires: key values");

            for(;;) {
                final List<String> tokens = Cols.asList(r.readSplitNonEmptyLine());
                if (tokens == null) break;

                map.addAll(tokens.get(0), tokens.subList(1, tokens.size()));
            }

            return map;
        }
        finally {
            close();
        }
    }

    public void open() throws IOException {
        if (file != null) {
            r = IO.openLineReader(file);
        }
        else {
            r = new LineReader( reader  ).setFile(null);
            }
//        r.configureSplitting(splitting_pattern, splitting_parts, splitting_errMsg);
//        if (commentStr != null) r.setCommentStr(commentStr);
    }

    public void close() throws IOException {
        IO.close(r);
    }

    
}
