package util.col;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A map with two layers of keys 
 * @author j
 */
public class Multi2Map<K1, K2, V> {
    final private Map<K1,MultiMap<K2,V>> map1 = new HashMap<>();
    
    public void add(K1 aK1, K2 aK2, V aValue) {
        getS(aK1, aK2).add(aValue);
    }

    public void addAll(K1 aK1, K2 aK2, Collection<V> aValue) {
        getS(aK1, aK2).addAll(aValue);
    }
    
    public Set<V> get(K1 aK1, K2 aK2) {
        MultiMap<K2,V> map2 = map1.get(aK1);    
        return map2 == null ? null : map2.get(aK2);
    }

    public Set<V> getS(K1 aK1, K2 aK2) {
        MultiMap<K2,V> map2 = map1.get(aK1);

        if (map2 == null) {
            map2 = new MultiMap<>();
            map1.put(aK1, map2);
        }
        
        return map2.getS(aK2);
    }

    // not as easy there might be empty maps or empty sets
    //public boolean isEmpty() {

    public void clear() {
        map1.clear();
    }
    
//    public int size() {
//        return map1.totalSize();
//    }

//    public Iterable<MultiMap<K2, V>> allValues() {
//        return map1.allValues();
//    }

//    public boolean addEmpty(K1 aKey) {
//        return map1.addEmpty(aKey);
//    }
//
//
//    public boolean remove(K1 aKey, MultiMap<K2, V> aValue) {
//        return map1.remove(aKey, aValue);
//    }
//
//    public void removeValue(MultiMap<K2, V> aValue) {
//        map1.removeValue(aValue);
//    }
//
//    public MultiMap<K1, MultiMap<K2, V>> removeAll(Predicate<MultiMap<K2, V>> aPredicate) {
//        return map1.removeAll(aPredicate);
//    }
//
//    public MultiMap<K1, MultiMap<K2, V>> retainAll(Predicate<MultiMap<K2, V>> aPredicate) {
//        return map1.retainAll(aPredicate);
//    }
//
//    public void merge(K1 aNewKey, K1 aOldKey) {
//        map1.merge(aNewKey, aOldKey);
//    }
//
//    public List<K1> keysByFrequency() {
//        return map1.keysByFrequency();
//    }
//
//    public boolean isEmpty() {
//        return map1.isEmpty();
//    }
            
            
    
    
}
