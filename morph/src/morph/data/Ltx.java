
package morph.data;

import java.util.Map;

/**
 *
 * @author Jirka Hana
 */
public interface Ltx {
    String getLemma();
    String getTag();
    Map<String,String> getAttrs();
}
