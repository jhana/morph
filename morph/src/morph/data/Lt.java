
package morph.data;

import java.util.Map;

/**
 *
 * @author Jirka Hana
 */
public class Lt implements Ltx {
    private final String lemma;
    private final String tag;

    public Lt(String aLemma, String aTag) {
        lemma = aLemma;
        tag = aTag;
    }


    @Override
    public Map<String, String> getAttrs() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getLemma() {
        return lemma;
    }

    @Override
    public String getTag() {
        return tag;
    }

}
