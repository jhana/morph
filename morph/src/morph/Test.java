
package morph;
import util.str.Strings;

/**
 *
 * @author Jirka
 */
public class Test {
    
    /** Creates a new instance of Test */
    public Test() {}

    String[] mNrs;
    
    public static void main(String[] args) {
        new Test().go();
    }

    void go() {
        mNrs = new String[10000];
        for(int i=0;i<10000;i++) {
            mNrs[i] = String.valueOf(i);
        }
        
        for (int i = 0; i < 100; i++) {
            concat();
            normal();
        }
    }

    void concat() {
        String x = "";
        for(int i=0;i<10000;i++) {
            x = Strings.concat(x, mNrs[i], "EFG");
        }
    }

    void normal() {
        String x = "";
        for(int i=0;i<10000;i++) {
            x = x + mNrs[i] + "EFG";
        }
    }

}
