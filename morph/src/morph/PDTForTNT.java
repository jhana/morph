//package morph;
//
//import util.*;
//import morph.readers.*;
//import java.io.*;
//import java.util.*;
//
///* 
// @todo auto naming switch 
// @todo merge with Forms only - select what to save
// @todo input format (PDT,TNT) output format (PDT TNT)
// @todo set i/o sgml tags (does not work)
// */
//
//
//
///**
// *
// */
//public class PDTForTNT extends UtilityBase {
//
//    /**
//     *
//     */
//    public static void main(String[] args) throws IOException {
//        new PDTForTNT().go(args);
//    }
//    
//    public void go(String[] aArgs) throws java.io.IOException {
//        argumentBasics(aArgs,2);
//        File inFile = getMArgs().getDirectFile(0);
//        File outBase = getMArgs().getDirectFile(1);
//
//        String[] codeStringArray = getMArgs().codeStringArray("-cp", "all");
//
//        File[] outFiles = new File[codeStringArray.length];
//        for (int i = 0; i < codeStringArray.length; i++) 
//            outFiles[i] = IO.addExtension(outBase,codeStringArray[i]); 
//
//        BoolTag[] boolTagArray = BoolTag.fromCodeStringArray(codeStringArray);
//        translate(inFile, outFiles, boolTagArray);
//    }
//    
//
//    protected String helpString() {
//        return "java PDTForTNT [options] {maFile} {tntFileBase}\n" +
//            "   creates training file(s) in TNT format based on a PTD file," +
//            "possibly retaining only specified subtags" +
//            "the resulting TNT file has the name: {tntFileBase}.all (unless -cpn is used)\n\n" +
//            "-cpn {code-strings}\n" +
//            "   filters the specified subtags, code-strings are separated by ':' or ';'\n" +
//            "   codes by position: PSgncGNptdavV; 'all' is an abbreviation for listing all codes\n" +
//            "   each resulting TNT file has the name: {tntFileBase}.{code-string}.lex\n" +
//            "   E.g.: java PDTForTNT -cpn all:P:Pgc pdt file\n" +
//            "      produces file.all, file.P and file.Pgc files\n" +
//            "   E.g.: java PDTForTNT -cpn all pdt file\n" +
//            "      is equivalent to java PDTForTNT pdt file\n\n";
//    }
//
//    /**
//     * 
//     */
//    void translatePDT(File aMAFile, File[] aOutFiles, BoolTag[] aConsideredPositions) throws java.io.IOException {
//        PdtLineReader in = new PdtLineReader(aMAFile, mIEnc, true, true);
//        final PrintWriter[] outs = IO.openPrintWriterBattery(aOutFiles, mOEnc);  //@todo fix encoding
//
//        for(int j=0;;j++) {
//            if (!in.readLine()) break;
//
//            if (in.isFDLineType()) {
//                for (int i = 0; i < aOutFiles.length; i++) {
//                    Tag filteredTag = in.tag().filterTag(aConsideredPositions[i]);
//                    outs[i].printf("%s\t%s\n", in.form(), filteredTag);
//                }
//            }
//            if ((j & 16383) == 0) System.out.print('.');  // 16383 = 2^26-1
//        }
//        System.out.println("--");
//        in.close();
//        IO.closeBattery(outs);
//    }
//
//    void translate(File aMAFile, File[] aOutFiles, BoolTag[] aConsideredPositions) throws java.io.IOException {
//        TntReader in = TntReader.openM(aMAFile, mIEnc);
//        final PrintWriter[] outs = IO.openPrintWriterBattery(aOutFiles, "ISO8859_5");  //@todo fix encoding
//
//        for(int j=0;;j++) {
//            if (!in.readLine()) break;
//
////            if (in.isFDLineType()) {
//                for (int i = 0; i < aOutFiles.length; i++) {
//                    Tag filteredTag = in.tag().filterTag(aConsideredPositions[i]);
//                    outs[i].printf("%s\t%s\n", in.form(), filteredTag);
////                }
//            }
//            if ((j & 16383) == 0) System.out.print('.');  // 16383 = 2^26-1
//        }
//        System.out.println("--");
//        in.close();
//        IO.closeBattery(outs);
//    }
//}