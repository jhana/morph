package morph;

import morph.ts.BoolTag;
import morph.ts.Tag;
import morph.ts.Tags;
import morph.io.*;
import util.io.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import lombok.Cleanup;
import morph.ts.Tagset;
import util.*;
import util.io.IO;
import morph.util.MUtilityBase;
import util.col.Counter;
import util.col.MultiMap;
import util.err.Log;






/**
 * @todo clean the code
 * @todo automatically obtain cp from endings of files from a directory
 * @todo add option for some sophisticated case processing (removing cased entries
 *    that do not have support in MA) If that changes anything in results.
 * @todo check whether unknownOnly does what it should
 */ 
public final class LexForTNT extends MUtilityBase {
    TNTLexicon mLexicon;
    private final static Pattern whiteSpaceSep = Pattern.compile("\\s+");
    boolean mUsePct = false;     // @todo configurable
    
    /**
     *
     */
    public static void main(String[] args) throws IOException {
        new LexForTNT().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        XFile maFile    = getArgs().getDirectFile(0);
        XFile outLexFNB = getArgs().getDirectFile(1);
        //@todo
        XFile trainedLexFNB = getArgs().getFile("trainedLex", null);

        List<BoolTag> tagFilters = getArgs().boolTags("filter", tagset, tagset.cAllTrue);
        System.out.println(tagFilters);
        
        boolean unknownOnly = getArgs().getBool("unknownOnly");
        boolean backupLex   = getArgs().getBool("backupLex");

        //Report.printlnA(slotStringArray);

        for (BoolTag tagFilter : tagFilters) {
            String tagFilterStr = tagFilter.toCodeStringHF();
            Log.info("Filter: %s", tagFilterStr);
            //BoolTag consideredSlots = BoolTag.fromSlotString(tagFilter);
            String ending = (backupLex) ? ".baklex" : ".lex";
            XFile outLexFN     = outLexFNB.addExtension(tagFilterStr + ending);      // @todo don't add "all" if no -cp was specified
            
            if (trainedLexFNB != null) {
                XFile trainedLexFN = trainedLexFNB.addExtension(tagFilterStr + ".lex");
                mLexicon = null;
                mLexicon = new TNTLexicon();
                mLexicon.mConsideredPositions = tagFilter;
                mLexicon.mUnknownOnly = unknownOnly;

                Log.info("  Loading the TNT trained lexicon");
                readTrainedLexicon(trainedLexFN); 
                Log.info("  Loading the MA-ed file");
                readMALexicon(maFile);
                Log.info("  Processing data and saving the result");
                if (backupLex)
                    mLexicon.writeMA(outLexFN);
                else
                    mLexicon.write(outLexFN);
            }
            else {
                simpleLexicon(tagFilter, maFile, outLexFN);
            }
        }
    }


    private void readTrainedLexicon(XFile aTNTLexFile) throws java.io.IOException {
        @Cleanup LineNumberReader tntLexFile = IO.openLineReader(aTNTLexFile);
        for(int j = 0;; j++) {
            String[] tokens = readTNTLexTokens(tntLexFile); 
            if (tokens == null) break;
            String form = tokens[0];

            Set<TagFreq> tagFreqs = new HashSet<TagFreq>();            // @noneffective
            for (int i = 2; i < tokens.length; i+=2)
                tagFreqs.add(new TagFreq(Tagset.getDef().fromString(tokens[i]), Integer.parseInt(tokens[i+1]) ));

            mLexicon.addTrained(form, tagFreqs);
        }
    }
    
    private String[] readTNTLexTokens(LineNumberReader aTNTLexFile) throws java.io.IOException {
        String[] tokens;
        do {
            String line = aTNTLexFile.readLine();
            if (line == null) return null;
            tokens = whiteSpaceSep.split(line);                           
        } while ( tokens[0].startsWith("%%") || tokens[0].equals("@USECASE") );
        return tokens;
    }

    /**
     * Compares the specified GS and tagged file
     */
    private void readMALexicon(XFile aMAFile) throws java.io.IOException {
        @Cleanup CorpusLineReader in = Corpora.openMm(aMAFile);

        for(;;) {
            if (!in.readLine()) break;
            mLexicon.addMA(in.form(), in.tags());
        }
    }
    
    
    
    void simpleLexicon(BoolTag aBoolTag, XFile aMAFile, XFile aOutFile) throws java.io.IOException {
        final MultiMap<String, Tag> lexicon = readInSimpleLex(aMAFile, aBoolTag);

        // --- write out the lexicon ---
        @Cleanup PrintWriter out = IO.openPrintWriter(aOutFile);
    
        List<String> sortedForms = new ArrayList<String>(lexicon.keySet());
        Collections.sort(sortedForms);
//        System.out.print("" + sortedForms);
        
        //out.println("%% lexicon created by LexForTNT");
        out.println("@USECASE		1");

        for(String form : sortedForms) {
            Set<Tag> maTags = lexicon.get(form);
            
            out.printf("%s   %d   ", form, maTags.size());
            
            String freqStr = " 1  ";
            if (mUsePct)  
                freqStr = " " + Util.formatPercentages(1, maTags.size(), 0) + "  ";
            
            for (Tag maTag : maTags) {
                out.print(maTag + freqStr);
            }

            out.println();
        }
   }
    
    
    private MultiMap<String, Tag> readInSimpleLex(XFile aMAFile, BoolTag aBoolTag) throws IOException {
        @Cleanup CorpusLineReader r = Corpora.openMm(aMAFile);

        final MultiMap<String, Tag> lexicon = new MultiMap<String, Tag>();

        while (r.readLine()) {
            lexicon.addAll(r.form(), Tags.filterTags(r.tags(), aBoolTag));
        }

        return lexicon;
    }

}
class TNTLexicon {
    BoolTag mConsideredPositions;  
    boolean mUnknownOnly;
    
    MultiMap<String,TagFreq> mTrainedLexicon;  
    MultiMap<String,Tag> mMALexicon;  
    
    TNTLexicon() {
        mTrainedLexicon = new MultiMap<String,TagFreq>();
        mMALexicon = new MultiMap<String,Tag>();
    }
    
    void addTrained(String aForm, Set<TagFreq> aTagFreqs) {
        // --- filter to only considered positions ---
        if ( mConsideredPositions.hasSomeSet() ) 
            aTagFreqs = filterTagFreqs(aTagFreqs);  //@todo why??? It is already filtered
        
        // filter mTrainedLexicon (+ add Freq)
        mTrainedLexicon.addAll(aForm, aTagFreqs);
    }

    void addMA(String aForm, Set<Tag> aMATags) {
        // --- filter to only considered positions ---
        Set<Tag> maTags = Tags.filterTags(aMATags, mConsideredPositions);
        
        Set<TagFreq> trainedTagFreqs = mTrainedLexicon.get(aForm);
        if (trainedTagFreqs != null) {
            if (mUnknownOnly) return;
            Set<Tag> trainedTags = tagFreqs2tags(trainedTagFreqs);
            maTags.removeAll(trainedTags);
        }
        if (aMATags.size() > 0)
            mMALexicon.addAll(aForm, maTags);
    }
    
    Set<TagFreq> filterTagFreqs(Set<TagFreq> aTagFreqs)  {
        Counter<Tag> counter = new Counter<Tag>();
        for (TagFreq tf : aTagFreqs) {
            //@todo Util.fassert(Tag.checkTag(tf.mTag), "Wrong tag " + tf.mTag);
            counter.add(tf.mTag.filterTag(mConsideredPositions), tf.mFreq );
        }

        aTagFreqs.clear();
        for (Tag tag  : counter.keySet()) {
            aTagFreqs.add( new TagFreq(tag, counter.frequency(tag)) );
        }
        return aTagFreqs;
    }

    Set<Tag> tagFreqs2tags(Set<TagFreq> aTagFreqs) {
        Set<Tag> result = new HashSet<Tag>();
        for (TagFreq tagFreq : aTagFreqs)
            result.add(tagFreq.mTag);
        return result;
    }
    
    
    Set<TagFreq> trainedTags(String aForm) {
        Set<TagFreq> tagFreqs = mTrainedLexicon.get(aForm);
        return (tagFreqs != null) ? tagFreqs : Collections.<TagFreq>emptySet();
    }
    
    Set<Tag> maTags(String aForm) {
        Set<Tag> tags = mMALexicon.get(aForm);
        return (tags != null) ? tags : Collections.<Tag>emptySet();
    }

    
    SortedSet<String> allSortedForms() {
        SortedSet<String> forms = new TreeSet<String>(mTrainedLexicon.keySet());
        forms.addAll(mMALexicon.keySet());
        return forms;
    }

    public void filteredCopy(String aConsideredPositions) {
        //@todo
    }

    public void writeMA(XFile aOutFile) throws java.io.IOException {
        PrintWriter out = IO.openPrintWriter(aOutFile);  //@todo
    
        out.println("%% lexicon created by LexForTNT");

        SortedSet<String> sortedForms = new TreeSet<String>(mMALexicon.keySet());
        
        for(String form : sortedForms) {
            Set<Tag> maTags = mMALexicon.get(form);
            
            int maTagsTotFreq = maTags.size();
          
            out.printf("%s   %d   ", form, maTagsTotFreq);

            if (maTags != null)
                for (Tag maTag : maTags)
                    out.printf(maTag + " 1  ");
            
            out.println();
        }
        out.close();
   }
    
    public void write(XFile aOutFile) throws java.io.IOException {
        PrintWriter out = IO.openPrintWriter(aOutFile);
    
        out.println("%% lexicon created by LexForTNT");
        out.println("@USECASE		1");

        SortedSet<String> sortedForms = allSortedForms();
        
        // @todo better (@ switches go before anything else)
        if (sortedForms.remove("@CARD")) {
            TagFreq tagFreq = mTrainedLexicon.get("@CARD").iterator().next();   // the one and only member
            out.printf("@CARD   %d   %s %d\n", tagFreq.mFreq, tagFreq.mTag, tagFreq.mFreq);
        }
        
        for(String form : sortedForms) {
            Set<TagFreq> trainedTags = mTrainedLexicon.get(form);
            Set<Tag> maTags = mMALexicon.get(form);
            
//            Set<TagFreq> trainedTags = trainedTags(form);       // maybe empty
//            Set<String> maTags = maTags(form);                  // maybe empty

            int trainedTagsTotFreq = (trainedTags != null) ? totalFreq(trainedTags) : 0;
            int maTagsTotFreq = (maTags != null) ? maTags.size() : 0;
          
            out.printf("%s   %d   ", form, trainedTagsTotFreq + maTagsTotFreq);

            if (trainedTags != null)
//                for (TagFreq tagFreq : new TreeSet<TagFreq>(trainedTags)) // not sorted
                for (TagFreq tagFreq : trainedTags)
                    out.printf("%s   %d   ", tagFreq.mTag, tagFreq.mFreq);
            
            if (maTags != null)
                for (Tag maTag : maTags)
                    out.printf(maTag + " 1  ");
            
            out.println();
        }
        out.close();
   }
    
   protected int totalFreq(Set<TagFreq> aTagFreqs) {
       int totalFreq = 0;
       for (TagFreq tagFreq : aTagFreqs)
           totalFreq += tagFreq.mFreq;
       return totalFreq;
   }
}

// Pair<String,Integer>
class TagFreq implements Comparable {
    Tag mTag;
    int mFreq;

    TagFreq(Tag aTag, int aFreq) {
        mTag  = aTag;
        mFreq = aFreq;
    }

    public int compareTo(Object o) {
        return mTag.compareTo(((TagFreq) o).mTag );
    }
}
