package morph.util.string;

import java.util.*;
import util.str.Strings;
import util.col.Cols;

/**
 * Object representing a string changing rule.
 * The rule can be used to change tails (e.g. stem tails) or heads (e.g.
 * ending heads) of strings.
 *
 * @todo optimize (compile into a tree of characters)
 *
 * @author  Jirka Hana
 */
public class Basic implements StringChangeRule {

    /**
     * Creates a new rule based on a string-based description.
     *
     * @param aRuleSpec rule specification in the following form:
     * <pre>
     * rule = subrule(:subrule)*
     * subrule = string(/string)+
     * </pre>
     */
    public Basic(String aRuleSpec)  {
        this(Strings.doubleSplit(aRuleSpec, ':', '/'));
    }

    public Basic(List<List<String>> aTuples)  {
        mTuples = aTuples;
        mSubruleNr = mTuples.get(0).size();
    }

    public Basic()  {
        mTuples = Collections.emptyList();
        mSubruleNr = 0;
    }
    
    public Basic(int aRulesSize)  {
        mTuples = Collections.emptyList();
        mSubruleNr = aRulesSize;
    }
    
    
// ------------------------------------------------------------------------------
// Attributes
// ------------------------------------------------------------------------------    
    
    /** 
     * Number of subrules (i.e. number of tuples) 
     * This is the number of items separated by ':' in the definition of the rule.
     */
    public int getSubruleNr() {
        return mTuples.size();
    }
    
    /** 
     * Number of related strings. For example, number of related stems. 
     * This is the number of items separated by '/' in the definition of each subrule.
     */
    public int getSubruleSize() {
        return mSubruleNr;
    }

    /**
     * Returns tuples as a list of lists.
     */
    protected List<List<String>> tuples() {
        return mTuples;
    }

    public String toString() {
        util.str.pp.Printer<List<String>> itemPrinter = new util.str.pp.Printer<List<String>>() {
            public String toString(List<String> aItem) {
                return Cols.toString(aItem, "", "", "//", "");
            }
        };

        return mTuples.toString(); //  Cols.toString(mTuples, itemPrinter, "", "", "::", "");  // todo there is some bug
    }
    
// ------------------------------------------------------------------------------
// Using the rule
// ------------------------------------------------------------------------------    
    
    /**
     * Changes a string's tail. 
     * If the string's tail does not match any subrule, the string is not changed.
     * 
     * @param aString the current string
     * @param aFrom the current string's index
     * @param aTo   the desired string's index (if equal to aFrom, the strng is not changed)
     * @return string with a changed tail
     */
    public String translateTail(String aString, int aFrom, int aTo) {
        if (aFrom == aTo) return aString;
        
        for (List<String> tuple : mTuples) {    
            if (aString.endsWith(tuple.get(aFrom)) ) 
                return Strings.replaceTail(aString,  tuple.get(aFrom), tuple.get(aTo));
        }
        return aString;
    }

    /** 
     * Changes a string's head.
     * If the string's head does not match any subrule, the string is not changed.
     * 
     * @param aString the current string
     * @param aFrom the current string's index
     * @param aTo   the desired string's index (if equal to aFrom, the strng is not changed)
     * @return string with a changed tail
     */
    public String translateHead(String aString, int aFrom, int aTo) {
        if (aFrom == aTo) return aString;
        
        for (List<String> tuple : mTuples) {    
            if (aString.startsWith(tuple.get(aFrom))) 
                return Strings.replaceHead(aString,  tuple.get(aFrom), tuple.get(aTo));
        }
        return aString;
    }


    
// ------------------------------------------------------------------------------
// Manipulating rules
// ------------------------------------------------------------------------------    

    /**
     * Merges two tail rules.
     *
     * @todo add parametr setting head/tail direction, or two functions
     */
    public Basic merge(final Basic aAddRule, final int aBaseMergePoint, final int aAddMergePoint) {
        assert aBaseMergePoint < getSubruleSize();
        assert aAddMergePoint  < aAddRule.getSubruleSize();
        
        // --- collect merge points ---
        List<String> mergePoints = new ArrayList<String>();
        for (List<String> tuple : mTuples) {
            mergePoints.add(tuple.get(aBaseMergePoint));
        }
        for (List<String> tuple : aAddRule.mTuples) {
            if (!mergePoints.contains(tuple.get(aAddMergePoint)))
                mergePoints.add(tuple.get(aAddMergePoint));
        }
        Collections.sort(mergePoints, Strings.tailLexOrderRL);
        
        // --- expand merge points by base and add rule ---
        List<List<String>> newTuples = new ArrayList<List<String>>();
        for (String mergePoint : mergePoints) {
            List<String> baseExpansion = instantiateRule(mergePoint,aBaseMergePoint);
            List<String> addExpansion  = aAddRule.instantiateRule(mergePoint,aAddMergePoint);
            addExpansion.remove(aAddMergePoint);
            baseExpansion.addAll(addExpansion);
            
            newTuples.add(baseExpansion);
        }
        
        return new Basic(newTuples);
    }    

    /**
     * Creates a list of all to's.
     * @todo pass param marking direction
     */
    protected List<String> instantiateRule(String aStart, int aFrom) {
        List<String> tuple = new ArrayList<String>(getSubruleSize());
        
        for(int i = 0; i < getSubruleSize(); i++) {
            tuple.add( translateTail(aStart, aFrom, i) );
        }
        
        return tuple;
    }    
    
    protected Basic filterByPossibleTails(List<String> aPossibleTails, int aIdx) {
        List<List<String>> newTuples = new ArrayList<List<String>>();
        for (List<String> tuple : mTuples) {
            for (String possible : aPossibleTails) {
                if ( tuple.get(aIdx).endsWith(possible) || possible.endsWith(tuple.get(aIdx)) ) {
                    newTuples.add( new ArrayList<String>(tuple) );
                }
            }
        }
        return new Basic(newTuples);
    }
// =============================================================================
// Implementation <editor-fold desc="Implementation">
// ==============================================================================
    
    /** Tuples, each representing a single subrule */
    final private List<List<String>>  mTuples;

    /** Number of stems (i.e. size of each subrule, or size of each tuple) */
    final private int mSubruleNr;

// </editor-fold>    
    
}
