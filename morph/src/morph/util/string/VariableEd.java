package morph.util.string;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import util.Triple;

/**
 *
 * @author Administrator
 */
public class VariableEd  extends EditDistance {
    public static void main(String[] aParams) {
        List<Triple<Character,Character,Double>> ccss = new ArrayList<Triple<Character,Character,Double>>();
        ccss.add(new Triple('a', 'A', 0.0));
        ccss.add(new Triple('A', 'a', 0.0));
        ccss.add(new Triple('b', 'B', 0.0));
        ccss.add(new Triple('B', 'b', 0.0));
        ccss.add(new Triple('c', 'C', 0.0));
        ccss.add(new Triple('C', 'c', 0.0));
        VariableEd ved  = new VariableEd( ccss, false );

        System.out.println("going in");
        double distance = ved.distance("abc", "ABC");
        System.out.println("distance=" + distance);

    }


    class SubstCostMatrix {
        final Map<Character,Map<Character,Integer>> map;
        public SubstCostMatrix(List<Triple<Character,Character,Double>> aSubstCosts, boolean aSymmetric) {
            map = new TreeMap<Character,Map<Character,Integer>>();

            for (Triple<Character,Character,Double> ccs : aSubstCosts) {
                add(ccs.mFirst, ccs.mSecond, ccs.mThird);
                if (aSymmetric) {
                    add(ccs.mSecond, ccs.mFirst, ccs.mThird);
                }
            }

            // debug
            for (Map.Entry<Character,Map<Character,Integer>> e : map.entrySet()) {
                char char1 = e.getKey();
                for (Map.Entry<Character,Integer> f : e.getValue().entrySet()) {
                    System.out.println(char1 + ":" + f.getKey() + " " + f.getValue());
                }
            }
        }

        private void add(Character aChar1, Character aChar2, Double aCost) {
            Map<Character,Integer>  map2 = map.get(aChar1);
            if (map2 == null) {
                map2 = new TreeMap<Character,Integer>();
                map.put(aChar1, map2);
            }

            int cost = (int)Math.round(aCost*1000);
            map2.put(aChar2, cost);
        }

        public int cost(char a, char b) {
            Map<Character,Integer>  map2 = map.get(a);
            if (map2 == null) {
                return 1000;
            }
            Integer cost = map2.get(b);
            return (cost == null) ? 1000 : cost;
        }
    }
    private final SubstCostMatrix substCostMatrix;

    public VariableEd(List<Triple<Character,Character,Double>> aSubstCosts, boolean aSymmetric) {
        substCostMatrix = new SubstCostMatrix(aSubstCosts, aSymmetric);
    }

    @Override
    public double distance(String s, String t) {
        final int n = s.length();
        final int m = t.length();

        // --- Step 1 ---
        if (n == 0) {
            return m;
        }
        if (m == 0) {
            return n;
        }
        final int[][] d = new int[n + 1][m + 1];

        // --- Step 2 ---
        for (int i = 0; i <= n; i++) {
            d[i][0] = i*1000;
        }

        for (int j = 0; j <= m; j++) {
            d[0][j] = j*1000;
        }

        // Step 3
        for (int i = 1; i <= n; i++) {
            final char s_i = s.charAt(i - 1);  // ith character of s

            // Step 4
            for (int j = 1; j <= m; j++) {
                final char t_j = t.charAt(j - 1);  // jth character of t

                // Step 5
                final int substCost = (s_i == t_j) ? 0 : substCostMatrix.cost(s_i, t_j);

                // Step 6
                d[i][j] = min(d[i - 1][j] + 1000, d[i][j - 1] + 1000, d[i - 1][j - 1] + substCost);
            }
        }

        // Step 7
        return d[n][m] / 1000.0;
    }



}
