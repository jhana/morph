package morph.util.string;

/**
 *
 * @author Jirka
 */
public interface StringChangeRule extends java.io.Serializable {
    /**
     * 
     * Number of subrules (i.e. number of tuples) 
     * This is the number of items separated by ':' in the definition of the rule.
     */
    int getSubruleNr();

    /**
     * 
     * Number of related strings. This is equal to the number of related stems
     * and it is the number of items separated by '/' in the definition of each subrule.
     */
    int getSubruleSize();


    String toString();

    /**
     * 
     * Changes a string's head.
     * If the string's head does not match any subrule, the string is not changed.
     * 
     * 
     * @param aString the current string
     * @param aFrom the current string's index
     * @param aTo   the desired string's index (if equal to aFrom, the strng is not changed)
     * @return string with a changed tail
     */
    String translateHead(String aString, int aFrom, int aTo);

    /**
     * Changes a string's tail. 
     * If the string's tail does not match any subrule, the string is not changed.
     * 
     * 
     * @param aString the current string
     * @param aFrom the current string's index
     * @param aTo   the desired string's index (if equal to aFrom, the strng is not changed)
     * @return string with a changed tail
     */
    String translateTail(String aString, int aFrom, int aTo);
    
}
