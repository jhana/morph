package morph.util.string;

/**
 *
 * @author Administrator
 */
public abstract class EditDistance {

    public abstract double distance(String s, String t);

    /** Get minimum of three values */
    protected final static int min(int a, int b, int c) {
        int mi = a < b ? a : b;
        return (c < mi) ? c : mi;
    }

    protected final static double min(double a, double b, double c) {
        double mi = a < b ? a : b;
        return (c < mi) ? c : mi;
    }

    public double normalize(String s, String t, double dist) {
        final int maxLen = Math.max(s.length(), t.length());

        return (dist*100.0/maxLen)/100.0;
    }

}