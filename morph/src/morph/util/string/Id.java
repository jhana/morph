package morph.util.string;

/**
 *
 * @author Jirka
 */
class Id implements StringChangeRule {
    
    public int getSubruleNr() {
        return 0;
    }

    public int getSubruleSize() {
        return 0;
    }
    
    public String translateHead(String aString, int aFrom, int aTo) {
        return aString;
    }

    public String translateTail(String aString, int aFrom, int aTo) {
        return aString;
    }

    public String toString() {
        return "";
    }

    
}
