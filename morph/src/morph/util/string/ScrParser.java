package morph.util.string;

import morph.ma.module.*;

/**
 *
 * @author Jirka
 */
public class ScrParser {
    
    private ScrParser() {}
    
    public static StringChangeRule getStrChangeRule(String aRuleSpec) {
        if (aRuleSpec.length() == 0) {
            return new Id();
        }
        else {
            return new Basic(aRuleSpec);
        }
    }
    
}
