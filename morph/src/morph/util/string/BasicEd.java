package morph.util.string;

/**
 *
 * @author Administrator
 */
public class BasicEd extends EditDistance {

//    public int basic(String s, String t) {
//        return basic(s,t, Integer.MAX_VALUE);
//    }
    public BasicEd() {}

    /** Assumes lower case */
    @Override
    public double distance(String s, String t) {
        final int n = s.length();
        final int m = t.length();

        // --- Step 1 ---
        if (n == 0) {
            return m;
        }
        if (m == 0) {
            return n;
        }
        final int[][] d = new int[n + 1][m + 1];

        // --- Step 2 ---
        for (int i = 0; i <= n; i++) {
            d[i][0] = i;
        }

        for (int j = 0; j <= m; j++) {
            d[0][j] = j;
        }

        // Step 3
        for (int i = 1; i <= n; i++) {
            final char s_i = s.charAt(i - 1);  // ith character of s

            // Step 4
            for (int j = 1; j <= m; j++) {
                final char t_j = t.charAt(j - 1);  // jth character of t

                // Step 5
                final int cost = (s_i == t_j) ? 0 : 1;

                // Step 6
                d[i][j] = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost);
            }
        }

        // Step 7
        return d[n][m] * 1.0;
    }

}