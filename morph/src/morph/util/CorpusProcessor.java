package morph.util;

import morph.io.*;
import util.io.*;
import java.io.*;
import java.util.regex.Pattern;
import lombok.Cleanup;
import util.err.Log;

/**
 * @todo make this more universal, move the multifile processing to a separate class, etc.
 * @todo update this
 * @author  Jiri
 * @todo make the reader generic: CorpusProcessor<T extends WordReader>
 */
public abstract class CorpusProcessor extends MUtilityBase {

    /**
     * Word counter
     */
    protected int mWords = 0;
    
    /**
     * Limit for the number of words to be read
     */
    protected int mCorpusSizeLimit;

    /**
     * Filename filter that files to be included into the corpus processing must
     * satisfy.
     */
    private RegexFileFilter fileFilter;

    @Override
    protected void argumentBasics(String[] aArgs, int aDirArgNr) {
        super.argumentBasics(aArgs, aDirArgNr);
        mCorpusSizeLimit = getArgs().getInt("corpusSizeLimit", Integer.MAX_VALUE);
    }

    public void setPattern(Pattern aPattern) {
        fileFilter = new RegexFileFilter(aPattern);
    }

    public void setEnding(String aEnding) {
        fileFilter = new RegexFileFilter(".*\\." + aEnding);
    }
    
    protected void process(WordAction aProcessor, XFile aInFile) throws IOException {
        mWords = 0;
        aProcessor.init();
        if  (aInFile.file().isFile())
            processFile(aProcessor, aInFile);
        else if (aInFile.file().isDirectory())
            processFiles(aProcessor, aInFile);
        else
            throw new IOException(aInFile + " does not exist");
    }

    protected void processFiles(WordAction aProcessor, XFile aDir) throws IOException{
        final File[] files = aDir.file().listFiles(fileFilter);
        int fileProcessed = 0;
        
        for(File file : files) {
            if (0 < mCorpusSizeLimit && mCorpusSizeLimit <= mWords) break;
            
            fileProcessed++;
            System.out.printf("Processing file [%d/%d]: %s\n", fileProcessed, files.length, file.getName() );
            Log.info("Processing file [%d/%d]: %s", fileProcessed, files.length, file.getName() );
            processFile(aProcessor, XFile.create(file, aDir) );
        }
    }

    protected void processFile(WordAction aProcessor, XFile aFileName) throws IOException {
        @Cleanup WordReader r = openReader(aFileName);
        configureReader(r);

        cycle(aProcessor, r);
    }

    /**
     * Opens the reader used by the processor (supports plain, wplain, PDT, TNT formats)
     */
    protected WordReader openReader(XFile aFile) throws IOException {
        return Corpora.openWordReader(aFile);
    }

    /**
     * This method is called after the reader is opened, to allow configuration code.
     */
    protected void configureReader(WordReader aR) {
    }
    
    protected void cycle(WordAction aProcessor, WordReader aR) throws IOException {
        for (;;) {
            if (!aR.nextWord()) break;
            mWords++;
            if (0 < mCorpusSizeLimit && mCorpusSizeLimit <= mWords) break;

            if (!aProcessor.processWord(aR)) break;
            //if (mWords % 20000 == 0) System.out.println(aR.word());
        }
    }        

}
