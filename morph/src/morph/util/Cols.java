package morph.util;

import java.util.*;
import util.col.MultiMap;

/**
 * Utilities working with collections.
 * 
 * @author jirka
 */
public class Cols {
    
    /**
     * Wraps an iterator with an iterable.
     * @param <T>
     * @param aIt
     * @return 
     */
    public static <T> Iterable<T> toIterable(final Iterator<T> aIt) {
        return new Iterable<T>() {
            @Override
            public Iterator<T> iterator() {
                return aIt;
            }
        };
    }    
    /**
     * Returns the first missing number in a list of sorted numbers
     * @param aNrs a sorted list
     * @param aOffset first legal number to return (usually 0 or 1)
     * @return first number &gt;= aOffset and not present in aNrs.
     */
    public static int firstUnused(final List<Integer> aNrs, final int aOffset) {
        for (int i = 0; i < aNrs.size(); i++) {
            if (aNrs.get(i) > i+aOffset) return i+aOffset;
        }
        return aNrs.size() + aOffset;
    }

    public static <T> T getFirstSet(T ... aItems) {
        for (T item : aItems) {
            if (item != null) return item;
        }
        return null;
    }
    
    // toto optimize
    public static  <K,V> Set<V> multiGet(final MultiMap<K,V> aMultiMap, final Collection<K> aKeys) {
        final Set<V> allValues = new HashSet<V>();
        for (K key : aKeys) {
            Set<V> vals = aMultiMap.get(key);
            if (vals != null) {
                allValues.addAll(vals);
            }
        }
        return allValues;
    }

    public static  <K,V> Set<V> multiGet(final MultiMap<K,V> aMultiMap, final K ... aKeys) {
        final Set<V> allValues = new HashSet<V>();
        for (K key : aKeys) {
            Set<V> vals = aMultiMap.get(key);
            if (vals != null) {
                allValues.addAll(vals);
            }
        }
        return allValues;
    }
    
    
}
