package morph.util.tbl;

import java.util.List;

/**
 *
 * @author Jirka Hana
 */
public class PlainPrinter implements Printer {

    @Override
    public String print(Tbl aTbl) {
        return new Printing(aTbl).print();
    }

    class Printing {
        Tbl tbl;
        List<String> colIds;
        List<String> lnIds;
        StringBuilder sb = new StringBuilder();

        public Printing(Tbl aTbl) {
            tbl = aTbl;
            colIds = aTbl.getColIds();
            lnIds = aTbl.getColIds();
        }

        public String print() {
            for (String lnId : lnIds) {
                printLine(lnId);
                sb.append("\n");
            }
            return sb.toString();
        }

        protected void printLine(String aLnId) {
            for (String colId : colIds) {
                Cell cell = tbl.getCell(colId, aLnId);
                sb.append(cell.data);
                sb.append(" ");
            }
        }

    }

}
