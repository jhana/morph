
package morph.util.tbl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * todo merged cells
 * todo lines
 * @author Jirka Hana
 */
public class Tbl {
    
    private Map<String, Cell> cells    = new HashMap<String,Cell>();
    private List<String> colIds;
    private List<String> lnIds;

    public List<String> getColIds() {
        return colIds;
    }

    public List<String> getLnIds() {
        return lnIds;
    }

    public Map<String, Cell> getCells() {
        return cells;
    }

    public void setColIds(String ... aColIds) {
        colIds = Arrays.asList(aColIds);
    }

    public void setLnIds(String ... aLnIds) {
        lnIds = Arrays.asList(aLnIds);
    }

    public void setVals(Map<String,Object> aVals) {
        for (Map.Entry<String,Object> entry : aVals.entrySet()) {
            setVal(entry.getKey(), entry.getValue());
        }
    }

    protected Cell getCell(String aColId, String aLnId) {
        return getCell(aColId + ":" + aLnId);
    }

    protected Cell getCell(String aKey) {
        Cell cell = cells.get(aKey);
        if (cell == null) {
            cells.put(aKey, new Cell());
        }
        return cell;
    }

    public void setVal(String aKey, Object aVal) {
        getCell(aKey).data = aVal;
    }

    public void setVal(String aLnId, String aColId, Object aVal) {
        getCell(aColId, aLnId).data = aVal;
    }

    public void setFormat(String aId, Format aFormat) {
        getCell(aId).format = aFormat;
    }

}
