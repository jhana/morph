
package morph.util.tbl;

/**
 *
 * @author Jirka Hana
 */
public interface Printer {
    String print(Tbl aTbl);
}
