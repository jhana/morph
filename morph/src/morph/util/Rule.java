package morph.util;

import util.str.Strings;
import java.util.*;
import util.*;
import util.err.Err;

/**
 *
 * @author  Jiri
 */
public class Rule {
    String mName;
    List<Integer> mBoundaries;
    List<Integer> mPoints;
    
    /** 
     * Creates a new instance of Rule 
     * The intervals are down closed and up open.
     *
     * pt1 bound1 pt2 bound2 pt3 bound3 pt4 bound4 pt5   
     * corresponds to
     *  (-inf,  bound1) -> pt1
     *  [bound1,bound2) -> pt2
     *  [bound2,bound3) -> pt3
     *  [bound3,bound4) -> pt4
     *  [bound4,inf)    -> pt5
     */
    public Rule(String aName, String aSpec)  {
        mName = aName;
        String[] nrs = Strings.split(aSpec);
        Err.assertE(nrs.length % 2 == 1, "");
        int len = nrs.length;

        mBoundaries = new ArrayList<Integer>();
        mPoints     = new ArrayList<Integer>();
        
        for (int i = 0;;) {
            mPoints.add(Integer.valueOf(nrs[i])); i++;
            if (i == nrs.length) break;
            mBoundaries.add(Integer.valueOf(nrs[i]));  i++;
        }
;    }

    
     public int getPoints(int aValue) {   
         for(int i = 0; i < mBoundaries.size(); i++) {
            if (aValue < mBoundaries.get(i))
                return mPoints.get(i);
         }
         return mPoints.get(mPoints.size()-1);
     }
    
     public String toString() {
         StringBuilder sb = new StringBuilder();
         
         // @todo binary search? but usually small
         for(int i = 0; i < mBoundaries.size(); i++) {
            sb.append(mPoints.get(i)).append(' ').append(mBoundaries.get(i)).append(' ');
         }
         sb.append(mPoints.get(mPoints.size()-1));
         return sb.toString();
     }
}
