package morph.util;

import morph.ts.BoolTag;
import morph.ts.Tag;
import morph.ts.Tags;
import java.util.Arrays;
import java.util.List;
import morph.ts.Tagset;
import util.err.Err;
import util.opt.*;
import util.io.*;

/**
 *
 * @author Jiri
 */
public class MArgs extends MainOptions {
    
    /** Creates a new instance of MArgs */
    public MArgs() {
    }

    
// -----------------------------------------------------------------------------
// Direct arguments - files 
// @todo getDirectFile and getDirectCorpus, etc. can be merged
// -----------------------------------------------------------------------------
    
    public XFile[] getAutoCorpora(int aArgPos, Format aFormatDef, List<BoolTag> aTagFilters)  {
        throw new UnsupportedOperationException();
//        XFile[] tmp = new XFile[aSlotStrings.length];
//
//        XFile base = fileFromString(getDirectArg(0), null, null, aFormatDef);
//        for (int i = 0; i < aSlotStrings.length; i++) {
//            tmp[i] = new XFile( IO.addExtension(base.file(), aSlotStrings[i].toString()), base.enc(), base.format() );
//        }
//
//        Report.println("Arg %d: %s", aArgPos, Arrays.toString(tmp));
//        return tmp;
    }    

// =============================================================================
// Normal M specific arguments
// =============================================================================
    
    /** 
     *
     */
    public Tag getTag(String aKey, Tagset aTagset)  {
        return aTagset.fromString(getString(aKey));
    }

    /** 
     *
     */
    public List<Tag> getTags(String aKey, Tagset aTagset)  {
        //return aTagset.fromString(getString(aKey));
        return Tags.fromStrings( aTagset, getStrings(aKey) );
    }

    
// -----------------------------------------------------------------------------
// Slot string
    
    /**
     *
     *
     * @return the slot index; aDef if the argument is not present;
     * -1 if the argument is present but the slot code is invalid.
     * todo - dependent on tagset
     */
    public int getSlot(String aArgName, Tagset aTagset, int aDef) {
        String slotName = getString(aArgName, null);
        if (slotName == null) return aDef;
        Err.assertE(slotName.length() == 1, "slot code must be exactly 1 character");  
        int slot = aTagset.code2slot(slotName.charAt(0));
        Err.assertE(slot != -1, "%s is not a correct slot code", slotName);  
        return slot;
    }

// -----------------------------------------------------------------------------
// Bool tags

    public BoolTag boolTag(String aArgName, Tagset aTagset, BoolTag aDef) {
        String slotString = getString(aArgName, null);
        if (slotString == null) return aDef;
        return aTagset.str2booltag(slotString);
        
    }
    
    public List<BoolTag> boolTags(String aArgName, Tagset aTagset, BoolTag aDef) {
        String[] tmp = getString(aArgName, aDef.toString()).split("[:;\\s]");
        return aTagset.str2booltag(Arrays.asList(tmp));
    }

// ----------------------------------------------------------------------------- 
    
    
    public boolean getActive(String aModuleId) {
        return getBool(aModuleId + '.' + "enabled", false);
    }
    
}
