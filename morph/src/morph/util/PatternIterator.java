package morph.util;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Jirka Hana
 */
public class PatternIterator implements Iterator<String> {

    final Matcher matcher;
    boolean prepared = false;

    public PatternIterator(CharSequence aStr, Pattern aPattern) {
        matcher = aPattern.matcher(aStr);
    }

    @Override
    public boolean hasNext() {
        if (prepared) {
            return true;
        }

        boolean hasNext = matcher.find();
        if (hasNext) {
            prepared = true;
        }
        return hasNext;
    }

    @Override
    public String next() {
        if (!prepared) {
            if (!matcher.find()) {
                throw new NoSuchElementException();
            }
        }
        prepared = false;

        return matcher.group();
    }

    @Override
    public void remove() {
        // todo
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
