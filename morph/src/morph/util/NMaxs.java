package morph.util;

import java.util.ArrayList;
import java.util.List;
import util.Pair;
import util.Pairs;

/**
 *
 * @author Jirka Hana
 */
public class NMaxs<K, V extends Comparable<V>> {

    /** the list is always sorted and has max howMany elements (except within put) */
    private final List<Pair<K, V>> list = new ArrayList<Pair<K, V>>();
    private final int howMany;

    public NMaxs(int aHowMany) {
        howMany = aHowMany;
    }

    public void put(K aId, V aVal) {
        //System.out.println("A: " + list);
        if (list.size() < howMany || aVal.compareTo(list.get(0).mSecond) > 0) {
            insert(aId, aVal);
        }
        //System.out.println("B: " + list);

        if (list.size() > howMany) {
            list.remove(0);
        }
        //System.out.println("C: " + list);
    }

    private void insert(K aId, V aVal) {
        for (int i = 0; i < list.size(); i++) {
            if (aVal.compareTo(list.get(i).mSecond) <= 0) {
                list.add(i, new Pair<K, V>(aId, aVal));
                return;
            }
        }
        list.add(new Pair<K, V>(aId, aVal));
    }



    public boolean contains(K aId) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).mFirst.equals(aId)) {
                return true;
            }
        }
        return false;
    }

    public List<Pair<K, V>> getList() {
        return list;
    }

    public List<K> getIds() {
        return Pairs.firsts(list);
    }

}
