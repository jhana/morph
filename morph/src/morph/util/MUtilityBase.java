package morph.util;

import java.io.File;
import morph.ts.Tagset;
import morph.ts.TagsetRegistry;
import util.err.Log;
import util.io.Format;
import util.io.FormatRegistry;
import util.io.IO;

/**
 * The subclass of this class usually follows this patter:
 * <code>
 * public class Xyz extends MUtilityBase {
 *     public static void main(String[] args) throws IOException {
 *        new Xyz().go(args,2);
 *     }
 *
 *     public void goE() throws java.io.IOException {
 *       // process arguments,
 *       XFile file1 = getArgs().getDirectFile(0);
 *       XFile file2 = getArgs().getDirectFile(1);
 *       // do the actual work
 *     }
 * </code>
 *
 *
 * @todo add plain text (several words per line) - TokWordReader
 * @author Jiri
 */
public class MUtilityBase extends util.UtilityBase {
    public final static Format cPml;
    public final static Format cCsts;
    public final static Format cTnt;
    public final static Format cFlt;
    public final static Format cPlain;
    public final static Format cTxt;
    public final static Format cVert;

    static {
        FormatRegistry formats = new FormatRegistry();
        IO.setFormats(formats);

        cPml   = new Format("PML",   "PDT 2.0 format (PML)");
        cCsts  = new Format("CSTS",  "PDT 1.0 format");
        cTnt   = new Format("TNT",   "TNT format");
        cFlt   = new Format("Flt",   "form lemma tag (space separated)");
        cPlain = new Format("Plain", "Plain (word per line) format");
        cTxt   = new Format("Txt",   "Plain text format");
        cVert  = new Format("vert",  "Vertical format (mixed with sgml tags)");

        formats.register(cPml,  "xml", "pml");
        formats.register(cCsts, "pm", "pmm", "p3m", "csts");
        formats.register(cTnt,  "tm", "tmm", "tt", "t");
        formats.register(cFlt,  "flt");
        formats.register(cPlain, "plain");
        formats.register(cPlain, "flines");
        formats.register(cTxt,   "txt");
        formats.register(cVert,   "vert");

    }

    protected Tagset tagset;

    /** Creates a new instance of MUtilityBase */
    public MUtilityBase() {
    }


    @Override
    protected void argumentBasics(String[] aCmdLineArgs, int aDirArgNr) {
        super.argumentBasics(aCmdLineArgs, aDirArgNr);

//        Log.configDeveloperLogger(null, 0, true);  // @todo configure level
//        Log.setULevel(Level.OFF);

        // todo copy tagset to tagsets
        try {
            TagsetRegistry.getDef().load(getArgs().getSubtree("tagsets"));
        }
        catch (Throwable e) {
            System.out.println(getArgs());
//            System.out.println(getArgs().getSubtree("tagsets"));
        }
        
        tagset = TagsetRegistry.getDef().getDefTs(); 
        
        if (tagset == null) {
            Log.warning("No default tagset loaded");
        }
        else {
            Log.info("Def tagset tagLen = %d", tagset.getLen());
        }
    }

    @Override
    protected void argumentBasics(String[] aCmdLineArgs) {
        argumentBasics(aCmdLineArgs, 0);
    }

    @Override
    protected MArgs createArgsObject() {
        return new MArgs();
    }

    /**
     * @return the name of the morph system, i.e. morph
     */
    @Override
    protected String getSystemName() {
        return "morph";
    }

    @Override
    protected MArgs getArgs() {
        return (MArgs) mArgs;
    }

    @Override
    protected File getAppFolder() {
        return super.getAppFolder().getParentFile();
    }

}
