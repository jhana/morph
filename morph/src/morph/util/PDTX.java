package morph.util;

import java.util.regex.*;


/**
 *
 * @author Jiri
 */
public class PDTX {
    private final static String [] cLemmaSeparators1 = new String[] {"-1", "-2", "-3", "-4", "-5", "-6", "-7", "-8"};
    private final static String [] cLemmaSeparators2 = new String[] {"_^", "_:", "_,", "_;"};
    private final static Pattern lemmaSeps = Pattern.compile("[`\\_\\-]");
    private final static Pattern lemmaSepsX = Pattern.compile("`|(_[\\^:,;])|(-[1-9])");

    private final static Pattern p = Pattern.compile("(.*?)(-[1-9]|`.+|_[:,;^].+)*");

    public static void main(String[] aStr) {
        //Pattern p = Pattern.compile("(.*?)(`|(_[\\^:,;].+)|(-[1-9]))*");


        check("Evropa_;G", "Evropa");
        check("ubytování_^(*3at)", "ubytování");
        check("v-1", "v");
        check("čtvrť_^(města)", "čtvrť");
        check("mésto-1_^(fyzické_umístění)", "místo");
        check("sv?j-1_^(p?ivlast.)", "sv?j");
        check("hledat_:T", "hledat");
        check("práce_^(jako_činnost_i_místo)", "práce");
        check("sto-2`100", "sto");
        check("dva`2", "dva");
        check("m-1`metr_:B", "m");
        check("s-2`sekunda_:B", "s");
        check("strana-3_^(u_soudu,_na_úřadě,_smluvní_strany;_na_něčí_straně)", "strana");
        //check("", "");
    }
    
    public static void check(String aIn, String aExpected) {
        String out = extract(aIn, p);
        if (aExpected.equals(out)) {
            System.out.printf("ok: %s -> %s\n", aIn, out);
        }
        else {
            System.out.printf("!!: %s -> %s / %s\n", aIn, out, aExpected);
        }
    }

    public static String extract(String aStr, Pattern aPattern) {
        Matcher matcher = aPattern.matcher(aStr);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        else {
            return null;
        }

    }


    /** Creates a new instance of PDTX */
    public PDTX() {
    }
    
    public static String pureLemma(String aFullLemma) {
        String[] lemmaParts = lemmaSeps.split(aFullLemma);
        return (lemmaParts.length > 0) ? lemmaParts[0] : aFullLemma;
    }
    
    /**
     * Not used. Roughly like pureLemma(String)
     */
    public static String stripLemma(String aLemma) {
        for (String sep : cLemmaSeparators1) {
            if (aLemma.indexOf('-') == -1) break;      // a quick check      
            int idx = aLemma.indexOf(sep);
            if (idx != -1) aLemma = aLemma.substring(0, idx);
        }

        for (String sep : cLemmaSeparators2) {
            if (aLemma.indexOf('_') == -1) break;      // a quick check         
            int idx = aLemma.indexOf(sep);
            if (idx != -1) aLemma = aLemma.substring(0, idx);
        }
        return aLemma;
    }            
    
}
