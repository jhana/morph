package morph.util;

import java.util.*;


/**
 *
 * @author  Jiri
 */
public class RuleBench {
    List<Rule> mRules;
    public String mPointsStr;
    
    
    /** Creates a new instance of RuleBench */
    public RuleBench() {
        mRules = new ArrayList<Rule>();
    }
    
    public void addRule(Rule aRule) {
        mRules.add(aRule);
    }
    
    public int getPoints(Integer ... aValues) {
        StringBuilder pointsStr = new StringBuilder(7*mRules.size());
        int points = 0;
        
        assert mRules.size() == aValues.length;
        for (int i = 0; i < mRules.size(); i++) {
            Rule rule = mRules.get(i);
            int pts = rule.getPoints(aValues[i]);
            points += pts;
            pointsStr.append(rule.mName).append(' ').append(pts).append(' ');
        }
        mPointsStr = pointsStr.toString();
        return points;
    }
    
    
}
