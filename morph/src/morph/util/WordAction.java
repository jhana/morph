package morph.util;

import java.io.IOException;
import morph.io.WordReader;

public interface WordAction {

    void init();

    /**
     * Processes a single word. Called for each word in the corpus (in the
     * order the words occur in the corpus).
     *
     * @param aR word reader, use aR.word() to access the current word
     * @return true if processing should continue, false otherwise
     * @throws java.io.IOException
     */
    boolean processWord(WordReader aR) throws IOException;
}
