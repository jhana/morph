package morph.util.tnt;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

/**
 * @todo currently ignores processing directives
 * Format: form totalFreq [tag freq]+
 * @author Administrator
 */
public class TntLex implements Iterable<TntLexEntry> {
    private Map<String, TntLexEntry> data = new TreeMap<String, TntLexEntry>();

    public Set<String> getForms() {
        return data.keySet();
    }
    
    public void add(TntLexEntry aEntry) {
        data.put(aEntry.getForm(), aEntry);
    }
    
    public TntLexEntry getEntry(String aForm) {
        return data.get(aForm);
    }

    public Iterator<TntLexEntry> iterator() {
        return data.values().iterator();
    }
    
    public void removeBelowFreq(int aFreq) {
        for (Iterator<Map.Entry<String,TntLexEntry>> i = data.entrySet().iterator(); i.hasNext();) {
            Entry<String,TntLexEntry> e = i.next();
            if (e.getValue().getTotalFreq() < aFreq) i.remove();
        }
   
    }

}
