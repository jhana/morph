package morph.util.tnt;

import morph.ts.Tag;
import util.PairC;

/**
 *
 * @author Administrator
 */
public class TagFreq extends PairC<Tag,Integer> {
    public TagFreq(Tag aTag, int aFreq) {
        super(aTag,aFreq);
    }
    
    public Tag getTag() {
        return mFirst;
    }

    public int getFreq() {
        return mSecond;
    }
}
