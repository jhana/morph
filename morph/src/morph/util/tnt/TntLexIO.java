package morph.util.tnt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import morph.ts.Tagset;
import util.err.Err;
import util.err.FormatError;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;
import util.str.Strings;

/**
 *
 * @author Administrator
 */
public class TntLexIO {
    public static TntLex load(XFile aFile) throws IOException {
        LineReader r = IO.openLineReader(aFile);
        try {
            return load(r);
        }
        finally {
            IO.close(r);
        }
    }    

    public static TntLex load(LineReader aR) throws IOException {
        aR.setCommentStr("%");  // is it configurable in tnt??
        //CorpusLineReader r = (aMAFile.format() == cCsts) ? new PdtLineReader(aMAFile).setFillLT(false, false).setFillTags(true) : TntReader.openMm(aMAFile);

        TntLex lex = new TntLex();
        for (;;) {
            TntLexEntry e = readEntry(aR);
            if (e == null) break;
            lex.add(e);
        }
        return lex;
    }
    
    public static List<String> readLine(LineReader aR) throws IOException {
        String line;
        do {
            line = aR.readNonEmptyLine();
            if (line == null) return null;
        } while (line.startsWith("@") || line.startsWith("%"));  // todo comment hack
            
        List<String> strs = Strings.splitL(line);
        Err.fAssert(strs.size() >= 4, aR.fErr("Wrong format, should be <form> <total-freq> [tag freq]+"));
        return strs;
    }
    
    public static TntLexEntry readEntry(LineReader aR) throws FormatError, IOException {
        List<String> strings = readLine(aR);
        if (strings == null) return null;
        try {
            TntLexEntry e = new TntLexEntry();
            e.setForm( strings.get(0) );
            //totalFreq is summed by addTagDirect
            for (int i = 2; i < strings.size(); i += 2) {
                e.addTagDirect(Tagset.getDef().fromString(strings.get(i)), Integer.valueOf(strings.get(i+1)));
            }
            return e;
        } catch (NumberFormatException e) {
            throw new FormatError(aR.fErr("Not a number"));
        }
    }

    public static void writeEntry(PrintWriter aW, TntLexEntry aLexEntry) {
            aW.printf("%s   %d", aLexEntry.getForm(), aLexEntry.getTotalFreq());
            
            for (int i=0; i< aLexEntry.getTags().size();i++) {
                aW.print(" " + aLexEntry.getTags().get(i) + " " + aLexEntry.getFreqs().get(i));
            }
            aW.println();
    }
}
