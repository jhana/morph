
package morph.util.tnt;

import java.util.ArrayList;
import java.util.List;
import morph.ts.Tag;
import util.col.Cols;

/**
 *
 * @author Administrator
 */
public class TntLexEntry {
    private String form;
    private int totalFreq;

    private final List<Tag> tags = new ArrayList<Tag>();
    private final List<Integer> freqs = new ArrayList<Integer>();


    public void setForm(String aForm) {
        form = aForm;
    }
    
    public String getForm() {
        return form;
    }

    public int getTotalFreq() {
        return totalFreq;
    }

    public List<Integer> getFreqs() {
        return freqs;
    }

    public List<Tag> getTags() {
        return tags;
    }

    /**
     * @todo OPTIMIZE
     * @param aTag
     * @return
     */
    public int getFreq(Tag aTag) {
        int idx = tags.indexOf(aTag);
        return (idx == -1) ? 0 : freqs.get(idx);
    }

    public void addTags(Iterable<Tag> aTags) {
        for (Tag tag : aTags) {
            addTag(tag);
        }
    }

    public void addTag(Tag aTag) {
        addTag(aTag, 1);
    }

    public void addTag(Tag aTag, int aFreq) {
        int idx = tags.indexOf(aTag);
        if (idx == -1) {
            addTagDirect(aTag, aFreq);
        }
        else {
            freqs.set(idx,freqs.get(idx)+aFreq);
            totalFreq += aFreq;
        }
    }

    public void addTagDirect(Tag aTag, int aFreq) {
        tags.add(aTag);
        freqs.add(aFreq);
        totalFreq += aFreq;
    }
    
    public void incFreqs(int aDelta) {
        for (int i = 0; i < freqs.size(); i++) {
            freqs.set(i, freqs.get(i) + aDelta);
        }
        totalFreq += (aDelta * freqs.size());
    }

    public void makeUniform() {
        for (int i=0; i<freqs.size();i++) {
            freqs.set(i, 1);
        }
        totalFreq = freqs.size();
    }

    public String toString() {
        return "form:" + form + " " + Cols.toString(tags);
    }
}
