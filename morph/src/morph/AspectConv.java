//package morph;
//
//
//import java.io.*;
//import java.util.*;
//import util.io.*;
//import util.*;
//import morph.io.*;
//import morph.util.*;
//import morph.ts.Tag;
//import util.col.Cols;
//import util.col.MultiMap;
//import util.err.Log;
//
///**
// * Converts a pdt corpus in the following way:
// * <ul>
// *  <li> outputs tnt format (just makes things easier)
// *  <li> for Verbs, AC, AG set's 14th slot to one of these values P, I, X (P and I), ? (not specified) based on lemma 
// *  <li> sets all C to Cl
// *  <li> sets all J to J?
// * <ul>
// * 
// * @author jirka
// */
//public class AspectConv extends MUtilityBase { 
//    /**
//     *
//     */
//    public static void main(String[] args) throws IOException {
//        new AspectConv().go(args);
//    }
//
//    public void go(String[] aArgs) throws java.io.IOException {
//        argumentBasics(aArgs,2);
//        
//        XFile inFile  = getArgs().getDirectFile(0, cCsts);
//        XFile outFile = getArgs().getDirectFile(1, cTnt);
//        
//        convert(inFile, outFile);
//    }
//
//
//    /**
//     * 
//     */
//    private void convert(XFile aInFile, XFile aOutFile) throws java.io.IOException {
//        PdtLineReader in = new PdtLineReader(aInFile).setOnlyTokenLines(true);
//        TntWriter out  = new TntWriter(aOutFile);
//
//        in.setMm(false);
//        
//        for(int j=0;;j++) {
//            if (!in.readLine()) break;
//
//            Tag tag = in.tag();
//            if (tag.subPOSIn("Bfipqst")) {  //MGN
//                boolean impf = in.lemma().contains(":T");
//                boolean pf = in.lemma().contains(":W");
//                char aspect;
//                if (pf && impf)
//                    aspect = 'X';
//                else if (pf)
//                    aspect = 'P';
//                else if (impf)
//                    aspect = 'I';
//                else
//                    aspect = '?';
//                    
//                
//                // todo get aspect
//                tag = tag.setSlot(13, aspect);
//            }
//            else if (tag.getPOS() == 'J') {
//                tag = tag.setSlot(1, '?');
//            }
//            else if (tag.getPOS() == 'C') {
//                tag = tag.setSlot(1, 'l');
//            }
//                
//            if (in.lineType().fd()) {
//                out.writeLine(in.form(), tag);
//            }
//                
//            if ((j & 16383) == 0) System.out.print('.');  // 16383 = 2^26-1
////          if ((j & (2<<26-1)) == 0) System.out.print('.');  
//        }
//        in.close();
//        out.close();
//    }
//}
