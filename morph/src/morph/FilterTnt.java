package morph;

import util.*;
import morph.io.*;
import util.io.*;
import java.io.*;
import java.util.*;
import morph.ts.BoolTag;
import morph.ts.Tags;
import morph.ts.Tag;
import morph.util.MUtilityBase;

/* 
 @todo auto naming switch 
 @todo allow setting input format (PDT,TNT) and output format (PDT TNT)
 @todo merge with Forms only? - select what to save
 */

/**
 *
 */
public class FilterTnt extends MUtilityBase {

    /**
     *
     */
    public static void main(String[] args) throws IOException {
        new FilterTnt().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        XFile inFile = getArgs().getDirectFile(0);
        XFile outName = getArgs().getDirectFile(1);

        List<BoolTag> tagFilters = getArgs().boolTags("filterSlotsA", tagset, tagset.cAllTrue);

        XFile[] outFiles = new XFile[tagFilters.size()];
        for (int i = 0; i < tagFilters.size(); i++) {
            outFiles[i] = outName.addExtension(tagFilters.get(i).toCodeStringHF()); 
            if (outName.compression().gz()) // add automatic support
                outFiles[i] = outFiles[i].addExtension("gz"); 
        }

        translate(inFile, outFiles, tagFilters);
    }
    


    void translate(XFile aInFile, XFile[] aOutFiles, List<BoolTag> aConsideredSlots) throws java.io.IOException {
        TntReader in = TntReader.open(aInFile);
        TntWriter[] outs = Battery.tntWriterBattery(aOutFiles);  

        for(int j=0;;j++) {
            if (!in.readLine()) break;
                for (int i = 0; i < aOutFiles.length; i++) {
                    Set<Tag> filteredTags = Tags.filterTags(in.tags(), aConsideredSlots.get(i));
                    outs[i].writeLine(in.form(), filteredTags);
            }
            if ((j & 16383) == 0) System.out.print('.');  // 16383 = 2^26-1
        }
        in.close();
        Battery.close(outs);
    }
}
