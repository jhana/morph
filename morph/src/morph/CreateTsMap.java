package morph;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import lombok.Cleanup;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import morph.util.MUtilityBase;
import util.col.Cols;
import util.err.Err;
import util.err.FormatError;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;
import util.str.Strings;

/**
 *
 * TODO add optional warning if non-determinism is introduced.
 * @author Jirka
 */
public class CreateTsMap extends MUtilityBase {

    public static void main(String[] args) throws IOException {
        new CreateTsMap().go(args);
    }

    interface Rule {
        String getInfo();
        String apply(String aOrigTag, String aCurTag);
    }

    @RequiredArgsConstructor
    @ToString
    class Condition {
        private final Pattern pattern;
        private final boolean positive;

        private Matcher matcher;

        public boolean check(String aString) {
            matcher = pattern.matcher(aString);
            return (positive == matcher.matches());
        }

        /**
         * Note: check must be called first and it must return true;
         * @param aReplacement
         * @return
         */
        public String replace(String aReplacement) {
            return matcher.replaceAll(aReplacement);
        }
    }

    @Data
    class OldRule implements Rule {
        private final Condition origCondition;
        private final Condition curCondition;
        private final String replacement;
        private final String info;

        @Override
        public String apply(String aOrigTag, String aCurTag) {
            if (!origCondition.check(aOrigTag)) return aCurTag;
            if (!curCondition.check(aCurTag)) return aCurTag;

            return curCondition.replace(replacement);
        }
    }

    @Data
    class SlotRule implements Rule {
        private final Condition origCondition;
        private final Condition curCondition;
        private final int slot;
        private final char val;
        private final String info;

        @Override
        public String apply(String aOrigTag, String aCurTag) {
            if (!origCondition.check(aOrigTag)) return aCurTag;
            if (!curCondition.check(aCurTag)) return aCurTag;
            
            try {
                return Strings.setCharAt(aCurTag, slot, val);
            }
            catch(IndexOutOfBoundsException ex) {
                throw new FormatError("Non-existing slot %d in '%s'", slot, aCurTag);
            }
        }
    }

    private int errors = 0;


    /**
     *
     * @param aArgs
     * @throws java.io.IOException
     */
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);
        Err.fAssert(tagset != null, "No tagset specified!");

        final XFile tagsetFile  = getArgs().getDirectFile(0);
        final List<String> origTags = IO.readInColumn(tagsetFile, 0);

        String slotRulePatternStr = String.format("slot\\s+(\\S+)\\s+(\\S+)\\s+([%s])\\s*=\\s*(.)", tagset.cCodeString);
        slotRulePattern = Pattern.compile(slotRulePatternStr);

        final List<Rule> rules = readRules(getArgs().getDirectFile(1));

        final XFile outFile  = getArgs().getDirectFile(2);


        createMap(outFile, origTags, rules);

        if (errors > 0) {
            System.err.printf("There were %d errors", errors);
            System.exit(-1);
        }
    }


// -----------------------------------------------------------------------------
// Compile Rules
// -----------------------------------------------------------------------------
    Pattern slotRulePattern;

    private List<Rule> readRules(XFile aFile) throws IOException {
        @Cleanup final LineReader r = IO.openLineReader(aFile);

        final List<Rule> rules = new ArrayList<Rule>();

        for (;;) {
            String line = r.readNonEmptyLine();
            if (line == null) break;

            try {
                handleLine(r, line.trim(), rules);
            }
            catch(FormatError e) {
                System.err.println(r.message("Error: ", e.getMessage()));
                errors ++;
            }
        }

//        for (Rule rule : rules) {
//            System.out.println("Rule: " + rule);
//        }

        //r.close();
        return rules;

    }

    private void handleLine(LineReader r, String line, List<Rule> rules) {
        if (line.startsWith("test rule")) {
            handleTest(r, line, rules);
        }
        else if (line.startsWith("test all")) {
            Err.fErr(r, "test all not supported yet");
        }
        else if (line.startsWith("rule")) {
            Err.fErr(r, "rule not supported yet");
        }
        else if (line.startsWith("slot")) {
            rules.add(parseSlotRule(r, line));
        }
        else {
            String[] strs = Strings.cWhitespacePattern.split(line);

            if (strs.length == 3) {
                Condition orig = compile(strs[0], r, "1st");
                Condition cur  = compile(strs[1], r, "2nd");
                rules.add(new OldRule(orig,cur,strs[2], getLineInfo(r)));
            }
            else if (strs.length == 4) {


            }
            else {
                Err.fErr(r, "Wrong format");
            }
        }
    }

    String getLineInfo(LineReader aR) {
        return String.format("[%d]: %s", aR.getLineNumber(), aR.getLine());
    }
    
    /**
     * Reads and performs a test of the last rule.
     * @param r
     * @param line
     * @param rules
     */
    private void handleTest(LineReader r, String line, List<Rule> rules) {
        Err.fAssert(!rules.isEmpty(), r, "No rule to test by %s", line);
        String[] strs = Strings.cWhitespacePattern.split(line);
        String orig, cur, expected;
        if (strs.length == 4) {
            cur = orig = strs[2];
            expected = strs[3];
        }
        else if (strs.length == 5) {
            orig     = strs[2];
            cur      = strs[3];
            expected = strs[4];
        }
        else {
            Err.fErr(r, "Format error: test rule <orig> <cur> <expected>\n%s", Cols.toString(Arrays.asList(strs)));
            return; // not used
        }

        String result = null;
        try {
            result = Cols.last(rules).apply(orig, cur);
        }
        catch(Throwable ex) {
            Err.fErr(r, ex, "Test failed - an error occurred during rule executio");
        }
        
        Err.fAssert(result.equals(expected), r, "Test failed. Expected %s, got %s", expected, result);

    }

    SlotRule parseSlotRule(LineReader r, String line) {
        Matcher m = slotRulePattern.matcher(line);
        Err.fAssert(m.matches(), r, "Incorrect slot rule %s", line);

        Condition orig = compile(m.group(1), r, "1st");
        Condition cur  = compile(m.group(2), r, "2st");
        String slot = m.group(3);
        int slotIdx = tagset.code2slot(slot.charAt(0));
        Err.fAssert(slotIdx != -1, "Unknown slot %s (known slots: %s)", slot, tagset.cCodeString );
        String val  = m.group(4);

        return new SlotRule(orig,cur, slotIdx, val.charAt(0), getLineInfo(r));
    }

    protected Condition compile(String aPatternStr, final LineReader aR, String aPatternIdx) {
        // y=X, y=[..], !y=X,
        try {
            final boolean negative = aPatternStr.startsWith("!");
            if (negative) aPatternStr = aPatternStr.substring(1);
            return new Condition(Pattern.compile(aPatternStr), !negative);
        }
        catch(PatternSyntaxException e) {
            Err.fErr(aR, e, "Incorrect %s regex %s", aPatternIdx, aPatternStr);
            return null;
        }
    }

// -----------------------------------------------------------------------------
// Run rules
// -----------------------------------------------------------------------------

    private void createMap(XFile outFile, List<String> origTags, List<Rule> rules) throws IOException {
        @Cleanup PrintWriter w = IO.openPrintWriter(outFile);

        for (String origTag : origTags) {
            String newTag = origTag;

            for(Rule rule : rules) {
                try {
                    newTag = rule.apply(origTag, newTag);
                }
                catch(Throwable ex) {
                    Err.fErr(ex, "An error occurred when applying rule\n %s\n on origTag='%s' and curTag='%s'\n %s", rule.getInfo(), origTag, newTag, ex.getMessage());
                }
            }

            if (!origTag.equals(newTag)) {
                w.println(origTag + " " + newTag);
            }
        }
    }

    /**
     * Both lists must be sorted.
     * @param <T>
     * @param aList
     * @param bList
     * @return
     * @todo make more general, move to Cols
     */
    List<String> minus(List<String> aList, List<String> bList) {
        Set<String> set = new TreeSet<String>(aList);
        set.removeAll(bList);
        List<String> result = new ArrayList<String>(set);
        Collections.sort(result);
        return result;
    }

    private <T> void dump(PrintWriter aW, List<T> aList) {
        for (T tag : aList) {
            aW.println(tag);
        }
    }

}

