package morph.abbrs;

import morph.io.*;
import morph.util.*;
import java.io.*;
import java.util.*;
import morph.util.CorpusProcessor;
import morph.util.WordAction;
import util.str.Strings;
import util.col.Counter;
import util.io.IO;
import util.io.XFile;
import util.str.Caps;

/**
 * Collects info about lemmas and produces a table suitable for statistical processing (by SAS, etc)
 * lemma: # of tokens, # of distinct forms (ic), # distinct tags
 * lemma, 
 * @author  Jiri
 */
public class AbbrGuesser extends CorpusProcessor {
    
    protected Counter<String> mSimpleCounter;
    protected static List<String> cRomanNums = Arrays.asList(
        "II", "III", "IV", "VI", "VII", "VIII", "IX", 
        "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX",
        "XXI", "XXII", "XXIII", "XXIV", "XXV","XXVI","XXVII","XXVIII", "XXIX", "XXX");
    
    PrintWriter mOut;
    PrintWriter mLog;
    
    /**
     */
    public static void main(String[] args) throws IOException {
        new AbbrGuesser().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        
        XFile inFileName   = mArgs.getDirectFile(0);
        XFile outFileName  = mArgs.getDirectFile(1);

        profileStart();
        
        SimpleCounter sc = new SimpleCounter();
        process(sc, inFileName);

        System.out.println("-- Pruning --");
        Pruner pruner = new Pruner(sc);
        process(pruner, inFileName);
        writeOut2(pruner, sc, outFileName);
        
        profileEnd("Lexicon created in ");
    }
    

    /** 
     * Processes a single word
     */
    class SimpleCounter implements WordAction {
        Counter<String> mSimpleCounter;
        boolean mPrevAllCaps;
        String mPrevForm;
        int mAllCapsStretch;    // length of the all caps sequence
        boolean mSPunctBeforeStrech;     // was the all caps sequence preceded by a sentence punctuation? .!?

        Counter<String> mPrevAllCapsCounter;
        Counter<String> mNextAllCapsCounter;
        Counter<String> mSPunctBeforeStrechCounter;

        List<String> cSentencePunctForms = Arrays.asList(".", "?", "!");
        
        public void init() {
            mSimpleCounter = new Counter<String>();
            mPrevAllCaps = false;
            mPrevForm = "";
            mPrevAllCapsCounter = new Counter<String>();
            mNextAllCapsCounter = new Counter<String>();
            mSPunctBeforeStrechCounter = new Counter<String>();
            mAllCapsStretch = 0;    // length of the all caps sequence
            mSPunctBeforeStrech = false;     // was the all caps sequence preceded by a sentence punctuation? .!?
        }

        
        public boolean processWord(WordReader aR) {
            String form  = aR.word();
            boolean allUpper = Caps.isAllUpperCase(form);
            //if (allUpper) System.out.println(form);
            
            
            if (form.length() > 1 && allUpper) {
                mSimpleCounter.add(form);
                if (mPrevAllCaps) {
                    mPrevAllCapsCounter.add(form, mAllCapsStretch);
                    mNextAllCapsCounter.add(mPrevForm);
                    if (mSPunctBeforeStrech) mSPunctBeforeStrechCounter.add(form);
                }
                if (mSimpleCounter.size() > 500000) clean();
            }
            
            if (allUpper) {
                mAllCapsStretch++;
            }
            else {
                mAllCapsStretch = 0;
                mSPunctBeforeStrech = cSentencePunctForms.contains(form);
            }
            
            mPrevAllCaps = allUpper;
            mPrevForm = form;
            return true;
        }

        protected void clean() {
            return;
//            System.out.printf("Word %d, Cleaning - Counter size: %d\n", mWords, mSimpleCounter.size());
//            for (Iterator<Map.Entry<String,Integer>> i = mSimpleCounter.entrySet().iterator(); i.hasNext();) {
//                if (i.next().getValue() < 2) i.remove();
//            }
            //report("Cleaned - Counter size: %d", mSimpleCounter.size());
        }
    }
    

    class Pruner implements WordAction {
        Counter<String> mSimpleCounter;
        Counter<String> mSpecialCounter;

        public Pruner(SimpleCounter aSimpleCounter) {
            mSimpleCounter  = aSimpleCounter.mSimpleCounter;
            mSpecialCounter = new Counter<String>();
        }
        
        public void init() {}

        public boolean processWord(WordReader aR) {
            String form  = aR.word();
            if (!Caps.isAllUpperCase(form) ) {
                String formAC = form.toUpperCase();
                if (mSimpleCounter.frequency(formAC) > 0) {
                    mSpecialCounter.add(formAC);
                }                    
            }
            return true;
        }
    }
    
    protected void writeOut2(Pruner aPr, SimpleCounter aSimpleCounter, XFile aFileName) throws java.io.IOException {
        System.out.println(aFileName);
        PrintWriter w = IO.openPrintWriter(aFileName);
        PrintWriter log = IO.openPrintWriter(aFileName.addExtension("bad"));

        RuleBench ruleBench = new RuleBench();
        ruleBench.addRule(new Rule("freq",        "-5 3 -4 4 -3 5 -2 6 -1 7 0"));
        ruleBench.addRule(new Rule("lowFreq",     "0 3 -1 6 -2 21 -3 31 -4 51 -5"));
        //ruleBench.addRule(new Rule("nextAllCaps", "0 21 -1 41 -3 61 -4 91 -5"));
        //ruleBench.addRule(new Rule("prevAllCaps", "0 21 -1 41 -3 61 -4 91 -5"));
        ruleBench.addRule(new Rule("surrAllCaps", "0 21 -1 41 -3 61 -4 91 -5"));
        ruleBench.addRule(new Rule("len",         "0 5 -1 6 -3 7 -4 8 -5"));
        ruleBench.addRule(new Rule("cons",        "-1 0 0 1 4"));
        
        int entries = 0;
        for (Map.Entry<String,Integer> e : aPr.mSimpleCounter.setSortedByFreq()) {
            String form = e.getKey();
            int capFreq     = e.getValue();
            int specFreq    = pct(aPr.mSpecialCounter.frequency(form), capFreq);
            int prevAllCaps = pct(aSimpleCounter.mPrevAllCapsCounter.frequency(form), capFreq);
            int nextAllCaps = pct(aSimpleCounter.mNextAllCapsCounter.frequency(form), capFreq);

            int surrCaps = (Math.min(prevAllCaps, nextAllCaps)+prevAllCaps+nextAllCaps)/3;
            
            
            int points = ruleBench.getPoints(capFreq, specFreq, surrCaps, form.length(), consCluster(form));
            if (cRomanNums.contains(form)) points = -20; 

            String tail = form + "  //  " + points + " [" + ruleBench.mPointsStr + "]" + capFreq + "  " + specFreq + "  p " + prevAllCaps + "/" + nextAllCaps;

            if (points > -2)
                w.println(tail);
            else if (points > -4)
                w.println(tail);
            else if (points > -6)
                ;//log.println(tail);
            else 
                log.println(tail);
        }

        w.close();
        log.close();
    }

    int consCluster(String aForm) {
        if ( (aForm.length() > 1) && Strings.allCharsIn(aForm.toLowerCase(), "bc\u010Dd\u010Ffghjkmn\u0148pq\u0159s\u0161t\u0165vwz\u017E") ) 
            return 1;
        else 
            return 0;
    }            

    int pct(int a, int b) {
        return Math.round( ((float)a)/((float)b) * 100.0f );
    }

}
