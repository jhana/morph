/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package morph;

/**
 *
 * @author Administrator
 */
public abstract class Translit {

    public String transliterate(String aStr) {
        if (aStr == null) return null;
        StringBuilder buf = new StringBuilder(2*aStr.length());

        for (int i = 0; i < aStr.length(); i++)
            buf.append( transliterate(aStr.charAt(i)) );

        return buf.toString();

    }

    // @todo bug - if all is upper case, digraphs have the second char in lower case
    public String transliterate(char aC) {
        if (Character.isUpperCase(aC)) {
            String tmp = transliterateTbl(Character.toLowerCase(aC));
            if (tmp.length() > 1)
                return Character.toUpperCase(tmp.charAt(0)) + tmp.substring(1);
            else
                return Character.toUpperCase(tmp.charAt(0)) + "";
        }
        else
            return transliterateTbl(aC);
    }

    public abstract String transliterateTbl(char aC);

}
