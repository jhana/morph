package morph;

import java.io.*;
import morph.io.*;
import morph.util.MUtilityBase;
import util.io.*;
import util.str.Transliterate;
import util.io.IO;


/**
 * Converts files from any encoding to any encoding.
 * See http://www.devsphere.com/mapping/docs/guide/encodings.html for possible encodings.
 * <p>
 *
 * To run it: 
 * <br>java Conv <InEncoding> <InFileName> <OutEncoding> <OutFileName>
 * <p> 
 *
 * To convert from Cp1251 (Windows Cyrilics) to ISO8859_5 (ISO Cyrilics) use simply:
 * <br> java Conv <InFileName> <OutFileName>
 * @todo output utf8 optiionaly with bom
 * @todo wildcards on input
 * @todo format conversion
 * @todo file args in xfile format
 */
public class Conv extends MUtilityBase {

    public static void main(String[] args) throws IOException {
        new Conv().go(args);
    }
    
        // todo input XFile and do also format conversions
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);

        XFile iFile = getArgs().getDirectFile(0);
        XFile oFile = getArgs().getDirectFile(1);

        singleFile(iFile, oFile);
    }
//        if (args.length == 4) {
//            argumentBasics(aArgs,4);
//            iEnc = getArgs()args[0];
//            iFile = args[1];
//            oEnc = args[2];
//            oFile = args[3];
//            singleFile(iEnc,iFile,oEnc,oFile);
//        }
//        else if (args.length == 3) {
//            argumentBasics(aArgs,3);
//            iEnc = args[0];
//            iFile = args[1];
//            oEnc = args[2];
//
//            List<File> files;
//            TODO
//            if (iFile.contains("*") || iFile.contains("?")) {
//                File file = new File(iFile);
//                File dir = 
//            }
//            else {
//                File file = new File(iFile);
//                if (file.isDirectory()) {
//                    files = file.l
//                }
//            }
//            
//            if (iFile.contains("*") || iFile.contains("?")) {
//                for (File file : )
//                oFile = autoName(iFile, oEnc);
//                
//            }
//            else {
//                oFile = autoName(iFile, oEnc);
//                singleFile(iEnc,iFile,oEnc,oFile);
////            }
//            
//        }
//        else if (args.length == 2) {     // ISO -> arg[1]
//            iEnc = "ISO8859_5";
//            iFile = args[0];
//            oEnc = args[1];
//            oFile = autoName(iFile, oEnc);
//        }
//        else if ((args.length == 1) && (!args[0].equals("-h"))) {    // Win -> ISO
//            iEnc  = "ISO8859_5";
//            iFile = args[0];
//            oEnc  = "t";
//            oFile = autoName(iFile,oEnc);
//        }

//    }

    
//    public static List<File> getFiles(String aFile) {
//        List<File> files;
//        File file = new File(aFile);
//        
//        if (aFile.contains("*") || aFile.contains("?")) {
//            File dir = file.getParentFile();
//            String lastPart = file.getName();
//            FilenameFilter filter = new FilenameFilter() {
//                @Override
//                public boolean accept(File dir, String name) {
//                    throw new UnsupportedOperationException("Not supported yet.");
//                }
//            }
//            File[] fileArr = dir.listFiles(null); 
//        }
//        else {
//            File file = new File(iFile);
//            if (file.isDirectory()) {
//                files = file.l
//            }
//        }
//        
//    }
    
    protected static void singleFile(String iEnc, String iFile, String oEnc, String oFile) throws IOException {
        XFile xIFile = new XFile(new File(iFile), iEnc);
        
        XFile xOFile = new XFile(new File(oFile), (oEnc.equals("t")) ? "utf8" : oEnc);
        
        if (oEnc.equals("t"))
            transliterate(xIFile, xOFile);
//        else if (args.length > 4 && args[4].equals("w"))
//            wPlain2Plain(xIFile, xOFile);
        else
            conv(xIFile, xOFile);
        
    }

    protected static void singleFile(XFile aInFile, XFile aOutFile)  throws java.io.IOException {
        String oEnc = aOutFile.enc().getId();

        if (oEnc.equals("t")) {
            XFile xOFile = new XFile(aOutFile.file(), "utf8");
            transliterate(aInFile, xOFile);
        }
        else if (oEnc.equals("tr")) {
            XFile xOFile = new XFile(aOutFile.file(), "utf8");
            transliterateX(aInFile, xOFile, new RusSciTranslit());
        }
        else if (aInFile.format().equals(cTxt)) {
            wPlain2Plain(aInFile, aOutFile);
        }
        else {
            conv(aInFile, aOutFile);
        }
    }
    
    
    static String autoName(String aIFileName, String aOEnc) {
        if (aOEnc.equals("Cp1251"))
            return Files.addBeforeExtension(aIFileName,"winCyr");            
        else if (aOEnc.equals("ISO8859_5"))
            return Files.addBeforeExtension(aIFileName,"isoCyr");            
        else if (aOEnc.equals("Cp1250"))
            return Files.addBeforeExtension(aIFileName,"winCE");            
        else if (aOEnc.equals("ISO8859_2"))
            return Files.addBeforeExtension(aIFileName,"isoCE");            
        else if (aOEnc.equals("t"))
            return Files.addBeforeExtension(aIFileName,"tr");            
        else 
            return Files.addBeforeExtension(aIFileName,aOEnc);            
    }
    
    static void wPlain2Plain(XFile aInFile, XFile aOutFile)  throws java.io.IOException {
        TokWordReader r = new TokWordReader(aInFile, null);
        PrintWriter w = IO.openPrintWriter(aOutFile);
        r.ignoreChars("&*@_");
        //r.setPunctChars("%#");
        
        for(;;) {
            if (!r.nextWord()) break;
            // skip id's or what ever it is %%.. + the next line
            if (r.word().equals("%")) {
                r.nextWord();
                assert r.word().equals("%");
                r.nextWord();
                r.nextWord();
                r.nextWord();
            }
            if (r.word().equals("#"))
                w.println(".");
            else {
                w.println(r.word());
            }
        }

        r.close();
        w.close();
    }

    
    static void transliterate(XFile aInFile, XFile aOutFile)  throws java.io.IOException {
        Reader r = IO.openLineReader(aInFile);
        PrintWriter w = IO.openPrintWriter(aOutFile);
        for(;;) {
            int c = r.read();
            if (c == -1) break;
            
            w.write(Transliterate.transliterate((char)c));
        }
        
        r.close();
        w.close();
    }

    static void transliterateX(XFile aInFile, XFile aOutFile, Translit aTranslit)  throws java.io.IOException {
        Reader r = IO.openLineReader(aInFile);
        PrintWriter w = IO.openPrintWriter(aOutFile);
        for(;;) {
            int c = r.read();
            if (c == -1) break;

            w.write(aTranslit.transliterate((char)c));
        }

        r.close();
        w.close();
    }

    // @todo add filters 
    static void conv(XFile aInFile, XFile aOutFile)  throws java.io.IOException {
        Reader r = IO.openLineReader(aInFile);
        PrintWriter w = IO.openPrintWriter(aOutFile);

        for(;;) {
            int c = r.read();
            if (c == -1) break;
            
            //w.write(Transliterate.fromUppsala((char)c));
            w.write(c);
          }
        
        r.close();
        w.close();
    }
}
