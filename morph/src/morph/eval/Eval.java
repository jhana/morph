package morph.eval;

import morph.ts.BoolTag;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import lombok.Cleanup;
import lombok.Cleanup;
import util.*;
import util.col.Counter;
import util.err.Err;
import util.io.*;
import morph.io.*;
import morph.ts.Tagset;
import morph.util.NMaxs;
import util.col.Cols;
import util.err.Log;

/*
 * @todo allow setting SGML tags
 * @todo cross info - table containing frequencies
 * @todo cross info relative to any slot (or subtag) of any tagger/GS  (?combinations?)
 * @todo cross info subtag to subtag
 * @todo details for subtags
 * @todo add options for various overview files type (all, ko w/ context, ..)
 * @todo add PDT input format
 * @todo create filter for importing info into SAS for advanced statistics (if necessary)
 */


/*
 * Notes about precision and recall:
 *
 * Hajic: http://www.cs.jhu.edu/~hajic/courses/cs465/cs46518/ppframe.htm
 *
 * out(w) = set of output items for w
 * true(w) = single correct output for w
 * errors(S)  = Sig ma i in S delta(out(wi) != true(wi)) ???
 * correct(S) = Sigma i in S delta(true(wi) in out(wi))
 * generated(S) = |Sigma i in S (out(wi))|
 *
 * If only one tag per word:
 * Error Rate: Err(S) = Error(S) / |S|
 * Accuracy = 1 - Err(S)
 * @ accuracy = prec = recall (??)
 * @ prec = Correct(S) / Generated(S) = Correct(S) / |S| (=recal) = (|S| - Error(S)) / |S| = 1 - Err(S) = accuracy
 *
 * If multiple or none tag per word:
 * Recall:     R(S) = Correct(S)/ |S|
 * Precisiion: P(S) = Correct(S) / Generated(S)
 * F-measure:  F(S) = 1 / (a/P + (1-a)/R)
 *    if a = 0.5 =>  F(S) = 2PR / (R+P)
 *
 * What we do:
 *  Precision = # tagged as X correctly / # tagged as X
 *  Recall    = # tagged as X correctly / # X
 _
 */


/**
 * The program compares two files in TNT format - a tagged file and a golden standard
 * and reports on accuracy, errors, etc.
 * <p>
 * The program uses 15*4 counters  - {15-positions} * {OK, more, less, gs}.
 * So for each position, it calculates:
 * <ul>
 *   <li> OK - X tagged as X
 *   <li> more - non-X tagged as X
 *   <li> less - X tagged as non-X
 *   <li> gs - statistics about subtag values for calculating entropy.
 * </ul>
 *
 * <p>fnc subTagCounter() is used to initialize the set of 15 counters (1 for each position)
 *
 * <p>fnc calculateErrors(gsTag, taggedTag) increments the counters properly.
 *
 * <p>The counters (class Counter) - if an item is added to the counter it increases
 * the integer associated with the item, if the item is new its integer is set to zero.
 *
 * <p>fnc printResults prints the results.
 *
 * <p>Parameters:
 * <ul>
 *  <li>-od - output directory
 *
 *  <li>-dp {codestring} - prints detailed messages for various subtags
 * </ul>
 */
public class Eval extends EvalBase {

    /**
     * Basic name of the output files that an ending (.overview, .ok, .ko) is added to
     */
    public String mBasicOutFileName;

    /**
     * Directory to write info files (overview, ok, ko) to
     */
    public File mOutDir;

    // --- limiting input ---
    protected Pattern mExcludeForms;

    // Limiting to tags with particular values in particular slot
    protected int mLimitSlot;   // -1=> no limit
    protected String mLimitValues;

    // --- Level of details ---
    protected BoolTag reportedSlots;

    protected int mDetailedSlot;
    protected int mCrossSlot;
    protected BoolTag mIndepForSlots;


    // --- presentation ---
    protected int mMarkGE;
    protected boolean mMarkMax;
    protected boolean mLatex;
    protected boolean mProduceOverviewFiles;

    // --- Objects for recording information ---
    protected int mNrOfTaggers;

    protected EvalInfo[] mInfos;

    protected GsInfo mGsInfo;

    /** Recorder to compute tagger independence */
    protected IndependenceInfo mErrorInfo;

    protected Counter<String> mCubicle;

    protected int mExcludedCount = 0;

// =============================================================================
//
// =============================================================================


    /**
     *
     */
    public static void main(String[] aArgs) throws IOException {
        new Eval().go(aArgs);
    }

    public void go(String[] aArgs) throws IOException {
        argumentBasics(aArgs,2);

        // --- getting files to process (to check, GS) ---
        XFile gsFile = getArgs().getDirectFile(0, cCsts);
        tagset = CorpusLineReader.createTagset(gsFile.getProperties().getOptions()); // tagset is determined by the tagset of the gs file
        
        boolean autonames = getArgs().getBool("autonames");
        List<XFile> toCheckFiles;
        List<BoolTag> consideredSlotsStrings; // What slots taggers consider
        List<XFile> maFiles = null;

        if (autonames) {
            consideredSlotsStrings = getArgs().boolTags("consideredSlots", tagset, tagset.cAllTrueWoVariant);
            XFile baseFileName = getArgs().getDirectFile(1, cCsts);
            toCheckFiles = morph.tag.Utils.x(baseFileName, BoolTag.toCodeStrings(consideredSlotsStrings));
            // todo handle the corresponding ma
        }
        else {
            toCheckFiles = getArgs().getDirectFiles(1, cCsts);
            consideredSlotsStrings = getArgs().boolTags("consideredSlots", tagset, tagset.cAllTrueWoVariant);
            // fill to match toCheckFiles in size
            while (consideredSlotsStrings.size() < toCheckFiles.size()) {
                consideredSlotsStrings.add(Cols.last(consideredSlotsStrings));
            }

            if (getArgs().getBool("maFiles")) {
                maFiles = getArgs().getFiles("maFiles");
            }
        }

        System.out.printf("Considered slots: '%s'\n ", Cols.toString(consideredSlotsStrings));
        System.out.printf("Checked files:\n%s\n", Cols.toStringNl(Arrays.asList(toCheckFiles), "  "));
        System.out.printf("auto: '%s'\n", autonames);

        Err.checkIFiles("gs", gsFile);
        //@TODO Err.checkIFiles("gs;toCheck", gsFile, toCheckFiles);

        // --- input limitation ---
        String excludeFormsStr = getArgs().getString("exclude.words", "");      // @todo getArgs().getRegex(...)
        mExcludeForms = Pattern.compile(excludeFormsStr);

        mLimitSlot   = getArgs().getSlot("limit.slot", tagset, -1);
        mLimitValues = getArgs().getString("limit.values", "");
        Log.info("limit: %s:%s", (mLimitSlot == -1 ? "-" : mLimitSlot), mLimitValues);

        // --- Produce advanced info? ---
        mDetailedSlot  = getArgs().getSlot("detailedSlot", tagset, -1);
        mCrossSlot     = getArgs().getSlot("crossSlot", tagset, -1);
        mIndepForSlots = getArgs().boolTag("indepForSlots", tagset, tagset.cAllFalse);
        Log.info("Details for slot:      %s", mDetailedSlot);
        Log.info("Cros slot:             %s", mCrossSlot);
        Log.info("Independence for slot: %s", mIndepForSlots);


        // --- output options ---

        mOutDir  = getArgs().getDir("output.dir", toCheckFiles.get(0).file().getParentFile());
        XFile logFile = getArgs().getFile("output.log", mOutDir, null);
        boolean omitHeader = getArgs().getBool("output.omitHeader");        // todo
        mProduceOverviewFiles = getArgs().getBool("output.overviewFiles");
        reportedSlots =  getArgs().boolTag("output.slots", tagset, tagset.codestr2booltag("s")); // subpos
        System.out.println("Reported slots: " + reportedSlots);

        mMarkGE  = getArgs().getInt("output.markGE", -1);  // @todo
        mMarkMax = getArgs().getBool("output.markMax");    // @todo
        mLatex   = getArgs().getBool("output.tex");


        // --- Init ---
        mNrOfTaggers = toCheckFiles.size();
        mInfos = new EvalInfo[mNrOfTaggers];
        mGsInfo = new GsInfo(tagset);
        mErrorInfo = new IndependenceInfo(consideredSlotsStrings, mIndepForSlots);
        mCubicle = new Counter<String>();

        // --- Get tagger names ---
        String tmpTaggerNames = getArgs().getString("taggerNames");
        List<String> taggerNames;
        if (tmpTaggerNames == null) {
            taggerNames = new ArrayList<String>();
            for(XFile file : toCheckFiles) {
                taggerNames.add(file.file().getName());
            }
        }
        else {
            taggerNames = Arrays.asList(tmpTaggerNames.split("[:;\\s]"));
            Err.fAssert(taggerNames.size() == toCheckFiles.size(), "# of tagger names must match the # of files to check");
        }

        for (int i = 0; i < mNrOfTaggers; i++) {
            mInfos[i] = new EvalInfo(taggerNames.get(i), tagset, mGsInfo, consideredSlotsStrings.get(i), mDetailedSlot, mCrossSlot);
        }

        checkFiles(gsFile, Files.toArray(toCheckFiles), maFiles);
        printResults(logFile);
    }


// =============================================================================
// Implementation
// =============================================================================

    /**
     * Compares the specified GS and tagged file
     */
    void checkFiles(XFile aGS, XFile[] aToCheckFiles, List<XFile> aMaFiles) throws java.io.IOException {
        Log.info("Checking");

        @Cleanup CorpusLineReader gs  = Corpora.openM(aGS);
        @Cleanup RBattery toCheck = RBattery.openM(aToCheckFiles);
        RBattery maRs = null;
        if (aMaFiles != null) {
            maRs = RBattery.openM(aMaFiles);
        }

        final Overview[] overviews = Overview.overviews(mProduceOverviewFiles, aToCheckFiles, mOutDir);

        for(;;) {
            if (! RBattery.read(gs, toCheck, maRs) ) break;

            Err.fAssert(gs.tag().toString().length() == tagset.getLen(), "Gs tag %s has an incorrect length", gs.tag());
            for (int i = 0; i < mNrOfTaggers; i++) {
                String tag = toCheck.tag(i).toString();
                Err.fAssert(tag.length() == tagset.getLen(), "[file %d:%d] Tag %s has an incorrect length (not %d).\n%s.",
                        toCheck.getLineNumber(), i, tag, tagset.getLen(), toCheck.rs()[i].line());
            }

            if (exclude(gs)) {
                for (int i = 0; i < mNrOfTaggers; i++) {
                    overviews[i].reportOk(gs.getFPlus());
                }
                continue;
            }

            mGsInfo.record(gs.tag());
            mErrorInfo.mGsTag = gs.tag();
            for (int i = 0; i < mNrOfTaggers; i++) {
                boolean ok = gs.tag().eq(toCheck.tag(i), mInfos[i].mConsideredSlots);
                mErrorInfo.record(i, toCheck.tag(i));
                if (ok) mInfos[i].mOKTagNr++;

                overviews[i].report(gs.getFPlus(), toCheck.tag(i), ok);
                mInfos[i].record(gs.tag(), toCheck.tag(i));
            }

//          @todo if cubicle:
//            char c1 = toCheck.tag(0).getCase();
//            char c2 = toCheck.tag(1).getCase();
//            char c3 = toCheck.tag(2).getCase();
//            char cOk = gs.tag().getCase();
//
//            mCubicle.add("" + c1 + c2 + c3 + cOk );
        }

        IO.close(overviews);
        IO.close(maRs);
    }

    /** Should a give form be excluded from evaluation */
    private boolean exclude(CorpusLineReader aGs) {
        boolean exclude =
            mExcludeForms.matcher(aGs.form()).matches() ||
            (mLimitSlot != -1 && !aGs.tag().slotValueIn(mLimitSlot, mLimitValues));

        if (exclude) mExcludedCount ++;
//        if (exclude)
//            mOut.println("Excluding: " + aGs.form());

        return exclude;
    }

// -----------------------------------------------------------------------------
// Reporting
// -----------------------------------------------------------------------------

    void printResults(XFile aLogFile) throws IOException {
        openOut(aLogFile);

        String caption     = getArgs().getString("output.caption");
        if (caption != null) {
            mOut.println(Util.sepp(caption));
        }

        // --- Info for individual taggers ---
        if ((mInfos.length == 1) || getArgs().getBool("indivTaggerInfo")) {
            for (EvalInfo info : mInfos) {
                mOut.println("Tagger: " + info.mTaggerName);
                printResults(info);
                mOut.println();
            }
        }

        // --- Comparison Info ---
        if (mInfos.length > 1)
            printComparison();

        if (mIndepForSlots.hasSomeSet())
            printTaggerIndependence();

        if (getArgs().getBool("cubicle"))
            mOut.println(mCubicle.toSortedString());

        closeOut();
    }

// -----------------------------------------------------------------------------
// Single tagger reporting
// -----------------------------------------------------------------------------

    /**
     * Writes summary info about accuracy, presicion, recall etc. for a tagger
     */
    void printResults(EvalInfo aInfo) {
        mOut.printf("Tokens: %d, words: %d, OK: %d", mGsInfo.mTokenNr, mGsInfo.mTokenNr-mGsInfo.mPunctNr, aInfo.mOKTagNr);
        if (mLimitSlot != -1) mOut.printf("; limited: %s:%s", Tagset.getDef().slotName(mLimitSlot), mLimitValues);


        mOut.printf("\nTags:                 %5.1f%%\n\n", aInfo.accuracyPerc());

        // --- Print basic info for each slot & calculate GSCounter parameters ---
        for (int slot : reportedSlots.toSlotsIdxs()) {
            if (aInfo.mConsideredSlots.get(slot)) {
                mGsInfo.mSlotCounters.get(slot).calculate();       // @todo move out, have a special info just for gs
                mOut.println(slotOverview(aInfo, slot));
            }
        }

        if (mDetailedSlot != -1)
            printDetailedEvalInfo(aInfo);
    }

    /**
     * Print detailed info for each subtag (about each value)
     */
    void printDetailedEvalInfo(EvalInfo aInfo) {
        if ( !aInfo.isSlotConsidered(mDetailedSlot) ) return;

        List<Character> mainValues = aInfo.detValuesList();

        // --- Print info for each value ---
        Table tbl = new Table('\n' + slotOverview(aInfo, mDetailedSlot));
        tbl.init("    Recl:   Prec: X:     %X:   OK: Less: More", mainValues);
        tbl.setSideFormats("     ", "%4s", "  %s: ");
        for (int l = 0; l < mainValues.size(); l++) {
            Character x = mainValues.get(l);
            tbl.setLine(l+1, 1,
                aInfo.detRecall(x), aInfo.detPrecision(x),
                aInfo.detIsX(x),    aInfo.detIsXRatio(x),
                aInfo.detOk(x),     aInfo.detLess(x), aInfo.detMore(x)
            );
        }
        tbl.setCellRenderers(1, 1, tbl.nrOfLines(), 2, new PercentCellRenderer(false));
        tbl.setCellRenderers(1, 2, tbl.nrOfLines(), 3, new PercentCellRenderer(false));
        tbl.setCellRenderers(1, 3, tbl.nrOfLines(), 4, new FormatCellRenderer("%5d"));
        tbl.setCellRenderers(1, 4, tbl.nrOfLines(), 5, new PercentCellRenderer());
        tbl.setCellRenderers(1, 5, tbl.nrOfLines(), 6, new FormatCellRenderer("%5d"));
        tbl.setCellRenderers(1, 6, tbl.nrOfLines(), 7, new FormatCellRenderer("%5d"));
        tbl.setCellRenderers(1, 7, tbl.nrOfLines(), 8, new FormatCellRenderer("%5d"));
        tbl.print(mOut);

        mOut.println();

        if (mCrossSlot != -1) {
            // --- Cross values ---
            List<Character> crossValues = new ArrayList(mGsInfo.values(mCrossSlot));
            crossTable(aInfo, MeasureType.recall, mainValues, crossValues);
            crossTable(aInfo, MeasureType.precision, mainValues, crossValues);
            crossTable(aInfo, MeasureType.fMeasure, mainValues, crossValues);
        }

        // --- Print explanation ---
        mOut.println("\nX    - # of X (in GS)");
        mOut.println("X%   - share of X in the subtag (in GS)");
        mOut.println("OK   - # of times    X was tagged as    X");
        mOut.println("less - # of times    X was tagged as notX");
        mOut.println("more - # of times notX was tagged as    X");
    }

    void crossTable(EvalInfo aInfo, MeasureType aType, List<Character> aMainValues, List<Character> aCrossValues) {
        Table tbl = new Table(" Cross - " + aType.name());
        tbl.init(aCrossValues, aMainValues);
        tbl.setSideFormats("    ", "%7s", "%3s");
        tbl.setCellRenderers(1,1, tbl.nrOfLines(), tbl.nrOfCols(), new PercentCellRenderer(false) );

        for (int i = 0; i < aMainValues.size(); i++ ) {
            for (int j = 0; j < aCrossValues.size(); j++ ) {
                tbl.setCellNaN(i+1,j+1, aInfo.crossMeasure(aType, aMainValues.get(i), aCrossValues.get(j)) );
            }
        }
        tbl.print(mOut);
    }

    /**
     * Returns string with basic info about the specified subtag
     * @param aIdx index of the subtag (0-14)
     * @return info string Position aIdx (subtagname): accuracy, (entropy)
     */
    String slotOverview(EvalInfo aInfo, int aSlot) {
        //return String.format("Position %2d (%-7s) - accuracy: %5.1f%%, entropy: %6.4f", idx, aSlot.text(), aInfo.accuracyPerc(aSlot), mGsInfo.entropy(idx) );
        return String.format("Position %2d (%-7s): %5.1f%%", aSlot, tagset.slotName(aSlot), aInfo.accuracyPerc(aSlot));
    }


// -----------------------------------------------------------------------------
// Comparison reporting
// -----------------------------------------------------------------------------


    void findVotingFilter() {
        // get the best 3 taggers for each slot
        List<NMaxs<Integer,Double>> counters = new ArrayList<NMaxs<Integer,Double>>();

        for (int slot = 0; slot < tagset.getLen(); slot++) {
            NMaxs<Integer,Double> counter = new NMaxs<Integer,Double>(5);
            counters.add(counter);

            for (int c = 0; c < mInfos.length; c++) {         // cols ~ taggers
                if (mInfos[c].isSlotConsidered(slot)) {
                    counter.put(c, mInfos[c].accuracy(slot));
                }
            }
        }

        // create filtering strings
        final StringBuilder names = new StringBuilder();
        final StringBuilder filter = new StringBuilder();
        for (int c = 0; c < mInfos.length; c++) {
            if (names.length() > 0) {
                names.append(';');
                filter.append(';');
            }

            names.append(mInfos[c].mTaggerName);
            for (int slot = 0; slot < tagset.getLen(); slot++) {
                if (counters.get(slot).contains(c) ) {
                    filter.append(tagset.slot2code(slot));
                }
            }
        }
        mOut.println("Best 3 taggers:");
        mOut.println("Names:" + names.toString());
        mOut.println("Use:  " + filter.toString());
    }


    void printComparison() {
        //mOut.println(Util.sepp("Comparison"));

        Table tbl = new SlotsCompEvalTable(mInfos, false);

        // --- set cells ---
        for (int c = 0; c < mInfos.length; c++) {         // cols ~ taggers
            tbl.setCell(1, c+1, mInfos[c].accuracy());
            for (int slot : reportedSlots.toSlotsIdxs()) {  // lines ~ slots
                if (mInfos[c].isSlotConsidered(slot))
                    tbl.setCell(slot+2, c+1, mInfos[c].accuracy(slot));
                else
                    tbl.setCell(slot+2, c+1, null);
            }
        }

        // delete non-reported slots
//        int deletedCounter = 0;
//        for (int slot = 0; slot < tagset.getLen(); slot++) {
//            if (!reportedSlots.get(slot)) {
//                int idx = slot+2-deletedCounter;
//                tbl.deleteLines(idx, idx+1);
//                deletedCounter++;
//            }
//        }
        for (int slot = tagset.getLen()-1; slot >= 0; slot--) {
            if (!reportedSlots.get(slot)) {
                tbl.deleteLines(slot);
            }
        }

        //tbl.insertMaxCol(1,2,1,tbl.nrOfLines(),tbl.nrOfCols(),true);

        if (mMarkGE >= 0) {
            tbl.markGEThan(mMarkGE, 1, 2, tbl.nrOfLines(), tbl.nrOfCols());
        }
        if (mMarkMax) {
            tbl.markMax(1, 1, tbl.nrOfLines(), tbl.nrOfCols());
        }

        //findVotingFilter();

        tbl.print(mOut);

        if (mDetailedSlot != -1) comparisonDetailedEvalInfo(tbl);
    }


    void comparisonDetailedEvalInfo(Table aShortTblx) {
        ValCompEvalTable tbl;

        // --- General info about the slot ---
        mOut.printf("\nPosition %d - (%s)\n", mDetailedSlot, tagset.slotName(mDetailedSlot));

        // --- Print recall ---
        tbl = new ValCompEvalTable(MeasureType.recall, mDetailedSlot, mInfos);
        tbl.insertMaxCol();
        tbl.insertXCol();
        tbl.printConsideredCols(mOut);

        // --- Print precision ---
        tbl = new ValCompEvalTable(MeasureType.precision, mDetailedSlot, mInfos);
        tbl.insertMaxCol();
        tbl.insertXCol();
        tbl.printConsideredCols(mOut);

        // --- Print fMeasure ---
        tbl = new ValCompEvalTable(MeasureType.fMeasure, mDetailedSlot, mInfos);
        tbl.insertMaxCol();
        tbl.insertXCol();
        tbl.printConsideredCols(mOut);
    }


    void printTaggerIndependence() {
        mOut.println(Util.sepp("Tagger independence"));
        mOut.print("      &");
        for (int i = 0; i < mNrOfTaggers; i++) {
            mOut.printf("%5s &", Files.getExtension(mInfos[i].mTaggerName));
        }
        mOut.println();

        for (int i = 0; i < mNrOfTaggers; i++) {
            mOut.printf("%5s &", Files.getExtension(mInfos[i].mTaggerName));
            for (int j = 0; j < mNrOfTaggers; j++) {
                if (mErrorInfo.mSharedSlots[i][j].hasAllCleared())
                    mOut.printf("  - &");
                else
                    mOut.printf("%3d &", Math.round( (1.0 - ((double)mErrorInfo.mCounters[i][j])/((double)mErrorInfo.mCounters[i][i])) *100) );
            }
            mOut.println();
        }
    }

//    /***
//     * Used only for PRT tagset to make subpos unique
//     */
//    Tag tt(Tag aTag) {
//        if (aTag.startsWith("SP")) return aTag.setSubPOS('R');
//        if (aTag.startsWith("RN")) return aTag.setSubPOS('9');
//        if (aTag.startsWith("NP")) return aTag.setSubPOS('Z');
//
//        switch (aTag.getSubPOS()) {
//            case 'C': return tt(aTag, 'C');
//            case 'S': return tt(aTag, 'C');
//            case 'D': return tt(aTag, 'P');
//            case 'E': return tt(aTag, 'P');
//            case 'I': return tt(aTag, 'P');
//            case 'N': return tt(aTag, 'P');
//            case 'P': return tt(aTag, 'P');
//            case 'R': return tt(aTag, 'P');
//            case 'T': return tt(aTag, 'P');
//            case 'A': return tt(aTag, 'V');
//        }
//        return aTag;
//    }
//
//    Tag tt(Tag aTag, char aPos) {
//        if (aTag.getPOS() == aPos) {
//            return aTag.setSubPOS( Character.toLowerCase(aTag.getSubPOS()) );
//        }
//        else
//            return aTag;
//    }


}







//java -ea EvalTNT -dp Pg -cpn "all;Pgnc" "out/test.tnt.voted;test.tnt.Pgnc;" out/gs.tnt.all

//java -ea EvalTNT -dp Pg -cpn "all;Pgnc" "test/test.tnt.voted;test.tnt.Pgnc;" test/gs.tnt.all
