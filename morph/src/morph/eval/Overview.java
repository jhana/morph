package morph.eval;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import morph.ma.lts.Fplus;
import morph.ma.lts.Lt;
import morph.ma.lts.Lts;
import morph.ts.Tag;
import util.Triple;
import util.col.Cols;
import util.col.Mapper;
import util.str.Strings;
import util.io.IO;
import util.io.XFile;

/**
 * Object handling overview files:
 * <ul>
 *  <li>file with marked errors
 *  <li>ko file - file with error forms
 *  <li>ok file - file with correct forms
 * </ul>
 * 
 * @author Jirka
 */
public class Overview implements Closeable {
    private final PrintWriter overview;
    private final PrintWriter ok;
    private final PrintWriter ko;

    public Overview(boolean aProduceOverview, XFile aOutFileBase) throws IOException {
        if (aProduceOverview) {
            overview = IO.openPrintWriter(aOutFileBase.addExtension("overview"));  //? flush after new line
            ok       = IO.openPrintWriter(aOutFileBase.addExtension("ok"));
            ko       = IO.openPrintWriter(aOutFileBase.addExtension("ko"));
        }
        else {
            overview = ok = ko = null;
        }
    }

    @Override
    public void close() {
        if (overview == null) return;
        IO.close(overview, ok, ko);
    }

    /**
     * Used when the word is ignored
     */    
    public void report(String aForm) {
        if (overview != null) {
            overview.println("     " + aForm);
        }
    }

    public void reportOk(Fplus aGsFplus) {
        report(aGsFplus, null, true, true, true);
    }

    public void report(Fplus aGsFplus, Tag aTcTag, boolean aTagOk) {
        report(aGsFplus, new Lts().add("?", aTcTag, null, null), aTagOk, true, aTagOk);
    }
    
    public void report(Fplus aGsFplus, Lts aTcLts, boolean aLtOk, boolean aLemmaOk, boolean aTagOk) {
        if (overview == null) return;
        
        String overviewLine = Strings.concat(aGsFplus.form, "<l>", aGsFplus.lts.ltE().lemma(), "<t>", aGsFplus.lts.ltE().tag().toString());

        if (aLtOk) {
            overview.print("     ");
            if (!aGsFplus.lts.ltE().tag().isPunct()) ok.println(overviewLine);
        }
        else {
            overview.printf("**%c%c ", (aLemmaOk ? ' ' : 'l'), (aTagOk ? ' ' : 't'));
            overviewLine += "<MM>" + ltsString(aTcLts);
            ko.println(overviewLine);
        }

        overview.println(overviewLine);
    }

    final Mapper<Lt, Tag> lt2tag = new Mapper<Lt, Tag>() {
       @Override
        public Tag map(Lt aOrigItem) {
            return aOrigItem.tag();
        }
    };

    final Mapper<Lt, String> lt2str = new Mapper<Lt, String>() {
       @Override
        public String map(Lt aOrigItem) {
            return aOrigItem.lemma() + ":" + aOrigItem.tag();
        }
    };
    
    String ltsString(Lts aTcLts) {
        final StringBuilder sb = new StringBuilder();
        
        final Set<String> lemmas = aTcLts.lemmas();
        
        for (String lemma : lemmas) {
            if (lemmas.size() > 1) sb.append("\n"); 
            final List<?> tags = new util.col.MappingList<Lt,Tag>(aTcLts.col(lemma), lt2tag );
            sb.append("        ").append( lemma ).append(": ");
            sb.append( Cols.toString(tags) );
        }
        
        return sb.toString();
    }
    
    
    // todo condense NNMS.., NNFS -> NN[MF]S
    String shortenTags(Collection<Tag> aTags) {
        return Cols.toString(aTags, "[", "]", ",", "[]");
        // @todo make abbrs configurable return Cols.toString(Tags.toAbbrs(aTags), "[", "]", ",", "[]");
    }
    
    public static Overview[] overviews(boolean aProduceOverview, XFile[] aToCheckFiles, File aOutDir) throws IOException {
        final Overview[] overviews = new Overview[aToCheckFiles.length];
        for (int i = 0; i < overviews.length; i++) {
            overviews[i] = new Overview(aProduceOverview, aToCheckFiles[i].putIntoADir(aOutDir) );
        }
        return overviews;
    }

//    public static void close(Overview[] aOverviews) {
//        for(Overview o : aOverviews)
//            o.close();
//    }
}
