package morph.eval;

import util.io.*;
import java.io.*;
import util.io.IO;
import morph.util.MUtilityBase;
import util.err.Err;



/**
 *
 */
public class CompareMA extends MUtilityBase {

    boolean mLineNrs;
    
    /**
     * CompareMA overviewFile1 overviewFile2 resultFile
     * resultFile shows how overviewFile2 is worse than overviewFile1, i.e. it
     * contains lines that are marked as incorrect in overviewFile2 but not in overviewFile1.
     */
    public static void main(String[] args) throws IOException {
        new CompareMA().go(args);
    }
    
    public void go(String[] aArgs) throws IOException {
        argumentBasics(aArgs,2);

        XFile overviewFile1Base = getArgs().getDirectFile(0);
        XFile overviewFile2Base = getArgs().getDirectFile(1); 

        mLineNrs = getArgs().getBool("lines");

        XFile overviewFile1 = overviewFile1Base.addExtension("overview");
        XFile overviewFile2 = overviewFile2Base.addExtension("overview"); 
        
        File path       = overviewFile1.file().getParentFile();
        XFile outputBase = overviewFile1Base.setFile(path, 
            "c" + overviewFile1Base.file().getName() + '-' + overviewFile2Base.file().getName()); 
        
        checkFile(overviewFile1, overviewFile2, outputBase);
    }

    protected String helpString() {
        return "morph.eval.CompareMA overviewFile1 overviewFile2\n" +
            "@todo";
    }
    
    
    /**
     * Compares the two specified overview files and prints out those lines that are only 
     * in the second one.
     */
    void checkFile(XFile aOverviewFile1, XFile aOverviewFile2, XFile aOutputBase) throws IOException {
        LineNumberReader overview1 = IO.openLineReader(aOverviewFile1); 
        LineNumberReader overview2 = IO.openLineReader(aOverviewFile2);

        PrintWriter better    = IO.openPrintWriter( aOutputBase.addExtension("better") );
        PrintWriter worse     = IO.openPrintWriter( aOutputBase.addExtension("worse") );
        PrintWriter okLessAmb = IO.openPrintWriter( aOutputBase.addExtension("okLessAmb") );
        
        for(;;) {
            String line1 = overview1.readLine();
            String line2 = overview2.readLine();

            if (line1 == null) break;
            Err.fAssert(line2 != null, "Overview files must have the same size");
            
            boolean ok1 = line1.charAt(0) != '*';
            boolean ok2 = line2.charAt(0) != '*';
            
            // NB: First two characters are either spaces (OK lines) or stars (KO lines)
            if (ok1 && !ok2) {
                reportLine(better, line2);
            }
            else if (!ok1 && ok2) {
                reportLine(worse, line1);
            }

            if (ok1 && lessAmbi(line1, line2)) reportOkLessAmb(okLessAmb, line1, line2);
        }    
        
        better.close();
        worse.close();
        okLessAmb.close();
    }
    
    boolean lessAmbi(String aLine1, String aLine2) {
        int idx1 = aLine1.indexOf("<MMt>");
        int idx2 = aLine2.indexOf("<MMt>");
        
        return (aLine1.length() - idx1) < (aLine2.length() - idx2);
    }
    
    void reportLine(PrintWriter aW, String aBadLine) {
        aW.println(aBadLine.substring(2));
    }
    
    void reportOkLessAmb(PrintWriter aW, String aLine1, String aLine2) {
        aW.println(aLine1.substring(2));
        aW.println("  " + aLine2);
    }
//    if (mLineNrs)
//        only2.printf("%4d: %s\n", overview2.getLineNumber(), line2.substring(2));
//    else
//        only2.println(line2.substring(2));
}