package morph.eval;

import java.util.*;
import morph.ts.Tagset;



class EvalTable extends Table {
    public EvalTable() {}
    
    public EvalTable(String aCaption) {
        super(aCaption);
    }

    // --- map(aInfos, .mTaggerName) ---
    static List<String> headers(EvalInfo[] aInfos) {
        final List<String> headers = new ArrayList<String>();
        for (EvalInfo info : aInfos)
            headers.add(info.mTaggerName);
        return headers;
    }
    
    /**
     * 
     * @param tagset
     * @return 
     */
    static List<String> slots(Tagset tagset) {
        final List<String> slots = new ArrayList<String>();
        slots.add("Tag:");
        for (char slot : tagset.cCodeString.toCharArray()) {
            slots.add("" + slot + ":");
        }
        return slots;
    }
}


