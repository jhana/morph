package morph.eval;

import util.str.Strings;
import util.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import morph.io.PdtLineReader;
import morph.util.MUtilityBase;
import util.io.IO;
import util.io.XFile;
import util.str.Caps;

public class EvalAbbr extends MUtilityBase {
    private final static Pattern lemmaSeps = Pattern.compile("[\\_\\-]");
    protected int mAllCaps = 0;
    protected int mAbbrNr = 0;
    protected int mOK = 0;
    protected int mLess = 0;
    protected int mMore = 0;

    
//    protected int mTotalTags = 0;
//    protected int mTotalLemmas = 0;

    protected static boolean mBoth = false;

    /**
     * Printer where the result will be directed 
     * @see #main(String[])
     */
    public java.io.PrintWriter mOut;
    
    private XFile mBasicOutFile;

    private Set<String> mAbbrs;
    
    /**
     *
     */
    public static void main(String[] args) throws IOException {
        new EvalAbbr().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);

        XFile gsFile = getArgs().getDirectFile(0); 
        XFile toCheckFile = getArgs().getDirectFile(1);
        mBasicOutFile = getArgs().getDirectFile(2);

        //loadAbbr(abbrFileName);
        //loadLocalAbbr(gsFileName, abbrFileName);
        checkFile(gsFile, toCheckFile);
    }

    protected String helpString() {
        return "morph.eval.EvalAbbr {GSFile} {toCheck} ???\n";
    }

    void loadAbbr(XFile aFile) throws java.io.IOException {
        mAbbrs = loadAbbrSet(aFile);
    }
    
//    void loadLocalAbbr(XFile aGS, XFile aGlobAbbr) throws java.io.IOException {
//        // --- acquire abbrs locally ---
//        PdtLineReader gs      = new PdtLineReader(aGS);
//
//        morph.learn.AbbrGuesser abbrGuesser = new morph.learn.AbbrGuesser();
//        String[] cmdLine = new String[] {"-ie", aGsEnc, "-oe", aGsEnc, aGS.getPath(), mBasicOutFileName + ".tmp"};
//        abbrGuesser.go(cmdLine);
//
//        Set<String> localAbbrs = loadAbbrSet(mBasicOutFileName + ".tmp");
//        Set<String> badAbbrs   = loadAbbrSet(IO.addExtension(aGlobAbbr, "bad"), aAbbrEnc);
//        
//        localAbbrs.removeAll(badAbbrs);
//        System.out.println("Local left: " + localAbbrs.size());
//        localAbbrs.removeAll(mAbbrs);
//        System.out.println("Local left: " + localAbbrs.size());
//        mAbbrs.addAll(localAbbrs);
//    }
    
    private Set<String> loadAbbrSet(XFile aFileName) throws java.io.IOException {
        Set<String> abbrs = new TreeSet<String>();
        
        LineNumberReader r = IO.openLineReader(aFileName);
        
        for (;;) {
            String line = r.readLine();
            if (line == null) break; 
            if (line.trim().length() == 0) continue;
            
            
            String[] tokens = Strings.split(line);
            assert (tokens.length > 0) : r.getLineNumber() + " " + line;
            
            String abbr = Strings.split(line)[0];
            abbrs.add(abbr);
        }
        r.close();
        return abbrs;
    }

   
    
    /**
     * Compares the specified GS and tagged file
     */
    void checkFile(XFile aGS, XFile aToCheck) throws java.io.IOException {
        PdtLineReader gs      = new PdtLineReader(aGS);
        PdtLineReader toCheck = new PdtLineReader(aToCheck).setTagTag("MMt");
        
        PrintWriter overview = IO.openPrintWriter(mBasicOutFile.addExtension("overview"));  
        PrintWriter ok       = IO.openPrintWriter(mBasicOutFile.addExtension("ok"));
        PrintWriter ko       = IO.openPrintWriter(mBasicOutFile.addExtension("ko"));
        PrintWriter log      = IO.openPrintWriter(mBasicOutFile.addExtension("log"));
        
        for(;;) {
            if (!gs.readLine()) break;
            if (!toCheck.readLine()) break;

            String form    = gs.form();
            String gsLemma = gs.lemma();

            if ( !Caps.isAllUpperCase(form) || form.length() == 1) {
                overview.println("  " + form);
                continue;
            }

            boolean consideredAbbr = toCheck.tag().startsWith("888");

            
            boolean isAbbr = Caps.isAllUpperCase(gs.lemmaWord()) && (gs.lemma().indexOf('`') == -1);
            isAbbr = isAbbr || gsLemma.equals("televize_:B") || gsLemma.startsWith("Bundesnachrichttendienst_:B_;K_,t") ||
                      gsLemma.equals("Beso_;K") || gsLemma.equals("Liaz-1_;K");
            isAbbr = isAbbr && !gsLemma.startsWith("Sn-2_:B_;H");
            
            // -- likely to be an error in PDT marking --
            boolean notB8 = false; //isAbbr && (!gs.lemma().contains("_:B")) && (gs.tag().getVariant() != '8');
            boolean weird = !isAbbr && (gs.lemma().contains("_:B") || (gs.tag().getVariant() == '8'));

//            boolean consideredAbbr = mAbbrs.contains(form);
            
            mAllCaps++;
            if (isAbbr) mAbbrNr++;

            if (isAbbr && consideredAbbr) {
                overview.println("  " + form);
                ok.println(gs.lemma());
                mOK++;
            }
            else if (isAbbr && !consideredAbbr) {
                overview.println("--" + form);
                ko.println("--" + form + "\t" + gs.lemma());
                mLess++;
            }
            else if (!isAbbr && consideredAbbr) {
                overview.println("++" + form);
                ko.println("++" + form + "\t" + gs.lemma());
                mMore++;
            }
            
            if (notB8 || weird) log.println(gs.lemma());
        }

        gs.close();
        overview.close();
        ok.close();
        ko.close();
        log.close();

        System.out.printf("all-caps - All: %d   Abbrs: %d\n", mAllCaps, mAbbrNr);
        System.out.printf("Error: %4.1f\n", 100.0 - Util.perc(mOK, mAbbrNr));
        System.out.printf("More: %4.1f\n", Util.perc(mMore, mAbbrNr));
        System.out.printf("Less: %4.1f\n", Util.perc(mLess, mAbbrNr));
        System.out.printf("Recall: %4.1f\n", Util.perc(mOK, mAbbrNr));
        System.out.printf("Prec: %4.1f\n", Util.perc(mOK, mAbbrNr+mMore));
        
        float recall = Util.perc(mOK, mAbbrNr);
        float prec =   Util.perc(mOK, mAbbrNr+mMore);
        float fmeasure = 2*recall*prec / (recall+prec);
        System.out.printf("F-measure: %4.1f\n", fmeasure);
    }

    void error() {
        System.out.println("Error!");
    }
}