/*
 * Table.java
 *
 * Created on May 11, 2004, 7:54 AM
 */
package morph.eval;

import util.*;
import java.util.*;
import java.io.*;


/**
 * Compares tagges on slots
 */
class SlotsCompEvalTable extends EvalTable {
    public SlotsCompEvalTable(EvalInfo[] aInfos, boolean aReverse) {
        init(headers(aInfos), slots(aInfos[0].tagset));
        setSideFormats("     ", "%7s", "%4s");
        setCellRenderers(1, 1, nrOfLines(), nrOfCols(), new PercentCellRenderer(aReverse) );
    }
    
}
