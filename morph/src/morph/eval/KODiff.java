package morph.eval;

import java.io.*;
import java.util.*;
import util.*;
import util.err.Err;
import util.io.*;
import morph.*;



/**
 *
 */
public class KODiff extends morph.util.MUtilityBase {

    boolean mLineNrs;
    
    /**
     * KODiff overviewFile1 overviewFile2 resultFile
     * resultFile shows how overviewFile2 is worse than overviewFile1, i.e. it
     * contains lines that are marked as incorrect in overviewFile2 but not in overviewFile1.
     */
    public static void main(String[] args) throws IOException {
        new KODiff().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);

        XFile overview1FileBasis = getArgs().getDirectFile(0);
        XFile overview2FileBasis = getArgs().getDirectFile(1); 
        XFile only2File          = getArgs().getDirectFile(2); 
        mLineNrs = getArgs().getBool("lineNumbers");
        
        XFile overview1File = overview1FileBasis.addExtension("overview");
        XFile overview2File = overview2FileBasis.addExtension("overview"); 
        Err.checkIFiles("overview1;overview2", overview1File, overview2File);
        
        checkFile(overview1File, overview2File, only2File);
        checkFile(overview2File, overview1File, only2File.addExtension("rev"));
    }

    /**
     * Compares the two specified overview files and prints out those lines that are only 
     * in the second one.
     */
    void checkFile(XFile aOverview1File, XFile aOverview2File, XFile aKO2OnlyFile) throws java.io.IOException {
        LineNumberReader overview1 = IO.openLineReader(aOverview1File);
        LineNumberReader overview2 = IO.openLineReader(aOverview2File);

        PrintWriter only2 = IO.openPrintWriter(aKO2OnlyFile);
        
        for(;;) {
            String line1 = overview1.readLine();
            String line2 = overview2.readLine();

            if (line1 == null) break;
            assert (line2 != null);
            
            // NB: First two characters are either spaces (OK lines) or stars (KO lines)
            if (line1.charAt(0) != '*' && line2.charAt(0) == '*')   
                if (mLineNrs)
                    only2.printf("%4d: %s\n", overview2.getLineNumber(), line2.substring(2));
                else
                    only2.println(line2.substring(2));
        }
        
        overview1.close();
        overview2.close();
        only2.close();
    }
}