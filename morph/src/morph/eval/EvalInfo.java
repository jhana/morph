package morph.eval;

import java.util.*;
import morph.ts.BoolTag;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.Util;
import util.col.Counter;

/**
 * @todo NOT USED!!
 */
//class SimpleErrorInfo {
//    Tagset tagset;
//
//    int[][] mCounters;
//    
//    boolean[] mErrorVector; //@to remove (or to specialized class)
//
//    
//    int mTaggerNr;
//    
//    SimpleErrorInfo(Tagset aTagset, int aTaggerNr) {
//        tagset = aTagset;
//        mTaggerNr = aTaggerNr;
//        mErrorVector = new boolean [mTaggerNr];
//         
//        mCounters = new int[mTaggerNr][];
//        for (int i = 0; i < mTaggerNr; i++) 
//            mCounters[i] = new int [mTaggerNr];
//    }
//
//
//    void record(int aTaggerIdx, boolean aOk) {
//        mErrorVector[aTaggerIdx] = !aOk;
//        
//        if (aTaggerIdx == mTaggerNr - 1) {
//            for (int i = 0; i < mTaggerNr; i++) {
//                boolean iTaggerError = mErrorVector[i];
//                for (int j = 0; j < mTaggerNr; j++) {
//                    if (iTaggerError && mErrorVector[j])     // both wrong
//                        mCounters[i][j]++;
//                }
//            }
//        }
//    }
//}




abstract class BaseInfo {
    
    /** 
     * Creates and initializes a counter every slot
     */
    protected final static ArrayList<Counter<Character>> slotCounters(final Tagset aTagset) {
        final ArrayList<Counter<Character>> tmp = new ArrayList<Counter<Character>>();
        for (int i = 0; i < aTagset.getLen(); i++) 
            tmp.add(new Counter<Character>());
        return tmp;
    }

//    abstract SortedSet<Character> values(int aPos);
//
//    /**  
//     * Set of all the possible values for certain subtag.
//     * Collects the values from both the correct and incorrect taggings
//     * @todo - collect just once from GS
//     */
//    List<Character> valuesList(int aPos) {
//        return new ArrayList<Character>(values(aPos));
//    }
}


public class EvalInfo { 
    
    Tagset tagset;
    
    /**
     * Look only at those position (bool tag)
     */
    protected String mTaggerName;

    /**
     * null if no cross slot statistics desired
     */
    int mDetailedSlot;
    
    /**
     * null if no cross slot statistics desired
     */
    int mCrossSlot;

    /**
     * Look only at those position (bool tag)
     */
    protected BoolTag mConsideredSlots;

//    /**
//     * Look only at those position (code string)
//     */
//    protected SlotString mConsideredSlotsString;


    /** 
     * Shared GS info
     */
    protected GsInfo mGsInfo;

    /**
     * Number of correct tags.
     */
    protected int mOKTagNr = 0;

    /**
     * Number of correct values for each slot.
     */
    protected int mOKNr[];
    
    // --- Detail counters (counts by values)---
    protected Counter<Character> mTaggedCorrectly;    
    protected Counter<Character> mNotApplied;
    protected Counter<Character> mAppliedToWrong;

    // --- Cross Counters ---
    protected CrossCounter mCrossTaggedCorrectly;    // position -> Counter by value
    protected CrossCounter mCrossNotApplied;
    protected CrossCounter mCrossAppliedToWrong;

    
    public EvalInfo(String aTaggerName, Tagset aTagset, GsInfo aGsInfo, BoolTag aConsideredSlotsString, int aDetailedSlot, int aCrossSlot) {
        mTaggerName = aTaggerName;
        tagset = aTagset;
        mGsInfo = aGsInfo;
        //mConsideredSlotsString = aConsideredSlotsString;
        mConsideredSlots = aConsideredSlotsString;

        mDetailedSlot = aDetailedSlot;
        if (mDetailedSlot != -1 && !mConsideredSlots.get(mDetailedSlot)) mDetailedSlot = -1;
        
        mCrossSlot = aCrossSlot;
        
        mOKNr = new int[tagset.getLen()];
        for (int i = 0; i < tagset.getLen(); i++) 
            mOKNr[i] = 0;

        if (mDetailedSlot != -1) {
            mTaggedCorrectly  = new Counter<Character>();
            mNotApplied       = new Counter<Character>();
            mAppliedToWrong   = new Counter<Character>();
        
            if (mCrossSlot != -1) {
                mCrossTaggedCorrectly = new CrossCounter(mDetailedSlot, mCrossSlot);
                mCrossNotApplied      = new CrossCounter(mDetailedSlot, mCrossSlot);
                mCrossAppliedToWrong  = new CrossCounter(mDetailedSlot, mCrossSlot);
            }
        }
    }


    
    /** 
     * For each slot, records values, errors and successes.
     * @param gsTag the correct tag
     * @param taggedTag the tag provided by the tagger
     */ 
    void record(Tag aGsTag, Tag aTaggedTag) {
        for (int slot = 0; slot < aGsTag.getLen(); slot++) {
            if ( ! isSlotConsidered(slot) ) continue;

            char gsSubTag     = aGsTag.getSlot(slot);
            char taggedSubTag = aTaggedTag.getSlot(slot);

            if (taggedSubTag == gsSubTag) 
                mOKNr[slot]++;

            // --- detailed information ---
            if (slot == mDetailedSlot) {
                recordDetails(gsSubTag, taggedSubTag);

                // --- detailed cross information ---
                if (mCrossSlot != -1) {
                    char crossGsSubTag = aGsTag.getSlot(mCrossSlot);
                    recordCross(gsSubTag, taggedSubTag, crossGsSubTag);
                }
            }
        }
    }

    protected void recordDetails(char aGsSubTag, char aTaggedSubTag) {  
        if (aTaggedSubTag == aGsSubTag) {
            mTaggedCorrectly.add(aTaggedSubTag);
        }
        else {
            mNotApplied.add(aGsSubTag);
            mAppliedToWrong.add(aTaggedSubTag);
        }
    }

    protected void recordCross(char aGsSubTag, char aTaggedSubTag, char aCrossGsSubTag) {  
        if (aTaggedSubTag == aGsSubTag) {
            mCrossTaggedCorrectly.add(aTaggedSubTag, aCrossGsSubTag);
        }
        else {
            mCrossNotApplied.add(aGsSubTag, aCrossGsSubTag);
            mCrossAppliedToWrong.add(aTaggedSubTag, aCrossGsSubTag);
        }
    }
    
// -----------------------------------------------------------------------------
// Absolute numbers
// -----------------------------------------------------------------------------

    double accuracyPerc() {
        return Util.perc(mOKTagNr,mGsInfo.mTokenNr);
    }

    double accuracy() {
        return ((double)mOKTagNr)/((double)mGsInfo.mTokenNr);
    }

// -----------------------------------------------------------------------------
// Slot numbers
// -----------------------------------------------------------------------------

    double accuracy(int aSlot) {
        return ((double)mOKNr[aSlot])/((double)mGsInfo.mTokenNr);
    }

    double accuracyPerc(int aSlot) {
        return Util.perc(mOKNr[aSlot],mGsInfo.mTokenNr);
    }


// -----------------------------------------------------------------------------
// Slot-Value numbers
// -----------------------------------------------------------------------------

    int detIsX(char aVal) {
        return detOk(aVal) + detLess(aVal);
        // @todo =/= !!!!!!!!!!!!!!!
        //mGsInfo.mSlotCounters.get(mDetailedSlot).frequency(aVal);
    }
    
    double detIsXRatio(char aVal) {
        return ((double)detIsX(aVal)) / ((double) mGsInfo.mSlotCounters.get(mDetailedSlot).sigma());  //@todo mSlotCounters can be some Enum Array
    }
                    
    int detTaggedAsX(char aVal) {
        return detOk(aVal) + detMore(aVal);
    }

    
    int detOk(char aVal) {
        return mTaggedCorrectly.frequency(aVal);
    }
    
    int detMore(char aVal) {
        return mAppliedToWrong.frequency(aVal);
    }
    
    int detLess(char aVal) {
        return mNotApplied.frequency(aVal);       // x was tagged as not-x  
    }
    

    double detMeasure(MeasureType aMeasure, char aVal) {
        switch (aMeasure) {
            case precision: return detPrecision(aVal);
            case recall:    return detRecall(aVal);
            case fMeasure:  return detFMeasure(aVal);
        }
        return 0.0;
    }

    double detPrecision(char aVal) {
        return ((double)detOk(aVal))/ ((double)detTaggedAsX(aVal) );      // # tagged as X correctly / # tagged as X 
    }

    double detRecall(char aVal) {
        return ((double)detOk(aVal))/((double)detIsX(aVal));      // # tagged as X correctly / # X
    }
    
    double detFMeasure(char aVal) {
        double prec = detPrecision(aVal);
        double recall = detRecall(aVal);
        return 2.0*prec*recall / (prec + recall);
    }
    

// -----------------------------------------------------------------------------
// Cross slot-slot value-value
// -----------------------------------------------------------------------------

    int crossIsX(char aMainVal, char aCrossVal) {
        return crossOk(aMainVal, aCrossVal) + crossLess(aMainVal, aCrossVal);
    }
    
//    float isXPerc(int aMainSlot, int aCrossSlot, char aMainVal, char aCrossVal) {
//        return Util.perc(isX(aPos,aVal),(int)mSlotCounters.get(aPos).mSigma);
//    }

    int crossTaggedAsX(char aMainVal, char aCrossVal) {
        return crossOk(aMainVal, aCrossVal) + crossMore(aMainVal, aCrossVal);
    }

    int crossOk(char aMainVal, char aCrossVal) {
        return mCrossTaggedCorrectly.frequency(aMainVal, aCrossVal);
    }
    
    int crossMore(char aMainVal, char aCrossVal) {
        return mCrossAppliedToWrong.frequency(aMainVal, aCrossVal);
    }
    
    int crossLess(char aMainVal, char aCrossVal) {
        return mCrossNotApplied.frequency(aMainVal, aCrossVal);
    }

    double crossRecall(char aMainVal, char aCrossVal) {
        return ((double)crossOk(aMainVal, aCrossVal)) / ((double)crossIsX(aMainVal, aCrossVal));      // # tagged as X correctly / # X
    }
    
    double crossPrecision(char aMainVal, char aCrossVal) {
        return ((double)crossOk(aMainVal, aCrossVal)) / ((double)crossTaggedAsX(aMainVal, aCrossVal));      // # tagged as X correctly / # X
    }

    double crossFMeasure(char aMainVal, char aCrossVal) {
        double prec = crossPrecision(aMainVal, aCrossVal);
        double recall = crossRecall(aMainVal, aCrossVal);
        return 2.0*prec*recall / (prec + recall);
    }
    
    double crossMeasure(MeasureType aMeasure, char aMainValue, char aCrossVal) {
        switch (aMeasure) {
            case precision: return crossPrecision(aMainValue, aCrossVal);
            case recall:    return crossRecall(aMainValue, aCrossVal);
            case fMeasure:  return crossFMeasure(aMainValue, aCrossVal);
        }
        return 0.0;
    }

    public boolean isSlotConsidered(int aSlot) {
        return mConsideredSlots.get(aSlot);
    }
    
// -----------------------------------------------------------------------------
// Implementation
// -----------------------------------------------------------------------------


    /**  
     * Set of all the possible values for certain subtag.
     * Collects the values from both the correct and incorrect taggings
     * @todo - collect just once from GS
     */
    SortedSet<Character> detValues() {
        assert mDetailedSlot != -1;
        SortedSet<Character> values = new TreeSet<Character>();
        values.addAll( mTaggedCorrectly.keySet() );
        values.addAll( mNotApplied.keySet() );
        return values;
    }

    List<Character> detValuesList() {
        return new ArrayList<Character>(detValues());
    }
}


