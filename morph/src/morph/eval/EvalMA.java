package morph.eval;

import morph.ts.BoolTag;
import morph.ts.Tag;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Cleanup;
import util.err.Err;
import util.io.*;
import morph.io.*;
import morph.ma.lts.Fplus;
import morph.ma.lts.Lt;
import morph.ma.lts.Lts;
import util.Triple;
import util.err.Log;
import util.fncs.Eq;
import util.fncs.Eqs;

/*
 * If multiple or none tag per word:
 * Recall:     R(S) = Correct(S)/ |S|
 * Precisiion: P(S) = Correct(S) / Generated(S)
 * F-measure:  F(S) = 1 / (a/P + (1-a)/R)
 *    if a = 0.5 =>  F(S) = 2PR / (R+P)
 */
/**
 * TODO: gracefully report errors in corpora:
 *  wrong tags
 *  multiple tags in gs
 */
public class EvalMA extends EvalBase {
    /** Comparator comparing lt objects considering lemmas only */
    private Eq<Lt> lemmaEq;
    /** Comparator comparing lt objects considering tags only */
    private Eq<Lt> tagEq;
    /** Comparator comparing lt objects considering both lemmas and tags */
    private Eq<Lt> ltEq;
    
    // --- structures to keep track of things ---
    /** Info for the whole corpus (currently not filled when breakup is requested) */
    private Info mAllInfo;
    /** Info for each value of the breakup slot */
    final protected SortedMap<Character, Info> mBreakupInfo = new TreeMap<Character, Info>();

    
    // --- Limiting input --- 
    /** to certain forms */
    protected Pattern limitForm;
    /** to certain tags */
    protected Pattern limitGsTag;
    protected int mLimitSlot;   // -1 => no limit
    protected String mLimitValues;

    
    /** Break down results by values of this slot. null =&gt; no break */
    protected int mBreakBySlot; 
    
    protected boolean mGetFreq;
    

    
    /**
     *
     */
    public static void main(String[] args) throws IOException {
        new EvalMA().go(args,2);
    }
    
    @Override public void goE() throws java.io.IOException {
        final XFile gsFile = getArgs().getDirectFile(0);
        final XFile maFile = getArgs().getDirectFile(1);
        Err.checkIFiles("gs;ma", gsFile, maFile);
        
        // --- Check only some tags/forms ---
        limitGsTag = getArgs().getPatternNE("limit.tag", ".*");
        
        mLimitSlot   = getArgs().getSlot("limit.slot", tagset, -1);
        mLimitValues = getArgs().getString("limit.values", "");
        Log.info("limit: %s:%s", (mLimitSlot == -1 ? "-" : mLimitSlot), mLimitValues);

        limitForm          = getArgs().getPatternNE("narrow");

        // --- Comparison options ---
        createComparators();



        mAllInfo = new Info(ltEq, lemmaEq, tagEq);

        mBreakBySlot = getArgs().getSlot("breakBySlot", tagset, -1);
        Log.info( mBreakBySlot == -1 ? "No break down" : ("Break down by: " + mBreakBySlot) );
        
        mGetFreq    = getArgs().getBool("freq");

        // --- output options ---
        File outDir   = getArgs().getDir("output.dir", maFile.file().getParentFile());
        XFile logFile = getArgs().getFile("output.log", outDir, null);
        boolean omitHeader = getArgs().getBool("output.omitHeader");

        // -- create outFileBase --
        String outNameBaseDef = maFile.file().getName();
        if (mLimitSlot != -1) outNameBaseDef += '.' + mLimitValues;
        String outNameBase = getArgs().getString("output.fileNameBase", outNameBaseDef);
        XFile outFileBase  = maFile.setFile(outDir, outNameBase);
        String ext = getArgs().getString("output.ext", null);
        if (ext != null) {
            outFileBase  = outFileBase.addExtension(ext);
        }

        // -- overview files options --
        boolean produceOverview = getArgs().getBool("output.overviewFiles");
        Overview overview = new Overview(produceOverview, outFileBase);
        
        checkFile(gsFile, maFile, overview);

        overview.close();
        
        printResults(logFile, omitHeader, outFileBase);
    }

    /** Creates lemma, tag and lt comparators based on user's options */
    private void createComparators() {
        final boolean lemmasCheck          = getArgs().getBool("lemmas.check", false);
        final Pattern lemmasGsExtract      = getArgs().getPatternNE("lemmas.gsExtract");
        System.out.println("lemmasGsExtract: " + lemmasGsExtract);
        final Integer lemmasGsExtractGroup = getArgs().getInt("lemmas.gsExtractGroup", 1);
        final boolean lemmasIgnoreCase     = getArgs().getBool("lemmas.ignoreCase");

        if (lemmasCheck) {
            lemmaEq = new Eq<Lt>() {
                @Override
                public boolean equals(Lt a, Lt b) {
                    final String blemma = lemmasGsExtract == null ? b.lemma() : extract(b.lemma(), lemmasGsExtract, lemmasGsExtractGroup);
                    Err.fAssert(blemma != null, "Cannot compare null lemmas");

                    boolean test = lemmasIgnoreCase ? blemma.equalsIgnoreCase(a.lemma()) : blemma.equals(a.lemma());
//                    if (!test) {
//                        System.out.printf("Lemma: %s > %s =/= %s\n", b.lemma(), blemma, a.lemma() );
//                    }
                    return test;
                }
            };
        }
        else {
            lemmaEq = Eqs.trueEq();
        }

        BoolTag mConsideredSlots = getArgs().boolTag("considerSlots", tagset, tagset.cAllTrueWoVariant);
        mConsideredSlots = getArgs().boolTag("considerSlotsX", tagset, mConsideredSlots);       // todo drop
        System.out.printf("considered positions: %s", mConsideredSlots);
        final BoolTag consideredSlots = mConsideredSlots;

        if (consideredSlots.hasAllSet()) {
            System.out.println("hasAll set tagEq = 2");
            tagEq = new Eq<Lt>() {
                @Override
                public boolean equals(Lt a, Lt b) {
                    return a.tag().equals(b.tag());
                }
            };
        }
        else {
            tagEq = new Eq<Lt>() {
                @Override
                public boolean equals(Lt a, Lt b) {
                    return a.tag().eq(b.tag(), consideredSlots);
                }
            };
        }

        ltEq = Eqs.and(lemmaEq, tagEq);
    }

    /**
     * Extracts a substring from a string, using a regex.
     * @param aStr string to extract from
     * @param aPattern pattern used for extraction
     * @param aGroup index of the group to extract
     * @return extracted string 
     * to util
     */
    public static String extract(final String aStr, final Pattern aPattern, int aGroup) {
        final Matcher matcher = aPattern.matcher(aStr);
        if (matcher.matches()) {
            return matcher.group(aGroup);
        }
        else {
            return null;
        }

    }


    /**
     * Compares the specified GS and tagged file
     */
    private void checkFile(XFile aGS, XFile aToCheck, Overview aOverview) throws java.io.IOException {
//        System.out.println("aGS:" + aGS );
//        System.out.println("aToCheck:" + aToCheck );
        @Cleanup CorpusLineReader gs      = Corpora.openM(aGS);
        @Cleanup CorpusLineReader toCheck = Corpora.openMm(aToCheck);

        try {
            checkFileM(gs, toCheck, aOverview);
        }
        catch(Exception e) {
            System.out.println("Error: " + e.getMessage());
            System.out.printf("gs: line: %d, line: %s\n", gs.getLineNumber(), gs.line());
            System.out.printf("tc: line: %d, line: %s\n", toCheck.getLineNumber(), toCheck.line());
            throw new RuntimeException(e);
        }
    }

    /**
     * Info object for a particular value of the observed slot.
     * @param aTag
     * @return 
     */
    private Info getBInfo(Tag aTag) {
        if (mBreakBySlot == -1) return mAllInfo;
    
        final Character val = aTag.getSlot(mBreakBySlot);
        Info bInfo = mBreakupInfo.get(val);
        if (bInfo == null) {
            bInfo = new Info(ltEq, lemmaEq, tagEq);                                              // info for the whole corpus (currently not filled when breakup is requested);
            mBreakupInfo.put(val, bInfo);
        }
        return bInfo;
    }
    
            //Tag    gsTag   = gs.tag().filterTag(mConsideredSlots);
    
    
    void checkFileM(CorpusLineReader gs, CorpusLineReader toCheck, Overview aOverview) throws IOException {
        
        for(;;) {
            if (!gs.readLine()) break;
            if (!toCheck.readLine()) {error(); break;}
            
            // --- Get GS info ---
            Fplus gsFplus = gs.getFPlus();
            Tag gsTag   = gsFplus.lts.ltE().tag();
            String form = gsFplus.form;
            
            // -- get frequency -- 
          int freq = 1;             // @todo get for pdt, set to 1 for tnt
//            String freqStr = (mGetFreq) ? gs.extract("<freq>") : null;
//            int freq = (freqStr == null) ? 1 : Integer.valueOf(freqStr);

            // -- limiting the input to certain tags / forms --
           if ( !limitGsTag.matcher(gsTag.toString()).matches() ||
                 (mLimitSlot != -1 && !gsTag.slotValueIn(mLimitSlot, mLimitValues)) ||  
                 (limitForm  != null && !limitForm.matcher(form).matches() ) ) {
                aOverview.report(form);
                //System.out.println("skipping " + form);
                continue;
            }

            // --- Get tc info ---
            Fplus tcFplus = toCheck.getFPlus();
            //System.out.println("checkFileM: " + tcFplus);
            
            // --- Do checking ---
            Triple<Boolean,Boolean,Boolean> result = mAllInfo.recordLt(freq, gsFplus.getLts().lt(), tcFplus.getLts() );  // todo find a better solution that returning a tripple

            if (mBreakBySlot != -1) {
                getBInfo(gsTag).recordLt(freq, gsFplus.getLts().lt(), tcFplus.getLts() );                   
            }

            // --- overview files ---
            aOverview.report(gsFplus, tcFplus.getLts(), result.mFirst, result.mSecond, result.mThird);
        }
    }

    
      
    void error() {

        System.err.println("Error!");
    }

    void printResults(XFile aLogFile, boolean aOmitHeader, XFile aOutFileBase) throws IOException {
        openOut(aLogFile);

        if (!aOmitHeader)
            mOut.printf("%-18s: %5s   %4s         %5s  %5s  %4s  %s\n", "", "T R", "Ambi", "L R", "Ambi", "Ric", "Tokens");
         
        reportingLine(aOutFileBase.file().getName(), mAllInfo);

        // @todo tables
        if (mBreakBySlot != -1) {
            for (Map.Entry<Character, Info> e : mBreakupInfo.entrySet()) {
                reportingLine("   " + String.valueOf(e.getKey()), e.getValue());
            }
        }            

        mOut.close();
    }

    private void reportingLine(String aName, Info aInfo) {
        mOut.printf("%-18s: %5.1f%%  %4.1f         %5.1f%%  %4.1f  %4.1f  %d\n",
            aName,    
            rev(aInfo.tagR()),   aInfo.tagAmbi(),
            rev(aInfo.lemmaR()), aInfo.lemmaAmbi(), rev(aInfo.lemmaRIC()),
            aInfo.mWordNr);
    }
    
    private double rev(double aNr) {
        return 100.0*(1.0-aNr);
    }
}


// =============================================================================
// Info
// =============================================================================

class Info {
    final Eq<Lt> lemmaEq;
    final Eq<Lt> tagEq;
    final Eq<Lt> ltEq;

    
    protected int mWordNr = 0;
    protected int mPunctNr = 0;

    protected int mOKLtNr = 0;
    protected int mOKLemmaNrIC = 0;
    protected int mOKLemmaNr = 0;
    protected int mOKTagNr = 0;

    protected int mGsTotalLemmas;
    protected int mGsTotalTags;

    protected int mTotalLts;
    protected int mTotalTags;
    protected int mTotalLemmas;

//    protected int mTagLess;
//    protected int mTagMore;

    @Deprecated /* use the code in util instead */
    public static <T> boolean contains(final Collection<T> aCol, final T aElement, final Eq<T> aEqualizer) {
        for (T x : aCol) {
            if (aEqualizer.equals(x, aElement)) return true;
        }
        
        return false;
    }

    public Info(Eq<Lt> ltEq, Eq<Lt> lemmaEq, Eq<Lt> tagEq) {
        this.ltEq = ltEq;
        this.lemmaEq = lemmaEq;
        this.tagEq = tagEq;
    }

    
    public Triple<Boolean,Boolean,Boolean> recordLt(int aFreq, Lt aGsLt, Lts aTcLts) {
        if (aGsLt.tag().isPunct()) mPunctNr += aFreq;
        mWordNr += aFreq;
        Triple<Boolean,Boolean,Boolean> result = new Triple<Boolean, Boolean, Boolean>(false, false,false);
        
        mTotalLts     += aFreq * aTcLts.size();
        mTotalTags    += aFreq * aTcLts.tags().size();
        mTotalLemmas  += aFreq * aTcLts.lemmas().size();
        
        if( contains( aTcLts.col(), aGsLt, ltEq) ) {
            result.mFirst = true;
            mOKLtNr += aFreq;
        }
        if ( contains( aTcLts.col(), aGsLt, lemmaEq) ) {
            result.mSecond = true;
            mOKLemmaNr   += aFreq; 
            mOKLemmaNrIC += aFreq;
        } 
//        else if ( contains( aTcLts, aGsLt, lemmaIcEq) ) {
//            mOKLemmaNrIC += aFreq;
//        }
        if ( contains( aTcLts.col(), aGsLt, tagEq) ) {
            result.mThird = true;
            mOKTagNr += aFreq;
            
        }
        return result;
    }

    public double tagR()      { return ((double)mOKTagNr) / ((double)mWordNr); }
    public double tagP()      { return ((double)mOKTagNr) / ((double)mTotalTags); }
    public double tagAmbi()   { return ((double)mTotalTags)   / ((double)mWordNr);}
                    
    public double lemmaR()    { return ((double)mOKLemmaNr)   / ((double)mWordNr);}
    public double lemmaRIC()  { return ((double)mOKLemmaNrIC) / ((double)mWordNr);}
    public double lemmaAmbi() { return ((double)mTotalLemmas) / ((double)mWordNr);}

    public String toString() {
        return String.format("Words: %d", mWordNr);
        
    }

}    

