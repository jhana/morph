
package morph.eval;

import java.util.*;
import java.io.*;
import util.err.Err;

/**
 *
 * All _to's_ in ranges are exclusive.
 *
 * @todo insert percentage column based on other column (colums, all = prev col/all)
 * @todo break into several tables (based on width)
 * @todo Separate data and view! 
 * @todo move to morph.util
 * @author  Jiri Hana
 */
public class Table {
    enum FloatFormat {std, percents, promiles};

    // --- Factory (default) options ---
    static FloatFormat sFloatFormat;
    static boolean sLatex;
    static boolean sMarkBest;
    static boolean sMarkBetter;
    
    static String sZeroZeroCell = "";
    static String sHeaderFormat = "%s";
    static String sFirstColumnFormat = "%s";
    static String sCellFormat = "%s";
    static String sMarkedCellFormat = "*%s";
    static String sMarked2CellFormat = "!%s";
    static String sColumnSeparator = "";
    
    static String sNullString;

    // --- Options ---
    FloatFormat mFloatFormat;
    boolean mLatex;
    boolean mMarkBest;
    boolean mMarkBetter;

    // --- Data ---
    String mCaption;
    List<List<Cell>> mLines;

   

    public Table() {
    }
    
    public Table(String aCaption) {
        mCaption = aCaption;
    }

    /** Creates a new instance of Table */
    public Table(String aCaption, int aL, int aC) {
        this(aCaption);
        init(aL, aC);
    }

    /** Creates a new instance of Table */
    public Table(int aL, int aC) {
        init(aL, aC);
    }


    protected void init(List<?> aHeader, List<?> aFirstColumn) {
        init(aFirstColumn.size() + 1, aHeader.size() + 1);
        //report("H: " + aHeader);
        setLine(0, 1, aHeader);
        //System.out.println("fc: " + aFirstColumn);
        setColumn(0, 1, aFirstColumn);
    }
    
    protected void init(String aHeader, List<?> aFirstColumn) {
        init(split(aHeader), aFirstColumn);
    }
    
    protected void init(int aL, int aC) {
        mLines = new ArrayList<List<Cell>>(aL);
        
        for (int l = 0; l < aL; l++) {
            List<Cell> line = new ArrayList<Cell>(aC);
            mLines.add(line);
            for (int c = 0; c < aC; c++) {
                line.add(new Cell(null));
            }
        }
    }
    
    
    final public int nrOfLines() {
        return mLines.size();
    }

    final public int nrOfCols() {
        return mLines.get(0).size();
    }
    
    
// -----------------------------------------------------------------------------
// Edit data 
// -----------------------------------------------------------------------------

    public void setCell(int aL, int aC, Object aObj) {
        getCell(aL,aC).mData = aObj;
    }
    
    public void setCellNaN(int aL, int aC, Double aObj) {
        setCell(aL, aC, aObj, Double.isNaN(aObj));
    }

    public void setCell(int aL, int aC, Object aObj, boolean aTestForNull) {
        getCell(aL,aC).mData = (aTestForNull) ? null : aObj;
    }

    public void setLine(int aL, int aStartC, Object ... aObjs) {
        setLine(aL, aStartC, Arrays.asList(aObjs));
    }
    
    public void setLine(int aL, int aStartC, List<?> aObjs) {
        List<Cell> line = mLines.get(aL);
        for (int c = 0; c < aObjs.size(); c++)
            line.get(aStartC+c).mData = aObjs.get(c);
    }

    public void setColumn(int aC, int aStartL, List<?> aObjs) {
        for (int l = 0; l < aObjs.size(); l++)
            setCell(aStartL+l, aC, aObjs.get(l));
//        for (int l = 0; l < aObjs.size(); l++)
//            System.out.printf("sc - Cell %d:%d %s\n", aStartL+l, aC, getCell(aStartL+l,aC).mData, aObjs.get(l) );
    }

    /**
     * Deletes a line.
     * @param aIdx index of the line to delete
     */
    public void deleteLines(final int aIdx) {
        mLines.remove(aIdx);
    }
    
    /**
     * Deletes lines between aFrom (inclusive) and aTo (exclusive). Nothing is deleted if aTo &lt;= aFrom.
     * @param aFrom inclusive index
     * @param aTo  exclusive index
     */
    public void deleteLines(final int aFrom, final int aTo) {
        for (int l = aFrom; l < aTo; l++)  {
            mLines.remove(l);
        }
    }

    /**
     * Inserts a column at the specified index (the current column is shifted right)
     */
    public void insertColumn(int aIdx) {
        for (int l = 0; l < nrOfLines(); l++) 
            mLines.get(l).add(aIdx, new Cell(null));
    }
    
    /**
     * Inserts a column at the specified index (the current column is shifted right)
     */
    public void insertColumn(int aIdx, List<Object> aValues, CellRenderer aRenderer) {
        assert aValues.size() == nrOfLines();
        for (int l = 0; l < nrOfLines(); l++) 
            mLines.get(l).add(aIdx, new Cell(aValues.get(l), aRenderer));
    }
    


// -----------------------------------------------------------------------------
// Formats
// -----------------------------------------------------------------------------
 
    public void setSideFormats(String aZeroZeroCell, String aHeaderFormat, String aFirstColumnFormat) {
        getCell(0,0).mRenderer = new FormatCellRenderer(aZeroZeroCell);
        
        FormatCellRenderer hr = new FormatCellRenderer(aHeaderFormat);
        for (int c=1; c < nrOfCols(); c++)
            setCellRenderer(0,c,hr);
            
        FormatCellRenderer fcr = new FormatCellRenderer(aFirstColumnFormat);
        for (int l=1; l < nrOfLines(); l++)     
            setCellRenderer(l,0,fcr);
    }

    public void setCellRenderer(int aL, int aC, CellRenderer aRenderer)  {
        //System.out.printf("Cell %d:%d %s\n", aL, aC, getCell(aL,aC).mData);
        getCell(aL,aC).mRenderer = aRenderer;
    }

    public void setCellRenderers(int aFromL, int aFromC, int aToL, int aToC, CellRenderer aRenderer) {
        for (int l = aFromL; l < aToL; l++) 
            for (int c = aFromC; c < aToC; c++)
                setCellRenderer(l, c, aRenderer);
    }
    
// -----------------------------------------------------------------------------
// Header functions
// -----------------------------------------------------------------------------
    
   
    public void insertMaxCol(int aC, int aFromL, int aFromC, int aToL, int aToC, boolean aLabels) {
        assert idxRange(aFromL, aFromC, aToL, aToC) : aMsg(aFromL, aFromC, aToL, aToC);
        assert colRange(aC) : aMsgC(aC);
        
        int aLabelC = aC;  // Maybe not used
        int aValueC = (aLabels) ? aC + 1 : aC;            

        // --- initialize vectors of values ---
        List valCol   = new ArrayList(nrOfLines());
        List labelCol = new ArrayList(nrOfLines());

        for (int l = 0; l < nrOfLines(); l++) {
            valCol.add(null);
            labelCol.add(null);
        }

        Cell maxCell = null;
        Cell headerCell = null;
        
        // --- fill appropriate slots ---
        for (int l = aFromL; l < aToL; l++) {
            List<Cell> line = mLines.get(l);
            int maxC = getMax(line, aFromC, aToC);
            maxCell = line.get(maxC);

            valCol.set(l,maxCell.mData);

            if (aLabels) {
                headerCell = getCell(0,maxC);
                labelCol.set(l,headerCell.mData);
            }            
        }    

        // --- insert vectors into the table ---
        insertColumn(aC, valCol, maxCell.mRenderer);        // @todo error - header cannot have %-renderer
        if (aLabels)
            insertColumn(aC, labelCol, headerCell.mRenderer);
    }
    
// -----------------------------------------------------------------------------
// Marking
// -----------------------------------------------------------------------------


    final void setMark(int aL, int aC, boolean aMark) {
        getCell(aL,aC).mMarked = aMark;
    }
    
    final void setMark(int aL, int aC) {
        setMark(aL, aC, true);
    }

    final void clearMark(int aL, int aC) {
        setMark(aL, aC, false);
    }
    


    void clearMarks() {
        clearMarks(0, nrOfLines(), 0, nrOfCols());
    }
    
    void clearMarks(int aFromL, int aFromC, int aToL, int aToC) {
        for (int l = aFromL; l < aToL; l++) 
            for (int c = aFromC; c < aToC; c++)
                clearMark(l,c);
    }


    void invertMarks(int aFromL, int aFromC, int aToL, int aToC) {
        for (int l = aFromL; l < aToL; l++) 
            for (int c = aFromC; c < aToC; c++)
                getCell(l,c).mMarked = !getCell(l,c).mMarked;
    }    
    
    
    void markMax(int aFromL, int aFromC, int aToL, int aToC) {
        for (int l = aFromL; l < aToL; l++) {
            List<Cell> line = mLines.get(l);
            int maxCol = getMax(line, aFromC, aToC);
            markGEThan(line, maxCol, aFromC, aToC);
        }
    }


    public void markGEThan(int aPivottingC, int aFromL, int aFromC, int aToL, int aToC) {
        for (int l = aFromL; l < aToL; l++) 
            markGEThan( mLines.get(l), aPivottingC, aFromC, aToC);
    }




    /**
     * In a line, marks everything greater or equal to the specified cell.
     */
    protected void markGEThan(List<Cell> aLine, int aPivottingC, int aFromC, int aToC) {
        Cell pivot = aLine.get(aPivottingC);
        for (int c = aFromC; c < aToC; c++) {
            Cell cell = aLine.get(c);
            try {
                cell.mMarked = (cell.compareTo(pivot) >= 0);
            }
            catch (ClassCastException e) {
                System.out.printf("Error: [c=%d] pivot=%s, cell=%s\n", c, pivot, cell, aLine);
                throw e;
            }

        }                
    }    

   
    
// -----------------------------------------------------------------------------
// Printing
// -----------------------------------------------------------------------------
    
    public void print(PrintStream aOut) {
        java.util.BitSet allCols = new java.util.BitSet(nrOfCols());
        allCols.set(0, nrOfCols());
        //System.out.printf("Table: %d x %d\n", nrOfLines(), nrOfCols() );
        printSomeCols(aOut, allCols );
    }
    
    
    public void printSomeCols(PrintStream aOut, java.util.BitSet aColBitMap) {       
        if (mCaption != null)
            System.out.println(mCaption);
        
        // --- Print lines ---
        for (int l = 0; l < nrOfLines(); l++) {
            for (int c = 0; c < nrOfCols(); c++) {
                if (aColBitMap.get(c)) {
//                    assert getCell(l,c).mData != null : String.format("[%d:%d]", l, c);
                    aOut.print(getCell(l,c));
                }
            }
            aOut.println();
        }
    }

// ----------------------------------------------------------------------------- 
// Implementation
// ----------------------------------------------------------------------------- 

// ----------------------------------------------------------------------------- 
// Assertion helpers

    protected final boolean lineRange(int aL)         {return aL < nrOfLines();}

    protected final boolean colRange(int aC)          {return aC < nrOfCols();}

    protected final boolean idxRange(int aL, int aC)  {return lineRange(aL) && colRange(aC);}

    protected final boolean idxRange(int aFromL, int aFromC, int aToL, int aToC) {
        return idxRange(aFromL,aFromC) && idxRange(aToL-1,aToC-1);
    }
    
    protected final String aMsgL(int aL) {
        return String.format("col %d, max %d", aL, nrOfLines());
    }

    protected final String aMsgC(int aC) {
        return String.format("col %d, max %d", aC, nrOfCols());
    }

    protected final String  aMsg(int aL, int aC)  {
        return String.format("idx [%d:%d], bounds [%d:%d]", aL, aC, nrOfLines(), nrOfCols());
    }

    protected final String  aMsg(int aFromL, int aFromC, int aToL, int aToC) {
        return String.format("quadrant [%d:%d]:[%d:%d], bounds [%d:%d]", aFromL, aFromC, aToL, aToC, nrOfLines(), nrOfCols());
    }
    

// ----------------------------------------------------------------------------- 
//

    protected final Cell getCell(int aL, int aC) {
        Err.iAssert( aL < nrOfLines() && aC < nrOfCols(), "idx [%d:%d], bounds [%d:%d]", aL, aC, nrOfLines(), nrOfCols());
        return mLines.get(aL).get(aC);
    }
    
    /**
     * In a line, finds a cell with maximal value.
     */
    protected int getMax(List<Cell> aLine, int aFromC, int aToC) {
        Cell maxCell = aLine.get(aFromC);
        int  maxC    = aFromC;

        for (int c = aFromC; c < aToC; c++) {
            if (maxCell.compareTo(aLine.get(c)) < 0 ) {
                maxCell = aLine.get(c);
                maxC = c;
            }
        }
        return maxC;
    }
    
    final List<String> split(String aColonSeparatedString) {
        return Arrays.asList(aColonSeparatedString.split(":"));
    }
    


    
}
    
    
class Cell implements Comparable {
//    static Table.DefaultCellRenderer defCR = new Table.DefaultCellRenderer();
    Object mData = null;
    CellRenderer mRenderer;   
    boolean mMarked;

    Cell(Object aObj, CellRenderer aRenderer) {
        mData = aObj;
        mRenderer = aRenderer;
    }

    Cell(Object aObj) {
        this(aObj,null);
    }

    @Override
    public String toString() {
        if (mRenderer != null)
            return mRenderer.toString(this);
        else if (mData != null)
            return "!" + mData.toString();
        else 
            return "![null]";
    }

    /**
     * Null is smaller than anything else (and equal to itself)
     */
    @Override
    public int compareTo(Object aObj) {
        Cell that = (Cell)aObj;
        if (mData == null && that.mData == null) return 0;
        if (mData == null)                       return -1;
        if (that.mData == null)                  return 1;

        return ((Comparable)mData).compareTo(that.mData);
    }

}




