package morph.eval;

public enum MeasureType {precision, recall, fMeasure};