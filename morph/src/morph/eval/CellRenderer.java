/*
 * CellRenderer.java
 *
 * Created on September 10, 2004, 4:21 PM
 */

package morph.eval;

/**
 *
 * @author  Jiri
 */
public interface CellRenderer {
    public String toString(Cell aCell);
}


abstract class CellRendererAdapter implements CellRenderer {
    protected String mark(Cell aCell) { return (aCell.mMarked) ? "*" : " ";}

    public String toString(Cell aCell) {
        return ( (aCell.mMarked) ? "*" : " ") + aCell.toString();         
    }
}


class DefaultCellRenderer extends CellRendererAdapter {
//        private static CellRenderer mSingleton = null;
//
//        public static CellRenderer defCellRenderer() {
//            if (mSingleton == null)
//                mSingleton = new DefaultCellRenderer();
//            return mSingleton;
//        }                
//        
//        private DefaultCellRenderer() {}

}    


class FormatCellRenderer extends CellRendererAdapter {
    String mFormat;

    FormatCellRenderer(String aFormat) {
        mFormat = aFormat;
    }

    public String toString(Cell aCell) {
        if (aCell.mData == null)    return String.format(mFormat, "-");
        return mark(aCell) + String.format(mFormat, aCell.mData);
    }
}    

class PercentCellRenderer extends CellRendererAdapter {
    enum Type {direct, percent, promile};
    String mFormat;
    boolean mReverse;

    PercentCellRenderer() {
        this(Type.percent,1,5, false);
    }

    PercentCellRenderer(boolean aReverse) {
        this(Type.percent,1,5, aReverse);
    }

    PercentCellRenderer(Type aType, int aDecPlaces, int aTotalWidth, boolean aReverse) {
//            mFormat = 
//            if (aDecPlaces > 0)
//                switch (aType) {
//                    case direct:            
//                    case percent: mFormat = String.format("%%%d.%df", 4+aDecPlaces, aDecPlaces);
//                    case promile: mFormat = String.format("%%%d.%df", +aDecPlaces, aDecPlaces);          
//            else
//                switch (aType) {
//                    case direct:            
//                    case percent: mFormat = String.format("%%%dd", 3);
//                    case promile: mFormat = String.format("%%%dd", 4);          
        mReverse = aReverse;
    }

    public String toString(Cell aCell) {
        if (aCell.mData == null)  
            return "       -";      //@todo
        else if (aCell.mData instanceof Double) {
            double perc = (Double) aCell.mData;
            if (mReverse) perc = 1.0 - perc;
            return String.format(" %6.1f", perc*100.0) + mark(aCell);
        }
        else
            return "Error " + aCell.mData.getClass() + " " + aCell.mData;

    }
}    


