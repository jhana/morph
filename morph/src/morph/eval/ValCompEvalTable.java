package morph.eval;

import util.*;
import java.util.*;
import java.io.*;
import util.col.Counter;

/**
 * Compares tagges on values of a single slot
 */
class ValCompEvalTable extends EvalTable {
    EvalInfo[] mInfos;
    int mSlot;
    int mColOffset;

    List<Character> mValues;
    
    public ValCompEvalTable(MeasureType aType, int aSlot, EvalInfo[] aInfos) {
        super(caption(aType));
        mInfos = aInfos;
        mSlot = aSlot;
        mValues = mInfos[0].mGsInfo.valuesList(mSlot);
        mColOffset = 1;   // 1st column
                
        init(headers(mInfos), mValues);
        setSideFormats("    ", "%7s", "%3s");
        setCellRenderers(1, 1, nrOfLines(), nrOfCols(), 
            new PercentCellRenderer(PercentCellRenderer.Type.percent, 1,5, true) );

        setCells(aType);
    }   
    
    void insertMaxCol() {
        insertMaxCol(1,mColOffset,1,nrOfLines(),nrOfCols(),true);
        mColOffset += 2;
    }
    
    void insertXCol() {
        CellRenderer r = new FormatCellRenderer("%5d");
        List col   = new ArrayList(nrOfLines());
        Counter<Character> valCounter = mInfos[0].mGsInfo.mSlotCounters.get(mSlot);

        col.add("X");  // header @todo add text (but renderer!)
        for (int val = 0; val < mValues.size(); val++) 
            col.add( valCounter.frequency(mValues.get(val)) );

        insertColumn(mColOffset, col, r); 
        getCell(0,mColOffset).mRenderer = new FormatCellRenderer("%5s");
        mColOffset ++;
    }
    
    static String caption(MeasureType aType) {
        switch (aType) {
            case recall:    return "Recall";
            case precision: return "Precision";
            case fMeasure:  return "F-Measure";
        }
        return null;
    }

    void setCells(MeasureType aType) {
        for (int val = 0; val < mValues.size(); val++) {
            for (int tgr = 0; tgr < mInfos.length; tgr++) {
                setCell(aType, val, tgr);
            }
        }
    }    

    void setCell(MeasureType aType, int aVal, int aTgr) {
        if (  mInfos[aTgr].isSlotConsidered(mSlot) ) {
           setCell(aVal+1, aTgr+1, mInfos[aTgr].detMeasure(aType, mValues.get(aVal))); 
        }
    }    
    
    
    void printConsideredCols(PrintStream aOut) {
        java.util.BitSet bitmap = new java.util.BitSet(mInfos.length+3);  // 1st coll, 2 max cols  @todo !!!!
        for(int i = 0; i < mColOffset; i++) 
            bitmap.set(i);
        for(int i = 0; i < mInfos.length; i++) 
            bitmap.set(i+mColOffset, mInfos[i].mConsideredSlots.get(mSlot));
        printSomeCols(aOut, bitmap);
    }
    
}

