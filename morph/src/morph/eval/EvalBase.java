
package morph.eval;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import morph.util.MUtilityBase;
import util.io.IO;
import util.io.XFile;

/**
 *
 * @author Administrator
 */
public class EvalBase extends MUtilityBase {
    // --- Output Options ---

    /**
     * Printer where the result will be directed 
     * @see #main(String[])
     */
    protected java.io.PrintStream mOut;

    
//        // --- output options ---
//        File outDir   = getArgs().getDir("output.dir", maFile.file().getParentFile());
//        XFile logFile = getArgs().getFile("output.log", outDir, null);
//        boolean omitHeader = getArgs().getBool("output.omitHeader");

    protected void openOut(XFile aLogFile) throws IOException {
        mOut = (aLogFile == null) ? 
            System.out :
            new PrintStream(new FileOutputStream(aLogFile.file(), true), true, aLogFile.enc().getId());
        
    }
    
    protected void closeOut()  {
        if (mOut != System.out) IO.close(mOut);
    }

}
