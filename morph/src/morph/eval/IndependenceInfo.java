package morph.eval;

import java.util.ArrayList;
import java.util.List;
import morph.ts.BoolTag;
import morph.ts.Tag;

/**
 * Counts how many time each pair of taggers both err on the same slot. 
 *
 * @author  Jirka
 */
class IndependenceInfo {
    /**
     * Numbers of errors shared by two taggers (diagonal contains # of errors of a tagger)
     */
    int[][] mCounters;
    
    /**
     * Temporary variable rememberring tags in a single cycle thru all the taggers
     * @see #record(int, Tag)
     */
    Tag[] mTagVector;

    /**
     * Temporary variable containing the GS (ie correct) tag
     */
    Tag mGsTag;

    /**
     * A matrix recording for every pair of taggers the slots they share
     * E.g. if i-th tagger is Pgce, and j-th tagger is Sgne, then they share ge slots and
     * mSharedSlots[i][j] will contain a bool tag corresponding to 'ge'. 
     */
    BoolTag[][] mSharedSlots;
    
    /**
     * Number of taggers.
     */
    int mTaggerNr;
    
    
    /**
     * @param aConsideredSlotsArray slots considered by taggers
     * @param aMask slots the taggers should be compared on (e.g. anything or only gender).
     */
    IndependenceInfo(final List<BoolTag> aConsideredSlotsArray, BoolTag aMask) {
        final List<BoolTag> consideredSlotsArray = new ArrayList<BoolTag>(aConsideredSlotsArray);
        
        mTaggerNr = consideredSlotsArray.size();
        mTagVector = new Tag[mTaggerNr];
        
        // --- initialize mCounters matrix ---
        mCounters = new int[mTaggerNr][];
        for (int i = 0; i < mTaggerNr; i++) 
            mCounters[i] = new int [mTaggerNr];
        
        // --- initialize mSharedSlots matrix ---
        mSharedSlots = new BoolTag[mTaggerNr][];
        for (int i = 0; i < mTaggerNr; i++) {
            mSharedSlots[i] = new BoolTag [mTaggerNr];
            consideredSlotsArray.set(i, consideredSlotsArray.get(i).and(aMask));
            for (int j = 0; j < mTaggerNr; j++) {
                mSharedSlots[i][j] = consideredSlotsArray.get(i).and(consideredSlotsArray.get(j));
            }
        }
    }

    /**
     * Records the current tag of the specified tagger and once all the current tags are recorded,
     * calculates the shared errors. The mGsTag variable must be set before calling this function for
     * the first time in each cycle thru the taggers.
     */
    void record(int aTaggerIdx, Tag aTag) {
        mTagVector[aTaggerIdx] = aTag;
        
        // --- calculate shared errors ---
        if (aTaggerIdx == mTaggerNr - 1) { // all tags were collected
            for (int i = 0; i < mTaggerNr; i++) {
                Tag iTag = mTagVector[i];
                for (int j = 0; j < mTaggerNr; j++) {
                    if (mSharedSlots[i][j].hasAllCleared()) continue;   // taggers have no slots in common
                    boolean iTaggerError = !mGsTag.eq(iTag,          mSharedSlots[i][j]);
                    boolean jTaggerError = !mGsTag.eq(mTagVector[j], mSharedSlots[i][j]);
                    if ( iTaggerError && jTaggerError )     // both wrong
                        mCounters[i][j]++;
                }
            }
        }
    }
}