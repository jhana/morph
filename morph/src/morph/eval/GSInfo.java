package morph.eval;

import java.util.*;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.col.Counter;


/**
 * Records basic statistics about tags in the GS 
 * - # of tokens
 * - # of punctuation tokens
 * - # of particular values for each slot.
 * For every slot, there is a counter counting values occurences.
 * @author  Jiri
 */
class GsInfo extends BaseInfo {
    /**
     * Number of tokens in GS
     */
    protected int mTokenNr = 0;      

    /**
     * Number of punctuations in GS
     */
    protected int mPunctNr = 0;

    protected ArrayList<Counter<Character>> mSlotCounters;           // For entropy

    GsInfo(Tagset aTagset) {
        mSlotCounters = slotCounters(aTagset);
    }

    void record(Tag aTag) {
        mTokenNr++;          // @todo move out
        if (aTag.isPunct()) mPunctNr++;
        for (int slot = 0; slot < aTag.getLen(); slot++) {
            mSlotCounters.get(slot).add(aTag.getSlot(slot));           
        }
    }

    // @todo move out
    double entropy(int aIdx) {
        return mSlotCounters.get(aIdx).entropy();
    }

    /**  
     * Set of all the possible values for certain subtag.
     * Collects the values from both the correct and incorrect taggings
     * @todo - collect just once from GS
     */
    SortedSet<Character> values(int aSlot) {
        return new TreeSet<Character>( mSlotCounters.get(aSlot).keySet() );
    }
    
    List<Character> valuesList(int aSlot) {
        return new ArrayList<Character>( values(aSlot) );
    }
    
    
}
