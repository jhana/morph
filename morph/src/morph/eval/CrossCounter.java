package morph.eval;

import util.col.Counter;

/**
 *
 * @author  Jiri
 */
/**
 * Counting for slots (main slots) relative to some other slot (cross slot).
 *
 */
class CrossCounter {
    int mMainSlot;
    int mCrossSlot;
    
    protected Counter<CharChar> mCounter;
    
    CrossCounter(int aMainSlot, int aCrossSlot) {
        mMainSlot = aMainSlot;
        mCrossSlot = aCrossSlot;
        mCounter = new Counter<CharChar>();
    }
    
    void add(char aMainValue, char aCrossValue) {
        mCounter.add( new CharChar(aMainValue, aCrossValue) );
    }
    
    int frequency(char aMainValue, char aCrossValue) {
        return mCounter.frequency(new CharChar(aMainValue, aCrossValue) ); 
    }
}

class CharChar {
    char mFirst;
    char mSecond;

    CharChar(char aFirst, char aSecond) {
        mFirst = aFirst;
        mSecond = aSecond;
    }
    
    @Override
    public boolean equals(Object aThat) {
        if (!(aThat instanceof CharChar)) return false;
        return mFirst == ((CharChar)aThat).mFirst && mSecond == ((CharChar)aThat).mSecond;
    }
    
    @Override
    public int hashCode() {
        return 17 + mFirst + 256*mSecond;
    }
}
