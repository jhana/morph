//package morph.eval;
//
//import morph.*;
//import util.*;
//import java.io.*;
//import java.util.*;
//import util.PDTSGML;
//
//
//
///**
// *
// */
//public class EvalOld extends  MUtilityBase {
//
//    /**
//     * Printer where the result will be directed 
//     * @see #main(String[])
//     */
//    public static java.io.PrintWriter mOut;
//    
//    /**
//     * Should all subtag statistics be printed? 
//     */
//    protected static boolean mDetailed;
//
//    protected static double mWordNr = 0.0;
//    protected static double mPunctNr = 0.0;
//
//    protected static double mOKLemmaNr = 0.0;
//    protected static double mOKTagNr = 0.0;
//    protected static double mOKLemmaAndTagNr = 0.0;
//
//    /**
//     * Correct subtags
//     */
//    protected static double mOKNr[];
//
//    protected static boolean mBoth = false;
//    
//    /**
//     *
//     */
//    public static void main(String[] args) throws IOException {
//        EvalOld eval = new EvalOld();
//        eval.go(args);
//    }
//    
//    public void go(String[] aArgs) throws java.io.IOException {
//        argumentBasics(aArgs,2);
//
//        XFile toCheckFile = getMArgs().getDirectFile(0);
//        XFile gsFile      = getMArgs().getDirectFile(1); 
//
//        mBasicOutFileName = IO.removeExtension(toCheckFile.getName());
//
//        mOutDir     = getMArgs().getFile("-od", toCheckFile.getParentFile());
//        mDetailed   = getMArgs().isArgPresent("-dr");
//        
//        checkFile(gsFile, toCheckFile);
//    }
//
//   protected String helpString() {
//       return ""; // @todo
//   }
//     
//    /**
//     * Compares the specified GS and tagged file
//     */
//    void checkFile(XFile aGS, XFile aToCheck) throws java.io.IOException {
//        LineNumberReader gs     = IO.createLNFileReader(aGS, "@todoenc");        
//        LineNumberReader tagged = IO.createLNFileReader(aToCheck, "@todoenc");
//
//        PrintWriter overview = IO.createFilePrintWriter(new File(mOutDir, mBasicOutFileName + ".overview.txt"), mOEnc);  //? flush after new line
//        PrintWriter ok       = IO.createFilePrintWriter(new File(mOutDir, mBasicOutFileName + ".ok.txt"), mOEnc);
//        PrintWriter ko       = IO.createFilePrintWriter(new File(mOutDir, mBasicOutFileName + ".ko.txt"), mOEnc);
//        
//        mOKNr = new double[15];
//        for (int i = 0; i< 15; i++) 
//            mOKNr[i] = 0.0;
//        
//        for(;;) {
//            String gsLine = gs.readLine();
//            String taggedLine = tagged.readLine();
//            if (gsLine == null) break;
//
//            if (PDTSGML.isFormLine(gsLine)) {
//                mWordNr++;
//
//                String form    = extract(gsLine, PDTSGML.cForm);
//                String gsLemma = extract(gsLine, PDTSGML.cLemma);
//                String gsTag   = extract(gsLine, PDTSGML.cTag);
//
//                String taggedLemma = extract(taggedLine, mInLemmaTag);
//                String taggedTag   = extract(taggedLine, mInTagTag);
//
//                assert Tag.checkTag(gsTag) :
//                    "\n!!! wrong tag in gs - ln: " + gs.getLineNumber() + " \"" + gsTag + "\"";
//
//                assert Tag.checkTag(taggedTag) :
//                    "\n!!! wrong tag in tagged - ln: " + tagged.getLineNumber() + " \"" + taggedTag+ "\"";
//
//                if (gsTag.equals("Z:-------------")) mPunctNr++;
//                
//                boolean error = false;
//                String overviewLine = form;
//
//                if (gsLemma.equals(taggedLemma) && gsTag.equals(taggedTag))
//                    mOKLemmaAndTagNr++;
//
//                overviewLine += " <l>" + gsLemma;
//                if (gsLemma.equals(taggedLemma)) {
//                    mOKLemmaNr++;
//                }
//                else {
//                    if (mBoth) error = true;
//                    overviewLine += "<MDl>" + taggedLemma;
//                }
//
//                overviewLine += "<t>" + gsTag;
//                if (gsTag.equals(taggedTag)) {
//                    mOKTagNr++;
//                    if (!gsTag.equals("Z:-------------"))
//                        ok.println(overviewLine);
//                }
//                else {
//                    error = true;
//                    overviewLine += "<MDt>" + taggedTag;
//                }
//                
//                if (error) {
//                    overview.print("**");
//                    ko.println(overviewLine);
//                }
//                else 
//                    overview.print("  ");
//                overview.println(overviewLine);
//                    
//                for (int i = 0; i < Tag.cTagLen; i++) {
//                    if (gsTag.charAt(i) == taggedTag.charAt(i)) 
//                        mOKNr[i]++;
//                }
//                    
//                
//            }
//        }
//
//        gs.close();
//        tagged.close();
//        overview.close();
//        ok.close();
//        ko.close();
//        
////        System.out.println("Results (tokens & words):");
//        prinPrec("Tags:                  ", mOKTagNr);
//        prinPrec("Lemmas:                ", mOKLemmaNr);
//        prinPrec("Lemmas & Tag:          ", mOKLemmaAndTagNr);
//        
//        if (mDetailed) {
//            prinPrec("Position  0 (POS):     ", mOKNr[0]);
//            prinPrec("Position  1 (SubPOS):  ", mOKNr[1]);
//            prinPrec("Position  2 (Gender):  ", mOKNr[2]);
//            prinPrec("Position  3 (Number):  ", mOKNr[3]);
//            prinPrec("Position  4 (Case):    ", mOKNr[4]);
//            prinPrec("Position  5 (PossGen): ", mOKNr[5]);
//            prinPrec("Position  6 (PossNr):  ", mOKNr[6]);
//            prinPrec("Position  7 (Person):  ", mOKNr[7]);
//            prinPrec("Position  8 (Tense):   ", mOKNr[8]);
//            prinPrec("Position  9 (Grade):   ", mOKNr[9]);
//            prinPrec("Position 10 (Neg):     ", mOKNr[10]);
//            prinPrec("Position 11 (Voice):   ", mOKNr[11]);
//            prinPrec("Position 12 ()         ", mOKNr[12]);
//            prinPrec("Position 13 ()         ", mOKNr[13]);
//            prinPrec("Position 14 (Variant)  ", mOKNr[14]);
//        }
//    }
//
//    void prinPrec(String aLabel, double aOK) {
//        System.out.println(aLabel + "& " + Util.formatPercentages(aOK/mWordNr) + " &  " + Util.formatPercentages((aOK-mPunctNr)/(mWordNr-mPunctNr)));
//    }
//
//    
//}