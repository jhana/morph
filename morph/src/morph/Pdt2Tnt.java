package morph;

import java.io.*;
import lombok.Cleanup;import lombok.Cleanup;

import util.io.*;
import morph.io.*;
import morph.ts.Tag;
import morph.ts.Tagset;
import morph.util.MUtilityBase;
import util.io.IO;

/**
 * Converts a file in the PDT format to files in the TNT format (tmm or tm).
 */
public class Pdt2Tnt extends MUtilityBase { 
    private enum Mode {
        mmm, mm, m;  // mmm not supported
        boolean m()  {return this == m;}
        boolean mm() {return this == mm;}
    
    };
    
    private Mode mMode;
    private boolean mSentenceBoundaries;
    private Tag cSentenceTag;

    
    /**
     *
     */
    public static void main(String[] args) throws IOException {
        new Pdt2Tnt().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);

        cSentenceTag = Tagset.getDef().fromStringNoCheck("Z#-------------");
        
        mMode = (getArgs().getBool("mmMode")) ?  Mode.mm : Mode.m;
        
        XFile inFile  = getArgs().getDirectFile(0, cCsts);
        XFile outFile = getArgs().getDirectFile(1, cTnt);
        
        mSentenceBoundaries = getArgs().getBool("sBoundaries");
        XFile outFormFile    = getArgs().getFile("formsFile");
        System.out.println("outFormFile: " + outFormFile);
        
        translate(inFile, outFile, outFormFile);
    }


    /**
     * 
     */
    private void translate(XFile aInFile, XFile aOutFile, XFile aOutFormFile) throws java.io.IOException {
        @Cleanup PdtLineReader in = new PdtLineReader(aInFile).setOnlyTokenLines(!mSentenceBoundaries);
        @Cleanup TntWriter out  = new TntWriter(aOutFile);
        PrintWriter outForm = (aOutFormFile == null) ? null : IO.openPrintWriter(aOutFormFile);

        in.setMm( mMode.mm());
        
        boolean prevWasSentence = false;
        
        for(int j=0;;j++) {
            if (!in.readLine()) break;
            
            if (in.lineType().fd()) {
                if (mMode.m())
                    out.writeLine(in.form(), in.tag());
                else
                    out.writeLine(in.form(), in.tags());

                if (outForm != null) outForm.println(in.form());
                prevWasSentence = false;
            }
            else if (mSentenceBoundaries && !prevWasSentence && in.lineType().s()) {
                out.writeLine("Z#", cSentenceTag);
                if (outForm != null) outForm.println("Z#");
                prevWasSentence = true;
            }
                
            if ((j & 16383) == 0) System.out.print('.');  // 16383 = 2^26-1
//          if ((j & (2<<26-1)) == 0) System.out.print('.');  
        }
        if (mSentenceBoundaries && !prevWasSentence) {
            out.writeLine ("Z#", cSentenceTag);
        }

        if (outForm != null) outForm.close();
    }
}