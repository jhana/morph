package morph.shell;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import lombok.Cleanup;
import morph.ma.Morph;
import morph.ma.MorphCompiler;
import morph.ma.module.Module;
import morph.ma.module.WordEntry;
import morph.ma.module.WordModule;
import util.UtilityBase;
import util.col.MultiMap;
import util.err.Log;
import util.io.IO;
import util.io.XFile;

/**
 * Loads words specification and saves it in the format
 * form lemma tags
 *
 * @author j
 */
public class Cut extends UtilityBase {
    public static void main(String[] args) throws IOException {
        new Cut().go(args,1);
    }

    /**
     * options, file file
     * @param aArgs
     * @throws java.io.IOException
     */
    @Override
    public void goE() throws java.io.IOException {
        //-c [n | n,m | n-m] Specify a single column, multiple columns (separated by a comma), or range of columns (separated by a dash).
        //-f [n | n,m | n-m] Specify a single field, multiple fields (separated by a comma), or range of fields (separated by a dash).
        //-dc Specify the field delimiter.
        //-s Suppress (don't print) lines not containing the delimiter.



    }


}

