package morph;

import morph.io.*;
import util.io.*;
import java.io.*;
import java.util.List;
import java.util.regex.Pattern;
import lombok.Cleanup;
import morph.ts.Tag;
import morph.util.MUtilityBase;
import morph.ts.BoolTag;
import morph.ts.Tagset;
import util.err.Err;
import util.err.FormatError;
import util.io.IO;



/**
 * 
 * Postprocesses the output of Czech tagger.
 * See main for more comments. 
 * todo The orig and new tags are from different tagsets   !!!
 * @see #main
 */
public class TranslBack extends MUtilityBase {

    /**
     * Translates back Czech tagset to Russian tagset.
     *
     * @param args arguments to the program. It should contain 3 direct tags - 
     * input file, translMemory file and output file.
     * @throws IOException
     */    
    public static void main(String[] args) throws IOException {
        new TranslBack().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);

        XFile inFileName           = getArgs().getDirectFile(0, cCsts);
        XFile outFileName          = getArgs().getDirectFile(2, cTnt);
        XFile translMemoryFileName = getArgs().getDirectFile(1); 

        List<BoolTag> taggedSlotsList     = getArgs().boolTags("taggedSlots", tagset, tagset.cAllTrue);
        List<String>  tagsetSlotsListStrs = BoolTag.toCodeStrings(taggedSlotsList);
        
        System.out.println("taggedSlotsList: " + tagsetSlotsListStrs);

        if (taggedSlotsList.size() == 1 && taggedSlotsList.get(0).hasAllSet()) {
            translate(inFileName, translMemoryFileName, outFileName);
        }
        else {
            System.out.println("xxxx");
            if (getArgs().getBool("autonames")) {
                // TODO assumes autonames now
                List<XFile>  inFiles = morph.tag.Utils.x(inFileName,  tagsetSlotsListStrs);
                List<XFile> outFiles = morph.tag.Utils.x(outFileName, tagsetSlotsListStrs);
                for (int i = 0; i < taggedSlotsList.size(); i++) {
                    BoolTag filter = taggedSlotsList.get(i);

                    System.out.println("in: " + inFiles.get(i) + " > " + outFiles.get(i));
                    translate(inFiles.get(i), translMemoryFileName, filter, outFiles.get(i));
                }
            }
            else {
                Err.assertE(taggedSlotsList.size() == 1, "Multiple filters require autonames");
                BoolTag filter = taggedSlotsList.get(0);
                translate(inFileName, translMemoryFileName, filter, outFileName);
                
            }
        }
    }

    /** 
     * Translates back Czech tagset to Russian tagset.
     *
     * @param aInFile
     * @param aTranslMemoryFile
     * @param aOutFile
     * @throws IOException
     * 
     * todo The orig and new tags are from different tagsets   !!!
     */
    void translate(XFile aInFile, XFile aTranslMemoryFile, XFile aOutFile) throws java.io.IOException {
        @Cleanup TntReader r = TntReader.open(aInFile);
        @Cleanup TntWriter w = new TntWriter(aOutFile);
        @Cleanup LineNumberReader memory = IO.openLineReader(aTranslMemoryFile);

        for(;;) {
            if (!r.readLine()) break;
            String memLine = memory.readLine();
            if (memLine == null) throw new FormatError("Memory too short m:[%d] x r:[%d]\n%s", memory.getLineNumber(), r.getLineNumber(), r.line() );

            Tag tag = r.tag();
            int translTagIdx = memLine.indexOf("<new>" + tag);
            if (translTagIdx != -1) {
                int origTagStart = memLine.indexOf("<orig>", translTagIdx);  // todo search next "<" or endof line
                Err.iAssert(origTagStart != -1, "Missing orig tag");
                origTagStart += 6; // 6 * "<orig>".length()
                int origTagEnd   = memLine.indexOf("<", origTagStart);
                String origTagStr = origTagEnd == -1 ? memLine.substring(origTagStart) : memLine.substring(origTagStart, origTagEnd);
                tag = Tagset.getDef().fromString( origTagStr );
            }

            w.writeLine(r.form(), tag);
        }
    }

    void translate(XFile aInFile, XFile aTranslMemoryFile, BoolTag aSlotFilter, XFile aOutFile) throws java.io.IOException {
        @Cleanup TntReader r = TntReader.open(aInFile);
        @Cleanup TntWriter w = new TntWriter(aOutFile);
        @Cleanup LineNumberReader memory = IO.openLineReader(aTranslMemoryFile);

        for(;;) {
            if (!r.readLine()) break;
            String memLine = memory.readLine();
            if (memLine == null) throw new FormatError("Memory too short m:[%d] x r:[%d]\n%s", memory.getLineNumber(), r.getLineNumber(), r.line() );

            Tag newTag = findMatchingTag(r.tag(), memLine, aSlotFilter);
            w.writeLine(r.form(), newTag);
        }
    }

    final Pattern newPattern = Pattern.compile("\\<new\\>");
    final Pattern origPattern = Pattern.compile("\\<orig\\>");

    private Tag findMatchingTag(Tag aTtTag, String memLine, BoolTag aSlotFilter) {
        String[] translStrs = newPattern.split(memLine, 0);
        if (translStrs.length == 1) return aTtTag;
        
        for (String translStr : util.col.Cols.subList(translStrs, 1)) {
            String[] tags = origPattern.split(translStr, 0);
            //System.out.println(Arrays.asList(tags) + " << " + translStr);

            Tag ttTag = Tagset.getDef().fromString(tags[0]);
            System.out.print("origTag " + ttTag);
            if (aTtTag.eq(ttTag, aSlotFilter)) {
                System.out.println(" >> " + tags[1]);
                return Tagset.getDef().fromString(tags[1]).filterTag(aSlotFilter);
            }
            System.out.println();
        }

        return aTtTag;
    }

}