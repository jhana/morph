//package morph;
//
//import util.*;
//import morph.readers.*;
//import java.io.*;
//import java.util.*;
//import java.util.regex.*;
//
//
///**
// * @todo automatically obtain cp from endings of files from a directory
// * @todo add option for some sophisticated case processing (removing cased entries
// *    that do not have support in MA) If that changes anything in results.
// */ 
//public final class TntBackupLex extends UtilityBase {
//    private final static Pattern whiteSpaceSep = Pattern.compile("\\s+");
//
//    MultiMap<String,Tag> mLexicon;
//    
//    /**
//     *
//     */
//    public static void main(String[] args) throws IOException {
//        new TntBackupLex().go(args);
//    }
//    
//    public void go(String[] aArgs) throws java.io.IOException {
//        argumentBasics(aArgs,2);
//        File maFN      = getMArgs().getDirectFile(0);
//        File outLexFNB = getMArgs().getDirectFile(1);
//
//        String[] codeStringArray = getMArgs().codeStringArray("-cp", "all");
//    
//        reportA(codeStringArray);
//
//        for (String codeString : codeStringArray) {
//            report("Filter: %s", codeString);
//            BoolTag consideredSlots = new BoolTag(codeString);
//            File outLexFN     = IO.addExtension(outLexFNB,     codeString + ".lex");      // @todo don't add "all" if no -cp was specified
//            
//            mLexicon = new MultiMap<String,Tag>();
//
//            report("  Loading the MA-ed file");
//            readLexicon(maFN, consideredSlots);
//            report("  Processing data and saving the result");
//            writeLexicon(outLexFN);
//        }
//    }
//
//    protected String helpString() {
//        return "java LexForTNT [options] {maFile} {lexFileBase}\n\n" +
//            " the resulting lexicon file has the name {lexFileBase}.all.lex (unless -cp is used)\n\n" +
//            "-cp {code-strings}\n" +
//            "   filters the specified subtags, code-strings are separated by ':' or ';'\n" +
//            "   codes by position: PSgncGNptd+vV; 'all' is an abbreviation for listing all codes\n" +
//            "   the resulting lexicon file has the name: {lexFileBase}.{code-string}.lex\n" +
//            "   E.g.: java LexForTNT -cpn all:P:Pgc file.ma lexicon\n" +
//            "      produces lexicon.all.lex, lexicon.P.lex and lexicon.Pgc.lex files\n" +
//            "   E.g.: java LexForTNT -cpn all file.ma lexicon\n" +
//            "      is equivalent to java LexForTNT file.ma lexicon\n\n" +
//            "-ie,-oe";
//    }
//
//    /**
//     */
//    private void readLexicon(File aMAFile, BoolTag aConsideredSlots) throws java.io.IOException {
//        PdtLineReader maR = new PdtLineReader(aMAFile, mIEnc);
//
//        for(;;) {
//            if (!maR.readLine()) break;
//            List<Tag> tags = Tags.fromStrings(maR.extractMMTagsS(), aConsideredSlots);
//            mLexicon.addAll(maR.form(), tags);
//        }
//
//        maR.close();
//    }
//    
//
//    public void writeLexicon(File aOutFile) throws java.io.IOException {
//        PrintWriter out = IO.createFilePrintWriter(aOutFile, mOEnc);  //@todo
//    
//        out.println("%% lexicon created by LexForTNT");
//
//        SortedSet<String> sortedForms = new TreeSet<String>(mLexicon.keySet());
//        
//        for(String form : sortedForms) {
//            Set<Tag> maTags = mLexicon.get(form);
//            
//            int maTagsTotFreq = maTags.size();
//          
//            out.printf("%s   %d   ", form, maTagsTotFreq);
//
//            if (maTags != null)
//                for (Tag maTag : maTags)
//                    out.printf(maTag + " 1  ");
//            
//            out.println();
//        }
//        out.close();
//   }
//}
//
