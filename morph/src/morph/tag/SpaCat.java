
package morph.tag;

import java.io.IOException;
import morph.io.CorpusWriter;
import morph.util.MArgs;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.str.Strings;

/**
 *
 * @author Jirka Hana
 */
public class SpaCat implements Russification.TranslModule {

    @Override
    public void init(MArgs aArgs) throws Exception {
    }

    @Override
    public void translate(RussificationCtx aCtx, CorpusWriter aW) throws IOException {
        String form = aCtx.curForm();
        Tag tag = aCtx.curTag();
        String newForm = form;
        Tag newTag = tag;

        // translates la/el -> l' / _ V
        if (tag.startsWith("DA")) {
            if (tag.eq("DAMS-0-0---") || tag.eq("DAFS-0-0---")) {
                // check if next word starts with vowel
                String nextForm = aCtx.getNextForm(0);
                if (nextForm != null && Strings.charIn(nextForm, 0, "todo")) {
                    newForm = (form.charAt(0) == 'L') ? "L'" : "l'";
                    // TODO Caps.capitalizeBy("l'", form); would probably not work, because it is mixed
                    newTag = Tagset.getDef().fromString("DACS-0-0---");
                }
            }
        }

        if (newForm != null) {
            aW.writeLine(newForm, newTag);
        }
    }

}
