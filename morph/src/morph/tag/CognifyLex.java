
package morph.tag;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import morph.util.MUtilityBase;
import morph.util.tnt.TntLex;
import morph.util.tnt.TntLexEntry;
import morph.util.tnt.TntLexIO;
import morph.ts.Tag;
import util.col.Cols;
import util.io.IO;
import util.io.LineReader;
import util.str.Strings;

/**
 *
 * @author Administrator
 */
public class CognifyLex extends MUtilityBase {
    // cognates: catalan-word -> [Spanish-word + distance]
    
    // spanish 
    TntLex srcLex = new TntLex();
    
    MultiCounter<String,String> cognates = new MultiCounter<String,String>();;
    
    
    public static void main(String[] args) throws IOException {
         new CognifyLex().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs, 4);
        
        // load spanish tnt lex file
        srcLex = TntLexIO.load( getArgs().getDirectFile(0) );
        
        // load cognates
        loadCognates();

        
        // process ma lex file (tnt format)
        LineReader r = IO.openLineReader(getArgs().getDirectFile(1));
        PrintWriter w = IO.openPrintWriter(getArgs().getDirectFile(3));
        try {
            processMaFile(r, w);
        }
        finally {
            IO.close(r,w);
        }
    }

    
    private void loadCognates() throws IOException {
        LineReader r = IO.openLineReader(getArgs().getDirectFile(2));
        r.configureSplitting(Strings.cWhitespacePattern, 3, "Format: src-word target-word score");

        try {
            for (;;) {
                String[] strs = r.readSplitNonEmptyLine();
                if (strs == null) break;

                String target = strs[0];
                String src = strs[1];
                double score = Double.valueOf(strs[2]);
                //System.out.println("Loading " + target + " " + src + " " + score);
                cognates.put(target, src, score);
            }            
        }
        finally {
            IO.close(r);
        }
    }

    double cogThreshold = 0.2; // 
    boolean inclDebugInfo = true;
    
    private void processMaFile(LineReader aR, PrintWriter aW) throws IOException {
// @todo
//        cogThreshold = getArgs().getDouble("cogThreshold"); 
//        inclDebugInfo = getArgs().getBool("inclDebugInfo");
        
        for (;;) {
            TntLexEntry maEntry = TntLexIO.readEntry(aR);
            if (maEntry == null) break;

            //System.out.println("Processing: " + maEntry);

            
            TntLexEntry mergedEntry = createNewLexEntry(maEntry, aW);
            TntLexIO.writeEntry(aW, mergedEntry);
        }    
    }    

    
    protected TntLexEntry createNewLexEntry(TntLexEntry aMaEntry, PrintWriter aW) {
            //System.out.println("form: " + aMaEntry.getForm());
                    
            Map<String,Double> cogs = cognates.get(aMaEntry.getForm());
//            if (cogs != null) {
//                System.out.println("cogs: " + Cols.toStringNl(cogs.entrySet()) );
//            }

            if (cogs==null) {
                cogs = cognates.get(aMaEntry.getForm().toLowerCase());
            }

//            if (cogs != null) {
//                System.out.println("lc cogs: " + Cols.toStringNl(cogs.entrySet()) );
//            }

            if (cogs == null) {
                // try direct look up
                TntLexEntry srcEntry = srcLex.getEntry(aMaEntry.getForm());   // spanish entry

                if (srcEntry == null) return aMaEntry;
                
                cogs = Collections.singletonMap(aMaEntry.getForm(), 0.0);
            }
            
            
            // -- get cognated entries from the spanish lexicon ---
            List<TntLexEntry> srcLexEntries = new ArrayList<TntLexEntry>();
            List<Double>      srcLexCogScores = new ArrayList<Double>();

            for (Map.Entry<String,Double> ce : cogs.entrySet()) {
                double cogScore = ce.getValue();
                if (cogScore > cogThreshold) continue;

                String srcForm = ce.getKey();

                TntLexEntry srcEntry = srcLex.getEntry(srcForm);   // spanish entry

                if (srcEntry != null) {
                    srcLexEntries.add(srcEntry);
                    srcLexCogScores.add(cogScore);

                    if (inclDebugInfo) {
                        aW.print("%% ");
                        aW.print(srcForm);
                        aW.print(" : ");
                        aW.print(cogScore);
                        aW.print("; ");
                        TntLexIO.writeEntry(aW, srcEntry);
                    }                        
                }
//                else {
//                    srcEntry = srcLex.getEntry(srcForm.toLowerCase());   // spanish entry
//
//                    if (srcEntry != null) {
//                        srcLexEntries.add(srcEntry);
//                        srcLexCogScores.add(ce.getValue());
//
//                        if (inclDebugInfo) {
//                            aW.print("%% ");
//                            aW.print(srcForm);
//                            aW.print(" : ");
//                            aW.print(cogScore);
//                            aW.print("; ");
//                            TntLexIO.writeEntry(aW, srcEntry);
//                        }
//                    }
//                }
            }
                
                

                return createNewLexEntry(aMaEntry, srcLexEntries, srcLexCogScores);
    }
    
    /** 
     * 
     * TODO weigh by cognate closeness or use only the best one
     * Override to change the merging alg.
     * @param maEntry
     * @param czeLexEntries
     * @param czeLexCogScores
     * @return
     */
    protected TntLexEntry createNewLexEntry(TntLexEntry maEntry, List<TntLexEntry> czeLexEntries, List<Double> czeLexCogScores) {
        if (czeLexEntries.isEmpty()) return maEntry;
        
//        int czeTotal = 0;
//        for (TntLexEntry e : czeLexEntries) {
//            czeTotal += e.getTotalFreq();
//        }

                
        TntLexEntry mergedEntry = new TntLexEntry();
        mergedEntry.setForm(maEntry.getForm());
        
        for (int i = 0; i < maEntry.getTags().size(); i++) {
           Tag tag = maEntry.getTags().get(i);

           int tagMergedFreq = 0;
           for (TntLexEntry e : czeLexEntries) {
               tagMergedFreq += e.getFreq(tag);
           }
           
           mergedEntry.addTag(tag, tagMergedFreq);
        }

        int maTagFreq =  mergedEntry.getTotalFreq() / maEntry.getTags().size();
        maTagFreq =  Math.max(1, maTagFreq);
        mergedEntry.incFreqs(maTagFreq);
        
        return mergedEntry;
    }
}
