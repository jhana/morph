package morph.tag;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import lombok.Cleanup;import lombok.Cleanup;

import morph.io.Corpora;
import morph.io.CorpusLineReader;
import morph.util.MUtilityBase;
import morph.util.string.BasicEd;
import morph.util.string.EditDistance;
import morph.util.string.VariableEd;
import morph.ts.BoolTag;
import morph.ts.Tag;
import util.Pair;
import util.PairC;
import util.Triple;
import util.col.Cols;
import util.io.IO;
import util.io.XFile;
import util.str.Strings;



/**
 * NOTE this is used for all languages not just spanish and catalan !!!
 *
 *
 * From the original python script (no longer valid):
# This is a cognate detector. It takes lists of nouns, adjectives
# verbs, adverbs, numerals from the Spanish corpus separately
# and compares them with words appeared in the Catalan corpus
# The code is based on the normalized (by the longest word) edit
# distance algorithm + phonological/orthographic alternation across
# Spanish and Catalan.
# The table with the actual characters is in
# /home/afeldman/SpanishPortug/catalan/cognates/dev/
 *
 *
 */
public class CognatesSpaCat extends MUtilityBase {


    static class LT extends PairC<String, Tag> {

        LT(String aLemma, Tag aTag) {
            super(aLemma, aTag);
        }

        String getLemma() {
            return mFirst;
        }

        Tag getTag() {
            return mSecond;
        }
    }

    static class Anal {

        public String toString() {
            return word + " " + Cols.toString(tags);
        }
        String word;
        int freq;
        //final Set<LT> lts = new TreeSet<LT>();       // all lts it occured with
        Set<Tag> tags = new TreeSet<Tag>();

        public Anal(String aWord, Set<Tag> aTags) {
            word = aWord;
            tags = aTags;
            freq = 0;
        }

//        public void add(LT aLt) {
//            lts.add(aLt);
//            freq++;
//        }
//        public void add(String aLemma, Tag aTag) {
//            add(new LT(aLemma, aTag));
//        }
        Set<Tag> getTags() {
            return tags;
        }

        boolean hasPos(char aPos) {
            for (Tag t : tags) {
                if (t.getPOS() == aPos) {
                    return true;
                }
            }
            return false;
        }

        /* returns a set of all POS in the anals */
        Set<Character> getPoss() {
            Set<Character> poss = new HashSet<Character>();
            for (Tag t : tags) {
                poss.add(t.getPOS());
            }
            return poss;
        }
    }

    static class BestStuff {

        Anal catAnal;
        Anal spaAnal;
        String cat;
        String spa;
        double dist = Double.MAX_VALUE;

        public void add(Anal aCatAnal, Anal aSpaAnal, double aDist) {
            if (aDist < dist) {
                catAnal = aCatAnal;
                spaAnal = aSpaAnal;
                cat = aCatAnal.word;
                spa = aSpaAnal.word;
                dist = aDist;
            }
        }

        public void add(String catWord, String spaWord, double aDist) {
            if (aDist < dist) {
                cat = catWord;
                spa = spaWord;
                dist = aDist;
            }
        }

        public Collection<Pair<Anal, Double>> getWinnersA() {
            if (dist < 100) {
                return Arrays.asList(new Pair<Anal, Double>(spaAnal, dist));
            } else {
                return Collections.<Pair<Anal, Double>>emptyList();
            }
        }

        public Collection<PairC<String, Double>> getWinners() {
            if (dist < 100) {
                return Arrays.asList(new PairC<String, Double>(spa, dist));
            } else {
                return Collections.<PairC<String, Double>>emptyList();
            }
        }
    }

    PrintWriter w;
    boolean filterByTag;
    BoolTag tagPattern;
    EditDistance ed;    // algorithm calculating ed
    int coef = 1;

    // pos - list of words
    private final Map<Character, List<Anal>> spaAnalMap = new HashMap<Character, List<Anal>>();
    private List<Anal> catAnals;

    public static void main(String[] args) throws IOException {
        new CognatesSpaCat().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs, 3);

        // --- configuration  ---
        filterByTag = getArgs().getBool("filterByTag.enabled");
        tagPattern = (filterByTag) ? getArgs().boolTag("filterByTag.pattern", tagset, null) : null;

        configureEd();

        readInSpa(getArgs().getDirectFile(1));
        w = IO.openPrintWriter(getArgs().getDirectFile(2));
        //w = new PrintWriter( System.out );

        processCognates();

        w.close();
    }

    private void configureEd() throws IOException {
        XFile costFile = getArgs().getFile("cost.file");
        if (costFile == null) {
            ed = new BasicEd();
        }
        else {
            List<String> lines = IO.readInLines(costFile);

            List<Triple<Character,Character,Double>> ccss = new ArrayList<Triple<Character,Character,Double>>();
            for (String line :  lines) {
                String[] parts = Strings.cWhitespacePattern.split(line);

                double cost = Double.valueOf( parts[2] );

                for (char char1 : parts[0].toCharArray()) {
                    for (char char2 : parts[1].toCharArray()) {
                        if (char1 != char2) {
                            ccss.add(new Triple(char1, char2, cost));
                        }
                    }
                }
            }

            ed = new VariableEd(ccss, true); // @todo TODO configurable symmetry
        }
    }

    private void processCognates() throws IOException {
        catAnals = readInAnals(getArgs().getDirectFile(0));  // can be read in in steps
        for (Anal catAnal : catAnals) {
            BestStuff bestStuff = getCognates(catAnal);
            dumpResult(bestStuff, catAnal);
        }
    }

    private BestStuff getCognates(Anal catAnal) {
//        w.println();
//        w.println("// =============================================");
//        w.println("// cat " + catAnal);

        final String catWord = catAnal.word;
        final BestStuff bestStuff = new BestStuff();
        double minDist = 2.0;

        for (Character catPos : catAnal.getPoss()) {
            final List<Anal> spaAnals = spaAnalMap.get(catPos);
            if (spaAnals == null) continue;

            for (Anal spaAnal : spaAnals) {
                String spaWord = spaAnal.word;
                if (Math.abs(catWord.length() - spaWord.length()) > minDist) continue;
                if (filterByTag && !hasTagIntersection(catAnal,spaAnal) ) continue;

                double dist = ed.distance(spaWord, catWord);
                if (dist < minDist) {
                    minDist = dist;
                    dist = ed.normalize(spaWord, catWord, dist);
                    bestStuff.add(catAnal, spaAnal, dist);
                }
            }
        }


//        for (Character catPos : catAnal.getPoss()) {
//            final List<Anal> spaAnals = spaAnalMap.get(catPos);
//            if (spaAnals == null) {
//                continue;
//            }
//            w.println(Cols.toString(spaAnals, "// spa ", "", "\n", ""));
//
//            if (filterByTag) {
//                filterAnals(spaAnals, catAnal, tagPattern);
//            }
//
//            w.println("// " + (spaAnals.isEmpty() ? "EMPTY" : "OK"));
//
//            for (Anal spaAnal : spaAnals) {
//                String spaWord = spaAnal.word;
//                if (Math.abs(catWord.length() - spaWord.length()) > minDist) {
//                    continue;
//                }
//                double dist = ed.basic(spaWord, catWord);
//                if (dist < minDist) {
//                    dist = ed.normalize(spaWord, catWord, dist);
//                    //bestStuff.add(catWord, spaWord, dist);
//                    bestStuff.add(catAnal, spaAnal, dist);
//                }
//            }
//        }

        return bestStuff;
    }

    private boolean hasTagIntersection(final Anal aA, final Anal aB) {
        return hasIntersection(aA.getTags(), aB.getTags(), tagPattern);
    }

    /**
     * @todo To tag utils
     * @param aATags
     * @param aBTags
     * @param aPattern
     * @return
     */
    private static boolean hasIntersection(final Iterable<Tag> aATags, final Iterable<Tag> aBTags, final BoolTag aPattern) {
        for (Tag tagA : aATags) {
            for (Tag tagB : aBTags) {
                if (tagA.eq(tagB, aPattern)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void filterAnals(final List<Anal> aFiltered, final Anal aFiltering, final BoolTag aPattern) {
        //System.out.println("cat:   " + aFiltering);
        for (Iterator<Anal> it = aFiltered.iterator(); it.hasNext();) {
            Anal filtered = it.next();
//            System.out.println("  spa: " + filtered);
            if (!intersect(filtered, aFiltering, aPattern)) {
                it.remove();
//                System.out.println("--removed");
            }
        }
//        System.out.println("Not removed");
    }

    private boolean intersect(final Anal aA, final Anal aB, final BoolTag aPattern) {
        for (Tag tagA : aA.getTags()) {
            //System.out.println("  tagA: " + tagA);
            for (Tag tagB : aB.getTags()) {
                //System.out.println("   tagB: " + tagB);
                if (tagA.eq(tagB, aPattern)) {
                    return true;
                }
            //System.out.println("    tagB: -");
            }
        }
        //System.out.println("    tagB: -------");
        return false;
    }


    // each word just once (all capitalizations), analyzed
    private List<Anal> readInAnals(XFile aFile) throws IOException {
        String narrow = getArgs().getString("narrow");
        @Cleanup CorpusLineReader r = Corpora.openMm(aFile);

        List<Anal> anals = new ArrayList<Anal>();
        for (int i = 0;; i++) {
            if (!r.readLine()) {
                break;
            }
            String form = r.form();
            //if (!form.equals("autor")) continue;
            if (narrow != null && !form.startsWith(narrow)) {
                continue;
            }
            Set<Tag> tags = r.tags();

            anals.add(new Anal(form, tags));
        //System.out.printf("cat: %s : %s", form, Cols.toString(tags));
        }
        //input_po = open("dev-types.txt")
        // todo read in MA-ed large catalan corpus, keep tags (pos/subpos) and freq - compile into a file

        return anals;
    }

    private void readInSpa(XFile aFile) throws IOException {
        final Map<String, Anal> spaAnals = new TreeMap<String, Anal>();
        final String narrow = getArgs().getString("narrow");

        @Cleanup CorpusLineReader r = Corpora.openMm(aFile);
        for (int i = 0;; i++) {
            if (!r.readLine()) {
                break;
            }
            String form = r.form();
            //if (!form.equals("autor")) continue;
            if (narrow != null && !form.startsWith(narrow)) {
                continue;
            }
            Set<Tag> tags = r.tags();
            //System.out.printf("spa: %s : %s", form, Cols.toString(tags));

            Anal anal = new Anal(form, tags); // todo
            for (Character pos : anal.getPoss()) {
                List<Anal> anals = spaAnalMap.get(pos);
                if (anals == null) {
                    anals = new ArrayList<Anal>();
                    spaAnalMap.put(pos, anals);
                }

                anals.add(anal);
            }
        }
    }

    private void dumpResult(BestStuff bestStuff, Anal catAnal) {
        final Collection<PairC<String, Double>> wss = bestStuff.getWinners();
        if (!wss.isEmpty()) {
            //w.println("// cat " + catAnal);
//            for (Pair<Anal, Double> ws : bestStuff.getWinnersA()) {
//                w.println("// spa " + ws.mFirst);
//            }
            w.print(catAnal.word);
            for (PairC<String, Double> ws : wss) {
                w.print(" " + ws.mFirst + " " + ws.mSecond);
            }
            w.println();
        }
    }

}
        
        