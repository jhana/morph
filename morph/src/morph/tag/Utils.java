
package morph.tag;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import util.io.Files;
import util.io.XFile;

/**
 *
 * @author Jirka Hana
 */
public class Utils {
    public static List<File> x(File aBaseFile, List<String> aPreExts) {
        File dir = aBaseFile.getParentFile();
        String baseName = aBaseFile.getName();

        final List<File> files = new ArrayList<File>();
        for (String preExt : aPreExts) {
            String shortFileName = Files.addBeforeExtension(baseName, preExt);
            files.add( new File(dir, shortFileName) );
        }
        return files;
    }

    public static List<XFile> x(File aBaseFile, XFile.Properties aProps, List<String> aPreExts) {
        File dir = aBaseFile.getParentFile();
        String baseName = aBaseFile.getName();

        final List<XFile> files = new ArrayList<XFile>();
        for (String preExt : aPreExts) {
            String shortFileName = Files.addBeforeExtension(baseName, preExt);
            files.add( new XFile(new File(dir,shortFileName), aProps) );
        }

        return files;
    }

    public static List<XFile> x(XFile aBaseFile, List<String> aPreExts) {
        return x(aBaseFile.getFile(), aBaseFile.getProperties(), aPreExts);
    }
}
