package morph.tag;

import java.io.*;
import java.util.*;
import lombok.Cleanup;
import util.io.*;
import morph.io.*;
import morph.util.*;
import morph.util.tnt.TntLex;
import morph.util.tnt.TntLexEntry;
import util.col.Cols;
import util.err.Err;
import util.err.Log;

/**
 * Collects all tags for each form from an disambiguated corpus (m-corpus).
 * 
 * @todo currently ingores lemmas, frequencies
 * @todo make it possible to specify source of analyses in PDT (MMl, l, ...)
 * @todo capitalization !!!!!!
 * @todo freq cutoff (e.g to prevent typos from getting in)
 * @todo store freq with tags
 */
public class CollectAnal extends MUtilityBase { 
    final TntLex lex = new TntLex();

    /**
     *
     */
    public static void main(String[] args) throws IOException {
        new CollectAnal().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        
        XFile inFile    = getArgs().getDirectFile(0);
        boolean mm      = getArgs().getBool("mm"); // @todo general support for m/mm/m+mm distinction (and/or lt-tag specification)
        XFile outFile   = getArgs().getDirectFile(1);

        collectAnalyses(inFile, mm);
        
        writeResults(outFile);
    }
    
    /**
     * 
     */
    void collectAnalyses(XFile aInFile, final boolean aMm) throws java.io.IOException {
        @Cleanup CorpusLineReader in = Corpora.open(aInFile);

        in.setMm(aMm);
        
        for(int j=0;;j++) {
            if (!in.readLine()) break;

            Err.fAssert(in.form() != null, in.getReader(), "null form");
            //String form = Caps.capitalizeBy(in.form(), in.lemma());

            try {
                TntLexEntry entry = lex.getEntry(in.form());
                if (entry == null) {
                    entry = new TntLexEntry();
                    entry.setForm(in.form());
                    lex.add(entry);
                }

                if (aMm) {
                    entry.addTags(in.tags());
                }
                else {
                    entry.addTag(in.tag());
                }
            }
            catch(Exception e) {
                Err.fErr(in.getReader(), "null: %s / %s / %s", in.form(), in.tag(), in.tags());
            }

            Log.dot(j);
        }
        
        //lex.removeBelowFreq(getArgs().getInt("freqThreshold")); // not needed, since input is compTODO put back one freqs done, but
    }

    
    
    private void writeResults(XFile aLexFile) throws java.io.IOException {
        @Cleanup final PrintWriter outLex  = IO.openPrintWriter(aLexFile);
        
        final Set<String> sortedForms = new TreeSet<String>(lex.getForms());

//        System.out.println("++++++++++++++");

        for( String form : sortedForms ) {
            TntLexEntry e = lex.getEntry(form);
//            System.out.println("form: " + form);
//            System.out.println("    : " + e.getTags());
//
            String tagStr = Cols.toString(e.getTags(), "", "", " ", "");
//            System.out.println("    : " + tagStr);
            outLex.printf("%s\t%s\n", form, tagStr);
        }    
    }    
}
