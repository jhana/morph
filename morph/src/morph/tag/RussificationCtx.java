package morph.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import morph.io.CorpusLineReader;
import morph.ts.Tag;
import util.col.Cols;


/**
 * Contextualized word reader., move reader context out the same way as empty ctx
 *
 * @author Administrator
 */
public class RussificationCtx {
    final private int mPrevCtx;
    final private List<String> mPrevForms;
    final private List<String> mPrevLemmas;
    final private List<Tag> mPrevTags;

    private String mCurForm;
    private String mCurLemma;
    private Tag    mCurTag;

    final private int mNextCtx;
    final private List<String> mNextForms;
    final private List<String> mNextLemmas;
    final private List<Tag>    mNextTags;

    final private CorpusLineReader mR;


    public RussificationCtx(CorpusLineReader aR, int aPrevCtx, int aNextCtx) {
        mR = aR;
        mPrevCtx = aPrevCtx;
        mNextCtx = aNextCtx;

        mPrevForms  = new ArrayList<String>(aPrevCtx);
        mPrevLemmas = new ArrayList<String>(aPrevCtx);
        mPrevTags   = new ArrayList<Tag>(aPrevCtx);

        mNextForms  = new ArrayList<String>(aNextCtx);
        mNextLemmas = new ArrayList<String>(aNextCtx);
        mNextTags   = new ArrayList<Tag>(aNextCtx);
    }

    public String curForm() {
        return mCurForm;
    }

    public String curLemma() {
        return mCurLemma;
    }

    public Tag curTag() {
        return mCurTag;
    }

    public boolean hasPrevForm(int aIdx) {
        return getItem(mPrevForms,aIdx) != null;
    }

    public String getPrevForm(int aIdx) {
        return getItem(mPrevForms,aIdx);
    }

    public boolean prevFormEq(int aIdx, String aStr) {
        String str = getItem(mPrevForms,aIdx);
        return str != null && str.equals(aStr);
    }


    public String getPrevLemma(int aIdx) {
        return getItem(mPrevLemmas,aIdx);
    }

    public Tag getPrevTag(int aIdx) {
        return getItem(mPrevTags,aIdx);
    }


    public boolean hasNextForm(int aIdx) {
        return getItem(mNextForms,aIdx) != null;
    }

    public String getNextForm(int aIdx) {
        return getItem(mNextForms,aIdx);
    }

    public boolean nextFormEq(int aIdx, String aStr) {
        String str = getItem(mNextForms,aIdx);
        return str != null && str.equals(aStr);
    }


    public String getNextLemma(int aIdx) {
        return getItem(mNextLemmas,aIdx);
    }

    public Tag getNextTag(int aIdx) {
        return getItem(mNextTags,aIdx);
    }


// ------------------------------------------------------------------------------
// Special functions
// ------------------------------------------------------------------------------
//    /**
//     * Check: Previous form is . / ! / ?, i.e. probably end of a sentence.
//     * ??? '...'
//     */
//    public boolean prevIsSentencePunct() {
//        String prevForm = getPrevWord(0);
//        return
//            prevForm != null && prevForm.length() == 1 &&  // quick check
//            (prevForm.equals(".") || prevForm.equals("?") || prevForm.equals("!"));
//    }

    /**
     * Simulatets an infinite array, with all items above the size of the 
     * real array filled with nulls.
     * @param <T>
     * @param aList
     * @param aIdx
     * @return
     */
    private <T> T getItem(List<T> aList, int aIdx) {
        return (aIdx < aList.size()) ? aList.get(aIdx) : null;
    }

//    public boolean isNotEmpty() {return !mEmpty;}
//
//    /**
//     * The previous form, i.e. the form preceding the current form.
//     */
//    public String previousForm()  {return mPrevForm;}
//
//    /**
//     * Capitalization of the previous form.
//     */
//    public Cap   previousCap() {return mPrevCap;}
//
//    /**
//     * Morphological analyzis of the previous form.
//     */
//    public Lts previousAnalysis() {return mPrevAnal;}
//
//    /**
//     * The current form.
//     */
//    public String      curForm()  {return mCurForm;}
//
//    /**
//     * Capitalization of the current form.
//     */
//    public Cap curCap()   {return mCurCap;}
//
//    /**
//     * The next form, i.e. the form following the current form.
//     */
//    public String      nextForm() {return mNextForm;}
//
//    /**
//     * Capitalization of the next form.
//     */
//    public Cap nextCap()  {return mNextCap;}
//
//// ------------------------------------------------------------------------------
//// Special functions
//// ------------------------------------------------------------------------------
//    /**
//     * Check: Previous form is . / ! / ?, i.e. probably end of a sentence.
//     * ??? '...'
//     */
//    public boolean prevIsSentencePunct() {
//        return
//            (mPrevForm.length() < 2) &&  // quick check
//            (mPrevForm.length() == 0 || mPrevForm.equals(".") || mPrevForm.equals("?") || mPrevForm.equals("!"));
//    }
//
//    public String toString() {
//        return mPrevForm + " : " + mCurForm + " : " + mNextForm;
//    }



    public void prepare() throws IOException {
        for (int i = 0; i < mPrevCtx; i++) {
            mPrevForms.add(null);
            mPrevLemmas.add(null);
            mPrevTags.add(null);
        }

        for (int i = -1; i < mNextCtx; i++) {
            if (!mR.readLine()) break;

            String form = mR.form();
            String lemma = mR.lemma();
            Tag tag = mR.tag();

            if (i == -1) {
                mCurForm = form;
                mCurLemma = lemma;
                mCurTag = tag;
            }
            else {
                mNextForms.add(form);
                mNextLemmas.add(lemma);
                mNextTags.add(tag);
            }
        }
    }

    public boolean hasNext() {
        return mCurForm != null;
    }
//
//    public boolean next() {
//        // move one level
//
//        if (!mR.nextWord()) return;
//
//    }

    public boolean next() throws IOException {
        if (!mR.readLine()) return false;

        String form = mR.form();
        String lemma = mR.lemma();
        Tag tag = mR.tag();

        if (mPrevForms.size() > 0) {
            // shift one index
            Cols.shiftUp(mPrevForms);
            Cols.shiftUp(mPrevLemmas);
            Cols.shiftUp(mPrevTags);

            mPrevForms.set(0, mCurForm);
            mPrevLemmas.set(0, mCurLemma);
            mPrevTags.set(0, mCurTag);
        }

        if (mNextForms.isEmpty()) {
            mCurForm = form;
            mCurLemma = lemma;
            mCurTag = tag;
        }
        else {
            // shift one index
            mCurForm  = mNextForms.get(0);
            mCurLemma = mNextLemmas.get(0);
            mCurTag   = mNextTags.get(0);

            Cols.shiftDown(mNextForms);
            Cols.shiftDown(mNextLemmas);
            Cols.shiftDown(mNextTags);

            mNextForms .set(mNextForms.size()-1, form);
            mNextLemmas.set(mNextForms.size()-1, lemma);
            mNextTags  .set(mNextForms.size()-1, tag);
        }

        return true;
    }




}


