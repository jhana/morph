package morph.tag;

import java.io.IOException;
import java.util.Random;
import morph.io.CorpusWriter;
import morph.util.MArgs;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.str.Strings;

class CzeRus implements Russification.TranslModule {

    private int copulaThreshold;
    private final Random rnd = new Random();

    public void init(MArgs aArgs) throws Exception {
        copulaThreshold = aArgs.getInt("copulaThreshold", 2);
        System.err.println("copulaThreshold=" + copulaThreshold);
    }

    // @todo handle capitalization !
    public void translate(RussificationCtx aCtx, CorpusWriter aW) throws IOException {
        // verb negation
        final String form = aCtx.curForm();
        final String lemma = aCtx.curLemma();
        final Tag tag = aCtx.curTag();
        final Tagset tagset = tag.getTagset();
        
        // split negated verbs into 'ne' + verb
        if (tag.getPOS() == 'V' && tag.getNegation() == 'N') {
            aW.writeLine("ne", tagset.fromStringFill("TT"));
            aW.writeLine(form.substring(2), tag.setNegation('A').setVariant('-'));   // drop  variant to make sure the following tagtransl works
        } else if (tag.getSubPOS() == '7') {
        } else if (tag.getPOS() == 'V' && Strings.in(form, "jsem", "jsi", "je", "jsme", "jste", "jsou")) {
            if (rnd.nextInt(10) < copulaThreshold) {
                aW.writeLine(form, tag);
            }
        } else {
            aW.writeLine(form, tag);
        }
    }
}
