package morph.tag;

import java.io.IOException;
import lombok.Cleanup;
import morph.io.Corpora;
import morph.io.CorpusLineReader;
import morph.io.CorpusWriter;
import morph.io.PdtWriter;
import morph.io.TntWriter;
import morph.util.MArgs;
import morph.util.MUtilityBase;
import util.err.Err;
import util.io.XFile;

/**
 * @todo make more general, support pdt output (preserving attributes etc.)
 * @todo General CorpusTranslation with callback methods (method reacting to tag, etc)
 * @author Jirka Hana
 */
public class Russification extends MUtilityBase {
    static interface TranslModule {
        void init(MArgs aArgs) throws Exception;
        void translate(RussificationCtx aCtx, CorpusWriter aW) throws IOException;
    }

    public static void main(String[] args) throws Exception {
        new Russification().go(args);
    }

    public void go(String[] aArgs) throws Exception {
        argumentBasics(aArgs, 3);

        String moduleName = getArgs().getDirectArg(0);
        TranslModule translModule;
        if (moduleName.equalsIgnoreCase("CzeRus")) {
                translModule = new CzeRus();
        }
        else if (moduleName.equalsIgnoreCase("SpaCat")) {
                translModule = new SpaCat();
        }
        else if (moduleName.equalsIgnoreCase("SpaPor")) {
                translModule = new SpaPor();
        }
        else {
            throw new Exception("Module " + moduleName + " is not supported.");
        }

        translModule.init(getArgs());

        XFile inFile = getArgs().getDirectFile(1, cCsts);
        XFile outFile = getArgs().getDirectFile(2, cCsts);

        translate(translModule, inFile, outFile);
    }

    private void translate(TranslModule aTranslModule, XFile aInFile, XFile aOutFile) throws IOException {
        @Cleanup CorpusLineReader r = Corpora.openM(aInFile);
        r.setOnlyTokenLines(true);
        
        RussificationCtx ctx = new RussificationCtx(r, 3, 3);
        ctx.prepare();

        @Cleanup CorpusWriter w = openCorpus(aOutFile);

        for (;;) {
            aTranslModule.translate(ctx, w);
            if (!ctx.next()) break;
        }

        // todo write footer
    }



    private CorpusWriter openCorpus(XFile aOutFile) throws IOException {
        if (aOutFile.format() == MUtilityBase.cTnt) {
            return new TntWriter(aOutFile);
        } else if (aOutFile.format() == MUtilityBase.cCsts) {
            PdtWriter w = new PdtWriter(aOutFile);
            w.setLemmaTagCore("l").optionsUpdated();
            w.writeHeader();
            return w;
        } else {
            Err.fErr("Format " + aOutFile.format() + " is not supported");
            return null;
        }
    }
}
