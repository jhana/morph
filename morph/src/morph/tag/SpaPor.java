
package morph.tag;

import java.io.IOException;
import morph.io.CorpusWriter;
import morph.util.MArgs;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.str.StringPair;
import util.str.Strings;

/**
 *
 * @author Jirka Hana
 */
public class SpaPor implements Russification.TranslModule {

    @Override
    public void init(MArgs aArgs) throws Exception {
    }

    // http://en.wikipedia.org/wiki/Differences_between_Spanish_and_Portuguese#Grammar
    @Override
    public void translate(RussificationCtx aCtx, CorpusWriter aW) throws IOException {
        String form = aCtx.curForm();
        Tag tag = aCtx.curTag();
        String newForm = translateArticle(form);        // todo could translate other words as well
        Tag newTag = tag;

        Tag nextTag  = aCtx.getNextTag(0);


        if (tag.startsWith("PX")) {
            // spanish corpus conflates article + poss
            if (form.contains("_")) {
                StringPair article_poss = Strings.splitIntoTwo(form, '_');
                
                Tag articleTag = Tagset.getDef().fromString("DA??-0-0---");
                articleTag = articleTag.setSlot(2,tag.getSlot(2));
                articleTag = articleTag.setSlot(3, tag.getSlot(3));

                aW.writeLine(translateArticle(article_poss.mFirst), articleTag);

                newForm = article_poss.mSecond;
            }
            else {
                // possessive prons agree with noun
                // they are apparently also are preceded by a determiner but this does not seem right
                if (nextTag != null && nextTag.startsWith("N")) {
                    newTag = newTag.setSlot(2,tag.getSlot(2));
                    newTag = newTag.setSlot(3, tag.getSlot(3));
                }
            }
        }


        if (newForm != null) {
            aW.writeLine(newForm, newTag);
        }
    }

//    static Map<String,String> articleMap = new TreeMap<String,String>();
//    articleMap.put("lo", "o");

    String translateArticle(String aSpa) {
        if (aSpa.equals("el")) return "o";
        if (aSpa.equals("la")) return "a";
        if (aSpa.equals("los")) return "os";
        if (aSpa.equals("las")) return "as";
        return aSpa;  // lo + all other words
    }
}
