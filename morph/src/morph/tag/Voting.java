package morph.tag;

import java.util.*;
import lombok.Cleanup;
import util.col.Counter;
import util.err.Err;
import util.io.*;
import morph.ts.BoolTag;
import morph.ts.Tag;
import morph.util.MUtilityBase;
import morph.io.*;
import morph.ts.Tagset;
import util.err.Log;

/**
 * Input:  result of several taggers and result of MA (all on the same file)
 * Output:
 *
 * MA is used to select legal tags (in theory can be replace by comparison
 * with the tagset, that way we could theoretically beat he recall-error of the MA)
 *
 * @author Jirka
 */
public class Voting extends MUtilityBase {
    /*
     * tagger idx -> tagged slots (marked by a string of slot abbreviations)
     */
    private List<BoolTag> taggedSlotsList;
    //private String[] votedSlotsList;

    /**
     * tagger idx -> considered slots (each bit marks whether the given slot
     * of a tag given by idx-st tagger sshould be considered
     *
     * Note: consideredSlotsList[i] is-a-subset-of taggedSlotsList[i]
     */
    private List<BoolTag> consideredSlotsList;

    /*
     * slot idx -> number of taggers voting over this slot
     */
    private int[] mNrOfTaggersPerSlot;

    /**
     *
     */
    public static void main(String[] aArgs) throws java.io.IOException {
        new Voting().go(aArgs);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);
        
        List<XFile> taggedFiles;
//        if (true /*getArgs().getBool("autonames")*/) {
            taggedSlotsList     = getArgs().boolTags("taggedSlots", Tagset.getDef(), Tagset.getDef().cAllTrue);
            consideredSlotsList = BoolTag.fill(getArgs().boolTags("votedSlots",  Tagset.getDef(), Tagset.getDef().cAllTrue), taggedSlotsList.size(), Tagset.getDef().cAllTrue);
            XFile baseFileName = getArgs().getDirectFile(0, cTnt);

            taggedFiles = morph.tag.Utils.x(baseFileName, BoolTag.toCodeStrings(taggedSlotsList));
//        }
//        else {
//            taggedFiles     = getArgs().getDirectFiles(0, cTnt);
//            taggedSlotsList = getArgs().codeStringArray("taggedSlots", "all", taggedFiles.length);
//            consideredSlotsStrs  = getArgs().codeStringArray("votedSlots",  "all", taggedFiles.length);
//        }
//        String[] taggedSlotsStrs = getArgs().getDirectArg(1).split("[:;\\s]");
//        //BoolTag[] taggedSlotsList = BoolTag.fromSlotStrings(taggedSlotsStrs);
//        String[] consideredSlotsStrs = getArgs().getDirectArg(1).split("[:;\\s]");
//        BoolTag[] consideredSlotsList = BoolTag.fromSlotStrings(taggedSlotsStrs);
//        XFile[] taggedFiles         = getArgs().getAutoCorpora(0, cTnt, taggedSlotsList);


        getNrOfTaggersPerSlot();

        System.out.println("# of taggers per slot: " + Arrays.toString(mNrOfTaggersPerSlot));
        System.out.println("Tagger Codes:          " + BoolTag.toCodeStrings(taggedSlotsList));
        System.out.println("Considered Slots Array:" + BoolTag.toCodeStrings(consideredSlotsList));

        XFile maFile  = getArgs().getDirectFile(1); 
        XFile outFile = getArgs().getDirectFile(2); 
        
        vote(taggedFiles, maFile, outFile);
    }

    /**
     * Compares the specified GS and tagged file
     */
    private void vote(final List<XFile> aTaggedFiles, final XFile aMaFile, final XFile aResultFile) throws java.io.IOException {
        Log.info("Voting");
        @Cleanup RBattery taggedRs    = RBattery.openM(aTaggedFiles);
        @Cleanup CorpusLineReader maR = Corpora.openMm(aMaFile);
        @Cleanup TntWriter out        = new TntWriter(aResultFile);

        for(int lineIdx = 1; ; lineIdx++) {
            if (!taggedRs.readLine()) break;
            Err.uAssertFatal(maR.readLine(), "MA file is too short. Must have the same number of forms as the tagged files");
        
            final Tag[] tags = taggedRs.tags();
            
            // --- get info from the MA file
            String form = maR.form();
            Set<Tag> legalTags = maR.tags();
            
            // --- find the best legal tag ---
            List<Counter<Character>> slotVoteCounters = collectVotes(tags);
            Tag bestTag = getBestTag(legalTags, slotVoteCounters);
            
            out.writeLine(form, bestTag);
        }
    }

    /** record votes for each slot */
    private List<Counter<Character>> collectVotes(final Tag[] tags) {
        final List<Counter<Character>> slotVoteCounters = new ArrayList<Counter<Character>>(tagset.getLen());
        for (int i = 0; i < tagset.getLen(); i++) {
            slotVoteCounters.add(collectSlotVotes(tags, i));
        }
        return slotVoteCounters;
    }


    /** record votes for a single slot */
    private Counter<Character> collectSlotVotes(Tag[] aTags, int aSlotIdx) {
        Counter<Character> counter = new Counter<Character>();
        
        // --- go thru the taggers considered for the particular slot ---
        for (int taggerIdx = 0; taggerIdx < consideredSlotsList.size(); taggerIdx++ ) {
            if ( consideredSlotsList.get(taggerIdx).get(aSlotIdx) ) {
                char slotValue = aTags[taggerIdx].getSlot(aSlotIdx);
                counter.add(slotValue);
            }
        }
        return counter;
    }
    
    /** Select one of the legal tags based on the votes */
    private Tag getBestTag(final Set<Tag> legalTags, final List<Counter<Character>> aVotesCounters) {
        double max = -50000;
        Tag bestTag = null;

        for (Tag legalTag : legalTags) {
    //        val = valueCountNonvotes(legalTag,subTagCounters,taggerNrList)  ///value(
            double val = value(legalTag, aVotesCounters);   // value of the tag relative to the voters (the higher the better)
            if (val > max) {
                max = val;
                bestTag = legalTag;
            }
        }
        return bestTag;
    }

    /**
     * Assign a tag a value relative to the voting taggers.
     *
     * @param aLegalTag a tag to assign the value to
     * @param aVotesCounters list of counters (for each slot one) recording number of votes for each slot value
     * @return value of the tag between 0 and taglen; the higher the better. Slots are valued equally
     */
    private double value(Tag aLegalTag, List<Counter<Character>> aVotesCounters) {
        double totalPrice = 0.0;
    
        for (int slotIdx=0; slotIdx < tagset.getLen(); slotIdx++) {
            if (mNrOfTaggersPerSlot[slotIdx] > 0) {
                char slotVal = aLegalTag.getSlot(slotIdx); // this slot's value
                int slotVotes = aVotesCounters.get(slotIdx).frequency(slotVal); // nr of votes for this slot's value
                double slotPrice = (1.0*slotVotes) / (1.0*mNrOfTaggersPerSlot[slotIdx]); // percentage of votes for this slot's value
                totalPrice += slotPrice;
            }
        }

        return totalPrice;
    }

    // calculates the numbers of considered taggers for each slot
    public void getNrOfTaggersPerSlot() {
        mNrOfTaggersPerSlot = new int[tagset.getLen()];  //ints are initialized to 0's

        // todo fnc for BoolTagSum
        for (BoolTag taggerConsideration : consideredSlotsList)
            for (int slot = 0; slot < tagset.getLen(); slot++) {
                if (taggerConsideration.get(slot))
                    mNrOfTaggersPerSlot[slot]++;
            }
    }


}
