
package morph.tag;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Administrator
 */
public class MultiCounter<K,V> {
    Map<K,Map<V,Double>> data = new TreeMap<K,Map<V,Double>>();

//    /**
//     * Override to change basic map's implementation.
//     * @return
//     */
//    protected Map<K,Map<V,Double>> newBasicMap() {
//        return new TreeMap<K,Map<V,Double>>();
//    }
    
    /**
     * Override to change score maps' implementation.
     * @return
     */
    protected Map<V,Double> newScoreMap() {
        return new TreeMap<V,Double>();
    }
    
    public Double put(K aKey, V aVal, double aScore) {
        Map<V,Double> scoreMap = data.get(aKey);
        
        if (scoreMap == null) {
            scoreMap = newScoreMap();
            data.put(aKey, scoreMap);
        }
        
        return scoreMap.put(aVal, aScore);
    }
    
    public Map<V,Double> get(K aKey) {
        return data.get(aKey);
    }
    
}