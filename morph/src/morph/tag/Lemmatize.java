package morph.tag;

import morph.io.*;
import util.io.*;
import java.io.*;
import lombok.Cleanup;
import morph.ma.lts.Fplus;
import morph.ma.lts.Lt;
import morph.ma.lts.Lts;
import morph.ts.Tag;
import morph.util.MUtilityBase;
import util.err.Err;



/**
 * Uses the output of a (tnt) tagger to disambiguate a result of morphological 
 * analysis. The output has one lemma and tag per word.
 */
public class Lemmatize extends MUtilityBase {

    public static void main(String[] args) throws IOException {
        new Lemmatize().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);

        XFile maFileName     = getArgs().getDirectFile(0, cCsts);
        XFile taggedFileName = getArgs().getDirectFile(1, cTnt);
        XFile outFileName    = getArgs().getDirectFile(2, maFileName.format()); 

        disambiguate(maFileName, taggedFileName, outFileName);
    }

    private void disambiguate(XFile aMaFile, XFile aTaggedFile, XFile aOutFile) throws java.io.IOException {
        @Cleanup CorpusLineReader maR  = Corpora.openMm(aMaFile)   .setOnlyTokenLines(false);
        @Cleanup CorpusLineReader tagR = Corpora.openM(aTaggedFile).setOnlyTokenLines(true);
        @Cleanup CorpusWriter     w    = Corpora.openWriter(aOutFile, getArgs().getSubtree("out"));

        for(;;) {
            if (!maR.readLine()) break;
            
            if (maR.isTokenLine()) {
                Err.fAssert(tagR.readLine(), "Input files are not compatible, they have different number of forms\n%s\n%s", aMaFile, aTaggedFile);
                // forms are not checked as one of them might be transcribed, etc

                final Fplus fplus = maR.getFPlus();
                final Tag tag = tagR.tag();

                Lt lt = fplus.getLts().first(Lts.hasTag(tag));
                if (lt == null /* allow non-matches */) {
                    lt = fplus.getLts().iterator().next();      // todo fuzzy matching according to some preferences
                }
                
                w.writeLine(fplus.getForm(), new Lts(lt));
            }
            else {
                // todo provide some general support for this, handle sentences for known formats, etc
                if (aMaFile.format() == aOutFile.format()) {
                    w.writeRaw(maR.line());
                }
            }
            // 
        }
    }
}