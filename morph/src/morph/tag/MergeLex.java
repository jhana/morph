package morph.tag;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import morph.util.MUtilityBase;
import morph.util.tnt.TntLex;
import morph.util.tnt.TntLexEntry;
import morph.util.tnt.TntLexIO;
import morph.ts.Tag;
import util.io.IO;
import util.io.LineReader;

/**
 *
 * @author Administrator
 */
public class MergeLex extends MUtilityBase {
    // ma based lex
    TntLex otherLex = new TntLex();
    
    public static void main(String[] args) throws IOException {
         new MergeLex().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs, 3);       
        
        otherLex = TntLexIO.load( getArgs().getDirectFile(1) );     // todo more than 1
        
        // process ma lex file (tnt format)
        LineReader r = IO.openLineReader(getArgs().getDirectFile(0));

        PrintWriter w = IO.openPrintWriter(getArgs().getDirectFile(2));
        try {
            processMaLex(r, w);
        }
        finally {
            IO.close(r);
            IO.close(w);
        }
    }

    
    boolean inclDebugInfo = true;
    
    private void processMaLex(LineReader aR, PrintWriter aW) throws IOException {
// @todo
//        inclDebugInfo = getArgs().getBool("inclDebugInfo");
        
        for (;;) {
            TntLexEntry maEntry = TntLexIO.readEntry(aR);
            if (maEntry == null) break;
            
            TntLexEntry mergedEntry = createNewLexEntry(maEntry, aW);
            TntLexIO.writeEntry(aW, mergedEntry);
        }    
    }    

    
    protected TntLexEntry createNewLexEntry(TntLexEntry aMaEntry, PrintWriter aW) {
        TntLexEntry otherEntry = otherLex.getEntry(aMaEntry.getForm());   // spanish entry
        if (otherEntry == null) return aMaEntry;

        List<TntLexEntry> otherEntries = Arrays.asList(otherEntry);

        if (inclDebugInfo) {
            for (TntLexEntry e : otherEntries) {
                String srcForm = e.getForm();
                aW.print("%% ");
                aW.print(srcForm);
                aW.print("; ");
                TntLexIO.writeEntry(aW, e);
            }
        }

        return createNewLexEntry(aMaEntry, otherEntries);
    }
    
    /** 
     * Override to change the merging alg.
     * @param maEntry
     * @param otherLexEntries
     * @return
     */
    protected TntLexEntry createNewLexEntry(TntLexEntry maEntry, List<TntLexEntry> otherLexEntries) {
        if (otherLexEntries.isEmpty()) return maEntry;
        
        TntLexEntry mergedEntry = new TntLexEntry();
        mergedEntry.setForm(maEntry.getForm());
        
        for (int i = 0; i < maEntry.getTags().size(); i++) {
           Tag tag = maEntry.getTags().get(i);

           int tagMergedFreq = 0;
           for (TntLexEntry e : otherLexEntries) {
               tagMergedFreq += e.getFreq(tag);
           }
           
           mergedEntry.addTag(tag, tagMergedFreq);
        }

        int maTagFreq =  mergedEntry.getTotalFreq() / maEntry.getTags().size();
        maTagFreq =  Math.max(1, maTagFreq);
        mergedEntry.incFreqs(maTagFreq);
        
        return mergedEntry;
    }
}
