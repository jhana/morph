package morph.tag;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import morph.io.Battery;
import morph.util.MUtilityBase;
import morph.util.tnt.TntLexEntry;
import morph.util.tnt.TntLexIO;
import morph.ts.BoolTag;
import morph.ts.Tag;
import util.io.Files;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;

/**
 *
 * @author jhana
 */
public class FilterLexBySlots extends MUtilityBase {

    private boolean uniform = false;

    public static void main(String[] args) throws IOException {
         new FilterLexBySlots().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs, 3);

        // load nonfiltered lexicon
        XFile srcLexFile = getArgs().getDirectFile(0);
        
        String[] consideredSlotsStrs = getArgs().getDirectArg(1).split("[:;\\s]");
        List<BoolTag> consideredSlotsList = tagset.codestr2booltag(Arrays.asList(consideredSlotsStrs));

        // --- create a list of output files (slot-strings precede the .lex extension) ---
        XFile outDir = getArgs().getDirectFile(2);

        final List<XFile> outFiles = new ArrayList<XFile>();
        for (String slotsStr : consideredSlotsStrs) {
            String shortFileName = Files.addBeforeExtension(srcLexFile.file().getName(), slotsStr);
            outFiles.add( new XFile(outDir, shortFileName) );
        }

        uniform = getArgs().getBool("uniform");

        // --- process --
        List<PrintWriter> ws = null;
        LineReader r = null;
        try {
            ws = Battery.openPrintWriters(outFiles);
            r = IO.openLineReader(srcLexFile);
            filterLexicon(r, consideredSlotsList, ws);
        }
        finally {
            Battery.close(ws);
            IO.close(r);
        }
    }

    private void filterLexicon(LineReader aR, List<BoolTag> consideredSlotsList, List<PrintWriter> aW) throws IOException {
        for (;;) {
            TntLexEntry srcEntry = TntLexIO.readEntry(aR);
            if (srcEntry == null) break;

            for (int i=0; i < consideredSlotsList.size(); i++) {
                filterEntry(srcEntry, consideredSlotsList.get(i), aW.get(i));
            }
        }
    }

    private void filterEntry(TntLexEntry srcEntry, BoolTag consideredSlots, PrintWriter aW) {
        TntLexEntry filteredEntry = createFilteredEntry(srcEntry, consideredSlots);
        TntLexIO.writeEntry(aW, filteredEntry);
    }

    // very inefficient - searching for tags in the tag lists over and over
    private TntLexEntry createFilteredEntry(TntLexEntry srcEntry, BoolTag consideredSlots) {
        TntLexEntry entry = new TntLexEntry();
        entry.setForm(srcEntry.getForm());

        for (Tag srcTag : srcEntry.getTags()) {
            Tag newTag = srcTag.filterTag(consideredSlots);
            entry.addTag(newTag, srcEntry.getFreq(srcTag));
        }

        if (uniform) entry.makeUniform();

        return entry;
    }

}
