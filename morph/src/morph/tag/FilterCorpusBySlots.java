package morph.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Cleanup;
import lombok.Cleanup;
import morph.io.Battery;
import morph.io.Corpora;
import morph.io.CorpusLineReader;
import morph.io.TntWriter;
import morph.util.MUtilityBase;
import morph.ts.BoolTag;
import morph.ts.Tag;
import util.err.Err;
import util.io.Files;
import util.io.XFile;

/**

 * @author jhana
 */
public class FilterCorpusBySlots extends MUtilityBase {

    public static void main(String[] args) throws IOException {
         new FilterCorpusBySlots().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs, 3);

        // load nonfiltered corpus
        XFile srcCorpusFile = getArgs().getDirectFile(0, cCsts);

        String[] consideredSlotsStrs = getArgs().getDirectArg(1).split("[:;\\s]");
        List<BoolTag> consideredSlotsList = tagset.codestr2booltag(Arrays.asList(consideredSlotsStrs));

        // --- create a list of output files (slot-strings precede the .lex extension) ---
        XFile outDir = getArgs().getDirectFile(2,cTnt);

        final List<XFile> outFiles = new ArrayList<XFile>();
        for (String slotsStr : consideredSlotsStrs) {
            String shortFileName = Files.addBeforeExtension(srcCorpusFile.file().getName(), slotsStr);
            outFiles.add( new XFile(outDir, shortFileName) );
        }

        // --- process --
        List<TntWriter> ws = null;
        try {
            @Cleanup CorpusLineReader r = Corpora.openM(srcCorpusFile);
            r.setOnlyTokenLines(true);

            if (outDir.format() == MUtilityBase.cTnt) {
                ws = Battery.tntWriterBattery(outFiles);
            }
            else {
                Err.fAssert(false, "Format " + outDir.format() + " is not supported");
            }

            filterCorpus(r, consideredSlotsList, ws);
        }
        finally {
            Battery.close(ws);
        }

    }

    private void filterCorpus(CorpusLineReader r, List<BoolTag> consideredSlotsList, List<TntWriter> ws) throws IOException {
        for (;;) {
            if (!r.readLine()) {
                break;
            }
            Tag tag = r.tag();
            String form = r.form();
            //String lemma = r.lemma();

            for (int i=0; i < consideredSlotsList.size(); i++) {
                Tag newTag = tag.filterTag(consideredSlotsList.get(i));
                ws.get(i).writeLine(form, newTag);
            }
        }
    }

}