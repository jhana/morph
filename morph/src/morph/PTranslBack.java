package morph;

import util.*;
import morph.io.*;
import util.io.*;
import java.io.*;
import java.util.*;
import morph.ts.Tag;
import morph.util.MUtilityBase;
import util.io.IO;



/**
 * Postprocesses the output of Czech tagger.
 * See main for more comments. 
 * @see #main
 */
public class PTranslBack extends MUtilityBase {
//
//    /**
//     * Translates back Czech tagset to Russian tagset.
//     *
//     * @param args arguments to the program. It should contain 3 direct tags - 
//     * input file, translMemory file and output file.
//     * @throws IOException
//     */    
//    public static void main(String[] args) throws IOException {
//        new PTranslBack().go(args);
//    }
//    
//    public void go(String[] aArgs) throws java.io.IOException {
//        argumentBasics(aArgs,3);
//
//        XFile inFileName           = getArgs().getDirectFile(0, cCsts);
//        XFile outFileName          = getArgs().getDirectFile(2, cTnt);
//
//        translate(inFileName, outFileName);
//    }
//
//    /** 
//     * Translates back Czech tagset to Russian tagset.
//     *
//     * @param aInFile
//     * @param aTranslMemoryFile
//     * @param aOutFile
//     * @throws IOException
//     */
//    void translate(XFile aInFile, XFile aOutFile) throws java.io.IOException {
//        TntReader r = TntReader.openM(aInFile);
//        TntWriter w = new TntWriter(aOutFile);
//
//        for(;;) {
//            if (!r.readLine()) break;
//            //String memLine = memory.readLine();
//
//            Tag tag = r.tag();
//            int translTagIdx = memLine.indexOf("<new>" + tag);
//            if (translTagIdx != -1) {
//                int origTagIdx = translTagIdx + 5 + Tag.cTagLen + 6;
//                tag = Tagset.getDef().fromString( memLine.substring(origTagIdx, origTagIdx + Tag.cTagLen) );
//            }
//
//            w.writeLine(r.form(), tag);
//        }
//        
//        r.close();
//        w.close();
//        memory.close();
//    }
}