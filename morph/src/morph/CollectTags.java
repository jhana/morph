package morph;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.Cleanup;import lombok.Getter;
import morph.io.Corpora;

import morph.io.CorpusLineReader;
import morph.util.MUtilityBase;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.col.Counter;
import util.io.IO;
import util.io.XFile;

/**
 * Collects tags in a corpus.
 * @author Administrator
 */
public class CollectTags extends MUtilityBase {
    @Getter private final Counter<Tag> tags = new Counter<Tag>();

    public static void main(String[] args) throws IOException {
        new CollectTags().go(args);
    }

    public List<Tag> getSortedTags() {
        final List<Tag> sortedTags = new ArrayList<Tag>(tags.keySet());
        Collections.sort(sortedTags);
        return sortedTags;
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);

        final XFile inFile  = getArgs().getDirectFile(0, cCsts);
        final XFile outFile = getArgs().getDirectFile(1, cCsts);
        final boolean mm    = getArgs().getBool("mm", false);

        collectTags(inFile, mm);
        dumpTags(outFile);
    }

    public void collectTags(final XFile aInFile, final boolean aMm) throws IOException {
        // todo experimental tmp code
//        Tagset ts = Tagset.getDef();
//        if (aInFile.getProperties().getOptions().containsKey("ts")) {
//            String val = aInFile.getProperties().getOptions().getString("ts");
//            if ("?".equals(val)) {
//                ts = new Tagset.TagsetBuilder().build();
//            }
//        }
        
        @Cleanup CorpusLineReader r = Corpora.open(aInFile).configure().setOnlyTokenLines(true).setMm(aMm);

        for(;;) {
            if (!r.readLine()) break;
            tags.addAll(r.tags());
            tags.add(r.tag());
        }
    }

    private void dumpTags(XFile aOutFile) throws IOException {
        final boolean outputFreq = ! getArgs().getBool("output.noFreq", false);

        @Cleanup PrintWriter w = IO.openPrintWriter(aOutFile);

        for(Tag tag : getSortedTags()) {
            w.print(tag.toString());
            if (outputFreq) {
                w.print('\t');
                w.println(tags.frequency(tag));
            }
        }
    }

}
