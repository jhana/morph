package morph;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;
import morph.util.MUtilityBase;
import util.io.IO;
import util.io.XFile;
import util.str.Strings;


public class TsDiff extends MUtilityBase {

    public static void main(String[] args) throws IOException {
        new TsDiff().go(args);
    }

    /**
     *
     * @param aArgs
     * @throws java.io.IOException
     */
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);
        //Log.setILevel(Level.ALL);  // @todo something better

        XFile tagsetAFile = getArgs().getDirectFile(0);
        XFile tagsetBFile = getArgs().getDirectFile(1);
        XFile outFile  = getArgs().getDirectFile(2);

        List<String> tagsetA = IO.readInColumn(tagsetAFile, 0);
        List<String> tagsetB = IO.readInColumn(tagsetBFile, 0);

        // ignore negation, degree


//        PrintWriter w = IO.openPrintWriter(outFile);

//        w.println("// tags in the first tagset but not in the second one");
//        List<String> diff = minus(tagsetA, tagsetB);
//        dump(w, diff);

//        w.println("// tags in the second tagset but not in the first one");
//        diff = minus(tagsetB, tagsetA);
//        dump(w, diff);
        printDiff(outFile, tagsetA, tagsetB);

//        w.close();
    }

    static class TagComparator implements  Comparator<String> {
        int[] slots;
        TagComparator(int ... aSlots) {
            slots = aSlots;
        }

        @Override
        public int compare(String aTagA, String aTagB) {
            for (int slot : slots) {
                char a = aTagA.charAt(slot);
                char b = aTagB.charAt(slot);

                if (a > b) {
                    return 1;
                }
                else if (a < b) {
                    return -1;
                }
            }

            return aTagA.compareTo(aTagB);
        }
    }

    void printDiff(XFile aOutFile, List<String> aTagsB, List<String> aTagsA) throws IOException {
        PrintWriter w = IO.openPrintWriter(aOutFile);

        SortedSet<String> tagsetA = new TreeSet<String>(aTagsA);
        SortedSet<String> tagsetB = new TreeSet<String>(aTagsB);

        // sort the tags // todo valid only for romance
        SortedSet<String> all = new TreeSet<String>(new TagComparator(0,1,7,5,3));
        all.addAll(tagsetA);
        all.addAll(tagsetB);

        String empty = Strings.spaces(all.first().length());
        // TODO parametrize @!!!!!
        //                                             psgncfmet d   a    v--i
        final Pattern acceptablePattern = Pattern.compile(".*"); // Pattern.compile(".........[\\-1][\\-A]...-");

        for (String tag : all) {
            if (!acceptablePattern.matcher(tag).matches()) continue;

            String tagA = tagsetA.contains(tag) ? tag : empty;
            String tagB = tagsetB.contains(tag) ? tag : empty;

            w.print( tagA.equals(tag) ? ' ' : 'e');
            w.print( tagB.equals(tag) ? ' ' : 'm');
            w.println("  " + tagA + "  " + tagB);
        }

        w.close();
    }

    /**
     * Both lists must be sorted.
     * @param <T>
     * @param aList
     * @param bList
     * @return
     * @todo make more general, move to Cols
     */
    List<String> minus(List<String> aList, List<String> bList) {
        Set<String> set = new TreeSet<String>(aList);
        set.removeAll(bList);
        List<String> result = new ArrayList<String>(set);
        Collections.sort(result);
        return result;
    }

    private <T> void dump(PrintWriter aW, List<T> aList) {
        for (T tag : aList) {
            aW.println(tag);
        }
    }

}

