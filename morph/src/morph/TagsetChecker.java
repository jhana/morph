package morph;

import util.io.*;
import java.io.*;
import java.util.*;
import morph.ts.Tags;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.str.Strings;
import morph.util.MUtilityBase;
import util.io.IO;


/**
 *
 * @author  Jiri
 */
public class TagsetChecker extends MUtilityBase {
    
    /** Creates a new instance of TagsetChecker */
    public TagsetChecker() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  throws IOException {
        new TagsetChecker().go(args);
    }
     
    public void go(String[] aArgs) throws IOException {
        argumentBasics(aArgs);
        check(getArgs().getDirectFile(0), getArgs().getDirectFile(1), getArgs().getDirectFile(2));
    }
    
    public void check(XFile aTagsetFile, XFile aTranslFile, XFile aLogFile) throws java.io.IOException {
        SortedSet<Tag> tagset = readInTagset(aTagsetFile);
        
        LineNumberReader translFile = IO.openLineReader(aTranslFile);
        PrintWriter log = IO.openPrintWriter(aLogFile);

        String line = translFile.readLine();
        int count =0;
        log.println("Tags not in the tagset:");
        
        while (line != null) {
            List<Tag> tags =  Tags.fromStrings(Tagset.getDef(), Strings.splitL(line) );
            assert tags.size() == 2 : "Error in translTable - ln: " + translFile.getLineNumber();
            
            if (!tagset.contains(tags.get(1)) && tags.get(1).getVariant() != '1') {
                log.printf("[%4d %4d] '%s'\n", translFile.getLineNumber(), (++count), tags.get(1));
            }
            
            line = translFile.readLine();
        }
        
        translFile.close();
        log.close();
    }

    SortedSet<Tag> readInTagset(XFile aTagsetFile) throws java.io.IOException {
        LineNumberReader tagsetReader = IO.openLineReader(aTagsetFile);
        SortedSet<Tag> tagset = new TreeSet<Tag>();
        
        String line = tagsetReader.readLine();
        while (line != null) {
            Tag tag = Tagset.getDef().fromString(line.trim());
            tagset.add(tag);
            line = tagsetReader.readLine();
        }
        
        tagsetReader.close();
        return tagset;
    }
    
}
