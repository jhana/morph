//
//package morph.io2;
//
//import java.util.Arrays;
//import pdt.Tag;
//import pdt.Tags;
//import util.err.Log;
//import util.err.MyException;
//import util.io.XFile;
//
///**
// *
// * @author Jirka Hana
// */
//public class TntReader implements FLayerReader, MLayerReader, MmLayerReader {
//    public FLayerReader getFLayerReader() {
//        return (FLayerReader) this;
//    }
//
//    public MLayerReader asMReader() {
//        return (MLayerReader) this;
//    }
//
//    public MmLayerReader asMmReader() {
//        return (MmLayerReader) this;
//    }
//
//
//    //protected String[] mTokens;
//
//    /**
//     * In most cases, aReadTag and aReadTags are not true at the same time.
//     * @todo remove and bool replace with set...
//     */
//    public TntReader(XFile aFile) throws java.io.IOException {
//        super(aFile);
//    }
//
//    /**
//     * Reads the next line from the TNT tagged file.
//     * Ignores TNT comments (lines starting with %%)
//     * Returns false once EOF was reached; true otherwise
//     */
//    public boolean next() throws java.io.IOException {
//        mForm = null;
//        do {
//            mLine = mR.readLine();
//            if (mLine == null) return false;
//            mLine = mLine.trim();
//        } while ( mLine.startsWith("%%") || mLine.length() == 0 );
//
//        mTokens = cWhiteSpacePattern.split(mLine);
//        assertE(mTokens.length > 1, "Format should be: 'form tag'");
//
//        mForm = mTokens[0];
//
//        try {
//            if (mFillTag)
//                mTag  = Tagset.getDef().fromString(mTokens[1]);      // @todo inteligent checking ?throw Exception?
//            if (mFillTags)
//                mTags = Tags.fromStringsS( Arrays.asList(mTokens).subList(1,mTokens.length) );
//        }
//        catch(MyException e) {
//            Log.log(e, "Error [%d] %s\n", mR.getLineNumber(), mR.getFile());
//        }
//
//        return true;
//    }
//
//}
