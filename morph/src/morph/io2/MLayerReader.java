
package morph.io2;

import morph.data.Ltx;

/**
 *
 * @author Jirka Hana
 */
public interface MLayerReader {
    String getForm();
    Ltx getLt();
}
