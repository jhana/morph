
package morph.io2;

/**
 *
 * @author Jirka Hana
 */
public interface LayerReader {
    public boolean next() throws java.io.IOException;
}
