/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph.acqp;

import com.google.common.collect.ImmutableSortedSet;
import java.util.SortedSet;
import java.util.TreeSet;
import lombok.Getter;

/**
 *
 * @todo optimize stems and endings for small sets
 * @author j
 */
public class Scheme {
    final @Getter SortedSet<String> stems;
    final @Getter SortedSet<String> endings;

    public Scheme(String aEnding) {
        this(ImmutableSortedSet.of(aEnding), new TreeSet<String>());
    }
//    public Scheme(Set<String> endings) {
//        this.endings = endings;
//        this.stems = new HashSet<String>();
//    }

    public Scheme(SortedSet<String> stems, SortedSet<String> endings) {
        this.stems = stems;
        this.endings = endings;
    }

    

    void addWord(FormFreq aFF, String stemCand) {
        stems.add(stemCand);    // todo remember aFF
    }
}
