package morph.acqp;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javax.xml.stream.XMLStreamException;
import morph.util.MUtilityBase;
import util.io.XFile;
import util.str.Strings;




/**
 * This is under development. It should be a re-implementation of Paramor.
 * @author j
 */
public class CreateBasicSchemes extends MUtilityBase {

    // manually provided resources
    private Set<String> endings;
    private int maxEngingLen;
    
    /**
     */
    public static void main(String[] args) throws IOException, XMLStreamException {
        new CreateBasicSchemes().go(args);
    }

// =============================================================================
// Implementation
// =============================================================================
    
    private void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        
        XFile inFile  = getArgs().getDirectFile(0);
        XFile outFile = getArgs().getDirectFile(1);
        
        loadInfo();
        
        learnFromCorpus(inFile);
    
    }
    
    
    private void loadInfo() {
        String endingsStr = "a|y|ě|o|ou||ám|ách|ami|...";
        endings = com.google.common.collect.ImmutableSet.<String>builder().addAll(
                Arrays.asList(endingsStr.split("|"))).build();
        
        maxEngingLen = 0;
        for (String ending : endings) {
            if (ending.length() > maxEngingLen) maxEngingLen = ending.length();
        }
        
    }

    Schemes schemes = new Schemes();
    
    private void learnFromCorpus(XFile aInFile) throws IOException {
        initialSegmentation(aInFile);
        
        //mergeIdenticalSchemes();

        //mergeSimilarSchemes();
        
    }

    private void initialSegmentation(XFile aInFile) throws IOException {
        // initial schemes
        for (String ending : endings) {
            schemes.add(new Scheme(ending));
        }

        
        final FreqReader r = FreqReader.builder()
                .setFile(aInFile)
                .setNarrow(null)
                .setMinFreq(-1)
                .open();

        for(;;) {
            FormFreq ff = r.next();
            if (ff == null) break;

            segment(ff);
        }
        r.close();
    } 
     
    private void segment(final FormFreq aFF) {
        for (int endingLen = 0; endingLen <= maxEngingLen; endingLen++) {
            String endingCand = Strings.getTail(aFF.getForm(), endingLen);
            if (!endings.contains(endingCand)) continue;

            String stemCand = Strings.removeTail(aFF.getForm(), endingLen);

            Scheme scheme = schemes.getSchemeByEnding(endingCand);

            scheme.addWord(aFF, stemCand);
        }
    }

    private void search() {
        List<Scheme> initialSchemes =  null; 
        
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
