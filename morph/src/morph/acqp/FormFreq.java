/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph.acqp;

import lombok.Getter;
import morph.acq.CapFreq;

/**
 *
 * @author j
 */
@Getter
public class FormFreq {
    String form;
    CapFreq freq;

    public FormFreq(String form, CapFreq freq) {
        this.form = form;
        this.freq = freq;
    }
    
    public int getTotalFreq() {
        return freq.getTotalFreq();
    }
    
    
}
