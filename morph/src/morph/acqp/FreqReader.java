package morph.acqp;

import java.io.IOException;
import java.util.regex.Pattern;
import morph.acq.CapFreq;
import util.err.Err;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;
import util.str.Strings;

/**
 *
 * consider using import com.google.common.io.LineReader;
 * @author j
 */
public class FreqReader {



    public static class FreqReaderBuilder {
        private XFile file;
        private Pattern narrow = null;
        private int minFreq = -1;
        
        private FreqReaderBuilder() {
            
        }
        
        // todo setOptions(Options aOption) 
        
        public FreqReaderBuilder setFile(XFile file) {
            this.file = file;
            return this;
        }

        public FreqReaderBuilder setMinFreq(int minFreq) {
            this.minFreq = minFreq;
            return this;
        }

        public FreqReaderBuilder setNarrow(Pattern narrow) {
            this.narrow = narrow;
            return this;
        }
        
        public FreqReader open() throws IOException {
            LineReader r = IO.openLineReader(file);
            r.configureSplitting(Strings.cWhitespacePattern, 2, "required <form> <f>:<f>:<f>:<f>");
            
            return new FreqReader(r, narrow, minFreq);
        }
    }
    
    public static FreqReaderBuilder builder() {
        return new FreqReaderBuilder();
    }

    
    private final LineReader r;
    private final Pattern narrow;
    private final int minFreq;
    
    private FreqReader(LineReader r, Pattern narrow, int minFreq) {
        this.r = r;
        this.narrow = narrow;
        this.minFreq = minFreq;
    }

    public FormFreq next() throws IOException {
        
        for(;;) {
            final String[] strs = r.readSplitLine();
            if (strs == null) return null;

            String form = strs[0];
            if ( narrow != null && !narrow.matcher(form).matches() ) continue;

            CapFreq freqs = null;
            try {
                freqs = CapFreq.fromString(strs[1]);
            }
            catch(Exception e) {
                Err.fErr(r, e, "frequency string \"%s\" cannot be parsed", strs[1] );
            }

            if ( minFreq != -1 && freqs.getTotalFreq() < minFreq ) continue;

            return new FormFreq(form, freqs);
        }
        
    }
    
    public void close() {
        IO.close(r);
    }

    
}
