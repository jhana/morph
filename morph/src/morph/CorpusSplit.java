package morph;

import java.io.*;
import java.util.List;
import morph.io.*;
import morph.util.MUtilityBase;
import util.col.Cols;
import util.err.Err;
import util.err.Log;
import util.io.XFile;

/**
 * Splits corpus into several parts with specified length.
 * Use cat * > x to connect individual files.
 * @todo other formats
 * @todo write log with boundaries (sentence ids) for each out file 
 */
public class CorpusSplit extends MUtilityBase {

    private int mTotal;     // number of words to read from the input file
    private List<Integer> mTotals;  // number of words to write into each output file
    private List<XFile> mOutFiles;  
    private int mPieces;

    public static void main(String[] args) throws IOException {
        new CorpusSplit().go(args);
    }
       
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,5);
        mTotal           = getArgs().getDirectInt(0);
        XFile inFileName = getArgs().getDirectFile(1);

        mTotals   = getArgs().getDirectIntsL(2);
        mOutFiles = getArgs().getDirectFiles(3, cCsts);
        Err.uAssert(mTotals.size() == mOutFiles.size(), "number of totals must be equal to number of output files");

        mPieces = getArgs().getDirectInt(4);
        
        split(inFileName);
    }
    
    public void split(XFile aInFile) throws java.io.IOException {

        // --- prepare data variables  ----
        PdtLineReader in = new PdtLineReader(aInFile).setOnlyTokenLines(false);
        
        int outputs = mOutFiles.size();
        List<PrintWriter> outs = Battery.openPrintWriters(mOutFiles);
        
        int[] idxs = new int[outputs+1];
        for(int i=0; i < idxs.length; i++) idxs[i]=0;
        

        // --- add an extra item to the totals ---
        mTotals.add(mTotal - Cols.sum(mTotals));
        Err.uAssert(mTotals.get(mTotals.size()-1) >= 0, "Must hold: mTotals[mTotals.length-1] >= 0");
        Log.info("Totals: %s", mTotals);

        // --- initialization of cycle counters etc. ----
        boolean skipping = false;
        boolean aboutToSwitch = false;

        int cycle = 0;
        int cur = 0;

        int curIdx   = idxs[cur];
        int curTotal = mTotals.get(cur);
        int curLimit = (cycle+1)*(curTotal/mPieces);  
        //@@ TntWriter curOut   = outs[cur];
        PrintWriter curOut   = outs.get(cur); //@
        
        int globalIdx = 0;
        
        // --- go thru the lines ----
        Log.info("Switching: cur %d, curIdx %d, globalIdx %d, curLimit %d\n", cur, curIdx, globalIdx, curLimit);
        for(;cycle < mPieces;) {
            if (!in.readLine()) {
                curOut.printf("<mdesc>End of piece: %d, curIdx: %d, globalIdx: %d\n", cur, curIdx, globalIdx, curLimit); //@
                Log.info("Switching: cur %d, curIdx %d, globalIdx %d, curLimit %d\n", cur, curIdx, globalIdx, curLimit);
                break;
            }
            
            if (in.lineType().fd()) {
                //@@ if (!skipping) curOut.writeLine(in.form(), in.tag());
                globalIdx++;
                curIdx++;
            }

            if (aboutToSwitch) {
                if ( (in.lineType().sentence()) || (curIdx >= curTotal) ) {
                    if (!skipping) curOut.printf("<mdesc>End of piece: %d, curIdx: %d, globalIdx: %d\n", cur, curIdx, globalIdx, curLimit); //@
                    aboutToSwitch = false;
                    // @todo write info about sentence id, globalIdx, etc.
                    idxs[cur] = curIdx;
                    
                    if (skipping) { 
                        cur = 0;
                        cycle ++;
                    }
                    else                
                        cur ++;

                    skipping = (cur == outputs);

                    curIdx   = idxs[cur];
                    curTotal = mTotals.get(cur);
                    curLimit = (cycle+1)*(curTotal/mPieces);  
                    curOut   = (skipping) ? null : outs.get(cur);
                    
                    Log.info("Switching: cur %d, curIdx %d, globalIdx %d, curLimit %d\n", cur, curIdx, globalIdx, curLimit);
                }
            }
            else
                aboutToSwitch = (curIdx >= curLimit - 1);
            
            if (!skipping) curOut.println(in.line());   //@
        }

        //Log.info("Last idxs: " + idxs[0] + ", " + idxs[1] + ", " + idxs[2]);
        
        in.close();
        Battery.close(outs);
    } 

}