package morph.stat;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import lombok.Cleanup;
import morph.io.Corpora;
import morph.io.WordReader;
import util.io.*;
import util.col.Counter;
import morph.util.MUtilityBase;

/**
 * Collects forms from a corpus and sorts them according to their frequencies.
 *
 */
public class FreqSort extends MUtilityBase {
   /**
     * Corpus size
     */
    private int mWords = 0;

    /**
     * Counter keeping track of frequencies of the words read from the corpus.
     */
    private Counter<String> mCounter = new Counter<String>();

    Pattern mNarrow;
    
    
    /**
     * Maximal numbers of words read from the corpus. 
     * If all the whole corpus should be read it is set to Integer.MAX_VALUE.
     */
    private int mCorpusSizeLimit;
    
    
// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------

    /**
     */
    public static void main(String[] args) throws IOException {
        new FreqSort().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        // --- get configuration ---
        argumentBasics(aArgs,2);
        
        XFile inFileName   = getArgs().getDirectFile(0);
        XFile outFileName  = getArgs().getDirectFile(1);

        // input limitations
        mNarrow          = getArgs().getPatternNE("narrow");
        mCorpusSizeLimit  = getArgs().getInt("corpusSizeLimit", Integer.MAX_VALUE);

        // output con configuration
        boolean printFreqs = getArgs().getBool("printFreqs");
        int topNOnly    = getArgs().getInt("topN", Integer.MAX_VALUE);

        
        // --- process ---
        profileStart();
        process(inFileName);
        profileEnd("File Analyzed in %f s\n");
    
        profileStart();
        writeResults(outFileName, printFreqs, topNOnly);
        profileEnd("Results written in %f s\n");
    }

    protected void process(XFile aFileName) throws java.io.IOException {
        @Cleanup WordReader r = Corpora.openWordReader(aFileName);
        for(;;) {
            r.nextWord();
            if (r.word() == null) break;
            if (mNarrow != null && !mNarrow.matcher(r.word()).matches() ) continue;
            if (mWords >= mCorpusSizeLimit) break;
            
            mCounter.add(r.word());
            mWords++;
        }
    }

    protected void writeResults(XFile aOutFile, boolean aPrintFreqs, int aTopNOnly) throws java.io.IOException {
        PrintWriter out = IO.openPrintWriter(aOutFile);
        
        int entries = 0;
        for(Map.Entry<String,Integer> entry : mCounter.setSortedByFreq()) {
            if (aPrintFreqs) out.print(entry.getValue() + ' ');
            out.println(entry.getKey());
            entries++;
            if (entries >= aTopNOnly) break;
        }
        
        out.close();
   }

}

