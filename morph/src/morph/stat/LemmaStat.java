package morph.stat;

import java.io.*;
import java.util.*;
import morph.io.*;
import util.io.*;
import morph.util.CorpusProcessor;
import morph.util.WordAction;
import morph.ts.Tag;
import util.io.IO;
import util.str.Cap;
import util.str.Caps;

/**
 * Collects info about lemmas and produces a table suitable for statistical processing (by SAS, etc)
 * lemma: # of tokens, # of distinct forms (ic), # distinct tags
 * lemma, 
 * @author  Jirka
 */
public class LemmaStat extends CorpusProcessor {
    
    class LemmaStruct {
        String mLemma;
        int mFreq;
        List<FormStruct> mForms;
        List<TagStruct> mTags;
        //@todo? replace tags with enum counter for S1 - P7

        public int hashCode() {
            return mLemma.hashCode();
        }
        
        public boolean equals(Object aObj) {
            if (! (aObj instanceof LemmaStruct) ) return false;
            return mLemma.equals(((LemmaStruct)aObj).mLemma);
        }
        
        LemmaStruct(String aLemma) {
            mLemma = aLemma;
            mForms = new ArrayList<FormStruct>(8);
            mTags = new ArrayList<TagStruct>(8);
        }
        
        void add(String aForm, Tag aTag) {
            addForm(aForm);
            addTag(aTag);
            mFreq ++;
        }

        void addForm(String aForm) {
            for (FormStruct fs : mForms) {
                if (fs.mForm.equalsIgnoreCase(aForm)) {
                    if (!fs.mCap.lower() && Caps.capitalization(aForm).less(fs.mCap)) {
                        fs.setForm(aForm);
                    }
                    
                    fs.inc();
                    return;
                }
            }
            mForms.add(new FormStruct(aForm));
        }
        
        void addTag(Tag aTag) {
            for (TagStruct ts : mTags) {
                if (ts.mTag.equals(aTag)) {
                    ts.inc();
                    return;
                }
            }
            mTags.add(new TagStruct(aTag));
        }

        class CountingStruct {
            int mFreq = 1;
            void inc() {mFreq++;}
        }
        
        class FormStruct extends CountingStruct {
            String mForm;
            Cap mCap;
            
            FormStruct(String aForm) {
                setForm(aForm);
            }
            void setForm(String aForm) {
                mForm = aForm;
                mCap = Caps.capitalization(aForm);
            }
            public String toString() {return mForm + ":" + mFreq;}
        }
        
        class TagStruct extends CountingStruct {
            Tag mTag;
            TagStruct(Tag aTag) {
                mTag = aTag;
            }
            public String toString() {return mTag + ":" + mFreq;}
        }
        
        
//        public String toStringN() {
//            StringBuilder sb = new StringBuilder();
//            sb.append(pad(mLemma, 30)).append(String.format("%8d", mFreq));
//            for (char nr : Arrays.asList('S', 'P')) {
//                for (char xcase : Arrays.asList('1', '2', '3', '4', '5', '6', '7', 'X')) {
//                    sb.append(String.format("%8d", tagFreqByNrCase(nr, xcase)));
//                }
//            }   
//            return sb.toString();
//        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(pad(mLemma, 30));
            sb.append(String.format("%8d ", mFreq));
            sb.append(String.format("%8d ", mForms.size()));
            //sb.append(mForms.toString());
            return sb.toString();
        }
        
        
        private String pad(String aStr, int aWidth) {
            aStr = (aStr.length() <= 30) ? aStr : aStr.substring(0, aWidth);
            return String.format("%-30s", aStr);
            
        }
        
//        private int tagFreqByNrCase(char aNr, char aCase)  {
//            int freq = 0;
//            for (TagStruct ts : mTags) {
//                if (ts.mTag.getNumber() == aNr && ts.mTag.getCase() == aCase)
//                    freq += ts.mFreq;
//            }
//            return freq;
//        }
    }
    
  
    PrintWriter mOut;

    // @todo to superclass
    protected boolean mCheckAllPOSs;
    protected String mCheckedPOSs;
    
    /**
     */
    public static void main(String[] args) throws IOException {
        new LemmaStat().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        
        XFile inFileName   = getArgs().getDirectFile(0);
        XFile outFileName  = getArgs().getDirectFile(1);

        mCheckedPOSs  = getArgs().getBool("-oc") ? "NAVD" : "";      // open class
        mCheckedPOSs  = getArgs().getString("checkedPOS", mCheckedPOSs);          // specified classes
        mCheckAllPOSs = mCheckedPOSs.equals("");

        profileStart();
        
        Counter counter = new Counter();
        process(counter, inFileName);
        writeOut(counter, outFileName);

        profileEnd("Lexicon created in ");
    }
    

    protected String helpString() {
        return "java [mem] LemmaStat [options] {in-file} {out-file}\n" +
            "{in-file} - can be a directory\n" + 
            "  -oe {enc} - encoding; default: ??@todo\n" +
            "  -ie {enc} - encoding; default: ??@todo\n" +
            "  -mw #     - how many words to read from the corpus: default: infinity\n" +
            " mem - usually the max heap limit must be raised. On Sun's JDK 5.0, use e.g. -Xmx700m";
    }

    
    protected void configureReader(WordReader aR) {
        ((CorpusLineWordReader) aR).reader().setMm(false);      // todo why????
    }
    
    class Counter implements WordAction {
        Map<String, LemmaStruct> mCounter;

        public void init() {
            mCounter = new HashMap<String, LemmaStruct>();
        }
        
        public boolean processWord(WordReader aR) {
            CorpusLineReader r = ((CorpusLineWordReader) aR).reader();
            
            String form  = r.form();
            String lemma = r.lemma();
            Tag tag      = r.tag();

            if (mCheckAllPOSs || tag.POSIn(mCheckedPOSs))
                process(form, lemma, tag);
            return true;
        }

        protected void process(String aForm, String aLemma, Tag aTag) {
            if (aTag.getPOS() == 'Z') return;

            LemmaStruct ls = mCounter.get(aLemma);
            if (ls == null) {
                ls = new LemmaStruct(aLemma);
                mCounter.put(aLemma, ls);
            }

            ls.add(aForm, aTag);
        }    
    }
    
    
    protected void writeOut(Counter aCounter, XFile aFileName) throws java.io.IOException {
        PrintWriter w = IO.openPrintWriter(aFileName);

        int entries = 0;
        for (LemmaStruct ls : toSortedSet(aCounter)) {
            w.println(ls);
        }
        
        w.close();
    }

    public SortedSet<LemmaStruct> toSortedSet(Counter aCounter) {
        Comparator<LemmaStruct> comparator = new Comparator<LemmaStruct>() {
            public int compare(LemmaStruct o1, LemmaStruct o2) {
                int freqDist = o2.mFreq - o1.mFreq;
                return (freqDist == 0) ? o1.mLemma.compareTo(o2.mLemma) : freqDist;
            }
            public boolean equals(Object obj) {
                return obj == this;
            }
        };
        
        SortedSet<LemmaStruct> freqSortedSet = new TreeSet(comparator);
        freqSortedSet.addAll(aCounter.mCounter.values());
        aCounter.mCounter = null;   // return memory
        return freqSortedSet;
    }

}
