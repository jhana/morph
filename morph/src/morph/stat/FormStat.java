package morph.stat;

import java.io.*;
import java.util.*;
import morph.io.*;
import util.io.*;
import morph.util.CorpusProcessor;
import morph.util.WordAction;
import util.str.Strings;
import util.io.IO;

/**
 * Collects info about forms and produces a table suitable for statistical processing (by SAS, etc)
 * lemma: # of tokens, # of distinct forms (ic), # distinct tags
 * lemma, 
 * @todo share code with LemmaStat
 * @author  Jirka
 */
public class FormStat extends CorpusProcessor {
    PrintWriter mOut;

    /**
     */
    public static void main(String[] args) throws IOException {
        new FormStat().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        
        XFile inFileName   = getArgs().getDirectFile(0);
        XFile outFileName  = getArgs().getDirectFile(1);

        profileStart();
        
        Counter counter = new Counter();
        process(counter, inFileName);
        //writeOutDecileSample(counter, outFileName);
        writeOut(counter, outFileName);

        profileEnd("Corpus processed in ");
    }
    

    protected String helpString() {
        return "java [mem] FormStat [options] {in-file} {out-file}\n" +
            "{in-file} - can be a directory\n" + 
            "  -oe {enc} - encoding; default: ??@todo\n" +
            "  -ie {enc} - encoding; default: ??@todo\n" +
            "  -mw #     - how many words to read from the corpus: default: infinity\n" +
            " mem - usually the max heap limit must be raised. On Sun's JDK 5.0, use e.g. -Xmx700m";
    }

    class Counter implements WordAction {
        util.col.Counter<String> mCounter;

        public void init() {
            mCounter = new util.col.Counter<String>(); 
        }
        
        public boolean processWord(WordReader aR) {
            if (!isNumber(aR.word())) mCounter.add(aR.word().toLowerCase());
            return true;
        }    

        boolean isNumber(String aForm) {
            return Strings.someCharsShared(aForm, "0123456789");
        }

    }
    
    protected void writeOut(Counter aCounter, XFile aFileName) throws java.io.IOException {
        PrintWriter w = IO.openPrintWriter(aFileName);
        for(Map.Entry<String, Integer> e : aCounter.mCounter.setSortedByFreq()) {
            w.printf("%-30s %8d\n", Strings.trim(e.getKey(), 30), e.getValue());
        }
        
        w.close();
    }

    /** 
     * Prints a random sample of a specified number of forms from each decile.
     *
     */
    protected void writeOutDecileSample(Counter aCounter, XFile aFileName) throws java.io.IOException {
        PrintWriter w = IO.openPrintWriter(aFileName);

        SortedSet<Map.Entry<String, Integer>> sorted = aCounter.mCounter.setSortedByFreq();
        aCounter.mCounter = null;

        int[] xiles = new int[]{1,5,10,25,50,100};
        
        int origSize = sorted.size();
         System.out.println("orig size: " + origSize);

        int sampleSize = 40;
        int lastXile = 0;

        for(int idx =0; idx < xiles.length; idx++) {
            int delta = xiles[idx] - lastXile;
            lastXile = xiles[idx];
            int xileSize = Math.round(origSize/100f*delta);
            List<String> xile = new ArrayList<String>();
            System.out.println("xIdx: " + idx);
            System.out.println("delta:   " + delta);
            System.out.println("size: " + xileSize);
        
            for(Iterator<Map.Entry<String, Integer>> it = sorted.iterator(); it.hasNext();  ) {
                Map.Entry<String, Integer> e = it.next();
                if (e.getValue() > 1) xile.add(e.getKey() + "    // " + e.getValue());
                it.remove();
                if (xile.size() == xileSize) break;
            }
            
            // --- write out a sample of the x-ile ---
            w.println("// ===================");
            w.println("// Xile: " + xiles[idx] );

            Collections.shuffle(xile);
            for (int i = 0; i < sampleSize; i++) {
                if (i >= xile.size()) {
                    w.println("// ### not enough entries");
                    break;
                }
                w.println(xile.get(i));
            }
            
        }
        
        w.close();
    }
    
}
