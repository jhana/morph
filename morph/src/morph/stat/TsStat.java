package morph.stat;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import morph.io.*;
import util.io.*;
import morph.util.CorpusProcessor;
import morph.util.WordAction;
import util.Pair;

/**
 * Collects statistics about a tagset from a corpus.
 * @todo configurable
 * @author  Jirka
 */
public class TsStat extends CorpusProcessor {
    private String separator = "\t";
    private int tickSize = 100;
    private boolean filterTag = false;

    private int tsSize = 0;


    /**
     */
    public static void main(String[] args) throws IOException {
        new TsStat().go(args,3);
    }

    @Override public void goE() throws java.io.IOException {
        tsSize                   = getArgs().getDirectInt(0);
        final XFile inFileName   = getArgs().getDirectFile(1);
        final XFile outFileName  = getArgs().getDirectFile(2);

        // todo 
        tickSize = getArgs().getInt("tickSize", 100);
        separator = getArgs().getString("separator", "\t");
        filterTag = getArgs().getBool("filterTag", false);

        profileStart();

        PrintWriter w = IO.openPrintWriter(outFileName);
        TagAction tagStat = new TagAction(w);
        process(tagStat, inFileName);
        IO.close(w);

        profileEnd("Corpus processed in ");
    }

    /**
     * Action to be performed for each token (for the tag associated with the token).
     */
    class TagAction implements WordAction {
        /** */
        private final util.col.Counter<String> counter = new util.col.Counter<String>();
        /** */
        List<Pair<String,Integer>> posCounter = new ArrayList<Pair<String,Integer>>();
        /** */
        private int wordCounter = 0;
        /** number of seen tags on the previous tick */
        private int prevSeenTags = 0;


        private final PrintWriter w;

        TagAction(PrintWriter aW) {
            w = aW;
        }

        @Override public void init() {
            report();
        }

        @Override public boolean processWord(WordReader aR) {
            CorpusLineReader r = ((CorpusLineWordReader)aR).reader();
            String tag = r.tag().toString();

            // todo generalize (currently takes out subpos)
            if (filterTag) {
                tag = "" + tag.charAt(1);
            }

            // --- update counters ---
            counter.add(tag);

            int freq = counter.frequency(tag);
            posCounter.add(new Pair<String,Integer>(tag,freq));

            wordCounter++;

            // --- report if needed ---
            if (wordCounter % tickSize == 0) {
                report();
                prevSeenTags = counter.keySet().size();
            }


            return true;
        }

        private void report() {
            int seenTags = counter.keySet().size();

            // tick
            if (true) {
                w.print(wordCounter);
                w.print(separator);
            }

            // number of tags
            if (true) {
                w.print(seenTags);
                w.print(separator);
            }

            // tagset coverage
            if (true) {
                w.printf("%.3f", (seenTags*1.0) / (tsSize*1.0) );
                w.print( separator);
            }

            // accession
            if (true) {
                if (counter.size() == 0) {
                    w.print( "N/A");
                    w.print(separator);

                    w.print("1.0");
                    w.print(separator);
                }
                else {
                    w.print( seenTags-prevSeenTags );
                    w.print(separator);

                    w.printf("%.3f", ((seenTags-prevSeenTags)*1.0) / (tickSize*1.0) );
                    w.print(separator);
                }
            }

            // corpus coverage by k most freq tags
            if (true) {
                // TODO highly ineffective
                List<Map.Entry<String,Integer>> x = counter.sortedByFreq();
                // sum freq of first k entries
                int sum = 0;
                for (int i=0; i<5 && i < x.size(); i++) {
                    sum += x.get(i).getValue();
                }

                w.printf("%d", sum );
                w.print(separator);

                w.printf("%.3f", (sum*1.0) / (wordCounter*1.0) );
            }

            w.println();
        }

    }


    @Override protected CorpusLineWordReader openReader(XFile aFile) throws IOException {
        return new CorpusLineWordReader(Corpora.openM(aFile));
    }

}
