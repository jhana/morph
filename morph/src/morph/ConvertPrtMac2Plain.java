package morph;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.Arrays;
import morph.util.MUtilityBase;
import util.err.Err;
import util.io.IO;
import util.io.XFile;

/**
 * Converts Portuguese mac corpus to a single file in pdt format.
 * @author Jirka
 */
public class ConvertPrtMac2Plain extends MUtilityBase {
    
    public static void main(String[] args) throws IOException {
        new ConvertPrtMac2Plain().go(args);
    }
    
    private void go(String[] aArgs) throws IOException  {
        argumentBasics(aArgs,2);
        
        XFile inFile   = getArgs().getDirectFile(0);
        XFile outFile  = getArgs().getDirectFile(1);

        profileStart();
        go(inFile, outFile);
        profileEnd("Converted in ");
    }
    
    private void go(XFile aInFile, XFile aOutFile) throws IOException {
        LineNumberReader r = IO.openLineReader(aInFile);
        PrintWriter w = IO.openPrintWriter(aOutFile);
        
        for (int i = 0; ; i++) {
            String s = r.readLine();
            if (s == null) break;
            s = s.trim();
            if (s.length() == 0) continue;

            String[] ss = s.split("_+");

            Err.fAssert(ss.length == 2, "SS not 2 %s", Arrays.toString(ss));    // @todo warning?
            if (ss.length < 2) continue;
            
            w.printf("<f>%s<l>?<t>%s\n", ss[0], ss[1]);
        }
        
        r.close();
        w.close();
        
    }
}
