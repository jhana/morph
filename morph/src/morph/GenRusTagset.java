package morph;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import morph.util.MUtilityBase;
import util.Pair;
import util.col.Cols;
import util.io.IO;
import util.io.XFile;
import util.str.Strings;

/**
 *
 * @author Jirka Hana
 */
public class GenRusTagset extends MUtilityBase {

    /**
     *
     */
    public static void main(String[] args) throws IOException {
        new GenRusTagset().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,1);
        XFile outFile = getArgs().getDirectFile(0);
        process(outFile);
    }

    private void process(XFile outFile) throws IOException {
        setup();

        for (String template : ts) {
            processTemplate(template, 0);
        }


        Collections.sort(result);
        IO.writeLines(outFile, result);
    }

    /**
     * Recursivelly instantiate a template in all possible ways.
     * @param aTemplate
     * @param aSlot
     */
    private void processTemplate(String aTemplate, int aSlot) {
        // template is fully filled, save the result if it is legal
        if (aSlot >= 15) {
            // TODO check restrictions
            for (Pair<Pattern,Pattern> r : restr) {
                if (r.mFirst.matcher(aTemplate).matches() && !r.mSecond.matcher(aTemplate).matches()) return;
            }

            result.add(aTemplate);
            return;
        }

        if (aTemplate.charAt(aSlot) == '?') {
            String pref = aTemplate.substring(0, aSlot);
            String suff = aTemplate.substring(aSlot+1);
            // instantiate the variable in all possible way for that slot
            for (Character value : values.get(aSlot)) {
                processTemplate(pref + value + suff, aSlot+1);
            }
        }
        else {
            // this slot is not a varible, move on.
            processTemplate(aTemplate, aSlot+1);
        }
    }

    List<String> result = new ArrayList<String>();

    List<List<Character>> values = new ArrayList<List<Character>>(15);
    List<Pair<Pattern,Pattern>> restr = new ArrayList<Pair<Pattern,Pattern>>();
    List<String> ts  = new ArrayList<String>();


    private void setup() {
        // values
        addVals("");
        addVals("");
        addVals("FMNX");
        addVals("PSX");
        addVals("123467X");
        addVals("FMNX");
        addVals("PS");
        addVals("123");  // removed X for person
        addVals("FPRX");
        addVals("123");
        addVals("AN");
        addVals("AP");
        addVals("");
        addVals("");
        addVals("");

        // restrictions
        // only nouns distinguish gender in plural
        addRestriction("..[FNM]P.*", "N.*");

        //AG (long participle) => tv \in {PA,RA,XP}
        addRestriction("AG.*", "AG...---P-.A--.", "AG...---R-.A--.", "AG...---X-.P--.");

        //PP (personal pronoun)
        addRestriction("PP-.*",      "PP-..--[12]------.");
        addRestriction("PP[^\\-].*", "PP.....3------.");
        addRestriction("PP.....3.*", "....[^6].*");   // locative only after preposition
        addRestriction("P5.*", "....[^1].*");   // nominative only without preposition

        // 3rd person possessive pronouns
        addRestriction("PS.....3.*", "PSXXX[MFN]S3------.", "PSXXXXP3------.");

        // imperative only in 2n person
        addRestriction("Vi.*",  ".{7}2.*");

        // TODO check if AAXXX -  can have negation/degree

        // past tense
        addRestriction(8, 'R', "AG.*", "Vp.*");

        // X gender
        addRestriction(2, 'X',
            // no agr gender in plural
            "A.XP.*",
            "P[8DSqwz]XP.*",
            "PSXXX..3.*",
            "CnXP.*",    // !! TODO check
            "CrXP.*",
            "VpXP.*",

            // plural tantum noun
            "NNXP.*",

            // foreign/abbr
            "NNXXX.*",  // foreign, NNXXX-----A---8
            "AAXXX.*",

            // 3rd person pers. pronoun
            "P[P5]XP.--3.*"
            );

        // X number
        addRestriction(3, 'X',
            // foreign/abbr
            "NN.XX.*",  // foreign, NN[FMNX]XX-----A---8
            "AAXXX.*",
            "VB-X---XP.*",

            "PSXXX..3.*"
            );

        // X case
        addRestriction(4, 'X',
            // foreign/abbr
            "NN.[PSX]X.*",  // foreign, NN[FMNX][PSX]X-----A---8
            "AAXXX.*",

            "PSXXX..3.*"
         );

        // .......X.......
        // X possessor's gender
        addRestriction(5, 'X', "PSXXXXP3------.");
        
        // person .......X.......
        addRestriction(7, 'X',
            // foreign
            "VB-X---XP.*"
        );

        // tense 
        addRestriction(8, 'X', "AG...---X-.P--.");  // TODO check voice
        //                      AGFS1---X-AA---


        // --- templates ---
        ts.add("NN???-----?----");
        ts.add("AA???----??----");
        ts.add("AC??------?----");
        ts.add("AG???---?-??---");
        ts.add("AU????----?----");
        ts.add("Ac??------?P---");
        ts.add("PP-??--1-------");
        ts.add("PP-??--2-------");
        ts.add("PP???--3-------");
        ts.add("P5???--3-------");
        ts.add("PD???----------");
        ts.add("PW--?----------");
        ts.add("Pw???----------");
        ts.add("PS???-??-------");
        ts.add("PS?????3-------");
        ts.add("PQ--?----------");
        ts.add("Pq???----------");
        ts.add("PZ--?----------");
        ts.add("Pz???----------");
        ts.add("P6--?----------");
        ts.add("P8???----------");
        ts.add("C=-------------");
        ts.add("C}-------------");
        ts.add("Cn???----------");
        ts.add("Cn?-?----------");
        ts.add("Cn--?----------");
        ts.add("Cr???----------");
        ts.add("Cj--?----------");
        ts.add("Cu--?----------");
        ts.add("Ca--?----------");
        ts.add("Ca???----------");
        ts.add("Cv-------------");
        ts.add("VB-?---??------");
        ts.add("Ve-------------");
        ts.add("Vf-------------");
        ts.add("Vi-?---?-------");
        ts.add("Vm-------------");
        ts.add("Vp??----R------");
        ts.add("Db-------------");
        ts.add("Dg-------??----");
        ts.add("RR--?----------");
        ts.add("RV--?----------");
        ts.add("RF-------------");
        ts.add("J^-------------");
        ts.add("J,-------------");
        ts.add("TT-------------");
        ts.add("II-------------");
        ts.add("Z#-------------");
        ts.add("Z:-------------");
        ts.add("X0-------------");
        ts.add("XX-------------");


//        // abbreviations
//        ts.add("AAXXX----1A---8");
//        ts.add("Db------------8");
//        ts.add("J^------------8");
//        ts.add("NNFPX-----A---8");
//        ts.add("NNFSX-----A---8");
//        ts.add("NNFXX-----A---8");
//        ts.add("NNMPX-----A---8");
//        ts.add("NNMSX-----A---8");
//        ts.add("NNMXX-----A---8");
//        ts.add("NNNPX-----A---8");
//        ts.add("NNNSX-----A---8");
//        ts.add("NNNXX-----A---8");
//        ts.add("NNXXX-----A---8");
    }

    private void addVals(String aVals) {
        values.add(Strings.toList(aVals));
    }

    private void addRestriction(int aSlot, char aVal, String ... aReqs) {
        String ctx = ".{" + aSlot + "}" + aVal + ".*";
        addRestriction(ctx, aReqs);
    }

    private void addRestriction(String aCtx, String ... aReqs) {
        addRestriction(aCtx, Cols.toString(Arrays.asList(aReqs), "(", ")", "|", ""));
    }

    private void addRestriction(String aCtx, String aReq) {
        Pattern ctx = Pattern.compile("^" + aCtx + "$");
        Pattern req = Pattern.compile("^" + aReq + "$");

        restr.add(new Pair<Pattern,Pattern>(ctx, req));
    }
}

