package morph;

import util.io.*;
import java.io.*;
import java.util.Collections;
import lombok.Cleanup;import java.util.HashSet;

import java.util.Set;
import morph.io.Corpora;

import morph.io.CorpusLineReader;
import morph.io.CorpusWriter;
import morph.io.PdtWriter;
import morph.io.TntWriter;
import morph.ma.lts.Lts;
import morph.ts.Tagset;
import morph.util.MUtilityBase;
import util.ColIo;
import util.col.Cols;
import util.col.MultiMap;
import util.err.Err;
import util.io.IO;
import util.str.Strings;

/**
 * Translates a tagset into another tagset. Currently works only for disambiguated corpora.
 * 
 * @todo allow regex rules taking into accocunt forms
 * @todo For n:1 translations, can output a "memory" file to help with back translation
 */
public class TagTransl extends MUtilityBase {

    private MultiMap<String, String> tagTranslMap; // = new MultiMap<String, String>();

    public static void main(String[] args) throws IOException {
        new TagTransl().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs, 3);

        XFile inFile = getArgs().getDirectFile(0, cCsts);
        XFile tagTranslFile = getArgs().getDirectFile(1);
        XFile outFile = getArgs().getDirectFile(2, cCsts);
        
        tagTranslMap = new ColIo(tagTranslFile).readInMultiMap(Strings.cWhitespacePattern);
        System.out.println("================\n\n");
        System.out.println(tagTranslMap.get("VpQW---XR-AA---"));
        
        translate(inFile, outFile);
    }


    private void translate(XFile aInFile, XFile aOutFile) throws IOException {
        @Cleanup CorpusLineReader r = Corpora.openM(aInFile)
                .configure()
                //.setTagset(new Tagset.TagsetBuilder().build())
                .setOnlyTokenLines(false);
                
        @Cleanup CorpusWriter w = openWriter(aOutFile);

        boolean outputNonToken = (aInFile.format() == aOutFile.format());

        for (;;) {
            if (!r.readLine()) break;

            if (r.isTokenLine()) {
                final String form = r.form();
                final String lemma = r.lemma();
                final String tag = r.tag().toString();

                final Lts lts = new Lts();
                for (String t : translate(tag)) {
                    if (!dropTag(form, lemma, t)) {
                        lts.add(lemma, Tagset.getDef().fromString(t), null, null);      // todo configure tagset
                    }
                }
                w.writeLine(form, lts);
            }
            else if (outputNonToken) {
                w.writeRaw(r.line());
            }
        }
    }

    /**
     * Can be overriden to filter out tags.
     *
     * @param form
     * @param lemma
     * @param tag
     * @return
     */
    protected boolean dropTag(String form, String lemma, String tag) {
        return false;
    }

    private Iterable<String> translate(String aTag) throws IOException {
        // try direct translation first
        final Set<String> newTags = tagTranslMap.get(aTag);
        if (newTags != null) return newTags;
        
        // try prefix translation
        final String prefix = aTag.substring(0, 2);
        final Set<String> newPrefixes = tagTranslMap.get(prefix);
        
        if (newPrefixes != null) {
            final Set<String> tags = new HashSet<String>();
            for (String newPrefix : newPrefixes) {
                tags.add( newPrefix + aTag.substring(2) );
            }
            return tags;
        }
        
        // return original tag
        return Collections.singletonList(aTag);     // todo use CreateTsMap as a backup (an unknown tag might be a result of russification.)
    }

    // todo replace with Corpora.openWriter + setLemmaTagCore("l")  (i kdyz by to asi melo kopirovat core z inputu)
    protected CorpusWriter openWriter(XFile aOutFile) throws IOException {
        if (aOutFile.format() == MUtilityBase.cTnt) {
            return new TntWriter(aOutFile);
        }
        else if (aOutFile.format() == MUtilityBase.cCsts) {
            return new PdtWriter(aOutFile).setLemmaTagCore("l").optionsUpdated();
        }
        else {
            Err.fAssert(false, "Format " + aOutFile.format() + " is not supported");
            return null;
        }
    }
    
}