package morph;

import java.io.*;
import lombok.Cleanup;
import util.io.*;
import morph.io.*;
import morph.util.MUtilityBase;
import util.err.Err;



/**
 * @share with LexForTNT
 */
public class MAForVoting extends MUtilityBase {

    /**
     *
     */
    public static void main(String[] args) throws IOException {
        new MAForVoting().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        XFile maFile = getArgs().getDirectFile(0);
        XFile out    = getArgs().getDirectFile(1);
        Err.checkIFiles("ma", maFile);
        Err.checkOFiles("out", out);
        transform(maFile, out);
    }

    protected String helpString() {
        return "java X  {maFile} {output}\n\n";
    }

    /**
     * Compares the specified GS and tagged file
     * @todo formats
     */
    void transform(XFile aMAFile, XFile aOutFile) throws java.io.IOException {
        @Cleanup CorpusLineReader in = new PdtLineReader(aMAFile).setMm(true);
        CorpusWriter out      = new TntWriter(aOutFile);

        for(;;) {
            if (!in.readLine()) break;
            out.writeLine(in.form(), in.tags());
        }

        out.close();
    }
}