package morph.cog;

import java.io.IOException;
import java.io.PrintWriter;
import lombok.Cleanup;
import morph.io.Corpora;
import morph.io.CorpusLineReader;
import morph.io.TntWriter;
import morph.util.MUtilityBase;
import util.io.IO;
import util.io.XFile;
import util.io.ezreader.EzReader;
import util.str.StringPair;
import util.str.transl.ReplaceTranslator;
import util.str.transl.Translator;
import util.str.transl.Translators;

/**
 * Note: all upper-case letters are automatically translated to lower-case first
 * @todo output tnt only; use CorpusTranslation
 * @author Administrator
 */
public class CogTransl extends MUtilityBase {
    public static void main(String[] args) throws IOException {
        new CogTransl().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,4);

        XFile inFile         = getArgs().getDirectFile(0);  // in: anal
        XFile translSpecFile = getArgs().getDirectFile(1);  // in: transl spec
        XFile outFile        = getArgs().getDirectFile(2);  // out: anal'
        XFile dicFile        = getArgs().getDirectFile(3);  // out: dic for back translation

        // todo: read in translSpec
        translator = readInTranslSpec(translSpecFile);

        translate(inFile, outFile, dicFile);
    }

    private Translator readInTranslSpec(XFile aTranslSpecFile) throws IOException {
        EzReader r = new EzReader(aTranslSpecFile);
        r.configureSplitting("string to replace", "replacement");
        
        Translators trs = new Translators();
        for (StringPair pair : r.readPairs()) {
            if (pair.mSecond.equals("null")) pair.mSecond = "";
             trs.add(new ReplaceTranslator(pair.mFirst, pair.mSecond));
        }

        return trs;
    }



    private void translate(XFile inFile, XFile outFile, XFile dicFile) throws IOException {
        @Cleanup CorpusLineReader r = Corpora.openMm(inFile);
        @Cleanup TntWriter w = new TntWriter(outFile);
        @Cleanup PrintWriter dic = IO.openPrintWriter(dicFile);

        for (;;) {
            if (!r.readLine()) break;
            String newForm = translate(r.form());
            if (!r.form().equals(newForm)) {
                dic.println(r.form() + " " + newForm);
            }

            w.writeLine(newForm, r.tags());
        }
    }

    Translator translator;

    private String translate(String aWord) {
        return translator.translate(aWord.toLowerCase());
    }


}
