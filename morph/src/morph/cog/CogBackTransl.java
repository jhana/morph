package morph.cog;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import morph.util.MUtilityBase;
import util.col.MultiMap;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;
import util.io.ezreader.EzReader;
import util.str.StringPair;
import util.str.Strings;

/**
 *
 * @author Administrator
 */
public class CogBackTransl extends MUtilityBase {
    public static void main(String[] args) throws IOException {
        new CogBackTransl().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,4);

        XFile inFile     = getArgs().getDirectFile(0);
        XFile sLgDicFile = getArgs().getDirectFile(1);
        XFile tLgDicFile = getArgs().getDirectFile(2);
        XFile outFile    = getArgs().getDirectFile(3);

        MultiMap<String,String> srcLgDic    = readInDic(sLgDicFile);
        MultiMap<String,String> targetLgDic = readInDic(tLgDicFile);

        translate(inFile, srcLgDic, targetLgDic, outFile);
    }

    private MultiMap<String, String> readInDic(XFile aLgDicFile) throws IOException {
        EzReader r = new EzReader(aLgDicFile).configureSplitting("orig-word", "translated-word");

        final MultiMap<String, String> dic = new MultiMap<String,String>();
        for (StringPair pair : r.readPairs()) {
            dic.add(pair.mSecond, pair.mFirst);
        }

        return dic;
    }


    private void translate(XFile aInFile, MultiMap<String, String> aSrcLgDic, MultiMap<String, String> aTargetLgDic, XFile aOutFile) throws IOException {
        LineReader r = IO.openLineReader(aInFile);
        r.configureSplitting(Strings.cWhitespacePattern, 3, "Format: src-word target-word score");

        PrintWriter w = IO.openPrintWriter(aOutFile);

        try {
            for (;;) {
                String[] strs = r.readSplitNonEmptyLine();
                if (strs == null) break;

                String src = strs[0];
                String target = strs[1];
                double score = Double.valueOf(strs[2]);

                translate(w, src, target, score, aSrcLgDic, aTargetLgDic);
            }
        }
        finally {
            IO.close(r,w);
        }
    }

    private void translate(PrintWriter w, String src, String target, double score, MultiMap<String, String> aSrcLgDic, MultiMap<String, String> aTargetLgDic) {

        for (String newSrc : translate(aSrcLgDic, src)) {
            for (String newTarget : translate(aTargetLgDic, target)) {
                w.println(newSrc + " " + newTarget + " " + score);
            }

        }


    }

    private Iterable<String> translate(MultiMap<String, String> aDic, String aWord) {
        Collection<String> newWords = aDic.get(aWord);
        if (newWords == null) {
            newWords = Arrays.asList(aWord);
        }
        return newWords;
    }
}
