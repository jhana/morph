package morph.cog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import morph.util.MUtilityBase;
import util.PairC;
import util.str.Strings;
import util.io.IO;
import util.io.XFile;

/**
 * Edit distance with configurable substitution distances and possibility of handling digraphs as single units
 * 
 * @author 
 */
public final class Simple extends MUtilityBase {
    private int mCutoff = 5000;

    public static void main(String[] aArgs) throws IOException {
        new Simple().go(aArgs);
    }
    
    public void go(String[] aArgs) throws IOException {
        argumentBasics(aArgs,4);
        
        XFile inAFile  = getArgs().getDirectFile(0, cPlain);
        XFile inBFile  = getArgs().getDirectFile(1, cPlain);
        XFile outFile  = getArgs().getDirectFile(2, cPlain);
        XFile distFile = getArgs().getDirectFile(3, cPlain);
        
        loadDistances(distFile);
        
        List<String> aStrings = readStrings(inAFile);
        List<String> bStrings = readStrings(inBFile);

        PrintWriter w = IO.openPrintWriter(outFile);
        PrintWriter minW = IO.openPrintWriter(outFile.addExtension("min"));
        
        List<Pair> block = new ArrayList<Pair>();
        
        profileStart();
        int i = 0;
        for(String aString : aStrings) {
            if ((i++ % 500) == 0) System.out.print(".");
            
            int minDistance = Integer.MAX_VALUE;
            block.clear();
            
            for(String bString : bStrings) {
                if ( Math.abs(aString.length() - bString.length()) < 2 ) {
                    int distance = distance(aString, bString);
                    distance = normalize(aString, bString, distance);
                    block.add(new Pair(aString, bString, distance));
                    
                    if (minDistance > distance) minDistance = distance;
                }
            }
            writeBlock(w, minW, block, minDistance);
        }
        profileEnd("Analyzed in %f s");
        
        w.close();
        minW.close();
    }
    
// =============================================================================    
// Distance
// =============================================================================    
    
    /**
     * Calculates the distance between Strings x and y using the
     * <b>Dynamic Programming</b> algorithm.
     */
    private int distance(String x, String y) {
        int m = x.length();
        int n = y.length();
        
        int[][] T = new int[m + 1][n + 1];
        
        T[0][0] = 0;
        for (int j = 0; j < n; j++) {
            T[0][j + 1] = T[0][j] + ins(y, j);
        }
        for (int i = 0; i < m; i++) {
            T[i + 1][0] = T[i][0] + del(x, i);
            for (int j = 0; j < n; j++) {
                T[i + 1][j + 1] =  min( T[i][j] + sub(x, i, y, j), T[i][j + 1] + del(x, i), T[i + 1][j] + ins(y, j) );
                //T[i + 1][j + 1] =  min( T[i][j] + sub(x, i, y, j), T[i][j + 1] + 100, T[i + 1][j] + 100 );
            }
        }
        
        return T[m][n];
    }
    
    //    private int sub(String x, int xi, String y, int yi) {
    //        return x.charAt(xi) == y.charAt(yi) ? 0 : 1;
    //    }

    private int sub(String x, int xi, String y, int yi) {
        return x.charAt(xi) == y.charAt(yi) ? 0 : distance( x.charAt(xi), y.charAt(yi) );
    }
    
    
    private int ins(String x, int xi) {
        return (xi<2) ? 200 : 100;
    }
    private int del(String x, int xi) {
        return (xi<2) ? 200 : 100;
    }
    private int min(int a, int b, int c) {
        return Math.min(Math.min(a, b), c);
    }
    
    private int normalize(String czword, String ruword, int mydistance) {
        int longerLen = Math.max(czword.length(), ruword.length()); 

        return Math.round(mydistance*100/longerLen);
    }

// =============================================================================    
// Reading strings to compare, handling digraphs 
// =============================================================================    
    
    private List<String> readStrings(XFile aFile) throws IOException {
        TreeSet<String> strings = new TreeSet<String>();
        
        BufferedReader r = new BufferedReader(IO.openReader(aFile));
        for(;;) {
            String str = r.readLine();
            if (str == null) break;
            
            strings.add( str.trim().toLowerCase() );
        }
        r.close();
        return new ArrayList<String>(strings);
    }

    private String removeDigraphs(String aStr, Map<String,Character> aMap) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < aStr.length(); i++) {
            PairC<Character,Integer> transl = findTranslation(aStr,i, aMap);
            if (transl == null) {
                sb.append(aStr.charAt(i));
            }
            else {
                sb.append( transl.mFirst);
                i+=(transl.mSecond-1);
            }
        }
                
        return sb.toString();
    } 
    
    private PairC<Character,Integer> findTranslation(String aStr, int aIdx, Map<String,Character> aMap) {
        for (Map.Entry<String,Character> e : aMap.entrySet()) {
            if (aStr.startsWith(e.getKey(), aIdx)) {
                return new PairC<Character,Integer>(e.getValue(), e.getKey().length());
            }
        }
        return null;
    }
    
// =============================================================================    
// Write output
// =============================================================================    

    private void writeBlock(PrintWriter aW, PrintWriter aMinW, List<Pair> aBlock, int minDistance) {
        for (Pair pair : aBlock) {
            if (pair.mDistance < mCutoff) {
                aW.print(pair.mAString + " " + pair.mBString + " " + pair.mDistance);
                if (pair.mDistance == minDistance) {
                    aW.print(" *");
                    aMinW.println(pair.mAString + " " + pair.mBString + " " + pair.mDistance);
                }
                aW.println();
            }
        }
    }
            
    class Pair {
        final String mAString;
        final String mBString;
        final int mDistance;
        
        Pair(String aAString, String aBString, int aDistance) {
            mAString = aAString;
            mBString = aBString;
            mDistance = aDistance;
        }
    }
    
// =============================================================================    
// Sub distances
// =============================================================================    

    Map<Character, Map<Character, Integer>> mSubDistances;

    private int distance(char aA, char aB) {
        Map<Character, Integer> aDistances = mSubDistances.get(aA);
        if (aDistances == null) return 100;
        
        Integer abDistance = aDistances.get(aB);
        return (abDistance == null) ? 100 : abDistance;
    }
    
    private void loadDistances(XFile aFile) throws IOException {

        mSubDistances = new HashMap<Character, Map<Character, Integer>>();
        
        BufferedReader r = new BufferedReader(IO.openReader(aFile));
        for(;;) {
            String str = r.readLine();
            if (str == null) break;
            str = str.trim();
            if (str.length() == 0) continue;
            
            String[] strs = Strings.split(str);
            try {
                addDistance( strs[0].charAt(0), strs[1].charAt(0), Integer.valueOf(strs[2]) ); 
                addDistance( strs[1].charAt(0), strs[0].charAt(0), Integer.valueOf(strs[2]) ); 
            }
            catch( RuntimeException e) {
                System.out.println("Error at line " + str);
                throw e;
            }
        }
        r.close();
    }

    private void addDistance(char aA, char aB, int aDistance) {
        Map<Character, Integer> aDistances = mSubDistances.get(aA);
        if (aDistances == null) {
            aDistances = new HashMap<Character,Integer>();
            mSubDistances.put(aA, aDistances);
        }
        
        aDistances.put(aB, aDistance);
    }
    

}

