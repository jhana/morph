package morph;

/**
 *
 * @author Administrator
 */
public class RusSciTranslit extends Translit{

    @Override
    public String transliterateTbl(char aC) {
        switch (aC) {
            case '\u0430': return "a";
            case '\u0431': return "b";
            case '\u0432': return "v";
            case '\u0433': return "g";
            case '\u0434': return "d";
            case '\u0435': return "e";
            case '\u0436': return "\u017E"; // \v{z}
            case '\u0437': return "z";
            case '\u0438': return "i";
            case '\u0439': return "j";
            case '\u043A': return "k";
            case '\u043B': return "l";
            case '\u043C': return "m";
            case '\u043D': return "n";
            case '\u043E': return "o";
            case '\u043F': return "p";
            case '\u0440': return "r";
            case '\u0441': return "s";
            case '\u0442': return "t";
            case '\u0443': return "u";
            case '\u0444': return "f";
            case '\u0445': return "x";
            case '\u0446': return "c";
            case '\u0447': return "\u010D"; // \v{c}
            case '\u0448': return "\u0161"; // \v{s}
            case '\u0449': return "\u0161\u010D"; // \v{s}\v{c}
            case '\u044A': return "\"";
            case '\u044B': return "y";
            case '\u044C': return "'";
            case '\u044D': return "3";
            case '\u044E': return "ju";
            case '\u044F': return "ja";
            case '\u0451': return "jo";

            default: return String.valueOf(aC);
        }
    }

}
