package morph;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import lombok.Cleanup;
import morph.ma.Morph;
import morph.ma.MorphCompiler;
import morph.ma.module.Module;
import morph.ma.module.WordEntry;
import morph.ma.module.WordModule;
import morph.util.MUtilityBase;
import util.col.MultiMap;
import util.err.Log;
import util.io.IO;
import util.io.XFile;

/**
 * Loads words specification and saves it in the format
 * form lemma tags
 *
 * @author j
 */
public class FlattenWords extends MUtilityBase {
    /**
     * Morphological analyzer
     */
    protected Morph mMorph;

    public static void main(String[] args) throws IOException {
        new FlattenWords().go(args,1);
        Log.setILevel(Level.ALL);  // @todo something better
    }

    /**
     *
     * @param aArgs
     * @throws java.io.IOException
     */
    @Override
    public void goE() throws java.io.IOException {

        // --- set up the morph object ---
        if (!setupMorph()) return;
        System.out.println("Loaded");
        System.out.println( mMorph.getModules() );
        MultiMap<String,WordEntry> entries = collectWords();
        dumpWords(entries);
    }


    private boolean setupMorph() throws IOException {
        mMorph = MorphCompiler.factory().compile(getArgs());
        return (mMorph != null);
    }

    private MultiMap<String,WordEntry> collectWords() {
        MultiMap<String,WordEntry> form2entry = new MultiMap<String,WordEntry>();
        for (Module module : mMorph.getModulesList()) {
            System.out.println("module: " + module);
            if (module instanceof WordModule) {
                System.out.println("wordModule: " + module);
                for (WordEntry entry : ((WordModule)module).getEntries().allValues()) {
                    form2entry.add(entry.getForm(), entry);
                }
            }
        }
        return form2entry;
    }

    private void dumpWords(MultiMap<String, WordEntry> form2entry) throws IOException {
        XFile outFile = getArgs().getDirectFile(0);
        @Cleanup PrintWriter w = IO.openPrintWriter(outFile);

        List<WordEntry> entries = com.google.common.collect.Lists.newArrayList(form2entry.allValues());
        Collections.sort(entries, WordEntry.lftComparator);

        for (WordEntry e : entries) {
            w.printf("%s\t%s\t%s\n", e.getForm(), e.getLemma(), e.getTag());   // todo
        }
    }
}

