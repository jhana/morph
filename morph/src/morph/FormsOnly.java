package morph;

import morph.io.*;
import util.io.*;
import java.io.*;
import lombok.Cleanup;
import morph.util.MUtilityBase;
import util.io.IO;

/**
 * @todo in/out encoding
 */
public class FormsOnly extends MUtilityBase {

    /**
     *
     */
    public static void main(String[] args) throws IOException {
        new FormsOnly().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        XFile inFile = getArgs().getDirectFile(0);
        XFile outFile = getArgs().getDirectFile(1);

        process(inFile, outFile);
    }


   /**
     *
     */
    void process(XFile aInFile, XFile aOutFile) throws java.io.IOException {
        @Cleanup WordReader in = Corpora.openWordReader(aInFile);
        @Cleanup PrintWriter out = IO.openPrintWriter(aOutFile);

        for(;;) {
            if (!in.nextWord()) break;
            out.println(in.word());
        }
    }
}