package morph;

import util.str.Strings;
import java.io.*;
import java.util.*;
import lombok.Cleanup;
import util.io.*;
import morph.io.*;
import morph.ma.lts.Fplus;
import morph.ma.lts.Lt;
import morph.ma.lts.Lts;
import morph.util.*;
import morph.ts.Tag;
import util.col.Cols;
import util.col.MappingList;
import util.col.MultiMap;
import util.err.Log;
import util.str.Caps;

/**
 * Collects analyses for the specified words from a PDT or TNT corpus.
 * For a tnt file all lemmas are assumed to be the same as the forms.
 * Lemmas are translated to pure lemmas.
 * @todo make it possible to specify source of analyses in PDT (MMl, l, ...)
 * @todo capitalization !!!!!!
 */
public class CollectLex extends MUtilityBase {
//    private MultiMap<String,PairC<String,Tag>> mLexicon;    // form -> {lemma x tag}
    private MultiMap<String,Lt> mLexicon = new MultiMap<String,Lt>();    // form -> {lemma x tag}

    private List<String> pruneWordsList;
    private Set<String>  pruneWordsSet;

    /**
     *
     */
    public static void main(String[] args) throws IOException {
        new CollectLex().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);

        XFile inFile    = getArgs().getDirectFile(0);
        XFile outFile   = getArgs().getDirectFile(1);
        XFile pruneFile = getArgs().getFile("pruneFile", null);

        if (pruneFile != null)
            loadPruneFile(pruneFile);

        collectAnalyses(inFile);

        writeLex(outFile);
    }

    /*
     * Loads prune file
     *
     */
    void loadPruneFile(XFile aPruneFile) throws java.io.IOException {
        System.out.println("Loading prune file");
        @Cleanup LineNumberReader in = IO.openLineReader(aPruneFile);

        final int pW = getArgs().getInt("pruneWordsN", Integer.MAX_VALUE);

        pruneWordsList = new ArrayList<String>(Math.max(100000, pW));
        for(int words = 0; words < pW; words++) {
            String word = in.readLine();
            if (word == null) break;
            pruneWordsList.add( Strings.split(word)[0].trim() );   // todo why trim, the white spaces should remove it
        }
        System.out.println("Prune Words List size = " + pruneWordsList.size());

        pruneWordsSet = new TreeSet<String>(pruneWordsList);
    }


    /**
     *
     */
    void collectAnalyses(XFile aInFile) throws java.io.IOException {
        @Cleanup CorpusLineReader in = getArgs().getBool("input.mm") ? Corpora.openMm(aInFile) : Corpora.openM(aInFile);

        Set<String> weirdFs = new HashSet<String>();

        int j;
        for(j=0;;j++) {
            if (!in.readLine()) break;

            if ( pruneWordsSet == null || pruneWordsSet.contains(in.form().toLowerCase()) ) {
//                String lemma = PDTX.pureLemma(in.lemma());   // @todo this is too specific
//                String form = Caps.capitalizeBy(in.form(), lemma);
//
//                // For pdt: report nonstandard forms
//                if (in.line().startsWith("<f ")) {
//                    String fTag = util.sgml.Sgml.getFirstTag(in.line(), 0);
//                    if (fTag.contains("gen")) {
//                        weirdFs.add(in.form() + " : " + in.lemma() + " : " + in.tag() );
//                    }
//                }

                Fplus fplus = in.getFPlus();
                String form = fplus.getForm();
                if (form.startsWith("Aaron")) System.out.println("Aaron: xxxx " + fplus);
                for (String lemma : fplus.getLts().lemmas()) {
                    //String simpleLemma = PDTX.pureLemma(lemma);
                    form = Caps.capitalizeBy(form, lemma);

                    for (Lt lt : fplus.getLts().col(lemma)) {
                        if (form.startsWith("Aaron")) System.out.println("Aaron: " + lt);
                        mLexicon.add(form, lt);
                    }
                }
            }
            else {

            }
            Log.dot(j);
        }
        System.out.println("Lines: " + j);
        System.out.println("Analyzes: " + mLexicon.size());
        //System.out.println(Cols.toStringNl(weirdFs));
    }



    void writeLex(XFile aLexFile) throws java.io.IOException {
        @Cleanup PrintWriter outLex  = IO.openPrintWriter(aLexFile);

        // todo write header

        // sort by prune file or alphabetically
        if (pruneWordsList != null && mArgs.getBool("sortByPruneFile")) {
            System.out.println("Sorting by pruning file...");
            int missing = 0;
            int forms = 0;
            for( String form : pruneWordsList ) {
                forms++;
                //Set<PairC<String,Tag>> lts = mLexicon.getS(form);
                Set<Lt> lts = mLexicon.getS(form);
                if (lts.isEmpty()) {
                    outLex.println("/// " + form);
                    missing ++;
                }
                else {
                    writeEntry(outLex, form, lts);
                }
            }
            System.out.println("Missing entries = " + missing);
            System.out.println("Forms = " + forms);
        }
        else {
            Set<String> sortedForms = new TreeSet<String>(mLexicon.keySet());
            System.out.println("Entries: " + sortedForms.size());
            for( String form : sortedForms ) {
                writeEntry(outLex, form, mLexicon.getS(form));
            }
        }

        System.out.println("Lines = " + lines);
    }

    int lines = 0;

    private void writeEntry(PrintWriter aOutLex, String aForm, Set<Lt> aLts) {
        Lts lts = new Lts(aLts);
        final Set<String> lemmas = lts.lemmas();
        if (lemmas.isEmpty()) {
            System.out.println("XX: " + aForm + " : " + aLts);
        }

        for (String lemma : lemmas) {
            String simplifiedLemma = PDTX.pureLemma(lemma);
            lines++;
            final List<Tag> tags = new MappingList<Lt,Tag>(lts.col(lemma), Lt.cTagMapper);
            String tagStr = Cols.toString(tags, "", "", " ", "");
            tagStr = tagStr.replace("X@-------------", "");
            tagStr = tagStr.trim();
            if (tagStr.isEmpty()) continue;

            if (simplifiedLemma.equals(aForm) || simplifiedLemma.equals("")) {
                aOutLex.println(aForm + "\t" + tagStr);
            } else {
                aOutLex.printf("%s\t@ %s\t%s\n", aForm, simplifiedLemma, tagStr);
            }
        }
    }
}
