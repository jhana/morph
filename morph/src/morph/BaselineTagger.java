package morph;

import java.io.*;
import java.util.*;
import lombok.Cleanup;
import morph.io.PdtWriter;
import morph.io.PdtLineReader;
import util.io.XFile;
import morph.ts.Tag;
import morph.util.MUtilityBase;
import util.col.Counter;
import util.err.Log;


/**
 * Random or unigram tagger
 */
public class BaselineTagger extends MUtilityBase {

    /**
     * Use unigram or random tagger?
     */
    boolean mUseUniTagger;

    /**
     * Unigram model
     */
    Counter<Tag> mCounter;

    public static void main(String[] args) throws IOException {
        new BaselineTagger().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        
        XFile trainingFile  = getArgs().getFile("unigram", null);  // training file
        mUseUniTagger = (trainingFile != null);

        if (mUseUniTagger) {
            mCounter = new Counter<Tag>();
            buildModel(trainingFile);
        }
        
        XFile inFile  = getArgs().getDirectFile(0);  // "ma.txt"
        XFile outFile = getArgs().getDirectFile(1);  // "tagged.txt";

        tag(inFile, outFile);
        Log.info("Done: %s -> %s", inFile, outFile);
    }

    /**
     * Build unigram model from the specified golden standard
     */
    public  void buildModel(XFile aGSFile) throws java.io.IOException {
        PdtLineReader gs = new PdtLineReader(aGSFile);

        for(;;) {
            if(!gs.readLine()) break;

            mCounter.add(gs.tag());
        }
    }
                
    /**
     * Tag a morphologically annotated file using the unigram model
     */
    public void tag(XFile aInFile, XFile aOutFile) throws java.io.IOException {
        @Cleanup PdtLineReader in = new PdtLineReader(aInFile).setOnlyTokenLines(false).setMm(true);
        @Cleanup PdtWriter    out = new PdtWriter(aOutFile);
        out.setOption(PdtWriter.cOptLemmaTag, "MMl");
        
        for(;;) {
            if(!in.readLine()) break;

            if ( in.lineType().fd() ) {
                Set<Tag> tags = in.tags();

                Tag bestTag;
                if (mUseUniTagger) {
                    bestTag = mCounter.max(tags); 
                }
                else { // --- random ---
                    int bestTagI = (int)Math.floor(Math.random()*tags.size()); 
                    List<Tag> tagsL = new ArrayList<Tag>(tags);
                    bestTag = tagsL.get(bestTagI);
                }

                String bestLemma = in.getFirstLemmaOf(bestTag);
                
                morph.ma.lts.Lts lt = new morph.ma.lts.Lts();
                lt.add(bestLemma, bestTag, "baseline", null);
                
                out.writeLine(in.form(), lt);
            }
            else
                out.writeLine(in.line());
        }

    }
}