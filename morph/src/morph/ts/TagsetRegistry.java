package morph.ts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import util.err.Err;
import util.opt.Options;

/**
 *
 * @author jirka
 */
public class TagsetRegistry {
    /** default instance (created by the class loader, so no synchronization needed) */
    private static TagsetRegistry instance = new TagsetRegistry();

    public static TagsetRegistry getDef() {
        return instance;
    }

    @Getter private final Map<String,Tagset> tagsets = new HashMap<String, Tagset>();
    @Getter @Setter private Tagset defTs;;

    public Tagset getTagset(String aId) {
        return tagsets.get(aId);   
    }

    public void addTagset(Tagset aTagset) {
        tagsets.put(aTagset.getId(), aTagset);
    }

    public void load(final Options aOptions) {
        List<String> tagsetids = aOptions.getStrings("ids");
        if (tagsetids == null) return;
        
        for (String id : tagsetids) {
            Tagset ts = new Tagset.TagsetBuilder().setId(id).setOptions(aOptions.getSubtree(id)).build();
            addTagset(ts);
        }
       
        final String defTsId = aOptions.getString("def");  // default tagset
        Err.fAssert(defTsId != null, "No default tagset specified (use def=<id>");
        
        final Tagset ts = getTagset(defTsId);
        Err.fAssert(ts != null, "Cannot set default tagset. there is not tagset with the id='%s'", defTsId);
        setDefTs(ts);
    }
    
    
//    public List<Tagset> getTagsets(String aProperty, String aValue) {
//        final List<Tagset> tss = new ArrayList<Tagset>();
//
//        for (Tagset ts : tagsets.values()) {
//            if ( aValue.equals( ts.getProperties().get(aProperty) )) {
//                tss.add(ts);
//            }
//        }
//
//        return tss;
//    }

} 