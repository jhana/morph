package morph.ts;

import java.util.ArrayList;
import java.util.List;

/**
 * Immutable vector of bools (bits) corresponding to slots in a tag.
 * Each component of the booltag has a <code>boolean</code> value. The
 * mBits of a <code>BoolTag</code> are indexed by nonnegative integers.
 * Individual indexed mBits can be examined, set, or cleared. 
 * A <code>BoolTag</code> can be modified by logical NOT, AND, OR, XOR, NAND.
 */
public final class BoolTag {
    public final static String cAllStr = "all";
    public final static String cNoneStr = "none";

    
    private final Tagset tagset;
    
    /**
     * The mBits in this BoolTag.  
     * Invariant: Slots getLen()..31 are always 0.
     */
    private final int mBits;
    
// --------------------------------------------------------------------------------------------
// Collections
// --------------------------------------------------------------------------------------------
    /**
     * Fills an array to a required length.
     * @param aTags
     * @param aReqLen
     * @param aFill
     * @return 
     */
    public static <T> List<T> fill(final List<T> aTags, final int aReqLen, final T aFill) {
        if (aTags.size() >= aReqLen) return aTags;
        
        final List<T> tags = new ArrayList<T>(aTags);
        for (int i = 0; i < aReqLen-aTags.size(); i++) {
            tags.add(aFill);
        }
        return aTags;
    }

    public static List<String> toCodeStrings(final List<BoolTag> aTags) {
        final List<String> codeStrings = new ArrayList<String>();
        for (BoolTag boolTag : aTags) {
            codeStrings.add(boolTag.toCodeString());
        }
        
        return codeStrings;
    }
    
    
// --------------------------------------------------------------------------------------------
// 
// --------------------------------------------------------------------------------------------
    /*
     * To prevent use of the constructor
     */
    BoolTag(Tagset aTagset) {
        this(aTagset, 0);
    }


    /*
     * Used internally to create the object.
     */
    private BoolTag(Tagset aTagset, int aBits) {
        tagset = aTagset;
        mBits = aBits;
    }

    public int getLen() {
        return tagset.getLen();
    }
    
// -----------------------------------------------------------------------------    
// Code string, slots
// -----------------------------------------------------------------------------    

    // todo cache
    public List<Integer> toSlotsIdxs() {
        final List<Integer> slotIdxs = new ArrayList<Integer>(getLen());
        for (int i = 0; i < getLen(); i++) {
            if (get(i)) slotIdxs.add( i );
        }
        return slotIdxs;
    }

    
    /**
     * Returns code string representation of this booltag. 
     * Unlike toCodeString, this function does return
     * all, resp. none, for fully set, resp. fully cleared, booltag.
     *
     * @see toCodeString
     */
    public String toCodeStringHF() {
        if (hasAllSet()) {
            return cAllStr;
        }
        if (hasAllCleared()) {
            return cNoneStr;
        }
        return toCodeString();
    }

    /**
     * Returns code string representation of this booltag. 
     * Unlike toCodeStringHF, this function does not return
     * all, resp. none, for fully set, resp. fully cleared, booltag.
     *
     * @see toCodeStringHF
     */
    public String toCodeString() {
        final StringBuilder sb = new StringBuilder(getLen());

        for (int i = 0; i < getLen(); i++) {
            if (get(i)) {
                sb.append(tagset.slotCode(i));
            }
        }
        return sb.toString();
    }
// --------------------------------------------------------------------------------------------
// set, get
// --------------------------------------------------------------------------------------------
    /** 
     * Returns the value of the bit of the specified slot. 
     *
     * @param  aSlot   slot index of the bit to be returned. Must be in <0,14> (not checked)
     * @return the value of the bit with the specified index.
     */
    public boolean get(int aSlot) {
        return (mBits & bit(aSlot)) != 0;
    }

    /**
     * Assigns the specified bit the specified value.
     *
     * @param  aSlot   slot index of the bit to be set. Must be in <0,14> (not checked)
     * @param aValue      value to be assigned to the specified bit
     */
    public BoolTag set(int aSlot, boolean aValue) {
        return (aValue) ? set(aSlot) : clear(aSlot);
    }

    /**
     * Sets the bit specified by the index to <code>true</code>.
     *
     * @param  aSlot   slot index of the bit to set. Must be in <0,14> (not checked)
     */
    public BoolTag set(int aSlot) {
        return new BoolTag(tagset, mBits | bit(aSlot));
    }

    /** 
     * Sets the bit specified by the index to <code>false</code>.
     *
     * @param  aSlot   index of the bit to be cleared. Must be nonnegative (not checked, asserted).
     */
    public BoolTag clear(int aSlot) {
        return new BoolTag(tagset, mBits & ~bit(aSlot));
    }

    /**
     * Set all bits in the specified range to <code>true</code>.
     *
     * @param aFrom the first index to be set
     * @param aTo the last index to be set
     */
// public void set(int aFrom, int aTo) {
//    for (int i = aFrom; i <= aTo; i++)
//       set(i);
// }
    /**
     * Set all bits in the specified range to <code>false</code>.
     *
     * @param aFrom the first index to be cleared
     * @param aTo the last index to be cleared
     */
// public void clear(int aFrom, int aTo)  {
//    for (int i=aFrom; i <= aTo; i++)
//       clear(i);
// }
// --------------------------------------------------------------------------------------------
// Logical operations (not in place)
// --------------------------------------------------------------------------------------------

    /** 
     * Result = ~this
     * <p>
     * Returns a logical <b>not</b> of this booltag.
     * Each bit in it the resulting bit is reversed (<code>true</code> to <code>false</code> and vice versa.)
     *
     * @return ~aTag
     */
    public BoolTag not() {
        return new BoolTag(tagset, ~mBits);
    }

    /** 
     * Result = this & aTag.
     * <p>
     * Returns a logical <b>and</b> of this booltag and the supplied one.
     * Each bit in it the resulting booltag has the value <code>true</code> iff
     * corresponding bit in both booltag have value <code>true</code>.
     *
     * @param  aTag  a booltag.
     * @return (this & aTag)
     */
    public BoolTag and(BoolTag aTag) {
        return new BoolTag(tagset, mBits & aTag.mBits);
    }

    /** 
     * Result = this & ~aTag.
     * <p>
     * Returns a logical <b>nand</b> of this booltag and the supplied one.
     * Each bit in it the resulting booltag has the value <code>true</code> iff
     * corresponding bit in this booltag is <code>true</code> and in aTag is <code>false</code> .
     *
     * @param  aTag  a booltag.
     * @return (this & ~aTag)
     */
    public BoolTag nand(BoolTag aTag) {
        return new BoolTag(tagset, mBits & (~aTag.mBits));
    }

    /** 
     * Result = this or aTag.
     * <p>
     * Returns a logical <b>or</b> of this booltag and the supplied one.
     * Each bit in it the resulting booltag has the value <code>true</code> iff
     * either the corresponding bit in this booltag and/or in aTag is <code>true</code> .
     *
     * @param  aTag  a booltag. It has to be at least as long as this booltag
     * @return (this or aTag)
     */
    public BoolTag or(BoolTag aTag) {
        return new BoolTag(tagset, mBits | aTag.mBits);
    }

    /**
     * Result = this xor aTag.
     * <p>
     * Returns a logical <b>xor</b> of this booltag and the supplied one.
     * Each bit in it the resulting booltag has the value <code>true</code> iff
     * either the corresponding bit in this booltag or in aTag is <code>true</code>, but not both.
     *
     * @param aTag a booltag. It has to be at least as long as this booltag
     * @return (this xor aTag)
     */
    public BoolTag xor(BoolTag aTag) {
        return new BoolTag(tagset, mBits ^ aTag.mBits);
    }
// --------------------------------------------------------------------------------------------
// Test
// --------------------------------------------------------------------------------------------
//    /**
//     * ## TODO: more effective
//     */
//    public BoolTag subMap(int aBeginIndex, int aEndIndex) {
//        // assert
//
//        int size = aEndIndex - aBeginIndex;
//        BoolTag tmp = new BoolTag(tagset);
//
//        for (int i = 0; i < size; i++) {
//            tmp.set(i, get(i + aBeginIndex));
//        }
//        return tmp;
//    }
// --------------------------------------------------------------------------------------------
// Test
// --------------------------------------------------------------------------------------------

    /** Tests whether this booltag is a subset of the supplied booltag
     *
     * @param aTag potential superset of this set
     * @return <CODE>true</CODE> iff this booltag is a subset of the supplied booltag
     */
    public boolean isSubsetOf(BoolTag aTag) {
        return ((mBits & aTag.mBits) == mBits) &&
                ((mBits | aTag.mBits) == aTag.mBits);
    }

    /** Tests whether all bits in this set are <code>true</code>
     * @return <code>true</code> iff all bits are <code>true</code>
     */
    public boolean hasAllSet() {
        return (mBits == tagset.cAllTrueBits); // todo only relevant
    }

    /** Tests whether all bits in this set are <code>false</code>
     * @see #allCleared(int,int)
     * @return <code>true</code> iff all bits are <code>false</code>
     */
    public boolean hasAllCleared() {
        return (mBits == 0);
    }

    /** 
     * Tests whether some bits in this set are <code>true</code>
     * @return <code>true</code> iff all bits are not <code>false</code>
     */
    public boolean hasSomeSet() {
        return (mBits != 0);
    }

    /** Tests whether all bits in the specified range are <code>false</code>.
     * Slow in comparison with allCleared() - allCleared() goes by longs, this goes by bits.
     *
     * @param aStart  start of the range (inclusive)
     * @param aEnd end of the range (exclusive)
     * @see #allCleared()
     * @return <code>true</code> if all bits in the specified range are <code>false</code>
     */
    public boolean hasAllCleared(int aStart, int aEnd) {
        for (int i = aStart; i < aEnd; i++) {
            if (get(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns number of <code>true</code> bits.
     *
     * @return number of <code>true</code> bits.
     * ## TODO: more effective
     */
    public int nrOfSet() {
        int sum = 0;
        for (int i = 0; i < getLen(); i++) {
            if (get(i)) {
                sum++;
            }
        }
        return sum;
    }
// --------------------------------------------------------------------------------------------
// Implementation
// --------------------------------------------------------------------------------------------
    /**
     * Returns a hash code value for this booltag. The hash code
     * is dependent on the internal booltag.
     * <p>Overrides the <code>hashCode</code> method of <code>Object</code>.
     *
     * @return  a hash code value for this booltag.
     */
    @Override
    public int hashCode() {
        return (int) mBits;
    }

    /** 
     * Compares this object against the specified object.
     * The result is <code>true</code> iff the argument 
     * <ul>
     *     <li>is not <code>null</code></li>
     *     <li>has exactly the same bits set to <code>true</code> as this bimap.
     *         That is, \forall i \in &lt; 0, getLen() &gt; : 
     *              ((BoolTag)aObj).get(i) == this.get(i)</li>
     * </ul>
     * <p>Overrides the <code>equals</code> method of <code>Object</code>.
     *
     * @param  aObj the object to compare with.
     * @return <code>true</code> if the objects are the same;
     *          <code>false</code> otherwise.
     * @see java.util.BoolTag#equals(Object,int,int)
     */
    @Override
    public boolean equals(Object aObj) {
        if (aObj == null || !(aObj instanceof BoolTag)) {
            return false;
        }
        return (mBits == ((BoolTag) aObj).mBits);
    }

    /**
     * Returns a string representation of this booltag.
     * Set is represented as set a sequence of '+' (for set slots) and '-'.
     *
     * @return  a string representation of this booltag.
     */
    @Override
    public String toString() {
        final StringBuilder buf = new StringBuilder(tagset.cAllHyphens);

        for (int i = 0; i < getLen(); i++) {
            buf.setCharAt(i, get(i) ? '+' : '-');
        }

        return buf.toString();
    }

    
    
// --------------------------------------------------------------------------------------------
// Implementation
// --------------------------------------------------------------------------------------------
    /**
     * Given a bit index, return a unit that masks that bit in its unit.
     * 
     * @param aSlot bit that should be set to <code>true</code>.
     * @return unit with all zeros except the bit at aSlot (?? bitIndex mod ADDRESS_BITS_PER_UNIT)
     *    3 -> 00..01000, 0 -> 00..00001
     */
    private int bit(int aSlot) {
        return 1 << (aSlot & tagset.cAllTrueBits); // = ? bitIndex % 32
    }
}
