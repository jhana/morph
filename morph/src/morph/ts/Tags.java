package morph.ts;

import java.util.*;
import util.*;

/**
 * Support for working with tag collections.
 *
 * @author  Jirka
 */
public final class Tags {
    
    /** To prevent object creation */
    private Tags() {
    }

    /** 
     * Converts a collection of strings to a list of tags.
     */
    public static List<Tag> fromStrings(Tagset aTagset, Collection<String> aStrings)  {
        List<Tag> tags = new ArrayList<Tag>(aStrings.size());
        for (String string : aStrings)  {
            tags.add(aTagset.fromString(string));
        }

        return tags;
    }

    /**
     *
     * Equivalent (but faster) to filterTags(Tags.fromStrings(aStrings), aBoolTag)
     */
    public static List<Tag> fromStrings(Tagset aTagset, Collection<String> aStrings, BoolTag aBoolTag)   {
        List<Tag> tags = new ArrayList<Tag>(aStrings.size());
        for (String string : aStrings) 
            tags.add(aTagset.fromString(string).filterTag(aBoolTag));
        
        return tags;
    }
    

    public static List<String> toAbbrs(Collection<Tag> aTags) {
        List<String> abbrs = new ArrayList<String>(aTags.size());
                
        for (Tag tag : aTags) {
            abbrs.add(tag.tagAbbr());
        }
        return abbrs;
    }
    
    
    
    /**
     *
     * HO: map(aTags, \t . t.filterTag(aConsideredPositions))
     */
    public static Set<Tag> filterTags(Collection<Tag> aTags, BoolTag aBoolTag) {
        Set<Tag> filteredTags = new TreeSet<Tag>();
        for (Tag tag : aTags)
            filteredTags.add(tag.filterTag(aBoolTag));
        
        return filteredTags;
    }

    /**
     * Keeps only tags with the specified value at the specified slot
     * @param aTags collection of tags to be filtered (the collection is modified)
     * @param aSlot the slot to check
     * @param aValue the required value
     * @return the modified collection (result and aTags are equal)
     */
    public static Collection<Tag> keep(Collection<Tag> aTags, int aSlot, char aValue) {
        for (Iterator<Tag> i = aTags.iterator(); i.hasNext();) 
            if (i.next().getSlot(aSlot) != aValue) i.remove();
        
        return aTags;
    }

//    public static Collection<Tag> keep(Collection<Tag> aTags, int aSlot, String aValues) {
//        for (Iterator<Tag> i = aTags.iterator(); i.hasNext();) 
//            if (!i.next().slotValueIn(aSlot, aValues)) i.remove();
//        
//        return aTags;
//    }
    
    public static boolean allTags(Collection<Tag> aTags, int aSlot, char aVal) {
        for (Tag tag : aTags) 
            if (tag.getSlot(aSlot) != aVal) return false;
        
        return true;
    }

    public static boolean someTags(Collection<Tag> aTags, int aSlot, char aVal) {
        for (Tag tag : aTags) 
            if (tag.getSlot(aSlot) == aVal) return true;
        
        return false;
    }

//    public static boolean someTagsIn(Collection<Tag> aTags, int aSlot, String aVals) {
//        for (Tag tag : aTags) 
//            if (tag.slotValueIn(aSlot, aVals)) return true;
//        
//        return false;
//    }

//    public static boolean noTagsIn(Collection<Tag> aTags, int aSlot, String aVals) {
//        return !someTagsIn(aTags, aSlot, aVals);
//    }
    
    
    public static Set<Character> collectValues(Collection<Tag> aTags, int aSlot) {
        Set<Character> vals =  new TreeSet<Character>();
        for (Tag tag : aTags) {
            vals.add(tag.getSlot(aSlot));
        }
        
        return vals;
    }    

    public static String collectValueStr(Collection<Tag> aTags, int aSlot) {
        Set<Character> vals =  collectValues(aTags, aSlot);

        StringBuilder sb = new StringBuilder(vals.size());
        for (Character val : vals) {
            sb.append(val);
        }

        return sb.toString();
    }

}
