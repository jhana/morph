package morph.ts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import static java.util.Arrays.asList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;
import lombok.Cleanup;
import lombok.Getter;
import morph.util.MUtilityBase;
import util.col.Cols;
import util.col.MultiMap;
import util.io.IO;
import util.io.XFile;
import util.str.Strings;

/**
 *
 * @author Jirka dot Hana at gmail dot com
 */
public class ConvRusTs extends MUtilityBase {
    public static void main(String[] args) throws IOException {
        new ConvRusTs().go(args);
    }

    private List<String> oldTagset;
    private List<String> newTagset;


    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);

        final XFile oldTagsFile = getArgs().getDirectFile(0, cPlain);
        final XFile newTagsFile = getArgs().getDirectFile(1, cPlain);
        final XFile     outFile = getArgs().getDirectFile(2, cPlain);

        oldTagset = IO.readInColumn(oldTagsFile, 0);
        newTagset = IO.readInColumn(newTagsFile, 0);


        MultiMap<String,String> map = createMap();
        dumpMap(outFile, map);
    }

    protected Tag translateVals(String tag) {
        switch (tag.charAt(1)) {
            case '6':
                return new Tag(tag).set(cSlotS, 'P').set(cSlotR, 'R');
            case '8':
                return new Tag(tag).set(cSlotS, 'S').set(cSlotR, 'R');
            case 'P':
            case '5':
            case 'S':
                return new Tag(tag).set(cSlotR, 'I');
            case 'p':
                return new Tag(tag).set(cSlotS, 'B');
            case 'e':
                return new Tag(tag).set(cSlotB, 'I');
            case 'm':
                return new Tag(tag).set(cSlotS, 'e').set(cSlotB, 'P');
            default:
                return new Tag(tag);
        }
    }

    private MultiMap<String,String> createMap() {
        final MultiMap<String,String> map = new MultiMap<String,String>();

        for(String tag : oldTagset) {
            Collection<String> newTags = translateTag(tag);
            map.addAll(tag, newTags);
        }

        return map;
    }

    private Collection<String> translateTag(String tag) {
        char var = tag.charAt(cOldSlotI);
        if (var != '-') tag = Strings.setCharAt(tag, cOldSlotI, '-');

        Collection<String> newTs = filterList(newTagset, preTranslateTag(tag));

        if (newTs.isEmpty()) {
            System.out.println("------");
            System.out.println("   orig: " + tag);
            System.out.println("   pre: " + preTranslateTag(tag));
            newTs = Arrays.asList("<ERROR--------->");
        }

        if (var != '-') {
            for (String newT : newTs) {
                tag = Strings.setCharAt(newT, cOldSlotI, var);
            }
        }
        
        return newTs;
    }


    // @todo use the util version with regex predicate
    private List<String> filterList(List<String> aList, String aRegex) {
        final Pattern pattern = Pattern.compile(aRegex);
        final List<String> filtered = new ArrayList<String>();

        for (String e : aList) {
            if (pattern.matcher(e).matches()) filtered.add(e);
        }


        return filtered;
    }


    /** @todo to utils */
    public static <K,V> Comparator<Map.Entry<K,V>> key2entryComparator(final Comparator<K> aComp) {
        return new Comparator<Map.Entry<K,V>>() {
            @Override
            public int compare(Entry<K, V> o1, Entry<K, V> o2) {
                return aComp.compare(o1.getKey(), o2.getKey());
            }
        };
    }

    /** @todo to utils */
    public static <K extends Comparable<K>,V> Comparator<Map.Entry<K,V>> key2entryComparator() {
        return new Comparator<Map.Entry<K,V>>() {
            @Override
            public int compare(Entry<K, V> o1, Entry<K, V> o2) {
                return o1.getKey().compareTo(o2.getKey());
            }
        };
    }

    /** @todo to utils */
    public static <K  extends Comparable<K>,V> List<Map.Entry<K,V>> sort(Collection<Map.Entry<K,V>> aMapEntries) {
        return Cols.sort( aMapEntries, ConvRusTs.<K,V>key2entryComparator() );
    }


    private void dumpMap(XFile aOutFile, MultiMap<String,String> aMap) throws IOException {
        @Cleanup PrintWriter w = IO.openPrintWriter(aOutFile);

        for( Map.Entry<String,Set<String>> e : sort(aMap.entrySet()) ) {
            w.print(e.getKey());
            w.print('\t');
            w.println(Cols.toString(e.getValue(), "", "", " ", ""));
        }
    }


//s=6 => s=P r=R
//s=8 => s=S r=R
//s=P5S => r=I
//s=p => s=B
//s=e => b=I
//s=m => s=e b=P
//// AG/V => r=RI
    private final static int cSlotS = 1;  // subpos
    private final static int cSlotR = 9;  // refl
    private final static int cSlotB = 11; // aspect

    private final static int cOldSlotI = 14; // var (last slot)

    public String preTranslateTag(String aTag) {
        String tag = repositionTag(aTag);
        tag = translateVals(tag).getTag();
        return Character.isLetterOrDigit(tag.charAt(1)) ?
            tag : new StringBuilder(tag).insert(2, "\\E").insert(1, "\\Q").toString();
    }

    private String repositionTag(String aTag) {
        return new StringBuilder()
            .append(aTag.charAt(0))  // p
            //.append("\\Q" + aTag.charAt(1) + "\\E")  // s
            .append(aTag.charAt(1))  // s
            .append(aTag.charAt(2))  // g
            .append('.')  // y
            .append(aTag.charAt(3))  // n
            .append(aTag.charAt(4))  // c
            .append(aTag.charAt(5))  // f
            .append(aTag.charAt(6))  // m
            .append(aTag.charAt(7))  // e
            .append('.')  // r
            .append(aTag.charAt(8)) // t
            .append('.')  // b
            .append(aTag.charAt(9)) // d
            .append(aTag.charAt(10)) // a
            .append(aTag.charAt(11)) // v
            .append(aTag.charAt(14)) // i
            .toString();
    }

    // Not used (allows disjunctive values); DOES NOT SEEM TO WORK
    public static class Tags {
        @Getter List<String> tags;

        public Tags(String ... aTags) {
            tags = asList(aTags);
        }

        public String getTag() {
            return tags.get(0);
        }

        public Tags set(int aPos, char aVal) {
            final List<String> tmp = new ArrayList<String>();

            for (String tag : tags) {
                tmp.add(Strings.setCharAt(tag, aPos, aVal));
            }

            tags = tmp;
            return this;
        }

        public Tags set(int aPos, String aVals) {
            final List<String> tmp = new ArrayList<String>();

            for (String tag : tags) {
                for (char val : aVals.toCharArray()) {
                    tmp.add(Strings.setCharAt(tag, aPos, val));
                }
            }

            tags = tmp;
            return this;
        }
    }

    public static class Tag {
        @Getter String tag;

        public Tag(String aTag) {
            tag = aTag;
        }

        public Tag set(int aPos, char aVal) {
//            System.out.println(tag + " " + aPos + " " + aVal);
            tag = Strings.setCharAt(tag, aPos, aVal);
//            System.out.println(tag);
            return this;
        }
    }

}
