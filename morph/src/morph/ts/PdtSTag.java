package morph.ts;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringEscapeUtils;
import util.str.Strings;
import util.sgml.SgmlTag;

/**
 * Note: the object is immutable (is this good??)
 *
 * <h1>Layer-tags</h1>
 *
 * Many Pdt tags are so-called layer-tags - they mark particular layer of information
 * associated with forms (morphological analysis, golden standard, automatic disambiguation, ...)
 * and this information has several levels (lemmas, tags). The cores of such tags
 * have two parts: layer string + level character.
 *
 * The standard set of layer-tag cores is as follows:
 *    MMl  = MM + l  / MMt = MM + t  - morph. analysis     
 *    MDl  = MD + l  / MDt = MD + t  - disambiguated layer
 *    l    =      l  / t   =    + t  - golden standard     
 * 
 * Note: This class assumes that level is marked by exactly one character!! 
 * Note: Currently assumes that one of the level is 'l' level - see setLayers(Collection).
 *
 * <h2>Source</h2>
 *
 * A layer can be further divided into sub-layers by a source atribute (src).
 * Fore example, different taggers will use MD layer and different source 
 * (e.g. <MDl src="a"> / <MDl src="b">).
 *
 * <h2>Selection</h2>
 *
 * Selection is marked by atribute das with possible values "+" (selected, default) or 
 * "-" (not selected). 
 *
 * <h1>Other tags</h1>
 *
 * <A> - syntax, .....
 *
 *
 * @author Jirka
 */
public final class PdtSTag extends SgmlTag {

// =============================================================================

    public final static String cANSelected = "das";
    public final static String cANSrc      = "src";
    

// =============================================================================
// General PDT-wide configuration 
// =============================================================================
    
    private static Set<String> mLayers;
    private static Set<String> mLCores;
    private static Set<String> mTCores;
    
    // --- defaults ---
    static {
        setLayers( Arrays.asList("", "MM", "MD", "MX") );
    }
    
    public static void setLayers(Collection<String> aLayerStrs) {
        mLayers = new HashSet<String>(aLayerStrs);

        mLCores = new HashSet<String>();
        mTCores = new HashSet<String>();
        
        for (String layerStr : aLayerStrs) {
            mLCores.add(layerStr + 'l' );
            mTCores.add(layerStr + 't' );
        }
    }
    
    public static Set<String> getLayers() {
       return mLayers;
    }
    
    public static Set<String> getLCores() {
        return mLCores;
    }

    public static Set<String> getTCores() {
        return mTCores;
    }
    
// =============================================================================

    
    
    private String mSrc;
    
    
    /**
     * Creates a new instance of PdtSTag 
     * No checks performed whether the token is legal.
     */
    public PdtSTag(String aToken) {
        super(aToken);
    }

    /**
     *
     * No checks performed whether the core or src are legal.
     * @param aCore core of the tag
     * @param aSrc ignored if null or empty string.
     */
    public PdtSTag(String aCore, String aSrc) {
        super("");
        if (aSrc == null || aSrc.equals(""))
            mToken = "<" + aCore + '>';
        else 
            mToken = "<" + aCore + " src=\"" + StringEscapeUtils.escapeHtml4(aSrc) + "\">";
    }

    
    /**
     * Return l for lemma, t for tag
     * Note: Assumes this is a layer-tag (no checks performed).
     */
    public String getLayer() {
        return Strings.substringB(getCore(), 0, 1);
    }
    
    /**
     *
     * Note: Assumes this is a layer-tag (no checks performed).
     */
    public char getLevel() {
        return Strings.lastChar(getCore());
    }

// =============================================================================    
// Tag types
// =============================================================================    

    public boolean isFTag() {
        return getCore().equals("f");       //@todo f.upper, ..?
    }

    public boolean isDTag() {
        return getCore().equals("d");       //@todo f.upper, ..?
    }
    
    public boolean isFDTag() {
        return isFTag() || isDTag();
    }
    
    public boolean isLTag() {
        return mLCores.contains(getCore());
    }

    public boolean isTTag() {
        return mTCores.contains(getCore());
    }

// =============================================================================    
// L/T specifics, attributes
// =============================================================================    
    
    public boolean isSelected() {
        return getAttribute(cANSelected, "+").equals("+");
    }    

    /**
     * Get src attribute value of the tag
     *
     * @return src attribute value if present on an empty string otherwise.
     */
    public final String getSrc()      {
        if (mSrc == null) mSrc = getAttribute(cANSrc, ""); 
        return mSrc;
    }
    
    
    private static Pattern selectedPtr = Pattern.compile(" das=\"(.)\"");
    
    public boolean isSelectedX() {
        Matcher matcher = selectedPtr.matcher(mToken);
        if ( !matcher.find() ) return true;
        return matcher.group().equals("+");     //@todo can check the char directly
    }   

    /**
     *
     * @return the id if specified; null otherwise
     */
    public final String getId() {
        return getAttribute(cIdPattern, null);
    }
    
    private final static Pattern cIdPattern = attrPattern("id");
    
// -----------------------------------------------------------------------------
// Edits 
// -----------------------------------------------------------------------------
    
    public PdtSTag setId(String aValue) {
        return setAttribute("id", aValue);
    }
    
    /**
     *
     * Note: Does not check whether this tag is a layer-tag.
     */
    public PdtSTag setLayer(String aLayerStr) {
        return setCore( getLayer() + getLevel() );
    }
    
    /**
     * Empty src means the src attribute will be removed. ???
     * Not implemented.
     */ 
    public PdtSTag setSrc(String aSrc) {
        if (aSrc.equals("")) 
            return removeSrc();
        else
            return setAttribute(cANSrc, aSrc);
    }
    
    /**
     * Empty src means the src attribute will be removed. ???
     * Not implemented.
     */ 
    public PdtSTag removeSrc() {
        return removeAttribute(cANSrc);
    }
    

    /**
     *
     * If aSelect is true, removes the attribute
     * @param aSelect 
     *
     */
    public PdtSTag setSelected(boolean aSelect) {
        if (aSelect)
            return removeAttribute(cANSelected);
        else 
            return setAttribute(cANSelected, "-");
    }   
    
    

// =============================================================================    
// Retyping of inherited functions
// =============================================================================    

    @Override
    public PdtSTag setCore(String aCore) {
        return (PdtSTag) super.setCore(aCore);
    }

    @Override
    public PdtSTag setAttribute(String aName, String aValue) {
        return (PdtSTag) super.setAttribute(aName, aValue);
    }

    @Override
    public PdtSTag removeAttribute(String aName) {
        return (PdtSTag) super.removeAttribute(aName);
    }


    /**
     * Transforms a standard layer tag from the l-level to the t-level.
     */
    public static String toTCore(String aLCore) {
        return Strings.removeTail(aLCore, 1) + 't';
    }

    /**
     * Transforms a standard layer tag from the l-level to the t-level.
     */
    public PdtSTag toTTag() {
        return setCore( toTCore(getCore()) );
    }
    
    
// =============================================================================    
// Implementation
// =============================================================================    
    
    @Override
    protected PdtSTag constructor(String aToken) {
        return new PdtSTag(aToken);
    }

    @Override
    public PdtSTag constructorx(String aToken, String aCore) {
        PdtSTag t = constructor(aToken);
        t.mCore = aCore;
        return t;
    }


}
