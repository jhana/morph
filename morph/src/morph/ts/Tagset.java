package morph.ts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import lombok.Getter;
import util.col.Cols;
import util.err.Err;
import util.opt.Options;
import util.opt.RootOptions;
import util.str.StringPair;
import util.str.Strings;



/**
 *
 * @author Jirka Hana
 * @todo this is a temporary transitional code
 */
public class Tagset {
    /** Len of the tags in the tagset. 0 means unspecified */
    @Getter private final int len;
    @Getter private final String id;

    public final int cAllTrueBits;   // 1....1 (32*)
    public final String cAllHyphens;
    public final BoolTag cAllTrue;
    public final BoolTag cAllFalse;
    public final BoolTag cAllTrueWoVariant;
    public final Tag cDummy;

    public final static int cPOS = 0;
    public final static int cSubPOS = 1;

    @Getter private final int negationSlot;
    @Getter private final char negationAffirmative;
    @Getter private final char negationNegated;

    @Getter private final boolean hasVariantSlot;
    @Getter private final int variantSlot;

    /** All slot codes ordered by their positions */
    public final String cCodeString;
    private final List<String> slotNames;

    public final Pattern openClassPattern;
    public final Pattern punctPattern;
    public final Pattern nrsPattern;


    @Deprecated
    public static Tagset getDef() {
        return TagsetRegistry.getDef().getDefTs();
    }

    public static class TagsetBuilder {
        final Options options = new RootOptions();

        public TagsetBuilder() {
            setOpt("len", 0);
            setOpt("id", "?");
        }
        
        public TagsetBuilder setOption(String aKey, Object aVal) {
            options.set(aKey, aVal);
            return this;
        }

        public TagsetBuilder setOptions(Options aOptions) {
            options.setOptions(aOptions);
            return this;
        }

        public TagsetBuilder setId(String aId) {
            return setOpt("id", aId);
        }

        public TagsetBuilder setTagLen(int aLen) {
            return setOpt("len", aLen);
        }

        public TagsetBuilder setSlotCodes(String aSlotCodes) {
            return setOpt("slotCodes", aSlotCodes);
        }

        public TagsetBuilder setOpenClass(String aOpenClassPattern) {
            return setOpt("openClass", aOpenClassPattern);
        }

        public TagsetBuilder setSlotName(int aSlot, String aName) {
            return setOpt("slotName." + aSlot, aName);
       }

        public Tagset build() {
            Err.iAssert(options != null, "null options");
            
            // len = 0 <=> 
            
            return new Tagset(options);
        }

        private TagsetBuilder setOpt(String aKey, Object aVal) {
            options.set(aKey, aVal);
            return this;
        }

    }

    /**
     * Slot char must be known or '-'.
     * @param aOptions
     * @param aKey
     * @param aNoslot slot char to use if no value is specified for aKey
     * @return
     */
    private int getSlot(Options aOptions, String aKey, char aNoslot) {
        char slotChar = aOptions.getChar(aKey, aNoslot);

        final int slotIdx = code2slot(slotChar);
        Err.fAssert(slotIdx != -1 || slotChar == '-', "The option %s uses an unknown slot:  %c (known: %s)", aKey, slotChar, cCodeString);

        return slotIdx;
    }

    /**
     *
     * @param aOptions options specifying the tagset (length, slots, and other properties)
     *
     * @todo add options warning
     */
    private Tagset(Options aOptions) {
        id = aOptions.getString("id");
        Err.assertE(id != null, "Missing tagset id (code tagset.id, opt=%s)", aOptions);

        Integer leno = aOptions.getInt("len");
        Err.assertE(leno != null, "Missing tagset len (code tagset.len, opt=%s)", aOptions);
        len = leno;

        cCodeString = aOptions.getString("slotCodes", ""); 
        Err.fAssert(len == 0 || cCodeString != null, "Missing slot codes %s", aOptions);
        Err.fAssert(len == 0 || cCodeString.length() == len, "Slot codes (%s) and tag len (%d) mismatch", cCodeString, len);

        negationSlot = getSlot(aOptions, "negation.slot", '-');
        negationAffirmative = aOptions.getChar("negation.affirmative", (char)0);
        negationNegated     = aOptions.getChar("negation.negated", (char)0);

        variantSlot = getSlot(aOptions, "variant.slot", '-');
        hasVariantSlot = variantSlot != -1;

        openClassPattern = aOptions.getPattern("openClass", null);
        punctPattern     = aOptions.getPattern("punct", null);
        nrsPattern       = aOptions.getPattern("nrs", null);

        if (len == 0) {
            slotNames = null;
            cAllTrueBits = 0;
            cAllFalse = null;
            cAllTrue  = null;
            cAllHyphens = "-";
            cDummy = new Tag(this, "-");
            cAllTrueWoVariant  = cAllTrue;
        }
        else {
            // --- human readable names for each slot ---
            slotNames = new ArrayList<String>(len);
            final List<String> tmpSlotNames = aOptions.getStrings("slotName");
            if (tmpSlotNames.isEmpty()) {
                for (Character c : cCodeString.toCharArray()) {
                    slotNames.add("" + c);
                }
            }
            else {
                slotNames.addAll(tmpSlotNames);
            }
        
            // --- Reads sorting configuration ---
            Cols.listAdd(sortings, len-1, null);
            for (String sortingStr : aOptions.getStrings("sorting")) {
                final char slot = sortingStr.charAt(0);
                final int slotIdx = code2slot(slot);
                Err.fAssert(slotIdx != -1, "Unknown sorting slot %c (%s)", slot, sortingStr);

                final String sortedVals = sortingStr.substring(1);
                Err.fAssert(!sortedVals.isEmpty(), "Empty sorting values (%s)", sortingStr);

                sortings.set(slotIdx, sortedVals);
            }

            // --- ---

            cAllTrueBits = (1 << len) - 1;  // 1....1 (len times)
            cAllFalse = codestr2booltag("none");    // risk of cyclic calls
            cAllTrue  = codestr2booltag("all");
            cAllHyphens = Strings.repeatChar('-', len);
            cDummy = new Tag(this, cAllHyphens);

            if (hasVariantSlot) {
                cAllTrueWoVariant  = cAllTrue.set(variantSlot, false);
            }
            else {
                cAllTrueWoVariant  = cAllTrue;
            }
        }
    }



// ==============================================================================
// Constructors
// ==============================================================================

    /**
     * Does check the tag using assertTag(Tag)
     * @param aTag
     * @return the corresponding tag object
     * @throws HException if the tag is not correct
     */
    public Tag fromString(String aTag)  {
        assertTag(aTag);
        return new Tag(this, aTag);
    }

    /**
     * Does not check the correctness of the tag
     * @param aTag
     * @return the corresponding tag object
     */
    public Tag fromStringNoCheck(String aTag) {
        return new Tag(this, aTag);
    }

    // tagabbr(%X)?    X =  A|N|#|A#|N#
   public Tag fromStringFill(String aTag) {
        if (aTag.length() >= len) return new Tag(this, aTag);

        // --- fill in the missing places ---
        StringPair parts = Strings.splitIntoTwo(aTag, '%');
        StringBuilder tag = new StringBuilder(parts.mFirst);
        tag.append(cAllHyphens.substring(tag.length()));
        setAbbrTail(tag, parts.mSecond);
        return fromString(tag.toString());
    }

    public Tag fromAbbr(String aAbbr, String aTmpl, String aNegationOk)  {
        final StringBuilder tag = fromAbbrI(aAbbr, aTmpl);

        if (negationSlot != -1 && TagUtil.slotValueIn(tag, 1, aNegationOk)) {
            TagUtil.setSlot(tag, negationSlot, 'A');
        }

        return fromString(tag.toString());
    }

    public Tag fromAbbr(String aAbbr, String aTmpl)  {
        return fromString(fromAbbrI(aAbbr, aTmpl).toString());
    }


    /**
     * Creates a tag from an abbreviation (relative to certain template).
     * 
     * @todo make work for unspecified tagsets
     *
     * @param aAbbr abbreviation or a full tag
     * @param aTmpl tag template, question marks mark slots to be filled by
     *    characters from the abbreviation aAbbr. If the template is shorter
     *    than the tag length {@link #len}, it is padded with dashes.
     * @return returns a full tag obtained in the following way:
     *   <ul>
     *   <li>If the abbreviation is of the full-tag length, it is returned; otherwise
     *   <li> the tag is obtained from the template by replacing all question
     *   marks by characters in the abbreviation. If there is an extra character
     *   in the abbreviation, it is used as an variant.
     *   </ul>
     * @todo template object
     * @todo configurable what is used as a variant
     * @throws FormatError if the abbreviation is too short or too long
     */
    StringBuilder fromAbbrI(String aAbbr, String aTmpl)  {
        if (len == 0) {
            throw new UnsupportedOperationException("");
        }
        else  {
            if (aAbbr.length() == len) return new StringBuilder(aAbbr);
            Err.fAssert(aTmpl.length() <= len, "util.Tag.fromAbbr - template too long (tmpl %s, taglen %d)", aTmpl, len);

            final StringBuilder tag = new StringBuilder(cAllHyphens);

            BoolTag boolTag = codestr2booltag("none");
            for (int i = 0; i < aTmpl.length(); i++) {
                if (aTmpl.charAt(i) == '?') boolTag = boolTag.set(i);
            }

            Err.fAssert(boolTag.nrOfSet() <= aAbbr.length(),     "util.Tag.fromAbbr - not enough fillers (abbr %s, tmpl %s)", aAbbr, aTmpl);
            Err.fAssert(boolTag.nrOfSet() + 1 >= aAbbr.length(), "util.Tag.fromAbbr - too many fillers (abbr %s, len=%d, tmpl %s, set %d; ts.len=%d)", aAbbr, aAbbr.length(), aTmpl, boolTag.nrOfSet(), len);

            int j = 0;
            for (int i = 0; i < aTmpl.length(); i++) {
                tag.setCharAt(i, boolTag.get(i) ? aAbbr.charAt(j++) : aTmpl.charAt(i) );
            }

            // --- Use the extra character for variant ---
            if (j < aAbbr.length() ) {
                char val = aAbbr.charAt(j);
                //setSlot(tag, int.variant, val);  // TODO configurable
                // todo configure - here it assumes the last slot is variant
                tag.setCharAt(len-1, val);
            }

            return tag;
        }
    }

    void setAbbrTail(StringBuilder aTag, String aTail )  {
        if (aTail.length() == 0) return;
        char val = aTail.charAt(0);

        if (negationSlot != -1 && Strings.inString(val, "AN")) {        // todo hack, negation values can be different from A/N
            TagUtil.setSlot(aTag, negationSlot, val);
        }
        else if (Character.isDigit(val) && hasVariantSlot) {
            TagUtil.setSlot(aTag, variantSlot,  val);
        }
        else {
            Err.err("util.Tag.fromAbbr - wrong tail (tag %s, tail %s)", aTag, aTail);
        }

        if (aTail.length() == 1) return;

        val = aTail.charAt(1);

        if (Character.isDigit(val)) {
            TagUtil.setSlot(aTag, variantSlot, val);
        }
        else {
            Err.err("util.Tag.fromAbbr - wrong tail (tag %s, tail %s)", aTag, aTail);
        }
    }

// =============================================================================


    public boolean isPunct(Tag aTag) {
        return aTag.matches(punctPattern);
    }


    /**
     * Returns a human readable slot name.
     *
     * @param aIdx slot index
     */
    public String slotName(int aIdx) {
        Err.iAssert(aIdx < len, "Slot index %d is not within tagset len (%d)", aIdx, len);
        return slotNames.get(aIdx);
    }

    public String slotNamePadded(int aIdx) {
        return String.format("%10s", slotName(aIdx));
    }

// =============================================================================
// Codes and code strings/booltags
// todo configurable
// =============================================================================


    public final char slotCode(int aIdx) {
        return slot2code(aIdx);
    }

    /**
     * Returns the code character of a slot. (e.g. 0 -> P, 2 -> g)
     *
     * @param aIdx slot index (0-15, checked only by String.charAt)
     * @returns the code character coresponding to aIdx
     * @see #slotCodeStr(int)
     */
    public final char slot2code(int aIdx) {
        return cCodeString.charAt(aIdx);
    }


    public int code2slot(char aCode) {
        return cCodeString.indexOf(aCode);
    }

    // todo optimize
    public BoolTag codestr2booltag(String aCodeString) {
        Err.iAssert(len != 0, "Underspecified tagset does not support this operation");

        BoolTag boolTag = new BoolTag(this);
        if ( aCodeString.equals("all") ) {
            for (int slot = 0; slot < len; slot++) {
                boolTag = boolTag.set(slot);
            }
        }
        else if ( aCodeString.equals("none")) {
        }
        else {
            for (int i = 0; i < aCodeString.length(); i++) {
                int slot = code2slot(aCodeString.charAt(i));
                //System.out.printf("%s:   code: %s - slot %d  (%s)\n", aCodeString, aCodeString.charAt(i), slot, boolTag);
                if (slot == -1) throw new IllegalArgumentException("Unknown slot code " + aCodeString.charAt(i));
                boolTag = boolTag.set(slot);
            }
        }

        return boolTag;
    }

    // todo test
    public BoolTag str2booltag(String aString) {
        if (aString == null) return null;

        if (aString.isEmpty() || (aString.charAt(0) != '+' && aString.charAt(0) != '-') ) {
            return codestr2booltag(aString);
        }
        else {
            return plusStr2booltag(aString);
        }
    }

    public List<BoolTag> str2booltag(List<String> aStrings) {
        if (aStrings.isEmpty()) {
            return Collections.emptyList();
        }
        else {
            final String firstString = aStrings.get(0);
            if (firstString.isEmpty() || (firstString.charAt(0) != '+' && firstString.charAt(0) != '-') ) {
                return codestr2booltag(aStrings);
            }
            else {
                return plusStr2booltag(aStrings);
            }
        }
    }


    // TODO test
    public BoolTag plusStr2booltag(String aPlusString) {
        if (aPlusString.length() != len) throw new IllegalArgumentException(String.format("IncorrectUnknown plus string length %s, excpecting %d characters.", aPlusString, len));

        BoolTag boolTag = new BoolTag(this);
        for (int slot = 0; slot < len; slot++) {
            if (aPlusString.charAt(slot) != '-') boolTag = boolTag.set(slot);
        }
        return boolTag;
    }

    /**
     * Converts a list of slot strings to the corresponding list of bool tags.
     */
    public List<BoolTag> codestr2booltag(List<String> aCodeStrings) {
        final List<BoolTag> tmp = new ArrayList<BoolTag>();
        // --- map --
        for (String ss : aCodeStrings) {
            tmp.add(codestr2booltag(ss));
        }
        return tmp;
    }

    /**
     * Converts a list of plus strings to the corresponding list of bool tags.
     */
    public List<BoolTag> plusStr2booltag(List<String> aCodeStrings) {
        final List<BoolTag> tmp = new ArrayList<BoolTag>();
        // --- map --
        for (String ss : aCodeStrings) {
            tmp.add(plusStr2booltag(ss));
        }
        return tmp;
    }


// =============================================================================
// Checks
// =============================================================================

    /**
     * Does a basic check of the tag. Currently, checks only length and chars.
     * @todo throw exception, not 
     */
    public boolean checkTag(String aTag) {
        if (len == 0) return true;
        
        if (aTag.length() != len) {
            System.out.printf("wrong length %d/%d\n", aTag.length(), len);
            return false;
        }
        // -- check if ascii -- @todo probably can be simpler
        for (int i = 0; i < len; i++) {
            int c = aTag.charAt(i);
            if (c < 32 || 128 < c) {
                System.out.println("Wrong char " + i + " " + c + " " + aTag);
                return false;
            }
        }
        return true;
    }

    /**
     * Throws HException if the tag is not correct.
     * (using checkTag(Tag) to check that), otherwise does nothing.
     */
    public void assertTag(String aTag)  {
        Err.assertE(checkTag(aTag), "Wrong tag - '%s' (len=%d)", aTag, len);
    }

        /**
     * Throws HException if the tag is not correct.
     * (using checkTag(Tag) to check that), otherwise does nothing.
     * The exception reports information about the place of the error (e.g. line in a file, etc.)
     */
    public void assertTag(String aTag, String aPlace) {
        Err.assertE(checkTag(aTag), "Wrong tag %s in %s", aTag, aPlace);
    }

// =============================================================================
//
// =============================================================================

    private final List<String> sortings = new ArrayList<String>();

    /**
     * 
     * 
     * Compares two tags taking into account custom sorting of values.
     * For example, for number S (sg) might precede P (pl) even though their
     * alphabetic order is the opposite. When no sorting is specified for a
     * particular slot, the alphabetic order is assumed.
     *
     * @todo: use comparator, provide simple string comparator as well (this one 
     * is slow and for some operations (say for storing tags in TreeSet) is often 
     * not necessary).
     * 
     * @param aA
     * @param aB
     * @return
     * @todo optimize
     */
    public int compare(Tag aA, Tag aB) {
        final String a = aA.getTag();
        final String b = aB.getTag();

       for (int i = 0; i < len; i++) {
            if (a.charAt(i) != b.charAt(i)) {
                final String sorting = sortings.get(i);
                if (sorting == null) {
                    return a.charAt(i) - b.charAt(i);
                }
                else {
                    final char ac = a.charAt(i);
                    final char bc = b.charAt(i);

                    // which one is first in the sorting string
                    for (int j = 0; j < sorting.length(); j++) {
                        final char c = sorting.charAt(j);
                        if (c == ac) {
                            return -1;
                        }
                        else if (c == bc) {
                            return 1;
                        }
                    }

                    // if the sorting string does not specify order, return alphabetic order
                    return a.charAt(i) - b.charAt(i);
                }
            }
        }

        return 0;

    }
}
