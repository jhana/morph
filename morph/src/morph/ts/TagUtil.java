/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph.ts;

import java.util.regex.Pattern;

/**
 *
 * @author jirka
 */
public class TagUtil {
    public  static char getSlot(CharSequence aTag, int aSlot) {
        return aTag.charAt(aSlot);
    }

    public static boolean slotValueIn(CharSequence aTag, int aSlot, String aValues) {
        return aValues.indexOf( getSlot(aTag, aSlot) ) != -1;
    }

    public static void setSlot(StringBuilder aTag, int aSlot, char aValue) {
        aTag.setCharAt(aSlot, aValue);
    }

    /**
     * Converts a tag template to a pattern.
     * @param value like a regular tag with ? marking character wildcard.
     * @return ? is replaced to a . and quote all other chars
     */
    public static Pattern createPattern(final String aTagTmpl) {
        final StringBuilder sb = new StringBuilder();
        sb.append("\\Q");
        boolean wasQuestionmark = false;
        for (int i = 0; i < aTagTmpl.length(); i++) {
            final char c = aTagTmpl.charAt(i);
            if (c == '?') {
                if (!wasQuestionmark) sb.append("\\E");
                sb.append('.');
                wasQuestionmark = true;
            }
            else {
                if (wasQuestionmark) sb.append("\\Q");
                sb.append(c);
                wasQuestionmark = false;
            }
        }
        
        sb.append("\\E");
        return Pattern.compile(sb.toString());
    }
    
}
