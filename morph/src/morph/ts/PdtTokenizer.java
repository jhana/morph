package morph.ts;




/**
 * @todo implement Iterable
 * @author Jiri
 */
public final class PdtTokenizer extends util.sgml.SgmlTokenizer {
    
    public PdtTokenizer(String aString) {
        super(aString);
    }
    
    /**
     * Note: constructed each time again (@todo cache?)
     */
    @Override
    public PdtSTag tag() {
        return new PdtSTag(mToken);
    }
    
}
