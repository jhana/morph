package morph.ts;

import java.io.Serializable;
import java.util.regex.Pattern;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @todo allow specifying order (say sg < pl)
 * @author jirka
 */
@EqualsAndHashCode(exclude={"tagset"})
public final class Tag implements Comparable<Tag>, Serializable {
    @Getter private final Tagset tagset;
    @Getter private String tag;

    Tag(Tagset aTagset, String aTag) {
        tagset = aTagset;
        tag = aTag;
    }


    public int getLen() {
        return tag.length();
    }

    @Override
    public String toString() {
        return tag;
    }

    /** todo amek configurable (e.g. sg before */
    @Override
    public int compareTo(Tag aTag) {
        return tagset.compare(this, aTag);
    }


// ==============================================================================
// Accessing/Setting slots
// ==============================================================================

    public char getSlot(int aSlot) {
        return tag.charAt(aSlot);
    }

    public Tag setSlot(int aSlot, char aValue) {
        return new Tag( tagset, tag.substring(0,aSlot) + aValue + tag.substring(aSlot+1) );
    }


    public char getPOS()        {return tag.charAt(0);}
    public char getSubPOS()     {return tag.charAt(1);}
    public char getNegation()   {return tag.charAt(tagset.getNegationSlot());}
    public char getVariant()    {return tag.charAt(tagset.getVariantSlot());}

    public Tag setPOS(char aVal)      {return setSlot(0, aVal);}
    public Tag setSubPOS(char aVal)   {return setSlot(1, aVal);}
    public Tag setNegation(char aVal) {return setSlot(tagset.getNegationSlot(), aVal);}
    public Tag setVariant(char aVal)  {return setSlot(tagset.getVariantSlot(), aVal);}

// -----------------------------------------------------------------------------
// Tests
// -----------------------------------------------------------------------------

    /**
     * Does the tag match a regex pattern?
     *
     * @param aPattern pattern to test against
     */
    public boolean matches(Pattern aPattern) {
        return aPattern.matcher(tag).matches();
    }

    public boolean slotValueIn(int aSlot, String aValues) {
        return aValues.indexOf( getSlot(aSlot) ) != -1;
    }

    /**
     * Convenience method for testing values of the POS slot.
     * Eq. to slotValueIn(0, aValues)
     */
    public boolean POSIn(String aValues)    {return slotValueIn(0, aValues);}

    /**
     * Convenience method for testing values of the SubPOS slot.
     * Eq. to slotValueIn(1, aValues)
     */
    public boolean subPOSIn(String aValues) {return slotValueIn(1, aValues);}

    /**
     * Checks whether the tag is a punctuation tag, i.e. POS is 'Z'.
     * @todo tagset specific
     */
    public boolean isPunct() {
        return tagset.punctPattern.matcher(tag).matches();
    }

    /**
     * Checks whether the tag is an open class tag, i.e. POS is in {N, A, V, D}.
     */
    public boolean isOpenClass() {
        return tagset.openClassPattern.matcher(tag).matches();
    }


    /**
     * Creates a new tag based on this and the supplied tag. Any slot in this tag
     * that has unspecified value ('-'), takes value from aTag2.
     * E.g:
     *  "abc-d----------".combineTags("ABCD----------E") -> "abcDd--------E");
     */
    public Tag combineTags(Tag aTag2) {
        if (aTag2.getLen() != getLen() ) throw new IllegalArgumentException("Tags must have the same length!");

        final StringBuilder result = new StringBuilder(tag);

        for (int i = 0; i < getLen(); i++) {
            if (getSlot(i) == '-') TagUtil.setSlot(result, i, aTag2.getSlot(i));
        }

        return new Tag(tagset, result.toString());
    }



    /**
     * Check whether two tags are equal considering only certain positions.
     *
     * If no positions are considered, it is always true.
     * @param aTag tag to compare this one with
     * @param aConsideredPositions positions to consider
     * @return true if the two tags are equal on the specified positions; false
     * otherwise
     * Not called equals because then lombok would not generate equals/hashcode
     */
    public boolean eq(Tag aTag, BoolTag aConsideredPositions) {
        for (int i = 0; i < getLen(); i++) {
            if (aConsideredPositions.get(i) && getSlot(i) != aTag.getSlot(i) ) return false;
        }

        return true;
    }

    public boolean startsWith(String aTagPrefix) {
        return tag.startsWith(aTagPrefix);
    }


    /**
     * Keeps only specified position in a tag and the rest is replaced by hyphens.
     * e.g. filterTag("AAAAAAAAAAAAAAA", "++--++--++--++-") = "AA--AA--AA--AA-"
     *
     * @param aTag a tag to be filtered
     * @param aBooleanTag a boolean tag specifying what should be kept, positions
     *    that are set are kept.
     * @return the filtered tag
     */
    public Tag filterTag(BoolTag aBoolTag) {
        if (aBoolTag.hasAllSet()) return this;

        final StringBuilder buf = new StringBuilder(tagset.cAllHyphens);   // @todo3 How about replacing SB by char[] (char[] buf = cAllHyphens.toCharArray();)

        for (int i = 0; i < getLen(); i++) {
            if (aBoolTag.get(i)) TagUtil.setSlot(buf, i, getSlot(i));
        }

        return new Tag(tagset, buf.toString());
    }


    /**
     * Creates a human readable abbreviation of the tag.
     * @todo make configurable
     */
    public String tagAbbr() {
        return tag; // todo should be in tagset
    }
//        if (getNegation() == 'N') return mTag;
//
//        char subPOS = getSubPOS();
//        if (subPOS == 'N')
//            return "" + getGender() + getNumber() + getCase();
//        else if (subPOS == 'f')
//            return "Vf";
//        else if (subPOS == 'i' )
//            return "Vi" + getNumber() + getPerson();
//        else if (subPOS == 'R')
//            return "RR" + getCase();
//        else if (subPOS == 'V')
//            return "RV" + getCase();
//
//        if (mTag.endsWith("-------------"))
//            return mTag.substring(0, 2);
//        else
//            return mTag;
//    }
    public boolean eq(String aTag) {
        return this.tag == null ? aTag == null : this.tag.equals(aTag);
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final Tag other = (Tag) obj;
//        if ((this.tag == null) ? (other.tag != null) : !this.tag.equals(other.tag)) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        return this.tag != null ? this.tag.hashCode() : 0;
//    }



}



