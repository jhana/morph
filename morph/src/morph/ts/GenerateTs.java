package morph.ts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import lombok.Cleanup;
import util.Numbers;
import util.UtilityBase;
import util.col.Cols;
import util.err.Err;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;
import util.str.Strings;

/**
 * Generates a full tagset on the basis of values for each slot, templates, and
 * restrictions.
 *
 * @author Jirka Hana
 */
public class GenerateTs extends UtilityBase {
    /**
     * Generated tagset
     * Collectted so it can be sorted before writing out.
     */
    private final List<String> result = new ArrayList<String>();

    // Tagset specification
    private final List<List<Character>> values = new ArrayList<List<Character>>();
    private final List<Restriction> restrictions = new ArrayList<Restriction>();
    private final List<String> templates  = new ArrayList<String>();

    /** number of slots in the tag */
    private int tagLen;

    public static void main(String[] args) throws IOException {
        new GenerateTs().go(args,2);
    }

    @Override public void goE() throws java.io.IOException {
        XFile inFile = getArgs().getDirectFile(0);
        XFile outFile = getArgs().getDirectFile(1);

        loadSpec(inFile);
        generate(outFile);
    }

    private void generate(XFile outFile) throws IOException {
        for (String template : templates) {
            processTemplate(template, 0);
        }

        Collections.sort(result);
        IO.writeLines(outFile, result);
    }

    /**
     * Recursivelly instantiate a template in all possible ways.
     * @param aTemplate tag template that is being filled
     * @param aSlot how far in the tag are we (how deep in the recursion)
     */
    private void processTemplate(String aTemplate, int aSlot) {
        // template is fully filled, save the result if it is legal
        if (aSlot >= tagLen) {
            // check all restrictions
            for (Restriction r : restrictions) {
                if (! r.satisfies(aTemplate)) return;
            }

            result.add(aTemplate);
        }
        // process next slot
        else {
            // if the slot is variable, instantiate it
            if (aTemplate.charAt(aSlot) == '?') {
                String pref = aTemplate.substring(0, aSlot);
                String suff = aTemplate.substring(aSlot+1);
                // instantiate the variable in all possible ways for that slot
                for (Character value : values.get(aSlot)) {
                    processTemplate(pref + value + suff, aSlot+1);
                }
            }
            // this slot is not a varible, move on.
            else {
                processTemplate(aTemplate, aSlot+1);
            }
        }
    }

    private void loadSpec(XFile aInFile) throws IOException {
        @Cleanup LineReader r = IO.openLineReader(aInFile);

        try {
            loadValues(r);
            loadRestrictions(r);
            loadTemplates(r);
        }
        catch(Exception e) {
            throw new RuntimeException("Error in tagset specification, line " + r.getLineNumber(), e);
        }
    }

    /** TODO move to utils */
    private void loadValues(LineReader r) throws IOException {
        for (String vals : readBlock(r)) {
            if (vals.equals("null")) vals = "";
            values.add(Strings.toList(vals));
        }
        tagLen = values.size();
    }

    private void loadRestrictions(LineReader r) throws IOException {
        for (String line : readBlock(r)) {
            String[] tokens = Strings.cWhitespacePattern.split(line);
            Err.assertE(tokens.length > 2, "Restriction specification too short:\n" + line);

            // ctx => reqs
            if ( tokens[1].equals("=>") ) {
                addRestriction(tokens[0], Cols.subList(tokens, 2));
            }
            // slot val => reqs OR
            else {
                Integer slot = Numbers.parseInt(tokens[0]);

                Err.assertE(tokens.length > 3, "Restriction specification too short:\n" + line);
                Err.assertE(slot != null, "First token must be slot number:\n" + line);
                Err.assertE(tokens[1].length() == 1,  "Val must be single character\n" + line);
                Err.assertE(tokens[2].equals("=>"),  "Required format: slot val => req+\n" + line);
                addRestriction(slot, tokens[1].charAt(0), Cols.subList(tokens, 2));
            }
        }
    }

    private void loadTemplates(LineReader r) throws IOException {
        for (String template : readBlock(r)) {
            templates.add(template);
        }
    }

    /**
     * Reads a list of lines starting at the current line and ending with
     * a closing brace.
     *
     * @param r reader to read from
     * @return list of lines. Empty lines and all-white-space lines are removed,
     * and comments are stripped.
     * @throws IOException .
     * TODO to utils
     */
    private List<String> readBlock(LineReader r) throws IOException {
        final List<String> lines = new ArrayList<String>();
        for (;;) {
            String line = r.readNonEmptyLine();
            Err.assertE(line != null, "Unexpected end of file.");
            line = line.trim();
            if (line.equals("}")) break;
            lines.add(line);
        }
        return lines;
    }

    /**
     * Adds restriction for on tags.
     * Convenience function for adding restriction with the context specified
     * by a single value for a single slot.
     *
     * @param aSlot slot specifying context
     * @param aVal value for that slot
     * @param aReqs requirements for all tags having aVal in slot aSlot.
     */
    private void addRestriction(int aSlot, char aVal, List<String> aReqs) {
        String ctx = ".{" + aSlot + "}" + aVal + ".*";
        addRestriction(ctx, aReqs);
    }

    /**
     * Adds restriction for on tags. Tags satisfying a context pattern
     * mustt satisfy at least one of the requirement pattern.
     *
     * @param aCtx context pattern
     * @param aReqs requirements patterns
     */
    private void addRestriction(String aCtx, List<String> aReqs) {
		// merge all requirement patters into a single disjunction pattern
        String reqStr = Cols.toString(aReqs, "(", ")", "|", "");

        Pattern ctx = Pattern.compile("^" + aCtx + "$");
        Pattern req = Pattern.compile("^" + reqStr + "$");

        restrictions.add(new Restriction(ctx, req));
    }

    static class Restriction {
        final private Pattern ctx;
        final private Pattern req;

        Restriction(Pattern aCtx, Pattern aReq) {
            ctx = aCtx;
            req = aReq;
        }

		/** Does a tag satisfy the restrictions */
        public boolean satisfies(String aTag) {
            if (! ctx.matcher(aTag).matches() ) return true;

            return req.matcher(aTag).matches();
        }
    }

}

