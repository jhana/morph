package morph.ma;

import morph.ma.module.Module;
import java.io.*;
import java.util.*;
import morph.common.UserLog;
import morph.util.MArgs;
import util.Pair;
import util.err.Err;
import util.err.Log;
import util.err.UException;
import util.io.XFile;



/**
 * This class is responsible for creating and initializing the analyzing (top level) and
 * supporting modules.
 */
public class MorphCompiler extends Compiler {

    public static MorphCompiler factory() {
        return new MorphCompiler();
    }

    /**
     * Creates a morphology object with all required modules.
     *
     * @param aArgs options object specifying:
     * <ul>
     *   <li>general morph options
     *   <li>which modules to load
     *   <li>individual modules' options
     * </ul>
      * @todo replace by options, deal somehow with tags etc.
     */
    public Morph compile(MArgs aArgs)  {
        mMorph = new Morph();
        args = aArgs;

        ulog = new UserLog(); // todo
        ulog.setDetailedError( args.getBool("debug.detailedError", false) );
        // @todo handle filter (see bak source)

        mMorph.prefs = args.getStrings("prefixes");
        while (mMorph.prefs.remove(""));   // remove all empty prefixes (@todo supported by args)

        mDataPath = args.getDir("dataPath", null);
        mBinPath = args.getDir("binPath", mDataPath);

        loadModules();

        if (ulog.getErrors() > 0) {
            System.err.printf("There were %d errors.", ulog.getErrors());
            return null;
        }

        return mMorph;
    }

// =============================================================================
// Implementation <editor-fold desc="Implementation">
// ==============================================================================
    /**
     * Result of the compilation
     */
    private Morph mMorph;
    private Map<String, Module> mModules;
    private String mCurModuleName; // for reporting

    private File mDataPath;
    private File mBinPath;


    private void loadModules() {
        mModules = mMorph.modules = new HashMap<String, Module>();
        mMorph.modulesList = new ArrayList<Module>();

        // --- get module names in proper order; possibly with stop flags ---
        List<String> moduleOrderStr = args.getStrings("moduleOrder");
        System.err.println("moduleOrder " + moduleOrderStr);

        // --- remove disabled modules (keeping the stop flags in place) ---
        for (Iterator<String> i = moduleOrderStr.iterator(); i.hasNext();) {
            String str = i.next();
            if (str.equals("!")) continue;  // skip stop flag
            if ( !args.getBool(str + ".enabled", true) ) {
                i.remove();
            }
        }

        // --- load and initialize modules ---
        List<Pair<String,Boolean>> modules = getModuleOrder(moduleOrderStr);
        for(Pair<String,Boolean> nameStop : modules) {
            mCurModuleName = nameStop.mFirst;
            loadModule(nameStop.mSecond);
        }
    }


    /**
     * Parses a module order string for the names of modules and stop flags.
     *
     * @param aModuleOrder module order specification modules and "!" flags, multiple flags are treated as one.
     * @return a list of modules each with a flag which is true if the module name was followed by "!"
     */
    protected List<Pair<String,Boolean>> getModuleOrder(List<String> aModuleOrder) {
        List<Pair<String,Boolean>> modules = new ArrayList<Pair<String,Boolean>>();

        for (int i = 0; i < aModuleOrder.size(); i++) {
            String module = aModuleOrder.get(i);
            if (module.equals("!")) continue;               // duplicate flag (all the modules preceding it are disabled)

            boolean stop = false;
            if (i+1 < aModuleOrder.size()) {
                if (aModuleOrder.get(i+1).equals("!")) {
                    stop = true;
                    i++;
                }
            }
            modules.add(new Pair<String,Boolean>(module,stop));
        }

        return modules;
    }


    /**
     * Loads the specified module, (re)compiling it if necessary.
     */
    private void loadModule(boolean aStopFlag) { //(String mCurModuleName) {
        info("Loading ...");

        Module tmp = createModuleObject();
        if (tmp == null) return;
        tmp.init(mCurModuleName, args);

        // --- Determine if the module should be (re)compiled ---
        int compilation = args.getInt("compilation", 1);   // 0 - never, 1 - if necessary, 2 - always
        boolean compile = false;
        // TODO remove when inteligent compilation is fixed
        compilation = 2;

        if (compilation == 1) {
            // --- is compilation needed? ---
            File binaryFile = getBinaryFile();
            if (binaryFile == null) {
                compile = true;
            }
            else {
                // -- check if any of the module's file has been changed --
                for (String fileKey : tmp.filesUsed()) {
                    List<XFile> files = args.getFilesOrFile(mCurModuleName + "." + fileKey, mDataPath);
                    if (files == null || files.isEmpty()) {
                        handleModuleError("the key %s(s) must specify module's files", fileKey);
                        return;
                    }

                    for (XFile file : files) {
                        if (file.file().lastModified() > binaryFile.lastModified()) {
                            compile = true;
                            break;
                        }
                    }
                }
            }
        }
        else if (compilation == 2) {
            compile = true;
        }

        // --- compile or load module ---
        Module module = ( compile ) ? compile(tmp) : tryToLoadBinary(tmp, compilation > 0);
        if (module == null) return;


        // --- configure module ---
        try {
            module.configure();
        }
        catch(UException e) {
            ulog.handleError(e, "Cannot configure module");
        }
        module.setStopWhenNotEmpty(aStopFlag);

        mModules.put(mCurModuleName, module);
        mMorph.modulesList.add(module);
        info("Loaded.");
    }

    private Module tryToLoadBinary(Module aModule, boolean aAllowCompilation) {
        try {
            return loadBinary();
        }
        catch(Exception e) {
            info("Binary loading failed.");

            if (aAllowCompilation) {
                return compile(aModule);
            }

            return null;
        }
    }

    private Module createModuleObject() {
        //info("Creating module object.");
        String moduleClass = null;
        try {
            moduleClass = args.getString(mCurModuleName + ".class");
            if (moduleClass == null) {
                handleModuleError("no class specified for the module", mCurModuleName);
                return null;
            }
            return (Module) Class.forName(moduleClass).newInstance();
        }
        catch(InstantiationException e) {
            handleModuleError("module class %s not found", moduleClass);
        }
        catch(IllegalAccessException e) {
            handleModuleError("module has not public constructor");
        }
        catch(ClassNotFoundException e) {
            handleModuleError("module class %s was not found", e);
        }
        return null;
    }

    private Module compile(Module aModule) {
        //info("Compiling ... ");
        try {
            aModule.compile(ulog);

            // TODO uncomment when inteligent compilation is fixed
            // if (errors == 0) saveBinary(aModule);

            return aModule;
        }
        catch(IOException e) {
            handleModuleError("I/O problems - %s", e);
        }
        catch(Throwable e) {
            handleModuleError("Loading of the module failed - %s", e);
        }
        return null;
    }


    /**
     * Save a compiled module into a file.
     */
    private void saveBinary(Module aModule) {
        info("Saving binary file ...");
        try {
            File binFile = args.getFileS(mCurModuleName + ".binFile", mBinPath, mCurModuleName + ".bin").file();

            ObjectOutput s = new ObjectOutputStream(new BufferedOutputStream(new java.util.zip.GZIPOutputStream(new FileOutputStream(binFile))));

            s.writeObject(aModule);
            s.flush();
            s.close();
        // @todo better error handling
        } catch (Exception e) {
            info("Error during saving the binary object\n");
            Log.info(e.toString());
        }
    }

    /**
     * Load the precompiled object
     */
    private Module loadBinary() throws IOException, ClassNotFoundException {
        info("Loading a binary version ...");
        ObjectInput s = null;
        try {
            File file = getBinaryFile();
//            if (file == null) {
//                handleModuleError("no binary file specified for the module", aModuleName);
//                return null;
//            }

            s = new ObjectInputStream(new BufferedInputStream(new java.util.zip.GZIPInputStream(new FileInputStream(file))));
            Module module = (Module)s.readObject();
            module.init(mCurModuleName, args);
            return module;
        }
        finally {
            if (s != null) s.close();
        }
    }

    private File getBinaryFile() {
        return args.getFileS(mCurModuleName + ".binFile", mBinPath, mCurModuleName + ".bin").file();
    }


    /** todo use wrapped ulog */
    private void handleModuleError(String aFormat, Object ... aParams) {
        ulog.handleError("[" + mCurModuleName + "] Error - " + aFormat, aParams);
    }

    private void info(String aFormat, Object ... aParams) {
        System.err.println("[" + mCurModuleName + "] " + String.format(aFormat, aParams));  // TODO
    }

// </editor-fold>
}


