package morph.ma;

import lombok.Getter;
import morph.io.Filter;
import morph.common.UserLog;
import morph.ts.Tagset;
import morph.util.MArgs;


/**
 *
 * @author  Jiri
 */
public abstract class Compiler {
    @Getter protected UserLog ulog;

    /**
     * Options specified in the properties file. Specifies what are the analysis 
     * modules and what options for each module are. 
     */
    @Getter protected MArgs args;

    /**
     * Filter applied to input text (transliteration, etc.). Possibly identity.
     */
    @Getter protected Filter inFilter = Filter.cIdentityFilter;  //@todo configurable
    
    @Getter protected Tagset tagset = Tagset.getDef(); // todo!!!

    public void init(UserLog aUlog, MArgs aArgs) {
        ulog = aUlog;
        args = aArgs;
    }
    
    
// -----------------------------------------------------------------------------
// Errors
// -----------------------------------------------------------------------------


    /**
     * Prints the specified error message and increases the error counter.
     * Does not throw or terminate anything.
     */
    @Deprecated
    protected void handleWarning(String aMsg, Object ... aParams) {
        ulog.handleWarning(aMsg, aParams);
    }
    

}
