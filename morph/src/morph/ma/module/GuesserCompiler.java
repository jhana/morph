//package morph.ma.module;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.regex.Pattern;
//import pdt.int;
//import pdt.Tag;
//import pdt.Tags;
//import util.col.pred.AbstractPredicate;
//import util.col.Cols;
//import util.col.MultiMap;
//import util.col.pred.Predicate;
//import util.col.pred.Predicates;
//import util.err.Err;
//import util.err.UException;
//import util.io.IO;
//import util.io.LineReader;
//import util.io.XFile;
//import util.opt.Options;
//import util.str.StrSet;
//import util.str.Strings;
//
//
//
///**
// *
// * @author  Jiri
// */
//public class GuesserCompiler extends ModuleCompiler<Guesser> {
//    protected @Override void compileI() throws java.io.IOException {
//        mModule.mSuperlativeOk     = mArgs.getString("superlativeOk");
//        mModule.mGuessedSubPOS     = mModuleArgs.getString("guessingOk", "");
//        mModule.mMinStemLen        = mModuleArgs.getInt("minStemLen", 1);
//        mModule.mHandleDerivations = mModuleArgs.getBool("handleDerivations", false);
//
//        initParadigms();
//        // @todo better
//        // @todo assert + log
//        if (mModule.mParadigms == null || mModule.mParadigms.mClasses == null) {
//            //warning("No Paradigms or Paradimgs.Classes object loaded");
//            return;
//        }
//
//        // @todo generalize
//        mModule.mEpenCons = "";
//        String[] conss = mModule.mParadigms.mClasses.get("C");
//        if (conss != null)  {  // @todo warning
//            for (String c : conss)   mModule.mEpenCons += c;
//        }
//
//        if (mModule.mHandleDerivations) {
//            initDerivations();
//        }
//
//        System.out.println("tagFilter.enabled=" + mModuleArgs.getBool("tagFilter.enabled", false));
//        if (mModuleArgs.getBool("tagFilter.enabled", false)) {
//            initTagFilter(mModuleArgs.getSubtree("tagFilter"));
//        }
//
//        System.out.println("caseFilter.enabled=" + mModuleArgs.getBool("caseFilter.enabled", false));
//        if (mModuleArgs.getBool("caseFilter.enabled", false)) {
//            initCaseFilter(mModuleArgs.getSubtree("caseFilter"));
//        }
//
//        if (mModuleArgs.getBool("slotFilters.enabled", false)) {
//            initSlotFilters(mModuleArgs.getSubtree("slotFilters"));
//        }
//
//    }
//
//    private void initTagFilter(Options aOptions) throws java.io.IOException {
//        XFile file = aOptions.getFile("file", mDataPath, null);
//        MultiMap<String, Tag> form2tags = loadTagFilter(file);
//        mModule.addFilter(new Guesser.TagFilter(form2tags));
//        System.out.println("Loaded Tag filter: " + file);
//    }
//
//    private MultiMap<String, Tag> loadTagFilter(XFile aFile) throws IOException {
//        LineReader r = IO.openLineReader(aFile);
//        r.configureSplitting(Strings.cWhitespacePattern, 2, "Format must be <form> <case:freq>+");
//        r.allowMoreThanReq(true);
//
//        MultiMap<String, Tag> form2cases = new MultiMap<String,Tag>();
//
//        for(;;) {
//            String[] strs = r.readSplitNonEmptyLine();
//            if (strs == null) break;
//
//            Err.fAssert(strs.length > 1, "Wrong format");
//
//            String form = strs[0];
//            List<String> tagStrs = Cols.subList(strs, 1);
//            List<Tag> tags = Tags.fromStrings(tagStrs);
//            form2cases.addAll(form, tags);
//        }
//        r.close();
//        System.out.println("Case filter size " + form2cases.size());
//        return form2cases;
//    }
//
//
//
//    private void initSlotFilters(Options aOptions) throws java.io.IOException {
//        for (String key : aOptions.keySet()) {
//            Err.fAssert(key.length() == 1, "slot filter's key must be a single character slot code");
//
//            XFile file = aOptions.getFile(key, mDataPath, null);
//            Map<String, StrSet> prevForm2vals = load(file);
//            int slot = int.fromCode(key.charAt(0));
//            LtsFilter filter = new Guesser.PrevWordFilter(prevForm2vals, slot, "");
//            mModule.addFilter(filter);
//
//            System.out.printf("Loaded slot %s filter: %s\n", slot, file);
//        }
//    }
//
//
//    private void initCaseFilter(Options aOptions) throws java.io.IOException {
//        XFile file = aOptions.getFile("file", mDataPath, null);
//        Map<String, StrSet> form2cases = load(file);
//        mModule.addFilter(new Guesser.CaseFilter(form2cases, int.casex, "1"));
//        System.out.println("Loaded Case filter: " + file);
//    }
//
//    private void initGenderFilter(Options aOptions) throws java.io.IOException {
//        System.out.println("Gender filter");
//        Map<String, StrSet> form2cases = load(aOptions.getFile("file", mDataPath, null));
//        mModule.addFilter(new Guesser.CaseFilter(form2cases, int.gender, ""));
//    }
//
//    private Map<String, StrSet> load(XFile aFile) throws IOException {
//        final LineReader r = IO.openLineReader(aFile);
//        r.configureSplitting(Strings.cWhitespacePattern, 2, "Format must be <form> <case:freq>+");
//        r.allowMoreThanReq(true);
//
//        final Map<String, StrSet> form2cases = new HashMap<String,StrSet>();
//
//        for(;;) {
//            String[] strs = r.readSplitNonEmptyLine();
//            if (strs == null) break;
//
//            Err.fAssert(strs.length > 1, "Wrong format");
//
//            String form = strs[0];
//            List<String> caseFreqs = Cols.subList(strs, 1);
//
//            StringBuilder sb = new StringBuilder();
//            for (String caseFreq : caseFreqs) {
//                sb.append( Strings.splitIntoTwo(caseFreq, ':').mFirst );
//            }
//            StrSet cases = new StrSet(sb.toString());
//            form2cases.put(form, cases);
//        }
//        r.close();
//        System.out.println("Case filter size " + form2cases.size());
//        return form2cases;
//    }
//
//
//    protected void initParadigms() throws java.io.IOException {
//        // paradigms are not shared (different configuration for diff modules)
//        try {
//            mModule.mParadigms = new Paradigms();
//            mModule.mParadigms.init(mModuleId, mArgs);
//
//            // --- Predicate filtering paradigms ---
//            // @todo move support for this to Prdgm compiler (then add to other prdgm modules)
//            Predicate<DeclParadigm> prdgmPredicate = new ParadigmsCompiler.NormalPrdgmPredicate();
//
////            final List<String> ignoredPrdgms = mM oduleArgs.getStrings("prdgms.ignore");
////            if (ignoredPrdgms != null && !ignoredPrdgms.isEmpty()) {
////                System.out.println("Ignored paradigms: " + ignoredPrdgms);
////                Predicate<DeclParadigm> kept = new AbstractPredicate<DeclParadigm>() {
////                    public @Override boolean isOk(DeclParadigm aPrdgm) {
////                        return !ignoredPrdgms.contains( aPrdgm.id );
////                    }
////                };
////                prdgmPredicate = Predicates.and(prdgmPredicate, kept);
////            }
//
//            final Pattern ignoredPrdgms = mModuleArgs.getPattern("prdgms.ignore");
//            if (ignoredPrdgms != null) {
//                System.out.println("Ignored paradigms: " + ignoredPrdgms);
//                Predicate<DeclParadigm> kept = new AbstractPredicate<DeclParadigm>() {
//                    public @Override boolean isOk(DeclParadigm aPrdgm) {
//                        return !ignoredPrdgms.matcher( aPrdgm.id ).matches();
//                    }
//                };
//                prdgmPredicate = Predicates.and(prdgmPredicate, kept);
//            }
//
//            final Pattern keptPrdgms = mModuleArgs.getPattern("prdgms.keep");
//            if (keptPrdgms != null) {
//                System.out.println("Kept paradigms: " + ignoredPrdgms);
//                Predicate<DeclParadigm> kept = new AbstractPredicate<DeclParadigm>() {
//                    public @Override boolean isOk(DeclParadigm aPrdgm) {
//                        return keptPrdgms.matcher( aPrdgm.id ).matches();
//                    }
//                };
//                prdgmPredicate = Predicates.and(prdgmPredicate, kept);
//            }
//
//            final Predicate<Ending> endingPredicate =
//                mModule.mGuessedSubPOS.equals("") ?
//                    new ParadigmsCompiler.NormalEndingPredicate() :
//                    new ParadigmsCompiler.SubPosEndingPredicate(mModule.mGuessedSubPOS);
//
//            mModule.mParadigms.compile(prdgmPredicate, endingPredicate);
//            mModule.mParadigms.configure();
//            mModule.mMaxEndingLen = mModule.mParadigms.getMaxEndingLen();
//        } catch (UException e) {
//            handleError("Cannot load paradigms submodule - %s", e.getMessage());
//        }
//    }
//
//    
//    private void initDerivations() throws IOException {
//        try {
//            mModule.mDerivations = new Derivations();
//            mModule.mDerivations.init(mModuleId, mArgs);
//            mModule.mDerivations.compile(mModule.mParadigms);
//            mModule.mDerivations.configure();
////            mModule.mMaxEndingLen  = mModule.mParadigms.getMaxEndingLen();
//        }
//        catch(UException e) {
//            handleError("Cannot load paradigms submodule - %s", e.getMessage());
//        }
//        
//    }
// 
//        
//}
