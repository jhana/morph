package morph.ma.module;

import morph.common.UserLog;
import morph.ma.module.prdgm.Derivation;
import morph.ma.module.prdgm.DeclParadigm;
import morph.ma.module.prdgm.StemAnal;
import morph.ma.module.prdgm.StemRule;
import morph.ma.module.prdgm.Ending;
import java.util.*;
import java.util.ArrayList;
import java.util.regex.Pattern;
import lombok.Getter;
import morph.ma.lts.*;
import morph.ma.*;
import morph.ts.Tags;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.str.Strings;
import util.str.Transliterate;
import util.col.MultiMap;
import util.err.Err;

/**
 * Ending based guesser.
 * Supports result filtering.
 * 
 * @author  Jiri
 */
public final class FGuesser extends EndingAModule {

// ------------------------------------------------------------------------------
// Setup
// ------------------------------------------------------------------------------
    @Override
    public int compile(UserLog aUlog)  {
        return new FGuesserCompiler().init(this, moduleId, aUlog, args).compile();
    }

    @Override
    public int configure()  {
        mUseClasses            = moduleArgs.getBool("classes.enabled", false);
        //boolean useLemmaFilter = moduleArgs.getBool("filter.lemma.enabled",   false); //-- in compiler
        mMarkEpen              = moduleArgs.getBool("markEpen",   false);


        if ( moduleArgs.getBool("filter.leo.enabled", false) || moduleArgs.getBool("leo.enabled", false) ) {
            collectInfo  = true;   // leo requires info with ending length
            ltsFilters.add(new LeoFilter(
                moduleArgs.getPattern("leo.notCounted",  ""),
                moduleArgs.getPattern("leo.notFiltered", "")
            ));
        }

        return 0;
    }

    @Override
    public String[] filesUsed() {
        return new String[] {"classes.file", "prdgms.file", "filter.ft.file"};
    }

// ------------------------------------------------------------------------------
// Use
// ------------------------------------------------------------------------------

    private Set<Character> getFormTagFilter(String aForm, String[] aDecapForms) {
        if (mFormTagFilter == null) return null;

        if (aDecapForms == null) {
            return mFormTagFilter.get(aForm);
        }
        else if (aDecapForms.length == 1){
            return morph.util.Cols.multiGet(mFormTagFilter, aForm, aDecapForms[0]);
        }
        else {
            return morph.util.Cols.multiGet(mFormTagFilter, aForm, aDecapForms[0], aDecapForms[1]);
        }
    }
    /**
     *
     * @return false (morph does not stop_
     * @todo handle capitalization
     */
    @Override
    public boolean analyzeImp(String aCurForm, String[] aDecapForms, List<BareForm> aBareForms, Context aCtx, SingleLts aLTS) {
        //  System.out.printf("analyze %s\n", aCurForm);

        //System.out.println("Analyze " + aCurForm + " " + mMaxEndingLen);
        final Set<Character> formFilter = getFormTagFilter(aCurForm, aDecapForms);
        //System.out.println("FOrm filter " + aCurForm + "  " + formFilter);

        for(BareForm bareForm : aBareForms) {
            final String form = bareForm.form;
            //if (formFilter != null) counter++;

            final int maxLen = Math.min(mMaxEndingLen, form.length()-mMinStemLen);

            for(int endingLen = 0; endingLen <= maxLen; endingLen++) {
                String stemCand   = Strings.removeTail(form, endingLen);
                String endingCand = Strings.getTail(   form, endingLen);
                //System.out.println(stemCand + "+" + endingLen + " " + mParadigms.endings.getS(endingCand).size());
                for(Ending endingE : mParadigms.getEndings().getS(endingCand)) {
                    if (formFilter != null && formFilter.size() > 0 && !formFilter.contains( endingE.getTag().getPOS() ) ) continue;

                    if ( prefixesOk(bareForm, endingE) ) {
                        //handleEpen(aLTS, bareForm, stemCand, stemCand, endingE);    // -> handleStemChange -> tryToAdd
                        x(aLTS, bareForm, stemCand, endingE);
                    }
                }
            }
        }

        filter(aCtx, aLTS);

        return false;
    }

    // todo tmp
    // ending has to be indentical to surface form, stem can be modified
    private void x(SingleLts aLTS, BareForm aBareForm, String aStemCand, Ending aEnding) {
        Collection<StemAnal> stems = Arrays.asList(new StemAnal(aStemCand, aBareForm, aEnding, collectInfo));
        for (StemRule rule : aEnding.getStemRules()) {
            stems = applyRule(rule, stems);
        }

        for (StemAnal stem : stems) {
            tryToAdd(aLTS, stem);
        }
    }

    private Collection<StemAnal> applyRule(StemRule aRule, final Collection<StemAnal> aStemAnals) {
        final List<StemAnal> newAnals = new ArrayList<>();
        for (StemAnal stemAnal :  aStemAnals) {
            if (aRule.isOptional()) {
                newAnals.add(stemAnal.copy());
            }

            final StemAnal newStemAnal = aRule.apply(stemAnal);
            if (newStemAnal != null) {
                newAnals.add(newStemAnal);
                if (newStemAnal.getInfo() != null) {
                    newStemAnal.getInfo().add(aRule);
                }
            }
        }
        return newAnals;
    }

    void tryToAdd(final SingleLts aLTS, final StemAnal aStemAnal) {
        final Ending ending = aStemAnal.getEnding();

        if (!ending.getPrdgm().stemPhonologyOk(aStemAnal.getStem())) return;

        final Tag tag = ending.getTag(aStemAnal.getBareForm().ne());   // @todo superlative

        if (tag == null)  // todo this should be checked during compilation
            Err.iErr(Transliterate.transliterate(ending.getEnding()) + " " + ending.getTag() + " " + ending.isNeOk());

        final String lemma = aStemAnal.getStem() + ending.getPrdgm().lemmatizingEnding.getEnding();
        final String lemmaWB = aStemAnal.getStem() + "+" + ending.getPrdgm().lemmatizingEnding.getEnding();


        if (!ending.getPrdgm().lemmaPhonologyOk(lemmaWB)) return;

        if (! ltFiltersOk(aStemAnal.getBareForm().form, lemma, tag, ending) ) return;

        final DecInfo info = (collectInfo) ? new DecInfo(aStemAnal.getStem(), aStemAnal.getSurfaceForm(), ending, false) : null;

        aLTS.add(lemma, tag, info);

//        if (mHandleDerivations)
//            derivations(aLTS, aBareForm, aStemCand, ending, tag, aPrdgm, info);
    }


//        if (prdgm.mEpenStem == 0 && aEnding.getStemId() != 0 && xxCC(aStemCand)) {  // lematizing stem can contain epenthesis
////            System.out.println("Epen 1");
//            String eStem = Strings.insertB(aStemCand, 1, 'e');       // insert e   // pisn-i -> pisEn,
//            handleTailChange(aLTS, aBareForm, aActualStem, eStem, aEnding, prdgm, true);
//        }
//        else if (prdgm.mEpenStem > 0 && aEnding.getStemId() == prdgm.mEpenStem && xxCeC(aStemCand)) { // current c-stem can contain epenthesis
////            System.out.println("Epen 2");
//            String noEStem = Strings.deleteCharB(aStemCand, 1);      // delete e  // her -> hr-a
//            handleTailChange(aLTS, aBareForm, aActualStem, noEStem, aEnding, prdgm, true);
//        }
//        handleTailChange(aLTS, aBareForm, aActualStem, aStemCand, aEnding, prdgm, false);
//    }



    //int counter = 0;

// =============================================================================
// Implementation <editor-fold desc="Implementation">
// ==============================================================================


    /*
     * String of subPOS that allow guessing.
     * language: guesser.guessedSubPOS
     */
    String mGuessedSubPOS;

    boolean mUseClasses;


    boolean mMarkEpen;

    String mEpenCons;   // @todo regex instead, can be improved

// =============================================================================
// filtering @todo create a achain of pluggable filters
// =============================================================================

    boolean mLeo;    // longest ending only
    //boolean mUseLemmaFilter;

    final List<LtFilter> ltFilters = new ArrayList<LtFilter>();
    final List<LtsFilter>   ltsFilters = new ArrayList<LtsFilter>();

    MultiMap<String,Character> mFormTagFilter;


    private void filterLts(Context aCtx, Lts aLts) {
        for (LtsFilter filter : ltsFilters) {
            filter.filter(aCtx, aLts);
        }
    }

    /** Filters the lts structure as a whole */
    public static interface LtsFilter {
        void filter(Context aCtx, Lts aLts);
    }

    public final static LtsFilter okLtsFilter = new LtsFilter() {
        @Override public void filter(Context aCtx, Lts aLts) {}
    };

    /** If one or more lt-items have the correct lemma(s), remove the other ones */
    public static class ConservativeLemmaFilter implements LtsFilter {
        private final MultiMap<String,String> form2Lemmas;    // todo make final

        public ConservativeLemmaFilter(MultiMap<String, String> form2Lemmas) {
            this.form2Lemmas = form2Lemmas;
        }

        @Override public void filter(final Context aCtx, final Lts aLts) {
            final Set<String> lemmas = form2Lemmas.get(aCtx.curForm());
            if (lemmas == null) return;

            if (noKnownLemma(aLts, lemmas) && lemmas.size() == 1) {
                aLts.setLemma(lemmas.iterator().next());
            }
            else {
                for (Iterator<Lt> it = aLts.col().iterator(); it.hasNext();) {
                    Lt lt = it.next();
                    if (!lemmas.contains(lt.lemma())) it.remove();
                }
            }
        }

        private boolean noKnownLemma(final Lts aLts, final Set<String> aDesiredLemmas) {
            for (Lt lt : aLts.col()) {
                if (aDesiredLemmas.contains(lt.lemma())) return false;
            }
            return true;
        }

    }

   public static class LeoFilter implements LtsFilter {
        private final Pattern notCounted;
        private final Pattern notFiltered;

        public LeoFilter(Pattern notCounted, Pattern notFiltered) {
            this.notCounted = notCounted;
            this.notFiltered = notFiltered;
        }

        @Override public void filter(final Context aCtx, final Lts aLts) {
            // --- find the maximal ending len (excluding certain subPos) ---
            // HO: ie. maxLen = max(mLts.filter(!tag.matches(notCounted)), endingLen())
            int maxLen = -1;
            for (Lt lt : aLts.col()) {
                if (maxLen < lt.info().getEndingLen() && !lt.tag().matches(notCounted)) {
                    maxLen = lt.info().getEndingLen();
                }
            }

            // @todo not a capital within a sentence (otherwise => probably a name => probably noun) ---

            // --- Delete every analysis with ending smaller than maximal
            // HO: mLts.remove( \x . x.endingLen() < maxLen && !x.subPOSIn(mNotFiltered) )
            for (Iterator<Lt> i = aLts.iterator(); i.hasNext();) {
                Lt lt = i.next();
                if (lt.info().getEndingLen() < maxLen) {
                    if (!lt.tag().matches(notFiltered) ) {
                        i.remove();
                    }
                }
            }
        }
    };


    private boolean ltFiltersOk(final String aForm, final String aCandidateLemma, final Tag aTag, final Ending aEnding) {
        for (LtFilter filter : ltFilters) {
            if (! filter.isOk(aForm, aCandidateLemma, aTag, aEnding) ) return false;
        }
        return true;
    }

    public static interface LtFilter {
        boolean isOk(String aForm, String aCandidateLemma, Tag aTag, Ending aEnding);
    }

    public final static LtFilter okFilter = new LtFilter() {
        @Override public boolean isOk(String aForm, String aCandidateLemma, Tag aTag, Ending aEnding) {
            return true;
        }
    };

    /**
     * Filtering by lemmas. If a form's lemma is known, only lt's with such
     * lemma are allowed to be guessed.
     *
     * @see ConservativeLemmaFilter
     */
    public static class LemmaFilter implements LtFilter {
        private final MultiMap<String,String> form2Lemmas;

        /**
         * @param form2Lemmas Optional map holding lemmas for some forms.
         * Can be null (no filtering is done in such a case).
         */
        public LemmaFilter(MultiMap<String, String> form2Lemmas) {
            this.form2Lemmas = form2Lemmas;
        }

        @Override public boolean isOk(final String aForm, final String aCandidateLemma, final Tag aTag, final Ending aEnding) {
            final Set<String> lemmas = form2Lemmas.get(aForm);
            return (lemmas == null || lemmas.contains(aCandidateLemma));
        }
    };

    public static class TagFilter implements LtFilter {
        /**
         * Optional structures holding lemmas for some forms.
         *
         * Filtering by lemmas. (If a form is found in this structure, only the
         * associated lemma is allowed to be guessed)
         */
        MultiMap<String,String> form2Lemmas;

        @Override public boolean isOk(final String aForm, final String aCandidateLemma, final Tag aTag, final Ending aEnding) {
            final Set<String> lemmas = form2Lemmas.get(aForm);
            return (lemmas == null || lemmas.contains(aCandidateLemma));
        }
    };

// =============================================================================



    // --- derivations ---
    boolean mHandleDerivations = true;
    Derivations mDerivations;      // prdgm id -> derivations



    // @todo more general
    private boolean prefixesOk(BareForm aBareForm, Ending aEnding) {
        return (!aBareForm.nej() || aEnding.isNejOk()) && (!aBareForm.ne() || aEnding.isNeOk());
    }

/*
    private void handleStemChange(LTXM aLTS, BareForm aBareForm, String aStemCand, Ending aEnding) {
        DeclParadigm prdgm = aEnding.prdgm;

        Collection<String> lemmatizingStems = aPrdgm.lemmatizingStems(aStemCand, aEnding.rootId); // @todo info about epen, applied rules, etc.
        for (String lemmatizingStem : lemmatizingStems) {
            tryToAdd(aLTS, aBareForm, lemmatizingStem, aEnding, aPrdgm);
        }
    }
 */

    private void handleEpen(SingleLts aLTS, BareForm aBareForm, String aActualStem, String aStemCand, Ending aEnding) {
        DeclParadigm prdgm = aEnding.getPrdgm();
//        System.out.printf("handleEpen %s %d. c-stem=%s %s, end=%s\n", prdgm.id, prdgm.mEpenStem, aStemCand, xxCC(aStemCand), aEnding);
//        System.out.printf("  %d %d %s -> %s\n", prdgm.mEpenStem, aEnding.getRootId(), xxCC(aStemCand),  (prdgm.mEpenStem == 0 && aEnding.getRootId() != 0 && xxCC(aStemCand)) );
//        System.out.printf("  %d %d %s -> %s\n", prdgm.mEpenStem, aEnding.getRootId(), xxCeC(aStemCand), (prdgm.mEpenStem > 0 && aEnding.getRootId() == prdgm.mEpenStem && xxCeC(aStemCand)) );


        if (prdgm.epenStem == 0 && aEnding.getStemId() != 0 && xxCC(aStemCand)) {  // lematizing stem can contain epenthesis
//            System.out.println("Epen 1");
            String eStem = Strings.insertB(aStemCand, 1, 'e');       // insert e   // pisn-i -> pisEn,
            handleTailChange(aLTS, aBareForm, aActualStem, eStem, aEnding, prdgm, true);
        }
        else if (prdgm.epenStem > 0 && aEnding.getStemId() == prdgm.epenStem && xxCeC(aStemCand)) { // current c-stem can contain epenthesis
//            System.out.println("Epen 2");
            String noEStem = Strings.deleteCharB(aStemCand, 1);      // delete e  // her -> hr-a
            handleTailChange(aLTS, aBareForm, aActualStem, noEStem, aEnding, prdgm, true);
        }
        handleTailChange(aLTS, aBareForm, aActualStem, aStemCand, aEnding, prdgm, false);
    }

    private void handleTailChange(SingleLts aLTS, BareForm aBareForm, String aActualStem, String aStemCand, Ending aEnding, DeclParadigm aPrdgm, boolean aEpen) {
        if (aEnding.getStemId() != 0) {      // ? exclude epenthesis??
            String lemmatizingStem = aPrdgm.lemmatizingStem(aStemCand, aEnding.getStemId());
            tryToAdd(aLTS, aBareForm, aActualStem, lemmatizingStem, aEnding, aPrdgm, aEpen);
        }

        tryToAdd(aLTS, aBareForm, aActualStem, aStemCand, aEnding, aPrdgm, aEpen);
    }




    void tryToAdd(SingleLts aLTS, BareForm aBareForm, String aActualStem, String aStemCand, Ending aEnding, DeclParadigm aPrdgm, boolean aEpen) {
        if (!aPrdgm.stemPhonologyOk(aStemCand)) return; // @todo move to Prdgm.lemmatizingStems

        final Tag tag = aEnding.getTag(aBareForm.ne());   // @todo superlative
        if (tag == null) Err.iErr(Transliterate.transliterate(aEnding.getEnding()) + " " + aEnding.getTag() + " " + aEnding.isNeOk());

        final String lemma = aStemCand + aPrdgm.lemmatizingEnding.getEnding();

        if (! ltFiltersOk(aBareForm.form, lemma, tag, aEnding) ) return;

        final DecInfo info = (collectInfo) ? new DecInfo(aStemCand, aActualStem, aEnding, aEpen) : null;

        aLTS.add(lemma, tag, info);

        if (mHandleDerivations)
            derivations(aLTS, aBareForm, aStemCand, aEnding, tag, aPrdgm, info);
    }


    // form=soudru��t� -> aStem=soudru�sk, aEnding=�
    private void derivations(SingleLts aLTS, BareForm aBareForm, String aStem, Ending aEnding, Tag aTag, DeclParadigm aPrdgm, final DecInfo aDeclInfo) {
        final Collection<Derivation> derivations = mDerivations.getDerivations(aPrdgm.id);
        if (derivations == null) return;

        for (Derivation derivation : derivations) {
            if (aStem.endsWith(derivation.mSuffix)) {                               // suffix=sk
                String rootCand   = Strings.removeTail(aStem, derivation.mSuffix);  // rootCand=soudru�
                addDerivation(aLTS, derivation, rootCand, aEnding, aTag, aDeclInfo);  // @todo overgenerates - maybe this would be translated by some subrule, do the same everywhere

                String mergePointRoot = derivation.mRootRule.translateTail(rootCand, 1, 0);
                if (!mergePointRoot.equals(rootCand))
                    addDerivation(aLTS, derivation, mergePointRoot, aEnding, aTag, aDeclInfo);
            }
        }
    }

    private void addDerivation(final SingleLts aLTS, final Derivation derivation, final String mergePointRoot, final Ending aEnding, final Tag aTag, final DecInfo aDeclInfo) {
        if (derivPhonologyOK(derivation,mergePointRoot)) {
            // @todo handle epenthesis
            if (derivation.mBaseParadigm.stemPhonologyOk(mergePointRoot)) {
                DerInfo info = (collectInfo) ? new DerInfo(mergePointRoot, derivation, false, aDeclInfo) : null;
                aLTS.add(mergePointRoot + derivation.mBaseParadigm.lemmatizingEnding.getEnding(), aTag, info);
            }

            String lemmatizingRoot = derivation.mBaseParadigm.lemmatizingStem(mergePointRoot, derivation.mBaseMergePoint);

            if (derivation.mBaseParadigm.stemPhonologyOk(lemmatizingRoot)) {
                DerInfo info = (collectInfo) ? new DerInfo(lemmatizingRoot, derivation, false, aDeclInfo) : null;
                aLTS.add(lemmatizingRoot + derivation.mBaseParadigm.lemmatizingEnding.getEnding(), aTag, info);
            }
        }
    }


    // @todo move to derivation; make a RootTails or even Phonotactics class (different instances - simple tails, regex, simple characters)
    private boolean derivPhonologyOK(Derivation aDerivation, String aRoot) {
        if (aDerivation.mRootTails == null || aDerivation.mRootTails.isEmpty()) return true;
        // -- go thru the class --
        // @todo optimize (similarly as stem rule)
        for (String c : aDerivation.mRootTails) {
            if (aRoot.endsWith(c)) return true;
        }

        return false;
    }


    // @todo regex
    private boolean xxCeC(String aString) {
        return
            (aString.length() > 3) &&
            Strings.charInB(aString, 0, mEpenCons) &&
            Strings.charAtB(aString, 1) == 'e' &&
            Strings.charInB(aString, 2, mEpenCons);
    }

    private boolean xxCC(String aString) {
        return
            (aString.length() > 2) &&
            Strings.charInB(aString, 0, mEpenCons) &&
            Strings.charInB(aString, 1, mEpenCons);
    }

    private void filter(Context aCtx, SingleLts aLTS) {
        filterLts(aCtx, aLTS);

        if (false) {
            filterByPrep(aLTS, aCtx);   
        }
    }

    /**
     * o-6 Hanky-2 mamince-6
     * o-6 detem-3 venovanem-6 darku-6
     * But noun-6 cannot be preceded by non-6 preposition
     */
    private void filterByPrep(Lts aLTS, Context aCtx) {
        if (aCtx.getPrevAnals(0) == null) return;

        Collection<Tag> prevTags = aCtx.getPrevAnals(0).tags();
        // @todo require punctuation to follow

        // must be nonabiguously prep
        if (! Tags.allTags(prevTags, Tagset.cPOS, 'R') ) return;

//        Set<Character> cases =  Tags.collectValues(prevTags, int.casex);
//
//        // --- go thru current analysis ---
//        for (Tag tag : aLTS.tags()) {
//            if (tag.POSIn("NAP") && ! caseOk(tag, cases) ) {
//                aLTS.remove(tag);
//            }
//        }

    }


// </editor-fold>

}
