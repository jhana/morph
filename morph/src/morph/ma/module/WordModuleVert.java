package morph.ma.module;

import morph.common.UserLog;

/**
 * todo the same as WordModule except reader
 * @author  Jirka Hana
 */
public class WordModuleVert extends WordModule {
    @Override
    public int compile(UserLog aUlog)  {
        return new WordModuleCompilerVert().init(this, moduleId, aUlog, args).compile();
    }
}
