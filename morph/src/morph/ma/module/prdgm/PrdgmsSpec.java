package morph.ma.module.prdgm;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import util.col.MultiMap;

/**
 *
 * @author jirka
 */
public class PrdgmsSpec {
    @Getter final Map<String,PrdgmSpec> id2prdgmSpec = new HashMap<String,PrdgmSpec>();
    @Getter final MultiMap<String,String> id2stringSet = new MultiMap<String, String>();

    public boolean addPrdgmSpec(PrdgmSpec aSpec) {
        PrdgmSpec old = id2prdgmSpec.put(aSpec.getId(), aSpec);
        if (old != null) System.err.println("OLD: " + old.getId());    // todo tmp
        return old == null;
    }

    public void addStringConstraint(String aId, Collection<String> aStrings) {
        // todo check for duplicate
        id2stringSet.addAll(aId, aStrings);
    }
    //mModule.mClasses.put(, );

}
