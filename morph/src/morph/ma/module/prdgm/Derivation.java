package morph.ma.module.prdgm;

import java.util.Set;
import morph.util.string.StringChangeRule;
import util.str.Cap;

/**
 * @todo why to subclas  prdgm
 * @author Jirka
 */
public class Derivation extends Paradigm {
    public String mSuperParadigm;

    /** paradigm of the words this derivation can be applied to */
    public DeclParadigm mBaseParadigm;

    /** id of the stem of the base-paradimg the suffix is attached to (i.e. id of the root) */
    public int mBaseMergePoint;        // past stem

    //int mAddMergePoint;         // currently always 0
    /** Condition on the root */
    public Set<String> mRootTails;

    /** Rule applied to the rule before adding the suffix */
    public StringChangeRule mRootRule;   // must be binary

    /** Suffix to add to the root */
    public String mSuffix;

    /** paradigm containing the endings attached after the suffix */
    public DeclParadigm mPrdgm;

    public Cap mCap = null;
    public String mExample;

    public Derivation(String aId) {
        super(aId);
    }


    @Override
    public String toString() {
        return id + " " + mSuperParadigm + " " + mSuffix + " " + mCap;
    }

    @Override
    protected int getPossEndingsNrImpl() {
        return mPrdgm.getPossEndingsNr();
    }

}
