package morph.ma.module.prdgm;

import util.col.pred.AbstractPredicate;
import util.col.pred.Predicate;
import util.col.pred.Predicates;

/**
 *
 * @author jirka
 */
public class Shelf {
     /**
     * A commonly used predicate used to drop non-productive paradigms.
     * @see #setPrdgmFilter(Predicate)
     */
    public static class NormalPrdgmPredicate extends AbstractPredicate<DeclParadigm> {
        @Override
        public boolean isOk(DeclParadigm aPrdgm) {
            return aPrdgm.type == DeclParadigm.Type.normal;
        }
    };

    /**
     * A commonly used predicate to keep only endings
     * of productive paradigms
     * @see #setEndingFilter(Predicate)
     */
    public static class NormalEndingPredicate extends AbstractPredicate<Ending> {
        @Override
        public boolean isOk(final Ending aEnding) {
            return aEnding.getPrdgm().type == DeclParadigm.Type.normal;
        }
    };

    /**
     * A commonly used predicate to keep only endings with a particular subpos
     * of productive paradigms
     * @see #setEndingFilter(Predicate)
     */
    public static class SubPosEndingPredicate extends AbstractPredicate<Ending> {

        private final String allowedSubPos;

        public SubPosEndingPredicate(String aAllowedSubPos) {
            allowedSubPos = aAllowedSubPos;
        }

        @Override
        public boolean isOk(final Ending aEnding) {
            return (aEnding.getTag().subPOSIn(allowedSubPos)
                    && aEnding.getPrdgm().type == DeclParadigm.Type.normal);
        }
    };

    /**
     * Only paradigms satisfying the supplied predicate will be kept.
     * The removed paradigms are still loaded and can be referred to by other
     * paradigms, but they are removed at the end of compilation and are
     * not used in analysis.
     *
     * @param aPredicate
     * @see #NormalPrdgmPredicate
     */
    public void setPrdgmFilter(Predicate<DeclParadigm> aPredicate) {
        mPrdgmPredicate = Predicates.and(new NonTemplatePredicate(), aPredicate);
    }

    /**
     * Only ending satisfying the supplied predicate and belonging to a paradigm
     * satisfying prdgmFilter will be kept.
     *
     * @param aPredicate
     * @see #SubPosEndingPredicate
     * @see #setPrdgmFilter(Predicate)
     */
    public void setEndingFilter(Predicate<Ending> aPredicate) {
        mEndingPredicate = aPredicate;
    }

// =============================================================================
// Implementation <editor-fold desc="Implementation">
// ==============================================================================
    /** True on non-template paradigms */
    private static class NonTemplatePredicate extends AbstractPredicate<DeclParadigm> {

        @Override
        public boolean isOk(DeclParadigm aPrdgm) {
            return aPrdgm.type != DeclParadigm.Type.template;
        }
    }

    /**
     * subpos of words allowing negative prefix
     * @todo generalize to any prefixes
     */
    private String mNegationOk;
    /**
     * subpos of words allowing superlative prefix
     */
    private String mSuperlativeOk;
    /**
     * Only paradigms satisfying this filter are kept
     */
    private Predicate<DeclParadigm> mPrdgmPredicate = new NonTemplatePredicate();
    /**
     * Only endings satisfying this filter are kept
     */
    private Predicate<Ending> mEndingPredicate = util.col.pred.Filters.trueFilter();

}
//
//
//
//        final Pattern filterPattern = mModuleArgs.getPattern("prdgms.idFilter");
//        if (filterPattern != null) {
//            log.info("prdgms.idFilter=%s", filterPattern);
//            setPrdgmFilter( new AbstractPredicate<DeclParadigm>() {
//               @Override
//                public boolean isOk(DeclParadigm aVal) {
//                    return filterPattern.matcher(aVal.id).matches();
//                }
//            });
//        }
//
//




//    void addEnding(final Ending aE, final boolean aDefEnding) {
//        prdgm.getEndings().add(aE);
//        // set as the lematizing ending/tag if requested or necessary
//        if (aDefEnding) {
//            prdgm.defTag = aE.getTag();
//            prdgm.lemmatizingEnding = aE.getEnding();
//        } else if (prdgm.lemmatizingEnding == null) {
//            if (prdgm.defTag == null || prdgm.defTag.equals(aE.getTag())) {
//                prdgm.defTag = aE.getTag();
//                prdgm.lemmatizingEnding = aE.getEnding();
//            }
//        }
//    }
//
//
//
//                    // @todo check consistency of stem rule and class
//                    // @todo translate class for each stem if necessary
//                    // class@X = class - (stemX.from - stemX.to) (but minuses can be partial !!! g vs. og
//
//    public void compile(String aNegationOk) {
//        final DeclParadigm prdgm = new DeclParadigm(id);
//
//        // defTag, prdgm.mTagTmpl, mNegationOk
//
//
//        if (mUseClasses) {
//            prdgm.classId = classId;
//            prdgm.permissibleClass = true;
//            prdgm.mClass = mModule.mClasses.get(classId);
//            assertTrue(prdgm.mClass != null, aR, "the class %s is not defined", classId);
//        }
//
//        System.out.println("--- Spec -----");
//        System.out.println("id=" + id);
//
//        System.out.println("-----");
//        // todo determine default stems
//        prdgm.nrOfStems();     // checks with rule, etc.
//
//        handleSubParadigms(aNegationOk);
//        handleInheritedEndings(aNegationOk);
//
//        if (prdgm.lemmatizingEnding == null) prdgm.cacheDefEnding();          // Determine the def ending, todo this is wrong!! randomly choosed
//
//        if (prdgm.type != DeclParadigm.Type.template) {
//            // rturn endings
//        }
//
//        // ending
//        boolean neOK = tags.get(0).subPOSIn(mNegationOk);
//        boolean nejOK = tags.get(0).subPOSIn(mSuperlativeOk);
//
//        // todo too lg-specific
//        //assertTrue(stemId != aHeader.prdgm.mEpenStem || ending.equals(""), aR, "€ associated with non-0 ending %s", stemId);
//
//        // --- prefixes tags ---
//        // todo drop/replace; too lg-specific
//
//        // Override inherited endings (i.e. remove any inherited ending with the same tag)
//        // @todo warning if ## and nothing deleted
//        if (aSpec.inheritedEndings != null) {
//            // HO: aHeader.inheritedEndings.remove(\e . e.tag = tag)
//            for (Iterator<Ending> i = aSpec.inheritedEndings.iterator(); i.hasNext();) {
//                if (tags.contains(i.next().getTag())) {
//                    i.remove();
//                }
//            }
//        }
//
//    }
//
//    private void handleSubParadigms(String aNegationOk) {
//        //final DeclParadigm subPrdgm = mModule.mParadigms.get(subPrdgmId);
//        //compiler.assertTrue(subPrdgm != null, aR, "the sub-paradigm %s is not defined", subPrdgmId);
//        //System.out.printf("   subprdgm: %s\n", subPrdgmId);
//
////        if (stemIds.isEmpty()) Cols.repeat(-1, subPrdgm.nrOfStems());
////        compiler.assertTrue(stemIds.size() == subPrdgm.nrOfStems(), aR, "Sub-paradigm %s requires %s stems", subPrdgmId, subPrdgm.nrOfStems());
//
//        for (SubPrdgmSpec spec : subPrdgmSpecs) {
//            // --- get inherited endings (create copy, so that the orig ending is not changed) - HO: map(copy) ---
//            for (Ending e : spec.prdgm.getEndings()) {
//                int stemId = spec.stemIds.get(e.getRootId());
//                // todo more modifications might be needed (root relabeling, etc.)
//                final Ending newE = new Ending(spec.prdgm, stemId, e.isNejOk(), e.isNeOk(), spec.suffix + e.getEnding(), e.getTag(), aNegationOk);
//                addEnding(newE, false);
//            }
//        }
//    }
//
//
//
//}
//private Tag readTag(final ReaderParser aR, final PrdgmSpec aHeader) {
//        String tagStr = null;
//        try {
//            tagStr = aR.getString("tag");
//        }
//        catch(Throwable e) {
//                if (true || detailedError) e.printStackTrace();
//                Err.fErr(e, "Cannot read tag.");
//            return null;
//        }
//
//        try {
//            if (aHeader.prdgm.mTagTmpl == null) {
//                // warn?
//                return tagset.fromStringFill(tagStr);
//            }
//            else {
//                return tagset.fromAbbr(tagStr, aHeader.prdgm.mTagTmpl, mNegationOk);
//            }
//        }
//        catch(Throwable e) {
//                if (detailedError) e.printStackTrace();
//                Err.fErr(e, "Tag format error (tag stri %s). tagTmpl=%s (prdgm=%s)", tagStr, aHeader.prdgm.mTagTmpl, aHeader.prdgm);
//            return null;
//        }
//    }


//
//
//
//
////        System.out.printf("--- All Endings ---");
////        for (Map.Entry<String,Set<Ending>> e : mModule.endings.entrySet()) {
////            System.out.println("   " + e);
////        }
////        System.out.printf("--- All Prdgms ---");
////        for (DeclParadigm p : mModule.mParadigms.values()) {
////            System.out.printf("Prdgm: %s\n", p.id);
////            for(Ending e : p.endings) {
////                System.out.println("   " + e);
////            }
////        }
//
//        mModule.mEndings.clear();
//        for (DeclParadigm p : mModule.mParadigms.values()) {
//            for(Ending e : p.getEndings()) {
//                mModule.mEndings.add(e.getEnding(), e);
//            }
//        }
//
////        System.out.printf("--- All Endings ---");
////        for (Map.Entry<String,Set<Ending>> e : mModule.endings.entrySet()) {
////            System.out.println("   " + e);
////        }
//
////        System.out.printf("--- no Endings ---");
////        for (Map.Entry<String,Set<Ending>> ee : mModule.endings.entrySet()) {
////            for(Ending e : ee.getValue()) {
////                if (e.getEnding().endsWith("no")) {
////                    System.out.println("   " + e);
////                }
////            }
////        }
//    }
//
//
//

        // defTag, prdgm.mTagTmpl, mNegationOk


//        if (mUseClasses) {
//            prdgm.classId = classId;
//            prdgm.permissibleClass = true;
//            prdgm.mClass = mModule.mClasses.get(classId);
//            assertTrue(prdgm.mClass != null, aR, "the class %s is not defined", classId);
//        }

        // todo determine default stems
//        prdgm.nrOfStems();     // checks with rule, etc.
//
//        // todo too lg-specific
//        //assertTrue(stemId != aHeader.prdgm.mEpenStem || ending.equals(""), aR, "€ associated with non-0 ending %s", stemId);
