package morph.ma.module.prdgm;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Getter;
import util.Pair;

/**
 * Note: this is temporary, needs more flexibility and polishing
 * @author jirka
 */
@Getter
@lombok.RequiredArgsConstructor
public class StrChange {
    private final List<Pair<Pattern, String>> changes;

//    public StrChange(List<Pair<Pattern, String>> changes) {
//        this.changes = changes;
//    }
    public static StrChange fromColonStr(final String aStr, final String aFromPref, final String aFromSuff, final String aToPref, final String aToSuff) {
        final List<Pair<Pattern, String>> changes = new ArrayList<Pair<Pattern, String>>();
        
        for (String item : aStr.split("\\:")) {
            String[] from_to = item.split("/");
            if (from_to.length > 2) throw new IllegalArgumentException();

            String from;
            String to;
            if (from_to.length == 2) {
                from = from_to[0];
                to = from_to[1];
            } 
            else {
                from = "";
                to = from_to[0];
            }
            
            if ("0".equals(from)) from = "";
            if ("0".equals(to)) to = "";

            from = aFromPref + from + aFromSuff;
            to = aToPref + to + aToSuff;
            
            changes.add(new Pair<Pattern, String>(Pattern.compile(from), to));
        }

        return new StrChange(changes);
    }

    public static StrChange fromColonPrefixStr(String aStr) {
        return fromColonStr(aStr, "\\A", "(.*?)\\Z", "", "$1");
    }
    
    public static StrChange fromColonSuffixStr(String aStr) {
        return fromColonStr(aStr, "\\A(.*?)", "\\Z", "$1", "");
    }

    public String change(final String aStr) {
        for (Pair<Pattern, String> pair : changes) {
            Matcher m = pair.mFirst.matcher(aStr);
            if (m.matches()) {
//                System.out.println("m " + m.group());
//                System.out.println("m " + m.replaceFirst(pair.mSecond));
//                System.out.println("m " + m.replaceAll(pair.mSecond));
                return m.replaceAll(pair.mSecond);
            }
        }
        return aStr;
    }

    @Override
    public String toString() {
        return "StrChange{" + "changes=" + changes + '}';
    }
    
}
