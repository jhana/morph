package morph.ma.module.prdgm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.regex.Pattern;
import lombok.Cleanup;
import lombok.Getter;
import morph.common.UserLog;
import morph.io.Filter;
import morph.io.ReaderParser;
import morph.util.string.ScrParser;
import util.err.UException;
import util.io.XFile;
import util.str.Strings;

/**
 * Parses the representation. Does only syntactic checking. No semantic/pragmatic checks or resolutions are performed.
 *
 * @todo replace by antlr
 * @author jirka
 */
public class ParadigmsReader {
    private final UserLog ulog;
    private @Getter final PrdgmsSpec fullSpec;

    public ParadigmsReader(UserLog aUserLog) {
        this.ulog = aUserLog;
        fullSpec = new PrdgmsSpec();
    }


    public void readIn(final Iterable<XFile> files, final Filter aFilter) throws java.io.IOException {
        for (XFile file : files) {
            ulog.info("Reading paradigm file: %s\n", file);
            @Cleanup ReaderParser r = new ReaderParser(file, aFilter, file.file().getName());
            readIn(r);
            ulog.info("Done reading paradigm file: %s\n", file);
        }
    }    

    
    public void readIn(final ReaderParser aR) throws java.io.IOException {
        for (;;) {
            try {
                if (aR.nextLine() == null) break;
                readEntry(aR);
            } catch (UException e) {   // format error skip the paradigm (was already reported)
                ulog.handleError(e, aR, "UException");
                aR.skipUntil("}");
            } catch (Throwable e) {  // other error skip the paradigm
                ulog.handleError(e, aR, "");
                aR.skipUntil("}");
            }
        }
    }
    
    public void readInClasses(XFile aFileName, final Filter aFilter) throws java.io.IOException {
        @Cleanup ReaderParser r = new ReaderParser(aFileName, aFilter, "classes");

        for (;;) {
            if (r.nextLine() == null) break;

            try {
                fullSpec.addStringConstraint( r.getString("setId"), Arrays.asList(r.getStringF("setStrings").split(":")) );
            } catch (java.util.NoSuchElementException e) {
                ulog.handleError(e);
            }
        }
    }
    

    private void readEntry(final ReaderParser aR) throws IOException {
        final String entryType = aR.getString("prdgmType (prdgm, prdgmC, tmpl)");
        //System.err.println("entry: " + new RusSciTranslit().transliterate(entryType));
        
        switch (entryType) {
            case "stringSet":
                readStringSet(aR);
                break;
            case "import":
                readImport(aR);
                break;
            case "con":
                readConstraint(aR);
                break;
            default:
                DeclParadigm.Type type = getParadigmType(entryType);
                ulog.assertTrue(type != null, aR, "Expecting an entry type keyword, found %s", entryType);
                readPrdgm(aR, type);
                break;
        }
    }

    /**
     * Reads in the paradigm type and id and creates the corresponding paradigm.
     */
    private DeclParadigm.Type getParadigmType(final String aPrdgmType) {
        switch (aPrdgmType) {
            case "prdgm":
                return DeclParadigm.Type.normal;
            case "prdgmC":
                return  DeclParadigm.Type.nonproductive;
            case "tmpl":
            case "miniprdgm":
                return  DeclParadigm.Type.template;
            default:
                return null;
        }
    }




    private void readStringSet(final ReaderParser aR) {
        addStringConstraint( aR.getString("setId"), aR.getStringF("setStrings") );
    }

    private void addStringConstraint(String aId, String aSetStr) {
        final List<String> strs = Arrays.asList( aSetStr.split("[:\\|]") );
        fullSpec.addStringConstraint(aId, strs);
    }


    private void readImport(final ReaderParser aR) throws IOException {
        final String fileName = aR.getString("imported file");
        ulog.info("import: " + fileName);

        final XFile curFile = aR.getReader().getFile();
        final XFile newFile = curFile.setShortFile(fileName);           // todo relative to it!! fileName can be "../../x.txt"
        
        readIn(Arrays.asList(newFile), aR.getFilter());
    }

    private void readConstraint(final ReaderParser aR) {
        throw new UnsupportedOperationException("Not yet implemented");
        // con lemma _ Vf.*
        // con lemma _ N..S1.*
        // con lemma _ A.YS1.*
        
    }


    private void readPrdgm(final ReaderParser aR, final DeclParadigm.Type type) throws IOException {
        String id = aR.getString("paradigmid");
        ulog.assertTrueW(!id.contains(":"), aR, "Prdgm id '%s' contains a colon. Inheritance colon must be separated by spaces.", id);

        final PrdgmSpec spec = new PrdgmSpec(String.valueOf(aR.getLineNumber()), id);
        spec.setLine(aR.getLineNumber() + " " + aR.line());

        spec.setType(type);
        if (!fullSpec.addPrdgmSpec(spec)) {
            ulog.handleError(aR, "Paradigm id %s already defined", spec.getId());
        }
        
        readPrdgmHeader(aR, spec);       // Read prdgm header

        // --- Read prdgm ending lines  ---
        for (;;) {
            try {
                aR.nextLine();
                if (aR.eatIf("}")) break;

                String firstToken = aR.getStringF("prdgmLineFirstWord");

                // subprdgm / ending
                if (firstToken.equals("@")) {
                   readSubPrdgm(aR, spec);
                }
                else {
                    readEndingLine(firstToken, aR, spec);
                }
            } catch (UException e) {
                ulog.handleError(e, aR, "");
            } catch (NoSuchElementException e) {
            }      // something was missing, ignore the whole line
        }
    }


    private final Pattern headerSwitchesPattern = Pattern.compile("\\-([aldcse€]|nc|epen)");
    private final Pattern subPrdgmSwitchesPattern = Pattern.compile("\\-[r]");

    private void readPrdgmHeader(final ReaderParser aR, final PrdgmSpec aSpec) {
        if (aR.hasMoreTokens()) {   // @todo : prdgm (modifications) : prdgm (modifications) ....
            if (aR.eatIf(":")) {
                readParentParadigm(aR, aSpec);
            }

            // todo check if each switch is max once, warn on unknown switches
            for(;;) {
                if (!aR.nextTokenMatches(headerSwitchesPattern))  {
                    aR.eatIf("{");              // todo but what if it is not the last token
                    if (aR.hasMoreTokens()) ulog.assertTrueW(false, aR, "Unknown switch %s", aR.nextToken());
                    break;
                }
                else if (aR.eatIf("-a")) {
                    aSpec.setTagTemplate( aR.getString("tagTmpl") );
                }
                else if ( aR.eatIf("-d")) {
                    aR.getString("defTag");
                    ulog.assertTrueW(false, aR, "-d switch no longer supported, mark the relevant line");
                }
                else if ( aR.eatIf("-l")) {
                    aSpec.setSampleLemma( aR.getStringF("sampleLemma") );
                }
                else if ( aR.eatIf("-c")) {
                    String tailStringSet = aR.getString("permisible stem tails");
                    if (tailStringSet.startsWith("\"")) {
                        ulog.assertTrue(tailStringSet.endsWith("\""), aR, "Closing quotes missing: %s", tailStringSet);
                        final List<String> strs = Arrays.asList( Strings.substringB(tailStringSet, 1, 1).split("[:\\|]") );
                        final String id = "_tmpStringSet-" + aSpec.getId();
                        fullSpec.addStringConstraint(id, strs);
                        //System.err.printf("Adding tail set: %s=%s\n", id, fullSpec.id2stringSet.get(id) );

                    }

                    //System.err.printf("Reading: prdgm %s setting stemTailSet %s\n", aSpec.getId(), tailStringSet);
                    aSpec.setStemTailSet(tailStringSet);
                }
                else if ( aR.eatIf("-s")) {
                    aSpec.setSStemRule(ScrParser.getStrChangeRule( aR.getStringF("stemRule")) );
                }
                // --- epenthesis ---
                else if ( aR.eatIf("-e") || aR.eatIf("-epen") || aR.eatIf("-€") ) {
                    aSpec.setEpenStem( aR.getInt("epen rootId") - 1 );   // id of the stem with epenthesis
                    aSpec.setEpenVowel( aR.getStringF("epen vowel") );   // string (vowel(s)) used in epenthesis, e.g. 'e' in Czech
                }
                else {
                    ulog.assertTrueW(false, aR, "Internal error, unknown switch %s, pls report", aR.nextToken());
                    break;
                }

//                // extension tokens, allow one argument
//                if (aR.nextTokenMatches(Pattern.compile("x.*")))  {
//                   aR.nextToken();
//                   if (!aR.nextTokenMatches(Pattern.compile("-[A-Za-z]+"))) aR.nextToken();
//                }
            }
            // @todo check consistency of -s & -? awith nr of stems
        }
    }



    /**
     * Reads info about a super-paradigm.
     *
     * @ if an error was encountered
     */
    private void readParentParadigm(final ReaderParser aR, final PrdgmSpec aSpec) {
        String superPrdgmId = aR.getString("superPrdgm");

        // todo while - iff matches -[xr]|+
        // -- changes to the ending initial character --
        StrChange change = null;
        if (aR.eatIf("-x")) {
            String changeRuleStr = aR.getStringF("ending change rule");
            try {
                change = StrChange.fromColonPrefixStr(changeRuleStr);
            }
            catch(IllegalArgumentException e) {
                ulog.handleError(e, aR, "Inheritance changing rule has wrong format");
            }
        }
        else {
            String extra  = aR.eatIf("+") ? aR.getString("Extra") : "";
            change = StrChange.fromColonPrefixStr(extra);

            if (aR.eatIf("-r")) {
                ulog.handleWarning("The -r inheritance switch is not supported yet");
                aR.getString("-r rule");
            }


//            StringChangeRule endingInitialRule = aR.eatIf("-r") ?
//            ScrParser.getStrChangeRule(aR.getStringF("ending init. rule")) : null;
        }

        // -- Extra string to add before the endings (between stems and the orig endings) --

        // -- Tag translations --
        List<String> tagModif = aR.eatIf("-t") ? parseTagModifStr(aR) : Collections.<String>emptyList();

        aSpec.addInheritance(superPrdgmId, change, tagModif, aR.getLineNumber());
    }

	/**
	 * Parses string specifying tag translation.
     * The format is <rule>(:<rule>)+  (i.e. the rules are separated by colons)
	 * Each rule has the format <slot>(<from-value><to-value>)+
	 * The last rule can have the form ".<to-value>" (using a dot as <from-value>) meaning anything is translated to <to-value>
	 */
    private List<String> parseTagModifStr(final ReaderParser aR) {
        String tagChangeStr = aR.getString("tagModif");

        if (tagChangeStr == null || tagChangeStr.isEmpty()) return Collections.emptyList();

        final List<String> tagModif = Arrays.asList(tagChangeStr.split("\\:"));
        // check the individual rules
		for (String str : tagModif) {
            ulog.assertTrue(Pattern.matches(".(..)+", str), aR,
                    "The tag modifying string '%s' in '%s' does not follow '<slot>(<from><to>)+' pattern", str, tagChangeStr);
            final int dotIdx = str.indexOf('.');
            ulog.assertTrue(dotIdx == -1 || dotIdx == str.length() -2, aR,
                    "The tag modifying string %s in %s does have dot in non-final pair.", str, tagChangeStr);	// todo allow '.' as value (use \.)
        }
        return tagModif;
    }



    /**
     * Adds info specified in a subparadigm
     * Format: @ suffix prdgm
     */
    private void readSubPrdgm(final ReaderParser aR, final PrdgmSpec aSpec) {
        StrChange change = null;
        String changeRuleStr = aR.getStringF("ending change rule");
        try {
            change = StrChange.fromColonPrefixStr(changeRuleStr);
        }
        catch(IllegalArgumentException e) {
            ulog.handleError(e, aR, "Inheritance changing rule has wrong format");
        }


        final String subPrdgmId = aR.getStringF("subPrdgm");

        // read stem parameters (mapping to the stem id's in the subparadigm)
        final List<Integer> stemIds = new ArrayList<>();
        while(aR.nextTokenMatches(cStemIdPattern)) {
            int stemId = Integer.valueOf(aR.getString("stemId").substring(1)) - 1;
            stemIds.add(stemId);
        }

        List<String> tagChange = Collections.emptyList();
        for (;;) {
            if (!aR.nextTokenMatches(subPrdgmSwitchesPattern))  break;


            if (aR.eatIf("-t")) {
                tagChange = parseTagModifStr(aR);
            }
        }

        aSpec.addSubPrdgm(subPrdgmId, change, stemIds, tagChange, aR.getLineNumber());
    }

    private final static Pattern cEndingSeparator = Pattern.compile("/");
    private final static Pattern cStemIdPattern = Pattern.compile("\\@\\d+");

    private final Set<String> cSuspiciousEnding_unclosedPrdgm = new HashSet<>(Arrays.asList(
        "stringSet", "import", "con", "prdgm", "prdgmC", "tmpl", "miniprdgm", "-a"
    ));
    private final Set<String> cSuspiciousEnding_other = new HashSet<>(Arrays.asList(
        "-a"
    ));
    
    
    
    private void readEndingLine(String firstToken, final ReaderParser aR, final PrdgmSpec aSpec) {
        ulog.assertTrueW(!cSuspiciousEnding_unclosedPrdgm.contains(firstToken), aR, "Ending '%s' looks suspicious, maybe you forgot to close the %s paradigm?", firstToken, aSpec.getId());
        ulog.assertTrueW(!cSuspiciousEnding_other.contains(firstToken), aR, "Ending '%s' looks suspicious, check the lines above", firstToken);
        ulog.assertTrueW(!firstToken.contains("@"), aR, "Suspicious ending '%s' containing '@'  (for sub-paradigms use: @ extra sub-paradigm-id).", firstToken);
        
        final List<String> endings = new ArrayList<>();
        for (String e : Strings.splitL(firstToken, cEndingSeparator)) {
            endings.add("0".equals(e) ? "" : e);
        }

        final List<String> tags = new ArrayList<>();
        do {
            String tagStr = aR.getString("tag");
            
            ulog.assertTrueW(!tagStr.startsWith("@") && !tagStr.endsWith("@"), aR, "Suspicious tag filler '%s' containing '@'  (for sub-paradigms use: @ extra sub-paradigm-id).", firstToken);
            
            tags.add(tagStr);
        } while (aR.hasMoreTokens() && !aR.nextTokenStartsWith("@"));

        final boolean defEnding = aR.eatIf("@d");
        //ulog.assertTrue(!defEnding || endings.size() == 1 && tags.size() == 1, aR,"Default ending marker @d allows one ending and one tag only.");
        // todo if more than use it for the first ending, first tag

        // --- stem tag (@1, @2, ..) ---
        int stemId = -1;
        if (aR.nextTokenMatches(cStemIdPattern)) {
            stemId = Integer.valueOf(aR.getString("stemId").substring(1)) - 1;
        }

        aSpec.addEndingLine(endings, tags, defEnding, stemId);
        // todo overriding - default, @- deletion, @+ adding,
    }
}
