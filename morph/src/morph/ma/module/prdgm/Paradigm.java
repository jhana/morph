package morph.ma.module.prdgm;

import java.util.Objects;

/**
 *
 * @author Jirka Hana
 */
public abstract class Paradigm implements Comparable, java.io.Serializable {
    /** A unique id of this paradigm */
    public String id;

// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------

    /**
     * Creates a new empty paradigm.
     *
     * @param aId the paradigms id (must be unique)
     */
    public Paradigm(String aId) {
        id = aId;
    }

    /**
     * Compares paradigms by their ids.
     */
    @Override
    public int compareTo(Object o) {
        return compareTo((Paradigm) o);
    }

    /**
     * Compares paradigms by their ids.
     */
    public int compareTo(Paradigm o) {
        return id.compareTo(o.id);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final Paradigm other = (Paradigm) obj;
        
        return Objects.equals(this.id, other.id);
    }
    
    

// -----------------------------------------------------------------------------
// Statistics
// -----------------------------------------------------------------------------

    /** Number of possible distinct endings (cached) */
    private int mPossEndingsNr = -1;

    /**
     * Returns the number of possible distinct endings (i.e. ending-strings).
     * For a paradigm with endings {0, -s, -ed, -ed}, this would enter 3 (counting -ed once).
     */
    public int getPossEndingsNr() {
        if (mPossEndingsNr == -1) {
            mPossEndingsNr = getPossEndingsNrImpl();
        }
        return mPossEndingsNr;
    }

    protected abstract int getPossEndingsNrImpl();

}
