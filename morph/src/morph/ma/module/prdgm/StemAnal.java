package morph.ma.module.prdgm;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import morph.ma.BareForm;

/**
 *
 * @author j
 */
@Getter
public class StemAnal {
    final String surfaceForm;
    String stem; // deeper stem (after applying some rules)
    final BareForm bareForm;
    final Ending ending;
    final List<StemRule> info;

    public StemAnal(String aSurfaceForm, BareForm aBareForm, Ending aEnding, boolean aCollectInfo) {
        this(aSurfaceForm, aSurfaceForm, aBareForm, aEnding, aCollectInfo ? new ArrayList<StemRule>() : null);
    }

    private StemAnal(String surfaceForm, String stem, BareForm bareForm, Ending ending, List<StemRule> info) {
        this.surfaceForm = surfaceForm;
        this.stem = stem;
        this.bareForm = bareForm;
        this.ending = ending;
        this.info = info;
    }

    public StemAnal copy() {
        return new StemAnal(surfaceForm, stem, bareForm, ending, info);
    }

}
