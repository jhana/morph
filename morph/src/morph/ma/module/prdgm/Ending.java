package morph.ma.module.prdgm;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import morph.ts.Tag;
import util.col.Cols;
import util.err.Err;

/**
 *
 * @todo negation/superlative is too language specific, replace by some more
 * general mechanism (and probably move out of here)
 *
 * @todo Ending attchement might be another stemRule
 *
 * @author  Jirka Hana
 */
@Getter
public class Ending implements Cloneable, Comparable, java.io.Serializable {
    private final DeclParadigm prdgm;
    /** For paradigms using different stems (knife-0, knive-s); zero based */
    private final int stemId;
    @Setter private String ending;
    private final Tag tag;
    private final Tag negatedTag;

    /** todo under development, should handle stem-tail changes, epenthesis and possibly more */
    private final List<StemRule> stemRules = new ArrayList<>();


    // this is too language specific drop/generalize
    private final boolean nejOk;
    private final boolean neOk;



    /**
     *
     * @param aPrdgm the paradigm this ending belongs too
     * @param aRootId id of the stem the ending attaches to.
     * @param aNejOk is superlative prefix ok with this ending (todo replace by a less lg specific mechanism)
     * @param aNeOK  is negative prefix ok with this ending (todo replace by a less lg specific mechanism)
     * @param aEnding the actual ending string
     * @param aTag the associated tag
     */
    public Ending(DeclParadigm aPrdgm, int aRootId, boolean aNejOk, boolean aNeOK, String aEnding, Tag aTag) {
        prdgm = aPrdgm;
        stemId = aRootId;
        nejOk = aNejOk;
        neOk = aNeOK;
        ending = aEnding;
        tag = aNeOK ? aTag.setNegation(aTag.getTagset().getNegationAffirmative()) : aTag;
        negatedTag = aNeOK ? aTag.setNegation(aTag.getTagset().getNegationNegated()) : null;
        Err.uAssert(aTag.getNegation() != 'N' || isNeOk(), "Tag %s is negated, but negation is not allowed.", aTag);
    }

    /**
     * Is superlative prefix ok with this ending
     * @todo replace by a less lg specific mechanism
     */
    public boolean isNejKo() {
        return !nejOk;
    }

    /**
     * Is negative prefix ok with this ending (todo replace by a less lg specific mechanism)
     * @todo replace by a less lg specific mechanism
     */
    public boolean isNeKo()  {
        return !neOk;
    }


    public Ending copy() {
        try {return (Ending) clone();}
        catch(CloneNotSupportedException e) {return null;}
    }

    @Override
    public String toString() {
        return String.format("prdgm id=%s, e=%s, tag=%s, stemId=%d (/%d), |rules|=%d (%s)", 
                prdgm.id, ending, tag, stemId, getPrdgm().nrOfStems(), 
                stemRules.size(), Cols.toString(stemRules));
    }

    @Override
    public int compareTo(Object o) {
        return compareTo((Ending) o);
    }

    public int compareTo(Ending o) {
        return tag.compareTo(o.tag);
    }

    public Tag getTag() {
        return tag;
    }

    public Tag getTag(boolean aNegation) {
        return (aNegation) ? negatedTag : tag;
    }


}