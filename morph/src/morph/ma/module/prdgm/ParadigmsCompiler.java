package morph.ma.module.prdgm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.Setter;
import morph.ma.module.ModuleCompiler;
import morph.ma.module.Paradigms;
import morph.ma.module.prdgm.PrdgmFilters.NonTemplatePredicate;
import morph.ma.module.prdgm.PrdgmSpec.EndingSpec;
import morph.ma.module.prdgm.PrdgmSpec.InheritanceSpec;
import morph.ma.module.prdgm.PrdgmSpec.SubPrdgmSpec;
import morph.ts.Tag;
import util.col.Cols;
import util.col.MultiMap;
import util.col.pred.Predicate;
import util.col.pred.Predicates;
import util.io.XFile;
import util.str.Strings;

/**
 * @todo filter prdgm/ending
 * @todo lemmatizingEnding
 * @todo stems
 * @todo create and pass context marked ulog
 * @todo epenthesis is currently very ad hoc
 * @todo stem-changes should be possible specified in referred paradigms, should be combined with main prdgm
 *
 * @author jirka
 */
public class ParadigmsCompiler extends ModuleCompiler<Paradigms> {
    private final static org.purl.net.jh.utilx.Logger log = org.purl.net.jh.utilx.Logger.getLogger(ParadigmsCompiler.class.getName());



    /**
     * Only paradigms satisfying this filter are kept.
     */
    private Predicate<DeclParadigm> prdgmPredicate = new PrdgmFilters.NonTemplatePredicate();

    @Getter final private Map<String,PrdgmStruct> prdgmStructs = new HashMap<>();


// =============================================================================
// Input
// =============================================================================
    private PrdgmsSpec fullSpec;

    private boolean useStemTails;

    /** Set of consonants referred to by epenthesis conditions. Defined as 'C' in stringSets. */
    private String epenConsPatternStr;

    /**
     * subpos of words allowing negative prefix
     * @todo generalize to any regex
     */
    private String mNegationOk;

    /**
     * subpos of words allowing superlative prefix
     * @todo generalize to any regex
     */
    private String mSuperlativeOk;



// =============================================================================
// Result
// =============================================================================

    @Getter private final MultiMap<String,Ending> endings = new MultiMap<>();
    @Getter private final Map<String,DeclParadigm> paradigms = new HashMap<>();
    @Getter private final MultiMap<String,String> stemTails = new MultiMap<>();  // todo replace with regexes

// =============================================================================


    /**
     * Only paradigms satisfying the supplied predicate will be kept.
     * The removed paradigms are still loaded and can be referred to by other
     * paradigms, but they are removed at the end of compilation and are
     * not used in analysis.
     *
     * @param aPredicate
     * @see #NormalPrdgmPredicate
     */
    public void setPrdgmFilter(Predicate<DeclParadigm> aPredicate) {
        prdgmPredicate = Predicates.and(new NonTemplatePredicate(), aPredicate);
    }


    @Override
    public void compileI() throws IOException {
        mNegationOk    = getArgs().getString("negationOk", "");
        mSuperlativeOk = getArgs().getString("superlativeOk", "");
        useStemTails     = mModuleArgs.getBool("classes.enabled", false);

        final ParadigmsReader reader = new ParadigmsReader(ulog);
        readIn(reader);

        for (Map.Entry<String,Set<String>> e :  fullSpec.id2stringSet.entrySet()) {
            stemTails.put(e.getKey(), e.getValue());
        }

        // todo-cons: epenthesis support
        Set<String> conss = stemTails.get("C");
        epenConsPatternStr = (conss == null) ? null : Cols.toString(conss, "(", ")", "|", "");

        // todo other fullSpec (classes, ...)

        for (PrdgmSpec prdgmSpec : fullSpec.id2prdgmSpec.values()) {
            try {
                getCompiledPrdgm(prdgmSpec.getId());
            }
            catch(Throwable e) {
                ulog.handleError(e, "[%s] Cannot compile paradigm id=%s\n", mModuleId, prdgmSpec.getId());
            }
        }
        
        finish();
    }

// =============================================================================
// Implementation
// =============================================================================


    private void readIn(ParadigmsReader reader) throws IOException {
        // for backward compatibility
        if (mModuleArgs.containsKey("classes.file")) {
            reader.readInClasses(getMFile("classes.file"), inFilter);
        }

        final List<XFile> files = getMFiles("prdgms.file");
        reader.readIn(files, inFilter);
        fullSpec = reader.getFullSpec();
    }




    // to prevent circular deps
    private final Set<String> prdgmsBeingCompiled = new HashSet<>();
    private PrdgmStruct getCompiledPrdgm(String aId) {
        PrdgmStruct prdgm = prdgmStructs.get(aId);
        if (prdgm == null) {
            final PrdgmSpec spec = fullSpec.id2prdgmSpec.get(aId);

            ulog.assertTrue( !prdgmsBeingCompiled.contains(aId), "Circular dependence of the paradigm %s on some of %s", prdgmsBeingCompiled); // todo remove id from prdgmsBeingCompiled
            prdgmsBeingCompiled.add(aId);

            prdgm = compileParadigm(spec);
            ulog.assertTrue(prdgm != null, "IE: Prdgm %s not compiled", aId);

            prdgmsBeingCompiled.remove(aId);

            prdgmStructs.put(aId, prdgm);
        }

        return prdgm;
    }


    /**
     * Paradigm being compiled, structure encapsulating paradigm-specification,
     * compiled paradigm and any intermediary structures.
     */
    @lombok.RequiredArgsConstructor
    @Getter
    @lombok.ToString
    private static class PrdgmStruct {
        /** Input. Specification of the paradigm as provided by the parser*/
        private final PrdgmSpec spec;
        /** Output. Compiled paradigm. */
        private final DeclParadigm prdgm;

        final List<Ending> inheritedEndings = new ArrayList<>();

        /** non-inherited endings (directly specified or via sub-paradigms */
        final List<Ending> coreEndings = new ArrayList<>();

        @Setter Ending coreLemmatizingEnding;
        
        /** Legal stems of this paradigm */
        @Setter private Pattern stemPattern;

        @Setter private String tagTemplate;


        @Setter private Ending inheritedLemmatizingEnding;

        public String getId() {
            return spec.getId();
        }

        private int defStemId = -1;

        public String getLocation() {
            return String.format("Prdgm id=%s, location=%s", spec.getId(), spec.getLocation());
        }
        
        /** 
         * Returns id of the default stem, i.e. the stem which is not explicitely marked by an @id. 
         * All endings that are not explicitely marked with an @id marker, will combine with the default stem.
         * The lowest number not used by the @id marker is returned
         */
        public int getDefStemId() {
            //System.out.println("..defstetmId: " + defStemId);
            if (defStemId == -1) {
                // --- use the lowest unused stem for endings without explicitely assigned stem ---
                final Set<Integer> stemIds = new HashSet<>();
                for (EndingSpec eSpec : getSpec().getEndingSpec()) {
                    if (eSpec.getStemId() != -1) stemIds.add(eSpec.getStemId());
                }
                final List<Integer> stemIdsList = new ArrayList<>(stemIds);
                Collections.sort(stemIdsList);
                defStemId = morph.util.Cols.firstUnused(stemIdsList, 0);
                //System.out.println("defstetmId: " + defStemId);
            }
            return defStemId;
        }


    };


    private PrdgmStruct compileParadigm(final PrdgmSpec aSpec) {
        //System.err.println("Compiling " + aSpec.getId());
        final String id = aSpec.getId();

        final DeclParadigm prdgm = new DeclParadigm(id);

        final PrdgmStruct prdgmStruct = new PrdgmStruct(aSpec,prdgm);
        prdgmStructs.put(id, prdgmStruct);

        prdgm.type = aSpec.getType();
        prdgmStruct.setTagTemplate(aSpec.getTagTemplate());
        prdgmStruct.getPrdgm().stemRule = aSpec.getSStemRule();
        if (aSpec.getStemTailSet() != null) {
//            System.err.printf("prdgm %s - tail-set %s\n", id, stemTails.get(aSpec.getStemTailSet()));
            prdgmStruct.getPrdgm().stemTails = stemTails.get(aSpec.getStemTailSet());
            prdgmStruct.getPrdgm().permissibleClass = true;
        }
        prdgmStruct.getPrdgm().sampleLemma = aSpec.getSampleLemma();

        prepareParentPrdgm(prdgmStruct);
        prepareCore(prdgmStruct);           // todo read core and subparadigms interweaved as  they go
        prepareSubPrdgms(prdgmStruct);

        combineEndings(prdgmStruct);

        compileStemChanges(prdgmStruct);


        finish(prdgmStruct);

//        System.err.println("--- Prdgm " + id + "---");
//        for (Ending e : prdgmStruct.prdgm.getEndings()) {
//            System.err.printf(" e: %s - rules: %s\n", e.getEnding(), Cols.toStringNl(e.getStemRules(), "  "));
//        }
//        System.err.println("----");
//
//
//        System.err.println("--- compiled " + aSpec.getId());
//        System.err.println("   " + prdgm.toString().replaceAll("\n", "\n    "));
//        System.err.println("--- --- --- ---");
        return prdgmStruct;
    }

    private void combineEndings(final PrdgmStruct aIntermediate) {
        final List<Ending> allEndings = new ArrayList<>();

        allEndings.addAll(aIntermediate.coreEndings);

        // add non-overriden inherited endigns - todo this must be done better so that one can add , override, drop endings
        final List<Tag> coreTags = new ArrayList<>();
        for (Ending e : aIntermediate.coreEndings) {
            coreTags.add(e.getTag());
        }

        for (Ending e : aIntermediate.inheritedEndings) {
            if (coreTags.contains(e.getTag())) {
                // lemmatizing ending was overriden
                if (e == aIntermediate.inheritedLemmatizingEnding)  {
                    // find first core edning with the same tag
                    for (Ending ce : aIntermediate.coreEndings) {
                        if ( e.getTag().equals(ce.getTag()) ) {
                            aIntermediate.inheritedLemmatizingEnding = ce;
                            //System.err.printf("[%s] Overriding le: %s -> %s\n", aIntermediate.getId(), e, ce);
                            break;
                        }
                    }
                    if (aIntermediate.inheritedLemmatizingEnding == e) System.out.println("Inherited lemmatizingEnding problem!! " + e);
                }
            }
            else {
                allEndings.add(e);
            }
        }

        aIntermediate.prdgm.getEndings().addAll(allEndings);


//        System.out.printf("Setting leme: core %s, inher %s, bak: %s\n", 
//                aIntermediate.getCoreLemmatizingEnding(),
//                aIntermediate.getInheritedLemmatizingEnding(),
//                (aIntermediate.coreEndings.isEmpty() ? null : aIntermediate.coreEndings.get(0))
//                );
        // get lemmatizing ending
        aIntermediate.prdgm.lemmatizingEnding = morph.util.Cols.getFirstSet(
                aIntermediate.getCoreLemmatizingEnding(),
                aIntermediate.getInheritedLemmatizingEnding()
                );
        // use lemmatizing tag regex to find the lemmatizing ending
        if (aIntermediate.prdgm.lemmatizingEnding == null) {
            aIntermediate.prdgm.lemmatizingEnding = morph.util.Cols.getFirstSet(
                findMatchingLemmatizingEnding(aIntermediate),
                (aIntermediate.coreEndings.isEmpty() ? null : aIntermediate.coreEndings.get(0)) );
        }
        
        //System.err.println("setting prdgm lemmatizing ending: " + aIntermediate.getId() + " " + aIntermediate.prdgm.lemmatizingEnding);
        assertTrue(aIntermediate.prdgm.lemmatizingEnding != null, aIntermediate, "No lematizing ending set");
    }

    private Ending findMatchingLemmatizingEnding(final PrdgmStruct aIntermediate) {
        Pattern lemmatizingTag = Pattern.compile("Vf.*|NN.S1.*|A.YS1.*");   // todo temporary, load from spec
        for (Ending e : aIntermediate.prdgm.getEndings()) {
            if (e.getTag().matches(lemmatizingTag)) return e;
        }
       return null;
    }
    
    private void compileStemChanges(final PrdgmStruct aPrdgmStruct) {
        // set epen stem -- todo inherit itwhat about epen in mini-prdgm
        aPrdgmStruct.prdgm.epenStem = aPrdgmStruct.spec.getEpenStem();
        if (aPrdgmStruct.spec.getEpenStem() != -1) {
            aPrdgmStruct.prdgm.epenVowel = aPrdgmStruct.spec.getEpenVowel();
        }

        // compile stem rules for each ending
        for (Ending e : aPrdgmStruct.prdgm.getEndings()) {
            compileStemRules(e);
        }
    }

    private void prepareCore(final PrdgmStruct aIntermediate) {
        boolean lemEndingMarked = false;    // we have already seen a lem ending (another one is an error)
        boolean hasLemEnding = false;       // this line has lem ending
        String locString = aIntermediate.getLocation();
        
        for (EndingSpec eSpec : aIntermediate.getSpec().getEndingSpec()) {
            if (eSpec.isDefEnding()) {
                assertTrue(!lemEndingMarked, aIntermediate, "Multiple lemmatizing endings '%s'", eSpec.getEndings());
                lemEndingMarked = true;
                hasLemEnding = true;
            }

            for (String tagSpec : eSpec.getTags()) {
                final Tag tag = createTag(aIntermediate.getTagTemplate(), tagSpec, locString);

                final boolean neOk  = tag.subPOSIn(mNegationOk);
                final boolean nejOk = tag.subPOSIn(mSuperlativeOk);

                for (String eStr : eSpec.getEndings()) {
                    int stemId = eSpec.getStemId() != -1 ? eSpec.getStemId() : aIntermediate.getDefStemId();
                    //System.out.println("  e:" + eStr + " " + stemId + " <- " + eSpec.getStemId() + " : " + aIntermediate.getDefStemId());
                    final Ending e = new Ending(aIntermediate.getPrdgm(), stemId, nejOk, neOk, eStr, tag);
                    aIntermediate.coreEndings.add(e);

                    if (hasLemEnding) { // the first ending and the first tag are set as lemmatizing
                        hasLemEnding = false;
                        System.out.printf("Def ending prdgm %s - e %s\n", aIntermediate.getSpec().getId(), e.getEnding());
                        aIntermediate.setCoreLemmatizingEnding(e);
                    }      // todo handle multiple
                    
                }
            }
            // todo def ending
        }
    }


    private void prepareParentPrdgm(final PrdgmStruct aPrdgmStruct) {
        final InheritanceSpec parentSpec = aPrdgmStruct.getSpec().getInheritanceSpec();
        if (parentSpec == null) return;

        final String parentId = parentSpec.getPrdgmId();

        assertTrue(fullSpec.id2prdgmSpec.containsKey(parentId), aPrdgmStruct, "the super paradigm %s is not defined", parentId);  // check by/after reader?
        final PrdgmStruct parentPrdgm = getCompiledPrdgm(parentId);

        //System.out.println(aPrdgmStruct.getId() + " - parent's lemmatizingEnding = " + parentPrdgm.prdgm.lemmatizingEnding);

//        if ("Cr-tretij".equals(aPrdgmStruct.getId())) {
//            System.err.println("le " + parentPrdgm.prdgm.lemmatizingEnding.hashCode() + "   " + parentPrdgm.prdgm.lemmatizingEnding);
//
//            for (Ending e : parentPrdgm.getPrdgm().getEndings()) {
//                System.err.println("le " + e.hashCode() + "   " + e);
//            }
//        }

        // --- fetch, copy and modify parent endings (could be dropped/overriden later) ---
        for (Ending e : parentPrdgm.getPrdgm().getEndings()) {
            final String newStr = parentSpec.getEndingChange().change(e.getEnding());
            final Tag newTag    = modifyTag(parentSpec.getTagModificaton(), e.getTag());

            final Ending newE = new Ending(aPrdgmStruct.prdgm, e.getStemId(), e.isNejOk(), e.isNeOk(), newStr, newTag);

            aPrdgmStruct.inheritedEndings.add(newE);

            if (e == parentPrdgm.prdgm.lemmatizingEnding) {
                aPrdgmStruct.setInheritedLemmatizingEnding(newE);
                //System.err.println("setting inherited lemmatizing ending: " + parentPrdgm.getId() + " " + newE);
            }
        }

       // --- retrieve other inherited things ---
        // inherit tagTemplate if necesary
        if (aPrdgmStruct.getTagTemplate() == null) {
            aPrdgmStruct.setTagTemplate(  parentPrdgm.getTagTemplate() );
        }

        //todo xxx this is wrong/not used

        // inherit stemPattern if necesary
        if (aPrdgmStruct.getStemPattern() == null) {
            aPrdgmStruct.setStemPattern(  parentPrdgm.getStemPattern() );
        }

//        if (aPrdgmStruct.getEpenInfo() == null) {
//            aPrdgmStruct.setStemPattern(  parentPrdgm.getStemPattern() );
//        }

    }

    private void prepareSubPrdgms(final PrdgmStruct aIntermediate) {
        for (SubPrdgmSpec subSpec : aIntermediate.getSpec().getSubPrdgmSpecs()) {
            prepareSubPrdgm(aIntermediate, subSpec);
        }
    }

    /**
     *
     * @param aMainPrdgmStruct structure of the main paradigm (the paradigm referring to the sub-prdgm)
     * @param aSubSpec  specification of the sub-paradigm reference (not the sub-prdgm itself)
     */
    private void prepareSubPrdgm(final PrdgmStruct aMainPrdgmStruct, final SubPrdgmSpec aSubSpec) {
        assertTrue(fullSpec.getId2prdgmSpec().containsKey(aSubSpec.getPrdgmId()), aMainPrdgmStruct,
            "the sub-paradigm %s is not defined", aSubSpec.getPrdgmId());  // check by/after reader?
        final PrdgmStruct subPrdgm = getCompiledPrdgm(aSubSpec.getPrdgmId());        // todo make sure it is not kept in the final result

        for (Ending e : subPrdgm.getPrdgm().getEndings()) {
            final String newStr = aSubSpec.getEndingChange().change(e.getEnding());
            final Tag newTag    = modifyTag(aSubSpec.getTagModificaton(), e.getTag());

//            if ("Vpeci".equals(aMainPrdgmStruct.getId()) && "aorS".equals(subPrdgm.getId())) {
//                System.err.println("PrepareSubPrdgm");
//                System.err.println("ref-spec params:" + aSubSpec.getStemIds());
//                System.err.println("ref-spec params:" + aSubSpec.getStemIds());
//                System.err.println("e: " + e.getStemId() + "  (" + e.getEnding() + ")");
//
//            }
            int stemId = aSubSpec.getStemIds().isEmpty() ? aMainPrdgmStruct.getDefStemId() : aSubSpec.getStemIds().get(e.getStemId());

            final Ending newE = new Ending(aMainPrdgmStruct.getPrdgm(), stemId, e.isNejOk(), e.isNeOk(), newStr, newTag);

            aMainPrdgmStruct.coreEndings.add(newE);
        }
    }

    private void finish(final PrdgmStruct prdgmStruct) {
        final List<Integer> stemIdsList = collectStems(prdgmStruct.prdgm.getEndings());

        assertTrue(stemIdsList.get(0) == 0, prdgmStruct, "There must be a default stem (unmarked or marked as @1)");
        assertTrue(Cols.last(stemIdsList) == stemIdsList.size() - 1, prdgmStruct, "There cannot be a gap in stemIds. %s", Cols.toString(stemIdsList)); // tod Really?
    }

// =============================================================================

    /**
     * Checks a condition, if not satisfied reports the problem to the user.
     * 
     * @param aCond
     * @param prdgmStruct
     * @param aFormatString
     * @param aParams 
     */
    private void assertTrue(final boolean aCond, PrdgmStruct prdgmStruct, String aFormatString, Object ... aParams)  {
        if (!aCond) ulog.assertTrue(false, " prdgm=" + prdgmStruct.getId() + "; " + aFormatString, aParams);
    }


    private static List<Integer> collectStems(final List<Ending> aEndings) {
        final Set<Integer> stemIds = new HashSet<>();

        for (Ending e : aEndings) {
            stemIds.add(e.getStemId());
        }

        final List<Integer> stemIdsList = new ArrayList<>(stemIds);
        Collections.sort(stemIdsList);
        return stemIdsList;
    }

    private Tag createTag(String aTagTmpl, String aTagStr, String aLoc) {
        try {
            // todo apply tag rules (global and local)
            if (aTagTmpl == null) {
                return tagset.fromStringFill(aTagStr);
            }
            else {
                return tagset.fromAbbr(aTagStr, aTagTmpl, mNegationOk);
            }
        }
        catch(Throwable e) {
            ulog.handleError(e, "[%s]\nTag format error (tag string/filler = '%s'). tagTmpl='%s'", aLoc, aTagStr, aTagTmpl);
            return tagset.cDummy;
        }
    }

    // todo Translate lemmatizingEnding if inherited
    /**
     * Applies modifications to a tag.
     *
     * @param aTagModificaton specifications of modification
     * @param aOldTag
     * @return
     * @todo develop some language, compile (change gender for plural but not for singular, etc)
     */
    protected Tag modifyTag(List<String> aTagModificaton, Tag aOldTag) {
        Tag newTag = aOldTag;

        for (String str : aTagModificaton) {
            int slot = tagset.code2slot(str.charAt(0));
            ulog.assertTrue(slot != -1, "-t: %s in %s is not a permissible slot code", str.charAt(0), str);

            for (int i = 1; i < str.length(); i+=2) {
                char from = str.charAt(i);
                char to   = str.charAt(i+1);

                if (from == '.' || aOldTag.getSlot(slot) == from) {
                    newTag = newTag.setSlot(slot, to);
                    break;
                }
            }
        }
        return newTag;
    }

    private void finish() {

        // --- Keep only wanted paradigms (templates and prdgms satisfying mPrdgmNegFilter) ---  // todo
        for (PrdgmStruct p : prdgmStructs.values()) {
            if ( prdgmPredicate.isOk(p.getPrdgm()) ) {
                paradigms.put(p.getId(), p.getPrdgm());
                //System.err.println("Keeping " + p.getId());
            }
            else {
                //System.err.println("Dropping " + p.getId());
            }
        }

//        System.err.println("=== AFTER Cleaning ====");
//        for (Paradigm p : paradigms.values()) {
//            System.err.println(Transliterate.transliterate(p.toString()));
//        }
//        System.err.println("=== ================ ====");


        for (DeclParadigm prdgm : paradigms.values() ) {
            for (Ending e : prdgm.getEndings()) {
                endings.add(e.getEnding(), e);
            }
        }

    }

    /**
     * @todo under development
     */
    private void compileStemRules(final Ending aEnding) {
        // todo epenthesis
        // --- epenthesis ---
        if (aEnding.getPrdgm().epenStem == 0 && aEnding.getStemId() != 0) { // lematizing stem can contain epenthesis
            final Pattern epenPatternCC = Pattern.compile( ".*" + epenConsPatternStr + epenConsPatternStr );

            final StemRule rule = new StemRule("epen1", true) {

                @Override
                public StemAnal apply(StemAnal aStemAnal) {
                    if (! epenPatternCC.matcher(aStemAnal.stem).matches() ) return null;

                    String eStem = Strings.insertB(aStemAnal.stem, 1, aEnding.getPrdgm().epenVowel);       // insert e   // pisn-i -> pisEn,
                    aStemAnal.stem = eStem;

                    return aStemAnal;
                }
            };
            aEnding.getStemRules().add(rule);

        }

        if (aEnding.getPrdgm().epenStem > 0 && aEnding.getStemId() == aEnding.getPrdgm().epenStem) { // current c-stem can contain epenthesis
            final Pattern epenPatternCeC = Pattern.compile( ".*" + epenConsPatternStr + aEnding.getPrdgm().epenVowel + epenConsPatternStr );

            final StemRule rule = new StemRule("epen2", true) {
                @Override
                public StemAnal apply(StemAnal aStemAnal) {
                    if (! epenPatternCeC.matcher(aStemAnal.stem).matches() ) return null;

                    String noEStem = Strings.deleteCharB(aStemAnal.stem, 1);      // todo delete epenVowel using regex

                    aStemAnal.stem = noEStem;
                    if (aStemAnal.info != null) {
                        aStemAnal.info.add(this);
                    }

                    return aStemAnal;
                }
            };
            aEnding.getStemRules().add(rule);

        }

        // --- stem-tail change ---
        //System.out.printf("Rule: %s singleStem=%s\n", aEnding, aEnding.getPrdgm().hasSingleStem());
        if (!aEnding.getPrdgm().hasSingleStem()) {
            final StemRule rule = new StemRule("tailc", true) {
                @Override
                public StemAnal apply(StemAnal aStemAnal) {
          //          System.out.print("Apply: " + aStemAnal.bareForm + " : " + aStemAnal.stem );
                    aStemAnal.stem = aEnding.getPrdgm().lemmatizingStem(aStemAnal.stem, aEnding.getStemId());
            //        System.out.println(" -> " + aStemAnal.stem );
                    return aStemAnal;
                }

                @Override
                public String toString() {
                    return String.format("%s : rule=%s", super.toString(), aEnding.getPrdgm().stemRule);
                }

            };
            aEnding.getStemRules().add(rule);
        }

    }

//    // @todo regex
//    private boolean xxCeC(String aString, String aEpenVowel) {
//        return
//            (aString.length() > 3) &&
//            Strings.charInB(aString, 0, mEpenCons) &&
//            aEpenVowel. Strings.charAtB(aString, 1) ==  &&
//            Strings.charInB(aString, 2, mEpenCons);
//    }
//
//    private boolean xxCC(String aString) {
//        return
//            (aString.length() > 2) &&
//            Strings.charInB(aString, 0, mEpenCons) &&
//            Strings.charInB(aString, 1, mEpenCons);
//    }



}
