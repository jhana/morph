package morph.ma.module.prdgm;

import util.col.pred.AbstractPredicate;
import util.col.pred.Predicate;

/**
 *
 * @author jirka
 */
public class PrdgmFilters {
     /**
     * A commonly used predicate used to drop non-productive paradigms.
     * @see #setPrdgmFilter(Predicate)
     */
    public static class NormalPrdgmPredicate extends AbstractPredicate<DeclParadigm> {
        @Override
        public boolean isOk(DeclParadigm aPrdgm) {
            return aPrdgm.type == DeclParadigm.Type.normal;
        }
    };

    /**
     * A commonly used predicate to keep only endings
     * of productive paradigms
     * @see #setEndingFilter(Predicate)
     */
    public static class NormalEndingPredicate extends AbstractPredicate<Ending> {
        @Override
        public boolean isOk(final Ending aEnding) {
            return aEnding.getPrdgm().type == DeclParadigm.Type.normal;
        }
    };

    /**
     * A commonly used predicate to keep only endings with a particular subpos
     * of productive paradigms
     * @see #setEndingFilter(Predicate)
     */
    public static class SubPosEndingPredicate extends AbstractPredicate<Ending> {

        private final String allowedSubPos;

        public SubPosEndingPredicate(String aAllowedSubPos) {
            allowedSubPos = aAllowedSubPos;
        }

        @Override
        public boolean isOk(final Ending aEnding) {
            return (aEnding.getTag().subPOSIn(allowedSubPos)
                    && aEnding.getPrdgm().type == DeclParadigm.Type.normal);
        }
    };

    /** True on non-template paradigms */
    public static class NonTemplatePredicate extends AbstractPredicate<DeclParadigm> {

        @Override
        public boolean isOk(DeclParadigm aPrdgm) {
            return aPrdgm.type != DeclParadigm.Type.template;
        }
    }

    /**
     * Only endings satisfying this filter are kept
     */
//    private Predicate<Ending> mEndingPredicate = util.col.pred.Filters.trueFilter();

}
