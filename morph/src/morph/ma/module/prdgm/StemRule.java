package morph.ma.module.prdgm;

import lombok.Getter;

/**
 *
 * @author j
 */
@Getter
@lombok.ToString
public class StemRule {
    boolean optional;
    String name;

    public StemRule(String name, boolean optional) {
        this.optional = optional;
        this.name = name;
    }

    public StemAnal apply(StemAnal aStemAnal) {
        return aStemAnal;
    }

}
