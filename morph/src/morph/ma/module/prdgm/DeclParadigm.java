package morph.ma.module.prdgm;

import java.util.*;
import java.util.regex.Pattern;
import lombok.Getter;
import morph.util.string.StringChangeRule;
import morph.util.string.Basic;
import util.str.Strings;
import util.str.Transliterate;
import util.col.Cols;
import util.err.Err;

/**
 * Encapsulates information about a declensional paradigm. 
 * Operations should be limited to accessors or very simple computations, anything
 * beyond that should be elsewhere (analyzer, generator, compiler, etc)
 * 
 * @author  Jirka Hana
 */
@Getter 
public final class DeclParadigm extends Paradigm {
    public enum Type {normal, nonproductive, template};

    /**
     * Type of this paradigm (normal/nonproductive/template)
     * Nonproductive are not used by the guesser, templates are used only by the compiler
     */
    public Type type;

    /**
1     * Positive/negative tail class.
     * If true the stem is okay if it's tail matches the mClass, if fals it is okay if it does not
     */
    public boolean permissibleClass = true;

    /**
     * Id of the set of strings the tail will be checked against
     * Note: for debugging only.
     */
    public String stemTailsId;

    /**
     * Set of strings the tail will be checked against.
     * @todo replace by regex?
     */
    public Set<String> stemTails;

//    /** The tag with lemmatizing ending */
//    public Tag defTag;

    /** Cache: Lemmatizing ending of the paradigm (stem + lemmatizingEnding = lemma) */
    public Ending lemmatizingEnding;

    /** Endings assigned to this paradigm */
    private @Getter List<Ending> endings;

    public String sampleLemma;
    public List<String> sampleLemmas;  //@todo

    /**
     * Rule capturing systematic relation between tails of multiple stems.
     * Null if only one stem is possible.
     * @todo check what if multiple stems but no rule???
     */
    public StringChangeRule stemRule;

    /**
     * Idx of the stem with epenthesis. If no epenthesis possible, -1.
     */
    public int epenStem = -1;

    /**
     * Epenthesising vowel.
     */
    public String epenVowel;

    /**
     * Tag template.
     * Used by compiler to enable inheritance, not used during runtime.
     */
    public String mTagTmpl;

// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------

    /**
     * Creates a new empty paradigm.
     *
     * @param aId the paradigms id (must be unique)
     */
    public DeclParadigm(String aId) {
        super(aId);
//        type = aType;
//        classId = aClassId;
//        lemmatizingEnding = aDefEnding;
        endings = new ArrayList<>();
        sampleLemma = "";
    }

    /**
     * Checks whether the stem's tail is appropriate for the paradigm (as specified by the paradigm's class)
     *
     * @param aStem lemmatizing stem
     */
    public boolean stemPhonologyOk(String aStem) {
        if ( stemTails == null )  return true;

        // -- go thru the class --
        // @todo optimize (similarly as stem rule)
        for (String c : stemTails) {
            if (aStem.endsWith(c)) return permissibleClass;
        }

        return !permissibleClass;
    }


    // todo under construction = needs to be read from prdgm spec.
    private Pattern lemmaOkPattern = null;
    private Pattern lemmaKoPattern = Pattern.compile(".*("
            + "(?!(a\\+u|o\\+u|e\\+u|i\\+e|i\\+u))([aeiouyáéíóúůýě]\\+[aeiouáéíóúůýě])|"
            + "[ščřž]\\+[yý]|"
            + "[hkr]\\+[ií]|"
            + "[ďťň]\\+[iíyýě]|"
            + "ch\\+[ií]"
            + ").*");

    public boolean lemmaPhonologyOk(String aLemma) {
        if ( lemmaOkPattern != null ) {
            if (! lemmaOkPattern.matcher(aLemma).matches()) return false;
        }

        if ( lemmaKoPattern != null ) {
            if (lemmaKoPattern.matcher(aLemma).matches()) return false;
        }

        return true;
    }

// -----------------------------------------------------------------------------
// Stems
// -----------------------------------------------------------------------------

    /**
     * Checks whether this paradigm allows only single stem.
     */
    public boolean hasSingleStem() {
        return nrOfStems() == 1;
    }

    /**
     * Checks whether this paradigm allows multiple stems.
     * Note: Checks presence of mStemRule is sufficient, stem rule cannot be trivial
     * TODO allow different stems without explicitely specifying the rule (it might be too irregular)
     */
    private int nrOfStems = -1;

    // for now, mStem must contain an identity rule for root with epen
    // e.g. s/s/� or �/� if no other change is going on (we assume no word ends with � :) so it will have no effect).
    public int nrOfStems() {
        if (nrOfStems == -1) {
            final Set<Integer> stemIds = new HashSet<>();

            for (Ending e : endings) {
                stemIds.add(e.getStemId());
            }
            nrOfStems = stemIds.size();

            final List<Integer> stemIdsList = new ArrayList<>(stemIds);
            Collections.sort(stemIdsList);
            Err.fAssert(stemIdsList.get(0) == 0, "StemIds must start from 0.");
            Err.fAssert(Cols.last(stemIdsList) == stemIdsList.size() - 1, "StemIds must consequitive?."); // tod Really?
        }

        if (stemRule == null) {
            stemRule = new Basic(nrOfStems);
        }

        if (stemRule.getSubruleSize() != nrOfStems) {
            Err.fAssert(false, "Prdg %s, Nr of stems (%d) does not match nr of rules (%d, %s)", id, nrOfStems, stemRule.getSubruleSize(), stemRule  );
        }
        //return (mStemRule == null) ? 1 : mStemRule.getSubruleSize();
        return nrOfStems;
    }




    /**
     * Converts a stem to the lemmatizing stem (i.e. stem nr. 0).
     *
     * @param aStem    a stem to convert
     * @param aStemId  id of aStem
     * @return lemmatizing stem
     */
    public String lemmatizingStem(String aStem, int aStemId) {
        Err.assertE(stemRule == null || aStemId < stemRule.getSubruleSize(), id + " " + aStem);
        return hasSingleStem() ? aStem : stemRule.translateTail(aStem, aStemId, lemmatizingEnding.getStemId());
    }

    /**
     * Returns all stems that such a lemma would be associated with if
     * it bellonged to this paradigm.
     * Even identical stems are returned. @todo ???
     *
     * @todo handle changes occuring with epenthesis
     * @param aLemma
     * @return
     */
    public String[] allStems(String aLemma) {
        final int nrOfStems = nrOfStems();
        String[] stems = new String[nrOfStems];

        String stem0 = Strings.removeEnding(aLemma, lemmatizingEnding.getEnding());

        //stems[0] = ((0 != mEpen) && aEpen) ? Strings.insertB(stem0, 1, 'e') : stem0;
        stems[0] = stem0;  // lemma already contains proper epenthesis ????
        for(int i = 1; i < nrOfStems; i++) {
            stems[i] = stem0;
            if (i == epenStem) {
                stems[i] = Strings.insertB(stem0, 1, epenVowel);                  // todo allow the position to be configurable (probably by regex match)
            }
            else if (epenStem == 0) {
                stems[i] = Strings.deleteCharB(stem0, 1);   // todo only if the epen vowel is there
            }
            
            stems[i] = stemRule.translateTail(stems[i], 0, i);
        }

        return stems;
    }



// -----------------------------------------------------------------------------
// Presentation
// -----------------------------------------------------------------------------
    // @optimize (comment in
    @Override
    public String toString() {
        return String.format("Prdgm id %s (%s) - sampleLemma: %s, lemEnding: %s, class: %s\n%s",
            id, type, Transliterate.transliterate(sampleLemma), Transliterate.transliterate(lemmatizingEnding.getEnding()), stemTailsId,
            Transliterate.transliterate(Cols.toStringNl(endings, "  ")));
    }


    /**
     * Declines the sample lemma.
     */
    public String toWordedString() {
        final StringBuilder tmp = new StringBuilder();
        tmp.append(String.format("Prdgm id %s - sampleLemma: %s, defEnding: %s, class: %s\n; stemRule: %s\n",
            id, Transliterate.transliterate(sampleLemma), Transliterate.transliterate(lemmatizingEnding.getEnding()),
            stemTailsId, stemRule));

        // @todo
        sampleLemmas = new ArrayList<>();
        sampleLemmas.add(sampleLemma);

        // HO: sampleStems = mSampleLemmas.map(\\x.Strings.removeEnding(x,lemmatizingEnding));
        final List<String> sampleStems = new ArrayList<>(sampleLemmas.size());
        for (String sampleLemma : sampleLemmas) {
            sampleStems.add(Strings.removeEnding(sampleLemma,lemmatizingEnding.getEnding()));
        }

        for (Ending e : new TreeSet<>(endings)) {
            assert ((e.getStemId() == 0) || (stemRule != null)) : id + " " + e;
            tmp.append("   ").append(e.getTag()).append(" ").append(e.getStemId());
            for (String sampleStem : sampleStems)  {
                String format = " %-" + (sampleStem.length() + 4) + "s";
                String form = (e.getStemId() == 0) ? sampleStem : stemRule.translateTail(sampleStem, 0, e.getStemId());
                form = Transliterate.transliterate(form  + e.getEnding());      // todo
                tmp.append(String.format(format, form));
            }
            tmp.append('\n');
        }
        return tmp.toString();
    }


// -----------------------------------------------------------------------------
// Statistics
// -----------------------------------------------------------------------------

    /**
     * Returns the number of possible distinct endings (i.e. ending-strings).
     * For a paradigm with endings {0, -s, -ed, -ed}, this would enter 3.
     */
    @Override
    protected int getPossEndingsNrImpl() {
        final Set<String> tmp = new HashSet<>();
        for (Ending e : this.endings) {
            tmp.add(e.getEnding());
        }
        return tmp.size();
    }
}
