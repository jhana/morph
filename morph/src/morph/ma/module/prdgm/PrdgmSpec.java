package morph.ma.module.prdgm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import morph.util.string.StringChangeRule;

/**
 * Representation of a single parsed paradigm.
 *
 * todo add line for error reporting
 * @author jirka
 */
@lombok.ToString
@lombok.RequiredArgsConstructor
public class PrdgmSpec {

    /** Superclass of references to other paradigms (miniparadigms, parent paradigms) */
    @Getter
    @lombok.RequiredArgsConstructor
    private class OtherPrdgmSpec {
        private final String prdgmId;
        private final StrChange stemChange;
        private final StrChange endingChange;  //??
        private final List<String> tagModificaton;
        private final String location;  // todo under dev, for reporting errors
    }

    /** Reference to the parent paradigm of this paradigm */
    @Getter
    public final class InheritanceSpec extends OtherPrdgmSpec {
        public InheritanceSpec(String prdgmId, StrChange stemChange, StrChange endingChange, List<String> tagModificaton, String aLoc) {
            super(prdgmId, stemChange, endingChange, tagModificaton, aLoc);
        }
    }

    /** Reference a mini-paradigm (aka sub-paradigms) */
    @Getter
    public final class SubPrdgmSpec extends OtherPrdgmSpec {

        public SubPrdgmSpec(String prdgmId, StrChange stemChange, StrChange endingChange, List<String> tagModificaton, List<Integer> stemIds, String aLoc) {
            super(prdgmId, stemChange, endingChange, tagModificaton, aLoc);
            this.stemIds = stemIds;
        }

        private final List<Integer> stemIds;

    }

    @Getter
    @lombok.RequiredArgsConstructor
    public final class EndingSpec {
        private final List<String> endings;
        private final List<String> tags;
        private final boolean defEnding;
        private final int stemId;
    }

    @Getter @Setter private String line;
    @Getter private final String location;  // todo under dev, for reporting errors

    // spec
    @Getter private final String id;
    @Getter @Setter private DeclParadigm.Type type;


    /** -a */
    @Getter @Setter private String tagTemplate;

//    /**
//     * Ending to be used with first stem to form lemma
//     * -le
//     * @todo
//     */
//    @Getter @Setter private Ending lemmatizingEnding;

//    @Getter @Setter private String lemmatizingTag;

    /** -l */
    @Getter @Setter private String sampleLemma;

    /**
     * Legal stem tails.
     */
    @Getter @Setter private String stemTailSet;

    @Getter @Setter private int epenStem = -1;
    @Getter @Setter private String epenVowel = "e"; // todo

    @Getter @Setter private StringChangeRule sStemRule;

    @Getter private InheritanceSpec inheritanceSpec;
    @Getter final private List<EndingSpec> endingSpec = new ArrayList<EndingSpec>();
    @Getter final private List<SubPrdgmSpec> subPrdgmSpecs = new ArrayList<SubPrdgmSpec>();


    public void addInheritance(String aPrdgmId, StrChange aEndingChange, List<String> aTagChange, int aLineNumber) {
        StrChange stemChange = null;

        inheritanceSpec = new InheritanceSpec(aPrdgmId, null, aEndingChange, aTagChange, String.valueOf(aLineNumber));
    }

    public void addSubPrdgm(String subPrdgmId, StrChange aEndingChange, List<Integer> stemIds, List<String> aTagChange, int aLineNumber) {
        StrChange stemChange = null;

        final SubPrdgmSpec spec = new SubPrdgmSpec(subPrdgmId, stemChange, aEndingChange, aTagChange, stemIds, String.valueOf(aLineNumber));
        subPrdgmSpecs.add(spec);
    }

    //String tagTmpl = "";
    /* Index of the unspecified stem. 0 by default */
    //int unspecified = 0;

    public void addEndingLine(List<String> endings, List<String> tags, boolean defEnding, int stemId) {
        final EndingSpec spec = new EndingSpec(endings, tags, defEnding, stemId);
        endingSpec.add(spec);
    }
}
