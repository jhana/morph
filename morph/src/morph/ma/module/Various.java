package morph.ma.module;

import morph.common.UserLog;
import java.util.*;
import morph.ma.lts.*;
import morph.ma.*;
import util.Numbers;
import morph.ts.Tag;


/**
 *
 * @author  Jiri
 */
public class Various extends Module {
    private boolean cParseRomanNumerals; 
    private Tag cRomanNumeralTag; 
    private Tag cNumberTag;      
    private java.text.NumberFormat cNumberFormat;
    
    @Override
    public int compile(UserLog aUlog)  {
        String country = moduleArgs.getString("nrFormatLocaleCountry", "CZE");   
        String lg      = moduleArgs.getString("nrFormatLocaleLg", "CZ");
        cNumberFormat = java.text.NumberFormat.getInstance(new Locale(country, lg));
        
        cRomanNumeralTag    = args.getTag(mpn("romanNumeralTag"), tagset);
        cNumberTag          = args.getTag(mpn("numberTag"), tagset);
        cParseRomanNumerals = args.getBool(mpn("parseRomanNumerals"), true);
        return 0;
    }

    @Override
    public Collection<Tag> getAllTags() {
        return Arrays.asList(cNumberTag, cRomanNumeralTag);
    }
    
    @Override
    public boolean analyzeImp(String aCurForm, String[] aForms, List<BareForm> aBareForms, Context aCtx, SingleLts aLTS) {
        // --- numbers --- 
        if (parseNumber(aCurForm) != null) {
            aLTS.add(aCurForm, cNumberTag, null); 
            return true;
        }
        
        // --- roman numerals ---
        if (cParseRomanNumerals && Numbers.roman(aCurForm) != -1) {    
            aLTS.add(aCurForm, cRomanNumeralTag, null);  // @todo normalize capitalization
        }

        return false;
    }

    /**
     * Attempts to parse a string into a number using the number format
     * according to nrFormatLocaleCountry and nrFormatLocaleLg property.
     *
     * Whatever the number format is, the string is not considered a number
     * if the first character is a letter (used for efficiency).
     *
     * @param aForm a string that might be a number
     * @return 
     * <ul>
     * <li>a Long if aForm can be interpreted as a long (e.g. 2, 2.0), 
     * <li>a Double (e.g. 2.01) or 
     * <li>null if it cannot be parsed as a number (incl. when it is empty)
     * </ul>
     * @see Character#isLetter(char)
     * @todo this considers '1aaaa' to be a number
     * @todo add similar options as tnt
     */
    private final Number parseNumber(String aForm) {
        // --- a quick check ---
        if (aForm.length() == 0 || Character.isLetter(aForm.charAt(0)) ) return null;
        
        try {
            return cNumberFormat.parse(aForm);
        }
        catch(java.text.ParseException e) {}
        return null;
    }
    
}
