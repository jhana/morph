package morph.ma.module;

import morph.common.UserLog;
import java.util.*;
import util.*;
import morph.ma.lts.*;
import morph.ma.*;
import morph.ts.Tag;

/**
 *
 * @author  Jiri
 */
public final class Proper extends Module {
    Tag cProperTag;     // @todo list
    boolean cStopIfSIntra;

    @Override
    public int compile(UserLog aUlog) throws java.io.IOException {
        cProperTag = args.getTag(mpn("tag"), tagset);
        cStopIfSIntra = moduleArgs.getBool("stopIfSIntra", false);
        return 0;
    }

    @Override
    public boolean analyzeImp(String aCurForm, String[] aForms, List<BareForm> aBareForms, Context aCtx, SingleLts aLTS) {
        if (aCtx.curCap().firstCap() || aCtx.curCap().caps() ||
                (aCtx.curCap().mixed() && aCurForm.length() > 0 &&  Character.isUpperCase( aCurForm.charAt(0) )) ) {
            aLTS.add(aCurForm, cProperTag, null);

            // possibly no more analyses after this module (if cStopIfSIntra is true and there is no punct. before the word)
            boolean stop = cStopIfSIntra && !aCtx.prevIsSentencePunct();
            //System.out.println(stop + "  " + aCtx.toString());

            return stop;
        }

        return false;
    }

}
