
package morph.ma.module;

import morph.ma.Context;
import morph.ma.lts.Lts;

/**
 *
 * @author Jirka Hana
 */
public interface LtsFilter {
    void filter(Lts aLTS, Context aCtx);

}
