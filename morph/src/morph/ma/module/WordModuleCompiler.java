package morph.ma.module;

import morph.io.*;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import util.io.*;
import util.col.MultiMap;
import morph.ts.Tag;
import util.err.FormatError;
import util.err.UException;
import util.str.Strings;

/**
 *
 * @author  Jiri
 */
class WordModuleCompiler extends ModuleCompiler<WordModule> {


// =============================================================================
// Implementation <editor-fold desc="Implementation">
// =============================================================================

    private String mNegPref;
    private String mNegationOk;
    private boolean mAutoNegation;
    private int wordLimit;
    private boolean skipEmptyWords;        // empty words are ignored (the annotator skipped them)

    private boolean tt;

    @Override
    protected void compileI() throws IOException {
        mModule.form2entry = new MultiMap<String, WordEntry>();

        mNegationOk = mModuleArgs.getString("negationOk", "");
        if (mNegationOk.equals("")) {       // get default key's value
            mNegationOk = args.getString("negationOk", "");
        }
        mAutoNegation = mNegationOk.length() > 0;
        mNegPref = args.getString("negationPref", "xxxx");

        wordLimit = mModuleArgs.getInt("wordLimit", Integer.MAX_VALUE);

        skipEmptyWords = mModuleArgs.getBool("skipEmptyWords", true);

        // todo remove tag translation and make a dedicated tool
        XFile ttMapFile = mModuleArgs.getFile("tt");
        tt = (ttMapFile != null);

        readInWords(getMFiles("file"));
    }

    private void readInWords(List<XFile> aFileNames) throws java.io.IOException {
        final ReaderParser r = new ReaderParser(aFileNames, inFilter, "words");

        for(int wordCounter=0; wordCounter < wordLimit;wordCounter++) {
            if (r.nextLine() == null) break;
            try {
                readEntry(r);
            }
            catch (java.util.NoSuchElementException e) {
                handleError(e,r);
            }
            catch (UException e) {
                handleError(e,r);
            }
        }

        r.close();
    }

    private void readEntry(ReaderParser aR) throws IOException {
        final String token  = aR.getStringF("initial entry's token");

        if (token.startsWith("@")) {
            try {
                if (token.equals("@")) {
                    readInGroup(aR);
                }
                else if (token.equals("@p")) {
                    readInSmallPrdgm(aR);
                }
                else if (token.equals("@pp")) {
                    readInSmallPrdgmX(aR);
                }
                else {
                    assertTrue(false, aR, "Nonexistent @ code - %s", token);
                }
            }
            catch (UException e) {
                handleError(e, aR);
                aR.skipUntil("}");
            }
            catch (FormatError e2) {
                handleError(e2, aR);
                aR.skipUntil("}");
            }
            catch (NoSuchElementException e) {
                handleError(e, aR);
                aR.skipUntil("}");
            }
        }
        else {
            readInWordEntry(aR, token);
        }
    }

    /**
     * Simple word entry: form (@ lemma) tag(s).
     *
     *
     * @param aR
     * @param aForm form string (cannot use / separator)
     * @throws IOException
     */
    private void readInWordEntry(ReaderParser aR, String aForm) throws IOException{
        if (!aR.hasMoreTokens() && skipEmptyWords) return;                      // ignoring words without any information

        final String lemma = (aR.eatIf("@")) ? aR.getStringF("lemma") : aForm;

        // --- read tags and create entries ---
        final List<Tag> tags = new ArrayList<Tag>();
        do {
            Tag tag = aR.getTagFill("tag");
            addEntryNeg(aForm, lemma, tag);
        } while(aR.hasMoreTokens());        // @todo negation/degrees???
    }


    /** Single tag multiple forms (lemma is the same as each form) */
    private void readInGroup(ReaderParser aR) throws IOException {
        final Tag tag = aR.getTagFill("tagx");

        // --- words for that tag ---
        for(;;) {
            if (aR.nextLine() == null) return;

            do {
                String form  = aR.getStringF("form");
                if (form.equals("}")) return;
                addEntryNeg(form, form, tag);
            } while (aR.hasMoreTokens());
        }
    }



    private void readInSmallPrdgm(ReaderParser aR) throws IOException {
        final String lemma   = aR.getStringF("lemma.");
        final String tagTmpl = aR.getString("fullTag.");

        // --- read form & tags and create entries ---
        for(;;) {
            if (aR.nextLine() == null) break;
            final String formStr  = aR.getStringF("form.");
            if (formStr.equals("}")) return;

            addEntryForTags(aR, formStr, lemma, tagTmpl);
        }
    }


    private void readInSmallPrdgmX(ReaderParser aR) throws IOException {
        String lemma   = aR.getStringF("lemma.");

        // --- read form & tags and create entries ---
        for(;;) {
            if (aR.nextLine() == null) break;
            String formStr  = aR.getStringF("form.");
            if (formStr.equals("}")) return;

            if (formStr.equals("@p")) {
                readInSubPrdgmX(aR, lemma);
            }
            else {
                addEntryForTags(aR, formStr, lemma, null);
            }

            // todo read in named subprdgm
        }
    }

    private void readInSubPrdgmX(ReaderParser aR, String aLemma) throws IOException {
        String tagTmpl = aR.getString("tagTemplate.");

        // --- read form & tags and create entries ---
        for(;;) {
            if (aR.nextLine() == null) break;
            String formStr  = aR.getStringF("form.");
            if (formStr.equals("}")) return;

            addEntryForTags(aR, formStr, aLemma, tagTmpl);
        }
    }

    private final static Pattern cFormSeparator = Pattern.compile("/");
    private void addEntryForTags(final ReaderParser aR, final String aFormStr, final String aLemma, final String aTagTmpl) {
        final List<String> forms = Strings.splitL(aFormStr, cFormSeparator);
        do {
            final Tag tag = aTagTmpl == null ? aR.getTagFill("tag") : aR.getTag("tag", aTagTmpl);
            for (String form : forms) {
                addEntryNeg(form, aLemma, tag);
            }
        } while (aR.hasMoreTokens());
    }

    /**
     * Add a word to the dictionaryy, adding affirmative and negated tag if requested and appropriate.
     * todo generalze, currently works only with the Czech PDT tagset!
     *
     * @param tag
     * @param form
     * @param lemma
     */
    private void addEntryNeg(String form, String lemma, Tag tag) {
        if (mAutoNegation && tag.getNegation() == '-' && tag.subPOSIn(mNegationOk)) {
            addEntry(form, lemma, tag.setNegation('A'));
            addEntry(mNegPref + form, lemma, tag.setNegation('N'));
        }
        else {
            addEntry(form, lemma, tag);
        }
    }

    private void addEntry(String aForm, String aLemma, Tag aTag) {
        /* This is a temporary hack to handle OldCzech via Modern Czech */
        if (tt) {
//            if (aTag.getGender() == 'Z') {
//                addEntry(aForm, aLemma, aTag.setGender('Y'));
//                addEntry(aForm, aLemma, aTag.setGender('N'));
//            }
//            else {
                mModule.form2entry.add(aForm, new WordEntry(aForm, aLemma, aTag));
//            }
        }
        else {
            mModule.form2entry.add(aForm, new WordEntry(aForm, aLemma, aTag));
        }
    }
}
