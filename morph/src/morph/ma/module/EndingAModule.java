package morph.ma.module;

import java.util.*;
import morph.ma.module.prdgm.Ending;
import morph.ts.Tag;

/**
 *
 * @author  Jirka
 */
public abstract class EndingAModule extends Module {

    /**
     * Access to the paradigms this module uses (e.g. for lexicon acquisition)
     * @return paradigms sub-module
     */
    public Paradigms getParadigms() {return mParadigms;}

    @Override
    public Collection<Tag> getAllTags() {
        Set<Tag> tags = new TreeSet<Tag>();
        for (Ending ending : mParadigms.getEndings().allValues()) {
            tags.add(ending.getTag());
        }
        return tags;
    }

    /**
     * Lex and guesser parameter
     * todo:
     * language: module:lexicon:maxEndingLen ????
     * language: module:guesser:maxEndingLen
     */
    protected int mMaxEndingLen;
    protected int mMinStemLen = 1;

    protected Paradigms mParadigms;

    /*
     * String of subPOS that allow superlative prefix.
     */
    protected String mSuperlativeOk;

}
