package morph.ma.module;

import morph.ma.module.prdgm.DeclParadigm;
import morph.ma.module.prdgm.Ending;
import java.io.IOException;
import java.util.Set;
import java.util.regex.Pattern;
import lombok.Cleanup;
import morph.io.ReaderParser;
import org.purl.net.jh.utilx.Logger;
import util.col.MultiMap;
import util.col.pred.AbstractPredicate;
import util.col.pred.Predicate;
import util.err.UException;
import util.io.XFile;



/*
 *
 * @author  Jiri
 */
public class FGuesserCompiler extends ModuleCompiler<FGuesser> {
    private final static Logger log = Logger.getLogger(FGuesserCompiler.class.getName());

    @Override protected void compileI() throws java.io.IOException {
        mModule.mSuperlativeOk     = args.getString("superlativeOk");
        mModule.mGuessedSubPOS     = mModuleArgs.getString("guessingOk", "");
        mModule.mMinStemLen        = mModuleArgs.getInt("minStemLen", 1);
        mModule.mHandleDerivations = mModuleArgs.getBool("handleDerivations", false);
        //System.out.println("FGuesserCompiler: mHandleDerivations=" + mModule.mHandleDerivations );

        if (!initParadigms()) return;
        // todo is this needed? OR is it already checked/reported in initParadigms?
        ulog.assertTrue(mModule.mParadigms != null, "No Paradigms object loaded");
        ulog.assertTrue(mModule.mParadigms.getClasses() != null, "No Paradimgs.Classes object loaded");

        // --- load in filters (todo the filters should handle it by themselves) ---
        if (mModuleArgs.getBool("filter.ft.enabled", false)) {
            log.info("loading ft filter");
            mModule.mFormTagFilter = new MultiMap<String,Character>();  // todo should be tree map
            final XFile file = mModuleArgs.getFile("filter.ft.file");
            ulog.assertTrue(file != null, "filter.ft.enabled=true, but no file specified (filter.ft.file)");  // todo what if file not specified, convention over conf
            readInTagFilter(file);
            log.info("loaded ft filter");
        }

        if (mModuleArgs.getBool("filter.lemma.enabled", false)) {
            final XFile file = getMFile("filter.lemma.file");
            log.info("loading lemma filter %s", file);
            ulog.assertTrue(file != null, "filter.lemma.enabled=true, but no file specified (filter.lemma.file)");

            // todo create morph.util method readMultimap(file, mModuleArgs.getSubtree("filter.lemma"));
            final Pattern keyPattern   = mModuleArgs.getPattern("filter.lemma.keyPattern",   "\\s*(\\S+)\\s+.*");
            final Pattern valsPattern  = mModuleArgs.getPattern("filter.lemma.valsPattern",  "\\s*\\S+\\s+(.*)");
            final Pattern valSeparator = mModuleArgs.getPattern("filter.lemma.valSeparator", "\\s+");
            final MultiMap<String,String> lemma2forms = org.purl.net.jh.utilx.IO.readInMultiMap(file, keyPattern, valsPattern, valSeparator);
            //System.out.println("Multimap: " + Cols.toStringNl(lemma2forms.entrySet()) );
            final MultiMap<String, String> form2Lemmas = org.purl.net.jh.utilx.Cols.transpose(lemma2forms);

            if (mModuleArgs.getBool("filter.lemma.conservative", true)) {
                mModule.ltsFilters.add( new FGuesser.ConservativeLemmaFilter(form2Lemmas) );
                log.info("loaded conservative lemma filter");
            }
            else {
                mModule.ltFilters.add( new FGuesser.LemmaFilter(form2Lemmas) );
                log.info("loaded radical lemma filter");
            }
        }


        mModule.mEpenCons = "";
        Set<String> conss = mModule.mParadigms.getClasses().get("C");
        if (conss != null)  {  // @todo warning
            for (String c : conss)   mModule.mEpenCons += c;
        }

        if (mModule.mHandleDerivations) initDerivations();
    }





    private void initDerivations() throws IOException {
        try {
            mModule.mDerivations = new Derivations();
            mModule.mDerivations.init(mModuleId, args);
            mModule.mDerivations.compile(ulog, mModule.mParadigms);
            mModule.mDerivations.configure();
            mModule.mMaxEndingLen  = mModule.mParadigms.getMaxEndingLen();
        }
        catch(UException e) {
            ulog.handleError(e, "Cannot load paradigms submodule");
        }

    }

    private boolean initParadigms() throws java.io.IOException {
        // Keep only endings of guessed subPOSs and of normal (productive) paradigms
        final Predicate<Ending> endingFilter = new AbstractPredicate<Ending>() {
            public boolean isOk(Ending aEnding) {
                return (  aEnding.getTag().subPOSIn(mModule.mGuessedSubPOS) &&
                          aEnding.getPrdgm().type == DeclParadigm.Type.normal );
            }
        };

        // Keep only normal paradigms (drop non-productive ones)
        final Predicate<DeclParadigm> prdgmFilter = new AbstractPredicate<DeclParadigm>() {
            public boolean isOk(DeclParadigm aPrdgm) {
                return aPrdgm.type == DeclParadigm.Type.normal;
            }
        };

        // paradigms are not shared (different configuration for diff modules)
        try {
            mModule.mParadigms = new Paradigms();
            mModule.mParadigms.init(mModuleId, args);
            if (mModule.mParadigms.compile(ulog, prdgmFilter, endingFilter) > 0) return false;
            mModule.mParadigms.configure();
            mModule.mMaxEndingLen  = mModule.mParadigms.getMaxEndingLen();
        }
        catch(Throwable e) {
            ulog.handleError(e, "Cannot load paradigms submodule");
            return false;
        }
        return true;
    }

    protected void readInTagFilter(XFile aFile) throws java.io.IOException {
        @Cleanup ReaderParser r = new ReaderParser(aFile, inFilter, "tagFilter");

        for(;;) {
            if (r.nextLine() == null /*|| r.getLineNumber() > 50*/) break;

            try {
                readInWordEntry(r);

//                String form = r.getStringF("form");
//                if (r.hasMoreTokens()) {
//                    List<String> tags = Strings.splitL(r.getString("tagFilters"));
//                    // TODO this is temporary to allow direct use of full word lists
//                    for (String tag : tags) {
//                        mModule.mFormFilter.add(form, tag.charAt(0));
//                    }
//                }
            }
            catch (java.util.NoSuchElementException e) {
                ulog.handleError(e);
            }
        }
    }

    // @todo do not ignore lemma
    private void readInWordEntry(ReaderParser aR) throws IOException{
        // --- read form and possibly lemma ---
        String form  = aR.getStringF("form");
        String lemma = (aR.eatIf("@")) ? aR.getStringF("lemma") : form;
        /// if (!form.startsWith("Ale")) return;   // todo xxxx

        // --- read tags (if any) put into the filter map ---
        while(aR.hasMoreTokens()) {
            String tag = aR.getString("tag");
            mModule.mFormTagFilter.add(form, tag.charAt(0));  // for now only pos
        }

    }


}
