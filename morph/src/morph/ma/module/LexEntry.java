package morph.ma.module;

import java.util.ArrayList;
import java.util.List;
import morph.ma.module.prdgm.DeclParadigm;
import morph.ma.module.prdgm.Ending;
import util.col.Cols;

/**
 *
 * @author  Jirka Hana
 */
public final class LexEntry implements java.io.Serializable{
    public String stem;
    final private  List<String> stems = new ArrayList<String>();
    public DeclParadigm paradigm;
    public String lemma;

    // @todo neOk, nejOk

    public LexEntry() {}

    public void addStem(final int aIdx, final String aStem) {
        //if (stems == null) stems = new ArrayList<String>();

        Cols.listAdd(stems, aIdx-1, aStem);
    }

    // todo compile
    public String getStem(int aIdx) {
        if (aIdx >= paradigm.nrOfStems()) throw new IllegalArgumentException("Stem idx=" + aIdx + " does not exist");

        if (aIdx == 0) return stem;

        if (stems == null) return stem;
        if (stems.size() < aIdx || stems.get(aIdx-1) == null) return stem;

        return stems.get(aIdx-1);
    }


    public boolean fits(final String aStemCand, final Ending aEnding) {
        //System.out.printf("   Fits1: %s; %d; %s\n", aStemCand, aEnding.rootId, aEnding);
        if (paradigm != aEnding.getPrdgm()) return false;
        //System.out.println("   Fits2: ++");

        return getStem(aEnding.getStemId()).equals(aStemCand);
    }

    public String getLemmatizingStem() {
        return stem;
    }

    @Override
    public String toString() {
        final StringBuilder tmp = new StringBuilder();
        tmp.append(lemma).append(", ").append(stem);

        // todo skip nulls, i.e. stems ==1
        tmp.append( Cols.toString(stems, "/", "", "/", "") );

        tmp.append(", ").append(paradigm.id);

        return tmp.toString();
    }

}


/* @todo
class LexEntries {
    LexEntry get(String aStem);
}

*/

