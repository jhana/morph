package morph.ma.module;

import morph.common.UserLog;
import java.util.List;
import java.util.Map;
import java.util.Set;
import morph.ma.BareForm;
import morph.ma.Context;
import morph.ma.lts.SingleLts;
import morph.ma.module.prdgm.Derivation;
import util.col.MultiMap;

/**
 *
 * @author Jirka
 */
public class Derivations  extends Module {

// -----------------------------------------------------------------------------
// Setup
// -----------------------------------------------------------------------------

    /**
     * Loads its own paradigms
     */
    @Override
    public int compile(UserLog aUlog) throws java.io.IOException {
        throw new UnsupportedOperationException();

//        // load and compile paradigms
//        return compile(prdgms);
    }

    public int compile(UserLog aUlog, Paradigms aParadigms) throws java.io.IOException {
        DerivationsCompiler compiler = new DerivationsCompiler();
        compiler.init(this, moduleId, aUlog, args);
        compiler.setPrdgms(aParadigms);
        return compiler.compile();
    }



    public int configure(Map<String, Module> aModules)  {
        System.out.println("Configuring derivations");
        //createSuperParadigms();

        return 0;
    }

// -----------------------------------------------------------------------------
// Analysis
// -----------------------------------------------------------------------------

    @Override
    public boolean analyzeImp(String aCurForm, String[] aForms, List<BareForm> bareForms, Context aCtx, SingleLts aLTS) {
        throw new UnsupportedOperationException();
    }

    public Set<Derivation> getDerivations(String aDeclPrdgmId) {
        return mPrdgm2Derivation.get(aDeclPrdgmId);
    }

    //Map<String, Derivation> mDerivations; decl prdgm -> derivations
    MultiMap<String, Derivation> mPrdgm2Derivation;

}
