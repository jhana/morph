/*
 @todo file specifying excluded paradigms
 @todo utilities: checkTags  file/endings/words/tagTransl
 @todo better info about tokens (surrounding punctuation)
*/

package morph.ma.module;


import morph.common.UserLog;
import java.io.*;
import java.util.*;
import morph.ma.module.prdgm.DeclParadigm;
import util.io.*;
import morph.util.MUtilityBase;




/**
 * Compiles Paradigms and prints out the result
 */
public class PrdgmCheck extends MUtilityBase {

    /**
     *
     * See helpString()
     */
    public static void main(String[] args) throws IOException {
        new PrdgmCheck().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,1);

        XFile outFileName  = getArgs().getDirectFile(0);

        Paradigms prdgms = new Paradigms();
        prdgms.init("Guesser.paradigms", getArgs());        // @todo won't work must be loaded from PrdgmCheck properties
        prdgms.compile(new UserLog());

        PrintWriter out = util.io.IO.openPrintWriter(outFileName);
        for(DeclParadigm prdgm : new TreeSet<DeclParadigm>(prdgms.getParadigms().values()))
            out.println(prdgm.toWordedString());
        out.close();
    }

    // @todo
    protected String helpString() {
        return
        "java PrdgmCheck [options] outFile\n" +
        "options:\n" +
        "  -oe {enc} - encoding of the output file; default: Cp1250";
    }


}