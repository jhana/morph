package morph.ma.module;


import morph.common.UserLog;
import java.util.*;
import lombok.Getter;
import morph.ts.Tag;
import morph.ma.*;
import morph.ma.lts.*;
import util.col.MultiMap;

/**
 *
 * @author  Jirka Hana
 */
public class WordModule extends Module {

    @Getter MultiMap<String, WordEntry> form2entry;

    @Override
    public int compile(UserLog aUlog)  {
        return new WordModuleCompiler().init(this, moduleId, aUlog, args).compile();
    }

    @Override
    public String[] filesUsed() {
        return new String[]  {"file"};
    }

    @Override
    public Collection<Tag> getAllTags() {
        Set<Tag> tags = new TreeSet<Tag>();
        for (WordEntry entry : getEntries().allValues()) {
            tags.add(entry.getTag());
        }
        return tags;
    }


    @Override
    public boolean analyzeImp(String aCurForm, String[] aDecapForms, List<BareForm> aBareForms, Context aCtx, SingleLts aLTS) {
        if (analyzeForm(aCurForm, aLTS)) return true;

        if (aDecapForms != null) {
            analyzeForm(aDecapForms[0], aLTS);
            if (aDecapForms.length > 1)
                analyzeForm(aDecapForms[1], aLTS);
        }
        return !aLTS.isEmpty();
    }

    public MultiMap<String,WordEntry> getEntries() {
        return form2entry;
    }

    public Set<WordEntry> getEntries(String aForm) {
        return form2entry.get(aForm);
    }


    private boolean analyzeForm(String aCurForm, SingleLts aLTS) {
        final Set<WordEntry> wordEntries = form2entry.get(aCurForm);
        if (wordEntries != null) {
            for (WordEntry wordEntry : wordEntries ) {
                aLTS.add(wordEntry.getLemma(), wordEntry.getTag(), null);
                if (wordEntry.getTag().isPunct()) return true;       // @todo hack!!!!
            }
        }
        return false;
    }
}
