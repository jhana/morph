package morph.ma.module;

import morph.common.UserLog;
import java.util.*;
import java.io.*;
import morph.ma.lts.*;
import morph.ma.*;
import util.io.*;
import morph.ts.Tag;
import util.str.Cap;
import util.str.Strings;



/**
 * Abbreviation processing.
 * @author  Jirka
 */
public final class StructGuesser extends Module {
    List<Tag> cAllCapsAbbrTags; 
    List<Tag> cAuthorsAbbrTags; 
    List<Tag> cPeriodAbbrTags; 
    String cConsCons;
    List<Tag> cConsConsTags; 
    List<Tag> cOneLetterTags; 

    Set<String> mAbbrList;
    Set<String> mNonAbbrList;

    @Override
    public String[] filesUsed() {
        if (args.getBool(mpn("lists.enabled"), true)) {
            return new String[] {"allCapsAbbrList", "allCapsNonAbbrList"};
        }
        else {
            return new String[] {};
        }
    }

    public Collection<Tag> getAllTags() {
        Set<Tag> tags = new TreeSet<Tag>();
        tags.addAll(cAllCapsAbbrTags);
        tags.addAll(cAuthorsAbbrTags);
        tags.addAll(cPeriodAbbrTags);
        tags.addAll(cConsConsTags);
        tags.addAll(cOneLetterTags);
        return tags;
    }

    
    public int compile(UserLog aUlog) throws IOException {
        cAllCapsAbbrTags = args.getTags(mpn("allCapsAbbrTags"), tagset);
        cAuthorsAbbrTags = args.getTags(mpn("authorsAbbrTags"), tagset);
        cPeriodAbbrTags  = args.getTags(mpn("periodAbbrTags"), tagset);
        cConsConsTags    = args.getTags(mpn("consConsTags"), tagset);  
        cOneLetterTags   = args.getTags(mpn("oneLetterTags"), tagset);
        cConsCons        = args.getString(mpn("consCons"), "");
        
        readInLists();
        return 0;
    }
    
    private void readInLists() throws IOException {
        if (args.getBool(mpn("lists.enabled"), true)) {
            mAbbrList    = readInList("allCapsAbbrList");
            mNonAbbrList = readInList("allCapsNonAbbrList");
        }
        else {
            mAbbrList    = mNonAbbrList  =new TreeSet<String>();
        }
    }

    private Set<String> readInList(String aPropertyName) throws IOException {
        if (! args.containsKey(mpn(aPropertyName)) ) return new TreeSet<String>();
        
        XFile file  = args.getFile(mpn(aPropertyName), null);
        return loadAbbrSet(file);
    }

    private Set<String> loadAbbrSet(XFile aFileName) throws IOException {
        Set<String> abbrs = new TreeSet<String>();
        
        LineNumberReader r = IO.openLineReader(aFileName);
        
        for (;;) {
            String line = r.readLine();
            if (line == null) break; 
            if (line.trim().length() == 0) continue;
            
            
            String[] tokens = Strings.split(line);
            assert (tokens.length > 0) : r.getLineNumber() + " " + line;
            
            String abbr = tokens[0];
            abbrs.add(abbr);
        }
        r.close();
        return abbrs;
    }
        
        
    
    public boolean analyzeImp(String aCurForm, String[] aDecapForms, List<BareForm> aBareForms, Context aCtx, SingleLts aLTS) {
        int len = aCurForm.length();
        
        // all capitals -> abbrs??
        if (aCtx.curCap().caps())
            doAllCaps(aCurForm, aCtx, aLTS);
        
        // author's abbreviation - @todo improve (not only when empty, but not in the middle of a norm. sentence)
        if (aCtx.prevWordEq(0, "(") && aCtx.nextWordEq(0,")") && len < 5 && aCtx.curCap().lower()) {   // @todo Bog refering to origform
            addLTS(aLTS, aCurForm, cAuthorsAbbrTags, null); // "AuthAbbr" );
        }
        
        // --- single letters ---
        if (len == 1 && Character.isLetter(aCurForm.charAt(0))) {
            addLTS( aLTS, aCurForm, cPeriodAbbrTags, null);// "PerAbbr" );  // @todo hack
            addLTS( aLTS, aCurForm, cOneLetterTags, null); //"1 letter" );
        }

        // abbreviations followed by a period (short, less vowels, ...)
        if (aLTS.isEmpty()) {
            if (aCtx.nextWordEq(0,".") && len < 4) {       // @todo nospace before
                addLTS(aLTS, aCurForm, cPeriodAbbrTags, null);  // "PerAbbr"
            }
        }
        
        
        // probably abbreviation (consonant clusters)
        if (Strings.allCharsIn(aCurForm, cConsCons) && 1 < len && len < 4) {        // @todo support for these functions
            addLTS(aLTS, aCurForm, cConsConsTags, null ); //"ConsAbbr"
        }

        return false;
    }

    private void doAllCaps(String aCurForm, Context aCtx, SingleLts aLTS) {
        // --- we are sure it can be an all caps abbr --- // @todo move this out of a guesser 
        if (mAbbrList.contains(aCurForm)) {    
            addLTS(aLTS, aCurForm, cAllCapsAbbrTags, null); //"CapsAbbr" 
            return;
        }
        
        // --- probably not an all caps abbr ---
        if (mNonAbbrList.contains(aCurForm)) return;
        if ( aCtx.getPrevCap(0) == Cap.caps && aCtx.getNextCap(0) == Cap.caps ) return;

        // --- probably yes ---
        addLTS(aLTS, aCurForm, cAllCapsAbbrTags, null); //"CapsAbbr" 
    }
        
        
    private void addLTS(SingleLts aLTS, String aForm, List<Tag> aTags, Info aInfo) {
        for ( Tag tag :  aTags ) {
            aLTS.add(aForm, tag,  aInfo);
        }
    }
    
}
