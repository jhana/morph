package morph.ma.module;

import java.util.*;
import morph.ma.*;
import morph.ma.lts.*;
import morph.ts.Tag;

public abstract class Module extends morph.common.Module implements java.io.Serializable {

// -----------------------------------------------------------------------------
// Setup
// -----------------------------------------------------------------------------
    
    /**
     * Returns all configuration keys specifying files loaded during compilation.
     *
     * In the future, this will be used to automaticaly recompile outdated binary 
     * modules.
     */
    public String[] filesUsed() {
        return new String[]{};
    }

    /**
     * Optional operation providing all tags that can be emitted by this module.
     * By default returns an empty iterable collection.
     * @return
     */
    public Collection<Tag> getAllTags() {
        return Collections.emptyList();
    }

// ------------------------------------------------------------------------------
// Analysis
// ------------------------------------------------------------------------------    
    
    /**
     * Provides analyses of a string.
     * 
     * @param aCtx context of the current word; can be null (if context is not considered, etc)
     * @param aLts an lts structure the module fills in, the structure is provided
     * from outside to allow dependency injection. It is assumed the structure contains
     * no lt-items (the module can assumes all lt-items present in this structure
     * were put there by it and thus it may filter any or all lt-items there).
     */
    public boolean analyze(String aCurForm, String[] aForms, List<BareForm> bareForms, Context aCtx, SingleLts aLts) {
        if (narrow == null || narrow.matcher(aCurForm).find()) {
            return analyzeImp(aCurForm, aForms, bareForms, aCtx, aLts);
        }
        else {
            return false;
        }
    }
    
    public abstract boolean analyzeImp(String aCurForm, String[] aForms, List<BareForm> bareForms, Context aCtx, SingleLts aLts);

}
