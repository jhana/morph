package morph.ma.module;

import morph.common.UserLog;
import util.str.Strings;
import java.util.*;
import morph.ma.*;
import morph.ma.lts.*;
import morph.ma.module.prdgm.Ending;
import util.col.MultiMap;
import morph.ts.Tag;

/**
 * Module responsible for analyzing words using a lexicon and paradigms
 * @author  Jirka Hana
 */

public class LexModule extends EndingAModule {

    MultiMap<String,LexEntry> mLexicon;

    @Override
    public int compile(UserLog aUlog)  {
        return new LexModuleCompiler().init(this, moduleId, aUlog, args).compile();
    }

    @Override
    public String[] filesUsed() {
        return new String[] {"file", "prdgms.file"};    // "classes.file"
    }


    @Override
    public boolean analyzeImp(String aCurForm, String[] aForms, List<BareForm> aBareForms, Context aCtx, SingleLts aLTS) {
        boolean decapped = !aCtx.curCap().lower();    // eff cache

        for(BareForm bareForm : aBareForms) {
            String form = bareForm.form;
            int maxLen = Math.min(mMaxEndingLen, form.length()-mMinStemLen);

            // --- Try various ending lengths ---

            for(int endingLen = 0; endingLen <= maxLen; endingLen++) {
                String stemCand   = Strings.removeTail(form, endingLen);
                String endingCand = Strings.getTail(   form, endingLen);

                for(Ending endingEntry : mParadigms.getEndings(endingCand)) {
                    if ( (bareForm.ne() && endingEntry.isNeKo()) || (bareForm.nej() && endingEntry.isNejKo()) ) continue;

                    addIfInLexicon(stemCand, endingEntry, bareForm, endingLen, aLTS);

                    // First-cap all all-cap
                    if (decapped) {
                        addIfInLexicon(aCurForm.substring(0, stemCand.length()) , endingEntry, bareForm, endingLen, aLTS);
                        if (aCtx.curCap().caps())
                            addIfInLexicon(aForms[0].substring(0, stemCand.length()) , endingEntry, bareForm, endingLen, aLTS);
                    }
                }
            }
        }

        return !aLTS.isEmpty();  // @todo configure
    }

    private void addIfInLexicon(String aStemCand, Ending aEndingEntry, BareForm aBareForm, int aEndingLen, SingleLts aLTS) {
        //System.out.println("addIfInLexicon: " + aEndingEntry);
        for(LexEntry lexEntry : mLexicon.getS(aStemCand)) {
            //System.out.println(lexEntry);
            if ( lexEntry.fits(aStemCand, aEndingEntry)) {
                Tag tag = aEndingEntry.getTag(aBareForm.ne());
                assert tag != null : aEndingEntry.getTag() + " " + aEndingEntry.isNeOk() + " " + aBareForm.ne() + " " + aEndingEntry.getTag();
                Info info = (collectInfo) ? new DecInfo(lexEntry.getLemmatizingStem(), aStemCand, aEndingEntry, false) : null;

                aLTS.add( lexEntry.lemma, tag, info);
            }
        }
    }


}

