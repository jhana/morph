//package morph.ma.module;
//
//import java.util.*;
//import morph.ma.lts.*;
//import morph.ma.*;
//import pdt.Tags;
//import pdt.Tag;
//import pdt.int;
//import util.col.MultiMap;
//import util.str.StrSet;
//import util.str.Strings;
//import util.str.Transliterate;
//
///**
// *
// * @author  Jiri
// */
//public final class Guesser extends EndingAModule {
//    private final List<LtsFilter> ltsFilters = new ArrayList<LtsFilter>();
//
//// ------------------------------------------------------------------------------
//// Setup
//// ------------------------------------------------------------------------------    
//    @Override
//    public int compile()  {
//        return new GuesserCompiler().init(this, mModuleId, mArgs).compile();
//    }
//
//    @Override
//    public int configure()  {
//        mUseClasses = mModuleArgs.getBool("classes.enabled", false);
//        mMarkEpen   = mModuleArgs.getBool("markEpen",   false);  
//        
//        if ( mModuleArgs.getBool("leo.enabled", false) ) {
//            mCollectInfo  = true;   // leo requires info with ending length
//            
//            // @todo move leo out of lts
//            Lts.setNotCounted( mModuleArgs.getPattern("leo.notCounted",  ""));    // @problem: sets all lts even for other modules
//            Lts.setNotFiltered(mModuleArgs.getPattern("leo.notFiltered", ".*"));
//
//            // @todo allow influencing order
//            addFilter(new LeoFilter());
//        }
//        
//        return 0;
//    }
//
//    @Override
//    public String[] filesUsed() {
//        return new String[] {"classes.file", "prdgms.file"};
//    }
//
//    public void addFilter(LtsFilter aFilter) {
//        ltsFilters.add(aFilter);
//        System.out.println("Filters: " + ltsFilters);
//    }
//
//    public List<LtsFilter> getFilters() {
//        return ltsFilters;
//    }
//
//// ------------------------------------------------------------------------------
//// Use
//// ------------------------------------------------------------------------------    
//    /**
//     *
//     * @return false (morph does not stop_
//     * @todo handle capitalization
//     */
//    @Override
//    public boolean analyze(String aCurForm, String[] aDecapForms, List<BareForm> aBareForms, Context aCtx, SingleLts aLTS) {
//        //System.out.println("Analyze " + aCurForm + " " + mMaxEndingLen);
//
//        for(BareForm bareForm : aBareForms) {
//            String form = bareForm.form;
//            int maxLen = Math.min(mMaxEndingLen, form.length()-mMinStemLen);
//            
//            for(int endingLen = 0; endingLen <= maxLen; endingLen++) {
//                String stemCand   = Strings.removeTail(form, endingLen);
//                String endingCand = Strings.getTail(   form, endingLen);
//                //System.out.println(stemCand + "+" + endingLen + " " + mParadigms.mEndings.getS(endingCand).size());
//                
//                for(Ending endingE : mParadigms.mEndings.getS(endingCand)) {
//                    if ( prefixesOk(bareForm, endingE) ) {
//                        handleEpen(aLTS, bareForm, stemCand, stemCand, endingE);    // -> handleStemChange -> tryToAdd
//                    }
//                }
//            }
//        }
//
//        int size = aLTS.size();
//        filter(aCtx, aLTS);
//        if (aLTS.size() < size) {
//            filtered ++;
//            if (filtered % 50 == 0) System.out.println("Filtered " + filtered);
//        }
//
//        return false;
//    }
//
//    
//// =============================================================================
//// Implementation <editor-fold desc="Implementation">
//// ==============================================================================
//
//    LtsFilter filter;
//
//    /* 
//     * String of subPOS that allow guessing.
//     * language: guesser.guessedSubPOS
//     */
//    String mGuessedSubPOS;
//    
//    boolean mUseClasses;
//
//    boolean mMarkEpen;
//    
//    String mEpenCons;   // @todo regex instead can be improved
//
//    boolean mHandleDerivations = false;
//    
//    Derivations mDerivations;      // prdgm id -> derivations
//
//    
//    
//    private boolean prefixesOk(BareForm aBareForm, Ending aEnding) {
//        return (!aBareForm.nej() || aEnding.isNejOk()) && (!aBareForm.ne() || aEnding.isNeOk());
//    }
//    
///*    
//    private void handleStemChange(LTXM aLTS, BareForm aBareForm, String aStemCand, Ending aEnding) {
//        DeclParadigm prdgm = aEnding.prdgm;
//
//        Collection<String> lemmatizingStems = aPrdgm.lemmatizingStems(aStemCand, aEnding.rootId); // @todo info about epen, applied rules, etc.
//        for (String lemmatizingStem : lemmatizingStems) {
//            tryToAdd(aLTS, aBareForm, lemmatizingStem, aEnding, aPrdgm);
//        }
//    }
// */
//    
//    private void handleEpen(SingleLts aLTS, BareForm aBareForm, String aActualStem, String aStemCand, Ending aEnding) {
//        DeclParadigm prdgm = aEnding.getPrdgm();
//        
//        if (prdgm.mEpenStem == 0 && aEnding.getRootId() != 0 && xxCC(aStemCand)) {  // lematizing stem can contain epenthesis 
//            String eStem = Strings.insertB(aStemCand, 1, 'e');       // insert e   // pisn-i -> pisEn,
//            handleTailChange(aLTS, aBareForm, aActualStem, eStem, aEnding, prdgm, true);
//        }
//        else if (prdgm.mEpenStem > 0 && aEnding.getRootId() == prdgm.mEpenStem && xxCeC(aStemCand)) {  
//            String noEStem = Strings.deleteCharB(aStemCand, 1);      // delete e  // her -> hr-a
//            handleTailChange(aLTS, aBareForm, aActualStem, noEStem, aEnding, prdgm, true);
//        }
//        handleTailChange(aLTS, aBareForm, aActualStem, aStemCand, aEnding, prdgm, false);
//    }
//
//    private void handleTailChange(SingleLts aLTS, BareForm aBareForm, String aActualStem, String aStemCand, Ending aEnding, DeclParadigm aPrdgm, boolean aEpen) {
//        if (aEnding.getRootId() != 0) {      // ? exclude epenthesis??
//            String lemmatizingStem = aPrdgm.lemmatizingStem(aStemCand, aEnding.getRootId());
//            tryToAdd(aLTS, aBareForm, aActualStem, lemmatizingStem, aEnding, aPrdgm, aEpen);
//        }
//        
//        tryToAdd(aLTS, aBareForm, aActualStem, aStemCand, aEnding, aPrdgm, aEpen);
//    }    
//    
//    
//    void tryToAdd(SingleLts aLTS, BareForm aBareForm, String aActualStem, String aStemCand, Ending aEnding, DeclParadigm aPrdgm, boolean aEpen) {
//        if (!aPrdgm.phonologyOK(aStemCand)) return; // @todo move to Prdgm.lemmatizingStems
//        
//        Tag tag = aEnding.getTag(aBareForm.ne());   // @todo superlative
//        assert tag != null : Transliterate.transliterate(aEnding.getEnding()) + " " + aEnding.getTag() + " " + aEnding.isNeOk();
//
//        DecInfo info = (mCollectInfo) ? new DecInfo(aStemCand, aActualStem, aEnding, aEpen) : null;
//        aLTS.add(aStemCand + aPrdgm.defEnding, tag, info);
//        
//        if (mHandleDerivations)
//            derivations(aLTS, aBareForm, aStemCand, aEnding, tag, aPrdgm, info);
//    }    
//    
//    
//
//    // form=soudru��t� -> aStem=soudru�sk, aEnding=�
//    private void derivations(SingleLts aLTS, BareForm aBareForm, String aStem, Ending aEnding, Tag aTag, DeclParadigm aPrdgm, final DecInfo aDeclInfo) {
//        Collection<Derivation> derivations = mDerivations.getDerivations(aPrdgm.id);
//        if (derivations == null) return;
//        
//        for (Derivation derivation : derivations) {
//            if (aStem.endsWith(derivation.mSuffix)) {                               // suffix=sk
//                String rootCand   = Strings.removeTail(aStem, derivation.mSuffix);  // rootCand=soudru�
//                addDerivation(aLTS, derivation, rootCand, aEnding, aTag, aDeclInfo);  // @todo overgenerates - maybe this would be translated by some subrule, do the same everywhere
//                
//                String mergePointRoot = derivation.mRootRule.translateTail(rootCand, 1, 0);
//                if (!mergePointRoot.equals(rootCand))
//                    addDerivation(aLTS, derivation, mergePointRoot, aEnding, aTag, aDeclInfo);
//            }
//        }
//    }
//
//    private void addDerivation(final SingleLts aLTS, final Derivation derivation, final String mergePointRoot, final Ending aEnding, final Tag aTag, final DecInfo aDeclInfo) {
//        if (derivPhonologyOK(derivation,mergePointRoot)) {
//            // @todo handle epenthesis
//            if (derivation.mBaseParadigm.phonologyOK(mergePointRoot)) {
//                DerInfo info = (mCollectInfo) ? new DerInfo(mergePointRoot, derivation, false, aDeclInfo) : null;
//                aLTS.add(mergePointRoot + derivation.mBaseParadigm.defEnding, aTag, info);
//            }
//
//            String lemmatizingRoot = derivation.mBaseParadigm.lemmatizingStem(mergePointRoot, derivation.mBaseMergePoint);
//                
//            if (derivation.mBaseParadigm.phonologyOK(lemmatizingRoot)) {
//                DerInfo info = (mCollectInfo) ? new DerInfo(lemmatizingRoot, derivation, false, aDeclInfo) : null;
//                aLTS.add(lemmatizingRoot + derivation.mBaseParadigm.defEnding, aTag, info);
//            }
//        }
//    }
//    
//    
//    // @todo move to derivation; make a RootTails or even Phonotactics class (different instances - simple tails, regex, simple characters)
//    private boolean derivPhonologyOK(Derivation aDerivation, String aRoot) {
//        if (aDerivation.mRootTails == null || aDerivation.mRootTails.length == 0) return true;
//        // -- go thru the class --
//        // @todo optimize (similarly as stem rule)
//        for (String c : aDerivation.mRootTails) {
//            if (aRoot.endsWith(c)) return true;
//        }
//        
//        return false;   
//    }
//    
//    
//    // @todo regex
//    private boolean xxCeC(String aString) {
//        return 
//            (aString.length() > 3) &&
//            Strings.charInB(aString, 0, mEpenCons) &&
//            Strings.charAtB(aString, 1) == 'e' &&
//            Strings.charInB(aString, 2, mEpenCons);
//    }
//    
//    private boolean xxCC(String aString) {
//        return 
//            (aString.length() > 2) &&
//            Strings.charInB(aString, 0, mEpenCons) &&
//            Strings.charInB(aString, 1, mEpenCons);
//    }
//
//    private void filter(Context aCtx, SingleLts aLTS) {
//        for (LtsFilter f : ltsFilters) {
//            f.filter(aLTS, aCtx);
//        }
////        if (mLeo)
////            aLTS.longestEndingsOnly();
////
////        if (false) {
////            filterByPrep(aLTS, aCtx);   // should remove only it's on analysis
////        }
////
////        filterByCase(aLTS, aCtx);
//    }
//
//
//    static class LeoFilter implements LtsFilter {
//        @Override
//        public void filter(Lts aLTS, Context aCtx) {
//            aLTS.longestEndingsOnly();
//        }
//    }
//
//    // @todo move out and make configurable
//    static class PrevWordFilter implements LtsFilter {
//        private final Map<String,StrSet> form2Vals;
//        private final int slot;
//        private final String valid; /** Always valid (wildcards) */
//
//        public PrevWordFilter(Map<String,StrSet> aMap, int aSlot, String aValid) {
//            form2Vals = aMap;
//            slot = aSlot;
//            valid = aValid;
//        }
//
//        @Override
//        public void filter(Lts aLts, Context aCtx) {
//            if (! aCtx.hasPrevWord(0)) return;
//
//            String prevForm = aCtx.getPrevWord(0);
//            StrSet possibleVals = form2Vals.get(prevForm);
//            if (possibleVals == null) return;
//
//            //System.out.println("Filtering: " + form);
//
//            for (Iterator<Lt> i = aLts.iterator(); i.hasNext();) {
//                Lt lt = i.next();
//
//                char val = lt.tag().getSlot(slot);
//                if (  val != '-' && !possibleVals.contains( val ) && valid.indexOf(val) == -1 ) {
//                    i.remove();
//                }
//            }
//        }
//    }
//
//    static class TagFilter implements LtsFilter {
//        private final MultiMap<String,Tag> form2tags;
//
//        public TagFilter(MultiMap<String,Tag> aMap) {
//            form2tags = aMap;
//        }
//
//        @Override
//        public void filter(Lts aLts, Context aCtx) {
//            final String form = aCtx.curForm();       // check if it is the correct form (capitalization, prefixes, etc)
//            final Set<Tag> possibleTags = form2tags.get(form);
//            if (possibleTags == null) return;
//
//            System.out.println("Filter found " + form);
//            boolean filtered = false;
//            for (Iterator<Lt> i = aLts.iterator(); i.hasNext();) {
//                Lt lt = i.next();
//                if ( !possibleTags.contains(lt.tag()) ) {
//                    i.remove(); filtered = true;
//                }
//            }
//            if (filtered) System.out.println("   +++++");
//        }
//    }
//
//
//    static class CaseFilter implements LtsFilter {
//        private final Map<String,StrSet> form2Cases;
//        private final int slot;
//        private final String valid;
//
//        public CaseFilter(Map<String,StrSet> aMap, int aSlot, String aValid) {
//            form2Cases = aMap;
//            slot = aSlot;
//            valid = aValid;
//        }
//
//        @Override
//        public void filter(Lts aLts, Context aCtx) {
//            String form = aCtx.curForm();       // check if it is the correct form (capitalization, prefixes, etc)
//            StrSet possibleVals = form2Cases.get(form);
//            if (possibleVals == null) return;
//
//            //System.out.println("Filtering: " + form);
//
//            for (Iterator<Lt> i = aLts.iterator(); i.hasNext();) {
//                Lt lt = i.next();
//
//                char val = lt.tag().getSlot(slot);
//                if (  val != '-' && !possibleVals.contains( val ) && valid.indexOf(val) == -1 ) {
//                    i.remove();
//                }
//            }
//        }
//    }
//
//    int filtered = 0;
//
//    /**
//     * o-6 Hanky-2 mamince-6
//     * o-6 detem-3 venovanem-6 darku-6
//     * But noun-6 cannot be preceded by non-6 preposition
//     */
//    static class PrepFilter implements LtsFilter {
//
//        @Override
//        public void filter(Lts aLTS, Context aCtx) {
//            if (aCtx.getPrevAnals(0) == null) return;
//
//            Collection<Tag> prevTags = aCtx.getPrevAnals(0).tags();
//            // @todo require punctuation to follow
//
//            // must be nonabiguously prep
//            if (! Tags.allTags(prevTags, int.POS, 'R') ) return;
//
//            Set<Character> cases =  Tags.collectValues(prevTags, int.casex);
//
//            // --- go thru current analysis ---
//            for (Tag tag : aLTS.tags()) {
//                if (tag.POSIn("NAP") && ! caseOk(tag, cases) ) {
//                    aLTS.remove(tag);
//                }
//            }
//        }
//
//        /**
//         *
//         * @todo generalize to slots and move to tag
//         */
//        boolean caseOk(Tag aTag, Set<Character> aCases) {
//            for (Character casex : aCases ) {
//                if (caseInters(aTag.getCase(), casex)) return true;
//            }
//            return false;
//        }
//
//    /**
//     * possible case values "[1-7]X-"
//     */
//    private boolean caseInters(char aCase, char aCase2) {
//        if (aCase == aCase2) return true;
//        if (aCase  == 'X' && aCase2 != '-') return true;
//        if (aCase2 == 'X' && aCase  != '-') return true;
//        return false;
//    }
//
//    }
//
//
//
//
//// </editor-fold>    
//    
//}
