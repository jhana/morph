package morph.ma.module;

import morph.common.UserLog;
import java.util.*;
import java.io.*;
import util.io.*;
import util.str.Strings;
import morph.ts.Tag;
import morph.ma.lts.*;
import morph.ma.*;
import util.str.Cap;

/**
 * Abbreviation processing.
 * @author  Jirka
 */
public final class AllCaps extends Module {

    @Override
    public int compile(UserLog aLog) throws IOException {
        cAllCapsAbbrTags = args.getTags(mpn("allCapsAbbrTags"), tagset);
        
        readInLists();
        return 0;
    }
        
    @Override
    public String[] filesUsed() {
        if (args.getBool(mpn("lists.enabled"), true)) {
            return new String[] {"allCapsAbbrList", "allCapsNonAbbrList"};
        }
        else {
            return new String[] {};
        }
    }
    
    @Override
    public boolean analyzeImp(String aCurForm, String[] aDecapForms, List<BareForm> aBareForms, Context aCtx, SingleLts aLTS) {
        int len = aCurForm.length();
        
        // all capitals -> abbrs??
        if (aCtx.curCap().caps())
            doAllCaps(aCurForm, aCtx, aLTS);
        
        return false;
    }

// =============================================================================
// Implementation <editor-fold desc="Implementation">
// ==============================================================================
    private List<Tag> cAllCapsAbbrTags; 

    private Set<String> mAbbrList;
    private Set<String> mNonAbbrList;

// ------------------------------------------------------------------------------
// Compilation
// ------------------------------------------------------------------------------    
    private void readInLists() throws IOException {
        if (moduleArgs.getBool("lists.enabled", true)) {
            mAbbrList    = readInList("allCapsAbbrList");
            mNonAbbrList = readInList("allCapsNonAbbrList");
        }
        else {
            mAbbrList    = mNonAbbrList  = new TreeSet<String>();
        }
    }

    private Set<String> readInList(String aPropertyName) throws IOException {
        if (! moduleArgs.containsKey(aPropertyName) ) return new TreeSet<String>();
        
        XFile file  = args.getFile(mpn(aPropertyName), null);     // @todo weird
        return loadAbbrSet(file);
    }

    private Set<String> loadAbbrSet(XFile aFileName) throws IOException {
        System.out.printf("Loading (non)abbr list %s\n", aFileName);
        Set<String> abbrs = new TreeSet<String>();
        
        LineNumberReader r = IO.openLineReader(aFileName);
        
        for (;;) {
            String line = r.readLine();
            if (line == null) break; 
            if (line.trim().length() == 0) continue;
            
            
            String[] tokens = Strings.split(line);
            assert (tokens.length > 0) : r.getLineNumber() + " " + line;
            
            String abbr = tokens[0];
            abbrs.add(abbr);
        }
        r.close();
        return abbrs;
    }

// ------------------------------------------------------------------------------
// Analysis
// ------------------------------------------------------------------------------    
    private void doAllCaps(String aCurForm, Context aCtx, SingleLts aLTS) {
        // --- we are sure it can be an all caps abbr --- 
        if (mAbbrList.contains(aCurForm)) {    
            addLTS(aLTS, aCurForm, cAllCapsAbbrTags, null); //"CapsAbbr" 
            return;
        }
        
        // --- probably not an all caps abbr ---
        if (mNonAbbrList.contains(aCurForm) && 
                !(aCtx.hasPrevWord(0) && aCtx.getPrevWord(0).equals("(") && aCtx.hasNextWord(0) && aCtx.getNextWord(0).equals(")")) ) return;
        if (aCurForm.length() > 5) return;
        if ( aCtx.getPrevCap(0) == Cap.caps || aCtx.getNextCap(0) == Cap.caps ) return;

        // --- probably yes ---
        addLTS(aLTS, aCurForm, cAllCapsAbbrTags, null); //"CapsAbbr" 
    }
        
        
    private void addLTS(SingleLts aLTS, String aForm, List<Tag> aTags, Info aInfo) {
        for ( Tag tag :  aTags ) {
            aLTS.add( aForm, tag,  aInfo );
        }
    }
    
// </editor-fold>
}
