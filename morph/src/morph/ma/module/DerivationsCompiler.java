package morph.ma.module;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import morph.ma.module.prdgm.Derivation;
import morph.util.string.ScrParser;
import util.col.MultiMap;
import util.err.Err;
import util.err.UException;
import util.str.Cap;

/**
 * This hard-coding of derivations is a temporary thing just to try the thing.
 * Derivations will be specified in a file the same way as paradigms, etc. are.
 *
 * This should be done somehow differently, it is hard to find affixes clustered
 * this way. Instead it would be better to specify affix by affix, forming a network.
 * Some affixes can possibly go in chains. But how to mix it with the rest?
 *
 * @author Jirka
 */
public class DerivationsCompiler extends ModuleCompiler<Derivations> {

    public void setPrdgms(Paradigms aPrdgms) {
        mParadigms = aPrdgms;
    }

    protected void loadParadigms() throws java.io.IOException {
        try {
            mParadigms = new Paradigms();
            mParadigms.init(mModuleId, args);
            mParadigms.compile(ulog);   // no filtering used for now
            mParadigms.configure();
        } catch (UException e) {
            ulog.handleError(e, "Cannot load paradigms submodule");
        }
    }


// =============================================================================
// Implementation <editor-fold desc="Implementation">
// ==============================================================================


    private Paradigms mParadigms;


    @Override
    protected void compileI() throws IOException {
        Err.iAssert(mParadigms != null, "Derivations - No paradigms loaded");

        //mModule.mDerivations      = new HashMap<String,Derivation>();
        mModule.mPrdgm2Derivation = new MultiMap<String,Derivation>();

        //if (true) return;

        // noun -> adj
        for (String prdgm : mParadigms.getParadigms().keySet()) {
            if (! Pattern.compile("N.*").matcher(prdgm).matches()) continue;
            addDerivation("", "Super_N_n_Adj" + prdgm, prdgm, "", "",   prdgm,  0, "%/%");
            addDerivation("", "Super_N_n_Adj" + prdgm, prdgm, "", "н", "AA", 0, "г/ж:к/ч:х/ш:ц/ч:л/ль");
        }

        for (String prdgm : mParadigms.getParadigms().keySet()) {
            if (! Pattern.compile("N.*").matcher(prdgm).matches()) continue;
            addDerivation("", "Super_N_sk_Adj" + prdgm, prdgm, "", "",   prdgm,  0, "%/%");  // todo names of people
            addDerivation("", "Super_N_sk_Adj" + prdgm, prdgm, "", "ск", "AA", 0, "г/ж:к/ч:х/ш:ц/ч:л/ль");
        }

        for (String prdgm : mParadigms.getParadigms().keySet()) {
            if (! Pattern.compile("N.*").matcher(prdgm).matches()) continue;
            addDerivation("", "Super_N_ov_Adj" + prdgm, prdgm, "", "",   prdgm,  0, "%/%");
            addDerivation("", "Super_N_ov_Adj" + prdgm, prdgm, "", "ов", "AA", 0, "");
        }

        for (String prdgm : mParadigms.getParadigms().keySet()) {
            if (! Pattern.compile("N.*").matcher(prdgm).matches()) continue;
            addDerivation("", "Super_N_ev_Adj" + prdgm, prdgm, "", "",   prdgm,  0, "%/%");  // todo names of people
            addDerivation("", "Super_N_ev_Adj" + prdgm, prdgm, "", "ев", "AA", 0, "");
        }

    }

    protected void compileI1() throws IOException {
        Err.iAssert(mParadigms != null, "Derivations - No paradigms loaded");

        //mModule.mDerivations      = new HashMap<String,Derivation>();
        mModule.mPrdgm2Derivation = new MultiMap<String,Derivation>();

        //if (true) return;

        List<String> prdgms = Arrays.asList(
                "NIstroj", "NIstroj-e3", "NIhrad", "NIhrad-e", "NIostrov", "NIostrov-e", "NIzamek", "NIdomecek",
                "NFzena", "NFburza", "NFskica", "NFruze", "NFzeme", "NFice", "NFja", "NFia", "NFpisen", "NFpanev", "NFnaruc", "NFkost");

        for (String prdgm : prdgms) {
            addDerivation("", "Super" + prdgm, prdgm, "C", "",   prdgm,  0, "%/%");
            addDerivation("", "Super" + prdgm, prdgm, "C", "ov", "AdjHard", 0, "ch/\u0161:h/\u017E");       // @todo epenthesis pisnovy
            addDerivation("", "Super" + prdgm, prdgm, "C", "ov", "advve",      0, "%/%");
        }

        prdgms = Arrays.asList("NMpan", "NMobcan", "NMhoch", "NMhugo", "NMmuz", "NMmuz-e3", "NMobyvatel", "NMsoudce", "NMa", "NMha");

        // Novak - Novakova - Novakuv
        for (String prdgm : prdgms) {
            addDerivation("", "Novak", "SuperM" + prdgm, prdgm, "C", "", prdgm,       0, "%/%",Cap.firstCap);
            addDerivation("", "Novakova","SuperM" + prdgm, prdgm, "C", "", "Nova",      0, "%/%",Cap.firstCap);
            addDerivation("", "Novakuv", "SuperM" + prdgm, prdgm, "C", "", "AdjPossM",  1, "%/%",Cap.firstCap); // no epenthesis (root #1)
        }

        prdgms = Arrays.asList("NFzena", "NFburza", "NFskica", "NFruze", "NFzeme", "NFice", "NFja", "NFia", "NFpisen", "NFpanev", "NFnaruc", "NFkost");

        // matka - mat?in
        for (String prdgm : prdgms) {
            addDerivation("", "SuperF" + prdgm, prdgm, "C", "", prdgm,       0, "%/%");
            addDerivation("", "SuperF" + prdgm, prdgm, "C", "", "AdjPossF",  0, "ch/\u0161:g/\u017E:h/\u017E:k/\u010D:r/\u0159");
        }

        // Plzen - Plzensko - plzensky - Plzenan/Plzenanka/Plzenak/Plzenacka
        // Znojmo â‚¬ - Znojemsk+o - Znojemsk+Ã½  (?not trying Znojmo, how to do epenthesis)
        prdgms = Arrays.asList(//"NIhrad");
                "NIstroj", "NIstroj-e3", "NIhrad", "NIhrad-e", "NIostrov", "NIostrov-e", "NIzamek", "NIdomecek",
                "NFzena", "NFburza", "NFskica", "NFruze", "NFzeme", "NFice", "NFja", "NFia", "NFpisen", "NFpanev", "NFnaruc", "NFkost");

        for (String prdgm : prdgms) {
            addDerivation("1", "Cheb",     "SuperSk" + prdgm, prdgm, "", "",         prdgm,     0, "%/%", Cap.firstCap);
            addDerivation("2", "Chebsko",  "SuperSk" + prdgm, prdgm, "", "sk",       "NNko",    0, "%/%", Cap.firstCap);
            addDerivation("3", "chebskÃ½",  "SuperSk" + prdgm, prdgm, "", "sk",       "AdjHard", 0, "%/%", Cap.lower);
            addDerivation("4", "Cheban",   "SuperSk" + prdgm, prdgm, "", "an",       "NMpan",   0, "%/%", Cap.firstCap);
            addDerivation("5", "Chebanka", "SuperSk" + prdgm, prdgm, "", "ank",      "NFburza", 0, "%/%", Cap.firstCap);
            addDerivation("6", "ChebÃ¡k",   "SuperSk" + prdgm, prdgm, "", "\u00E1k",  "NMhoch",  0, "%/%", Cap.firstCap);
            addDerivation("7", "Chebacka", "SuperSk" + prdgm, prdgm, "", "a\u010Dk", "NFburza", 0, "%/%", Cap.firstCap);
            addDerivation("8", "IrÃ¡nec",   "SuperSk" + prdgm, prdgm, "", "ec",       "NMpan",   0, "%/%", Cap.firstCap);
    //        addDerivation("",     "Super" + prdgm, prdgm, "", "sk",  "advy", 0, "%/%");
        }

        // Bulhar - Bulharka - Bulharsko - bulharskÃ½ - bulharsky
        prdgms = Arrays.asList("NMpan", "NMobcan", "NMhoch", "NMhugo", "NMmuz", "NMmuz-e3", "NMobyvatel", "NMsoudce", "NMa", "NMha");

        for (String prdgm : prdgms) {
            addDerivation("1", "Bulhar",    "SuperBul" + prdgm, prdgm, "", "",   prdgm,     0, "%/%",  Cap.firstCap);
            addDerivation("2", "Bulharka",  "SuperBul" + prdgm, prdgm, "", "k",  "NFburza",  0, "%/%", Cap.firstCap);
            addDerivation("3", "Bulharsko", "SuperBul" + prdgm, prdgm, "", "sk", "NNko",     0, "%/%", Cap.firstCap);       // @todo warning sk + NNo => clash
            addDerivation("4", "bulharskÃ½", "SuperBul" + prdgm, prdgm, "", "sk", "AdjHard", 0, "%/%",  Cap.lower);
    //        addDerivation("",     "Super" + prdgm, prdgm,  "", "sk", "advy", 0, "%/%");
        }
        // Nemec - !Nem-ka - Nemecko - nemecky - nemecky  1Nem


        // complicated alternations, @todo allow optionality
        addDerivation("",     "SuperVpecu",    "Vpecu",   "", "",  "Vpecu",    0, "%/%");
        addDerivation("",     "SuperVmaze",    "Vmaze",   "", "",  "Vmaze",    0, "%/%");  // ?? at/a't
        addDerivation("",     "SuperVumre",    "Vumre",   "", "",  "Vumre",    0, "%/%");
        addDerivation("",     "SuperVmine",    "Vmine",   "", "",  "Vmine",    0, "%/%");
        addDerivation("",     "SuperVtiskne",  "Vtiskne", "", "",  "Vtiskne",  0, "%/%");
        addDerivation("",     "SuperVzacne",   "Vzacne",  "", "",  "Vzacne",   0, "%/%");
        addDerivation("",     "SuperVkryt",    "Vkryt",   "", "",  "Vkryt",    0, "%/%");
        addDerivation("",     "SuperVkupovat", "Vkupovat","", "",  "Vkupovat", 0, "%/%");
        addDerivation("a",     "SuperVprosi",   "Vprosi",  "", "",  "Vprosi",   0, "%/%");   // changes
        addDerivation("",     "SuperVsazi",    "Vsazi",   "", "",  "Vsazi",    0, "%/%");
        addDerivation("",     "SuperVdelat",   "Vdelat",  "", "",  "Vdelat",   0, "%/%");

        addDerivation("",     "SuperVpecu",    "Vpecu",   "", "en",        "NNi", 0, "%/%");
        addDerivation("",     "SuperVmaze",    "Vmaze",   "", "\u00E1n",   "NNi", 0, "%/%");  // ?? at/a't
        addDerivation("",     "SuperVumre",    "Vumre",   "", "en",        "NNi", 0, "%/%");
        addDerivation("",     "SuperVmine",    "Vmine",   "", "nut",       "NNi", 0, "%/%");
        addDerivation("",     "SuperVtiskne",  "Vtiskne", "", "nut",       "NNi", 0, "%/%");
        addDerivation("",     "SuperVzacne",   "Vzacne",  "", "nut",       "NNi", 0, "%/%");
        addDerivation("",     "SuperVkryt",    "Vkryt",   "", "t",         "NNi", 0, "%/%");
        addDerivation("",     "SuperVkupovat", "Vkupovat","", "ov\u00E1n", "NNi", 0, "%/%");
        addDerivation("b",     "SuperVprosi",   "Vprosi",  "", "en",        "NNi", 0, "%/%");   // changes
        addDerivation("",     "SuperVprosi",   "Vprosi",  "tdsz", "en",    "NNi", 0, "d/z:t/c:s/\u0161:z/\u017E");   // changes
        addDerivation("",     "SuperVprosi",   "Vprosi",  "dtn", "\u011Bn","NNi", 0, "%/%");   // changes
        addDerivation("",     "SuperVprosi",   "Vprosi",  "td",  "\u011Bn","NNi", 0, "st/\u0161t:zd/\u017Ed");   // changes
        addDerivation("",     "SuperVsazi",    "Vsazi",   "", "en",        "NNi", 0, "%/%");
        addDerivation("",     "SuperVdelat",   "Vdelat",  "", "\u00E1n",   "NNi", 0, "%/%");

        //System.out.println(Cols.toStringNl( mModule.mPrdgm2Derivation.entrySet()) );
////        String prdgm = "Vkupovat";
////        addDerivation("",     "SuperV" + prdgm, prdgm, "C", "",     prdgm,  0, "%/%");
////        addDerivation("",     "SuperV" + prdgm, prdgm, "C", "ov\u00E1n", "NNi",  0, "%/%");
////
////        prdgm = "Vprosi";
////        addDerivation("",     "SuperV" + prdgm, prdgm, "C", "",   prdgm,  0, "%/%");
////        addDerivation("",     "SuperV" + prdgm, prdgm, "C", "en", "NNi",  0, "at/ac:et/ec:it/ic:ot/oc:ut/uc:d/z:s/\u0161:z/\u017E");
////
////        addDerivation("",     "SuperVa" + prdgm, prdgm, "stZdN", "",   prdgm,  0, "%/%");
////        addDerivation("",     "SuperVa" + prdgm, prdgm, "stZdN", "\u011Bn", "NNi", 0, "st/\u0161t:zd/\u017Ed");
//
        prdgms = Arrays.asList("AdjHard", "AdjSoft");
        for (String prdgm : prdgms) {
            addDerivation("", "Super" + prdgm, prdgm, "C", "", prdgm, 0, "ch/\u0161:h/\u017E");       // @todo epenthesis pisnovy
            addDerivation("", "Super" + prdgm, prdgm, "C", "", "adve",      0, "%/%");
            addDerivation("", "Super" + prdgm, prdgm, "C", "", "advve",     0, "%/%");
            addDerivation("", "Super" + prdgm, prdgm, "C", "ost", "NFkost",    0, "%/%");
        }

        addDerivation("", "SuperCky", "AdjHard", "ckSk", "", "AdjHard", 0, "ch/\u0161:h/\u017E");       // @todo epenthesis pisnovy
        addDerivation("", "SuperCky", "AdjHard", "ckSk", "", "advy",      0, "%/%");

    }


    private void addDerivation(Derivation aDerivation) {
        //System.out.println("adding der " + aDerivation.mPrdgm.id + " " + aDerivation);
        //mModule.mDerivations.put(aDerivation.mId, aDerivation);
        mModule.mPrdgm2Derivation.add(aDerivation.mPrdgm.id, aDerivation);
    }


    private void addDerivation(String aId, String aEx, String aSuperPrdgm, String aBasePrdgm, String aRootTailClass, String aSuffix, String aAddPrdgm, int aBaseMergePoint, String aRootChangeRule, Cap aCapChange) {
        Derivation der = addDerivation(aId, aSuperPrdgm, aBasePrdgm, aRootTailClass, aSuffix, aAddPrdgm, aBaseMergePoint, aRootChangeRule);
        der.mCap = aCapChange;
        der.mExample = aEx;
        //System.out.println(der);
    }

    private Derivation addDerivation(String aId, String aSuperPrdgm, String aBasePrdgm, String aRootTailClass, String aSuffix, String aAddPrdgm, int aBaseMergePoint, String aRootChangeRule) {
        //System.out.println("paradigms\n" + Cols.toStringNl(mModule.mParadigms.mParadigms.values()));
        final Derivation der = new Derivation(aId);
        der.mSuperParadigm  = aSuperPrdgm;
        der.mBaseParadigm   = mParadigms.getParadigm(aBasePrdgm);
        if (der.mBaseParadigm == null) {
            System.out.println("Warning: base prdgm" + aBasePrdgm + " is unknown");
            return der;
        }

        der.mRootTails      = mParadigms.getClasses().get(aRootTailClass);
        Err.assertE(aRootTailClass.equals("") || der.mRootTails != null, "mRootTails " + aRootTailClass);
        der.mSuffix         = aSuffix;
        der.mPrdgm          = mParadigms.getParadigm(aAddPrdgm);
        //Err.assertE(der.mPrdgm != null, "aAddPrdgm " + aAddPrdgm);
        if (der.mPrdgm == null) {
            System.out.println("Warning: prdgm " + aAddPrdgm + " is unknown");
            return der;
        }

        der.mBaseMergePoint = aBaseMergePoint;
        der.mRootRule       = ScrParser.getStrChangeRule(aRootChangeRule);

        addDerivation(der);

        return der;
    }

// </editor-fold>
}
