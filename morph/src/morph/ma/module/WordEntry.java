package morph.ma.module;

import java.util.Comparator;
import lombok.Data;
import lombok.Getter;
import morph.ts.Tag;
import util.col.Mapper;

/**
 * Form with its analysis. There might be multiple WordEntries for a single form.
 * @author  Jirka Hana
 */
@Data
public class WordEntry implements java.io.Serializable {
    @Getter private final String form;
    @Getter private final String lemma;
    @Getter private final Tag tag;

    public static Mapper<WordEntry,String> e2form = new Mapper<WordEntry,String>() {
        @Override
        public String map(WordEntry aOrigItem) {
            return aOrigItem.form;
        }
    };
    
    public static Comparator<WordEntry> lftComparator = new Comparator<WordEntry>() {
        @Override
        public int compare(WordEntry o1, WordEntry o2) {
            int tmp = o1.getLemma().compareTo(o2.getLemma());
            if (tmp != 0) return tmp;
            tmp = o1.getForm().compareTo(o2.getForm());
            if (tmp != 0) return tmp;
            return o1.getTag().compareTo(o2.getTag());
        }
    };

    public static Comparator<WordEntry> ltfComparator = new Comparator<WordEntry>() {
        @Override
        public int compare(WordEntry o1, WordEntry o2) {
            int tmp = o1.getLemma().compareTo(o2.getLemma());
            if (tmp != 0) return tmp;
            tmp = o1.getTag().compareTo(o2.getTag());
            if (tmp != 0) return tmp;
            return o1.getForm().compareTo(o2.getForm());
        }
    };


    public static Comparator<WordEntry> lemmaComparator = new Comparator<WordEntry>() {
        @Override
        public int compare(WordEntry o1, WordEntry o2) {
            return o1.getLemma().compareTo(o2.getLemma());
        }
    };
}
