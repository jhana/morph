package morph.ma.module;

import morph.common.UserLog;
import java.io.*;
import java.util.*;
import util.io.*;
import util.opt.Options;
import morph.io.*;
import morph.util.MArgs;


//if (mMorph.mOptions.mFilterTransliteration) 
//    inFilter = new Filter() {
//        public String filter(String aString) {return Transliterate.transliterate(aString);}
//    };
//else
//    inFilter = Filter.cIdentityFilter;


/**
 * Object that may but does not have to be used by modules' compile method. 
 *
 * @param <T> 
 * @author  Jirka
 */
public abstract class ModuleCompiler<T extends Module> extends morph.ma.Compiler {
    
    /** Creates a new instance of ModuleCompiler */
    public ModuleCompiler() {
    }
   
    /**
     * 
     * @param aModule 
     * @param aModuleId 
     * @param aModules   @todo drop
     * @param aArgs 
     * @return 
     */
    public ModuleCompiler init(T aModule, String aModuleId, UserLog aULog, MArgs aArgs)  {
        super.init(aULog, aArgs);

        ulog.setDetailedError( args.getBool("debug.detailedError", false) );

        mModule = aModule;
        mModuleId = aModuleId; 
        mModuleArgs = aArgs.getSubtree(aModuleId);
        mDataPath   = args.getDir("dataPath", null);

        return this;
    }

    /**
     * Compiles the module. 
     * Calls compileI method to do the compilation itself.
     * Any uncaught exceptions are caught (increasing error count).
     *
     * @return number of errors that occured during compilation
     * @see #compileI
     */
    public int compile() {
        try {
            compileI();
        }
        catch(Throwable e) {
            ulog.handleError(e, "[%s] Uncaught compiler error\n", mModuleId);
        }
        
        return ulog.getErrors();
    }
    
    public List<String> properties() {
        //@todo move to the module
        return Arrays.asList("??");
    }

// =============================================================================
// Implementation <editor-fold desc="Implementation">
// =============================================================================
    transient protected Options mModuleArgs;
    protected T mModule;
    protected String mModuleId; 

    /**
     * Root of language files.
     * Specified via (global) dataPath property.
     */
    protected File mDataPath;

    transient protected boolean detailedError = false;
    
    /**
     * Implement your own compilation code in this method.
     * Anytime an error occurs, increment mErrors.
     */
    protected abstract void compileI() throws Exception;
    
// -----------------------------------------------------------------------------
// Checking and error handling <editor-fold desc="Checking and error handling">
// -----------------------------------------------------------------------------
    @Deprecated
    protected void assertTrueW(final boolean aCond, ReaderParser aR, String aFormatString, Object ... aParams)  {
        ulog.assertTrueW(aCond, aR, aFormatString, aParams);
    }
    
    @Deprecated
    protected void assertTrue(final boolean aCond, ReaderParser aR, String aFormatString, Object ... aParams)  {
        ulog.assertTrue(aCond, aR, aFormatString, aParams);
    }

    @Deprecated
    protected void handleError(ReaderParser aR, String aFormatString, Object ... aParams)  {
        ulog.handleError(aR, aFormatString, aParams);
    }
    
    @Deprecated
    protected void handleError(Exception aE, ReaderParser aR) {
        ulog.handleError(aE, aR, "");
    }
    
    
// </editor-fold>    
// -----------------------------------------------------------------------------
// Properties <editor-fold desc="Properties">
// -----------------------------------------------------------------------------
    
    @Deprecated
    protected String mpn(String aPropertyName) {
        return mModuleId + '.' + aPropertyName;
    }
    
    /**
     * Retrieves a file relative to mDataPath.
     *
     * @ if the property is not specified, or the file cannot be read.
     * @see #mDataPath
     */
    public XFile getMFile(String aPropertyName)  {
        XFile file = mModuleArgs.getFile(aPropertyName, mDataPath, null);

        ulog.assertTrue(file != null, "The property %s:File is unspecified.\n", aPropertyName);
        ulog.assertTrue(file.file().canRead(), "Cannot find or read the file %s, defined by %s\n", file.file(), aPropertyName);

        return file;
    }

    /**
     * Gets a list of files.
     * 
     * @param aPropertyName property names used for singleton lists; full lists
     *    are specified by property <code>aPropertyName</code><i>s</i>. If both 
     * properties are specified, the full list property is used.
     * 
     * @return list of files
     */
    public List<XFile> getMFiles(String aPropertyName)  {
        List<XFile> files = null;
        if ( mModuleArgs.containsKey(aPropertyName+'s') ) {
            files = mModuleArgs.getFiles(aPropertyName+'s', mDataPath);
        }
        else if ( mModuleArgs.containsKey(aPropertyName) ) {
            XFile file = mModuleArgs.getFile(aPropertyName, mDataPath, null);
            files = new ArrayList<>();
            files.add(file);
        }
        else {
            ulog.handleError("Nor %s:File,%ss:List<File> is specified.\n", aPropertyName, aPropertyName);
        }

        for (XFile file : files) {
            ulog.assertTrue(file.file().canRead(), "Cannot find or read the file %s, defined by %s\n", file.file(), aPropertyName);
        }
        
        return files;
    }
    
//</editor-fold>    
//</editor-fold>
}
