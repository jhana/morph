package morph.ma.module;

import java.io.*;
import java.util.regex.Pattern;
import lombok.Cleanup;
import morph.io.*;
import util.io.*;
import util.col.MultiMap;

/**
 *
 * @author  Jiri
 */
public class LexModuleCompiler extends ModuleCompiler<LexModule> {
    
    @Override
    protected void compileI() throws IOException {
        mModule.mSuperlativeOk = args.getString("superlativeOk");
        if (!initParadigms()) return;

        mModule.mLexicon = new MultiMap<String,LexEntry>();
        readInLexicon(getMFile("file"));
    }

    private boolean initParadigms() throws java.io.IOException {
        try {
            mModule.mParadigms = new Paradigms();
            mModule.mParadigms.init(mModuleId, args);
            if (mModule.mParadigms.compile(ulog) > 0) return false;
            mModule.mParadigms.configure();
            mModule.mMaxEndingLen  = mModule.mParadigms.getMaxEndingLen();
        }
        catch(Throwable e) {
            ulog.handleError(e, "Cannot load paradigms submodule");
        }
        return true;
    }
    
    
    /**
     * Reads in the lexicon from the specified file 
     * Required format: lemma stem paradigmId
     */
    private void readInLexicon(XFile aFileName) throws java.io.IOException {
        @Cleanup ReaderParser r = new ReaderParser(aFileName, inFilter, "lexicon"); 

        for(;;) {
            if (r.nextLine() == null) break;
            
            try {
                readLexEntry(r);
            }
            catch (java.util.NoSuchElementException e) {
                ulog.handleError(e);
            }
        }

        mModule.mLexicon = new MultiMap<String,LexEntry>(mModule.mLexicon);    // rehashing
    }

    // todo share with paradigms compiler
    private final static Pattern cStemIdPattern = Pattern.compile("\\@\\d+");

    private void readLexEntry(ReaderParser aR)  {
        LexEntry le = new LexEntry();

        le.lemma      = aR.getStringF("lemma");

        le.stem  = aR.getStringF("stem");
        mModule.mLexicon.add(le.stem, le);

        while (aR.nextTokenMatches(cStemIdPattern)) {
            int stemId = Integer.valueOf(aR.getString("stemId").substring(1)) - 1;
            String stem = aR.getStringF("stem" + stemId);
            // todo check it has not been specified yet
            le.addStem(stemId, stem);
            mModule.mLexicon.add(stem, le);
        }


        String paradigmId = aR.getString("paradigmId");
        le.paradigm = mModule.mParadigms.getParadigm(paradigmId);
        // @todo/TODO handle the errors 
        ulog.assertTrue(le.paradigm != null, aR, "the paradigm id %s is not defined", paradigmId);
        // todo assertTrue(le.paradigm.nrOfStems() == le.stems.size(), aR, "Stems problems @todo");

        // --- if additional stems not specified, default them to the stem 1 ---
    }


}
