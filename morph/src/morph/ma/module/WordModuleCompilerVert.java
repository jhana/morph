package morph.ma.module;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import util.io.*;
import util.col.MultiMap;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.err.UException;

/**
 *
 * @author  Jiri
 */
class WordModuleCompilerVert extends ModuleCompiler<WordModule> {


// =============================================================================
// Implementation <editor-fold desc="Implementation">
// =============================================================================

    private String mNegPref;
    private String mNegationOk;
    private boolean mAutoNegation;
    private int wordLimit;

    @Override
    protected void compileI() throws IOException {
        mModule.form2entry = new MultiMap<String, WordEntry>();

        mNegationOk = mModuleArgs.getString("negationOk", "");
        if (mNegationOk.equals("")) {       // get default key's value
            mNegationOk = args.getString("negationOk", "");
        }
        mAutoNegation = mNegationOk.length() > 0;
        mNegPref = args.getString("negationPref", "xxxx");

        wordLimit = mModuleArgs.getInt("wordLimit", Integer.MAX_VALUE);

        readInWords(getMFiles("file"));
    }

    private void readInWords(List<XFile> aFileNames) throws java.io.IOException {
        final LineReader r = IO.openLineReader(aFileNames);
//        final ReaderParser r = new LineNumberInputStream(null)Reader(aFileNames, inFilter, "words");
        r.configureSplitting(Pattern.compile("\\|"), 4, "Incorrect format - requires: lemma|tag|..|forms");

        for(int wordCounter=0; wordCounter < wordLimit;wordCounter++) {
            final String[] tokens = r.readSplitNonEmptyLine();
            if (tokens == null) break;
            try {
                readInEntry(tokens);
            }
            catch (java.util.NoSuchElementException e) {
                r.message("Error", e.getMessage());
            }
            catch (UException e) {
                r.message("Error", e.getMessage());
            }
        }

        r.close();
    }

    private final static Pattern cCommaPattern = Pattern.compile("\\,");

    private void readInEntry(final String[] aTokens) throws IOException{
        String lemma = aTokens[0];
        Tag tag   = Tagset.getDef().fromStringFill(aTokens[1]);
        String forms = aTokens[3];

        // todo handle negation
        for(String form : cCommaPattern.split(forms)) {
            mModule.form2entry.add( form, new WordEntry(form, lemma, tag ) );
        }
    }


}
