package morph.ma.module;

import morph.common.UserLog;
import java.io.IOException;
import morph.ma.module.prdgm.ParadigmsCompiler;
import java.util.*;
import lombok.Getter;
import lombok.Setter;
import morph.ma.*;
import morph.ma.lts.SingleLts;
import morph.ma.module.prdgm.DeclParadigm;
import morph.ma.module.prdgm.Ending;
import util.col.MultiMap;
import util.col.pred.Predicate;

/**
 * @author  Jirka
 */
public class Paradigms extends Module {
    /*@Getter*/ @Setter private MultiMap<String,Ending> endings;
    @Getter @Setter private Map<String,DeclParadigm> paradigms;
    @Getter @Setter private MultiMap<String,String> classes;

// -----------------------------------------------------------------------------
// Setup
// -----------------------------------------------------------------------------


    @Override
    public int compile(UserLog aUlog) throws IOException  {
        return compile(aUlog, null, null);
    }

    public int compile(UserLog aUlog, final Predicate<DeclParadigm> aPrdgmFilter, final Predicate<Ending> aEndingFilter) throws java.io.IOException {
        final ParadigmsCompiler compiler = new ParadigmsCompiler();
        compiler.init(this, moduleId, aUlog, args);
        if (aPrdgmFilter  != null) compiler.setPrdgmFilter(aPrdgmFilter);
//      if (aEndingFilter != null) compiler.setEndingFilter(aEndingFilter);

        if ( compiler.compile() == 0) {
            endings   = compiler.getEndings();
            paradigms = compiler.getParadigms();
            classes   = compiler.getStemTails();
        }

        return aUlog.getErrors();
    }

    @Override public int configure()  {
        //createSuperParadigms();

        return 0;
    }

    public int getMaxEndingLen() {
        int maxLen = 0;
        for (String e : getEndings().keySet()) {
            if (e.length() > maxLen) maxLen = e.length();
        }

        return maxLen;
    }

// -----------------------------------------------------------------------------
// Analysis
// -----------------------------------------------------------------------------

    @Override
    public boolean analyzeImp(String aCurForm, String[] aForms, List<BareForm> bareForms, Context aCtx, SingleLts aLTS) {
        throw new UnsupportedOperationException();
    }

    public MultiMap<String, Ending> getEndings() {
        return endings;
    }

    public Set<Ending> getEndings(String aEndingStr) {
        return getEndings().getS(aEndingStr);
    }


    public DeclParadigm getParadigm(String aPrdgmId) {
        return getParadigms().get(aPrdgmId);
    }
}
