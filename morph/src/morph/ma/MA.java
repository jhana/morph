/*
 @remove PDTFilter
 @todo file specifying excluded paradigms
 @todo configurable transliteration support
 @todo utilities: checkTags  file/endings/words/tagTransl
 @todo better info about tokens (surrounding punctuation)
*/

package morph.ma;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.regex.Pattern;
import lombok.Cleanup;
import util.col.MultiMap;
import util.Util;
import util.err.Log;
import util.io.*;
import morph.ts.Tag;
import morph.io.*;
import morph.util.MUtilityBase;
import morph.ma.lts.*;


public class MA extends MUtilityBase {
    /**
     * Morphological analyzer
     */
    protected Morph mMorph;

    /**
     * Printer where the result will be directed (@todo see argument -o in main fnc)
     * @see #main(String[])
     */
    protected CorpusWriter mOut;

    protected Filter mOutFilter;

    /**
     * Regex pattern for filtering input.
     * If the pattern is not null, only words satisfying it are analyzed.
     */
    protected Pattern mNarrow;

    /** Statistics */
    protected int mWords = 0;

    // --- Tag translation ---
    boolean mTagTransl;

    PrintWriter mTagTranslMemW = null;

    MultiMap<Tag,Tag> mTagTranslMap;

    /**
     *
     * See helpString()
     * @param args
     */
    public static void main(String[] args) throws IOException {
       new MA().go(args);
    }

    WordReader in;

    /**
     *
     * @param aArgs
     * @throws java.io.IOException
     */
    public void go(String[] aArgs) throws java.io.IOException {
        XFile outFile = null;

        //todo tmp
//        argumentBasics(aArgs,2);
//        profileStart("Loading Morphology ... ");
//        if (!setupMorph()) System.exit(-1);
//        //Lt.setInfoIrelevant(false);
//        profileEnd("Done in %2.2f s.\n");

        if (Arrays.asList(aArgs).contains("-direct")) {
            argumentBasics(aArgs,0);
            in = new LineWordReader(new InputStreamReader(System.in, Encoding.cUtf8.getId()), null);
            mOut = new PdtWriter(new PrintWriter(new OutputStreamWriter(System.out,Encoding.cUtf8.getId()), true));
            mOut.setOptions(getArgs().getSubtree("output"));
            mOut.optionsUpdated();
            getArgs().set("considerCtx", false);
        }
        else {
            argumentBasics(aArgs,2);
            // --- set up input/output ---
            XFile inFile = getArgs().getDirectFile(0, cCsts);
                 outFile = getArgs().getDirectFile(1, cCsts);

            in = Corpora.openWordReader(inFile);
            mOut = Corpora.openWriter(outFile, getArgs().getSubtree("output"));
        }
        Log.setILevel(Level.ALL);  // @todo something better


        mNarrow    = getArgs().getPatternNE("narrow");
        mOutFilter = mOut.getFilter();

        // --- set up tag translation ---
        mTagTransl = getArgs().getBool("output.tagTransl.enabled");
        if (mTagTransl) {
            File dataDir = getArgs().getDir("dataPath", null);
            XFile tagTranslFile = getArgs().getFile("output.tagTransl.tagTranslFile", dataDir, null);
            tagTranslInit(tagTranslFile);

            mTagTranslMemW = (outFile == null) ? null : IO.openPrintWriter(outFile.addExtension("ttmem"));
        }

        // --- set up the morph object ---
        profileStart("Loading Morphology ... ");
        if (!setupMorph()) System.exit(-1);
        //Lt.setInfoIrelevant(false);
        profileEnd("Done in %2.2f s.\n");

        // --- Do the main task ---
        profileStart("Analyzing ... ");
        analyze();
        profileEnd("Done  in %2.2f s. Speed: %2.2f tokens/s.\n", mWords);

        IO.close(mOut, mTagTranslMemW);
    }


    private boolean setupMorph() throws IOException {
        //Log.setLevel(0);
        Lt.setInfoIrelevant(getArgs().getBool("infoIrelevant"));  // tODO needed for some filtering (leo) but do not want to keep duplicates in results
        System.err.println("infoIrelevant=" + Lt.isInfoIrelevant());
        mMorph = MorphCompiler.factory().compile(getArgs());
        return (mMorph != null);
    }

    // todo, input should be able to contain tags that info should be passed to output
    private void analyzeFileHack(XFile aInFile) throws java.io.IOException {
        Log.info("Analyzing file %s", aInFile);
        @Cleanup CorpusLineReader r = Corpora.openF(aInFile);   // does not read plain text
        assert r != null;
        r.setOnlyTokenLines(false);

        mOut.writeHeader();
        analyzeWReader((PdtLineReader)r);
        Log.info("File analyzed");

        mOut.writeFooter();
        mOut.close();
    }

    // todo remove, this is a quick fix to preserve corpus format
    private void analyzeWReader(PdtLineReader aR) throws java.io.IOException {
        System.out.println("ABCxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        for(int j=0;;j++) {
            if (!aR.readLine()) break;

            if (aR.lineType().fd()) {
                Context ctx = Context.emptyContext(aR.form());
                Lts lts = mMorph.analyze(ctx.getCur(), ctx);
                mWords++;
                Log.dot(mWords);
                if (mTagTransl) lts = translateTags(ctx.getCur(), lts);

                mOut.writeLine(ctx.getCur(), lts);
            }
            else {
                ((PdtWriter)mOut).writeLine(aR.line());
            }

            if ((j & 16383) == 0) System.out.print('.');  // 16383 = 2^26-1
//          if ((j & (2<<26-1)) == 0) System.out.print('.');
        }
    }

    // todo, input should be able to contain tags that info should be passed to output -- could be done via handler
    private void analyze() throws java.io.IOException {
        mOut.writeHeader();

        int ctxSize = (getArgs().getBool("considerCtx")) ? 2 : 0;
        Context ctx = new Context(in, ctxSize, ctxSize);
        ctx.prepare();

        for( ;ctx.hasNext(); ) {
            Lts lts;
            String curForm = ctx.getCur();

            if (mNarrow == null || mNarrow.matcher(curForm).matches() ) {
                lts = mMorph.analyze(ctx.getCur(), ctx);
                mWords++;
                Log.dot(mWords);
                if (mTagTransl) lts = translateTags(ctx.getCur(), lts);

                mOut.writeLine(ctx.getCur(), lts);
            }
            else {
                lts = null;
                mOut.writeLine(ctx.getCur(), lts);
            }

            ctx.update(lts);
        }
        Log.info("\n"); // nl after dots

        mOut.writeFooter();
        mOut.close();
        in.close();
    }

    private Lts translateTags(String aForm, Lts aLTs) {
        final MultiMap<Tag,Tag> new2oldMemory = new MultiMap<Tag,Tag>();
        final Lts newLts = aLTs.translateTags(mTagTranslMap, new2oldMemory);

        // --- record translation memory map into a file
        if (mTagTranslMemW != null) {
            mTagTranslMemW.print("<f>" + mOutFilter.filter(aForm));   // for debug only
            for (Map.Entry<Tag,Set<Tag>> e : new2oldMemory.entrySet()) {
                for (Tag old : e.getValue()) {
                    mTagTranslMemW.print("<new>" + e.getKey() + "<orig>" + old);
                }
            }
            mTagTranslMemW.println();
        }

        return newLts;
    }


    void tagTranslInit(XFile aTagTranslFile) throws java.io.IOException {
        mTagTranslMap = new MultiMap<Tag,Tag>();
        LineNumberReader r = IO.openLineReader(aTagTranslFile);
        String fileId = "tagTransl";

        for(;;) {
            String line = r.readLine();
            if (line == null) break;
            line = Util.preprocessLine(line);
            PushBackTokenizer tok = new PushBackTokenizer(line, " \t\n\r\f", false, r, "tagTransl");

            if (!tok.hasMoreTokens()) continue;

            try {
                Tag rusTag = tok.getTag("Orig tag");
                for(;tok.hasMoreTokens();) {
                    Tag czTag = tok.getTag("New tag");
                    mTagTranslMap.add(rusTag, czTag);

                    if (czTag.getNegation() == 'A')
                        mTagTranslMap.add(rusTag.setNegation('N'), czTag.setNegation('N'));
                }
            }
            catch (java.util.NoSuchElementException e) {}

        }

        r.close();
    }
}

