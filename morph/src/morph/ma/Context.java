package morph.ma;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import morph.io.WordReader;
import morph.io.WordReaderList;
import morph.ma.lts.Lts;
import util.col.Cols;
import util.str.Cap;
import util.str.Caps;

/**
 * Contextualized word reader., move reader context out the same way as empty ctx
 *
 * @todo considere detaching from the reader (maybe just iterator?)
 * @author Administrator
 */
public class Context {
    private final int mPrevCtx;
    private List<String> mPrevForms;
    private List<Lts> mPrevAnals;
    private List<Cap> mPrevCaps;

    private String mCurForm;
    private Cap mCurCap;

    private final int mNextCtx;
    private List<String> mNextForms;
    private List<Cap> mNextCaps;

    private WordReader mR;

    /**
     * Creates a simulated empty context of a word.
     *
     * Calling {@link #update(Lts)} on such context throws an IOException.
     * @param aForm word to put into the context
     * @return empty, nonupdateable context
     * @todo create specific reader for one word, so it is more
     */
    public static Context emptyContext(final String aForm) {
        return new EmptyContext(aForm);
    }


    /** Used for non-updatable empt context */
    protected Context(String aForm) {
        mPrevCtx = 0;
        mNextCtx = 0;
        mCurForm = aForm;
        mCurCap = Caps.capitalization(aForm);
    }

    public Context(WordReader aR, int aPrevCtx, int aNextCtx) {
        mR = aR;
        mPrevCtx = aPrevCtx;
        mNextCtx = aNextCtx;

        mPrevForms = new ArrayList<String>(aPrevCtx);
        mPrevCaps = new ArrayList<Cap>(aPrevCtx);
        mPrevAnals = new ArrayList<Lts>(aPrevCtx);

        mNextForms = new ArrayList<String>(aNextCtx);
        mNextCaps = new ArrayList<Cap>(aNextCtx);
    }

    public String getCur() {
        return mCurForm;
    }

    public Cap getCap() {
        return mCurCap;
    }

    public String curForm() {
        return mCurForm;
    }

    public Cap curCap() {
        return mCurCap;
    }

    public boolean hasPrevWord(int aIdx) {
        return getItem(mPrevForms,aIdx) != null;
    }

    public String getPrevWord(int aIdx) {
        return getItem(mPrevForms,aIdx);
    }

    public boolean prevWordEq(int aIdx, String aStr) {
        String str = getItem(mPrevForms,aIdx);
        return str != null && str.equals(aStr);
    }

    public Cap getPrevCap(int aIdx) {
        return getItem(mPrevCaps,aIdx);
    }

    public Lts getPrevAnals(int aIdx) {
        return getItem(mPrevAnals,aIdx);
    }

    public boolean hasNextWord(int aIdx) {
        return getItem(mNextForms,aIdx) != null;
    }

    public String getNextWord(int aIdx) {
        return getItem(mNextForms,aIdx);
    }

    public boolean nextWordEq(int aIdx, String aStr) {
        String str = getItem(mNextForms,aIdx);
        return str != null && str.equals(aStr);
    }


    public Cap getNextCap(int aIdx) {
        return getItem(mNextCaps,aIdx);
    }


// ------------------------------------------------------------------------------
// Special functions
// ------------------------------------------------------------------------------
    /**
     * Check: Previous form is . / ! / ?, i.e. probably end of a sentence.
     * ??? '...'
     */
    public boolean prevIsSentencePunct() {
        String prevForm = getPrevWord(0);
        return
            prevForm != null && prevForm.length() == 1 &&  // quick check
            (prevForm.equals(".") || prevForm.equals("?") || prevForm.equals("!"));
    }

    /**
     * Simulatets an infinite array, with all items above the size of the 
     * real array filled with nulls.
     * @param <T>
     * @param aList
     * @param aIdx
     * @return
     */
    private <T> T getItem(List<T> aList, int aIdx) {
        return (aIdx < aList.size()) ? aList.get(aIdx) : null;
    }

//    public boolean isNotEmpty() {return !mEmpty;}
//
//    /**
//     * The previous form, i.e. the form preceding the current form.
//     */
//    public String previousForm()  {return mPrevForm;}
//
//    /**
//     * Capitalization of the previous form.
//     */
//    public Cap   previousCap() {return mPrevCap;}
//
//    /**
//     * Morphological analyzis of the previous form.
//     */
//    public Lts previousAnalysis() {return mPrevAnal;}
//
//    /**
//     * The current form.
//     */
//    public String      curForm()  {return mCurForm;}
//
//    /**
//     * Capitalization of the current form.
//     */
//    public Cap curCap()   {return mCurCap;}
//
//    /**
//     * The next form, i.e. the form following the current form.
//     */
//    public String      nextForm() {return mNextForm;}
//
//    /**
//     * Capitalization of the next form.
//     */
//    public Cap nextCap()  {return mNextCap;}
//
//// ------------------------------------------------------------------------------
//// Special functions
//// ------------------------------------------------------------------------------
//    /**
//     * Check: Previous form is . / ! / ?, i.e. probably end of a sentence.
//     * ??? '...'
//     */
//    public boolean prevIsSentencePunct() {
//        return
//            (mPrevForm.length() < 2) &&  // quick check
//            (mPrevForm.length() == 0 || mPrevForm.equals(".") || mPrevForm.equals("?") || mPrevForm.equals("!"));
//    }
//
//    public String toString() {
//        return mPrevForm + " : " + mCurForm + " : " + mNextForm;
//    }



    public void prepare() throws IOException {
        for (int i = 0; i < mPrevCtx; i++) {
            mPrevForms.add(null);
            mPrevCaps.add(null);
            mPrevAnals.add(null);
        }

        for (int i = -1; i < mNextCtx; i++) {
            mR.nextWord();
            String form = mR.word();
            if (form == null) break;
            Cap cap = Caps.capitalization(form);

            if (i == -1) {
                mCurForm = form;
                mCurCap = cap;
            }
            else {
                mNextForms.add(form);
                mNextCaps.add(cap);
            }
        }
    }

    public boolean hasNext() {
        return mCurForm != null;
    }
//
//    public boolean next() {
//        // move one level
//
//        if (!mR.nextWord()) return;
//
//    }

    public void update(Lts aCurAnal) throws IOException {
        if (mPrevForms.size() > 0) {
            // shift one index
            Cols.shiftUp(mPrevForms);
            Cols.shiftUp(mPrevCaps);
            Cols.shiftUp(mPrevAnals);

            mPrevForms.set(0, mCurForm);
            mPrevCaps.set(0, mCurCap);
            mPrevAnals.set(0, aCurAnal);
        }


        mR.nextWord();
        String curForm = mR.word();
        Cap curCap = (curForm == null) ? null : Caps.capitalization(curForm);

        if (mNextForms.isEmpty()) {
            mCurForm = curForm;
            mCurCap = curCap;
        }
        else {
            // shift one index
            mCurForm = mNextForms.get(0);
            mCurCap = mNextCaps.get(0);

            Cols.shiftDown(mNextForms);
            Cols.shiftDown(mNextCaps);

            mNextForms.set(mNextForms.size()-1, curForm);
            mNextCaps.set(mNextForms.size()-1, curCap);
        }
    }

    public String toString() {
        return Cols.toString(mPrevForms, "[", "]", ",", "[]") + "  $  " + mCurForm + "  $  " + Cols.toString(mNextForms, "[", "]", ",", "[]");
    }


}

//class NormalContext extends Context {
//
//}


class EmptyContext extends Context {
    /** Used for non-updatable empt context */
    protected EmptyContext(String aForm) {
        super(aForm);
    }

    public boolean hasPrevWord(int aIdx) {
        return false;
    }

    public String getPrevWord(int aIdx) {
        return null;
    }

    public boolean prevWordEq(int aIdx, String aStr) {
        return false;
    }

    public Cap getPrevCap(int aIdx) {
        return null;
    }

    public Lts getPrevAnals(int aIdx) {
        return null;
    }

    public boolean hasNextWord(int aIdx) {
        return false;
    }

    public String getNextWord(int aIdx) {
        return null;
    }

    public boolean nextWordEq(int aIdx, String aStr) {
        return false;
    }


    public Cap getNextCap(int aIdx) {
        return null;
    }


    public boolean prevIsSentencePunct() {
        return false;
    }

    public void prepare() throws IOException {
    }

    public boolean hasNext() {
        return false;
    }

    public void update(Lts aCurAnal) throws IOException {
        throw new IOException("Empty context");
    }




}


