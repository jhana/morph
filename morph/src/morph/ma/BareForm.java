package morph.ma;

import java.util.BitSet;

/**
 *
 * @author  Jirka Hana
 */
/**
 * Form without prefixes (prefixes are represented as booleans)
 *
 * @todo this is way too much language specific.
 */
public class BareForm {
    public String form;
    BitSet mBitmap;            // @todo replace by non-growing BitSet

    private BareForm(String aForm, BitSet aBitmap) {
        form = aForm;
        mBitmap = aBitmap;
    }

    public BareForm(String aForm) {
        this(aForm, new BitSet());
    }

    public BareForm(String aForm, BareForm aBaseBForm, int aPrefIdx) {
        this(aForm, copyBits(aBaseBForm.mBitmap, aPrefIdx));
    }

    private static BitSet copyBits(BitSet aBitmap, int aSet) {
        BitSet bitmap = new BitSet();
        bitmap.or(aBitmap);
        bitmap.set(aSet);
        return bitmap;
    }


    public boolean ne()  {return mBitmap.get(0);}
    public boolean nej() {return mBitmap.get(1);}

    public String toString() {
        return (ne() ? "ne+" : "") + (nej() ? "nej+" : "") + form;
    }
}


