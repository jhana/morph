package morph.ma;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import morph.ma.module.Module;
import morph.util.MUtilityBase;
import morph.ts.Tag;
import util.err.Log;
import util.io.IO;
import util.io.XFile;
import util.str.Strings;


public class CollectTagset extends MUtilityBase {
    /**
     * Morphological analyzer
     */
    protected Morph mMorph;

    public static void main(String[] args) throws IOException {
        new CollectTagset().go(args);
    }

    /**
     *
     * @param aArgs
     * @throws java.io.IOException
     */
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,1);
        Log.setILevel(Level.ALL);  // @todo something better

        // --- set up the morph object ---
        if (!setupMorph()) return;
        List<Tag> tags = getAllTags();
        Collections.sort(tags, Strings.comparator());
        XFile outFile = getArgs().getDirectFile(0);
        IO.writeLines(outFile, tags);
    }


    private boolean setupMorph() throws IOException {
        mMorph = MorphCompiler.factory().compile(getArgs());
        return (mMorph != null);
    }

    private List<Tag> getAllTags() {
        Set<Tag> tags = new TreeSet<Tag>();
        for (Module module : mMorph.modulesList) {
            tags.addAll(module.getAllTags());
        }
        return new ArrayList<Tag>(tags);
    }

}

