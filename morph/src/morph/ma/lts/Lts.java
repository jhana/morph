package morph.ma.lts;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import util.col.Cols;
import util.col.MultiMap;
import morph.ts.Tag;
import util.col.NotSingleton;

/**
 * A collection of lemma+tag items.
 *
 * @todo make an efficient single lts for a single lt?
 *
 * @author Jirka
 */
@lombok.EqualsAndHashCode
public class Lts implements Iterable<Lt> {
    protected final Set<Lt> lts;

    public Lts() {
        lts = new TreeSet<Lt>();
    }

    public Lts(Collection<Lt> aLts) {
        lts = new TreeSet<Lt>(aLts);
    }

    public Lts(Lt ... aLts) {
        this(Arrays.asList(aLts));
    }

    public Lts add(String aLemma, Tag aTag, String aSrc, Info aInfo) {
        add(new Lt(aLemma, aTag, aSrc, aInfo));
        return this;
    }

    public Lts add(Lt aLt) {
        lts.add(aLt);
        return this;
    }

    public Lts add(Lts aLts) {
        lts.addAll(aLts.lts);
        return this;
    }

    public Lts addAll(Collection<Lt> aLts) {
        lts.addAll(aLts);
        return this;
    }

    //public void setLemma()


    @Override
    public String toString() {
        return Cols.toString(lts);
    }



// -----------------------------------------------------------------------------
// Collection views <editor-fold desc="Collection views">
// -----------------------------------------------------------------------------

    public Set<Lt> col() {
        return lts;
    }

    @Override
    public Iterator<Lt> iterator() {
        return lts.iterator();
    }

    public static Predicate<Lt> hasTag(final Tag aTag) {
        return new Predicate<Lt>() {
            public boolean apply(Lt aLt) {
                return aLt.tag().equals(aTag);
            }
        };
    }

    public static Predicate<Lt> hasLemma(final String aLemma) {
        return new Predicate<Lt>() {
            public boolean apply(Lt aLt) {
                return aLt.lemma().equals(aLemma);
            }
        };
    }
    
    
    public List<Lt> filter(Predicate<Lt> aPredicate) {
        return new ArrayList<Lt>( Collections2.filter(lts, aPredicate) );
    }

    public Lt first(Predicate<Lt> aPredicate) {
        for (Lt lt : lts) {
            if (aPredicate.apply(lt)) return lt;
        }
        return null;
    }


    /**
     * Filter lts by lemma
     * @todo return Lts?
     */
    public List<Lt> col(String aLemma) {
        return filter(hasLemma(aLemma));
//        final List<Lt> tmp = new ArrayList<Lt>(this.lts.size());
//        for(Lt lt : this.lts) {
//            if (lt.lemma().equals(aLemma)) tmp.add(lt);
//        }
//        return tmp;
    }

    /**
     * The items in list are sorted by their lemmas.
     * Sorted? Does not look like.
     * 
     */
    public final List<Lt> toList() {
        return new ArrayList<Lt>(lts);
    }

    // todo lightweight view of lemmas

    public SortedSet<String> lemmas() {
        final SortedSet<String> lemmas = new TreeSet<String>();
        for (Lt lt : lts) {
            lemmas.add(lt.lemma());
        }
        return lemmas;
    }

    // todo lightweight view of tags
    public SortedSet<Tag> tags() {
        final SortedSet<Tag> tags = new TreeSet<Tag>();
        for(Lt lt : lts) {
            tags.add(lt.tag());
        }
        return tags;
    }


    public int size() {
        return lts.size();
    }

    public boolean isEmpty() {
        return lts.isEmpty();
    }

//</editor-fold>


    // @todo replace with filter(predicate)
    public Set<Lt> lts() {
        return lts;
    }

    /**
     * For singleton lts, returns the one and only lt. Returns null otherwise.
     */
    public Lt lt() {
        try {
            return ltE();
        }
        catch(NotSingleton e) {
            return null;
        }

    }


    /**
     * For singleton lts, returns the one and only lt.
     *
     * @throws NotSingleton if there are non or than one lts.
     */
    public Lt ltE() {
        NotSingleton.check(lts);

        return lts.iterator().next();
    }

// ------------------------------------------------------------------------------
// Modifications <editor-fold desc="Modifications">
// ------------------------------------------------------------------------------

    public void setLemma(String aLemma) {
        final List<Lt> newLts = new ArrayList<Lt>();
        for(Lt lt : lts) {
            newLts.add(lt.setLemma(aLemma));
        }
        lts.clear();
        lts.addAll(newLts);
    }
    
    /**
     * Removes all entries with the specified tag.
     */
    public void remove(Tag aTag) {
        for (Iterator<Lt> it = lts.iterator(); it.hasNext(); ) {
            Lt lt = it.next();
            if (lt.tag().equals(aTag)) it.remove();
        }
    }

    /**
     * Create a new lts with translated tags. Leaves this lts unaffected.
     *
     * @param aTagTranslMap map specifying the translation
     * @param aTranslMemory map for an easy translation back, the provided map
     * is filled with translations newTag -> origTag(s)
     * @returns
     */
    public Lts translateTags(final MultiMap<Tag,Tag> aTagTranslMap, final MultiMap<Tag,Tag> aTranslMemory) {
        final Set<Lt> newLts = new TreeSet<Lt>();

        for (Lt lt : lts) {
            final Tag origTag = lt.tag();

            final Set<Tag> newTags = aTagTranslMap.get(origTag);

            if (newTags == null) {
                newLts.add(new Lt(lt));
            }
            else {
                for (Tag t : newTags) {
                    Lt newLt = new Lt(lt).setTag(t);
                    newLts.add(newLt);

                    aTranslMemory.add(t, origTag);
                }
            }
        }

        return new Lts(newLts);
    }

//</editor-fold>

}
