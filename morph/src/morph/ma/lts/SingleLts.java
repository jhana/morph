package morph.ma.lts;

import morph.ts.Tag;

/**
 * A collection of lemma+tag items produced (mostly) by a single module (source).
 *
 * Note that in addition to the add method that adds a lemma+tag item with
 * the default source, there is also the inherited add method specifying the source 
 * explicitely.
 *
 * @author Jirka
 */
public class SingleLts extends Lts {
    String mSrc;
    
    public SingleLts(String aSrc) {
        mSrc = aSrc;
    }
    
    public void add(String aLemma, Tag aTag, Info aInfo) {
        lts.add(new Lt(aLemma, aTag, mSrc, aInfo));
    }

    
    
            
}
