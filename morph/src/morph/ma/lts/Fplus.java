package morph.ma.lts;

import java.util.Comparator;
import java.util.Set;
import lombok.Getter;
import morph.ts.Tag;

/**
 * Form plus lemma/tag(s) and other information
 * @author jirka
 */
@lombok.EqualsAndHashCode
@lombok.ToString(callSuper=true, includeFieldNames=true)
public class Fplus {
    
    public @Getter final String form;
    public @Getter final Lts lts;

    public Fplus(final String form) {
        this(form, new Lts());
    }

    public Fplus(final String form, final Lts lts) {
        this.form = form;
        this.lts = lts;
    }

    // convenience methods
    public String lemma()       {return getLts().lt().lemma();}
    public Tag tag()            {return getLts().lt().tag();}
    public String tagStr()      {return tag().toString();}    
    public Set<String> lemmas() {return getLts().lemmas();}
    public Set<Tag> tags() {return getLts().tags();}
    
    
    public static Comparator<Fplus> createFormComparator() {
        return new Comparator<Fplus>() {
            @Override
            public int compare(Fplus o1, Fplus o2) {
                return o1.getForm().compareTo(o2.getForm());
            }
        };
    }
    
}
