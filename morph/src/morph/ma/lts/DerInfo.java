package morph.ma.lts;

import morph.ma.module.prdgm.Derivation;

/**
 *
 * @author Jirka
 */
public class DerInfo extends Info {
    private final String mRoot;
    private final Derivation mDerivation;  // contains info about super-paradigm
    private final boolean mRootEpen;      // epenthesis in the root
    private final DecInfo mDeclInfo;


    /** Creates a new instance of DerivInfo */
    public DerInfo(String aRoot, Derivation aDerivation, boolean aRootEpen, DecInfo aDeclInfo) {
        mRoot       = aRoot;
        mDerivation = aDerivation;
        mRootEpen   = aRootEpen;
        mDeclInfo   = aDeclInfo;
    }

    public @Override boolean hasEpen() {
        return mRootEpen;
    }

    public @Override String toString() {
        return mDerivation.id;
    }

    public Derivation getDerivation() {
        return mDerivation;
    }

    /**
     * Super paradigm.
     */
    public @Override String getPrdgmId() {
        return mDerivation.mSuperParadigm;
    }


    public DecInfo getDeclInfo() {
        return mDeclInfo;
    }

    /**
     * Declension paradigm
     */
    public @Override String getDeclPrdgmId() {
        return mDeclInfo.getPrdgmId();
    }

    public String getDeclLemma() {
        return mDeclInfo.getLemma();
    }

    public @Override int getEndingLen() {
        throw new UnsupportedOperationException();
        // @todo suffix + ending
    }



}
