package morph.ma.lts;

import morph.ma.module.prdgm.Ending;
import morph.ma.module.prdgm.DeclParadigm;

/**
 *
 * @author Jirka
 */
public class DecInfo extends Info {

    private String mActualStem;
    private String mLemmatizingStem;
    private Ending mEnding;   // contains info about paradigm
    private boolean mEpen;    // was there an epenthesis in the stem

    /** Creates a new instance of DeclInfo */
    public DecInfo(String aLemmatizingStem, String aActualStem, Ending aEnding, boolean aEpen) {
        mLemmatizingStem = aLemmatizingStem;
        mActualStem = aActualStem;
        mEnding = aEnding;
        mEpen = aEpen;
    }

    public @Override String toString() {
        return String.format("%s%s+%s (%d, %s,%s)", mActualStem, (mEpen ? "€" : ""), mEnding.getEnding(), getEndingLen(), mLemmatizingStem, mEnding.getPrdgm().id);
    }

    public Ending getEnding()     {return mEnding;}

    public String getActualStem() {
        return mActualStem;
    }

    /**
     * The lemmatizing stem (i.e. stem nr 0).
     */
    public String getLemmatizingStem()       {return mLemmatizingStem;}

    public DeclParadigm getPrdgm()     {return mEnding.getPrdgm();}

    public @Override String getPrdgmId()     {return mEnding.getPrdgm().id;}
    public @Override String getDeclPrdgmId() {return mEnding.getPrdgm().id;}

    public @Override boolean hasEpen()       {
        return mEpen;
    }

    public @Override int getEndingLen()      {
        return mEnding.getEnding().length();
    }

    public String getLemma() {
        return mLemmatizingStem + mEnding.getPrdgm().lemmatizingEnding;
    }

}
