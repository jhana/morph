package morph.ma.lts;

/**
 * @todo subclass and provide different kind of info (more and less verbose, pure decl x deriv )
 * @author Jirka
 */
public abstract class Info implements Comparable<Info> {
    public DerInfo asDerInfo() {
        return (DerInfo) this;
    }

    public DecInfo asDecInfo() {
        return (DecInfo) this;
    }
    
    
    public abstract String getPrdgmId();
    public abstract String getDeclPrdgmId();

    public abstract boolean hasEpen();
    public abstract int getEndingLen();

    
    /**
     * Compares only paradigm id.
     */
    @Override
    public int compareTo(Info aInfo) {
        return getPrdgmId().compareTo(aInfo.getPrdgmId());
    }    

    /**
     * Compares only paradigm id.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Info)) return false;
        Info info = (Info)obj;

        return getPrdgmId().equals(info.getPrdgmId());
    }

    /**
     * Depends on paradigm id.
     */
    @Override
    public int hashCode() {
        return getPrdgmId().hashCode(); 
    }
    
}
