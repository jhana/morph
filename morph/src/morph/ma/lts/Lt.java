package morph.ma.lts;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.Util;
import util.col.Mapper;

/**
 *
 * @author Jirka
 */
public final class Lt implements Comparable<Lt> {
    // todo take this out into the provider so that some lts in the jvm can consider it and some not
    private static boolean mInfoIrelevant = true;   // is info used in comparisons?

    // empty info for modules not using it (prdgId must be nonempty (Filter needs some prdgmid)
    private final static Info cEmptyInfo = new DecInfo("", "",
            new morph.ma.module.prdgm.Ending(new morph.ma.module.prdgm.DeclParadigm("0"), 0, false, false, "", Tagset.getDef().cDummy), false);

    public static final Mapper<Lt, String> cLemmaMapper = new Mapper<Lt, String>() {
        @Override public String map(Lt aOrigItem) {
            return aOrigItem.mLemma;
        }
    };

    public static final Mapper<Lt, Tag> cTagMapper = new Mapper<Lt, Tag>() {
        @Override public Tag map(Lt aOrigItem) {
            return aOrigItem.mTag;
        }
    };


    /**
     * Is info used in comparisons (equals/hashcode/compareTo)?
     * If info is relevant it cannot be null (it can have a dummy info, but it must be present)
     */
    public static void setInfoIrelevant(boolean aA){
        mInfoIrelevant = aA;
    }

    public static boolean isInfoIrelevant(){
        return mInfoIrelevant;
    }

// =============================================================================
    private final String mLemma;
    private final Tag mTag;
    private final String mSrc;        // which module supplied this tag
    private final Info mInfo;         // info a module may supply (which prdgm was used, ...)

// =============================================================================
    public Lt(Lt aLt) {
        this(aLt.mLemma, aLt.mTag, aLt.mSrc, aLt.mInfo);
    }

    public Lt(String aLemma, Tag aTag) {
        this(aLemma, aTag, null, null);
    }

    public Lt(String aLemma, Tag aTag, String aSrc, Info aInfo) {
        mLemma = aLemma;
        mTag = aTag;
        mSrc = aSrc;
        mInfo = (!mInfoIrelevant && aInfo == null) ? cEmptyInfo : aInfo;
        // if we need infos and info is not supplied, use a dummy one (other possibility would be to complicate compareTo/... with tests for null)
    }

// =============================================================================
// Attributes

    public String lemma() {return mLemma;}
    public Tag tag()      {return mTag;}
    public Info info()    {return mInfo;}
    public String src()   {return mSrc;}

    /**
     * Returns a new Lt object with the specified tag (otherwise identical to this one).
     */
    public Lt setTag(Tag aTag) {
        return new Lt(mLemma, aTag, mSrc, mInfo);
    }

    /**
     * Returns a new Lt object with the specified lemma (otherwise identical to this one).
     */
    public Lt setLemma(String aLemma) {
        return new Lt(aLemma, mTag, mSrc, mInfo);
    }

// ------------------------------------------------------------------------------
// Canonic
// ------------------------------------------------------------------------------

    public String toString() {
        return mLemma + ":" + mTag.toString() + " (" + mSrc + "," + String.valueOf(mInfo) + ")";
        //return Strings.concat(mLemma, ":", mTag.toString(), " (", mSrc, ",", String.valueOf(mInfo), ")");
    }

    @Override
    public int compareTo(Lt o) {
        int tmp = mLemma.compareTo(o.mLemma);
        if (tmp != 0) return tmp;

        tmp = mTag.compareTo(o.mTag);
        if (tmp != 0) return tmp;

        //if (true) return 0;         // todo hack

        tmp = compare(mSrc, o.mSrc);
//        tmp = mSrc.compareTo(o.mSrc);       /// todo compare with nulls
        if (mInfoIrelevant || tmp != 0) return tmp;

        return compare(mInfo,o.mInfo);
    }

    // todo to util
    private static <T extends Comparable<T>> int compare(T a, T b) {
        if (a == b) return 0;

        if (a == null) return -1;
        if (b == null) return 1;

        return a.compareTo(b);

    }


    @Override
    public int hashCode() {
        return mLemma.hashCode() + mTag.hashCode();     /// @todo better
    }

    /**
     *
     * As with any element, parents are not checked
     */
    @Override
    public boolean equals(Object aE) {
        if (aE == this) return true;
        if (!(aE instanceof Lt)) return false;

        Lt otherLt = (Lt) aE;

        return eq(mLemma, otherLt.mLemma) && 
               eq(mTag,otherLt.mTag) && 
               eq(mSrc,otherLt.mSrc) && 
               ( mInfoIrelevant || eq(mInfo,otherLt.mInfo) );    // todo tmp
    }

    private boolean eq(Object a, Object b) {
        return a == b || (a != null && a.equals(b));
    }
    
    public boolean equalsOnItem(Lt aE) {
        return mLemma.equals(aE.mLemma) && mTag.equals(aE.mTag);
    }
}
