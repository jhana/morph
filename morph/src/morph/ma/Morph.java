package morph.ma;

import morph.ma.module.*;
import morph.ma.lts.*;
import java.util.*;
import lombok.Getter;
import util.str.Caps;

public class Morph implements java.io.Serializable {

    /**
     * Modules in the order as used for analysis.
     * @see #analyzeI(String, Context, LTStruct)
     */
    @Getter List<Module> modulesList;

    @Getter Map<String, Module> modules;

    Map<String, Integer> moduleNameToIdx;

    /**
     * Prefixes to strip to get bare forms
     * @see removePrefixes(String)
     */
    List<String> prefs;

    //private transient List<SingleLts> mAnalyses;



// -----------------------------------------------------------------------------
// Infrastructure
// -----------------------------------------------------------------------------

    /**
     * Returns a module with the specified name.
     *
     * @return the specified module if it exists; null otherwise
     */
    public Module getModule(String aModuleName) {
        return modules.get(aModuleName);
    }

// -----------------------------------------------------------------------------
// Analysis
// -----------------------------------------------------------------------------

    public Lts analyze(String aForm) {
        return analyze(aForm, Context.emptyContext(aForm));
    }
    /**
     * Analyzes a form in context.
     * Performs an analyzis by applying a sequence of analyzing modules.
     * @param aForm form to analyze
     * @param aCtx cannot be null, but can be empty (Context.emptyContext)
     * @return all possible analyses of the form
     */
    public Lts analyze(final String aForm, final Context aCtx) {
        //mAnalyses = new ArrayList<SingleLts>(mModulesList.size());

        // --- create various decapitalizations (AAA, Aaa, aaa) ---
        String[] decapForms;          // @todo eff just array with 3 items?
        String lcCase;

//        switch (aCtx.curCap()) {
//            case lower:
//                decapForms = null;
//                lcCase = aForm;
//                break;
//            case firstCap:
//            case mixed:
//                decapForms = new String[1];
//                decapForms[0] = lcCase = aForm.toLowerCase();
//                break;
//            case caps:
//                decapForms = new String[2];
//                decapForms[0] = Strings.toFirstCapital(aForm);
//                decapForms[1] = lcCase = aForm.toLowerCase();
//        }

        if (aCtx.curCap().lower()) {
            decapForms = null;
            lcCase = aForm;
        }
        else if (!aCtx.curCap().caps()) { // first cap or mixed case
            decapForms = new String[1];
            decapForms[0] = lcCase = aForm.toLowerCase();
        }
        else {      // all caps
            decapForms = new String[2];
            decapForms[0] = Caps.toFirstCapital(aForm);
            decapForms[1] = lcCase = aForm.toLowerCase();
        }

        final List<BareForm> bareForms = removePrefixes(lcCase);      // @todo ? a feeding module??

        // --- Ask modules to provide their analyses ---
        final Lts allLts = new Lts();
        for (Module mam : modulesList ) {
            final SingleLts lts = new SingleLts(mam.getSrc());
            mam.analyze(aForm, decapForms, bareForms, aCtx, lts);
            allLts.add(lts);
            if (allLts.size() > 0 && mam.isStopWhenNotEmpty() ) break;
        }

        return allLts;
    }


    /**
     * Strips prefix candidates (e.g. ne, nej) and returns all the possible
     * ways to do it.
     * Assumes none of the prefixes is empty (would never finish)
     */
    private List<BareForm> removePrefixes(String aForm) {
        List<BareForm> bareForms = new ArrayList<BareForm>(4);
        // --- Analysis as if without prefixes ---
        bareForms.add(new BareForm(aForm));

        // --- Check each prefix  ---
        // @todo ? compile all the possible concatenations? (0, ne, nej, nejne)
        for(int i = 0; i < prefs.size(); i++) {
            String pref = prefs.get(i);
            for(int bFormIdx = 0; bFormIdx < bareForms.size(); bFormIdx++) {
                BareForm bForm1 = bareForms.get(bFormIdx);
                if (bForm1.form.startsWith(pref)) {
                    String bForm2Str = bForm1.form.substring(pref.length());
                    bareForms.add(new BareForm(bForm2Str, bForm1, i));
                }
            }
        }
        return bareForms;
    }
}
