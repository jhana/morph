//package morph.corpus;
//
//import java.io.IOException;
//import java.io.LineNumberReader;
//import java.io.PrintWriter;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.TreeMap;
//import java.util.regex.Pattern;
//import java.util.regex.PatternSyntaxException;
//import javax.naming.Context;
//import javax.swing.text.html.HTML.Tag;
//import morph.io.CorpusLineReader;
//import morph.io.CorpusReader;
//import morph.io.CorpusWriter;
//import morph.io.PdtWriter;
//import morph.io.PushBackTokenizer;
//import morph.io.TntWriter;
//import morph.io.WordReader;
//import morph.ma.lts.Lts;
//import morph.util.MUtilityBase;
//import util.Util;
//import util.col.Cols;
//import util.col.MultiMap;
//import util.err.Err;
//import util.err.FormatError;
//import util.err.Log;
//import util.io.IO;
//import util.io.LineReader;
//import util.io.XFile;
//import util.str.StringPair;
//import util.str.Strings;
//
///**
// *
// * @author jhana
// */
//public class Tt extends MUtilityBase {
//    private PrintWriter memoryW = null;
//    private final MultiMap<Tag, Tag> ttMap = new MultiMap<String, String>();
//
//    private boolean translateSpaArticles = false; // TODO hack
//
//    public static void main(String[] args) throws IOException {
//        new Tt().go(args);
//    }
//
//    public void go(String[] aArgs) throws java.io.IOException {
//        argumentBasics(aArgs, 3);
//
//        XFile inFile     = getArgs().getDirectFile(0, cCsts);
//        XFile tagMapFile = getArgs().getDirectFile(1);
//        XFile outFile    = getArgs().getDirectFile(2, cCsts);
//
//        XFile memoryFile  = outFile.addExtension("ttmem");
//
//        translateSpaArticles = getArgs().getBool("translateSpaArticles", false);
//
//        loadTagDictionary(tagMapFile);
//
//        translate(inFile, outFile, memoryFile);
//    }
//
//
//    // @todo pass context
//    private void translate(XFile aInFile, XFile aOutFile, XFile aMemoryFile) throws IOException {
//        CorpusLineReader r = null;
//        CorpusWriter w = null;
//        try {
//            r = CorpusReader.openMm(aInFile);       // todo configurable
//            r.setOnlyTokenLines(true);
//
//            if (aOutFile.format() == MUtilityBase.cTnt) {
//                w = new TntWriter(aOutFile);
//            }
//            else if (aOutFile.format() == MUtilityBase.cCsts) {
//                w = new PdtWriter(aOutFile).setLemmaTagCore("l").optionsUpdated();
//                ((PdtWriter)w).writeHeader();
//            }
//            else {
//                Err.fAssert(false, "Format " + aOutFile.format() + " is not supported");
//            }
//
//            for (;;) {
//                if (!r.readLine()) {
//                    break;
//                }
//                String tag = r.tag().toString();
//                String form = r.form();
//                String lemma = r.lemma();
//
//                StringPair form_tag = translate(form, tag, r);
//
//                Lts lts = new Lts().add(lemma, Tagset.getDef().fromString(form_tag.mSecond), null, null);
//                w.writeLine(form_tag.mFirst, lts);
//            }
//
//            // todo write footer
//        } finally {
//            IO.close(r);
//            IO.close(w);
//        }
//    }
//
//
//    // Note after call to this method aR might be in invalid state (calls to form
//    // etc do not reflect the current state (due to pushback call)
//    private StringPair translate(String aForm, String aTag, CorpusLineReader aR) throws IOException {
//        String prefix = aTag.substring(0, 2);
//
//        // TODO hack translates la/el -> l'
//        if (translateSpaArticles && prefix.equals("DA")) {
//            if (aTag.equals("DAMS-0-0---") || aTag.equals("DAFS-0-0---")) {
//                // check if next word starts with vowel
//                if ( aR.readLine()) {
//                    String nextWord = aR.form();
//                    aR.pushBack();
//
//                    if (Strings.charIn(nextWord, 0, "aeiouAEIOUhH")) {
//                        return new StringPair("l'", "DACS-0-0---");
//                    }
//                }
//            }
//        }
//
//        String newPrefix = mTagTranslMap.get(prefix);
//        if (newPrefix != null) {
//            return new StringPair(aForm, newPrefix + aTag.substring(2));
//        }
//
//        String newTag = mTagTranslMap.get(aTag);
//        //System.out.println("old Tag " + aTag + " -> " + newTag);
//        if (newTag == null) {
//            newTag = aTag;
//        }
//
//        return new StringPair(aForm, newTag);
//    }
//
//    void loadTagDictionary(XFile aTagTranslFile) throws java.io.IOException {
//        mTagTranslMap = new TreeMap<String, String>();
//
//        LineReader r = IO.openLineReader(aTagTranslFile);
//        r.configureSplitting(Strings.cWhitespacePattern, 2, "Required format: old tag (prefix) / new tag (prefix)");
//        String fileId = "tagTransl";
//
//        for (;;) {
//            String[] tags = r.readSplitNonEmptyLine();
//            if (tags == null) {
//                break;
//            }
//            mTagTranslMap.put(tags[0], tags[1]);
//        }
//
//        r.close();
//
//        //System.out.println(mTagTranslMap);
//    }//
////
////    /**
////     * Translates back Czech tagset to Russian tagset.
////     *
////     * @param aInFile
////     * @param aTranslMemoryFile
////     * @param aOutFile
////     * @throws IOException
////     */
////    void translate(XFile aInFile, XFile aTranslMemoryFile, XFile aOutFile) throws java.io.IOException {
////        PdtLineReader r = new PdtLineReader(aInFile);
////        r.setOnlyFDLines(false).setFillLTs(true);
////        PdtWriter     w = new PdtWriter(aOutFile);   // todo more configurable
////
//////        if (mTagTransl) mTagTranslMemW.close();
////
////
//////        TntReader r = TntReader.openM(aInFile);
//////        TntWriter w = new TntWriter(aOutFile);
////        LineNumberReader memory = IO.openLineReader(aTranslMemoryFile);
////
////        for(;;) {
////            if (!r.readLine()) break;
////
////            if(r.lineType().Type().fd())
////
////
////
////            String memLine = memory.readLine();
////
////            Tag tag = r.tag();
////            int translTagIdx = memLine.indexOf("<new>" + tag);
////            if (translTagIdx != -1) {
////                int origTagIdx = translTagIdx + 5 + Tag.cTagLen + 6;
////                tag = Tagset.getDef().fromString( memLine.substring(origTagIdx, origTagIdx + Tag.cTagLen) );
////            }
////
////            w.writeLine(r.form(), tag);
////        }
////
////        r.close();
////        w.close();
////        memory.close();
////
//
//    /**
//     * Printer where the result will be directed (@todo see argument -o in main fnc)
//     * @see #main(String[])
//     */
//
//
//    // todo, input should be able to contain tags that info should be passed to output
//    private void analyzeFile(XFile aInFile) throws java.io.IOException {
//        Log.info("Analyzing file %s", aInFile);
//        WordReader r = CorpusReader.openWordReader(aInFile);
//        assert r != null;
//
//        mOut.writeHeader();
//        analyzeWReader(r);
//        Log.info("File analyzed");
//
//        mOut.writeFooter();
//        mOut.close();
//        r.close();
//    }
//
//    private void analyzeWReader(WordReader aR) throws java.io.IOException {
//        int ctxSize = (getArgs().getBool("considerCtx")) ? 2 : 0;
//        Context ctx = new Context(aR,ctxSize,ctxSize);
//        ctx.prepare();
//
//        for( ;ctx.hasNext(); ) {
//            Lts lts;
//            String curForm = ctx.getCur();
//
//            if ( curForm.equals("<p>") || curForm.startsWith("<p ") ||
//                 curForm.equals("<s>") || curForm.startsWith("<s ") ||
//                 curForm.equals("<doc>") || curForm.startsWith("<doc ") ) {
//                lts = null;
//                if (mOut instanceof PdtWriter) {
//                    ((PdtWriter)mOut).writeLine(curForm);
//                }
//            }
//            else if (mNarrow == null || mNarrow.matcher(curForm).matches() ) {
//                lts = mMorph.analyze(ctx.getCur(), ctx);
//                mWords++;
//                Log.dot(mWords);
//                if (mTagTransl) translateTags(ctx.getCur(), lts);
//                if (mAspectTagTransl) lts = aspectTagTranslation(lts);
//
//                mOut.writeLine(ctx.getCur(), lts);
//            }
//            else {
//                lts = null;
//                mOut.writeLine(ctx.getCur(), lts);
//            }
//
//            ctx.update(lts);
//        }
//        Log.info("\n"); // nl after dots
//    }
//
//
//    private void translateTags(String aForm, Lts aLTs) {
//        Map<Tag,Tag> memory = aLTs.translateTags(mTagTranslMap);
//
//        // --- record translation memory map into a file
//        mTagTranslMemW.print("<f>" + mOutFilter.filter(aForm));   // for debug only
//        for (Map.Entry<Tag,Tag> e : memory.entrySet()) {
//            mTagTranslMemW.print("<new>" + e.getKey() + "<orig>" + e.getValue());
//        }
//        mTagTranslMemW.println();
//    }
//
//    private List<ReplacementUnit> readInMapSpec(XFile aFile) throws IOException {
//        LineReader r = IO.openLineReader(aFile);
//        r.configureSplitting(Strings.cWhitespacePattern, 3, "required format: <lemma-regex> <regex> <regex> <replacement strings>");
//
//        List<ReplacementUnit> list = new ArrayList<ReplacementUnit>();
//        for (;;) {
//            String[] strs = r.readSplitNonEmptyLine();
//            if (strs == null) break;
//
//            ReplacementUnit ru = new ReplacementUnit();
//            list.add(ru);
//
//            ru.lemma   = compile(strs[0], r, "leamma-regex");
//            ru.tagOrig = compile(strs[1], r, "2nd");
//            ru.tagNow  = compile(strs[2], r, "3rd");
//            ru.results  = Cols.subList(strs, 3);
//        }
//
//        r.close();
//        return list;
//    }
//
//    static class ReplacementUnit {
//        Pattern lemma;   // not used yet
//        Pattern tagOrig;
//        Pattern tagNow;
//
//        List<String> results;
//    }
//
//
//
//    private Pattern compile(String aPatternStr, LineReader aR, String aPatternIdx) {
//        try {
//            return Pattern.compile("^" + aPatternStr + "$");
//        }
//        catch(PatternSyntaxException e) {
//            throw new FormatError(e, aR.fErr("Incorrect " + aPatternIdx + " regex: " + aPatternStr));
//        }
//    }
//
//
//    void readInTtMap(XFile aTagTranslFile) throws java.io.IOException {
//        LineReader r = IO.openLineReader(aTagTranslFile);
//        r.configureSplitting(Strings.cWhitespacePattern, 2, "Required format: old tag (prefix) / new tags (prefix)");
//        String fileId = "tagTransl";
//
//        for (;;) {
//            String[] tags = r.readSplitNonEmptyLine();
//            if (tags == null) {
//                break;
//            }
//            mTagTranslMap.put(tags[0], tags[1]);
//        }
//
//        r.close();
//
//
//        LineNumberReader r = IO.openLineReader(aTagTranslFile);
//        String fileId = "tagTransl";
//
//        for(;;) {
//            String line = r.readLine();
//            if (line == null) break;
//            line = Util.preprocessLine(line);
//            PushBackTokenizer tok = new PushBackTokenizer(line, " \t\n\r\f", false, r, "tagTransl");
//
//            if (!tok.hasMoreTokens()) continue;
//
//            try {
//                Tag rusTag = tok.getTag("Orig tag");
//                for(;tok.hasMoreTokens();) {
//                    Tag czTag = tok.getTag("New tag");
//                    mTagTranslMap.add(rusTag, czTag);
//
//                    if (czTag.getNegation() == 'A')
//                        mTagTranslMap.add(rusTag.setNegation('N'), czTag.setNegation('N'));
//                }
//            }
//            catch (java.util.NoSuchElementException e) {}
//
//        }
//
//        r.close();
//    }
//}
//
//
//}
