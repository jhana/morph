package morph.corpus;

import java.io.IOException;
import java.io.PrintWriter;
import lombok.Cleanup;import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import morph.io.Corpora;
import morph.io.CorpusLineReader;
import morph.io.TntWriter;
import morph.io.WordReader;
import morph.util.MUtilityBase;
import util.io.IO;
import util.io.XFile;
import util.io.ezreader.EzReader;
import util.str.StringPair;
import util.str.transl.RegexTranslator;
import util.str.transl.Translator;
import util.str.transl.Translators;

/**
 *
 * @todo handle more formats (tnt, plain, possibly pdt) both on input and output
 * @todo implement as a filter on a general corpus to corpus utility
 * @author Jirka Hana
 */
public class TranslateFL extends MUtilityBase {
    public static void main(String[] args) throws IOException {
        new TranslateFL().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);

        XFile inFile  = getArgs().getDirectFile(0);
        XFile trSpec  = getArgs().getDirectFile(1);
        XFile outFile = getArgs().getDirectFile(2);

        translator = readInTranslSpec(trSpec);

        if (inFile.format() == MUtilityBase.cPlain) {
            translatePlain(inFile, outFile);
        }
        else {
            translate(inFile, outFile);
        }
    }

    private Translator readInTranslSpec(XFile aTranslSpecFile) throws IOException {
        EzReader r = new EzReader(aTranslSpecFile);
        r.configureSplitting("string to replace", "replacement");

        final Translators trs = new Translators();

        for (StringPair pair : r.readPairs()) {
            String from = pair.mFirst;
            String to   = pair.mSecond;

            if (from.startsWith("^") && from.endsWith("$") ) {
                w2w.put(from.substring(1, from.length()-1), to);
            }
            else {
                if (to.equals("null")) to = "";
                trs.add(new ReplaceTranslatorRegex(from, to));
            }
        }
        System.out.println("Dictionary: " + w2w);
        return trs;
    }

    private void translate(XFile inFile, XFile outFile) throws IOException {
        CorpusLineReader r = null;
        TntWriter w = null;
        try {
            r = Corpora.openM(inFile);
            w = new TntWriter(outFile);

            for (;;) {
                if (!r.readLine()) break;
                String newForm = translate(r.form());

                w.writeLine(newForm, r.tag());
            }
        }
        finally {
            IO.close(r,w);
        }
    }

    private void translatePlain(XFile inFile, XFile outFile) throws IOException {
        @Cleanup WordReader  r = Corpora.openWordReader(inFile);
        @Cleanup PrintWriter w = IO.openPrintWriter(outFile);

        for (;;) {
            if (!r.nextWord()) break;
            String newForm = translate(r.word());

            w.println(newForm);
        }
    }

    private final Map<String,String> w2w = new HashMap<String,String>();
    private Translator translator;

    private String translate(String aWord) {
        // check word 2 word map first
        String toWord = w2w.get(aWord);
        if (toWord != null) {
            return toWord;
        }
        else {
            return translator.translate(aWord);
        }
    }



public class ReplaceTranslatorRegex extends RegexTranslator {
    public ReplaceTranslatorRegex(String aOrig, String aReplacement) {
        super( Pattern.compile(aOrig), Matcher.quoteReplacement(aReplacement.toString()) );
    }
}

}
