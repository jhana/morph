package morph.corpus;

import java.io.IOException;
import java.util.Set;
import lombok.Cleanup;
import morph.io.Corpora;
import morph.io.CorpusLineReader;
import morph.io.CorpusWriter;
import morph.io.TntWriter;
import morph.util.MUtilityBase;
import morph.ts.Tag;
import util.io.XFile;

/**
 * todo allow some other output than tnt
 *
 * @author Jirka
 */
public class RemoveWords extends MUtilityBase {

    public static void main(String[] args) throws IOException {
         new RemoveWords().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs, 2);

        final XFile inCorpusFile  = getArgs().getDirectFile(0, cCsts);
        final XFile outCorpusFile = getArgs().getDirectFile(1, cCsts);

        // --- process --
        @Cleanup CorpusWriter     w = new TntWriter(outCorpusFile);
        @Cleanup CorpusLineReader r = Corpora.openMm(inCorpusFile);;

        r.setOnlyTokenLines(true);
        filterCorpus(r, w);
    }

    private void filterCorpus(final CorpusLineReader aR, final CorpusWriter aW) throws IOException {
        for (;;) {
            if (!aR.readLine()) {
                break;
            }
            String form = aR.form();
            Set<Tag> tags = aR.tags();

            aW.writeLine("dummy_word", tags);
        }
    }

}