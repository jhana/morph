package morph.corpus;

import java.io.*;
import lombok.Cleanup;
import util.io.*;
import morph.io.*;
import morph.util.MUtilityBase;
import util.io.XFile;

/**
 * Splits corpus into several parts with specified length.
 * Use cat * > x to connect individual files.
 * @todo other formats
 * @todo write log with boundaries (sentence ids) for each out file
 */
public class Extract extends MUtilityBase {

    public static void main(String[] args) throws IOException {
        new Extract().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);
        int maxWords      = getArgs().getInt("corpusSizeLimit", Integer.MAX_VALUE);
        XFile inFileName  = getArgs().getDirectFile(1);
        XFile outFileName = getArgs().getDirectFile(2);

        extract(maxWords, inFileName, outFileName);
    }

    public void extract(int maxWords, XFile aInFile, XFile aOutFile) throws java.io.IOException {
        @Cleanup WordReader r = Corpora.openWordReader(aInFile);
        @Cleanup PrintWriter w = IO.openPrintWriter(aOutFile);

        int word = 0;
        for(;;) {
            if (word >= maxWords || !r.nextWord()) break;

            w.println(r.word());
            word++;
        }
        System.out.printf("Read %n words\n", word);
    }

}