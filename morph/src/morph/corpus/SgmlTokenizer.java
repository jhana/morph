package morph.corpus;

import java.io.IOException;
import java.io.Reader;
import util.err.FormatError;
import util.sgml.SgmlTag;

/**
 * Add iterable interface (all tokens, tags only, text only)
 *
 */
public class SgmlTokenizer  {
    public enum TokenType {
        tag, text, bof, eof, eol;

        public boolean tag()  {return this == tag;}
        public boolean text()  {return this == text;}
        public boolean eol()  {return this == eol;}
        public boolean bof()  {return this == bof;}
        public boolean eof()  {return this == eof;}
    };

    /**
     * The current token type.
     */
    private TokenType mTokenType;

    /**
     * The current token.
     * Includes angle brackets for tags.
     */
    private String mToken;

    /**
     * The parsed input.
     */
    final private Reader mReader;

    private int lineNo;

    private boolean tokenPushedBack = false;

    private boolean charPushedBack = false;

    private int pushedBackChar;


    public SgmlTokenizer(Reader aReader) {
        mReader = aReader;
        lineNo = 0;
        mTokenType = TokenType.bof;
    }


    /**
     * Returns the current token type.
     *
     * @see #TokenType
     */
    public TokenType tokenType() {return mTokenType;}

    /**
     * Returns the current token.
     */
    public String token()     {return mToken;}

    /**
     * Returns the integer corresponding to the current token.
     * No checks are performed.
     */
    public int tokenInt()     {return Integer.parseInt(mToken);}

    /**
     * Note: constructed each time again (@todo cache?)
     */
    public SgmlTag tag() {
        if (!mTokenType.tag()) throw new IllegalStateException("Current token (" + mTokenType + ") is not a tag");
        return new SgmlTag(mToken);
    }

    public int getLineNo() {
        return lineNo;
    }

    /**
     * Repeated calls have not effect.
     */
    public void pushBack() {
        if (mTokenType.bof()) throw new IllegalStateException("Nothing to push back");
	    tokenPushedBack = true;
    }



    /**
     * Moves to the next token. The next token becomes the current token.
     *
     * @return type of the new token
     * @see #TokenType
     */
    public TokenType next() throws java.io.IOException {
        // previous token was pushed-back
        if (tokenPushedBack) {
            tokenPushedBack = false;
            return mTokenType;
        }

        int c = charPushedBack ? pushedBackChar : mReader.read();
        charPushedBack = false;

        if (c == -1) {
            mToken = null;
            mTokenType = TokenType.eof;
            return mTokenType;
        }

        // new lines: possibilities n (unix), r (mac), r+n (win)
        if (c == '\n' ) {
            mTokenType = TokenType.eol;
            mToken = null;
            lineNo++;
            return mTokenType;
        }
        else if (c == '\r' ) {
            // skip \n if it is there
            c = mReader.read();
            if (c != '\n') {
                pushedBackChar = c;
                charPushedBack = true;
            }
            mTokenType = TokenType.eol;
            mToken = null;
            lineNo++;
            return mTokenType;
        }
        else if (c == '<') {
            mTokenType = TokenType.tag;

            final StringBuilder buf = new StringBuilder();
            for (;;) {
                buf.append((char)c);          // todo optimize
                if (c == '>') break;

                c = mReader.read();
                assertFormat(c != -1, buf, "Tag interrupted by the end of the stream.");
                assertFormat(c != '<',buf, "'<' inside of a tag.");
            }

            mToken = buf.toString();
        }
        else {
            mTokenType = TokenType.text;

            // read untill eol/eof/<
            final StringBuilder buf = new StringBuilder();
            for (;;) {
                buf.append((char)c);          // todo optimize

                c = mReader.read();
                if (c == '<' || c == '\r' || c == '\n' || c == '<') {
                    pushBackChar(c);
                    break;
                }
                if (c == -1) break;
            }
            mToken = buf.toString();
        }

        return mTokenType;
    }

    /**
     * A convenience method, abbreviating next() followed byt token().
     */
    public String getNextToken() throws IOException {
        next();
        return token();
    }

    

    private void pushBackChar(int aChar) {
	    charPushedBack = true;
        pushedBackChar = aChar;
    }


    private void assertFormat(boolean aTest, StringBuilder aBuf, String aMsg) {
        if (!aTest) throw new FormatError("Error [%d]: %s -- %s", lineNo, aMsg, aBuf.toString());
    }

}



