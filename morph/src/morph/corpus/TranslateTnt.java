package morph.corpus;

import java.io.IOException;
import java.io.PrintWriter;
import lombok.Cleanup;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import morph.io.Corpora;
import morph.io.CorpusLineReader;
import morph.io.TntWriter;
import morph.io.WordReader;
import morph.util.MUtilityBase;
import morph.ts.Tag;
import util.io.IO;
import util.io.XFile;
import util.io.ezreader.EzReader;

/**
 * Temporary solution. Translates form when the form + tag matches.
 *
 * @see TranslateFL
 * @author Jirka Hana
 */
public class TranslateTnt extends MUtilityBase {
    public static void main(String[] args) throws IOException {
        new TranslateTnt().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);

        XFile inFile  = getArgs().getDirectFile(0);
        XFile trSpec  = getArgs().getDirectFile(1);
        XFile outFile = getArgs().getDirectFile(2);

        translator = readInTranslSpec(trSpec);

        if (inFile.format() == MUtilityBase.cPlain) {
            translatePlain(inFile, outFile);
        }
        else {
            translate(inFile, outFile);
        }
    }

    private Translators readInTranslSpec(XFile aTranslSpecFile) throws IOException {
        final EzReader r = new EzReader(aTranslSpecFile);
        r.configureSplitting("form pattern", "tag pattern", "find", "replace");

        final Translators trs = new Translators();
        for (List<String> strs : r.readLists()) {
            String formPattern = strs.get(0);
            String tagPattern  = strs.get(1);
            String find        = strs.get(2);
            String replace     = strs.get(3);

            if (replace.equals("null")) replace = "";

            trs.add(new FormTranslator(formPattern, tagPattern, find, replace));
        }

        return trs;
    }

    private void translate(XFile inFile, XFile outFile) throws IOException {
        CorpusLineReader r = null;
        TntWriter w = null;
        try {
            r = Corpora.openM(inFile);
            w = new TntWriter(outFile);

            for (;;) {
                if (!r.readLine()) break;
                String newForm = translate(r.form(), r.tag());

                w.writeLine(newForm, r.tag());
            }
        }
        finally {
            IO.close(r,w);
        }
    }

    private void translatePlain(XFile inFile, XFile outFile) throws IOException {
        @Cleanup WordReader  r = Corpora.openWordReader(inFile);
        @Cleanup PrintWriter w = IO.openPrintWriter(outFile);

        for (;;) {
            if (!r.nextWord()) break;
            String newForm = translate(r.word(), null);

            w.println(newForm);
        }
    }

    private Translators translator;

    private String translate(String aWord, Tag aTag) {
        return translator.translate(aWord, aTag);
    }

    public class FormTranslator  {
        private final Pattern formPattern;
        private final Pattern tagPattern;
        private final Pattern find;
        private final String  replace;

        public FormTranslator(String aFormPattern, String aTagPattern, String aFind, String aReplace) {
            formPattern    = Pattern.compile(aFormPattern);
            tagPattern = Pattern.compile(aTagPattern);
            
            find = Pattern.compile(aFind);
            replace = aReplace;
        }

        public String translate(String aWord, Tag aTag) {
            if (formPattern.matcher(aWord).matches() && aTag.matches(tagPattern)) {
                return find.matcher(aWord).replaceAll(replace);
            }
            else {
                return aWord;
            }
        }
    }

    public static class Translators  {
        final List<FormTranslator> translators = new ArrayList<FormTranslator>();;

        public void add(FormTranslator aTranslator) {
            translators.add(aTranslator);
        }

        public String translate(String aWord, Tag aTag) {
            for (FormTranslator t : translators) {
                aWord = t.translate(aWord,aTag);
            }
            return aWord;
        }
    }

}
