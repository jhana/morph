package morph.corpus;

import java.io.IOException;
import java.io.PrintWriter;
import lombok.Cleanup;
import lombok.Cleanup;import java.util.regex.Matcher;
import java.util.regex.Pattern;

import morph.util.MUtilityBase;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;
import util.str.Strings;

/**
 *
 * @author jirka
 */
public class Csts2Stanford extends MUtilityBase {

    public static void main(String[] args) throws IOException {
         new Csts2Stanford().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs, 2);

        final XFile inCorpusFile  = getArgs().getDirectFile(0, cCsts);
        final XFile outCorpusFile = getArgs().getDirectFile(1);
        //final boolean tags = getArgs().getBool("noTags", false);

        // --- process --
        @Cleanup LineReader   r = IO.openLineReader(inCorpusFile);;
        @Cleanup PrintWriter  w = IO.openPrintWriter(outCorpusFile);

        //r.setOnlyTokenLines(false);
        filterCorpus(r, w);
        IO.close(r,w);
    }

    final Pattern sPattern = Pattern.compile("\\<s.*?\\>");
    //final Pattern fPattern = Pattern.compile("\\<(f|d).*?\\>(.*?)\\<l\\>.*?\\<t\\>(.*?)\\<.*");
    final Pattern fPattern = Pattern.compile("\\<(f|d).*?\\>(.*?)\\<l\\>.*?\\<t\\>([^\\<]*).*$");
    private int counter = 0;    // prevents outputing empty sentences

    private void filterCorpus(final LineReader aR, final PrintWriter aW) throws IOException {
        for (int i=0;;i++) {
            if (i%10000 == 0) System.err.print('.');
            String line = aR.readLine();
            if (line == null) break;

            final Matcher sMatcher = sPattern.matcher(line);
            if (sMatcher.matches()) {
                if (counter > 0) {
                    aW.println();
                    counter=0;
                }
            }
            else {
                final Matcher fMatcher = fPattern.matcher(line);
                if (fMatcher.matches()) {
                    counter++;
                    final String form = fMatcher.group(2);
                    String tag  = fMatcher.group(3);
                    if (!Strings.charIn(tag, 14, "-8")) {
                        tag = Strings.setCharAt(tag, 14, '-');
                    }
                    if (Strings.charIn(tag, 2, "IY")) {
                        tag = Strings.setCharAt(tag, 2, 'M');
                    }
                    if (Strings.charIn(tag, 3, "D")) {
                        tag = Strings.setCharAt(tag, 3, 'P');
                    }
                    if (Strings.charIn(tag, 4, "5")) {
                        tag = Strings.setCharAt(tag, 4, '1');
                    }

                    aW.printf("%s/%s ", form, tag);
                }
            }
        }
        aW.println();
    }

}