//package morph;
//
//import util.*;
//import java.io.*;
//import java.util.*;
//
///**
// * Creates uni-, bi- and trigram tables for Jiri Mirovsky's tagger
// */
//public class NGramCounter extends UtilityBase {
//    /**
//     * Counter of tag unigram frequencies
//     */
//    Counter<Integer> mUniCounter;
//
//    /**
//     * Counter of tag bigram frequencies
//     */
//    Counter<Bigram>  mBiCounter;
//
//    /**
//     * Counter of tag trigram frequencies
//     */
//    Counter<Trigram> mTriCounter;
//    
//    /**
//     * Table for translation of tags to integer indexes used by the tagger
//     */
//    Map<String,Integer> mTagTranslTbl;
//
//    int mTagNr;
//    
//    /**
//     * The -2 tag
//     */
//    Integer mTag1;
//
//    /**
//     * The -1 tag
//     */
//    Integer mTag2;
//    
//    /**
//     * arguments:
//     *   <tagTranslFile> <TrainingFile> <unigramTableOutFile> <bigramTableOutFile> <trigramTableOutFile>
//     */
//    public static void main(String[] args) throws IOException {
//        new NGramCounter().go(args);
//    }
//    
//    public void go(String[] aArgs) throws java.io.IOException {
//        argumentBasics(aArgs,4);
//
//        readTranslTable(getMArgs().getDirectFile(0));
//        buildTables(getMArgs().getDirectFile(1));
//        
//        saveTabls(getMArgs().getDirectFile(2), getMArgs().getDirectArg(3));
//    }
//
//    protected String helpString() {
//        return "@todo"; // @todo
//    }
//
//    /**
//     * Builds uni-, bi- and trigram tables from the specified training file.
//     */
//    public void buildTables(File aTrainingFile) throws java.io.IOException {
//        LineNumberReader in = IO.createLNFileReader(aTrainingFile, mIEnc);
//        mTagNr = 0;
//        
//        mUniCounter = new Counter<Integer>();
//        mBiCounter  = new Counter<Bigram>();
//        mTriCounter = new Counter<Trigram>();
//        
//        Integer zTag = mTagTranslTbl.get("WWWWWWWWWWWWWWW");
//        assert (zTag != null);
//        
//        mTag1 = mTag2 = mTagTranslTbl.get("Z#-------------");;
//
//        for(;;) {
//            String line = in.readLine();
//            if (line == null) break;
//
//            if (line.startsWith("<f")) {
//                String tag = extract(line, mInTagTag);             // in case of multiple tags only the 1st one is considered
//                Integer tagIdx = mTagTranslTbl.get(tag);
//                if (tagIdx != null)
//                    countTag(tagIdx);
//                else
//                    System.out.println("Error: Unknown tag - " + tag + " (ignored)");
//            }
//            else if (line.startsWith("<d"))
//                countTag(zTag);
//        }
//
//        in.close();
//    }
//    
//    void countTag(Integer aTag) {
//        mTagNr++;
//        mUniCounter.add(aTag);
//        mBiCounter.add( new Bigram(mTag2,aTag));
//        mTriCounter.add(new Trigram(mTag2,mTag1,aTag));
//        
//        mTag1 = mTag2;
//        mTag2 = aTag;
//    }
//                
//    /** 
//     * Read the tag translation table from a file.
//     */
//    public void readTranslTable(File aFile) throws java.io.IOException {
//        LineNumberReader in = IO.createLNFileReader(aFile, "@todoenc");  // ascii
//        mTagTranslTbl = new HashMap<String,Integer>();
//
//        for(;;) {
//            String line = in.readLine();
//            if (line == null) break;
//            mTagTranslTbl.put( line, in.getLineNumber() );
//        }       
//        in.close();
//    }
//
//    /** 
//     * Writes out the resulting tables to files.
//     */
//    public void saveTabls(File aDir, String aFileEnding) throws java.io.IOException {
//        writeTable(aDir, "Unigramy.tab.", aFileEnding, mUniCounter, 0);
//        writeTable(aDir, "Bigramy.tab.",  aFileEnding, mBiCounter, 1);
//        writeTable(aDir, "Trigramy.tab.", aFileEnding, mTriCounter, 1);
//        
//        PrintWriter out     = IO.createFilePrintWriter(new File(aDir, "konst.txt." + aFileEnding), mOEnc);
//        out.println(mTagNr);
//        out.println(mTagNr-1);
//        out.println(mTagNr-2);
//        out.println("Comment: Line 1 - # of unigram tokens, Line 2 - # of bigram tokens, Line 3 - # of trigram tokens");
//        out.close();
//    }
//
//    /**
//     * Writes out a single n-gram table.
//     */
//    protected <T> void writeTable(File aDir, String aFileCore, String aFileEnding, Counter<T> aCounter, int aMinFreq) throws java.io.IOException {
//        PrintWriter out     = IO.createFilePrintWriter(new File(aDir, aFileCore + aFileEnding), mOEnc);
//        for( T e : new TreeSet<T>(aCounter.keySet()) )  {
//            int freq = aCounter.frequency(e);
//            if (freq > aMinFreq) out.println(e + " " + freq);;
//        }
//        out.close();
//    }
//}
//
//
//
///**
// * Class representing a bigram (Integer^2). 
// * Ordered lexically.
// */
//class Bigram implements Comparable {
//    Integer mTag1;
//    Integer mTag2;
//    
//    public Bigram (Integer aTag1, Integer aTag2) {
//        mTag1 = aTag1;
//        mTag2 = aTag2;
//    }
//    
//    /** 
//     * Lexical ordering on bigrams
//     */
//    public int compareTo(Object o) {
//        Bigram bi = (Bigram)o;
//
//        int comp = mTag1.compareTo(bi.mTag1);
//        if (comp != 0) return comp;
//
//        return mTag2.compareTo(bi.mTag2);
//    }    
//    
//    public String toString() {
//        return mTag1.toString() + ' ' + mTag2;
//    }
//    
//    public int hashCode() {
//        return mTag1.hashCode() | mTag2.hashCode();
//    }
//    
//    public boolean equals(Object aObj) {
//        return compareTo(aObj) == 0;
//    }
//    
//}
//
///**
// * Class representing a trigram (Integer^3)
// */
//class Trigram implements Comparable {
//    Integer mTag1;
//    Integer mTag2;
//    Integer mTag3;
//    
//    public Trigram (Integer aTag1, Integer aTag2, Integer aTag3) {
//        mTag1 = aTag1;
//        mTag2 = aTag2;
//        mTag3 = aTag3;
//    }
//    
//    /** 
//     * Lexical ordering on trigrams.
//     */
//    public int compareTo(Object o) {
//        Trigram tri = (Trigram)o;
//
//        int comp = mTag1.compareTo(tri.mTag1);
//        if (comp != 0) return comp;
//
//        comp = mTag2.compareTo(tri.mTag2);
//        if (comp != 0) return comp;
//        
//        return mTag3.compareTo(tri.mTag3);
//    }    
//    
//    public String toString() {
//        return mTag1.toString() + ' ' + mTag2 + ' ' + mTag3;
//    }
//
//    public int hashCode() {
//        return mTag1.hashCode() | mTag2.hashCode();
//    }
//
//    public boolean equals(Object aObj) {
//        return compareTo(aObj) == 0;
//    }
//}
