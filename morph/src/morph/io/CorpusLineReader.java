package morph.io;

import util.io.*;
import java.util.*;
import lombok.Getter;
import morph.ma.lts.Fplus;
import morph.ts.Tag;
import morph.ts.Tagset;
import morph.ts.TagsetRegistry;
import util.err.Err;
import util.io.IO;
import util.opt.Options;



public abstract class CorpusLineReader extends CorpusLineRW implements CorpusLineReaderI {
    protected final LineReader r;

    protected String line;
    protected Fplus fplus;

    // --- options ---
    @Getter protected /* final */ Tagset tagset = Tagset.getDef();
    @Getter protected boolean mm = true;
    @Getter protected boolean onlyTokenLines = true;

    
// -----------------------------------------------------------------------------    
// Configuration
// -----------------------------------------------------------------------------    
    
    public CorpusLineReader(XFile aFile) throws java.io.IOException {
        super(aFile);
        r = IO.openLineReader(aFile);   // todo move to open
        
    }

    /** 
     * Cannot be in constructor as it calls inherited methods.
     * Cannot be in (future) open method as it should be possible to override it before calling open.
     * @return 
     */
    public CorpusLineReader configure() {
        setOptions(file.getProperties().getOptions());
        return this;
    }
    
    /**
     * This method should be called if overriden.
     * @param options
     * @return 
     */
    public CorpusLineReader setOptions(final Options options) {
        if (options != null) {
            setTagset(createTagset(options));
            setMm(options.getBool("mm", mm));
            setOnlyTokenLines(options.getBool("onlyTokenLines", onlyTokenLines));
        }

        return this;
    }
    
    /** Todo move somewhere else */
    public static Tagset createTagset(final Options options) {
        final String tsStr = options.getString("ts");
        if (tsStr == null) {
            return Tagset.getDef();
        }
        else if ("?".equals(tsStr)) {
            return new Tagset.TagsetBuilder().build();
        }
        else {
            // load the tagset with that id
            Tagset ts = TagsetRegistry.getDef().getTagset(tsStr);
            Err.fAssert(ts != null, "Tagset with id=%s is not known", tsStr);
            return ts;
        }
    }
    
    public CorpusLineReader setTagset(Tagset tagset) {
        this.tagset = tagset; 
        return this;
    }    

    
    public CorpusLineReader setOnlyTokenLines(boolean aOnlyTokenLines) {
        onlyTokenLines = aOnlyTokenLines; 
        return this;
    }
    
    @Deprecated
    public CorpusLineReader setMm(boolean aMm) {
        mm = aMm;
        return this;
    }

    
// -----------------------------------------------------------------------------    
    
    /**
     * Reads and processes the next line in corpus.
     *
     * The overriding function must set fplus to null!
     * @return true if the line was read; false if eof was reached
     * @ if processing of the line was not successful (wrong format, wrong tag, ..)
     * 
     */
    @Override
    public abstract boolean readLine() throws java.io.IOException;

    public void pushBack() {
        r.pushBack();
    }

    public LineReader getReader() {
        return r;
    }
    
    @Override
    public LineTypeI lineType() {throw new java.lang.UnsupportedOperationException();}

    @Override
    public boolean isTokenLine() {return true;}

    public int getLineNumber()  {return r.getLineNumber();}

    @Override
    public String line()        {return line;}

    public Fplus getFPlus() {
        return fplus;
    }
    

    /**
     * Null if eof was reached
     */
    @Override
    public String form()        {return (fplus == null) ? null : fplus.getForm();}

    public final String lemma()       {return (fplus == null) ? null : fplus.getLts().lt().lemma();}
    public final Tag tag()            {return (fplus == null) ? null : fplus.getLts().lt().tag();}
    public final String tagStr()      {return (fplus == null) ? null : tag().toString();}    //todo vice versa

    public Set<String> lemmas() {return (fplus == null) ? null : fplus.getLts().lemmas();}
    public Set<Tag> tags() {return (fplus == null) ? null : fplus.getLts().tags();}
    
    @Override
    public void close() throws java.io.IOException {
        r.close();
    }

    final void assertE(boolean aTest, String aMsg) {
        if (aTest) return;

        Err.fErr("Error [%s %d] - %s\n", file, r.getLineNumber(), aMsg);
    }

    /**
     * Used only for unit testing
     */
    public void testSetLine(String aLine) {line = aLine;}

    
}
