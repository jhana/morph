package morph.io;

import util.io.*;
import java.io.*;
import java.util.*;
import morph.ts.Tag;
import util.io.IO;
import util.opt.Options;
import util.opt.SingleStrOptions;
import util.str.Transliterate;


public abstract class CorpusWriter extends CorpusLineRW implements Closeable  {
    public static final String cOptInfo   = "info";
    public static final String cOptSrc    = "src";
    public static final String cOptTranslit    = "translit";    // todo allow configuring transliteration
    
    protected final PrintWriter mW;
    Filter mFilter;

    public boolean writeInfo = false;
    public boolean writeSrc  = false;
    
    public CorpusWriter(XFile aFile, Filter aFilter) throws java.io.IOException {
        super(aFile);
        mW = IO.openPrintWriter(aFile);
        mFilter = aFilter;
    }

    public CorpusWriter(PrintWriter aW, Filter aFilter) throws java.io.IOException {
        super(null);
        mW = aW;
        mFilter = aFilter;
    }
    
    public CorpusWriter setFilter(Filter aFilter) {
        mFilter = aFilter;
        return this;
    }

    public Filter getFilter() {
        return mFilter;
    }
    
    /**
     * Calls optionsUpdated at the end.
     *
     * @param aOptions options to set, can be empty or null
     */
    public CorpusWriter setOptions(Options aOptions) {
        if (aOptions != null) {
            for (String key : aOptions.keySet()) {
                setOptionImpl(key, aOptions);
            }
        }
        optionsUpdated();
        
        return this;
    }


    public CorpusWriter setOption(String aKey, Object aValue) {
        setOptions(new SingleStrOptions(aKey, aValue));
        return this;
    }

    /**
     * Retrieve the specified options from the option object.
     *
     * Override if there are any options to set. Overriding method should call
     * super.setOptionImpl(..).
     */
    protected CorpusWriter setOptionImpl(String aKey, Options aOptions) {
        if (aKey.equals(cOptInfo)) {
            writeInfo = aOptions.getBool(aKey);
        }
        else if (aKey.equals(cOptSrc)) {
            writeSrc = aOptions.getBool(aKey);
        }
        else if (aKey.equals(cOptTranslit)) {
            mFilter = new Filter() {
                @Override
                public String filter(String aString) {
                    return Transliterate.transliterate(aString);    // allow configuring the exact transliteration aKey.startsWith(cOptTranslit), and use the rest as a key for the translit registry
                }
            };
        }

        return this;
    }
    
    /**
     * Function to incorporate option changes. Call if a particular set of options requires that.
     * 
     * Override if any changes must be done.
     */
    public CorpusWriter optionsUpdated() {return this;}
    
    @Override
    public void close() throws java.io.IOException {
        mW.close();
    }

    public void writeHeader() {}
    public void writeFooter() {}

    public void writeRaw(String aLine) {
        mW.println(aLine);
    }

    public abstract void writeLine(String aForm, Tag aTag);
    public abstract void writeLine(String aForm, Collection<Tag> aTags);
    public abstract void writeLineS(String aForm, Collection<String> aTags);
    public abstract void writeLine(String aForm, morph.ma.lts.Lts aLts);

    protected final String filter(String aString) {
        return (mFilter == null) ? aString : mFilter.filter(aString);
    }
}
