package morph.io;

import util.io.*;
import java.util.*;
import morph.ma.lts.Fplus;
import morph.ma.lts.Lt;
import morph.ts.Tag;
import morph.ts.Tags;
import morph.ts.Tagset;
import util.err.Log;
import util.err.MyException;


/**
 *
 * All lemmas are set to an empty string (tnt format does not support lemma information)
 * @todo maybe lazy filling instead 
 *
 */
public class TntReader extends CorpusLineReader {
    protected String[] mTokens;
    
    /**
     * Reads disambiguated files (only one tag).
     * Does not fill tags.
     */
    public static TntReader open(XFile aFile) throws java.io.IOException {
        return new TntReader(aFile);
    }


    /**
     * In most cases, aReadTag and aReadTags are not true at the same time.
     * @todo remove and bool replace with set...
     */
    private TntReader(XFile aFile) throws java.io.IOException {
        super(aFile);
    }


    /**
     * Reads the next line from the TNT tagged file. 
     * Ignores TNT comments (lines starting with %%)
     * Returns false once EOF was reached; true otherwise
     */
    @Override
    public boolean readLine() throws java.io.IOException {
        fplus = null;

        do {
            line = r.readLine();
            if (line == null) return false;
            line = line.trim();
        } while ( line.startsWith("%%") || line.length() == 0 );

        mTokens = cWhiteSpacePattern.split(line);
        assertE(mTokens.length > 1, "Format should be: 'form tag'");

        fplus = new Fplus(mTokens[0]);

        try {
            final Set<Tag> tags = new HashSet<Tag>( Tags.fromStrings( tagset, Arrays.asList(mTokens).subList(1,mTokens.length)) );
            for (Tag tag : tags) {
                fplus.getLts().add(new Lt("?", tag));
            }
        }
        catch(MyException e) {
            Log.log(e, "Error [%d] %s\n", r.getLineNumber(), r.getFile());
            /// todo throw
        }
        
        return true;
    }

}

