package morph.io;


public interface PdtSgmlConst {
    public final static String cForm       = "<f>";
    public final static String cFormCore   = "f";
    public final static String cFormStart  = "<f";

    public final static String cLemma      = "<l>";
    public final static String cLemmaCore  = "l";

    public final static String cTag        = "<t>";
    public final static String cTagCore    = "t";
    
    public final static String cPunctCore  = "d";
    public final static String cPunctStart = "<d";

    public final static String cMALemma      = "<MMl>";
    public final static String cMALemmaCore  = "MMl";

    public final static String cMATag        = "<MMt>";
    public final static String cMATagCore    = "MMt";

    public final static String cTagLemma      = "<MDl>";
    public final static String cTagLemmaCore  = "MDl";

    public final static String cTagTag        = "<MDt>";
    public final static String cTagTagCore    = "MDt";
}

