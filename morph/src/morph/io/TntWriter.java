package morph.io;

import java.io.PrintWriter;
import morph.ma.lts.*;
import util.io.*;
import java.util.*;
import util.col.Cols;
import morph.ts.Tag;
import morph.ts.Tagset;



public class TntWriter extends CorpusWriter  {

    public TntWriter(PrintWriter aWriter) throws java.io.IOException {
        super(aWriter, null);
    }

    public TntWriter(PrintWriter aWriter, Filter aFilter) throws java.io.IOException {
        super(aWriter, aFilter);
    }

    public TntWriter(XFile aFile) throws java.io.IOException {
        super(aFile, null);
    }

    public TntWriter(XFile aFile, Filter aFilter) throws java.io.IOException {
        super(aFile, aFilter);
    }
    
    
    public void writeLine(String aForm, Tag aTag)  {
        mW.printf("%s\t%s\n",  aForm, aTag);
    }

    public void writeLine(String aForm, Collection<Tag> aTags)  {
        mW.printf("%s\t%s\n", aForm, Cols.toString(aTags,"", "", " ", ""));
    }

    public void writeLineS(String aForm, Collection<String> aTags)  {
        mW.printf("%s\t%s\n", aForm, Cols.toString(aTags, "", "", " ", ""));
    }

    public void writeLine(String aForm, Lts aLTs) {
        mW.print(filter(aForm));
        mW.print('\t'); 

        if (aLTs.isEmpty()) {
            mW.println(Tagset.getDef().cAllHyphens);
            return;
        }

        for (Lt lt : aLTs) {
            mW.print(lt.tag().toString());
            mW.print(' ');
        }
        
        mW.println();
    }

}
