package morph.io.wiki;

import java.io.PrintWriter;
import java.io.Reader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import morph.util.MUtilityBase;
import util.io.IO;
import util.io.XFile;

/**
 *
 * @author Jirka Hana
 */
public class Wiki extends MUtilityBase {

    public static void main(String[] args) throws Exception {
        new Wiki().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException, XMLStreamException {
        //argumentBasics(aArgs, 2);
        argumentBasics(aArgs, 2);
        XFile inFile = getArgs().getDirectFile(0); // IO.fileFromString("d:/morph/corpora/rus/wiki.sample.xml@utf8", null);
        XFile outFile = getArgs().getDirectFile(1);//inFile.addExtension("words");
        int maxWords = getArgs().getInt("corpusSizeLimit", Integer.MAX_VALUE);

        Reader r = null;
        PrintWriter w = null;
        WikiDump dump = null;
        int words = 0;

        try {
            r = IO.openLineReader(inFile);
            w = IO.openPrintWriter(outFile);

            dump = new WikiDump(r);

            out: for (WikiArticle a : dump) {
                for (String str : a) {
                    w.println(str);
                    words++;
                    if (words >= maxWords) break out;
                }
            }
        }
        finally {
            if (dump != null) dump.close();
            IO.close(r,w);
        }
    }

}
