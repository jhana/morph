
package morph.io.wiki;

import java.io.Reader;
import java.util.Iterator;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Jirka Hana
 */
public class WikiDump implements Iterable<WikiArticle> {
    private final XMLInputFactory inputFactory;
    private final XMLEventReader xmlEventReader;
    private boolean inText = false;

    public WikiDump(Reader r) throws java.io.IOException, XMLStreamException {
        inputFactory = XMLInputFactory.newInstance();
        xmlEventReader = inputFactory.createXMLEventReader(r);
    }

    public void close() throws XMLStreamException {
        if (xmlEventReader != null) xmlEventReader.close();
    }

    public WikiArticle next() throws XMLStreamException {
        StringBuilder sb = new StringBuilder();

        while (xmlEventReader.hasNext()){
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isStartElement() && xmlEvent.asStartElement().getName().getLocalPart().equals("text")){
                inText = true;
            } else if (xmlEvent.isCharacters() && inText){
                sb.append(xmlEvent.asCharacters().getData());;
            } else if (xmlEvent.isEndElement() && xmlEvent.asEndElement().getName().getLocalPart().equals("text")){
                inText = false;
                return new WikiArticle(sb.toString());
            } else {
                if (inText && xmlEvent.isStartElement()) throw new RuntimeException("Format error");
                if (inText && xmlEvent.isEndElement())   throw new RuntimeException("Format error");
            }
        }

        return null;
    }


    @Override
    public Iterator<WikiArticle> iterator() {
        return new ArticleIterator(this);
    }

    static class ArticleIterator implements Iterator<WikiArticle> {
        final WikiDump dump;
        WikiArticle prepared = null;

        ArticleIterator(WikiDump aDump) {
            dump = aDump;
        }

        @Override
        public boolean hasNext() {
            prepare();
            return prepared != null;
        }

        private void prepare() {
            if (prepared != null) return;

            try {
                prepared = dump.next();
            }
            catch(XMLStreamException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public WikiArticle next() {
            prepare();
            WikiArticle tmp = prepared;
            prepared = null;
            return tmp;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }






}
