package morph.io.wiki;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import morph.util.PatternIterator;
import util.col.Cols;

/**
 *
 * @author Jirka Hana
 */
        /*
         * [[..|X]] -> X
         * [[X]] -> X
         * [..] -> null
         * [[..:..]] -> null
         * omit:
         * {{ .. }}
         * <!-- .. -->
         * <ref>..</ref>
         * <..>
         * </..>
         * ==
         * ===
         * ====
         * '''
         * ''
         * *
         *
         * translate:
         * &...
         */

// @todo [[1984]]a goda -> 1894a goda
// @todo cleanup -- replace all omits with spaces then tokenize
public class WikiArticle implements Iterable<String> {

    private final String article;

    public WikiArticle(String aStr) {
        StringBuilder sb = new StringBuilder(aStr);
        deleteRange(sb, "{{", "}}", true);
        deleteRange(sb, "{", "}", true);
        deleteRange(sb, "<!--", "-->", true);
        deleteRange(sb, "<ref>", "</ref>", true);
        delete(sb, "====", "===", "==", "*", "'''", "''");
        // todo: [[1941 ???]]?
        handleLinks(sb);
        article = sb.toString();
    }

    public @Override Iterator<String> iterator() {
        Pattern p = tokenizingPattern();
        return new PatternIterator(article, p);
    }


    private static Pattern tokenizingPattern() {
        String cp = "[\\p{javaLowerCase}\\p{javaUpperCase}]";
        String wordp = cp + "+(\\-" + cp + "+)*";
        List<String> tokens = Arrays.asList(wordp, "[\\-\\$]?\\d+(\\-?" + cp + "+)?", "\\.\\.\\.", "\\p{P}");
        String tokensp = Cols.toString(tokens, "(", ")", ")|(", "");
        return Pattern.compile(tokensp);
    }

    private void deleteRange(StringBuilder aSb, String aFrom, String aTo, boolean aReqEnd) {
        int end = 0;

        for (;;) {
            int start = aSb.indexOf(aFrom, end);
            if (start == -1) {
                return;
            }
            end = aSb.indexOf(aTo, start + 1);
            if (end == -1) {
                if (aReqEnd) {
                    return;
                } else {
                    end = aSb.length();
                }
            }
            else {
                end += aTo.length();
            }

            setToSpaces(aSb, start, end);
        }
    }

    private void setToSpaces(StringBuilder aSb, int aFrom, int aTo) {
        setCharsTo(aSb, aFrom, aTo, ' ');
    }

    private void setCharsTo(StringBuilder aSb, int aFrom, int aTo, char aChar) {
        for (int i = aFrom; i < aTo; i++) {
            aSb.setCharAt(i, aChar);
        }
    }

    private void delete(StringBuilder aSb, String... aStrs) {
        for (String str : aStrs) {
            delete(aSb, str);
        }
    }

    private void delete(StringBuilder aSb, String aStr) {
        int end = 0;

        for (;;) {
            int start = aSb.indexOf(aStr, end);
            if (start == -1) {
                return;
            }
            end = start + aStr.length();

            setCharsTo(aSb, start, end, ' ');
        }
    }

    public @Override String toString() {
        return article;
    }

    private boolean in(String aToken, String... aStrs) {
        return Arrays.asList(aStrs).contains(aToken);
    }

    /*
     * [[..|X]] -> X
     * [[X]] -> X
     * [..] -> null
     * [[..:..]] -> null
     * http:....
     */
    private void handleLinks(StringBuilder aSb) {
        int end = 0;

        for (;;) {
            int start = aSb.indexOf("[", end);
            if (start == -1) {
                break;
            }
            end = aSb.indexOf("]", start + 1);    // note: the first ] if they are ]]
            if (end == -1) {
                break;
            }
            end += 1;   // to make it exclusive index

            // [...] -- delete
            if (aSb.charAt(start + 1) != '[') {
                setToSpaces(aSb, start, end);
            } // [[...]]
            else {
                end++;  // move after the second bracket
                if (end > aSb.length()) return;
                String subStr = (String) aSb.subSequence(start, end);
                // [[..:..]] - delete
                if (subStr.indexOf(':') != -1) {
                    setToSpaces(aSb, start, end);
                } // [[...]] or [[..|..]]
                else {
                    //System.out.println("subStr: " + subStr);
                    int prefixEnd = subStr.indexOf('|');
//                        System.out.println("  prefixEnd: " + prefixEnd);
                    prefixEnd = start + (prefixEnd == -1 ? 1 : prefixEnd);
//                        System.out.println("  prefixEnd: " + prefixEnd);
                    setToSpaces(aSb, start, prefixEnd + 1);      // delete [[..| or [[
//                        System.out.println("  >" + aSb + "<");
                    setToSpaces(aSb, end - 2, end);      // ]]

                    // '[[1948]]a' --> '  1948  a' ==> '  1948a  '
                    for (int i = end; i < aSb.length(); i++) {
                        if (!Character.isLetter(aSb.charAt(i)) ) break;
                        aSb.setCharAt(i-2, aSb.charAt(i));
                        aSb.setCharAt(i, ' ');
                    }

                }
            }
        }

        // http
        deleteRange(aSb, "http:", " ", false);
    }

}

