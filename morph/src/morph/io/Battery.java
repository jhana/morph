package morph.io;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;

/**
 * Synchronized battery of readers. 
 * (Forms must match)
 * @todo make an object
 */
public final class Battery {

    public static List<LineReader> openLineReaders(Iterable<XFile> aFiles) throws java.io.IOException {
        final List<LineReader> rs = new ArrayList<LineReader>();
        try {
            for (XFile file : aFiles) {
                rs.add(IO.openLineReader(file));
            }
            return rs;
        } catch (IOException e) {
            IO.close(rs);
            throw e;
        }
    }

    // todo if exception happens; close what has been opened
    public static List<PrintWriter> openPrintWriters(Iterable<XFile> aFiles) throws java.io.IOException {
        final List<PrintWriter> ws = new ArrayList<PrintWriter>();
        try {
            for (XFile file : aFiles) {
                ws.add(IO.openPrintWriter(file));
            }
            return ws;
        } catch (IOException e) {
            IO.close(ws);
            throw e;
        }
    }

    public static PrintWriter[] openPrintWriters(XFile[] aFiles) throws java.io.IOException {
        final PrintWriter[] ws = new PrintWriter[aFiles.length];
        try {
            for (int i = 0; i < aFiles.length; i++) {
                ws[i] = IO.openPrintWriter(aFiles[i]);
            }
            return ws;
        } catch (IOException e) {
            IO.close(ws);
            throw e;
        }
    }

    public static List<TntWriter> tntWriterBattery(Iterable<util.io.XFile> aFiles) throws java.io.IOException {
        final List<TntWriter> ws = new ArrayList<TntWriter>();
        try {
            for (XFile file : aFiles) {
                ws.add(new TntWriter(file));
            }
            return ws;
        } catch (IOException e) {
            IO.close(ws);
            throw e;
        }
    }

    public static TntWriter[] tntWriterBattery(util.io.XFile[] aFiles) throws java.io.IOException {
        final TntWriter[] ws = new TntWriter[aFiles.length];
        try {
            for (int i = 0; i < aFiles.length; i++) {
                ws[i] = new TntWriter(aFiles[i]);
            }
            return ws;
        } catch (IOException e) {
            IO.close(ws);
            throw e;
        }
    }

    /**
     * Closes a battery (an array of closeables - readers, writers).
     */
    public static void close(Closeable[] aCs) throws java.io.IOException {
        IO.close(aCs);
    }

    /**
     * Closes a battery (a list of closeables - readers, writers).
     */
    public static void close(Iterable<? extends Closeable> aCs) throws java.io.IOException {
        IO.close(aCs);
    }
}
