package morph.io;

import util.io.*;
import java.io.*;
import java.util.Arrays;
import java.util.regex.*;
import morph.ma.lts.Fplus;
import morph.ma.lts.Lts;
import morph.ts.PdtTokenizer;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.err.Err;
import util.opt.Options;

/**
 * Note: unqoutes (via the tokenizer) HTML4 entities.
 * @todo? two classes (+ a common superclass) one for disambiguated one for normal (share interface with TNT)
 * @todo? inits
 */
public final class PdtLineReader extends CorpusLineReader {

    /**
     * Used to tokenize lemma.
     */
    private final static Pattern cLemmaSeps = Pattern.compile("[\\_\\-]");

    public enum LineType implements LineTypeI {

        form, punct, sentence, other, error;

        public boolean form() {
            return this == form;
        }

        public boolean punct() {
            return this == punct;
        }

        public boolean sentence() {
            return this == sentence;
        }

        public boolean other() {
            return this == other;
        }

        public boolean error() {
            return this == error;
        }   // todo doc, p

        public boolean fd() {
            return this == form || this == punct;
        }

        // synonym
        public boolean f() {
            return this == form;
        }

        public boolean d() {
            return this == punct;
        }

        public boolean s() {
            return this == sentence;
        }
    };

    private LineType mLineType;    // --- Configuration ---
    //boolean mOnlyFDLines = true;
    private String mLemmaTag = "l";
    private String mTagTag = "t";

    // --- Parser data structure --- (only one parser can run at a time)
    //int mCur;
    //String mTagStart;
    //int mTagStartLen;
// -----------------------------------------------------------------------------
// Configuration
// -----------------------------------------------------------------------------
    /**
     * Defaults:
     * <ul>
     * <li>Reading only FDLines (see setOnlyFDLines(boolean))
     * <li>filling lemma and tag (see setMm(false))
     * </ul>
     */
    public PdtLineReader(XFile aFile) throws java.io.IOException {
        super(aFile);
    }

    @Override
    public CorpusLineReader setOptions(Options options) {
        super.setOptions(options);
            
        setLemmaTag(options.getString("lemmaTag", mLemmaTag));      // todo only prefix 'MM' + l/t, MD + l/t, ...
        setTagTag(options.getString("tagTag", mTagTag));
        
        return this;
    }

    
    
    public PdtLineReader setLemmaTag(String aTagCore) {
        mLemmaTag = aTagCore;
        return this;
    }

    public PdtLineReader setTagTag(String aTagCore) {
        mTagTag = aTagCore;
        return this;
    }

    @Override
    public PdtLineReader setOnlyTokenLines(boolean aOnlyFDLines) {
        return (PdtLineReader) super.setOnlyTokenLines(aOnlyFDLines);
    }

    /**
     * Are MMl/t tags or l/t tags used?
     *
     * @param aMm
     *   if true:  <MMl> for ambiguous leammas, <MMt> for ambiguous tags;
     *   if false: <l> for lemma, <t> for tag.
     */
    @Override
    public PdtLineReader setMm(boolean aMm) {
        super.setMm(aMm);
        if (aMm) {
            mLemmaTag = "MMl";
            mTagTag = "MMt";
        }
        else {
            mLemmaTag = "l";
            mTagTag = "t";
        }

        return (PdtLineReader) this;
    }

// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------
    @Override
    public LineType lineType() {
        return mLineType;
    }

    @Override
    public boolean isTokenLine() {
        return mLineType.fd();
    }

    /**
     * Reads the next line from the Pdt file.
     *
     * @return false once EOF was reached or an IO error occured; true otherwise
     */
    @Override
    public boolean readLine() {
        fplus = null;

        do {
            try {
                line = r.readLine();
            } catch (IOException e) {
                return false;
            }
            if (line == null) {
                return false;
            }
            line = line.trim();

            // @todo optimize
            if (line.startsWith("<f>") || line.startsWith("<f ")) {
                mLineType = LineType.form;
                fillFLT();
            } else if (line.startsWith("<d>") || line.startsWith("<d ")) {
                mLineType = LineType.punct;
                fillFLT();
            } else if (line.startsWith("<s>") || line.startsWith("<s ")) {
                mLineType = LineType.sentence;
            } else {
                mLineType = LineType.other;
            }
        } while (onlyTokenLines && !lineType().fd());

        return true;
    }

    /**
     * Gets the word part of the lemma.
     */
    public String lemmaWord() {
        final String lemma = fplus.getLts().lt().lemma();

        final String[] parts = cLemmaSeps.split(lemma);
        if (parts.length > 0 && parts[0].length() > 0) {
            return parts[0];
        } else {
            return lemma;   // either no decorations or '-' or '_'
        }
    }

    /**
     * Fills the form and possibly lemma, and/or tag fields.
     *
     * @param aFormTagCore is the core of the SGML tag marking a form
     *    (usually f for words and d for punctuation)
     */
    private void fillFLT() {
        final PdtTokenizer tok = new PdtTokenizer(line);

        tok.next();

        Err.fAssert(tok.next().text(), r, "Expecting form, found %s", tok.token());
        fplus = new Fplus(tok.token());

//        skipTill(tok, mLemmaTag);
//        for (;;) {
//            if (tok.tokenType().eof()) break;
//
//            // todo opt extract info
//            Err.fAssert(tok.next().text(), mR, "Expecting lemma, found %s", tok.token());
//            final String lemma = tok.token();
//
//            for (;;) {
//                skipTill(tok, mTagTag, mLemmaTag); // read till
//                if (tok.tokenType().eof()) break;
//                if (tok.tag().getCore().equals(mLemmaTag)) break;
//
//                Err.fAssert(tok.next().text(), mR, "Expecting tag, found %s", tok.token());
//                final Tag tag = Tagset.getDef().fromString(tok.token());
//                fplus.getLts().add(lemma, tag, null, null);
//            }
//        }

        String lemma = null;
        for (;;) {
            skipTill(tok, mTagTag, mLemmaTag); // read till
            if (tok.tokenType().eof()) break;

            if (tok.tag().getCore().equals(mLemmaTag)) {
                Err.fAssert(tok.next().text(), r, "Expecting lemma, found %s", tok.token());
                lemma = tok.token();
            }
            else if (tok.tag().getCore().equals(mTagTag)) {
                ///final Info info = tok.tag().getAttribute("info"); Info - would need to be parsed
                final String src = tok.tag().getAttribute("src");
                Err.fAssert(tok.next().text(), r, "Expecting tag, found %s", tok.token());
                final Tag tag = Tagset.getDef().fromString(tok.token());
                Lts lts = fplus.getLts();
                Err.fAssert(lts != null, r, "Cannot extract lts, fplus=%s", fplus);
                try {
                    fplus.getLts().add(lemma, tag, src, null);
                }
                catch(Exception e) {
                    Err.fAssert(false, r, "Error!!");
                }
            }
            else {
                // should not happend
            }
        }

    }

    // @todo to util move to PdtTokenizer
    private void skipTill(final PdtTokenizer tok, final String aTagCore) {
        for (;;) {
            if (tok.next().eof()) break;
            if (tok.tokenType().tag() && tok.tag().getCore().equals(aTagCore)) break;
        }
    }

    private void skipTill(final PdtTokenizer tok, final String ... aTagCores) {
        for (;;) {
            if (tok.next().eof()) break;
            if (tok.tokenType().tag() && Arrays.asList(aTagCores).contains(tok.tag().getCore())) break;
        }
    }


    //@todo can extract <doc>when asking for d tag (<d)
    //copy from DA/LAW/..
//    public String extractByCore(String aTagCore) {
//        return extractByCore(aTagCore, 0);
//    }

    public String extractByCore(String aTagCore, int aStartIdx) {
        int tagStartIndex = line.indexOf('<' + aTagCore, aStartIdx);
        if (tagStartIndex == -1) {
            return null;
        }
        int tagEndIndex = line.indexOf('>', tagStartIndex + 1);
        assert tagEndIndex != -1 : aTagCore + " : " + line;

        int endIndex = line.indexOf('<', tagEndIndex + 1);

        return (endIndex != -1) ? line.substring(tagEndIndex + 1, endIndex) : line.substring(tagEndIndex + 1).trim();
    }

//    public String extract(String aTag) {
//        int startIndex = mLine.indexOf(aTag);
//        if (startIndex == -1) {
//            return null;
//        }
//        startIndex += aTag.length();
//
//        int endIndex = mLine.indexOf("<", startIndex);
//
//        return (endIndex != -1) ? mLine.substring(startIndex, endIndex) : mLine.substring(startIndex).trim();
//    }

    public String getFirstLemmaOf(Tag aTag) {
        int tagIdx = line.indexOf(aTag.toString());
        int lemmaIdx = line.lastIndexOf('<' + mLemmaTag, tagIdx);
        return extractByCore(mLemmaTag, lemmaIdx);
    }

//    public List<Tag> extractTags(String aSgmlTag) {
//        return Tags.fromStrings(extractValues(aSgmlTag));
//    }
//
//    public List<String> extractValues(String aTagCore) {
//        List<String> values = new ArrayList<String>();
//        mTagStart = '<' + aTagCore;
//        mTagStartLen = mTagStart.length();
//
//        for (mCur = 0; mCur != -1;) {
//            String value = nextVal();
//            if (value == null) {
//                break;
//            }
//            values.add(value);
//        }
//        return values;
//    }

//    protected String nextVal() {
//        // --- find the next tag ---
//        for (;;) {
//            mCur = mLine.indexOf(mTagStart, mCur);
//            if (mCur == -1) {
//                return null;
//            }
//            char nextChar = mLine.charAt(mCur + mTagStartLen);
//            if (nextChar == '>' || nextChar == ' ') {
//                break;
//            }
//            mCur++;
//        }
//
//        mCur = mLine.indexOf('>', mCur + mTagStartLen) + 1;
//        assertE(mCur != 0, "Wrong format (no closing >)");
//
//        // --- get the value following the tag ---
//        int endIndex = mLine.indexOf('<', mCur);
//        String tmp = (endIndex != -1) ? mLine.substring(mCur, endIndex) : mLine.substring(mCur).trim();
//
//        mCur = endIndex;
//
//        return tmp;
//    }
 }

