package morph.io;

import util.io.*;
import java.io.*;
import java.util.regex.Pattern;
import lombok.Getter;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.io.IO;
import util.str.Strings;

/**
 *
 * @author  Jirka Hana
 */
public class ReaderParser implements Closeable {
    final @Getter LineReader reader;

    @Getter PushBackTokenizer tokenizer;
    
    final @Getter String fileId;
    
    final @Getter Filter filter;
    
    public ReaderParser(LineReader aR, Filter aFilter, String aFileId) throws java.io.IOException  {
        reader = aR;
        filter = aFilter;
        fileId = aFileId;
    }
    
    public ReaderParser(Iterable<XFile> aFileNames, Filter aFilter, String aFileId) throws java.io.IOException  {
        this(IO.openLineReader(aFileNames), aFilter, aFileId);
    }

    /** Creates a new instance of ReaderParser */
    public ReaderParser(XFile aFileName, Filter aFilter, String aFileId) throws java.io.IOException  {
        this(IO.openLineReader(aFileName), aFilter, aFileId);
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }

    public String nextLine() throws IOException {
        for(;;) {
            String line = reader.readNonEmptyLine();
            if (line == null) {
                reader.close();
                return null;
            }

            line = removeComments(line).trim();
            if (line.equals("")) continue;

            tokenizer = new PushBackTokenizer(line, Strings.cWhiteSpaceChars, false, reader, fileId);

            if (tokenizer.hasMoreTokens()) return line;
        }
    }

    // todo more effective
    public static String removeComments(String aLine) {
        for(;;) {
            int comment1Idx = aLine.indexOf("//");
            int commentIdx  = aLine.indexOf("/*");

            // "//" precedes "/* ... */"
            if (comment1Idx != -1 && (commentIdx == -1 || comment1Idx < commentIdx) ) {
                return aLine.substring(0, comment1Idx);
            }

            if (commentIdx == -1) return aLine;

            int endCommentIdx = aLine.indexOf("*/");
            if (endCommentIdx != -1 && (endCommentIdx+2 < aLine.length()) )
                aLine = aLine.substring(0, commentIdx) + aLine.substring(endCommentIdx+2);
            else
                aLine = aLine.substring(0, commentIdx);
        }
    }



    public boolean hasMoreTokens() {
        return tokenizer.hasMoreTokens();
    }    

    
    public void skipUntil(String aLinePrefix) throws IOException { 
        String line;
        do {
             line = reader.readLine();
        } while (line != null && line.startsWith(aLinePrefix));
    }

    public void skipUntil(Pattern aPattern) throws IOException { 
        String line;
        do {
             line = reader.readLine();
        } while (line != null && aPattern.matcher(line).matches());
    }
    
    public String line() {
        return tokenizer.string();
        
    }
    
    public int getLineNumber() {
        return reader.getLineNumber();
    }
    
// --- Getting specific tokens -------------------------------------------------

    public String getString(String aId) {
        return tokenizer.getString(aId);
    }
    
    public String getStringF(String aId) {
        return filter.filter(tokenizer.getString(aId));
    }

    public Tag getTag(String aId)  {
        return tokenizer.getTag("tag");
    }

    public Tag getTag(String aId, String aTagTmpl)  {
        return Tagset.getDef().fromAbbr(tokenizer.getString(aId), aTagTmpl);
    }

    public Tag getTag(String aId, String aTagTmpl, String aNegationOk)  {
        return Tagset.getDef().fromAbbr(tokenizer.getString(aId), aTagTmpl, aNegationOk);
    }
    
    
    /**
     * Like getTag, but automatically adds hyphens to the end if they are missing.
     */
    public Tag getTagFill(String aId)  {
        return tokenizer.getTagFill("tag");
    }


    public int getInt(String aId) {
        return tokenizer.getInt(aId);
    }

    public char getChar(String aId) {
        return tokenizer.getChar(aId);
    }

    public boolean getBoolean(String aId) {
        return tokenizer.getString(aId).equals("1");
    }
// --- -------------------------------------------------------------------------

    public void eat(String aToken) {
        tokenizer.eat(aToken);
    }
    
    /**
     * Checks whether the next token is specified token. 
     * It is eaten if it is, otherwise the cursor does not move.
     *
     * @param  aToken the token to be checked
     * @return <code>true</code> if the token is <tt>aToken</tt>; 
               <code>false</code> otherwise (i.e. other token or no token at all)
     */
    public boolean eatIf(String aToken) {
        return tokenizer.eatIf(aToken);
    }
    
    public boolean isNextToken(String aToken) {
        return tokenizer.isNextToken(aToken);
    }

    public String nextToken() {
        return tokenizer.nextToken();
    }
    
    public boolean nextTokenStartsWith(String aTokenStart) {
        return tokenizer.nextTokenStartsWith(aTokenStart);
    }

    public boolean nextTokenMatches(Pattern aPattern) {
        return tokenizer.nextTokenMatches(aPattern);
    }
    

    
// --- Error reporting ---------------------------------------------------------

    public String msg(String aMsg, Object ... aObjs) {
        return tokenizer.msg("", aMsg, aObjs);
    }
    
    public String warning(String aMsg, Object ... aObjs) {
        return tokenizer.warning(aMsg, aObjs);
    }

    public String error(String aMsg, Object ... aObjs) {
        return tokenizer.error(aMsg, aObjs);
    }
    
// --- Filtering ---------------------------------------------------------------
}



        
