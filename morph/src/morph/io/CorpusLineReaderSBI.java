package morph.io;

public interface CorpusLineReaderSBI extends java.io.Closeable {
    boolean readLine() throws java.io.IOException;

    @Override
    void close() throws java.io.IOException;

    public LineTypeI lineType();
    public boolean isTokenLine();

    public String form();
}

