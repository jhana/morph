package morph.io;

import java.util.*;
import java.io.*;
import util.io.*;
import util.*;

/**
 *
 * @author  Jiri
 */
public class TokWordReader extends AbstractWordReader {
    StreamTokenizer mRR; 
    String mIgnoreChars;
    
    /** Creates a new instance of TokWordReader */
    public TokWordReader(XFile aFile, Filter aFilter) throws IOException {
        super(aFile, aFilter);

        mRR = new StreamTokenizer(mR);         
        mRR.resetSyntax();
	mRR.whitespaceChars(0, ' ');
        mRR.whitespaceChars(' ', ' ');
        mRR.wordChars('0', '9');
        mRR.wordChars('a', 'z');
	mRR.wordChars('A', 'Z');
	mRR.wordChars(128 + 32, 255);

        setPunctChars("\".,!?_:");
        setWordChars("-~\'");
//        r.whitespaceChars(' ', ' ');
//        r.whitespaceChars('\"', '\"');
//        r.whitespaceChars('.', '.');
//        r.whitespaceChars(',', ',');
//        r.whitespaceChars('-', '-');
//        r.whitespaceChars('!', '!');
//        r.whitespaceChars('?', '?');
//        r.whitespaceChars('_', '_');
//        r.wordChars('~', '~');
//        r.wordChars('\'', '\'');

//	commentChar('/');
//	quoteChar('"');
//	quoteChar('\'');
//	parseNumbers();
    
    }

    /**
     * 
     * Sets the characters to be ordinary chars - each is returned as a single word.
     */
    public void setPunctChars(String aPunctChars) {
        for (char c : aPunctChars.toCharArray()) {  // not very effective
            mRR.ordinaryChar(c);
        }
    }
    
    /**
     * 
     * Sets the characters to be word chars - they are port of words (as letters)
     */
    public void setWordChars(String aWordChars) {
        for (char c : aWordChars.toCharArray()) {  // not very effective
            mRR.wordChars(c, c);
        }
    }
    
    /**
     * 
     * Sets the characters to be word chars - they are port of words (as letters)
     */
    public void ignoreChars(String aIgnoreChars) {
        mIgnoreChars = aIgnoreChars;
    }

    
    public boolean nextWord() {
        try {
            for(;;) {
                int ttype = mRR.nextToken();
                if (ttype == mRR.TT_EOF) {mWord = null; return false;}
                if (ttype == mRR.TT_WORD) {
                    mWord = (mFilter == null) ? mRR.sval : mFilter.filter(mRR.sval);
                    mWords++;
                    return true;
                }
                else if (mIgnoreChars.indexOf((char)mRR.ttype) == -1) {
                    mWord = String.valueOf((char)mRR.ttype);
                    if (mFilter != null) mWord = mFilter.filter(mWord);
                    mWords++;
                    return true;
                }
            }
        }
        catch(IOException e) {
            System.out.println(e);
            return false;
        }
    }
    
}
