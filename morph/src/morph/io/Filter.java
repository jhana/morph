package morph.io;

/**
 * Filter used by ReaderParse to translate incomming strings.
 * @todo change to the standard reader chaining
 */
public interface Filter {
    public String filter(String aString);

    public final static Filter cIdentityFilter = 
        new Filter() {
            public final String filter(String aString) {return aString;}
        };
}
