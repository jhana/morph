package morph.io;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class WordReaderList implements WordReader {
    private final List<String> words;
    private int idx;

    public WordReaderList(String ... aWords) {
        this(Arrays.asList(aWords));
    }
    public WordReaderList(List<String> aWords) {
        words = aWords;
        idx = -1;
    }

    @Override
    public void close()  {
    }

    @Override
    public boolean nextWord()  {
        idx++;
        return idx < words.size();
    }

    @Override
    public String word() {
        return words.get(idx);
    }

}
