package morph.io;

import util.io.*;
import java.util.regex.Pattern;
import morph.ma.lts.Fplus;
import morph.ma.lts.Lt;
import morph.ts.Tagset;
import util.XmlStr;


/**
 * todo configurable
 * @author jirka
 */
public class VerticalOczPlainReader extends CorpusLineReader {
    protected final static Pattern cBarPattern = Pattern.compile("\\|");
    

    /**
     * Reads forms only.
     * Does not fill tags.
     */
    public static VerticalOczPlainReader open(XFile aFile) throws java.io.IOException {
        return new VerticalOczPlainReader(aFile);
    }

    /**
     * Reads disambiguated files (only one tag).
     * Does not fill tags.
     */
    public static VerticalOczPlainReader openM(XFile aFile) throws java.io.IOException {
        return new VerticalOczPlainReader(aFile);
    }

    /**
     * Reads morphologically analyzed (possibly more than one tags).
     * Fills tags does not fill tag.
     */
    public static VerticalOczPlainReader openMm(XFile aFile) throws java.io.IOException {
        return new VerticalOczPlainReader(aFile);
    }


    /**
     * In most cases, aReadTag and aReadTags are not true at the same time.
     * @todo remove and bool replace with set...
     */
    private VerticalOczPlainReader(XFile aFile) throws java.io.IOException {
        super(aFile);
    }




    /**
     * Reads the next form block from the file.
     * Ignores empty lines, including white-space only lines.
     * Returns false once EOF was reached; true otherwise
     *
     * todo optionally require tag/lemma
     */
    @Override
    public boolean readLine() throws java.io.IOException {
        fplus = null;
        do {
            do {
                line = r.readLine();
                if (line == null) return false;
                line = line.trim();
            } while ( line.length() == 0 );
        } while (onlyTokenLines && XmlStr.isTag(line));

        extractFplust();

        return true;
    }

    // form tag+
    private void extractFplust() {
        final String[] tokens = cWhiteSpacePattern.split(line);
        final String form = tokens[0];
        fplus = new Fplus(form);

//        for(int i = 1; i < tokens.length; i+=2) {
//            final String[] tags = cBarPattern.split(tokens[i]);
//            final String lemma = tokens[i+1];
//
//            for(String tag : tags) {
//                fplus.getLts().add(new Lt(lemma, Tagset.getDef().fromString(tag)));
//            }
//        }
    }
}

