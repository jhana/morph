package morph.io;

import java.io.*;
import util.io.*;
import util.str.Strings;

/**
 * Every line contains exactly one word.
 * @author  Jirka Hana
 */
public class LineWordReader extends AbstractWordReader {
    private boolean firstWordOnly = true;
    
    
    /** 
     * Creates a new instance of LineWordReader.
     */
    public LineWordReader(XFile aFileName, Filter aFilter) throws IOException {
        super(aFileName, aFilter);
//        System.out.println("LineWordReader");
    }

    public LineWordReader(Reader aReader, Filter aFilter) throws IOException {
        super(aReader, aFilter);
    }
    
    /**
     * Skips trims leading and trailing spaces, skips empty lines and comments (slash* ... *slash)
     */
    @Override
    public boolean nextWord() throws java.io.IOException {
        do {
            mWord = mR.readLine();
            if (mWord == null) return false;
            mWord = mWord.trim();
        } while (mWord.equals("") || (mWord.charAt(0) == '/' && mWord.startsWith("/*") && mWord.endsWith("*/") ) );
        
//        mWord = Strings.cWhitespacePattern.split(mWord)[0];
        //System.out.println("mWord=" + mWord);
        if (firstWordOnly) {
            mWord = Strings.cWhitespacePattern.split(mWord,2)[0];
        }
        
        if (mFilter != null) {
            mWord = mFilter.filter(mWord);
        }
        mWords++;
        return true;
    }

    
}
