package morph.io;

public interface CorpusLineReaderI extends CorpusLineReaderSBI {
    public String line() ;
}
