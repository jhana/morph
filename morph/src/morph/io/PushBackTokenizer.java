
package morph.io;

import java.util.*;
import java.io.*;
import java.util.regex.Pattern;
import lombok.Getter;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.str.Strings;

/**
 * The string tokenizer class allows an application to break a
 * string into tokens. The tokenization method is much simpler than
 * the one used by the <code>StreamTokenizer</code> class. The
 * <code>StringTokenizer</code> methods do not distinguish among
 * identifiers, numbers, and quoted strings, nor do they recognize
 * and skip comments.
 * <p>
 * The set of delimiters (the characters that separate tokens) may
 * be specified either at creation time or on a per-token basis.
 * <p>
 * An instance of <code>StringTokenizer</code> behaves in one of two
 * ways, depending on whether it was created with the
 * <code>returnDelims</code> flag having the value <code>true</code>
 * or <code>false</code>:
 * <ul>
 * <li>If the flag is <code>false</code>, delimiter characters serve to
 *     separate tokens. A token is a maximal sequence of consecutive
 *     characters that are not delimiters.
 * <li>If the flag is <code>true</code>, delimiter characters are themselves
 *     considered to be tokens. A token is thus either one delimiter
 *     character, or a maximal sequence of consecutive characters that are
 *     not delimiters.
 * </ul><p>
 * A <tt>StringTokenizer</tt> object internally maintains a current
 * position within the string to be tokenized. Some operations advance this
 * current position past the characters processed.<p>
 * A token is returned by taking a substring of the string that was used to
 * create the <tt>StringTokenizer</tt> object.
 * <p>
 * The following is one example of the use of the tokenizer. The code:
 * <blockquote><pre>
 *     StringTokenizer st = new StringTokenizer("this is a test");
 *     while (st.hasMoreTokens()) {
 *         System.out.println(st.nextToken());
 *     }
 * </pre></blockquote>
 * <p>
 * prints the following output:
 * <blockquote><pre>
 *     this
 *     is
 *     a
 *     test
 * </pre></blockquote>
 *
 * <p>
 * <tt>StringTokenizer</tt> is a legacy class that is retained for
 * compatibility reasons although its use is discouraged in new code. It is
 * recommended that anyone seeking this functionality use the <tt>split</tt>
 * method of <tt>String</tt> or the java.util.regex package instead.
 * <p>
 * The following example illustrates how the <tt>String.split</tt>
 * method can be used to break up a string into its basic tokens:
 * <blockquote><pre>
 *     String[] result = "this is a test".split("\\s");
 *     for (int x=0; x&lt;result.length; x++)
 *         System.out.println(result[x]);
 * </pre></blockquote>
 * <p>
 * prints the following output:
 * <blockquote><pre>
 *     this
 *     is
 *     a
 *     test
 * </pre></blockquote>
 *
 * @author  unascribed
 * @version 1.29, 01/23/03
 * @see     java.io.StreamTokenizer
 * @since   JDK1.0
 */
public class PushBackTokenizer implements Enumeration {
    protected final String mStr;

    protected int mCurrentPosition;
    protected int mNewPosition;
    protected int mMaxPosition;
    protected int mOldPosition;
    
    @Getter protected String delimiters;
    protected boolean retDelims;
    protected boolean delimsChanged;

    /** 
     * For error mesages, in case this was read from a reader
     */
    public LineNumberReader mReader;

    /** 
     * For error mesages, in case this was read from a file
     */
    public String mFileId;

    
    /**
     * maxDelimChar stores the value of the delimiter character with the
     * highest value. It is used to optimize the detection of delimiter
     * characters.
     */
    private char maxDelimChar;
    
    
    /**
     * Constructs a string tokenizer for the specified string. All
     * characters in the <code>delim</code> argument are the delimiters
     * for separating tokens.
     * <p>
     * If the <code>returnDelims</code> flag is <code>true</code>, then
     * the delimiter characters are also returned as tokens. Each
     * delimiter is returned as a string of length one. If the flag is
     * <code>false</code>, the delimiter characters are skipped and only
     * serve as separators between tokens.
     * <p>
     * Note that if <tt>delim</tt> is <tt>null</tt>, this constructor does
     * not throw an exception. However, trying to invoke other methods on the
     * resulting <tt>PushBackTokenizer</tt> may result in a <tt>NullPointerException</tt>.
     *
     * @param  aStr            a string to be parsed.
     * @param  aDelim          the delimiters.
     * @param  aReturnDelims   flag indicating whether to return the delimiters as tokens.
     * @param  aReader
     * @param  aFileId
     */
    public PushBackTokenizer(String aStr, String aDelim, boolean aReturnDelims, LineNumberReader aReader, String aFileId) {
        mStr = aStr;
        setDelims(aDelim);
        delimsChanged = false;
        retDelims = aReturnDelims;
        mReader = aReader; 
        mFileId = aFileId;
        
        mCurrentPosition = 0;
        mNewPosition = -1;
        mOldPosition = -1;
        mMaxPosition = mStr.length();
    }
    
    /**
     * Constructs a string tokenizer for the specified string. All
     * characters in the <code>delim</code> argument are the delimiters
     * for separating tokens.
     * <p>
     * If the <code>returnDelims</code> flag is <code>true</code>, then
     * the delimiter characters are also returned as tokens. Each
     * delimiter is returned as a string of length one. If the flag is
     * <code>false</code>, the delimiter characters are skipped and only
     * serve as separators between tokens.
     * <p>
     * Note that if <tt>delim</tt> is <tt>null</tt>, this constructor does
     * not throw an exception. However, trying to invoke other methods on the
     * resulting <tt>PushBackTokenizer</tt> may result in a
     * <tt>NullPointerException</tt>.
     *
     * @param   aStr            a string to be parsed.
     * @param   aDelim          the delimiters.
     * @param   aReturnDelims   flag indicating whether to return the delimiters
     *                         as tokens.
     */
    public PushBackTokenizer(String aStr, String aDelim, boolean aReturnDelims) {
        this(aStr, aDelim, aReturnDelims, null, "");
    }

    /**
     * Constructs a string tokenizer for the specified string. The
     * characters in the <code>delim</code> argument are the delimiters
     * for separating tokens. Delimiter characters themselves will not
     * be treated as tokens.
     *
     * @param   mStr     a string to be parsed.
     * @param   delim   the delimiters.
     */
    public PushBackTokenizer(String mStr, String delim) {
        this(mStr, delim, false);
    }
    
    /**
     * Constructs a string tokenizer for the specified string. The
     * tokenizer uses the default delimiter set, which is
     * <code>"&nbsp;&#92;t&#92;n&#92;r&#92;f"</code>: the space character,
     * the tab character, the newline character, the carriage-return character,
     * and the form-feed character, the A0 character (non-breaking space) . Delimiter characters themselves will
     * not be treated as tokens.
     *
     * @param   mStr   a string to be parsed.
     */
    public PushBackTokenizer(String mStr) {
        this(mStr, Strings.cWhiteSpaceChars, false);
    }

    public final void setDelims(String aDelimChars) {
//        if (! aDelimChars.equals(Strings.cWhiteSpaceChars)) {
//            System.err.print("setting delimiters: ");
//            for (char c : aDelimChars.toCharArray()) {
//                System.err.print(" " + (int)c);
//            }
//            System.err.println();
//            new RuntimeException().printStackTrace();
//        }

        
        delimiters = aDelimChars;
        
        /* delimiter string specified, so set the appropriate flag. */
        delimsChanged = true;
        
        if (delimiters == null) {
            maxDelimChar = 0;
        }
        else {
            char m = 0;
            for (int i = 0; i < delimiters.length(); i++) {
                char c = delimiters.charAt(i);
                if (m < c)
                    m = c;
            }
            maxDelimChar = m;
        }
    }
    
    
    /**
     * Skips delimiters starting from the specified position. 
     * 
     * @return If retDelims is false, returns the index of the first non-delimiter 
     * character at or after startPos. 
     *         If retDelims is true, startPos is returned.
     */
    private int skipDelimiters(int startPos) {
        if (delimiters == null)
            throw new NullPointerException();
        
        int position = startPos;
        while (!retDelims && position < mMaxPosition) {
            char c = mStr.charAt(position);
            if ((c > maxDelimChar) || (delimiters.indexOf(c) < 0))
                break;
            position++;
        }
        return position;
    }

    public String string() {
        return mStr;
    }
    
    /**
     * Skips ahead from startPos and returns the index of the next delimiter
     * character encountered, or mMaxPosition if no such delimiter is found.
     */
    private int scanToken(int startPos) {
        int position = startPos;
        while (position < mMaxPosition) {
            char c = mStr.charAt(position);
            if ((c <= maxDelimChar) && (delimiters.indexOf(c) >= 0))
                break;
            position++;
        }
        if (retDelims && (startPos == position)) {
            char c = mStr.charAt(position);
            if ((c <= maxDelimChar) && (delimiters.indexOf(c) >= 0))
                position++;
        }
        return position;
    }
    
    /**
     * Tests if there are more tokens available from this tokenizer's string.
     * If this method returns <tt>true</tt>, then a subsequent call to
     * <tt>nextToken</tt> with no argument will successfully return a token.
     *
     * @return  <code>true</code> if and only if there is at least one token
     *          in the string after the current position; <code>false</code>
     *          otherwise.
     */
    public boolean hasMoreTokens() {
        // Temporary store this position and use it in the following nextToken()  (if the delimiters do not change before that)
        mNewPosition = skipDelimiters(mCurrentPosition);
        return (mNewPosition < mMaxPosition);
    }
    
    /**
     * Returns the next token from this string tokenizer.
     *
     * @return     the next token from this string tokenizer.
     * @exception  NoSuchElementException  if there are no more tokens in this
     *               tokenizer's string.
     */
    public String nextToken() {
        // Use mNewPosition iff it has been computed (in hasMoreElements()) and delimiters have not changed
        mCurrentPosition = (mNewPosition >= 0 && !delimsChanged) ? mNewPosition : skipDelimiters(mCurrentPosition);
        mOldPosition = mCurrentPosition;  // to allow push back
        
        // Reset these  
        delimsChanged = false;
        mNewPosition = -1;
        
        if (mCurrentPosition >= mMaxPosition)
            throw new NoSuchElementException();

        mCurrentPosition = scanToken(mCurrentPosition);         // position of the next delimiter (or the last char in this string)
        return mStr.substring(mOldPosition, mCurrentPosition);
    }
    
    /**
     * Returns the next token in this string tokenizer's string. First,
     * the set of characters considered to be delimiters by this
     * <tt>StringTokenizer</tt> object is changed to be the characters in
     * the string <tt>delim</tt>. Then the next token in the string
     * after the current position is returned. The current position is
     * advanced beyond the recognized token.  The new delimiter set
     * remains the default after this call.
     *
     * @param      delim   the new delimiters.
     * @return     the next token, after switching to the new delimiter set.
     * @exception  NoSuchElementException  if there are no more tokens in this
     *               tokenizer's string.
     */
    public String nextToken(String delim) {
        setDelims(delim);
        
        return nextToken();
    }

    /**
     * Returns the next token from this string tokenizer if it matches the
     * specified pattern.
     *
     * @param  aPatern regex pattern to match the next token against
     * @return the next token from this string tokenizer if it matches <tt>aPatern</tt>;
     *     null otherwise  (i.e. it does not match or no token at all)
     */
    public String nextTokenIf(final Pattern aPattern) {
        if (!hasMoreElements()) return null;
        final String token = nextToken();

        if (aPattern.matcher(token).matches()) {
            return token;
        }
        else {
            pushBack();
            return null;
        }
    }


    /**
     * Returns the cursor to the place where it was before reading the last token.
     */
    public void pushBack() {
        mCurrentPosition = mOldPosition;
        mNewPosition = mOldPosition;
    }
    
    /**
     * Eats the next token.  Throws exception if the token is different than specified.
     */
    public void eat(String aToken) {
        if (!getString(aToken).equals(aToken)) {
            reportTokenError(aToken);
            throw new NoSuchElementException();
        }
    }

    /**
     * Checks whether the next token is specified token. 
     * It is eaten if it is, otherwise the cursor does not move.
     *
     * @param  aToken the token to be checked
     * @return <code>true</code> if the token is <tt>aToken</tt>; 
               <code>false</code> otherwise (i.e. other token or no token at all)
     */
    public boolean eatIf(String aToken) {
        if (!hasMoreElements()) return false;
        if (nextToken().equals(aToken)) return true;
        
        pushBack();
        return false;
    }

    /**
     * Checks whether the next token is specified token. 
     * The token is not eaten.
     *
     * @param  aToken the token to be checked
     * @return <code>true</code> if the token is <tt>aToken</tt>; 
               <code>false</code> otherwise (i.e. other token or no token at all)
     */
    public boolean isNextToken(String aToken) {
        boolean tmp = eatIf(aToken);
        if (tmp) pushBack();    // for false it was pushed back in eatIf
        return tmp;
    }

    /**
     * Checks whether the next token starts with the specified string. 
     * The token is not eaten.
     *
     * @param  aTokenStart the required prefix of the next token
     * @return <code>true</code> if the next token starts with <tt>aTokenStart</tt>; 
               <code>false</code> otherwise (i.e. other token or no token at all)
     */
    public boolean nextTokenStartsWith(final String aTokenStart) {
        if (!hasMoreElements()) return false;
        final boolean result = nextToken().startsWith(aTokenStart);
        pushBack();

        return result;
    }

    /**
     * Checks whether the next token matches the specified pattern.
     * The token is not eaten.
     *
     * @param  aPatern regex pattern to match the next token against
     * @return <code>true</code> if the next token matches <tt>aPatern</tt>;
     *         <code>false</code> otherwise (i.e. it does not match or no token at all)
     */
    public boolean nextTokenMatches(final Pattern aPattern) {
        if (!hasMoreElements()) return false;
        final String token = nextToken();

        final boolean result = aPattern.matcher(token).matches();
        pushBack();

        return result;
    }

    
    
    /**
     * Returns the same value as the <code>hasMoreTokens</code>
     * method. It exists so that this class can implement the
     * <code>Enumeration</code> interface.
     *
     * @return  <code>true</code> if there are more tokens;
     *          <code>false</code> otherwise.
     * @see     java.util.Enumeration
     * @see     java.util.StringTokenizer#hasMoreTokens()
     */
    @Override
    public boolean hasMoreElements() {
        return hasMoreTokens();
    }
    
    /**
     * Returns the same value as the <code>nextToken</code> method,
     * except that its declared return value is <code>Object</code> rather than
     * <code>String</code>. It exists so that this class can implement the
     * <code>Enumeration</code> interface.
     *
     * @return     the next token in the string.
     * @exception  NoSuchElementException  if there are no more tokens in this
     *               tokenizer's string.
     * @see        java.util.Enumeration
     * @see        java.util.StringTokenizer#nextToken()
     */
    @Override
    public Object nextElement() {
        return nextToken();
    }
    
    /**
     * Calculates the number of times that this tokenizer's
     * <code>nextToken</code> method can be called before it generates an
     * exception. The current position is not advanced.
     *
     * @return  the number of tokens remaining in the string using the current
     *          delimiter set.
     * @see     java.util.StringTokenizer#nextToken()
     */
    public int countTokens() {
        int count = 0;
        int currpos = mCurrentPosition;
        while (currpos < mMaxPosition) {
            currpos = skipDelimiters(currpos);
            if (currpos >= mMaxPosition)
                break;
            currpos = scanToken(currpos);
            count++;
        }
        return count;
    }

// --- Getting specific tokens -------------------------------------------------
    
    /**
     * Like nextToken(), but repots an error if there is no token
     */
    public String getString(String aTokenId) {
        try {
            return nextToken();
        }
        catch (java.util.NoSuchElementException e) {
            reportTokenError(aTokenId);
            throw e;
        }
    }

    public Tag getTag(String aId)  {
        return Tagset.getDef().fromString(getString("tag"));
    }

    public Tag getTagFill(String aId)  {
        try {
            return Tagset.getDef().fromStringFill(getString("tag"));
        }
        catch(Exception e) {
            reportError(aId);
            throw new RuntimeException(e);
        }
    }
    
    protected int getInt(String aTokenId) {
        try {
            return Integer.parseInt(getString(aTokenId));
        }
        catch (java.lang.NumberFormatException e) {
            reportTypeError(aTokenId, "integer");
            throw e;
        }
    }
    
    protected char getChar(String aTokenId) {
        String tmp = getString(aTokenId);

        if (tmp.length() != 1 ) {
            reportTypeError(aTokenId, "character");
            throw new NoSuchElementException();
        }

        return tmp.charAt(0);
    }

// --- Error reporting ---------------------------------------------------------

    /**
     * Used to report context sensitive messages, such as errors and warnings.
     * @param aType
     * @param aMsg
     * @return 
     */
    public String msg(String aType, String aMsg, Object ... aObjs) { 
        return String.format("%s[%s: %d:%d] - %s\n  %s\n  %s^", aType,
            mFileId, mReader.getLineNumber(), getLastPosition(), String.format(aMsg, aObjs), mStr, 
            Strings.spaces(getLastPosition()));
    }

    public String warning(String aMsg, Object ... aObjs) {
        return msg("Warning ", aMsg, aObjs);
    }
    
    public String error(String aMsg, Object ... aObjs) {
        return msg("Error ", aMsg, aObjs);
    }

    public String tokenError(String aExpecting) { 
        return error(String.format("expecting %s, got %s; ignoring the whole line", aExpecting, lastToken()));
    }

    public String typeError(String aTokenId, String aType) {
        return tokenError(String.format("%s - a(n) %s", aTokenId, aType));
    }   
    
    public void reportError(String aMsg) {
        System.out.println(error(aMsg));
    }

    public void reportTokenError(String aTokenId) {
        System.out.println(tokenError(aTokenId));
    }

    public void reportTypeError(String aTokenId, String aType) {
        System.out.println(typeError(aTokenId, aType));
    }
    
    /**
     * Position before reading the last token. 
     * Used for error reports - report this as the error position.
     */
    public int getLastPosition() {
        return mOldPosition;
    }

    /**
     * The token during which error occured.
     * Used for error reports. Ineffective for other use.
     */
    public String lastToken() {
        return (mOldPosition >= mMaxPosition) ? "[EOL]" : mStr.substring(mOldPosition, mCurrentPosition);
    }
    
    public String state() {
        return String.format("%s\n%s^", mStr, Strings.spaces(mCurrentPosition)) ;
    }
    
}
