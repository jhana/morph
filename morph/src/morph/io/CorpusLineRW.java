package morph.io;

import java.util.regex.Pattern;
import lombok.Getter;
import util.io.XFile;


public abstract class CorpusLineRW {
    protected final static Pattern cWhiteSpacePattern = Pattern.compile("\\s+");

    @Getter protected final XFile file;

    /**
     * @param aFile can be null
     */
    public CorpusLineRW(XFile aFile) {
        file = aFile;
    }
}