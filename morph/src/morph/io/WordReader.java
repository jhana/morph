package morph.io;

import java.io.Closeable;

/**
 *
 * @author Jirka
 */
public interface WordReader extends Closeable {
   
    public @Override void close() throws java.io.IOException;


    // @todo add iterator inteface
    
    /**
     * The current word, i.e. the word that was read by last invocation of {@link #nextWord()}.
     * Null if eof was reached.
     */
    public String word();

    /**
     * Reads the next word, skips any non-word tokens. 
     * The word is returned by word(). If eof is reached, word() will return 
     * null (and this function <code>false</code>).
     *
     * @return <code>true</code> if a word was read; <code>false</code> if eof was reached 
     * or IOError occurs.  
     * @see #word()
     * @ if format error occurs (word sgml tag not followed by a word, etc.).
     */
    public boolean nextWord() throws java.io.IOException;  
}
