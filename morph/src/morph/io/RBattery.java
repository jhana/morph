package morph.io;

import java.io.IOException;
import java.util.Collection;
import util.io.*;
import morph.ts.Tag;
import util.err.Err;

/**
 * A battery of readers.
 *
 * Used to read several files containing alternative analysis/tagging of the same file.
 *
 * @todo make similar - there is just one TntReader (doing both M and Mm) =>? factory
 */
public class RBattery extends CorpusLineReaderBattery {
    private static XFile[] toFiles(final Collection<XFile> aFiles) {
        return aFiles.toArray(new XFile[aFiles.size()]);
    }

    public static RBattery openM(final Collection<XFile> aFiles) throws java.io.IOException {
        return openM(toFiles(aFiles));
    }

    public static RBattery openMm(final Collection<XFile> aFiles) throws java.io.IOException {
        return openMm(toFiles(aFiles));
    }

    @Deprecated
    public static RBattery openM(XFile[] aFiles) throws java.io.IOException {
        final RBattery b = new RBattery(aFiles);
        
        for (int i = 0; i < b.mSize; i++) {
            b.mRs[i] = Corpora.openM(aFiles[i]);
        }
        return b;
    }

    @Deprecated
    public static RBattery openMm(XFile[] aFiles) throws java.io.IOException {
        final RBattery b = new RBattery(aFiles);

        for (int i = 0; i < b.mSize; i++) {
            b.mRs[i] = Corpora.openMm(aFiles[i]);
        }
        return b;
    }

//    @todo how to set Pdt properties??? SgmlTags, ....
//    public static RBattery openMm(CorpusFile[] aFiles) throws java.io.IOException {
//        RBattery b = new RBattery(aFiles); 
//        
//        for (int i = 0; i < b.mSize; i++) {
//            b.mRs[i] = (aFiles[i].format().tnt()) ? TntReader.openMm(aFiles[i]) : ??? new PdtLineReader(aFiles[i]);
//        }
//        return b;
//    }

    public static RBattery openTnt(XFile[] aFiles) throws java.io.IOException {
        final RBattery b = new RBattery(aFiles);
        for (int i = 0; i < b.mSize; i++) {
            b.mRs[i] = TntReader.open(aFiles[i]);
        }
        return b;
    }
    
    private RBattery(XFile[] aFiles) throws java.io.IOException {
        super(aFiles);        // @todo !!!!
        mRs = new CorpusLineReader[mSize]; 
    }

    
    
    public Tag tag(int aIdx)    {return ((CorpusLineReader) mRs[aIdx]).tag();}

// a quick & dirty translation of tagset for cz -> ru   
//    public Tag tag(int aIdx)    {
//        Tag t = ((CorpusLineReader) mRs[aIdx]).mTag;
//        if (t.getGender()=='I' || t.getGender()=='Y') t = t.setGender('M');
//        if (t.getPOS() == 'A' && t.getNumber() == 'P') t = t.setGender('X');
//        if (t.getSubPOS() == 'p' && t.getNumber() == 'P') t = t.setGender('X');
//        if (t.getGender()=='Q') t = t.setGender('F').setNumber('S');
//        if (t.getSubPOS() == 'p') t = t.setPerson('-');
//        
//        t = t.setVariant('-');
//        
//        return t;
//    }
    
    public Tag[] tags()  {
        final Tag[] tags = new Tag[mRs.length];
        for (int i = 0; i < mRs.length; i++) 
            tags[i] = tag(i);

        return tags;
    }

    public static boolean read(CorpusLineReaderSBI ... aReaders) throws IOException {
        Boolean xMore = null;

        for (CorpusLineReaderSBI r : aReaders) {
            if (r == null) continue;
            
            boolean more = r.readLine();
            if (xMore == null) {
                xMore = more;
            }
            else {
                if (! xMore.equals(more)) {
                    StringBuilder sb = new StringBuilder();
                    Err.fErr("Error - corpora have different nr of words !!"); // %s / %s (GS line: %d)\nGS: %s\nTC %s",
                        //gsMore, tcMore, gs.getLineNumber(), gs.line(), toCheck.rs()[0].line());
                }
            }
        }
        return xMore;
    }


}    