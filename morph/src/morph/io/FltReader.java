package morph.io;

import morph.ma.lts.Fplus;
import morph.ma.lts.Lt;
import util.io.*;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.err.MyException;


/**
 * Supports format form - lemma - tag. The information is always nonambiguous.
 * @todo maybe lazy filling instead 
 */
public class FltReader extends CorpusLineReader {
    
    /**
     * Reads disambiguated files (only one tag).
     * Does not fill tags.
     */
    public static FltReader openM(XFile aFile) throws java.io.IOException {
        return new FltReader(aFile);
    }

    /**
     */
    public FltReader(XFile aFile) throws java.io.IOException {
        super(aFile);
    }



    /**
     * Reads the next line from the TNT tagged file. 
     * Ignores TNT comments (lines starting with %%)
     * Returns false once EOF was reached; true otherwise
     */
    public boolean readLine() throws java.io.IOException {
        fplus = null;
        do {
            line = r.readLine();
            if (line == null) return false;
            line = line.trim();
        } while ( line.length() == 0 );

        String[] tokens = cWhiteSpacePattern.split(line);
        assertE(tokens.length == 3, "Format should be: 'form lemma tag'");

        fplus = new Fplus(tokens[0]);
        String lemma  = tokens[1];
        try {
            Tag tag  = Tagset.getDef().fromString(tokens[2]);
            fplus.getLts().add(new Lt(lemma, tag));
        }
        catch(MyException e) {
           assertE(false, "Wrong tag " + tokens[2]); 
        }
        
        return true;
    }

}

