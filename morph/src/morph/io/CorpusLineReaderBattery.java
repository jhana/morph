package morph.io;

import util.io.*;

/**
 * Synchronized battery of readers. 
 * (Forms should match)
 */
public class CorpusLineReaderBattery implements CorpusLineReaderSBI {
    protected final int mSize;                  // to speed up access in cycles, etc.
    protected CorpusLineReaderI[] mRs;
    protected int lineNumber = 0;

    protected CorpusLineReaderBattery(final XFile[] aFiles) throws java.io.IOException {
        mSize = aFiles.length;
    }
    
    public CorpusLineReaderI[] rs() {return mRs;}
    
    /**
     * Size of the battery
     */
    public int size() {return mSize;}
    
    /**
     * Reads the next line of each reader.
     * Returns false once EOF was reached in any reader; true otherwise
     */
    @Override
    public boolean readLine() throws java.io.IOException {
        boolean result = true;
        for (CorpusLineReaderI r : mRs) {
            result &= r.readLine();
        }
        lineNumber++;

        return result;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    @Override
    public void close()  {
        IO.close(mRs);
    }

    /** Determined by the first reader */
    @Override
    public LineTypeI lineType() {
        return mRs[0].lineType();
    }

    /** Determined by the first reader */
    @Override
    public boolean isTokenLine() {
        return mRs[0].isTokenLine();
    }

    /** Determined by the first reader */
    @Override
    public String form()        {return mRs[0].form();}
}    