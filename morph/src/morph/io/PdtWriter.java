package morph.io;

import java.io.PrintWriter;
import java.util.*;
import util.io.*;
import morph.ma.lts.*;
import org.apache.commons.lang3.StringEscapeUtils;
import util.col.Cols;
import util.col.ColPrinter;
import morph.ts.Tag;
import morph.ts.Tagset;
import util.str.StringPair;
import util.opt.Options;

/**
 *
 * @todo filtering (transliteration)
 * @todo tag translation
 * @author  Jirka Hana
 */
public class PdtWriter extends CorpusWriter {
    public static final String cOptLemmaTag  = "pdt.lemmaTag";
    public static final String cOptSrcAttr   = "pdt.srcAttr";
    public static final String cOptLi        = "pdt.li";

    boolean mLines = false;
    boolean mTagTransl = false;

    //MultiMap<Tag,Tag> mTagTranslTbl;

    String mLemmaTagCore = "MMl";
    String mLemmaTag;

    String mTagTagCore   = "MMt";
    String mTagTag;

    private ColPrinter mTagPrinter;
// -----------------------------------------------------------------------------
// Configuration
// -----------------------------------------------------------------------------

    public PdtWriter(PrintWriter aWriter) throws java.io.IOException {
        super(aWriter, null);    //@todo check: ?null or Filter.cIdentityFilter more effective?
    }

    public PdtWriter(PrintWriter aWriter, Filter aFilter) throws java.io.IOException {
        super(aWriter, aFilter);
    }

    public PdtWriter(PrintWriter aWriter, Filter aFilter, Options aOptions) throws java.io.IOException {
        super(aWriter, aFilter);
        setOptions(aOptions);
    }

    public PdtWriter(XFile aFile) throws java.io.IOException {
        super(aFile, null);    //@todo check: ?null or Filter.cIdentityFilter more effective?
    }

    public PdtWriter(XFile aFile, Filter aFilter) throws java.io.IOException {
        super(aFile, aFilter);
    }

    public PdtWriter(XFile aFile, Filter aFilter, Options aOptions) throws java.io.IOException {
        super(aFile, aFilter);
        setOptions(aOptions);
    }

    /**
     * After setting any of cOptlemmaTag, cOptInfo,cOptSrc, or cOptSrcAttr, call options updated.
     */
    @Override
    public PdtWriter setOptionImpl(String aKey, Options aOptions) {
        if (aKey.equals(cOptLemmaTag)) {
            setLemmaTagCore( aOptions.getString(aKey) );
        }
        // drop this option
        else if (aKey.equals(cOptSrcAttr)) {
            // @todo
        }
        else if (aKey.equals(cOptLi)) {
            mLines = aOptions.getBool(aKey);
        }
        else {
            super.setOptionImpl(aKey, aOptions);
        }
        return this;
    }

    public PdtWriter setLemmaTagCore(String aLemmaTagCore) {
        mLemmaTagCore = aLemmaTagCore;
        mTagTagCore = morph.ts.PdtSTag.toTCore(mLemmaTagCore);
        return this;
    }

    @Override
    public PdtWriter optionsUpdated() {
        onSetLTTags();
        return this;
    }

    private void onSetLTTags() {
        mLemmaTag = '<' + mLemmaTagCore + '>';
        mTagTag = '<' + mTagTagCore + '>';

        // use stringbuilder
        mTagPrinter = new ColPrinter<StringPair>('<' + mTagTagCore + ' ', ">",  " ", mTagTag) {
            public String toString(StringPair aItem)	{
                return aItem.mFirst + "=\"" + quote(aItem.mSecond) + '\"';
            }
        };
    }

// -----------------------------------------------------------------------------
// I/O
// -----------------------------------------------------------------------------

    /**
     * Raw output of a line.
     * @param aLine
     */
    public void writeLine(String aLine)  {
        mW.println(aLine);
    }

    @Override
    public void writeLine(String aForm, Tag aTag)  {
        mW.printf("%s\t%s\n",  quote(aForm), aTag);
    }

    @Override
    public void writeLine(String aForm, Collection<Tag> aTags) {
        mW.printf("%s\t%s\n", quote(aForm), Cols.toString(aTags,"", "", " ", ""));
    }

    @Override
    public void writeLineS(String aForm, Collection<String> aTags)  {
        mW.printf("%s\t%s\n", quote(aForm), Cols.toString(aTags, "", "", " ", ""));
    }

    @Override
    public void writeLine(String aForm, Lts aLTs) {
       writeLine(aForm, aLTs, "");
    }

    public void writeLine(final String aForm, Lts aLts, final String aRest) {
        mW.print(PdtSgmlConst.cForm);
        mW.print(quote(filter(aForm)));

        if (aLts == null || aLts.isEmpty()) {
            aLts = new Lts();
            aLts.add( new Lt("X", Tagset.getDef().fromString(Tagset.getDef().cAllHyphens), "No Analysis", null) ); // todo this should be done outside
        }

        writeItems(aLts);

        mW.println(aRest);
    }

    private void writeItems(final Lts aLts) {
        for (String lemma : aLts.lemmas()) {
            if (mLines) mW.print("\n  ");
            mW.print(mLemmaTag);
            mW.print(quote(filter(lemma)));
            for (Lt lt : aLts.col(lemma)) {
                if (mLines) mW.print("\n     ");
                writeItem(lt);
            }
        }
    }

    private void writeItem(Lt aLt) {
        final List<StringPair> attrs = new ArrayList<StringPair>(3);  // @ efff -use one list and clear it each time after use (but would require synchronization)

        // --- Info ---
        if ( writeInfo && aLt.info() != null) {    // info may be collected (e.g. for filtering) without need to print it
            attrs.add( new StringPair("info", filter(aLt.info().toString())) );
        }

        // --- Src ---
        if ( writeSrc && aLt.src() != null) {
            attrs.add(new StringPair("src", aLt.src()));
        }

//        // --- Transl ---
//        if (mTagTransl) {
//            Set<Tag> newTags = mTagTranslTbl.get(aTS.tag());
//
//            if (newTags != null) {
//                for (Tag tag : newTags) {
//                    writeSgmlTag(mTagTagFront, attrs);
//                    writeTag(aTS.tag());
//                }
//
//                if (newTags.size() ==)
//
//            }
//        }
//
        writeSgmlTag(attrs);
        writeTag(aLt.tag());
    }

    private void writeSgmlTag(List<StringPair> aAttrs) {
        // escape quotes in the attributes (e.g. tvrdyj znak)
        // @todo maybe do it more effective
        mW.print(Cols.toString(aAttrs, mTagPrinter));
    }

    private void writeTag(Tag aTag) {
        mW.print(aTag.toString());
    }

    /**
     * @todo configurable, or at least real.
     * @todo get from input file if present
     */
    @Override
    public void writeHeader() {
//        mW.println("<csts lang=cs>");
        mW.println("<csts>");
        mW.println("<doc file=\"data/example-check\" id=0>");
//        mW.println("<a>");
//        mW.println("<mod>S");
//        mW.println("<txtype>inf");
//        mW.println("<genre>mix");
//        mW.println("<med>j");
//        mW.println("<temp>2001");      // ???
//        mW.println("<authname>y");
//        mW.println("<opus>example-check");
//        mW.println("<id>0");
//        mW.println("</a>");
        mW.println("<c>");
    }

    @Override
    public void writeFooter() {
        mW.println("</c>");
        mW.println("</doc>");
        mW.println("</csts>");
    }

    private String quote(String aStr) {
        return StringEscapeUtils.escapeXml(aStr);
    }


}