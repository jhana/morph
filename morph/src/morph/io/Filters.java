package morph.io;

import util.*;
import util.str.Transliterate;

/**
 *
 * @author  Jirka Hana
 */
public final class Filters  {
    
    public final static Filter transliterate() { 
        return new Filter() {
            public final String filter(String aString) {
                return Transliterate.transliterate(aString);}
        };
    }

    public final static Filter fromUppsala() { 
        return new Filter() {
            public final String filter(String aString) {
                return Transliterate.fromUppsala(aString);}
        };
    }
    
}
