package morph.io;

/**
 * Adapts CorpusLineReaders to WordReader.
 *
 * @author Jirka
 */
public class CorpusLineWordReader implements WordReader {
    CorpusLineReader mR;
    
    /** 
     * Creates a new instance of CorpusWordReader 
     * @param aR 
     */
    public CorpusLineWordReader(CorpusLineReader aR) {
        mR = aR;
    }

    @Override
    public void close() throws java.io.IOException {
        mR.close();
    }
    
    @Override
    public boolean nextWord() throws java.io.IOException {
        return mR.readLine();
    }

    @Override
    public String word() {
        return mR.form();
    }

    public CorpusLineReader reader() {return mR;}
}
