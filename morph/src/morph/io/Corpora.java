
package morph.io;

import java.io.IOException;
import morph.util.MUtilityBase;
import util.err.Err;
import util.err.FormatError;
import util.io.XFile;
import util.opt.Options;

/**
 * Support for opening corpora readers/writer.
 * @author Administrator
 */
public class Corpora {

    public static CorpusWriter openWriter(XFile aFile, Options aOptions) throws IOException {
        // todo make transliteration configurable
        final Filter outFilter = aOptions.getBool("translit") ? Filters.transliterate() : Filter.cIdentityFilter;

        CorpusWriter out;
        // @todo move to output
        if (aFile.format() == MUtilityBase.cCsts) {
            out = new PdtWriter(aFile, outFilter);
        }
        else if (aFile.format() == MUtilityBase.cTnt) {
            out = new TntWriter(aFile, outFilter);
        } 
        else {
            throw new FormatError("Only tnt & pdt formats are supported for M/MM output");
        }
        
        out.setOptions(aOptions);
        return out;
    }
   
    
    public static CorpusLineReader openF(XFile aFile) throws IOException {
        return open(aFile);
    }

    /**
     * Reads disambiguated files (only one tag).
     * Does not fill tags.
     */
    public static CorpusLineReader openM(XFile aFile) throws IOException {
        return open(aFile).setMm(false);
    }

    /**
     * Reads morphologically analyzed (possibly more than one tags).
     * Fills tags does not fill tag.
     */
    public static CorpusLineReader openMm(XFile aFile) throws IOException {
        return open(aFile).setMm(true);
    }

    public static CorpusLineReader open(XFile aFile) throws IOException {
        CorpusLineReader r = null;
        
        if(aFile.format() == MUtilityBase.cCsts)  r = new PdtLineReader(aFile);
        else if(aFile.format() == MUtilityBase.cTnt)  r = TntReader.open(aFile);
        else if(aFile.format() == MUtilityBase.cFlt)  r = new FltReader(aFile);
        else if(aFile.format() == MUtilityBase.cVert)  r = VerticalOczPlainReader.open(aFile);
        else throw new IOException(String.format("Format %s is incompatible with this option. @todo ???Really", aFile.format().getId()));
        
        // todo tmp experimental code 
        if (aFile.getProperties().getOptions() != null) {
            r.setOptions(aFile.getProperties().getOptions());
        }
        
        return r;
        
    }
    
    /**
     * Currently supports pdt, tnt and plain (onev word per line) formats.
     * @todo add TokWordReader (somehow configurable)
     * @todo add patern filtering (read only words satisfying/not satisfying a pattern)
     */
    public static WordReader openWordReader(XFile aFile) throws IOException {
        System.out.println("INFILE " + aFile);
        if(aFile.format() == MUtilityBase.cCsts || 
           aFile.format() == MUtilityBase.cTnt || 
           aFile.format() == MUtilityBase.cVert || 
           aFile.format() == MUtilityBase.cFlt) 
            return new CorpusLineWordReader( openF(aFile) );
        if(aFile.format() == MUtilityBase.cPlain) 
            return new LineWordReader(aFile, null);

        Err.fErr("Word reading is not supported for %s format (file %s)", aFile.format(), aFile.file() );
        return null;
    }
}
        