package morph.io;

import java.io.*;
import util.io.*;
import util.*;
import util.io.IO;


public abstract class AbstractWordReader implements WordReader, java.io.Closeable {
    /**
     * Filter used usually for transliterations.
     * @todo move to a superclass
     */
    protected Filter mFilter; 

    final protected LineReader mR;
    protected String mWord;
    protected int mWords;
    
    /**
     * ",.-!?_ and space are considered whitespace characters, ~ and \ are considered 
     * word characters.
     */
    public AbstractWordReader(XFile aFile, Filter aFilter) throws IOException {
        this(IO.openReader(aFile), aFilter);
        mR.setFile(aFile);
    }

    public AbstractWordReader(Reader aReader, Filter aFilter) throws IOException {
        mR =  new LineReader(aReader);
        mFilter = aFilter;
        mWords = 0;
    }
    
    @Override
    public void close() throws java.io.IOException {
        mR.close();
    }

    /**
     * The current word (the word that was read by last invocation of nextWord() function)
     * Null if eof was reached.
     * @see #nextWord()
     */
    @Override
    public String word()    {return mWord;}

    /**
     * Reads the next word from the reader, skipping non-word tokens. 
     * The word is returned by word(). If eof is reached, word() will return 
     * null (and this function <code>false</code>).
     *
     * @return <code>true</code> if a word was read; <code>false</code> if eof was reached.  
     * @todo do something about this - other class???
     *   line = Util.preprocessLine(line);
     * @see #word()
     */
    @Override
    abstract public boolean nextWord() throws IOException;
}
