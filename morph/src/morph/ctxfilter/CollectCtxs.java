package morph.ctxfilter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;
import lombok.Cleanup;
import lombok.Cleanup;
import morph.io.Corpora;
import util.err.Log;
import morph.util.MUtilityBase;
import morph.io.WordReader;
import morph.ma.Context;
import morph.ma.Morph;
import morph.ma.MorphCompiler;
import morph.ma.lts.Lt;
import morph.ma.lts.Lts;
import morph.ts.Tag;
import morph.ts.Tags;
import morph.ts.Tagset;
import util.col.Cols;
import util.col.Counter;
import util.col.MultiMap;
import util.err.Err;
import util.io.IO;
import util.io.XFile;


public class CollectCtxs extends MUtilityBase {
    /** 
     * Morphological analyzer 
     */
    private Morph mMorph;

    /** Counter of words in the corpus */
    private int mWords = 0;

    private XFile corpusFile;
    private Pattern narrow;

    protected final List<Map<String,Counter<Character>>> ctxValCounters = new ArrayList<Map<String,Counter<Character>>>();

    public static void main(String[] args) throws IOException {
        new CollectCtxs().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);

        // --- set up the morph object ---
        profileStart("Loading Morphology ... ");
        if (!setupMorph()) System.exit(-1);
        profileEnd("Done in %2.2f s.\n");

        corpusFile = getArgs().getDirectFile(0, cCsts);
        narrow = getArgs().getPatternNE("narrow");

        for (int i=0;;i++) {
            System.out.println("Iteration #" + i);
        
            profileStart("   Collecting ctx -> vals   ... ");
            analyzeFile();
            profileEnd("   Done in %2.2f s. Speed: %2.2f tokens/s.\n", mWords);
            dumpResults();

            filterFilters();

            createWordList();
            // todo add wordlist to morph
            if (true) break;
        }

        profileStart("Dumping results ... ");
        dumpWordList();
        profileEnd("Done");
    }



   
    private boolean setupMorph() throws IOException {
        Lt.setInfoIrelevant(getArgs().getBool("infoIrelevant"));  // tODO needed for some filtering (leo) but do not want to keep duplicates in results
        System.out.println("infoIrelevant=" + Lt.isInfoIrelevant());
        mMorph = MorphCompiler.factory().compile(getArgs());
        return (mMorph != null);
    }

//    private void setFilter() {
//        Guesser guesser = (Guesser) mMorph.getModule("Guesser");
//        for(Iterator<LtsFilter> it = guesser.getFilters().iterator(); it.hasNext();) {
//            if (it.next() instanceof CtxValFilter) it.remove();
//        }
//
//        guesser.getFilters().add(null);
//    }

//    class CtxValFilter implements LtsFilter {
//        protected final List<Map<String,Counter<Character>>> ctxValCounters = new ArrayList<Map<String,Counter<Character>>>();
//
//        @Override
//        public void filter(Lts aLTS, Context aCtx) {
//            if (! aCtx.hasPrevWord(0) ) return;
//
//            String prevWord =
//
//
//        }
//
//
//
//
//    }

    private final int cGSlot = 2;


    
    private void analyzeFile() throws java.io.IOException {
        for (int slot = 0; slot < tagset.getLen(); slot++) {
            ctxValCounters.add(new HashMap<String, Counter<Character>>());
        }

        @Cleanup WordReader r = Corpora.openWordReader(corpusFile);
        Err.fAssert(r != null, "In reader is null");
        
        analyzeWReader(r);
    }

    private void analyzeWReader(WordReader aR) throws java.io.IOException {
        //System.out.println("=====================");
        final Context ctx = new Context(aR,1,0);
        ctx.prepare();
        //System.out.println("Ctx " + ctx);

        for( ;ctx.hasNext(); ) {
            //System.out.println("A");
            Lts lts = null;
            String curForm = ctx.getCur();

            if (narrow == null || narrow.matcher(curForm).matches() ) {
                //System.out.println("B");
                lts = mMorph.analyze(curForm, ctx);
                mWords++;
                Log.dot(mWords);

                if (ctx.hasPrevWord(0)) {
                    //System.out.println("C");
                    String prevWord = ctx.getPrevWord(0);

                    for (int slot = 0; slot < tagset.getLen(); slot++) {
                    //for (int slot = cGSlot; slot < cGSlot+1; slot++) {
                        Counter<Character> counter = ctxValCounters.get(slot).get(prevWord);
                        if (counter == null) {
                            counter = new Counter<Character>();
                            ctxValCounters.get(slot).put(prevWord, counter);
                        }

                        Set<Character> vals = Tags.collectValues(lts.tags(), slot);

                        if (vals.size() == 1) { // todo param
                            //System.out.println("D " + prevWord + " " + curForm);
                            for(Character val : vals) {
                                counter.add(val);
                            }
                        }
//                        System.out.printf("prev=%s, cur=%s\n", prevWord, curForm);
//                        System.out.println(" vals=" + vals);
//                        System.out.println(" counter=" + counter);
                    }
                }
            }

            ctx.update(lts);
        }
        Log.info("\n"); // nl after dots
    }

    //todo possibly remove some values
    private void filterFilters() {
        for (int slot = 0; slot < tagset.getLen(); slot++) {
            Map<String,Counter<Character>> map = ctxValCounters.get(slot);
            for (Counter<Character> counter : map.values()) {
                counter.calculate();
            }

            for (Iterator< Map.Entry<String,Counter<Character>> > it = map.entrySet().iterator(); it.hasNext();) {
                Counter<Character> counter = it.next().getValue();
                if (counter.size() > 1) it.remove();        // todo param
            }
        }
    }

    private void createWordList() throws IOException  {
        @Cleanup WordReader r = Corpora.openWordReader(corpusFile);
        createWordList(r);
    }

    private MultiMap<String,Tag> wordList;

    private void createWordList(WordReader aR) throws IOException  {
        wordList = new MultiMap<String,Tag>();

        final Context ctx = new Context(aR,1,0);
        ctx.prepare();

        for( ;ctx.hasNext(); ) {
            Lts lts = null;
            String curForm = ctx.getCur();
            boolean filtered = false;

            if (narrow == null || narrow.matcher(curForm).matches() ) {
                lts = mMorph.analyze(curForm, ctx);

                if (ctx.hasPrevWord(0)) {
                    String prevWord = ctx.getPrevWord(0);

                    // filter lts by values associated with context
                    for (int slot = 0; slot < tagset.getLen(); slot++) {
                        //int slot = 2;
                        Counter<Character> counter = ctxValCounters.get(slot).get(prevWord);
                        if (counter != null) {
                            Set<Character> vals = counter.getMap().keySet();
                            for (Iterator<Lt> i = lts.iterator(); i.hasNext();) {
                                Lt lt = i.next();
                                Character val = lt.tag().getSlot(slot);
                                if (!vals.contains(val)) { i.remove(); filtered = true;}
                            }
                        }
                    }

                    if (filtered && !lts.tags().isEmpty())
                        wordList.addAll(curForm, lts.tags());
                }
            }

            ctx.update(lts);
        }
    }

    private void dumpWordList() throws IOException {
        XFile outFile = getArgs().getDirectFile(1, cPlain);
        PrintWriter out = IO.openPrintWriter(outFile);

        try {
            SortedSet<String> words = new TreeSet<String>(wordList.keySet());

            for (String word : words ) {
                out.print(word);
                Set<Tag> tags = wordList.get(word);
                out.println(" " + Cols.toString(tags, "", "", " ", "ERROR"));
            }
        }
        finally {
            IO.close(out);
        }
    }



    private void dumpResults() throws IOException {
        for (int slot = 0; slot < tagset.getLen(); slot++) {
            dumpResults(slot);
        }
    }

    private final Comparator<Counter<Character>> comparator = new Comparator<Counter<Character>>() {
        @Override public int compare(Counter<Character> o1, Counter<Character> o2) {
            long diff = o1.sigma() - o2.sigma();
            if (diff > 0L) return 1;
            else if (diff < 0L) return -1;
            else return 0;
        }
    };

    private void dumpResults(int aSlot) throws IOException {
        char code = Tagset.getDef().slot2code(aSlot);
        XFile outFile = getArgs().getDirectFile(1, cPlain).addExtension(""+code);
        PrintWriter out = IO.openPrintWriter(outFile);

        try {
            Map<String,Counter<Character>> map = ctxValCounters.get(aSlot);
            for (Counter<Character> counter : map.values()) {
                counter.calculate();
            }
            List<Map.Entry<String,Counter<Character>>> entries = Cols.sortByVals(map.entrySet(), comparator);

            for (Map.Entry<String,Counter<Character>> e : entries) {
                String ctxWord = e.getKey();
                Counter<Character> counter = e.getValue();
                if (counter.size() <= 1) {
                    counter.calculate();
                    if (counter.sigma() > 1 && !counter.keySet().contains('-') ) {
                        out.print(ctxWord);
                        out.print(" " + counter.sigma() + " ");
                        out.println(counter.toSortedString("=",";"));
                    }
                }
            }
        }
        finally {
            IO.close(out);
        }
    }

} 

