package morph.common;

import java.io.PrintWriter;
import java.util.logging.Level;
import lombok.Getter;
import lombok.Setter;
import morph.io.ReaderParser;
import util.err.Logger;

/**
 * @todo Merge with Logger.
 * 
 * @author jirka
 */
public class UserLog {
    @Getter public final int cMaxNrOfErrors = 20;
    @Getter @Setter public boolean detailedError = true;

    @Getter private int errors = 0;
    @Getter private int warnings = 0;
    @Getter private final PrintWriter out;

    public UserLog(PrintWriter out) {
        this.out = out;
    }

    public UserLog() {
        this(new PrintWriter(System.err, true));
    }


    public void info(String msg, Object ... args) {
        out.printf(String.format(msg+"\n", args));
    }

    

    public void assertTrueW(final boolean aCond, ReaderParser aR, String aFormatString, Object ... aParams)  {
        if (aCond) return;
        handleWarning( aR, aFormatString, aParams );
    }

    public void assertTrueW(final boolean aCond, String aFormatString, Object ... aParams)  {
        if (aCond) return;
        handleWarning( aFormatString, aParams );
    }

    public void assertTrue(final boolean aCond, ReaderParser aR, String aFormatString, Object ... aParams)  {
        if (aCond) return;
        handleError( aR, aFormatString, aParams );
    }

    public void assertTrue(final boolean aCond, String aFormatString, Object ... aParams)  {
        if (aCond) return;
        handleError( aFormatString, aParams );
    }



    public void handleWarning(ReaderParser aR, String aFormatString, Object ... aParams)  {
        handleWarning( aR.msg(aFormatString, aParams) );
    }

    /**
     * Prints the specified error message and increases the error counter.
     * Does not throw or terminate anything.
     */
    public void handleWarning(String aMsg, Object ... aParams) {
        out.println("Warning: " + String.format(aMsg, aParams));
        warnings++;
    }



    public void handleError(ReaderParser aR, String aFormatString, Object ... aParams)  {
        handleError( aR.msg(aFormatString, aParams) );
    }

    public void handleError(Throwable aE, ReaderParser aR, String aMsg) {
        String msg = aE.getMessage() == null ? aE.toString() : aE.getMessage();
        handleError(aE, aR.msg(aMsg + msg));
    }

    public void handleError(Throwable aE) {
        handleError(aE, aE.getMessage());
    }

    public void handleError(String aMsg, Object ... aParams) {
        handleError((Throwable)null, aMsg, aParams);
    }

    /**
     * Prints the specified error message and increases the error counter.
     * Does not throw an exception.
     */
    public void handleError(Throwable aE, String aMsg, Object ... aParams) {
        out.print("ERROR: ");
        out.print(String.format(aMsg, aParams));
        if (aE != null) out.print("\n  " + aE.getMessage());    // todo include aE.mesage into the message placeholder?
        out.println();

        if (detailedError && aE != null) aE.printStackTrace(out);

        errors++;
        if (errors > cMaxNrOfErrors) {
            out.println("Too many errors");
            System.exit(-1);
        }
    }


}
