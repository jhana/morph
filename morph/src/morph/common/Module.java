package morph.common;

import java.io.IOException;
import java.io.Serializable;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.Setter;
import morph.ts.Tagset;
import morph.util.MArgs;
import util.opt.Options;

/**
 *
 * Initialization sequence:
 * <ul>
 * <li>Binary loading:
 *      <ul>
 *          <li>binaryLoading
 *          <li>init()
 *          <li>configure(..)
 *      </ul>
 * <li>Compilation:
 *      <ul>
 *          <li>init()
 *          <li>compilation
 *          <li>configure(..)
 *      </ul>
 *  </ul>
 *
 * @author  Jirka Hana
 */
public abstract class Module implements Serializable {
    // =============================================================================
    // Implementation <editor-fold desc="Implementation">
    // ==============================================================================
    @Setter
    protected transient MArgs args;
    // --- configuration ---
    @Getter
    @Setter
    protected boolean collectInfo;
    protected transient Options moduleArgs;
    @Getter
    protected String moduleId;
    protected Pattern narrow;
    @Getter
    protected String src;
    /**
     * If true, analysis will stop after this module if at least one analysis
     * was found (not necessary by this module).
     */
    @Getter
    @Setter
    protected boolean stopWhenNotEmpty;
    protected transient Tagset tagset = Tagset.getDef(); // todo

    /**
     *
     * @return number of errors
     * @throws java.io.IOException
     */
    public abstract int compile(UserLog aUlog) throws IOException;

    public int configure() {
        return 0;
    }

    // -----------------------------------------------------------------------------
    // Setup
    // -----------------------------------------------------------------------------
    /**
     * Sets module id, and options objects.
     * An overriding method should call this method first.
     * Precedes compilation, follows binary loading.
     * @param aModuleId
     * @param aArgs
     */
    public void init(String aModuleId, MArgs aArgs) {
        moduleId = aModuleId;
        args = aArgs;
        moduleArgs = aArgs.getSubtree(aModuleId);
        src = moduleArgs.getString("src", "?"); // @todo ?? move to configure?
        // @todo ?? move to configure?
        collectInfo = args.getBool("collectInfo", false);
        narrow = moduleArgs.getPattern("narrow", null);
        //System.out.println("collectInfo " + mCollectInfo);
    }

    //@xDeprecated
    protected String mpn(String aPropertyName) {
        return moduleId + '.' + aPropertyName;
    }
    // </editor-fold>
    
}
