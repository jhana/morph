package morph.acq;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.XMLEvent;
import util.str.StringPair;
import util.str.Strings;
import util.col.Cols;
import morph.ma.lts.DecInfo;
import morph.ma.lts.DerInfo;
import morph.ma.lts.Lt;
import morph.ma.module.Paradigms;
import util.str.Caps;

/**
 *
 */
class ADerivEntry extends AEntry {
    /** (lemma,prdgmId) -> stems */
    protected Map<StringPair,String[]>   mEntries = new HashMap<StringPair, String[]>();
    /** (lemma,prdgmId) -> ?? */
    protected Map<StringPair,String[]> mDeclInfos = new HashMap<StringPair, String[]>();

    @Override
    public void setParadigm(Paradigms aParadigms) {
    }

    public double getPctOfPossibleForms() {
        return -1.0;
    }

    @Override
    public void update(String aForm, CapFreq aFreqs, boolean aName, Set<Lt> aLts) {

        for (Lt lt : aLts) {
            DerInfo info = lt.info().asDerInfo();
            DecInfo declInfo = info.getDeclInfo();
            String lemma = declInfo.getLemma();
            if (aName) lemma = Caps.toFirstCapital(lemma);

            StringPair declLp = new StringPair(lemma, declInfo.getPrdgmId());

            String[] stems = mEntries.get(declLp);
            String[] declInfos = mDeclInfos.get(declLp);

            if (stems == null) {
                stems = new String[declInfo.getPrdgm().nrOfStems()];
                mEntries.put(declLp,stems);

                declInfos = new String[declInfo.getPrdgm().nrOfStems()];
                mDeclInfos.put(declLp,declInfos);
                //stems[ declInfo.getEnding().getRootId() ] = declInfo.getActualStem();
            }

            updateStems(stems, declInfos, aForm, declInfo);
        }

        addForm(aForm, aFreqs, aLts);
    }

    @Override
    public void merge(AEntry aE) {
        ADerivEntry e = (ADerivEntry) aE;
        assert mLemma.equals(e.mLemma) && mParadigmId.equals(e.mParadigmId);

        mergeForms(e);

        // merge entries
        for (Map.Entry<StringPair,String[]> de : e.mEntries.entrySet()) {
            StringPair lp = de.getKey();
            String[] stems = mEntries.get(lp);
            if (stems == null) {
                mEntries.put(lp, de.getValue());
                mDeclInfos.put(lp, e.mDeclInfos.get(lp));
            }
            else {
                String[] declInfos = mDeclInfos.get(lp);
                String[] declInfos2 = e.mDeclInfos.get(lp);
                mergeStems(this, stems, declInfos, aE, de.getValue(), declInfos2);
            }
        }
    }


    public void write(XMLStreamWriter aW) throws XMLStreamException {
        aW.writeStartElement("L");
            aW.writeCharacters(mLemma);
            aW.writeStartElement("P");
                aW.writeCharacters(mParadigmId);
            aW.writeEndElement();

            writeForms(aW);

            for (Map.Entry<StringPair,String[]> de : mEntries.entrySet()) {
                String lemma = de.getKey().mFirst;
                String prdgm = de.getKey().mSecond;
                String stems = Cols.toString(Arrays.asList(de.getValue()), Cols.<String>emptyNullPrinter(), ':');
                String dis   = getStemsString(mDeclInfos.get(de.getKey()));

                aW.writeCharacters("\n    ");
                aW.writeStartElement("b");
                    aW.writeStartElement("l");
                        aW.writeCharacters(lemma);
                    aW.writeEndElement();
                    aW.writeStartElement("p");
                        aW.writeCharacters(prdgm);
                    aW.writeEndElement();
                    aW.writeStartElement("s");
                        aW.writeCharacters(stems);
                    aW.writeEndElement();
                    aW.writeStartElement("di");
                        aW.writeCharacters(dis);
                    aW.writeEndElement();
                aW.writeEndElement();
         }

        aW.writeEndElement();
        aW.writeCharacters("\n");
    }


    // nextOpenTag(core)
    // nextCloseTag(core)
    // nextString


    public void read(XMLEventReader aR) throws XMLStreamException {
        aR.nextEvent(); // L;
            mLemma = aR.nextEvent().asCharacters().getData();
            aR.nextEvent(); // P;
            mParadigmId = aR.nextEvent().asCharacters().getData();
            aR.nextEvent(); // /P;

            readForms(aR);

            for(;;) {
                XMLEvent e = XmlUtils.peekTag(aR);
                if (! (e.isStartElement() && e.asStartElement().getName().getLocalPart().equals("b")) ) break;

                aR.nextEvent();  // eat peeked b
                    aR.nextEvent(); // l
                        String bLemma = aR.nextEvent().asCharacters().getData();
                    aR.nextEvent(); // /l
                    aR.nextEvent(); // p
                        String bPrdgm = aR.nextEvent().asCharacters().getData();
                    aR.nextEvent(); // /p
                    aR.nextEvent();
                        String[] bStems = Strings.cColonPattern.split( aR.nextEvent().asCharacters().getData(), -1);
                    //System.out.println(bStems);
                    aR.nextEvent();
                    aR.nextEvent(); // di
                        String[] bDeclInfos = Strings.cColonPattern.split( aR.nextEvent().asCharacters().getData(), -1);
                    aR.nextEvent(); // /di

                    aR.nextEvent(); // /b
                mEntries.put(  new StringPair(bLemma, bPrdgm), bStems);
                mDeclInfos.put(new StringPair(bLemma, bPrdgm), bDeclInfos);
            }
        aR.nextEvent(); // /L
    }
// =============================================================================
// Implementation <editor-fold desc="Implementation">
// ==============================================================================
// </editor-fold>
}
