package morph.acq;

import util.str.Transliterate;
import util.str.StringPair;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import lombok.Cleanup;
import util.err.Err;
import util.err.Log;
import util.io.*;
import util.col.ColPrinter;
import util.col.Cols;
import morph.io.*;
import morph.ma.module.Paradigms;
import morph.ma.module.prdgm.DeclParadigm;
import morph.ma.Morph;
import morph.ma.MorphCompiler;
import morph.ma.module.Derivations;
import morph.util.MUtilityBase;
import util.str.Caps;

/**
 * @todo epen
 * @author Jiri
 */
public class FilterCandidates extends MUtilityBase {
    private final static java.util.logging.Logger log = java.util.logging.Logger.getLogger(FilterCandidates.class.getName());

    static {
        log.setLevel(java.util.logging.Level.ALL);
    }

    public static void main(String[] args) throws IOException, XMLStreamException {
        new FilterCandidates().go(args);
    }

// =============================================================================
// Implementation
// =============================================================================

    private PrintWriter mW;
    private Paradigms mParadigms;
    private Derivations mDerivations;
    private SortedSet<String> mLemmas = new TreeSet<String>();

    /**
     * Consider entries obtained via derivation (super-paradigm entries).
     */
    private boolean mConsiderDeriv;

    private int mSplittingPrefixLen;

    private int mMinNrOfForms;
    private int mMinNrOfTokens;

    private double mFormsIdx;
    private double mTokensIdx;
    private double mCoverageIdx;

    private double mRelBeam;
    private double mAbsBeam;

    private Pattern noFiltering = null;
    private int noFilteringMin = 2;

    private double mFringeThreashold = 0.005;

    private boolean mWriteDropped;
    private Pattern mNarrow;


    private void go(String[] aArgs) throws IOException, XMLStreamException {
        argumentBasics(aArgs,2);

        XFile inFile   = getArgs().getDirectFile(0);
        XFile outFile  = getArgs().getDirectFile(1);
        Err.checkIFiles("in", inFile);
        Err.checkOFiles("out", outFile);

        mMinNrOfForms      = getArgs().getInt("minNrOfForms");
    	mMinNrOfTokens     = getArgs().getInt("minNrOfTokens");
        mFormsIdx          = getArgs().getDouble("formsIdx");
        mTokensIdx         = getArgs().getDouble("tokensIdx");
        mCoverageIdx       = getArgs().getDouble("coverageIdx");
        mRelBeam           = getArgs().getDouble("relBeam");
        mAbsBeam           = getArgs().getDouble("absBeam");
        mConsiderDeriv     = getArgs().getBool("derivation");
        //System.out.println("mConsiderDeriv = " + mConsiderDeriv);
        noFiltering         = getArgs().getPatternNE("noFiltering.tags");
        noFilteringMin      = getArgs().getInt("noFiltering.minForms", 1);

        mSplittingPrefixLen = getArgs().getInt("splittingPrefixLen");
        mWriteDropped       = getArgs().getBool("writeDropped");
        mNarrow             = getArgs().getPatternNE("narrow");

        mWriteDropped       = false;  // todo remove


        XFile lemmaFile = getArgs().getFile("lemmaFile", null);
        if (lemmaFile != null) loadLemmas(lemmaFile);

        // --- get paradigms ---
        Morph morph = MorphCompiler.factory().compile(getArgs());
        Err.iAssertFatal(morph != null, "Cannot create Morph object");
        mParadigms = (Paradigms) morph.getModule("Paradigms");
        Err.iAssertFatal(mParadigms != null, "Cannot get Paradigms object");
// TODO
//        mDerivations = new Derivations();
//        mDerivations.init("Derivations", getArgs());
//        int derErr = mDerivations.compile(mParadigms);
//        Err.iAssertFatal(derErr == 0, "Cannot compiles Derivations object");

        profileStart("Processing ...");
        proces(inFile, outFile);
        profileEnd();
    }

    /**
     * Reads in list of lemmas that can be used to prune the hypotheses
     * @todo make this a general utility for reading lists, frequency sets, etc.
     */
    private void loadLemmas(XFile aLemmaFile) throws IOException {
        @Cleanup CorpusLineReader r = Corpora.openM(aLemmaFile);

        for(;;) {
            if (!r.readLine()) break;
            mLemmas.add(r.lemma());
        }
    }

    /**
     *
     * @param aInFile file with candidates (in xml)
     * @param aOutFile lexicon file
     * @throws j00000000ava.io.IOException
     */
    private void proces(XFile aInFile, XFile aOutFile) throws IOException, XMLStreamException {
        final int minNrOfTokensPreF = getArgs().getInt("minNrOfTokensPreFilter");
        final int minNrOfFormsPreF  = getArgs().getInt("minNrOfFormsPreFilter");

        mW = IO.openPrintWriter(aOutFile);
        mW.printf("// minNrOfFormsPreFilter %d, minNrOfTokensPreFilter %d, minNrOfForms %d, minNrOfTokens %d\n", minNrOfFormsPreF, minNrOfTokensPreF, mMinNrOfForms, mMinNrOfTokens);
        mW.printf("// formsIdx %f, tokensIdx %f, coverageIdx %f\n", mFormsIdx, mTokensIdx, mCoverageIdx);
        mW.printf("// mRelBeam %f, mAbsBeam %f\n", mRelBeam, mAbsBeam);
        mW.printf("// Narrow: %s, splittingPrefixLen %d, lemmaFile %s\n\n", mNarrow, mSplittingPrefixLen, (mLemmas != null));

        XMLEventReader r = XmlUtils.openXmlReader(aInFile);
        r.nextEvent();  // root
        AEntry e = null;
        String pref = "";   // prefix of up to mSplittingPrefixLen characters

        for(; pref != null;) {
            SortedSet<AEntry> entries = new TreeSet<AEntry>();
            if (e != null) entries.add(e);

            // --- read single prefix entries ---
            for (;;) {
                e = AEntry.readEntry(r);
                //System.out.println("Entry:" + e);
                if (e == null) {pref = null; break;}
                if ( mNarrow != null && !(mNarrow.matcher(e.mLemma).matches()) ) continue;

                //System.out.println("Entry:" + e);
                if (e instanceof ADeclEntry) {
                    e.setParadigm(mParadigms);
                }
                else {
                    // todo
                }

                String ePref = e.mLemma.substring(0, Math.min(e.mLemma.length(), mSplittingPrefixLen));

                // --- prefix done ---
                if (!ePref.equalsIgnoreCase(pref)) {
                    pref = ePref;
                    break;
                }

                //System.out.println("E: " + e + "  added.");
                if ( e.mForms.size() >= minNrOfFormsPreF && e.totalFreq() >= minNrOfTokensPreF && (mConsiderDeriv || e instanceof  ADeclEntry)) {       // a quick pre-filter
                    e.filterFringes(mFringeThreashold);     // remove forms with very low frequencies
                    entries.add(e);
                    //System.out.println("E: " + e + "  added.");
                }
            }


            System.out.printf("Filtering prefix %s (size %d)\n", pref, entries.size());
            Log.info("Filtering prefix %s (size %d)", pref, entries.size());

            filterAndWrite(entries);
            entries = null;
        }

        r.close();
        mW.close();
    }



    /**
     * Repeatadly pulls out a lattice and filters it out, untill the pre-lexicon is empty.
     * @param aPreLexicon
     * @throws java.io.IOException
     */
    private void filterAndWrite(SortedSet<AEntry> aPreLexicon) throws IOException {
        while (!aPreLexicon.isEmpty()) {
            SortedSet<AEntry> lattice = pickALattice(aPreLexicon);   // picks a (maximal) set of entries whose forms form an inclusion-lattice
            SortedSet<AEntry> filtered = filterBestReliable(lattice);
            write(filtered, lattice);
            lattice  = null;
            filtered = null;
        }
    }



// -----------------------------------------------------------------------------
// Filtering
// -----------------------------------------------------------------------------

    /**
     * Returns (and removes) a set of entries, whose forms form an inclusion-lattice.
     * If an entry can be put into more than one lattice it is given into the one that is constructed first.
     *
     * @param aPreLexicon a set to extract the entries from. It has to contain at leas one entry.
     * @return a set of entries, whose forms forms form an inclusion-lattice.
     */
    private SortedSet<AEntry> pickALattice(SortedSet<AEntry> aPreLexicon) {
        SortedSet<AEntry> lattice      = new TreeSet<AEntry>();   // entries contained by a top
        SortedSet<String> sharedForms = new TreeSet<String>();  // forms of aTop (i.e. all forms of the set)

        // --- start with the first entry ---
        AEntry anEntry = Cols.getFirstOut(aPreLexicon);      // pulls out some (the first) entry

        lattice.add(anEntry);
        sharedForms.addAll(anEntry.getForms());

        for(;;) {
            SortedSet<AEntry> toAdd      = new TreeSet<AEntry>();   // entries to be added to lattice after one cycle thru the entries (iterators prevent immediate addition)
            SortedSet<String> formsToAdd = new TreeSet<String>();  // union of all forms in toAdd

            // --- go thru the prelex and find all subsumed/subsuming entries ---
            for (Iterator<AEntry> j = aPreLexicon.iterator(); j.hasNext(); ) {
                AEntry candidate = j.next();
                // -- subsumed by the current top --
                if ( sharedForms.containsAll(candidate.getForms()) ) {
                    toAdd.add(candidate);
                    j.remove();
                }
                // -- a new top --
                else if ( candidate.getForms().containsAll(sharedForms) ) {
                    toAdd.add(candidate);
                    j.remove();
                    formsToAdd.addAll(candidate.getForms());
                    sharedForms.addAll(candidate.getForms());
                }
            }

            lattice.addAll(toAdd);
            if (formsToAdd.isEmpty()) break;     // no new forms were added, i.e. the top stayed the same
        }

        return lattice;
    }


     /**
      * Get the the best realiable lex entries:
      *   1) Reliable: they must have occured with enough forms and tokens, and
      *   2) Best: they get a good score (a linear combination of # of forms, # of tokens and paradigm coverage)
      *      Good means within a specified interval from the best score.
      *   @todo do not count: entry.containsFormEndingWith("ov�") => weight--;
      *   @todo do not drop: entry.mLemma.endsWith("�")
      *
      * If there is a list of known lemmas:
      *    If at least one entry has its lemma in the list, drop those that don't
      */
    private SortedSet<AEntry> filterBestReliable(SortedSet<AEntry> aEntries) {
        //System.out.println("filterBestReliable");
        SortedSet<AEntry> filtered = new TreeSet<AEntry>(aEntries);

        // --- compare to the list of known lemmas ---
        if (!mLemmas.isEmpty()) filterByLemmas(filtered);

        // --- Find the maximal weight  ---
        int maxForms = 0;         // # of distinct forms
        int maxTokens = 0;        // # of distinct tokens
        double maxCoverage = 0.0; // paradigm coverage
        double maxScore = 0.0;

        for(AEntry entry : filtered) {
            // --- get characteristics ---
            int forms = entry.mForms.size();
            int tokens = entry.totalFreq();
            double coverage = entry.getPctOfPossibleForms();

            // calculate score
            double score = mFormsIdx * forms + mTokensIdx * tokens + mCoverageIdx * coverage;
            entry.setScore(score);

            // --- check maxima ---
            if (maxForms < forms) maxForms = forms;
            if (maxTokens < tokens) maxTokens = tokens;
            if (maxCoverage < coverage) maxCoverage = coverage;
            if (maxScore < score) maxScore = score;
        }

        // --- Ignore unreliable lattices ---
        if (maxForms < mMinNrOfForms || maxTokens < mMinNrOfTokens) filtered.clear();

        maxScore = maxScore * (1.0-mRelBeam) - mAbsBeam;

        //System.out.println("Filtering");
        // --- Keep only those entries that are above the threashold ---
        for(Iterator<AEntry> i = filtered.iterator(); i.hasNext(); ) {               //for(Iterator<AEntry> i :: filtered) {
            AEntry e = i.next();
            //System.out.println( Transliterate.transliterate(e.toString()) );
            if (e.getScore() < maxScore && !keepEntry(e) ) {
                i.remove();
                //System.out.println("removed");
            }
        }

        return filtered;
    }


    /**
     * Keep all nouns and adjectives.
     */
    private boolean keepEntry(AEntry e) {
        return (noFiltering != null) && e.getForms().size() >= noFilteringMin && noFiltering.matcher(e.getParadigm().lemmatizingEnding.getTag().toString() ).matches();
    }


    /**
     * If at least one entry has its lemma in the list of lemmas,
     * all the entries that do not, are deleted.
     *
     * @param aEntries entries to filter
     */
    private void filterByLemmas(SortedSet<AEntry> aEntries) {
        // --- check if at least one entry has a known lemma ---
        boolean someLemmaKnown = false;
        for(AEntry entry : aEntries) {
            if (mLemmas.contains(entry.mLemma)) {someLemmaKnown = true; break;}
        }
        if (someLemmaKnown) {
            for(Iterator<AEntry> i = aEntries.iterator(); i.hasNext(); ) {
                if (!mLemmas.contains(i.next().mLemma))
                    i.remove();
            }
        }
    }

// -----------------------------------------------------------------------------
// Output
// -----------------------------------------------------------------------------
    /**
     * Can produce multiple identical entries from several deriv-entries (different super-prdgms, equal sub-prdgms)
     */
    private void write(SortedSet<AEntry> aFillteredEntries, SortedSet<AEntry> aAllEntries) throws IOException {
        if (aFillteredEntries.isEmpty()) return;

        // --- approved entries ---
        for(AEntry entry : aFillteredEntries) {
            //mW.print("   ");
            writeEntry(entry);
        }

        // --- dropped entries ---
        if (mWriteDropped) {
            aAllEntries.removeAll(aFillteredEntries);
            for(AEntry entry : aAllEntries) {
                mW.print("// ");
                writeEntry(entry);
            }
        }

        mW.println();
    }

    private void writeEntry(AEntry aEntry) {
        if (aEntry instanceof ADeclEntry)
            writeEntry((ADeclEntry)aEntry);
        else
            writeEntry((ADerivEntry)aEntry);
    }

    private void writeEntry(ADerivEntry aEntry) {
        if (aEntry.mEntries.size() == 1) return;   // @todo hack move to merging?? or reading in
        mW.println("// "  + comment(aEntry));
        for (Map.Entry<StringPair,String[]> e : aEntry.mEntries.entrySet()) {
            String lemma = e.getKey().mFirst;
            String prdgmId = e.getKey().mSecond;
            DeclParadigm prdgm = mParadigms.getParadigm(prdgmId);
            //Err.uAssert(prdgm != null, "%s %s", prdgmId, Transliterate.transliterate(aEntry.mLemma) ) ;
            //Err.uAssert(aEntry.mLemma   != null, Transliterate.transliterate(aEntry.mLemma));
            //Err.uAssert(prdgm.lemmatizingEnding != null, prdgm.toString());

            mW.printf("%s  %s  %s\n", lemma, getStemStr(lemma, prdgm, e.getValue()), prdgmId); //, comment(aEntry));

        }
    }

    private void writeEntry(ADeclEntry aEntry) {
        DeclParadigm prdgm = mParadigms.getParadigm(aEntry.mParadigmId);
        Err.uAssert(prdgm != null, "%s %s", aEntry.mParadigmId, Transliterate.transliterate(aEntry.mLemma) ) ;
        Err.uAssert(aEntry.mLemma   != null, Transliterate.transliterate(aEntry.mLemma));
        Err.uAssert(prdgm.lemmatizingEnding != null, "%s", prdgm);

        String stemsStr = getStemStr(aEntry.mLemma, prdgm, aEntry.getStems());

        mW.printf("%s  %s  %s      //%s\n", aEntry.mLemma, stemsStr, aEntry.mParadigmId, comment(aEntry));
    }

    private static String getStemStr(final String aLemma, final DeclParadigm aPrdgm, final String[] aStems) {
        // --- fill gaps in seen stems with calculated ones ---
        String[] calculatedStems = aPrdgm.allStems(aLemma);  //@todo aEntry.mEpen
        for (int i = 0; i < aStems.length; i++) {
            if (aStems[i].equals("")) {
                aStems[i] = calculatedStems[i];
            }
        }

        // --- ensure that stems have the proper capitalization (determined by the lemmas) ---
        if (Caps.capitalization(aLemma).firstCap()) {
            for (int i = 0; i < aStems.length; i++) {
                aStems[i] = Caps.toFirstCapital(aStems[i]);
            }
        }

        // --- create a string of stem specification for the lexicon ---
        StringBuilder stemsStr = new StringBuilder(aStems[0]);
        if (aStems.length > 1) {
            for (int i = 0; i < aStems.length; i++) {
                if (!aStems[i].equals(aStems[0]))
                    stemsStr.append( String.format(" @%d %s", i+1, aStems[i]) );
            }
        }

        return stemsStr.toString();
    }

    private String comment(AEntry aEntry) {
        StringBuilder tmp = new StringBuilder(aEntry.mForms.size() * 12);      // @todo estimate size
        tmp.append(aEntry.mForms.size()).append('/').append(aEntry.totalFreq()).append(' ');
        if (aEntry instanceof ADerivEntry) {
            tmp.append(((ADerivEntry)aEntry).getPrdgmId()).append(' ');
        }

        tmp.append(Cols.toString(aEntry.getFormsMap().entrySet(), ColPrinter.<String,FormInfo>map()));

        return tmp.toString();
    }

}