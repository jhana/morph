package morph.acq;

import java.util.Arrays;
import java.util.Set;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import morph.ma.lts.DecInfo;
import morph.ma.lts.Lt;
import util.str.Strings;
import util.err.Log;

class ADeclEntry extends AEntry {

    public String[] getStems() {
        return mStems;
    }
    
    
    @Override
    public void update(String aForm, CapFreq aFreqs, boolean aName, Set<Lt> aLts) {
        for (Lt lt : aLts) {
            final DecInfo declInfo = (DecInfo)lt.info();
            if (mStems == null) {
                mStems = new String[declInfo.getPrdgm().nrOfStems()];
                mDeclInfos = new String[declInfo.getPrdgm().nrOfStems()];
            }
            updateStems(mStems, mDeclInfos, aForm, declInfo);
        }

        addForm(aForm, aFreqs, aLts);
    }        
    
    @Override
    public void merge(AEntry aE) {
        ADeclEntry e = (ADeclEntry) aE;
        assert mLemma.equals(e.mLemma) && mParadigmId.equals(e.mParadigmId);
        Log.assertW(mDeclInfos != null, "x1" + Arrays.toString(mStems) + "\n " + this);
        Log.assertW(e.mDeclInfos != null, "x2" + Arrays.toString(e.mStems) + "\n " + e);
        
        mergeStems(this, mStems, mDeclInfos, e, e.mStems, e.mDeclInfos);
        mergeForms(e);
    }
    

    @Override
    public void write(XMLStreamWriter aW) throws XMLStreamException {
        String stems = getStemsString(mStems);
        String dis   = getStemsString(mDeclInfos);
        
         aW.writeStartElement("l");
            aW.writeCharacters(mLemma);
            aW.writeStartElement("P");
                aW.writeCharacters(mParadigmId);
            aW.writeEndElement();
            aW.writeStartElement("s");
                aW.writeCharacters(stems);
            aW.writeEndElement();
            aW.writeStartElement("di");
                aW.writeCharacters(dis);
            aW.writeEndElement();

            writeForms(aW);

        aW.writeEndElement();
        aW.writeCharacters("\n");
    }

    @Override
    public void read(XMLEventReader aR) throws XMLStreamException {
        aR.nextEvent(); // l;
            mLemma = aR.nextEvent().asCharacters().getData();
            aR.nextEvent(); // P;
            mParadigmId = aR.nextEvent().asCharacters().getData();
            aR.nextEvent(); // /P;
            aR.nextEvent(); // s
            mStems     = Strings.cColonPattern.split( aR.nextEvent().asCharacters().getData(), -1);
            aR.nextEvent(); // /s
            aR.nextEvent(); // di
            mDeclInfos = Strings.cColonPattern.split( aR.nextEvent().asCharacters().getData(), -1);
            aR.nextEvent(); // /di
            readForms(aR);
        aR.nextEvent(); // /l
    }
    
// =============================================================================
// Implementation <editor-fold desc="Implementation">
// ==============================================================================
    private String[] mStems;

    private String[] mDeclInfos;
    
// </editor-fold>

}
