package morph.acq;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import lombok.Cleanup;
import util.io.*;
import util.err.Err;
import util.err.UException;
import morph.util.MUtilityBase;
import morph.io.*;
import util.col.Cols;
import util.str.Caps;

/**
 * Input: corpus (ptd/tnt/plain format), 
 * Output: list of forms with frequencies, sorted by frequency
 *
 * @todo sort by freq or alpha
 * @todo do something about capitalization
 * @todo keep info about capitalization and do something about it
 */
public class CollectForms extends MUtilityBase {
    private int mWords; // counting words
    //private Counter<String> mCounter = new Counter<String>();
    private final Map<String, CapFreq> mCounter = new HashMap<String, CapFreq>();

    private Pattern narrow;
    
    private long cleanupMem;
    //private int cleanupMinFreq;

    /**
     */
    public static void main(String[] args) throws IOException {
        new CollectForms().go(args);
    }
    
    private void go(String[] aArgs) throws IOException {
        argumentBasics(aArgs,2);
        System.out.println("Args: " + Cols.toString(Arrays.asList(aArgs)));
        
        XFile inFile   = getArgs().getDirectFile(0);
        XFile outFile  = getArgs().getDirectFile(1);
        Err.checkIFiles("in", inFile);
        Err.checkOFiles("out", outFile);

        cleanupMem     =10000000L; //= getArgs().getLong("cleanup.minMem", 0L);
        //cleanupMinFreq = getArgs().getInt("cleanup.minFreq", 2);
        
        narrow = getArgs().getPattern("narrow", null);
        
        
        profileStart();
        go(inFile, outFile);
        profileEnd("Forms collected in %f");
    }
    
    
    private void go(XFile aInFile, XFile aOutFile) throws IOException {
        collect(aInFile);
        filter();
        dump(aOutFile);
    }    
    
    
    private void collect(XFile aInFile) throws IOException {
        final int corpusSizeLimit = getArgs().getInt("corpusSizeLimit", Integer.MAX_VALUE);
        final boolean mergeCaps   = getArgs().getBool("mergeCaps", true);
        
        @Cleanup WordReader r = Corpora.openWordReader(aInFile);
        try {
            for(;;) {
                r.nextWord();
                if (r.word() == null) break;
                if (mWords >= corpusSizeLimit) break;
                mWords++;
                
                if (narrow != null && !narrow.matcher(r.word()).find()) continue;
                
                String lcWord = r.word();
                if (mergeCaps) lcWord = lcWord.toLowerCase();
                CapFreq freqs = mCounter.get(lcWord);
                if (freqs == null) {
                    freqs = new CapFreq();
                    mCounter.put(lcWord, freqs);
                }
                freqs.addFreq( Caps.capitalization(r.word()), 1 );

                if (mWords % 1000000 == 0) System.out.println("Done so far:" + mWords/1000000 + "M");

                // remove low freq entries if not enough memory
                if (mWords % 256 == 0 && 0 < cleanupMem && needsMem()) {
                    cleanup();
                }
            }
        }
        catch(IOException e) {
            Err.fErr(e, "I/O error on file %s\n", aInFile);
        }
        catch (UException e) {
            Err.fErr(e, "Format exception (word %d)", mWords);      // @todo line?
        }
    }

    public boolean needsMem() {
        return (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) < cleanupMem;
    }


    private void cleanup() {
        System.out.println("Free  memory: " + Runtime.getRuntime().freeMemory());
        System.out.println("Max   memory: " + Runtime.getRuntime().maxMemory());
        System.out.println("Total memory: " + Runtime.getRuntime().totalMemory());
        int minFreq = 0;
        int droppedCount = 0;
        for (;;) {
            Runtime.getRuntime().gc();

            if ( !needsMem() || mCounter.isEmpty() ) break;
            minFreq++;

            for(Iterator<Map.Entry<String, CapFreq>> it = mCounter.entrySet().iterator(); it.hasNext();) {
                if (it.next().getValue().getTotalFreq() < minFreq) {
                    it.remove();
                    droppedCount++;
                }
            }
        }
        System.out.printf("Dropped %d entries with frequency below %d\n", droppedCount, minFreq);
    }

    /**
     * Drops 
     * - all entries with frequency below pref:freqThreashold 
     * - entries with length below pref:lenThreashold 
     * - numbers
     */
    private void filter() {
        final int freqThreashold = getArgs().getInt("freqThreshold");
        final int lenThreashold  = getArgs().getInt("lenThreshold");
        final boolean dropNumbers = getArgs().getBool("dropNumbers");
        
        for(Iterator< Map.Entry<String,CapFreq> > i = mCounter.entrySet().iterator(); i.hasNext(); ) {
            Map.Entry<String,CapFreq> e = i.next();
            // remove infrequent, short words or non-words
            if ( e.getValue().getTotalFreq() < freqThreashold || 
                 e.getKey().length() < lenThreashold || 
                 ( dropNumbers && isSpecial(e.getKey()) )  ) 
                i.remove();
        }
    }

    /**
     * Only letters or hyphens (not all hyphens)
     */
    private boolean isSpecial(String aString) {
        for (int i = 0; i < aString.length(); i++) {
            char c = aString.charAt(i);
            if (!isLetterish(c)) return true;   // contains a non-letter non-hyphen character
        }

        // --- contains at least some pure letter ---
        for (int i = 0; i < aString.length(); i++) {
            if (Character.isLetter(aString.charAt(i))) return false;    // a word
        }
        
        return true;   // only hyphens 
    }

    private boolean isLetterish(char aChar) {
        return Character.isLetter(aChar) || aChar == '-';
    }
    
    private void dump(XFile aOutFile) {
        final boolean printFreq = !getArgs().getBool("noFreq");

        PrintWriter w = null;
        
        try {
            System.out.printf("Saving - words %d, entries %d\n", mWords, mCounter.size());
            w = IO.openPrintWriter(aOutFile);        

            for(Map.Entry<String,CapFreq> e : Cols.sort(mCounter.entrySet(), comp) ) {
                w.print(e.getKey());
                if (printFreq) {
                    w.print('\t');
                    w.print(e.getValue());
                }
                w.println();
                
            }
        }
        catch(IOException a) {
            System.err.printf("Error opening %s\n", aOutFile);
        }
        finally {
            if (w != null) w.close();
        }
    }    
    
    private final Comparator< Map.Entry<String,CapFreq> > comp = new Comparator<Map.Entry<String,CapFreq>>() {
        public int compare(Map.Entry<String,CapFreq> o1, Map.Entry<String,CapFreq> o2) {
            int tmp = o2.getValue().getTotalFreq() - o1.getValue().getTotalFreq();
            if (tmp != 0) return tmp;
            return o1.getKey().compareTo(o2.getKey());
        }
    };
    
//    protected List<Map.Entry<String,CapFreq>> sort(Set<Map.Entry<String,CapFreq>> aMap) {
//        List<Map.Entry<String,CapFreq>> list = new ArrayList<Map.Entry<String,CapFreq>>(aMap);
//        Collections.sort(list,comp);
//        return list;
//    }
    
//    private void dump(XFile aOutFile) {
//        PrintWriter w = null;
//        SortedSet<Map.Entry<String,Integer>> entries = mCounter.setSortedByFreq();  // @todo sort alphabetically instead
//        
//        try {
//            System.out.printf("Saving - words %d, entries %d\n", mWords, entries.size());
//            w = IO.openPrintWriter(aOutFile);        
//
//            for(Map.Entry<String,Integer> e : entries) {
//                w.printf("%s\t%d\n", e.getKey(), e.getValue());
//            }
//        }
//        catch(IOException a) {
//            System.out.printf("Error opening %s\n", aOutFile);
//        }
//        finally {
//            if (w != null) w.close();
//        }
//    }    
}
