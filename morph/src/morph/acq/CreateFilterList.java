package morph.acq;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;
import javax.xml.stream.XMLStreamException;
import lombok.Cleanup;
import morph.io.Corpora;
import morph.io.WordReader;
import util.err.Err;
import util.io.*;
import morph.util.MUtilityBase;
import morph.ma.*;
import morph.ma.lts.*;
import morph.ma.module.WordEntry;
import morph.ma.module.WordModule;
import morph.ts.Tag;
import morph.ts.Tags;
import util.col.Counter;
import util.col.MultiMap;
import util.str.StrSet;

//public class CreateFilterList extends MUtilityBase {
//
//    /**
//     */
//    public static void main(String[] args) throws IOException, XMLStreamException {
//        new CreateFilterList().go(args);
//    }
//
//// =============================================================================
//// Implementation
//// =============================================================================
//
//// --- Options ---
//    private int mCorpusSizeLimit;
//    private Pattern mNarrow;        // @todo replace by reseting matcher
//
//// --- ---
//    /**
//     * Morphological analyzer
//     */
//    private Morph mMorph;
//    private WordModule mCcWords;
//    /**
//     * Counter of processed words
//     */
//    private int mWords;
//    private XFile mOutFile;
//
//    private void go(String[] aArgs) throws java.io.IOException, XMLStreamException {
//        argumentBasics(aArgs, 2);
//
//        XFile inFile = getArgs().getDirectFile(0);
//        mOutFile = getArgs().getDirectFile(1);
//        Err.checkIFiles("in", inFile);
//        Err.checkOFiles("out", mOutFile);
//
//
//        mCorpusSizeLimit = getArgs().getInt("corpusSizeLimit", Integer.MAX_VALUE);
//        System.out.println("mCorpusSizeLimit " + mCorpusSizeLimit);
//
//        mNarrow = getArgs().getPatternNE("narrow");
//        System.out.println("narrow:" + mNarrow);
//
////        int minNrOfForms    = getArgs().getInt("minNrOfForms");
////        int nrOfMergedFiles = getArgs().getInt("nrOfMergedFiles");
//        prepareMa();
//
//        profileStart("Running MA...");
//        process(inFile);
//        profileEnd();
//
//        dump(2, mOutFile);
////        dump(0, mOutFile.addExtension("g"));
////        dump(1, mOutFile.addExtension("n"));
////        dump(2, mOutFile.addExtension("c"));
//    }
//
//    private void dump(int idx, XFile aFile) throws IOException {
//        PrintWriter w = IO.openPrintWriter(aFile);
//        for (WordInfo wi : anals.values()) {
//            Counter<StrSet> counter = wi.counters.get(idx);
//            counter.calculate();
//            if (counter.sigma() > 3) {
//                w.print(wi.form);
//                w.print(' ');
//
//                for (Map.Entry<StrSet, Integer> e : counter.getMap().entrySet()) {
//                    w.print(e.getKey().getString() + ":" + e.getValue());
//                    w.print(' ');
//                }
//                w.println();
//
//                w.println(toString(counter, wi.examples.get(idx)));
//            }
//        }
//        IO.close(w);
//
//    }
//
//    public String toString(Counter aCounter, MultiMap aExamples) {
//        StringBuilder tmp = new StringBuilder(aCounter.size() * 4);
//        SortedSet<StrSet> keys = new TreeSet<StrSet>(aCounter.getMap().keySet());
//
//        // format: key = freq
//        for (StrSet key : keys) {
//            tmp.append("//  ");
//            tmp.append(key).append('=').append(aCounter.frequency(key));
//            tmp.append(aExamples.get(key)).append('\n');
//        }
//        return tmp.toString();
//    }
//
//    private void prepareMa() throws java.io.IOException {
//        mMorph = MorphCompiler.factory().compile(getArgs());
//        Err.assertFatal(mMorph != null, "Cannot create Morph object");
//
//        mCcWords = (WordModule) mMorph.getModule("Words");
//
//    }
//    Map<String, WordInfo> anals = new HashMap<String, WordInfo>();
//
//    static class WordInfo {
//        final List<Counter<StrSet>> counters = new ArrayList<Counter<StrSet>>(3);
//        final List<MultiMap<StrSet,String>> examples = new ArrayList<MultiMap<StrSet,String>>(3);
//        final String form;
//
//        public WordInfo(String aForm) {
//            form = aForm;
//            for (int i=0; i < 3; i++) {
//                counters.add(new Counter<StrSet>());
//            }
//            for (int i=0; i < 3; i++) {
//                examples.add(new MultiMap<StrSet,String>());
//            }
//        }
//
//        public void add(int aIdx, StrSet aPossibilities, String aCtx) {
//            counters.get(aIdx).add(aPossibilities);
//            examples.get(aIdx).add(aPossibilities, aCtx);
//        }
//
//
////        // frequency of case-ambiguity classes
////        private final Counter<StrSet> casesFreq = new Counter<StrSet>();
////        private final MultiMap<StrSet, String> examples = new MultiMap<StrSet, String>();
////
////        private final Counter<StrSet> genFreq = new Counter<StrSet>();
////        private final MultiMap<StrSet, String> genExamples = new MultiMap<StrSet, String>();
////
////        private final Counter<StrSet> nrFreq = new Counter<StrSet>();
////        private final MultiMap<StrSet, String> nrExamples = new MultiMap<StrSet, String>();
////
//////        public void addCases(StrSet aCases) {
//////            casesFreq.add(aCases);
//////        }
////
////        public void addCases(StrSet aCases, String aPrep) {
////            casesFreq.add(aCases);
////            examples.add(aCases, aPrep);
////        }
////
////        public void addGen(StrSet aCases, String aCtx) {
////            casesFreq.add(aCases);
////            genExamples.add(aCases, aCtx);
////        }
////
////        public void addNr(StrSet aCases, String aCtx) {
////            casesFreq.add(aCases);
////            nrExamples.add(aCases, aCtx);
////        }
//    }
//
//    private void process(XFile aInFile) throws IOException, XMLStreamException {
//        final int corpusSizeLimit = getArgs().getInt("corpusSizeLimit", Integer.MAX_VALUE);
//
//        @Cleanup WordReader r = Corpora.openWordReader(aInFile);
//
//        int ctxSize = 3;
//        Context ctx = new Context(r, ctxSize, ctxSize);
//        ctx.prepare();
//
////                if (mWords >= corpusSizeLimit) break;
////                mWords++;
//
//        for (; ctx.hasNext();) {
//            Lts lts = mMorph.analyze(ctx.curForm());
//            doFiltering(ctx, lts);
//            ctx.update(lts);
//        }
//    }
//
//    private void doFiltering(Context aCtx, Lts aLts) {
//        filter_prepXCase(aCtx, aLts);
//        filter_prepAdjNoun(aCtx, aLts);
//    }
//
//    private void filter_prepXCase(Context aCtx, Lts aLts) {
//        String prep = aCtx.curForm();
//        // TODO extract from lts
//        // @todo possibly all must be R as said by morph (not just cc words)
//        Set<WordEntry> wordEntries = mCcWords.getEntries(prep.toLowerCase());
//        if (wordEntries == null) {
//            return;
//        }
//
//        Set<Tag> tags = getTags(wordEntries, "R.*");
//        if (tags.isEmpty()) {
//            return;
//        }
//        Set<Character> rCases = Tags.collectValues(tags, int.casex);
//
//        String nextWord = aCtx.getNextWord(0);
//        if (nextWord == null) {
//            return;
//        }
//
//        Lts lts = mMorph.analyze(nextWord);
//
//
//        // collect noun cases
//        Set<Character> nCases = new HashSet<Character>();
//        for (Lt lt : lts.col()) {
//            Tag tag = lt.tag();
//            if (tag.getCase() != '-') {
//                nCases.add(tag.getCase());
//            }
//        }
//        if (nCases.isEmpty()) {
//            return;
//        }
//
//
//        rCases.retainAll(nCases);
//        StrSet inters = new StrSet(rCases);
//
//        getWi(nextWord).add(2,inters, prep);
//    }
//
//    private WordInfo getWi(String aForm) {
//        WordInfo wi = anals.get(aForm);
//        if (wi == null) {
//            wi = new WordInfo(aForm);
//            anals.put(aForm, wi);
//        }
//
//        return wi;
//
//    }
//    private void filter_prepAdjNoun(Context aCtx, Lts aLts) {
//        String prep = aCtx.curForm();
//        // TODO extract from lts
//        // @todo possibly all must be R as said by morph (not just cc words)
//        Set<WordEntry> wordEntries = mCcWords.getEntries(prep.toLowerCase());
//        if (wordEntries == null) {
//            return;
//        }
//
//        Set<Tag> tags = getTags(wordEntries, "R.*");
//        if (tags.isEmpty()) {
//            return;
//        }
//
//        // A N
//        String word1 = aCtx.getNextWord(0);
//        String word2 = aCtx.getNextWord(1);
//
//        if (word1 == null || word2 == null) return;
//        
//        Lts lts1 = mMorph.analyze(word1);
//        Lts lts2 = mMorph.analyze(word2);
//
//        Collection<Tag> tags1 = lts1.tags();
//        Collection<Tag> tags2 = lts2.tags();
//
//        if (!allTags(tags1, Pattern.compile("(A|P[SDwqz]|Cr|Ca[\\-]).*")) ) return;
//        //if (!allTags(tags2, Pattern.compile("(N|A|P[SDwqz]|C[ra]).*")) ) return;
//
//        // todo do intersection of gn at once
//
//        StrSet genders1 = new StrSet( Tags.collectValueStr(tags1, int.gender) );
//        StrSet genders2 = new StrSet( Tags.collectValueStr(tags1, int.gender) );
//        StrSet genders = genders1.inters(genders2);
//
//
//        StrSet numbers1 = new StrSet( Tags.collectValueStr(tags1, int.nr) );
//        StrSet numbers2 = new StrSet( Tags.collectValueStr(tags1, int.nr) );
//        StrSet numbers = numbers1.inters(numbers2);
//
////        WordInfo wi1 = getWi(word1);
////        String ctx1 = prep + " + _ + " + word2;
////        wi1.add(0, genders, ctx1);
////        wi1.add(1, numbers, ctx1);
//
//        WordInfo wi2 = getWi(word2);
//        String ctx2 = prep + " + " + word1 + " + _";
//        wi2.add(0, genders, ctx2);
//        wi2.add(1, numbers, ctx2);
//
//
//        
//    }
//
//    protected boolean allTags(Collection<Tag> aTags, Pattern aPattern) {
//        for  (Tag tag : aTags) {
//            if (!tag.matches(aPattern)) return false;
//        }
//        return true;
//    }
//
//
//    private void processX(XFile aInFile) throws IOException, XMLStreamException {
//        final int corpusSizeLimit = getArgs().getInt("corpusSizeLimit", Integer.MAX_VALUE);
//
//        @Cleanup WordReader r = Corpora.openWordReader(aInFile);
//        for (;;) {
//            r.nextWord();
//            if (r.word() == null) {
//                break;
//            }
//            if (mWords >= corpusSizeLimit) {
//                break;
//            }
//            mWords++;
//
//            String cur = r.word();
//
//            // @todo possibly all must be R as said by morph (not just cc words)
//            Set<WordEntry> wordEntries = mCcWords.getEntries(cur.toLowerCase());
//            if (wordEntries == null) {
//                continue;
//            }
//
//            Set<Tag> tags = getTags(wordEntries, "R.*");
//            if (tags.isEmpty()) {
//                continue;
//            }
//            Set<Character> rCases = Tags.collectValues(tags, int.casex);
//
//
//            // read N/Adj
//            r.nextWord();
//            if (r.word() == null) {
//                break;
//            }
//            if (mWords >= corpusSizeLimit) {
//                break;
//            }
//            mWords++;
//
//            Lts lts = mMorph.analyze(r.word());
//
//
//            // collect noun cases
//            Set<Character> nCases = new HashSet<Character>();
//            for (Lt lt : lts.col()) {
//                Tag tag = lt.tag();
//                if (tag.getCase() != '-') {
//                    nCases.add(tag.getCase());
//                }
//            }
//            if (nCases.isEmpty()) {
//                continue;
//            }
//
//
//            rCases.retainAll(nCases);
//            StrSet inters = new StrSet(rCases);
//
//            WordInfo wi = anals.get(r.word());
//            if (wi == null) {
//                wi = new WordInfo(r.word());
//                anals.put(wi.form, wi);
//            }
//            wi.add(-1,inters, cur);
//        }
//    }
//
//    Set<Tag> getTags(Set<WordEntry> wordEntries, String aTagPattern) {
//        Set<Tag> tags = new HashSet<Tag>();
//        for (WordEntry we : wordEntries) {
//            if (we.getTag().getPOS() != 'R') {
//                return Collections.<Tag>emptySet();
//            }
//            tags.add(we.getTag());
//        }
//        return tags;
//    }
//}
