package morph.acq;

import util.str.Strings;
import java.io.*;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;
import javax.xml.stream.XMLStreamException;
import util.col.Counter;
import util.err.Err;
import util.io.*;
import morph.ma.*;
import morph.ma.lts.*;
import morph.common.UserLog;
import morph.util.MUtilityBase;
import util.err.Log;


public class RankTails extends MUtilityBase {


// -----------------------------------------------------------------------------
    
    /**
     */
    public static void main(String[] args) throws IOException, XMLStreamException {
        new RankTails().go(args);
    }

// -----------------------------------------------------------------------------
    
// --- Options ---    
    private int mCorpusSizeLimit;
    private int mDumpThreshhold;
    private Pattern mNarrow;        // @todo replace by reseting matcher

// --- ---    
    
    /**
     * Morphological analyzer
     */
    private Morph mMorph;
    morph.ma.module.WordModule mWordModule;

    private int mWords;
    
    XFile mOutFile;

// -----------------------------------------------------------------------------
    
    private void go(String[] aArgs) throws java.io.IOException, XMLStreamException {
        argumentBasics(aArgs,2);
        
        XFile inFile = getArgs().getDirectFile(0);
        mOutFile     = getArgs().getDirectFile(1);
        Err.checkIFiles("in", inFile);
        Err.checkOFiles("out", mOutFile);

        mCorpusSizeLimit = getArgs().getInt("corpusSizeLimit", Integer.MAX_VALUE);
        mDumpThreshhold  = getArgs().getInt("dumpThreshold");
        mNarrow          = getArgs().getPatternNE("narrow");
        
        prepareMa();

        profileStart("Getting stems...");
        Counter<String> stems = getStems(inFile);
        profileEnd("Done.\n");
        
        profileStart("Getting tails...");
        getTails(stems);
        profileEnd("Done.\n");
    }

    private void prepareMa() throws java.io.IOException {
        // --- configure ma ---
        getArgs().setBy("Guesser.markEpen", "markEpen");
        getArgs().set("Guesser.leo.enabled", false);
        getArgs().set("Guesser.handleDerivations", false);   
        getArgs().set("collectInfo",          true);   // collect info about prdgms and epenthesis
        getArgs().set("compile", true);
        Lt.setInfoIrelevant(false);      // Info necessary to prevent conflating equal analyses with different prdgms

        mMorph = MorphCompiler.factory().compile(getArgs());
        Err.assertFatal(mMorph != null, "Cannot create Morph object");

        // --- get filter ---
        mWordModule = new morph.ma.module.WordModule();
        mWordModule.init("filter.words", getArgs());
        mWordModule.compile(new UserLog());
        mWordModule.configure();
        
        Log.info("MA Loaded\n");
    }
    
    
    private Counter<String> getStems(XFile aInFile) throws IOException, XMLStreamException {
        LineNumberReader r = IO.openLineReader(aInFile);
        
        Counter<String> stemCounter = new Counter<String>();

        for(; mWords < mCorpusSizeLimit;) {
            String line = r.readLine();
            if (line == null) break;
            //if ( mNarrow != null && !mNarrow.matcher(line).matches() ) continue;
            
            String[] ff = Strings.cWhitespacePattern.split(line);
            Err.uAssert(ff.length == 2,"Format error: [%d] %s", r.getLineNumber(), line);

            String form = ff[0];
            int freq = Integer.parseInt( ff[1] );
                    
            Lts lts = mMorph.analyze(form);     

            filter(form,lts);

            Set<String> stems = new TreeSet<String>();
            for (Lt lt : lts) {
                if (lt.info() instanceof DecInfo) {
                    DecInfo info = (DecInfo) lt.info();
                    stems.add( info.getLemmatizingStem() );
                }
            }

            for (String stem : stems) {
                stemCounter.add(stem);
            }
            
            mWords++;
            Log.dot(mWords);
        }
        r.close();
        return stemCounter;
    }
    
    private void filter(String aForm, Lts aLts) {
        Set<Lt> lts = aLts.lts();
        Set<morph.ma.module.WordEntry> wordEntries = mWordModule.getEntries(aForm);
        //System.out.println("Filtering: " + (wordEntries == null ? null : wordEntries.size()) );
        if (wordEntries == null) return;
        
        for (Iterator<Lt> i = lts.iterator(); i.hasNext();) {
            Lt lt = i.next();
            if ( !lt.tag().POSIn("NAVD") || !contains(wordEntries, lt)) {       // @todo configurable
                i.remove();
            }
        }
        
    }

    private boolean contains(Set<morph.ma.module.WordEntry> aWordEntries, Lt aLt) {
        for (morph.ma.module.WordEntry we : aWordEntries) {
            if ( we.getLemma().equals(aLt.lemma()) && we.getTag().equals(aLt.tag())) return true;
        }
        return false;
    }
    
    
    private void getTails(Counter<String> aCounter) throws IOException, XMLStreamException {
        Counter<String> tailCounter = new Counter<String>();
        int maxSuffixLen = 4;
        int minRootLen = 1;
        
        for (Iterator< Map.Entry<String,Integer> > i = aCounter.entrySet().iterator(); i.hasNext(); ) {
            Map.Entry<String,Integer> e = i.next(); i.remove();
            String stem  = e.getKey();
            Integer freq = e.getValue();

            int maxLen = Math.min(maxSuffixLen, stem.length()-minRootLen);
            
            for(int suffixLen = 1; suffixLen <= maxLen; suffixLen++) {
                String suffix = Strings.getTail(   stem, suffixLen);
                tailCounter.add(suffix, freq);
            }
        }
        dump(tailCounter);
    }
    
    /**
     * Writes out lexical entries in LexStat object into a temporary file.
     * These files are then merged by merge.
     *
     */
    private void dump(Counter<String> aCounter) throws IOException, XMLStreamException {
        PrintWriter w = IO.openPrintWriter(mOutFile);
        for (Map.Entry<String,Integer> e : aCounter.entrySet()) {
            w.print(e.getValue());
            w.print("\t");
            w.println(e.getKey());
        }
        w.close();
    }

}
