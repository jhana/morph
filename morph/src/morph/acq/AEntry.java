package morph.acq;

import java.io.IOException;
import java.util.*;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.XMLEvent;
import util.col.Cols;
import morph.ma.lts.DecInfo;
import morph.ma.lts.Lt;
import morph.ma.module.prdgm.DeclParadigm;
import morph.ma.module.prdgm.Ending;
import morph.ma.module.Paradigms;
import util.err.Err;

abstract class AEntry implements Comparable<AEntry> {
    String mLemma;
    String mParadigmId;
    protected final SortedMap<String,FormInfo> mForms = new TreeMap<String,FormInfo>();

    private double mScore;

    // --- cached ---
    private DeclParadigm mParadigm; // can be null
    private double mPctOfPossibleForms = 0.0;

    private int mTotalFreq = -1;


    public static AEntry readEntry(XMLEventReader aR) throws IOException, XMLStreamException {
        XMLEvent e = XmlUtils.peekTag(aR);
        if (e.isEndElement()) return null;
        String localPart = e.asStartElement().getName().getLocalPart();

        AEntry entry;
        if (localPart.equals("l")) {
            entry = new ADeclEntry();
        }
        else {
            entry = new ADerivEntry();
        }
        entry.read(aR);

        return entry;
    }

// -----------------------------------------------------------------------------
// Attributes
// -----------------------------------------------------------------------------

    public String getLemma() {
        return mLemma;
    }

    public String getPrdgmId() {
        return mParadigmId;
    }

    /**
     * Returns the set of encounterred forms.
     * The set's iterator will return the keys in ascending order.
     * ? Why is this not a sorted set??
     *
     */
    public Set<String> getForms() {
        return mForms.keySet();
    }

    public Map<String,FormInfo> getFormsMap() {
        return mForms;
    }

// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------

    /*
     * @param aForm lower-case form
     * @param aCap  capitalization of the original form
     */
    public abstract void update(String aForm, CapFreq aFreqs, boolean aName, Set<Lt> aLts);

    public abstract void merge(AEntry aE);

    public abstract void write(XMLStreamWriter aW) throws XMLStreamException;
    public abstract void read(XMLEventReader aR) throws XMLStreamException;


// -----------------------------------------------------------------------------
// Filtering Info
// -----------------------------------------------------------------------------
    /**
     * Returns the set of encounterred forms.
     * The set's iterator will return the keys in ascending order.
     * ? Why is this not a sorted set??
     *
     */
    @Deprecated
    Set<String> forms() {
        return mForms.keySet();
    }

    public double getScore() {
        return mScore;
    }

    public void setScore(double aScore) {
        mScore = aScore;
    }

    int totalFreq() {
        if (mTotalFreq == -1) {
            mTotalFreq = 0;
            for (FormInfo fi : mForms.values())
                mTotalFreq += fi.freq();
        }
        return mTotalFreq;
    }


    public void setParadigm(Paradigms aParadigms) {
        mParadigm = aParadigms.getParadigm(mParadigmId);
        Err.uAssert(mParadigm != null, "Paradigm not found %s (Lemma %s)", mParadigmId, mLemma);    // @todo warning?
    }

    public DeclParadigm getParadigm() {
        return mParadigm;
    }

    public double getPctOfPossibleForms() {
        if (mPctOfPossibleForms == 0.0) {
            Err.uAssert(mParadigm != null, "Paradigm not found %s (Lemma %s)", mParadigmId, mLemma);    // @todo warning ?
            int possForms = mParadigm.getPossEndingsNr();
            mPctOfPossibleForms = (1.0*mForms.size()) / (1.0*possForms);
        }
        return mPctOfPossibleForms;
    }

// ------------------------------------------------------------------------------
// Standard (toString, comparisons)
// ------------------------------------------------------------------------------

    @Override
    public String toString() {
        return String.format("%s:%s:%s", mLemma, mParadigmId, mForms);
    }

    /**
     * Compares only lemma and prdgm.
     * This comparison is nearly case insensitive - it is equivalent to comparing
     * by 3 keys, in this order:
     * <ul>
     * <li>case-insensitive lemmas,
     * <li>case sensitive lemmas,
     * <li>paradigms
     * </ul>
     */
    @Override
    public int compareTo(AEntry o) {
        int tmp = mLemma.compareToIgnoreCase(o.mLemma);
        if (tmp != 0) return tmp;

        tmp = mLemma.compareTo(o.mLemma);
        if (tmp != 0) return tmp;

        return mParadigmId.compareTo(o.mParadigmId);
    }


    /**
     * Compares only lemma and prdgm.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AEntry)) return false;
        AEntry e = (AEntry) obj;
        return mLemma.equals(e.mLemma) && mParadigmId.equals(e.mParadigmId);
    }

//    public int hashCode() {
//        return mLemma.hashCode() + mParadigmId.hashCode();
//    }

    /**
     * Remove forms with very low frequencies.
     * Remove forms whose frequency is below (average frequency * aFringeThreashold).
     *
     * Invalidates any cached values, except totalFreq.
     */
    public void filterFringes(double aFringeThreashold) {
        totalFreq();
        long minFreq = Math.round( mTotalFreq / mForms.size() * aFringeThreashold );
        //System.out.println("minFreq=" + minFreq);
        for (Iterator<Map.Entry<String,FormInfo>> i = mForms.entrySet().iterator(); i.hasNext();) {
            Map.Entry<String,FormInfo> e = i.next();
            if (e.getValue().freq() < minFreq) {
                //System.out.println("Removing " + e);
                i.remove();
            }
        }

        totalFreq();
    }

// =============================================================================
// Implementation <editor-fold desc="Implementation">
// ==============================================================================



    protected static String getStemsString(final String[] aStems) {
        // more effective
        if (aStems == null) return "%";

        return Cols.toString(Arrays.asList(aStems), Cols.<String>emptyNullPrinter(), ':');
    }



    protected static void updateStems(String[] aStems, String[] aDeclInfos, final String aForm, final DecInfo aDeclInfo) {
        try {
            String prevStem = aStems[ aDeclInfo.getEnding().getStemId() ];
            String newStem = aDeclInfo.getActualStem();

            if (prevStem == null) {
                aStems[ aDeclInfo.getEnding().getStemId() ] = newStem;
                aDeclInfos[ aDeclInfo.getEnding().getStemId() ] = aDeclInfo.toString();
            }
            else {
                //Log.assertW(newStem.equals(prevStem), "Multiple stems for %s: %s x %s\n%s\n%s\n", aForm, prevStem, newStem, aDeclInfos[ aDeclInfo.getEnding().getRootId() ], aDeclInfo);
            }
        }
        catch(Exception e) {
            System.out.println(e);
            System.out.printf("stems %s should have %d stems.\n", Arrays.toString(aStems), aDeclInfo.getPrdgm().nrOfStems() );
            System.out.printf("form=%s, aDeclInfo=%s\n", aForm, aDeclInfo );
            System.out.printf("Paradigm: %s, scrule=%s\n", aDeclInfo.getPrdgm(), aDeclInfo.getPrdgm().stemRule);
            System.out.printf("e %s\n", aDeclInfo.getEnding() );
            System.out.printf("All endings: \n" );

            for(Ending en : aDeclInfo.getEnding().getPrdgm().getEndings()) {
                System.out.printf(" e %s\n", en );
            }

            throw new RuntimeException(e);
        }

    }

    static void mergeStems(AEntry aE1, String[] aStems1, String[] aDeclInfos1, AEntry aE2, String[] aStems2, String[] aDeclInfos2) {
        assert aStems1.length == aStems1.length;
        // unknown stems are empty strings (not null)
        for (int i=0; i < aStems1.length; i++) {
            String stem1 = aStems1[i];
            String stem2  = aStems2[i];
            //System.out.println("Stem: " + stem);
            if (stem1.equals("")) {
                aStems1[i] = stem2;
                aDeclInfos1[i] = aDeclInfos2[i];
            }
            else {
                if (! (stem2.equals("") || stem1.equals(stem2)) ) {
//                        System.out.printf("Multiple stems for the slot %d: >%s< - >%s<\n", i, stem1, stem2);
//                        System.out.println(Arrays.toString(aStems1));
//                        System.out.println(aE1);
//                        System.out.println(aDeclInfos1[i]);
//                        System.out.println(Arrays.toString(aStems2));
//                        System.out.println(aE2);
//                        System.out.println(aDeclInfos2[i]);
                }
            }
        }
    }


// Forms

    /**
     *
     * @param aForm lower-case form
     * @param aCap  capitalization of the original form
     * @param aFreq frequency
     * @param aLts
     */
    protected void addForm(String aForm, CapFreq aFreqs, Set<Lt> aLts) {
        Err.assertE(mForms.get(aForm) == null, "should be null " + mForms.get(aForm) + " " + aForm);

        mForms.put(aForm, new FormInfo(aFreqs, aLts) );
    }

    protected void mergeForms(AEntry aE) {
        SortedMap<String,FormInfo> toAdd = new TreeMap<String,FormInfo>();
        for (Map.Entry<String,FormInfo> e : aE.mForms.entrySet()) {
            FormInfo fi = mForms.get(e.getKey());
            if (fi != null) {
                fi.merge(e.getValue());
            }
            else
                toAdd.put(e.getKey(), e.getValue());
        }

        mForms.putAll(toAdd);
    }

    protected void writeForms(XMLStreamWriter aW) throws XMLStreamException {
        for (Map.Entry<String,FormInfo> ee : mForms.entrySet()) {
            String form = ee.getKey();
            FormInfo formInfo = ee.getValue();

            aW.writeCharacters("\n    ");
            aW.writeStartElement("f");
                aW.writeCharacters(form);
                aW.writeStartElement("fq");
                    aW.writeCharacters(formInfo.getCapFreq().toString());
                aW.writeEndElement();
                aW.writeStartElement("t");
                   aW.writeCharacters("");
                aW.writeEndElement();
            aW.writeEndElement();
        }
    }

    protected void readForms(XMLEventReader aR) throws XMLStreamException {
        for(;;) {
            XMLEvent e = XmlUtils.peekTag(aR);
            if (e.isEndElement()) return;
            if (! e.asStartElement().getName().getLocalPart().equals("f")) return;

            aR.nextEvent(); // eat peeked f
                String form = aR.nextEvent().asCharacters().getData();
                aR.nextEvent(); // fq
                String freqStr = aR.nextEvent().asCharacters().getData();
                aR.nextEvent(); // /fq
                aR.nextEvent(); // t
                //@todo testString tagsStr = aR.nextEvent().asCharacters().getData();
                aR.nextEvent(); // /t
                mForms.put(form, FormInfo.fromString(freqStr, ""));
                aR.nextEvent(); // /f
            }
    }



// </editor-fold>
}

