package morph.acq;

import java.io.IOException;
import java.io.Writer;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.XMLEvent;
import util.io.IO;
import util.io.XFile;

/**
 *
 * @author 
 */
public class XmlUtils {
    
    /** Creates a new instance of XmlUtils */
    private XmlUtils() {
    }

    /**
     * Reades xml header
     */
    public static XMLEventReader openXmlReader(final XFile aFile) throws IOException, XMLStreamException  {
        System.setProperty("javax.xml.stream.XMLInputFactory", "com.bea.xml.stream.MXParserFactory");
        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        
        XMLEventReader r = xmlif.createXMLEventReader(IO.openReader(aFile));
        r.nextEvent();     // xml decl

        return r;
    }
    

    public static XMLStreamWriter openXmlWriter(Writer aW) throws IOException, XMLStreamException {
        System.setProperty("javax.xml.stream.XMLOutputFactory", "com.bea.xml.stream.XMLOutputFactoryBase");
        XMLOutputFactory factory = XMLOutputFactory.newInstance();

        XMLStreamWriter xw = null;
        try {
            xw = factory.createXMLStreamWriter(aW);
        }
        catch (XMLStreamException e) {
            xw.close();
            throw e;
        }
        
        return xw;
    }
    
    /**
     * Ignores whitespace, comments, ..
     * @todo make some XMLReader utils
     */
    public static XMLEvent peekTag(XMLEventReader aR) throws XMLStreamException {
        // Skip white space, comments and processing instructions:
        XMLEvent e = null;;
        int eventType = 0;
        
        for(;;) {
            e = aR.peek();
            eventType = e.getEventType();
            
            if (!
                (eventType == XMLStreamConstants.SPACE ||
                 eventType == XMLStreamConstants.COMMENT ||
                 eventType == XMLStreamConstants.PROCESSING_INSTRUCTION ||
                 (eventType == XMLStreamConstants.CHARACTERS && e.asCharacters().isWhiteSpace()) ||
                 (eventType == XMLStreamConstants.CDATA && e.asCharacters().isWhiteSpace())
                ) ) 
                break;

            aR.next();  // eat the peeked ignorable event
        }
            
        if (eventType != XMLStreamConstants.START_ELEMENT && eventType != XMLStreamConstants.END_ELEMENT) {
            throw new XMLStreamException("expected XMLStreamConstants.START_ELEMENT or XMLStreamConstants.END_ELEMENT not "
                                         + eventType, e.getLocation());
        }
        return e;
    }
    
}
