package morph.acq;

import java.util.HashSet;
import java.util.Set;
import morph.ts.Tag;
import util.col.Cols;
import morph.ma.lts.Lt;
import util.str.Cap;

class FormInfo {
    private CapFreq mCapFreq;
    private Set<Tag> mTags;

    private FormInfo() {}

    public FormInfo(CapFreq aCapFreq, Set<Lt> aLts) {
        mCapFreq = aCapFreq;
        
        mTags = new HashSet<Tag>();
        for (Lt lt : aLts) {
            mTags.add(lt.tag());
        }
        // @todo convert to array to save space??
    }
    
    public CapFreq getCapFreq() {
        return mCapFreq;
    }
    
    
    public void setFreq(Cap aCap, int aFreq) {
        mCapFreq.setFreq(aCap, aFreq);
    }

    public void merge(FormInfo aFormInfo) {
        mCapFreq.merge(aFormInfo.mCapFreq);
        // @todo assumes the tags are the same
    }
    

    public int freq(Cap aCap) {
        return mCapFreq.getFreq(aCap);
    }

    /**
     * Total frequency of all capitalizations.
     */
    public int freq() {
        return mCapFreq.getTotalFreq();
    }
    
    public boolean allLowerCase() { 
        return mCapFreq.onlyLowerCase();
    }
    
    public boolean allFirstCap() { 
        return mCapFreq.onlyFirstCap();
    }

    public boolean allAllCaps() { 
        return mCapFreq.onlyCaps();
    }

    public boolean allMixed() { 
        return mCapFreq.onlyMixed();
    }

// -----------------------------------------------------------------------------    
// Read/Write
// -----------------------------------------------------------------------------    

    public String toString() { 
        return mCapFreq.toString();
    }

    public static FormInfo fromString(String aCapsStr, String aTagsStr)  {
        FormInfo tmp = new FormInfo();

        tmp.mCapFreq = CapFreq.fromString(aCapsStr);

        // @todo this is tagset specific
//@todo        char sep = (aTagsStr.charAt(1) == ':') ? '/' : ':';             // Z:-- (punctuation uses : as subPOS, but always nonambiguous)
//        tmp.mTags = Tags.fromStringsS(Strings.splitL(aTagsStr, sep));

        return tmp;
    }

    public String tagsString() {
        return Cols.toString(mTags, "", "", ":", "");
    }


    
}
