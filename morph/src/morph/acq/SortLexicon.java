package morph.acq;

import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import morph.ma.Context;
import morph.ma.Morph;
import morph.ma.MorphCompiler;
import morph.ma.lts.*;
import morph.ma.module.LexModule;
import morph.ma.module.Paradigms;
import morph.util.MUtilityBase;
import util.err.Err;
import util.str.StringPair;
import util.str.Strings;
import util.col.Counter;
import util.err.Log;
import util.io.IO;
import util.io.XFile;

/**
 * Prepares a file for manual lexicon pruning
 * Input: Frequency list (+ a lexicon)
 * Output: List of lex entries sorted by freq
 *
 * @author Jirka
 */
public class SortLexicon extends MUtilityBase {
    /**
     * Morphological analyzer
     */
    private Morph mMorph;
    private Paradigms mParadigms;
    private Counter<StringPair> mLexEntriesCounter = new Counter<StringPair>();       // Lemma*Prdgm -> Int
    private Map<StringPair, String> mLexiconComments = new HashMap<StringPair, String>(); // Lemma*Prdgm -> comment from the lexicon

    private String mDesiredLexiconSrc;

    private int mWords;

    private Set<StringPair> mAlreadyProcessed = new HashSet<StringPair>();      // processed in some previous pruning

    /**
     */
    public static void main(String[] args) throws IOException {
        new SortLexicon().go(args);
    }

    private void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);

        XFile inFile  = getArgs().getDirectFile(0);      // frequency list (comp file = forms * freq)
        XFile outFile = getArgs().getDirectFile(1);      // list of lex entries sorted by freq
        Err.checkIFiles("in", inFile);
        Err.checkOFiles("out", outFile);

        prepareMa();

        profileStart("Running MA & Counting...");
        maAndCount(inFile);
        profileEnd();

        XFile pruningFile = getArgs().getFile("alreadyPruned");
        if (pruningFile != null) {
            Log.info("already pruned:  %s\n", pruningFile);
            Err.checkIFiles("pruning by", pruningFile);
            readAlreadyPruned(pruningFile);
        }

        profileStart("Writing results...");
        readInComments();
        writeResults(outFile);
        profileEnd();
    }

    final static Pattern pruneEntryPattern = Pattern.compile("(?://)?\\s*(\\S+)\\s+(\\S+).*");

    private void readAlreadyPruned(XFile aPruneFile) throws java.io.IOException {
        LineNumberReader r = IO.openLineReader(aPruneFile);

        for(;;) {
            String line = r.readLine();
            if (line == null) break;
            line = line.trim();
            if (line.length() == 0) continue;

            Matcher m = pruneEntryPattern.matcher(line);
            Err.fAssert(m.matches(), "[%d] Wrong bad-entry format\n  %s", r.getLineNumber(), line);
            String lemma = m.group(1);
            String prdgm = m.group(2);
            mAlreadyProcessed.add(new StringPair(lemma,prdgm));
        }
        r.close();
        Log.info("Already processed entries: %d", mAlreadyProcessed.size());
    }

    private void prepareMa() throws java.io.IOException {
        // --- configure ma ---
        getArgs().set("collectInfo", true);   // collect info about prdgms and epenthesis
        Lt.setInfoIrelevant(false);    // Info necessary to prevent conflating equal analyses with different prdgms

        mMorph = MorphCompiler.factory().compile(getArgs());
        Err.iAssertFatal(mMorph != null, "Cannot create Morph object");

        // --- get paradigms ---
        String lexModuleName = getArgs().getString("lexicon");
        LexModule lexModule  = (morph.ma.module.LexModule) mMorph.getModule(lexModuleName);
        Err.iAssertFatal(lexModule != null, "Cannot get the Lexicon module");
        mParadigms = lexModule.getParadigms();

        Log.info("MA Loaded");

        mDesiredLexiconSrc = lexModule.getSrc();
    }

    private void readInComments() throws java.io.IOException {
        String lexModuleName = getArgs().getString("lexicon");
        File dataPath = getArgs().getDir("dataPath", null);
        XFile file = getArgs().getFile(lexModuleName + ".file", dataPath, null);

        LineNumberReader r = IO.openLineReader(file);

        for(;;) {
            String line = r.readLine();
            if (line == null) break;
            line = line.trim();
            if (line.length() == 0) continue;

            try {
                processLexEntry(r, line);
            }
            catch (java.util.NoSuchElementException e) {
                System.out.println(e.getMessage());
            }
        }
        r.close();
    }

    final static Pattern lexEntryPattern = Pattern.compile("(\\S+)\\s+(?:\\S+)\\s+(?:@2\\s+\\S+\\s+)?(?:@3\\s+\\S+\\s+)?(\\S+)\\s+//(.*)");

    private void processLexEntry(LineNumberReader aR, String aLine)  {
        Matcher m = lexEntryPattern.matcher(aLine);
        if (m.matches()) {
            String lemma = m.group(1);
            String prdgm = m.group(2);
            String comment = m.group(3);
            StringPair lexEntryId = new StringPair(lemma, prdgm);

            if ( mLexEntriesCounter.getMap().containsKey(lexEntryId) ) {
                mLexiconComments.put(lexEntryId, comment);
            }
//            else {
//                System.out.println("Not found: " + lexEntryId);
//            }

        }
    }


    private void maAndCount(XFile aInFile) throws IOException {
        LineNumberReader r = IO.openLineReader(aInFile);

        for(;;) {
            String line = r.readLine();
            if (line == null) break;
            String[] ff = Strings.split(line);

            // --- My format from compiler ---
            Err.fAssert(ff.length == 2," [%d]: %s", r.getLineNumber(), line);

            String form = ff[0];
            int freq = Integer.parseInt( ff[1] );

            Lts lts = mMorph.analyze(form);
            mWords++;

            Log.dot(mWords);

            // --- give points to every used lexical entry ---
            if (lts != null) {
                Set<StringPair> lexEntries = new HashSet<StringPair>();

                for (Lt lt : lts) {
                    if (lt.src().equals(mDesiredLexiconSrc)) {
                        lexEntries.add(new StringPair(lt.lemma(), lt.info().getPrdgmId()));
                    }
                }

                for (StringPair lexEntryId : lexEntries) {
                    mLexEntriesCounter.add(lexEntryId, freq);
                    if (lexEntryId.mFirst.equals("partir")) {
                        int f = mLexEntriesCounter.frequency(lexEntryId);
                        System.out.println("  " + f);
                    }
                }
            }
        }
        r.close();
    }


    /**
     * Writes out lexical entries in LexStat object into a temporary file.
     * These files are then merged by merge.
     *
     */
    private void writeResults(XFile aOutFile)  {
        PrintWriter w = null;
        try {
            Log.info("Saving (words %d)", mWords);
            w = IO.openPrintWriter(aOutFile);
            writeResults(w);
        }
        catch(IOException a) {  //@todo semi-automatic exception reporting, with option to report the exception object
            System.out.printf("Error opening/writing %s\n", a);
        }
        finally {
            if (w != null) w.close();
        }
    }

    private void writeResults(PrintWriter w) throws IOException {
        Set<Map.Entry<StringPair,Integer>> lpfs = mLexEntriesCounter.setSortedByFreq();
        mLexEntriesCounter = null;

        for (Map.Entry<StringPair,Integer> lpf : lpfs) {
            String lemma = lpf.getKey().mFirst;
            String prdgm = lpf.getKey().mSecond;
            StringPair lexEntryId = new StringPair(lemma,prdgm);

            if (!mAlreadyProcessed.contains(lexEntryId)) {
                String comment = mLexiconComments.get(lexEntryId);
                w.printf("%s   %s   // %d %s\n", lemma, prdgm, lpf.getValue(), comment);
            }
        }

    }
}
