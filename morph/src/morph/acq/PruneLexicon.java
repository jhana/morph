package morph.acq;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import morph.util.MUtilityBase;
import util.err.Err;
import util.str.StringPair;
import util.io.IO;
import util.io.XFile;

/**
 * Can be called repeatedly (output of one pruning fed as an input into another pruning).
 *
 * @author Jirka
 */
public class PruneLexicon extends MUtilityBase {
    /**
     * Entries marked (manualy) as bad.
     */
    private Set<StringPair> mBadEntries = new HashSet<StringPair>(); // Lemma*Prdgm -> comment from the lexicon 
    

    /**
     */
    public static void main(String[] args) throws IOException {
        new PruneLexicon().go(args);
    }
    
    private void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);
        
        XFile inLexFile  = getArgs().getDirectFile(0);      // lexicon to prune
        XFile pruneFile  = getArgs().getDirectFile(1);      // pruning list (XX)lemma paradigm comment
        XFile outLexFile = getArgs().getDirectFile(2);      // pruned lexicon
        Err.checkIFiles("in lex;prune by", inLexFile, pruneFile);
        Err.checkOFiles("outLexFile", outLexFile);

        readInBadEntries(pruneFile);

        pruneLexicon(inLexFile, outLexFile);
    }

    final static Pattern badEntryPattern = Pattern.compile("//\\s*(\\S+)\\s+(\\S+).*");

    private void readInBadEntries(XFile aPruneFile) throws java.io.IOException {
        LineNumberReader r = IO.openLineReader(aPruneFile); 

        for(;;) {
            String line = r.readLine();
            if (line == null) break;
            line = line.trim();
            if (line.length() == 0) continue;
            
            if (line.startsWith("//")) {
                Matcher m = badEntryPattern.matcher(line);
                Err.fAssert(m.matches(), "[%d] bad entry format line\n  %s", r.getLineNumber(), line);
                String lemma = m.group(1);
                String prdgm = m.group(2);
                mBadEntries.add(new StringPair(lemma,prdgm));
            }
        }
        r.close();
    }


    private void pruneLexicon(XFile aInFile, XFile aOutFile) throws java.io.IOException {
        LineNumberReader r = IO.openLineReader(aInFile); 
        PrintWriter w = IO.openPrintWriter(aOutFile);

        for(;;) {
            String line = r.readLine();
            if (line == null) break;
            line = line.trim();
            if (line.length() == 0 || line.startsWith("//")) {
                w.println(line);
                continue;
            }
            
            StringPair lexEntryId = getLexEntryId(r, line);

            if (mBadEntries.contains(lexEntryId)) {
                w.print("// ");
                w.println(line);
            }
            else {
                w.println(line);
            }
        }
        r.close();
        w.close();
    }

    final static Pattern lexEntryPattern = Pattern.compile("(\\S+)\\s+(?:\\S+)\\s+(?:@2\\s+\\S+\\s+)?(?:@3\\s+\\S+\\s+)?(\\S+)\\s+//(?:.*)");

    // @todo make a general lex entry reader
    private StringPair getLexEntryId(LineNumberReader aR, String aLine)  {
        Matcher m = lexEntryPattern.matcher(aLine);
        Err.fAssert(m.matches(), "[%d] Wrong lex entry format line\n  %s", aR.getLineNumber(), aLine);

        String lemma = m.group(1);
        String prdgm = m.group(2);
        return new StringPair(lemma, prdgm);
    }
}
 