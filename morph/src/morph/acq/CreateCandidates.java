package morph.acq;

import util.str.Strings;
import java.io.*;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;
import javax.xml.stream.XMLStreamException;
import util.err.Err;
import util.err.Log;
import util.io.*;
import morph.util.MUtilityBase;
import morph.ma.*;
import morph.ma.lts.*;
import morph.common.UserLog;
import morph.ts.Tag;
import util.str.Caps;


public class CreateCandidates extends MUtilityBase {

    /**
     */
    public static void main(String[] args) throws IOException, XMLStreamException {
        new CreateCandidates().go(args);
    }

// =============================================================================
// Implementation
// =============================================================================
    
// --- Options ---    
    private int mCorpusSizeLimit;
    private int mDumpThreshhold;
    private Pattern mNarrow;        // @todo replace by reseting matcher

// --- ---    
    
    /**
     * Morphological analyzer
     */
    private Morph mMorph;

    /** 
     * Counter of processed words
     */
    private int mWords;
    
    XFile mOutFile;
    String mOutFileShortName;
    
    private void go(String[] aArgs) throws java.io.IOException, XMLStreamException {
        argumentBasics(aArgs,2);
        
        XFile inFile = getArgs().getDirectFile(0);
        mOutFile     = getArgs().getDirectFile(1);
        Err.checkIFiles("in", inFile);
        Err.checkOFiles("out", mOutFile);

        mOutFileShortName = mOutFile.file().getName();
        
        mCorpusSizeLimit    = getArgs().getInt("corpusSizeLimit", Integer.MAX_VALUE);
        System.out.println("mCorpusSizeLimit " + mCorpusSizeLimit);
        mDumpThreshhold     = getArgs().getInt("dumpThreshold");
        mNarrow             = getArgs().getPatternNE("narrow");
        System.out.println("narrow:" + mNarrow);
        int minNrOfForms    = getArgs().getInt("minNrOfForms");
        int nrOfMergedFiles = getArgs().getInt("nrOfMergedFiles");
        prepareMa();

        profileStart("Running MA...");
        ma(inFile);
        profileEnd();
        
        profileStart("Merging results of MA...");
        new Merge(mOutFile,nrOfMergedFiles,minNrOfForms).go();
        profileEnd();
    }

    private void prepareMa() throws java.io.IOException {
        // --- configure ma ---
        getArgs().setBy("Guesser.markEpen", "markEpen");
        //getArgs().set("Guesser.leo.enabled", false);
        //getArgs().set("Guesser.handleDerivations", false);  // @todo configurable
        getArgs().set("collectInfo",          true);       // collect info about prdgms and epenthesis
        Lt.setInfoIrelevant(false);      // Info necessary to prevent conflating equal analyses with different prdgms

        mMorph = MorphCompiler.factory().compile(getArgs());
        Err.assertFatal(mMorph != null, "Cannot create Morph object");

        // --- get filter --- // @todo configurable
        if (getArgs().getBool("filter.words.enabled")) {
            mWordModule = new morph.ma.module.WordModule();
            mWordModule.init("filter.words", getArgs());
            mWordModule.compile(new UserLog());
            mWordModule.configure();
        }
    }
    
    
    private void ma(XFile aInFile) throws IOException, XMLStreamException {
        LexStat lexStat = new LexStat();
        int block = 0;

        LineReader r = IO.openLineReader(aInFile);
        r.configureSplitting(Strings.cWhitespacePattern, 2, "required <form> <f>:<f>:<f>:<f>");

        for(; mWords < mCorpusSizeLimit;) {
            String[] strs = r.readSplitLine();
            if (strs == null) break;

            String form = strs[0];
            if ( mNarrow != null && !mNarrow.matcher(form).matches() ) continue;

            // TODO there is probably some support for format errors
            CapFreq freqs = null;
            try {
                freqs = CapFreq.fromString(strs[1]);
            }
            catch(Exception e) {
                System.out.printf("Format Error [%d]: frequency string \"%s\" cannot be parsed\n%s\n", r.getLineNumber(), strs[1], r.getLine() );
            }

            if (freqs.getLowerFreq() > 1) {  // @todo more sophisticated elimination of errors
                Lts lts = mMorph.analyze(form);
//                System.out.println(Cols.toStringNl(lts.col(),"XX  "));
                filter(form,lts,false);
                if (lts != null) lexStat.add(form, lts, false, freqs);
            }
            
            if (freqs.getLowerFreq() < freqs.getFirstCapFreq()) {         // Ff only (possibly some FF, for now ignore fFf)
                Lts lts = mMorph.analyze(form);
                filter(form,lts,true);
                if (lts != null) lexStat.add(form, lts, true, freqs);
            }

            if (lexStat.size() > mDumpThreshhold) {
                dump(lexStat, block);
                block++;
            }
            mWords++;
            Log.dot(mWords);
        }
        r.close();

        dump(lexStat, block);
        
        System.out.printf("Done: %d, block %d\n", mWords, block);
    }
    
    /**
     * Filters guesser's output by more precise modules (currently only word list).
     * We cannot use the info in the wordlists directly because they do not contain
     * info about paradigms.
     * 
     * @param aForm form
     * @param aLts analyses suggested by the guesser
     * @param aCap is the form capitalized?
     */
    private void filter(String aForm, Lts aLts, boolean aCap) {
        if (mWordModule == null) return;
        //System.out.println(aForm);
        //System.out.println(Cols.toStringNl(aLts.col("krajina"), "  "));;
        if (aCap) aForm = Caps.toFirstCapital(aForm);
            
        Set<morph.ma.module.WordEntry> wordEntries = mWordModule.getEntries(aForm);
        //System.out.println("Filtering: " + (wordEntries == null ? null : wordEntries.size()) );
        if (wordEntries == null) return;
        
//        System.out.println("==========");
//        System.out.println(Cols.toStringNl(wordEntries));
//        System.out.println("==========");
        Set<Lt> lts = aLts.lts();
        
        for (Iterator<Lt> i = lts.iterator(); i.hasNext();) {
            Lt lt = i.next();
            String lemma = (lt.info() instanceof DecInfo) ? lt.lemma() : lt.info().asDerInfo().getDeclLemma();
            lemma = aCap ? Caps.toFirstCapital(lemma) : lemma;
            if ( !contains(wordEntries, lemma, lt.tag())) {
                i.remove();
            }
        }
        //System.out.println("after filtering " + aForm);
        //System.out.println(Cols.toStringNl(aLts.col(), "  "));;
    }

    private boolean contains(Set<morph.ma.module.WordEntry> aWordEntries, String aLemma, Tag aTag) {
        for (morph.ma.module.WordEntry we : aWordEntries) {
            if ( we.getLemma().equals(aLemma) && we.getTag().equals(aTag)) return true;
        }
        return false;
    }
    
    private morph.ma.module.WordModule mWordModule;
    
    
    /**
     * Writes out lexical entries in LexStat object into a temporary file.
     * These files are then merged by merge.
     *
     */
    private void dump(LexStat aLexStat, int aBlock) throws IOException, XMLStreamException {
        Log.info("Saving (words %d)", mWords);
        aLexStat.write(getTmpFileName(0, aBlock));
        aLexStat.clear();
    }

    private XFile getTmpFileName(int aLevel, int aBlock) {
        return mOutFile.setShortFile( String.format("tmp%s.%02d.%03d", mOutFileShortName, aLevel, aBlock) );
    }
    
    
}
