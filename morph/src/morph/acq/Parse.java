package morph.acq;

import java.awt.datatransfer.StringSelection;
import javax.xml.stream.*;
import java.util.Iterator;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import util.io.Encoding;
import util.io.IO;

/**
 * This is a simple parsing example that illustrates
 * the XMLEventReader class.
 *
 * @author Copyright (c) 2003 by BEA Systems. All Rights Reserved.
 */


public class Parse {
    private static String filename = null;
    
    private static void printUsage() {
        System.out.println("usage: java examples.basic.Parse <xmlfile>");
    }

/*    static class XmlEventReader implements Iterable<XMLEvent> {
        XMLEventReader mIterator;
        XmlEventReader(XMLEventReader aR) {
            mIterator = aR;
        }

        Iterator<XMLEvent> iterator() {
            return new Iterator<XMLEvent>() {
                public boolean hasNext() {return mIterator.hasNext();}
                public XMLEvent next()    {return mIterator.next();}
            };
        }
    }
  */  
   
    
    public static void main(String[] args) throws Exception {
        try {
            filename = args[0];
        } catch (ArrayIndexOutOfBoundsException aioobe){
            printUsage();
            System.exit(0);
        }
        
        System.setProperty("javax.xml.stream.XMLInputFactory", "com.bea.xml.stream.MXParserFactory");
        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        XMLEventReader xr = xmlif.createXMLEventReader(IO.openReader(IO.fileFromString(filename, null, Encoding.cUtf8)));

        // Parse the XML
        try {
            while(xr.hasNext()){
                printEvent(xr.nextEvent());
            }
        } catch (XMLStreamException ex) {
            System.out.println(ex.getMessage());
        }
        
    }
    
    private static void printEvent(XMLEvent e) {
        if (e == null) {System.out.println("XXnull"); return;}
        switch (e.getEventType()) {
            case XMLStreamConstants.START_ELEMENT:
                System.out.print("<");
                System.out.print(e.asStartElement().getName());
                printAttributes(e.asStartElement());
                System.out.println(">");
                break;
            
            case XMLStreamConstants.END_ELEMENT:
                System.out.print("</");
                System.out.print(e.asEndElement().getName());
                System.out.println(">");
                break;

            case XMLStreamConstants.CHARACTERS:
                Characters chars = e.asCharacters();
                String str = chars.getData().trim();
                if (str.length() == 0)  return;
                System.out.println("**>" + str + "<**");
                break;
            default:
        }
    }

    private static void printAttributes(StartElement e){
        Iterator i = e.getAttributes();
        while(i.hasNext()) {
            Attribute attr = (Attribute)i.next();
            System.out.printf("%s=%s ", attr.getName(), attr.getValue());
        }
    }
}
