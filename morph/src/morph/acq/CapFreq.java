package morph.acq;

import util.str.Strings;
import util.err.Err;
import util.str.Cap;

/**
 *
 * @author Administrator
 */
public final class CapFreq {
    /** Creates a new instance of CapFreq */
    public CapFreq() {
    }

    /**
     * Constructs a CapFreq object from a string specification.
     * @param aStr string specification - colon separated numbers,
     * in the order allLower, firstCap, allCaps, mixed.
     * @todo do via a LAW-like parser
     */
    public static CapFreq fromString(String aStr)  {
        CapFreq tmp = new CapFreq();

        String[] freqs = Strings.cColonPattern.split(aStr); 
        Err.assertE(freqs.length == 4, "Incorrect frequency specification (%s)", aStr);
        tmp.mLowerFreq    = Integer.valueOf(freqs[0]); 
        tmp.mFirstCapFreq = Integer.valueOf(freqs[1]); 
        tmp.mCapsFreq     = Integer.valueOf(freqs[2]); 
        tmp.mMixedFreq    = Integer.valueOf(freqs[3]); 

        return tmp;
    }

// -----------------------------------------------------------------------------    
// Attributes
// -----------------------------------------------------------------------------    

    public int getTotalFreq() {
        return mLowerFreq + mFirstCapFreq + mCapsFreq + mMixedFreq;
    }

    public int getLowerFreq() {
        return mLowerFreq;
    }

    public void setLowerFreq(int aLowerFreq) {
        mLowerFreq = aLowerFreq;
    }

    public int getFirstCapFreq() {
        return mFirstCapFreq;
    }

    public void setFirstCapFreq(int aFirstCapFreq) {
        mFirstCapFreq = aFirstCapFreq;
    }

    public int getCapsFreq() {
        return mCapsFreq;
    }

    public void setCapsFreq(int aCapsFreq) {
        mCapsFreq = aCapsFreq;
    }

    public int getMixedFreq() {
        return mMixedFreq;
    }

    public void setMixedFreq(int aMixedFreq) {
        mMixedFreq = aMixedFreq;
    }
    
// -----------------------------------------------------------------------------    
//
// -----------------------------------------------------------------------------    

    public void addFreq(Cap aCap, int aInc) {
        switch(aCap) {
            case lower:    mLowerFreq    +=aInc; break;
            case firstCap: mFirstCapFreq +=aInc; break;
            case caps:     mCapsFreq     +=aInc; break;
            case mixed:    mMixedFreq    +=aInc; break;
        }
        mTotalFreq += aInc;
    }
    
    
    public boolean onlyLowerCase() { 
        return mTotalFreq == mLowerFreq;
    }
    
    public boolean onlyFirstCap() { 
        return mTotalFreq == mFirstCapFreq;
    }

    public boolean onlyCaps() { 
        return mTotalFreq == mCapsFreq;
    }

    public boolean onlyMixed() { 
        return mTotalFreq == mMixedFreq;
    }
    
    public int getFreq(Cap aCap) {
        switch(aCap) {
            case lower:    return mLowerFreq;
            case firstCap: return mFirstCapFreq;
            case caps:     return mCapsFreq;
            case mixed:    return mMixedFreq;
        }
        return -1;
    }
    
    public void setFreq(Cap aCap, int aFreq) {
        switch(aCap) {
            case lower:    mLowerFreq    = aFreq; break;
            case firstCap: mFirstCapFreq = aFreq; break;
            case caps:     mCapsFreq     = aFreq; break;
            case mixed:    mMixedFreq    = aFreq; break;
        }
        mTotalFreq = mLowerFreq + mFirstCapFreq + mCapsFreq + mMixedFreq;
    }

    public void merge(CapFreq aOther) {
        mLowerFreq    += aOther.mLowerFreq;
        mFirstCapFreq += aOther.mFirstCapFreq;
        mCapsFreq     += aOther.mCapsFreq;
        mMixedFreq    += aOther.mMixedFreq;
        mTotalFreq    += aOther.mTotalFreq;
    }

    public String toString() { 
        return Strings.concat(
                String.valueOf(mLowerFreq), ":", 
                String.valueOf(mFirstCapFreq), ":",
                String.valueOf(mCapsFreq),  ":", 
                String.valueOf(mMixedFreq) );
    }
    
// =============================================================================    
// Implementation
// =============================================================================    

    private int mTotalFreq;
    private int mLowerFreq; 
    private int mFirstCapFreq;
    private int mCapsFreq;
    private int mMixedFreq;
}
