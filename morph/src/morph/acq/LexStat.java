package morph.acq;

import java.io.*;
import java.util.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import morph.ma.lts.DecInfo;
import morph.ma.lts.DerInfo;
import morph.ma.lts.Lt;
import morph.ma.lts.Lts;
import util.str.StringPair;
import util.col.Cols;
import util.col.MultiMap;
import util.io.IO;
import util.io.XFile;
import util.str.Cap;
import util.str.Caps;


/**
 * Used by CreateCandidates to colect frequencies and coverage info about candidate lexical entries.
 * Statistics is organized by lemma+paradigm, each assigned a set of AEntry (ADelcEntry or ADerivEntry) objects.
 * Lemmas in keys for DeclEntries distinguish case.
 *
 * @author Jirka
 */
class LexStat {
    
    /** Creates a new instance of LexStat */
    public LexStat() {
    }

    public int size() {
        return mMap.size();
    }

    public void clear() {
        mMap.clear();
    }


    /**
     *
     * @todo
     * If the form occurs is (or might be) both a common or proper noun, call
     * this function twice.
     * @ 
     *
     * Entries with no info or 0-paradigm are not added.
     */
    public void add(String aForm, Lts aLts, boolean aName, CapFreq aFreqs) {
        MultiMap<StringPair, Lt> lp2lt = organizeByLp(aLts);        
        String form = (aName) ? Caps.toFirstCapital(aForm) : aForm;          // form with proper case
        
        // --- merge with existing lemma+prdgm statistics (case mismatch possible only with derivations) ---
        for(Map.Entry<StringPair, Set<Lt>> e : lp2lt.entrySet() ) {
            Set<Lt> lts = e.getValue();
            StringPair lp = e.getKey();
            boolean isDecl = Cols.getFirstElement(lts).info() instanceof DecInfo;
            
            if (isDecl) {
                addDecl(form, lp, lts, aName, aFreqs);
            }
            else {
                addDeriv(form, lp, lts, aName, aFreqs);
            }
        }
    }    

    private void addDeriv(final String aForm, final StringPair aLp,  final Set<Lt> aLts, final boolean aName, final CapFreq aFreqs) {
        // --- remove all lts with case inconsistent with case specified in derivation ---
        for(Iterator<Lt> i = aLts.iterator(); i.hasNext();) {
            Lt lt = i.next();
            DerInfo derInfo = (DerInfo) lt.info();
            Cap cap = derInfo.getDerivation().mCap;
            
            if (cap != null && aName != cap.firstCap()) i.remove();
        }
        if (aLts.isEmpty()) return;

        
        AEntry entry = mMap.get(aLp); // entry with the same l+p (deriv lemmas are always lower case)
        if (entry == null) {
            entry = new ADerivEntry();
            entry.mLemma      = aLp.mFirst;
            entry.mParadigmId = aLp.mSecond;
            mMap.put(aLp, entry);
        }
        entry.update(aForm, aFreqs, aName, aLts);
    }

    private void addDecl(final String form, final StringPair lp,  final Set<Lt> lts, final boolean aName, final CapFreq aFreqs) {
        if (aName) {
            lp.mFirst = Caps.toFirstCapital(lp.mFirst);  // decl lemmas distinguish capitalization
        }
        
        AEntry entry = mMap.get(lp); // entry with the same l+p 
        if (entry == null) {
            entry = new ADeclEntry();
            entry.mLemma = lp.mFirst;
            entry.mParadigmId = lp.mSecond;
            mMap.put(lp, entry);
        }
        entry.update(form, aFreqs, aName, lts);
    }
    
    

    private MultiMap<StringPair, Lt> organizeByLp(final Lts aLts) {
        // --- organize lts by lemma+paradigm ---
        MultiMap<StringPair,Lt> lp2lt = new MultiMap<StringPair,Lt>();
        for (Lt lt : aLts) {
            if (lt.info() == null || lt.info().getPrdgmId().equals("0")) continue;
            StringPair lp = new StringPair(lt.lemma().toLowerCase(), lt.info().getPrdgmId());
            lp2lt.add(lp, lt);
        }
        return lp2lt;
    }


    
    public void write(XFile aFile) throws IOException, XMLStreamException {
        XMLStreamWriter xw = null;
        Writer w = null;
        try{
            System.setProperty("javax.xml.stream.XMLOutputFactory", "com.bea.xml.stream.XMLOutputFactoryBase");
            XMLOutputFactory factory = XMLOutputFactory.newInstance();
            w = IO.openWriter(aFile);
            xw = factory.createXMLStreamWriter(w);
            write(xw);
        }
        finally {
            if (xw != null) xw.close();
            IO.close(w);
        }
                    
    }
            
    public void write(XMLStreamWriter aW) throws XMLStreamException {
        aW.writeStartDocument();
        aW.writeStartElement("entries");
        aW.writeCharacters("\n");
        for(AEntry e : new TreeSet<AEntry>(mMap.values()) ) {
            e.write(aW);
        }
        aW.writeEndElement();
        aW.writeEndDocument();
    }

    
// =============================================================================
// Implementation <editor-fold desc="Implementation">
// ==============================================================================

    private static String mFStart = "";

    /** 
     * (Lemma, PrdgmId) -> Entry
     */
    private SortedMap<StringPair,AEntry> mMap = new TreeMap<StringPair,AEntry> ();

    

// </editor-fold>    
}
