package morph.acq;

import java.io.*;
import java.util.*;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import util.PairC;
import util.col.BoolSet;
import util.col.Cols;
import util.io.*;


/**
 * Merges files produced by LexGuesser into a single (sorted) file.
 * 
 * @author Jirka Hana
 */
public class Merge {

    /**
     * Merges all files in the aOutFile's directory and satifying the tmp[aOutFile].00.* 
     * pattern into a single file.
     * If there are too many files to be merged in one step, files are merged in 
     * a tree-way.
     */
    public Merge(XFile aOutFile, int aMaxNrOfMergedFiles, int aReqForms) throws IOException {
        mMaxNrOfMergedFiles = aMaxNrOfMergedFiles;
        mOutFile = aOutFile;    // @todo encoding must be utf�
        mFormThreashold = aReqForms - 1;
        System.setProperty("javax.xml.stream.XMLInputFactory", "com.bea.xml.stream.MXParserFactory");
        mXmlRFactory = XMLInputFactory.newInstance();
    }
    
    public void go() throws IOException, XMLStreamException {
        final String inFilePattern = String.format("tmp%s.00.", mOutFile.file().getName());
        FilenameFilter fileFilter = new FilenameFilter() {
           public boolean accept(File aDir, String aName) {
              return aName.startsWith(inFilePattern);
           }
        };
        
        File[] files = mOutFile.file().getAbsoluteFile().getParentFile().listFiles(fileFilter);
        
        List<XFile> tmpFiles = Arrays.asList( Files.filesToXFiles(files, mOutFile ) );
        
        mergeLevel(0, tmpFiles);
    }

    
    /**
     * Factory used to create xml readers.
     * @see #mXrBattery
     */
    final private XMLInputFactory mXmlRFactory;

    /**
     * Maximal number of files that can be merged in one step.
     */
    final private int mMaxNrOfMergedFiles;

    /**
     * Xml readers to read from 
     */
    private List<XMLEventReader> mXrBattery;

    /**
     * Readers used by the xml batter.
     */
    private List<Reader> mRBattery;

    /**
     * Minimal entries from each of the merged file and their source(s).
     */
    private final SortedSet<PairC<AEntry,BoolSet>> mMemory = new TreeSet<PairC<AEntry,BoolSet>>();
    /**
     * for keeping track of which readers have something available
     */
    private BoolSet mAvailableRs;       

    private final XFile mOutFile;
    

    /**
     * To be saved, an entry needs to cover more forms than mFormThreashold 
     */
    private final int mFormThreashold;
    
    
    /**
     * Produces next level in the merging trees. 
     * Each node at this level has up to mMaxNrOfMergedFiles dtr-files from the 
     * lower level. If the result is a single file, it is the final result.
     * The final result can be filtered - only entries with more entries than {@link #mFormThreashold} are saved.
     */
    private void mergeLevel(int aLevel, List<XFile> aInFiles) throws IOException , XMLStreamException{
        //System.out.printf("Level %d: files: %d\n", aLevel, aInFiles.size());
        // --- final merge ---
        if (aInFiles.size() <= mMaxNrOfMergedFiles) {
            mergePart(aInFiles, mOutFile, false);
        }
        // --- partial merge ---
        else {
            int parts = (int) Math.ceil( aInFiles.size()*1.0 / mMaxNrOfMergedFiles );   // # of files at this level
            //System.out.println("#:" + parts);
            List<XFile> partialOuts = new ArrayList<XFile>(parts);                  // files at this level

            for (int i=0; i < parts; i++) {
                int from = i*mMaxNrOfMergedFiles;
                int to = Math.min((i+1)*mMaxNrOfMergedFiles, aInFiles.size());
                        
                List<XFile> partInFiles = aInFiles.subList(from, to);
                XFile outFile = getTmpFileName(aLevel + 1, i);
                partialOuts.add( outFile );
                //System.out.printf("   Part %d: %d-%d\n", i, from, to);

                mergePart(partInFiles, outFile, true);
            }

            mergeLevel(aLevel + 1, partialOuts);   // recursive step
        }
    }
    
        
    private void mergePart(List<XFile> aInFiles, XFile aOutFile, boolean aPartial) throws IOException, XMLStreamException {
        // --- prepare input ---
        mRBattery = openBatteryR(aInFiles);
        mXrBattery = openBattery(mRBattery);
        mAvailableRs      = BoolSet.allTrue(mXrBattery.size());
        BoolSet whereFrom = BoolSet.allTrue(mXrBattery.size());      // initially we read from all the readers

        // --- prepare output ---
        Writer w = IO.openWriter(aOutFile);
        XMLStreamWriter xw = XmlUtils.openXmlWriter(w);
        xw.writeStartDocument();
        xw.writeStartElement("merged");
        xw.writeCharacters("\n");
        

        for(;;) {
            getNewData(whereFrom);
            if (mAvailableRs.hasAllCleared()) break;   // no reader has words available => we are done
                
            // save the smallest entry
            PairC<AEntry,BoolSet> min = mMemory.first();    // write down the smallest entry
            mMemory.remove(min);
            if (aPartial || min.mFirst.mForms.size() > mFormThreashold)
                min.mFirst.write(xw);
            whereFrom = min.mSecond;     // next time refil from those places
        } ;
        
        
        // --- finish and close output ---
        xw.writeEndElement();
        xw.writeEndDocument();
        xw.close();
        w.close();

        // --- close inputs ---
        for (XMLEventReader xr : mXrBattery) {
            xr.close();
        }
        IO.closeBattery(mRBattery);
        
        // --- delete input files to save space ---
        for (XFile file : aInFiles) {
            file.file().delete();
        }
    }    

        
    /**
     * Loads new data from the specified readers and adds them to memory.
     */       
    private void getNewData(final BoolSet aWhereFrom) throws IOException, XMLStreamException {
        for (int i = 0; i < mXrBattery.size(); i++) {
            if (aWhereFrom.get(i)) {
                AEntry e = getEntry(i);
                if (e == null) 
                    mAvailableRs = mAvailableRs.clear(i);
                else 
                    addToMemory(e, new BoolSet(mXrBattery.size()).set(i));
            }
        }
    }
    
    /**
     * Adds a new entry to memory, merging with an existing entry if necessary 
     * (i.e. lemma and paradigm are the same).
     */
    private void addToMemory(AEntry aE, BoolSet aWhereFrom) {
        // --- try to merge it with something in memory ---  // @todo quit early from walk thru SORTED memory
        for (PairC<AEntry,BoolSet> e : mMemory) {
            int compLemma = e.mFirst.mLemma.compareTo(aE.mLemma);

            if ( compLemma == 0 && e.mFirst.mParadigmId.equals(aE.mParadigmId) ) {
            	e.mFirst.merge(aE);
                e.mSecond = aWhereFrom.or(e.mSecond);
            	return;
            }
    	}

        // --- cannot be merged, add as new ---
        mMemory.add(new PairC<AEntry,BoolSet>(aE,aWhereFrom));
    }
    
    /**
     * Loads one entry from the specified reader.
     */
    private AEntry getEntry(int aRIdx) throws IOException, XMLStreamException {
        XMLEventReader r = mXrBattery.get(aRIdx);
        return AEntry.readEntry(r);
    }
    
    /**
     * @todo to utils
     */
    private List<Reader> openBatteryR(final List<XFile> aFiles) throws IOException  {
        List<Reader> battery = new ArrayList(aFiles.size());
        
        for (XFile file : aFiles) {
            battery.add( IO.openReader(file) );
        }

        return battery;
    }

    
    private List<XMLEventReader> openBattery(final List<Reader> aRs) throws IOException, XMLStreamException  {
        List<XMLEventReader> battery = new ArrayList<XMLEventReader>(aRs.size());
    
        for (Reader r : aRs) {
            XMLEventReader xr = mXmlRFactory.createXMLEventReader(r);
            battery.add(xr);
            xr.nextEvent();     // xml declaration
            xr.nextEvent();     // document root (entries)
        }

        return battery;
    }
    
    /**
     * @todo to utils
     */
    private LineNumberReader[] openBatteryLR(final List<XFile> aFiles) throws IOException  {
        LineNumberReader[] battery = new LineNumberReader[aFiles.size()];
        
        for (int i=0; i < aFiles.size(); i++) {
            battery[i] = IO.openLineReader(aFiles.get(i));
        }

        return battery;
    }

    private XFile getTmpFileName(int aLevel, int aBlock) {
        return mOutFile.setShortFile( String.format("tmp%s.%02d.%03d", mOutFile.file().getName(), aLevel, aBlock) );
    }

}
