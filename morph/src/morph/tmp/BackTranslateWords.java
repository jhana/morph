package morph.tmp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import lombok.Cleanup;
import morph.io.Corpora;
import morph.io.CorpusLineReader;
import morph.ma.lts.Fplus;
import morph.ma.lts.Lt;
import morph.ma.lts.Lts;
import morph.ts.Tag;
import morph.util.MUtilityBase;
import morph.util.PDTX;
import util.ColIo;
import util.col.Cols;
import util.col.MultiMap;
import util.io.IO;
import util.str.Strings;

/**
 * Translates a word list based on a set of (hardcoded)rules.
 * 
 * @todo load rules
 * @todo enable complete translation only option (all rules have to be applied, no intermediate results are stored)
 * @todo allow a single rule to apply optionally if there are multiple hits within a single word
 * @author jirka
 */
public class BackTranslateWords  extends MUtilityBase {
    /**
     * Generated tagset
     * Collectted so it can be sorted before writing out.
     */
    //private final List<String> result = new ArrayList<String>();

    public static void main(String[] args) throws IOException {
        new BackTranslateWords().go(args,4);
    }

    
    @Override public void goE() throws java.io.IOException {
        final MultiMap<String,String> new2old =  new ColIo(getArgs().getDirectFile(1)).readInMultiMap(Strings.cWhitespacePattern);
        
        CorpusLineReader r = Corpora.openMm(getArgs().getDirectFile(0));
        
        Tag unknownTag = tagset.fromStringFill("X@");

        final List<Fplus> oldFpluss = new ArrayList<Fplus>();
        for (;;) {
            if (!r.readLine()) break;
            
            Fplus newFplus = r.getFPlus();

            newFplus.getLts().remove(unknownTag);
            if (newFplus.getLts().isEmpty()) continue;
            
            // --- Translate forms back (potentially 1:n) ---
            final Set<String> oldForms = new2old.get(newFplus.getForm());
            if (oldForms.isEmpty()) {
                oldFpluss.add(newFplus);
            }
            else {
                for (String oldForm : oldForms) {
                    oldFpluss.add(new Fplus(oldForm, newFplus.getLts()));
                }
            }
        }
        
        Collections.sort(oldFpluss, Fplus.createFormComparator());
        
        @Cleanup PrintWriter w = IO.openPrintWriter(getArgs().getDirectFile(3));
        boolean alwaysLemma = getArgs().getBool("output.alwaysLemma", true);
        saveWordList(w, alwaysLemma, oldFpluss);
    }

    /**
     * Note: tags are translated as well, this is very hacky. Should be a separate tool or at least work on fplus
     * @param w
     * @param alwaysLemma
     * @param oldFpluss 
     */
    private void saveWordList(PrintWriter w, boolean alwaysLemma, List<Fplus> oldFpluss) throws IOException {
        final MultiMap<String,String> tag2tag =  new ColIo(getArgs().getDirectFile(2)).readInMultiMap(Strings.cWhitespacePattern);

        for (Fplus fplus : oldFpluss) {
            Lts lts = fplus.getLts();
            for (String lemma : lts.lemmas()) {
                
                w.print(fplus.getForm());
                String simpleLemma = translateLemma(lemma);
                if (!fplus.getForm().equals(simpleLemma) || alwaysLemma) w.print(" @ " + simpleLemma);
                
                final Set<String> trTags = new HashSet<String>();
                for (Lt lt : lts.col(lemma)) {
                    final Set<String> tags = tag2tag.get(lt.tag().toString());
                    if (tags == null) {
                        trTags.add(lt.tag().toString());
                    }
                    else {
                        trTags.addAll(tags);
                    }
                }
                
                w.println(Cols.toString(trTags, " ", "", " ", ""));
            }
        }
    }

    private String translateLemma(String aLemma) {
        return PDTX.pureLemma(aLemma);
    }
    
}