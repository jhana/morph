package morph.tmp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Pattern;
import morph.ts.TagUtil;
import morph.util.MUtilityBase;
import util.col.Cols;
import util.col.MultiMap;
import util.io.IO;
import util.str.Strings;

/**
 * Input:  
 * <ul>
 * <li>translation map template 
 * <li>a list of all legal new tags
 * </ul>
 * Output: Fully instantiated translation multimap (old tag -> new tags)
 * @author jirka
 */
public class TranslRusTsOld2New  extends MUtilityBase {
    /**
     * Generated tagset
     * Collectted so it can be sorted before writing out.
     */
    //private final List<String> result = new ArrayList<String>();

    public static void main(String[] args) throws IOException {
        new TranslRusTsOld2New().go(args,3);
    }

    @Override public void goE() throws java.io.IOException {
        final Map<String,String> mapTmpl = new util.ColIo(getArgs().getDirectFile(0)).readInMap();
        final List<String> allNewTags  = IO.readInLines(getArgs().getDirectFile(1));
        final MultiMap<String,String> result = new MultiMap<String, String>();
        
        for (Map.Entry<String,String> on : mapTmpl.entrySet()) {
            String template = on.getValue();
            char variant = Strings.lastChar(template);
            if (variant != '-' && variant != '8') {
                System.out.print("tg: " + template);
                template = setCharAtB(template, 0, '-');
                System.out.println(" -> " + template);
            }
            
            Pattern pattern = TagUtil.createPattern(template);
            List<String> newTags = findAll(allNewTags, pattern);
            System.out.println(" -> " + newTags);

            // restore variant
            if (variant != '-' && variant != '8') {
                List<String> newTagsVar = new ArrayList<String>();
                for (String tg : newTags) {
                    newTagsVar.add( setCharAtB(tg, 0, variant) );
                }
                newTags = newTagsVar;
            }
            
            //if (newTags.isEmpty()) newTags.add("<ERROR!!>") ;
            //Err.fAssert(!newTags.isEmpty(), "No new tags match %s", on.getValue());
            result.addAll(on.getKey(), newTags);
            //System.out.println(on.getKey() + " " + Cols.toString(newTags, "", "", " ", "<ERROR>!"));
        }
        System.out.println(Cols.toStringNl(result.entrySet()));

        PrintWriter w = IO.openPrintWriter(getArgs().getDirectFile(2));
        for (String key : new TreeSet<String>(result.keySet())) {
            w.print(key);
            w.print(" ");
            w.println(Cols.toString(new TreeSet<String>(result.get(key)), "", "", " ", "<ERROR>!"));
        }
        IO.close(w);
    
    }
    
    /**
     * The character at the specified reverse index is set to <code>ch</code>. 
     * A new string is created that is identical to <code>aStr</acode>, 
     * except that it contains the character <code>aChar</code> at position <code>aIdx</code>. 
     *
     * @param      aIdx   the index of the character to modify counted from the end.
     *                <code>0 <= aIdx < aStr.length()</code>
     * @param      aChar      the new character.
     * @throws     IndexOutOfBoundsException  if not aIdx is illegal
     */
    public static String setCharAtB(String aStr, int aIdx, char aChar) {
        return Strings.setCharAt(aStr, aStr.length() - 1 - aIdx, aChar);
    }


    /** todo to util */
    public static List<String> findAll(final Iterable<String> aAllStrs, final Pattern pattern) {
        final List<String> result = new ArrayList<String>();
        for (String str : aAllStrs) {
            if (pattern.matcher(str).matches()) result.add(str);
        }
        return result;
    }

    
    
}

