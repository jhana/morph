package morph.tmp;

import com.google.common.collect.Sets;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.regex.Pattern;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import morph.util.MUtilityBase;
import org.jdom2.Document;
import org.jdom2.Element;
import util.col.Cols;
import util.col.MultiMap;
import util.err.Err;
import util.io.IO;
import util.io.XFile;
import util.io.ezreader.EzReader;

/**
 * Translates a word list based on a set of (hardcoded)rules.
 * 
 * @todo load rules
 * @todo enable complete translation only option (all rules have to be applied, no intermediate results are stored)
 * @todo allow a single rule to apply optionally if there are multiple hits within a single word
 * @author jirka
 */
public class TranslateWords  extends MUtilityBase {
    private void loadTranslations(XFile directFile) throws IOException {
        Document xml = util.Jdom.readXml(directFile);
        for (Element e : util.Jdom.elements(xml, "mutace") ) {
            
            Element preE = e.getChild("pre");
            if (preE != null)
            System.out.printf("e.pre %s > %s\n", preE.getAttributeValue("text"), preE.getAttributeValue("vysledek") );
            
            Element mutE = e.getChild("mut");
            if (mutE != null)
            System.out.printf("e.mut %s > %s\n",  mutE.getAttributeValue("from"), mutE.getAttributeValue("to"));

            Element postE = e.getChild("post");
            if (postE != null)
            System.out.printf("e.post%s > %s\n",  postE.getAttributeValue("text"),  postE.getAttributeValue("vysledek") );
            
            List<String> from = new ArrayList<String>();
            List<String> to = new ArrayList<String>();
            
            from.add(mutE.getAttributeValue("from"));
            final String toStr = mutE.getAttributeValue("to");
            to.add( "0".equals(toStr) ? "" : toStr);
            
            if (preE != null) {
                String ctx_fromStr = preE.getAttributeValue("text");
                boolean bound =  "|".equals(ctx_fromStr);
                String[] ctx_from = bound ? new String[] {"^"} : ctx_fromStr.split("\\|");
                String[] ctx_to   = bound ? new String[] {""}  : preE.getAttributeValue("vysledek").split("\\|");

                List<String> from2 = new ArrayList<String>();
                List<String> to2 = new ArrayList<String>();
                
                for (int r = 0; r < from.size(); r++) {
                    for (int i = 0; i < ctx_from.length; i++) {
                        from2.add( ctx_from[i] + from.get(r));
                        to2  .add( ctx_to[i] + to.get(r));
                    }
                }

                from = from2;
                to   = to2;
            }
            
            if (postE != null) {
                String ctx_fromStr = postE.getAttributeValue("text");
                boolean bound =  "|".equals(ctx_fromStr);
                String[] ctx_from = bound ? new String[] {"$"} : ctx_fromStr.split("\\|");
                String[] ctx_to   = bound ? new String[] {""}  : postE.getAttributeValue("vysledek").split("\\|");
                Err.fAssert(ctx_from.length == ctx_to.length, "diff len post_from %s post_to", ctx_from, ctx_to);

                List<String> from2 = new ArrayList<String>();
                List<String> to2 = new ArrayList<String>();
                
                for (int r = 0; r < from.size(); r++) {
                    for (int i = 0; i < ctx_from.length; i++) {
                        from2.add(  from.get(r) + ctx_from[i]);
                        to2  .add(  to.get(r)   + ctx_to[i]);
                    }
                }
                
                from = from2;
                to   = to2;
            }
            
            for (int r = 0; r < from.size(); r++) {
                translations.add( new Translation(from.get(r), to.get(r)) );
                System.out.println(Cols.last(translations));
             }            
           
        }
    }

    // todo make it more like a phonological rule (left _ right)
    @RequiredArgsConstructor
    @ToString
    class Translation {
        private final Pattern pattern;
        private final String replacement;

        public Translation(String orig, String replacement) {
            this(Pattern.compile(orig), replacement);
        }
        
        public Set<String> translate(Set<String> aInput) {
            final Set<String> out = new HashSet<String>();
            
            for (String str : aInput) {
                String str2 = pattern.matcher(str).replaceAll(replacement);
                if (!str.equals(str2)) out.add(str2);
            }

            if (out.isEmpty()) {
                return aInput;
            }
            else {
                out.addAll(aInput);
                return out;
            }
        }
    }
    
    private final List<Translation> translations = new ArrayList<Translation>(); 
    
    /**
     * Generated tagset
     * Collectted so it can be sorted before writing out.
     */
    //private final List<String> result = new ArrayList<String>();

    public static void main(String[] args) throws IOException {
        new TranslateWords().go(args,4);
    }

    
    @Override public void goE() throws java.io.IOException {
        loadTranslations( getArgs().getDirectFile(0) );
//        translations.add(new Translation("ie", "í"));
//        translations.add(new Translation("óv", "ův"));
        
        final List<String> origWords = new EzReader(getArgs().getDirectFile(1)).configureSplitting("Word", "Freq").readColumn(0);
        final MultiMap<String,String> orig2new = translate(origWords);

        // write new 2 orig dic
        writeDic(orig2new);
        
        // write new forms
        IO.writeLines(getArgs().getDirectFile(3), Sets.newTreeSet(orig2new.allValues()));
    
    }

    private MultiMap<String,String> translate(final List<String> origWords) {
        final MultiMap<String,String> orig2new = new MultiMap<String,String>();
        
        for (String origWord : origWords) {
            Set<String> result = new HashSet<String>();
            result.add(origWord);
            
            for (Translation tr : translations) {
                result = tr.translate(result);
            }

            orig2new.addAll(origWord, result);
        }
        
        return orig2new;
    }
    
    private void writeDic(MultiMap<String, String> orig2new) throws IOException {
        @Cleanup PrintWriter w = IO.openPrintWriter(getArgs().getDirectFile(2));
        
        for (Map.Entry<String, Set<String>> entry : orig2new.entrySet()) {
            for (String neww : entry.getValue()) {
                w.println(neww + "\t" + entry.getKey());
            }
        }
    }

}