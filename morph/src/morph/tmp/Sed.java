package morph.tmp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import morph.util.MUtilityBase;
import util.io.IO;
import util.io.LineReader;

/**
 * Quick and dirty sed-like processor but with perl-style regex
 * @author jirka
 */
public class Sed extends MUtilityBase {
    final List<String> commands = Arrays.asList(
        "s#^.<#<#",// for some reason <doc is sometimes preceded by ef bb bf
        "s#^<#//<#",
        "s#(//<doc)#<doc>$1#",
        "s#(//</doc>)#</doc>$1#",
        "s#(//<odstavec)#<p>$1#",
        "s#(//</odstavec>)#</p>$1#",
        "s#(\\S+)\\s\\s+(.+)#$1  // $2#",
        "s#//.*##",
        "s#(^[^<\\s].*$)#<f>$1#",
        "s#\\s*$##"
    );
    
    final List<Pattern> patterns = new ArrayList<Pattern>();
    final List<String> replacements = new ArrayList<String>();
    
    public static void main(String[] args) throws IOException {
        new Sed().go(args, 2);
    }
 
    @Override
    public void goE() throws java.io.IOException {
        compile();
        process();
    }

    private void compile() {
        for (String str : commands) {
            String[] strs = str.split("\\#", -1);
            patterns.add(Pattern.compile(strs[1]));
            replacements.add(strs[2]);
            System.out.printf("ed: '%s' > %s\n", strs[1], strs[2]);
        }
    }

    private void process() throws IOException {
        LineReader  r = IO.openLineReader( getArgs().getDirectFile(0));
        PrintWriter w = IO.openPrintWriter(getArgs().getDirectFile(1));
        
        proces(r,w);
        
        IO.close(r,w);
    }

    private void proces(LineReader r, PrintWriter w) throws IOException {
        for(;;) {
            String line = r.readLine();
            if (line == null) return;
            
            for (int i = 0; i < patterns.size(); i++) {
                Pattern pat = patterns.get(i);
                String replacement = replacements.get(i);
                
                line = pat.matcher(line).replaceFirst(replacement);
            }
            
            w.println(line);
        }
    }
}
