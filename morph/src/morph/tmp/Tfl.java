package morph.tmp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Cleanup;
import morph.ma.Morph;
import morph.ma.MorphCompiler;
import morph.ma.lts.Lt;
import morph.ma.lts.Lts;
import morph.util.MUtilityBase;
import util.err.Err;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;
import util.str.Strings;

/**
 *
 * @author jirka
 */
public class Tfl  extends MUtilityBase {

    /**
     */
    public static void main(String[] args) throws IOException {
        new Tfl().go(args);
    }

    /**
     * Morphological analyzer
     */
    private Morph mMorph;

    /**
     * Counter of processed words
     */
    private int mWords;

    private void go(String[] aArgs) throws java.io.IOException  {
        argumentBasics(aArgs,2);

        XFile inFile  = getArgs().getDirectFile(0);  // simple list of forms
        XFile outFile = getArgs().getDirectFile(1);  //

        prepareMa();

        profileStart("Adding info from MA ...");
        ma(inFile, outFile);
        profileEnd();
    }

    private void prepareMa() throws java.io.IOException {
        mMorph = MorphCompiler.factory().compile(getArgs());
        Err.assertFatal(mMorph != null, "Cannot create Morph object");
    }

    private Map<String, String> parseMap(String aText) {
        final Map<String,String> map = new HashMap<String,String>();
        if (aText.isEmpty()) return map;

        char sep = aText.charAt(0);

        final List<String> items = Strings.splitL(aText.substring(1), sep);
        Err.fAssert(items.size() % 2 == 0, "Map specification requires even number of tokens (%s)", items);

        for (int i = 0; i < items.size(); i +=2) {
            String prev = map.put(items.get(i), items.get(i+1));
            Err.fAssert(prev == null, "Repeated value %s",  items.get(i));
        }

        return map;
    }

    // todo partition output by tag as well (e.g. POS, gender for nouns)
    private void ma(XFile aInFile, XFile aOutFile) throws IOException {
        @Cleanup LineReader  r = IO.openLineReader(aInFile);            // todo replace with plain text corpus reader
        @Cleanup PrintWriter w = IO.openPrintWriter(aOutFile);
        r.configureSplitting(Strings.cWhitespacePattern, 2, "required <form> <f>:<f>:<f>:<f>");
        Set<String> done = new HashSet<String>();

        boolean alwaysLemma = getArgs().getBool("out.alwaysLemma", true);

        String prefixStr = getArgs().getString("out.prefix");  // e.g. "// " so that correct entries can be uncommented, or "" if they should be entered directly
        Map<String, String> srcPrefixes = parseMap(prefixStr);
        String srcStr = getArgs().getString("out.suffix");
        Map<String, String> srcSuffixes = parseMap(srcStr);

        for(;;) {
            String[] strs = r.readSplitLine();
            if (strs == null) break;
            String form = strs[0].trim();
            String formLc = form.toLowerCase();
            boolean alreadyDidLc = (done.contains(formLc) && Character.isUpperCase(form.charAt(0)));
            done.add(formLc);
            final Lts lts = mMorph.analyze(form);

            if (!lts.isEmpty()) {

                for (String lemma : lts.lemmas()) {
                    List<Lt> lemmaLts = lts.col(lemma);

                    if (alreadyDidLc) {
                        w.print("// ");
                    }

                    // collect sources, skip top, if in other sources
                    Set<String> srcs = collectSrcs(lemmaLts);
                    for (String src : srcs) {                   // todo partition by map (merge some srcs togetther)
                        String prefix = srcPrefixes.get(src);
                        if (prefix != null) w.print(prefix);

                        w.print(form);
                        if (!form.equals(lemma) || alwaysLemma) w.print(" @ " + lemma);
                        for (Lt lt : lts.col(lemma)) {
                            if (!src.equals(lt.src())) continue;

                            w.print(" " + lt.tag());
                        }

                        String suffix = srcSuffixes.get(src);
                        if (suffix != null)
                            w.print(suffix);
                        else {
                            w.print(" // " + src);
                        }

                        w.println();
                    }
                }

                mWords++;
            }
            else {
                if (alreadyDidLc) w.print("// ");
                w.println(form);
            }
        }
    }


    private Set<String> collectSrcs(Collection<Lt> aLts) {
         final Set<String> srcs = new HashSet<String>();
        for (Lt lt : aLts) {
            srcs.add(lt.src());
        }
        return srcs;
    }

}

