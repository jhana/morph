package morph.tmp;

import java.io.IOException;
import java.util.regex.Pattern;
import morph.io.Corpora;
import morph.io.CorpusLineReader;
import morph.io.TntWriter;
import morph.util.MUtilityBase;
import morph.ts.Tag;
import util.err.Err;
import util.io.IO;
import util.io.XFile;

/**
 * Add hoc class combinign Old Czech taggers.
 * @author jirka
 */
public class CombineOCz  extends MUtilityBase {
    public static void main(String[] args) throws IOException {
        new CombineOCz().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,4);

        XFile defaultTagger  = getArgs().getDirectFile(0);
        XFile altTagger      = getArgs().getDirectFile(1);  // todo allow a sequence
        Pattern whenAlt      = Pattern.compile(getArgs().getDirectArg(2));
        XFile outFile       = getArgs().getDirectFile(3);

        merge(defaultTagger, altTagger, whenAlt, outFile);

    }

    private void merge(XFile defaultTaggerFile, XFile altTaggerFile, Pattern aWhenAlt, XFile outFile) throws IOException {
        CorpusLineReader rd = null;
        CorpusLineReader rma = null;
        TntWriter w = null;

        try {
            rd  = Corpora.openM(defaultTaggerFile);
            rma = Corpora.openM(altTaggerFile);
            w = new TntWriter(outFile);

            for (;;) {
                boolean rdStatus  = rd.readLine();
                boolean rmaStatus = rma.readLine();
                Err.iAssert(rdStatus == rmaStatus, "Diff number of lines");
                if (!rdStatus) break;

                Tag tag;
                if (rma.tag().matches(aWhenAlt)) {
                    tag = rma.tag();
                }
                else {
                    tag = rd.tag();
                }

                w.writeLine(rd.form(), tag);
            }
        }
        finally {
            IO.close(rd,rma,w);
        }
    }

}
