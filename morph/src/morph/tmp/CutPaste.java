package morph.tmp;

import java.io.IOException;
import lombok.Cleanup;
import morph.io.Corpora;
import morph.io.CorpusLineReader;
import morph.io.TntWriter;
import morph.io.WordReader;
import morph.util.MUtilityBase;
import util.err.Err;
import util.io.XFile;

/**
 * Replaces forms in a a corpus with forms from another file (on tnt simulates cut/paste)
 *
 * @author jirka
 */
public class CutPaste  extends MUtilityBase {
    public static void main(String[] args) throws IOException {
        new CutPaste().go(args);
    }

    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,3);

        XFile formsFile   = getArgs().getDirectFile(0);
        XFile tagsFile    = getArgs().getDirectFile(1);
        XFile outFile     = getArgs().getDirectFile(2);

        merge(formsFile, tagsFile, outFile);

    }

    private void merge(XFile inDirectFile, XFile inMaFile, XFile outFile) throws IOException {
        @Cleanup WordReader fR = Corpora.openWordReader(inDirectFile);
        @Cleanup CorpusLineReader tR = Corpora.openM(inMaFile);
        @Cleanup TntWriter w = new TntWriter(outFile);          // todo make general

        for (;;) {
            boolean rfStatus  = fR.nextWord();
            boolean rtStatus = tR.readLine();
            Err.iAssert(rfStatus == rtStatus, "Diff number of lines");
            if (!rfStatus) break;

            w.writeLine(fR.word(), tR.tag());
        }
    }

}
