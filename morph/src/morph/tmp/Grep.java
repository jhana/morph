package morph.tmp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Cleanup;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;

/**
 *
 * @author jirka
 */
public class Grep extends util.UtilityBase {

    public static void main(String[] args) throws IOException {
        new Grep().go(args, 3);
    }

    @Override
    public void goE() throws IOException {
        String pattern = getArgs().getDirectArg(0);
        XFile inFile = getArgs().getDirectFile(1);
        XFile outFile = getArgs().getDirectFile(2);
        
        filter(Pattern.compile(pattern), -1, inFile, outFile);
    }


    private void filter(final Pattern regex, final int group, final XFile inFile, final XFile outFile) throws IOException {
        @Cleanup LineReader r = IO.openLineReader(inFile);
        @Cleanup PrintWriter w = IO.openPrintWriter(outFile);
        
        for (;;) {
            String line = r.readLine();
            if (line == null) break;
            Matcher m = regex.matcher(line);
            if (m.find()) {
                if (group == -1) {
                    w.println(line);
                }
                else {
                    w.println(m.group(group));
                }
            }
            
        }
        
    }
}
