
package morph.lg.rus;

import java.io.IOException;
import java.util.regex.Pattern;
import morph.TagTransl;

/**
 *
 * @author Jirka dot Hana at gmail dot com
 */
public class NewTsTagTransl extends TagTransl {

    public static void main(String[] args) throws IOException {
        new NewTsTagTransl().go(args);
    }

    private final Pattern verbTag = Pattern.compile("V.*");
    private final Pattern reflVerbTag = Pattern.compile("V........R......");
    private final Pattern ireflVerbTag = Pattern.compile("V........I......");
    private final Pattern reflVerbForm = Pattern.compile(".*\u0441(\u044F|\u044C)");

    @Override
    protected boolean dropTag(String form, String lemma, String tag) {
        if (reflVerbTag.matcher(tag).matches()) return !reflVerbForm.matcher(form).matches();
        if (ireflVerbTag.matcher(tag).matches()) return reflVerbForm.matcher(form).matches();

        return false;

    }

}
