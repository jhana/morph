/*
 * Sgml.java
 *
 * Created on December 3, 2005, 3:52 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package morph.sb;

import java.io.IOException;
import org.xml.sax.ContentHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author Jirka
 */
public class Sgml {

    /** Default parser to use */
    private String cVendorParserClass = "org.apache.xerces.parsers.SAXParser";
    
    public static void main(String[] args) {
        try {
            Sgml viewer = new Sgml();
            viewer.parse(args[0]);
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /** Creates a new instance of Sgml */
    public Sgml() {
    }

    public void parse(String xmlURI) throws IOException, SAXException {
        
        // Create instances needed for parsing
        XMLReader reader = XMLReaderFactory.createXMLReader(cVendorParserClass);
        ContentHandler jTreeContentHandler = new SgmlContentHandler();
        ErrorHandler jTreeErrorHandler = new JTreeErrorHandler();
        
        // Register content handler
        reader.setContentHandler(jTreeContentHandler);
        
        // Register error handler
        reader.setErrorHandler(jTreeErrorHandler);
        
        // Parse
        InputSource inputSource = new InputSource(xmlURI);
        reader.parse(inputSource);
    }
    
}
