package morph.gen;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.regex.Pattern;
import lombok.Cleanup;
import util.col.MultiMap;
import util.Util;
import util.err.Log;
import util.io.*;
import morph.ts.Tag;
import morph.io.*;
import morph.ma.Context;
import morph.ma.Morph;
import morph.ma.MorphCompiler;
import morph.util.MUtilityBase;
import morph.ma.lts.*;
import util.col.Cols;


/**
 * Commandline interface to MorphGen.
 * 
 * @todo under development  
 * @author j
 */
public class Generator extends MUtilityBase {
    /**
     * Morphological analyzer
     */
    protected MorphGen mMorph;

    protected CorpusLineReader in;

    /**
     * Printer where the result will be directed (@todo see argument -o in main fnc)
     * @see #main(String[])
     */
    protected CorpusWriter out;

    protected Filter mOutFilter;

    /**
     * Regex pattern for filtering input.
     * If the pattern is not null, only words satisfying it are analyzed.
     */
    protected Pattern mNarrow;

    /** Statistics */
    protected int mWords = 0;


    /**
     *
     * See helpString()
     * @param args
     */
    public static void main(String[] args) throws IOException {
       new Generator().go(args);
    }

    /**
     *
     * @param aArgs
     * @throws java.io.IOException
     */
    public void go(String[] aArgs) throws java.io.IOException {
        XFile outFile = null;

        //todo tmp
//        argumentBasics(aArgs,2);
//        profileStart("Loading Morphology ... ");
//        if (!setupMorph()) System.exit(-1);
//        //Lt.setInfoIrelevant(false);
//        profileEnd("Done in %2.2f s.\n");

//        if (Arrays.asList(aArgs).contains("-direct")) {
//            argumentBasics(aArgs,0);
//            in = new LineWordReader(new InputStreamReader(System.in, Encoding.cUtf8.getId()), null);
//            mOut = new PdtWriter(new PrintWriter(new OutputStreamWriter(System.out,Encoding.cUtf8.getId()), true));
//            mOut.setOptions(getArgs().getSubtree("output"));
//            mOut.optionsUpdated();
//            getArgs().set("considerCtx", false);
//        }
//        else {
            argumentBasics(aArgs,2);
            // --- set up input/output ---
            XFile inFile = getArgs().getDirectFile(0, cCsts);
                 outFile = getArgs().getDirectFile(1, cCsts);

            in = Corpora.openM(inFile);
            out = Corpora.openWriter(outFile, getArgs().getSubtree("output"));
//        }
        Log.setILevel(Level.ALL);  // @todo something better


        mNarrow    = getArgs().getPatternNE("narrow");
        mOutFilter = out.getFilter();

        // --- set up the morph object ---
        profileStart("Loading Morphology ... ");
        if (!setupMorph()) System.exit(-1);
        //Lt.setInfoIrelevant(false);
        profileEnd("Done in %2.2f s.\n");

        // --- Do the main task ---
        profileStart("Analyzing ... ");
        generate();
        profileEnd("Done  in %2.2f s. Speed: %2.2f tokens/s.\n", mWords);

        IO.close(out);
    }


    private boolean setupMorph() throws IOException {
        //Log.setLevel(0);
        Lt.setInfoIrelevant(getArgs().getBool("infoIrelevant"));  // tODO needed for some filtering (leo) but do not want to keep duplicates in results
        System.err.println("infoIrelevant=" + Lt.isInfoIrelevant());
        mMorph = morph.gen.MorphCompiler.factory().compile(getArgs());
        return (mMorph != null);
    }


    private void generate() throws java.io.IOException {
        out.writeHeader();

        for(;;) {
            if (!in.readLine()) break;
            
            if (in.isTokenLine()) {
                final Fplus fplus = in.getFPlus();
                final Collection<String> forms = mMorph.generate(fplus.lts.ltE());
                out.writeLine(Cols.toString(forms, "", "", "|", ""), fplus.lts);
                
            }
            else {
                // todo pass line if possible ?
            }
        }

        out.writeFooter();
        IO.close(in,out);
    }

}

