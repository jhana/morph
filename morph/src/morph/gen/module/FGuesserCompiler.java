package morph.gen.module;

import java.io.IOException;
import java.util.regex.Pattern;
import lombok.Cleanup;
import lombok.Getter;
import morph.common.ModuleCompiler;
import morph.io.ReaderParser;
import morph.ma.module.LexEntry;
import util.col.MultiMap;
import util.io.XFile;



/*
 *
 * @author  Jiri
 */
public class FGuesserCompiler extends ModuleCompiler<FGuesser> {
    @Getter final protected GenParadigms paradigms = new GenParadigms();
    
    @Override
    protected void compileI() throws IOException {
        initParadigms();
    }

    private boolean initParadigms() throws java.io.IOException {
        try {
            System.out.println("ac 1 ");
            paradigms.init(mModuleId, args);
            System.out.println("ac 2 ");
            if (paradigms.compile(ulog) > 0) return false;
            System.out.println("ac 3 ");
            paradigms.configure();
            System.out.println("ac 4 ");
        }
        catch(Throwable e) {
            ulog.handleError(e, "Cannot load paradigms submodule");
            System.err.println(e);
            e.printStackTrace();
        }
        return true;
    }
    
    
}
