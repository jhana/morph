package morph.gen.module;

import morph.common.ModuleCompiler;
import morph.io.*;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import lombok.Cleanup;
import morph.gen.module.WordModule.LemmaEntry;
import morph.ma.module.WordEntry;
import util.io.*;
import morph.ts.Tag;
import util.err.FormatError;
import util.err.UException;
import util.str.Strings;

/**
 *
 * @author  Jiri
 */
class WordModuleCompiler extends ModuleCompiler<WordModule> {


// =============================================================================
// Implementation <editor-fold desc="Implementation">
// =============================================================================

    private String mNegPref;
    private String mNegationOk;
    private boolean mAutoNegation;
    private int wordLimit;
    private boolean skipEmptyWords;        // empty words are ignored (the annotator skipped them)

    private boolean tt;

    @Override
    protected void compileI() throws IOException {
        mModule.lemma2entry = new HashMap<>();

        mNegationOk = mModuleArgs.getString("negationOk", "");
        if (mNegationOk.equals("")) {       // get default key's value
            mNegationOk = args.getString("negationOk", "");
        }
        mAutoNegation = mNegationOk.length() > 0;
        mNegPref = args.getString("negationPref", "xxxx");

        wordLimit = mModuleArgs.getInt("wordLimit", Integer.MAX_VALUE);

        skipEmptyWords = mModuleArgs.getBool("skipEmptyWords", true);

        // todo remove tag translation and make a dedicated tool
        XFile ttMapFile = mModuleArgs.getFile("tt");
        tt = (ttMapFile != null);

        readInWords(getMFiles("file"));
    }

    private void readInWords(List<XFile> aFileNames) throws java.io.IOException {
        @Cleanup final ReaderParser r = new ReaderParser(aFileNames, inFilter, "words");

        for(int wordCounter=0; wordCounter < wordLimit;wordCounter++) {
            if (r.nextLine() == null) break;
            try {
                readEntry(r);
            }
            catch (java.util.NoSuchElementException | UException e) {
                handleError(e,r);
            }
        }
    }

    private void readEntry(ReaderParser aR) throws IOException {
        final String token  = aR.getStringF("initial entry's token");

        if (token.startsWith("@")) {
            try {
                switch (token) {
                    case "@":
                        readInGroup(aR);
                        break;
                    case "@p":
                        readInSmallPrdgm(aR);
                        break;
                    case "@pp":
                        readInSmallPrdgmX(aR);
                        break;
                    default:
                        assertTrue(false, aR, "Nonexistent @ code - %s", token);
                        break;
                }
            }
            catch (UException | FormatError | NoSuchElementException e) {
                handleError(e, aR);
                aR.skipUntil("}");
            }
        }
        else {
            readInWordEntry(aR, token);
        }
    }

    /**
     * Simple word entry: form (@ lemma) tag(s).
     *
     *
     * @param aR
     * @param aForm form string (cannot use / separator)
     * @throws IOException
     */
    private void readInWordEntry(ReaderParser aR, String aForm) throws IOException{
        if (!aR.hasMoreTokens() && skipEmptyWords) return;                      // ignoring words without any information

        final String lemma = (aR.eatIf("@")) ? aR.getStringF("lemma") : aForm;

        // --- read tags and create entries ---
        do {
            Tag tag = aR.getTagFill("tag");
            addEntryNeg(aForm, lemma, tag);
        } while(aR.hasMoreTokens());        // @todo negation/degrees???
    }


    /** Single tag multiple forms (lemma is the same as each form) */
    private void readInGroup(ReaderParser aR) throws IOException {
        final Tag tag = aR.getTagFill("tagx");

        // --- words for that tag ---
        for(;;) {
            if (aR.nextLine() == null) return;

            do {
                String form  = aR.getStringF("form");
                if (form.equals("}")) return;
                addEntryNeg(form, form, tag);
            } while (aR.hasMoreTokens());
        }
    }



    private void readInSmallPrdgm(ReaderParser aR) throws IOException {
        final String lemma   = aR.getStringF("lemma.");
        final String tagTmpl = aR.getString("fullTag.");

        // --- read form & tags and create entries ---
        for(;;) {
            if (aR.nextLine() == null) break;
            final String formStr  = aR.getStringF("form.");
            if (formStr.equals("}")) return;

            addEntryForTags(aR, formStr, lemma, tagTmpl);
        }
    }


    private void readInSmallPrdgmX(ReaderParser aR) throws IOException {
        String lemma   = aR.getStringF("lemma.");

        // --- read form & tags and create entries ---
        for(;;) {
            if (aR.nextLine() == null) break;
            String formStr  = aR.getStringF("form.");
            if (formStr.equals("}")) return;

            if (formStr.equals("@p")) {
                readInSubPrdgmX(aR, lemma);
            }
            else {
                addEntryForTags(aR, formStr, lemma, null);
            }

            // todo read in named subprdgm
        }
    }

    private void readInSubPrdgmX(ReaderParser aR, String aLemma) throws IOException {
        String tagTmpl = aR.getString("tagTemplate.");

        // --- read form & tags and create entries ---
        for(;;) {
            if (aR.nextLine() == null) break;
            String formStr  = aR.getStringF("form.");
            if (formStr.equals("}")) return;

            addEntryForTags(aR, formStr, aLemma, tagTmpl);
        }
    }

    private final static Pattern cFormSeparator = Pattern.compile("/");
    private void addEntryForTags(final ReaderParser aR, final String aFormStr, final String aLemma, final String aTagTmpl) {
        final List<String> forms = Strings.splitL(aFormStr, cFormSeparator);
        do {
            final Tag tag = aTagTmpl == null ? aR.getTagFill("tag") : aR.getTag("tag", aTagTmpl);
            for (String form : forms) {
                addEntryNeg(form, aLemma, tag);
            }
        } while (aR.hasMoreTokens());
    }

    /**
     * Add a word to the dictionaryy, adding affirmative and negated tag if requested and appropriate.
     * todo generalze, currently works only with the Czech PDT tagset!
     *
     * @param tag
     * @param form
     * @param lemma
     */
    private void addEntryNeg(String form, String lemma, Tag tag) {
        if (mAutoNegation && tag.getNegation() == '-' && tag.subPOSIn(mNegationOk)) {
            addEntry(form, lemma, tag.setNegation('A'));
            addEntry(mNegPref + form, lemma, tag.setNegation('N'));
        }
        else {
            addEntry(form, lemma, tag);
        }
    }

    // todo the only difference from the corresponding analysis compiler
    private void addEntry(String aForm, String aLemma, Tag aTag) {
        LemmaEntry le = mModule.lemma2entry.get(aLemma);
        if (le == null) {
            le = new LemmaEntry();
            mModule.lemma2entry.put(aLemma, le);
        }
        
        Set<WordEntry> wes = le.getTag2forms().getS(aTag);

        
        //System.out.printf("Adding entry %s %s %s\n", aForm, aLemma, aTag);
        wes.add(new WordEntry(aForm, aLemma, aTag));   // todo eq, hash
    }
}
