package morph.gen.module;

import java.util.*;
import lombok.Getter;
import morph.ma.lts.*;
import morph.ma.module.LexEntry;
import morph.common.UserLog;
import morph.ma.module.prdgm.Ending;
import util.col.MultiMap;

/**
 * Module responsible for analyzing words using a lexicon and paradigms
 * @author  Jirka Hana
 */

public class LexGenModule extends GenModule {
    @Getter protected morph.gen.module.GenParadigms paradigms;
    @Getter protected MultiMap<String,LexEntry> lemma2entry;     //  lemma -> entry

    @Override
    public int compile(UserLog aUlog)  {
        LexModuleCompiler compiler = new LexModuleCompiler();
        compiler.init(this, moduleId, aUlog, args);
        System.out.println("aa 1");
        compiler.compile();
        System.out.println("aa 2");
        if (compiler.getUlog().getErrors() > 0) return compiler.getUlog().getErrors();
        System.out.println("aa 3");
        
        paradigms   = compiler.getParadigms();
        lemma2entry = compiler.getLemma2entry();

        System.out.println("aa 4");
                
        return 0;
    }

    @Override
    public Collection<String> generate(final Lt aLt) {
        final Set<LexEntry> entries = lemma2entry.get(aLt.lemma());
        if (entries == null) return Collections.emptySet();
        
        final Collection<String> forms = new HashSet<>();
        for (LexEntry entry : entries) {
            generate(aLt, entry, forms);
        }
        
        return forms;
    }
    
    private void generate(final Lt aLt, final LexEntry entry, final Collection<String> aResults) {
        final Set<Ending> endings = paradigms.getPt2endings().get(entry.paradigm, aLt.tag());
        if (endings == null) return;
        
        for (Ending ending : endings) {
            String form = genderateForm(entry, ending);
            aResults.add(form);
        }
        
    }

    private String genderateForm(LexEntry entry, Ending ending) {
        String stem = entry.getStem(ending.getStemId());
        return stem + ending.getEnding();
    }
}

