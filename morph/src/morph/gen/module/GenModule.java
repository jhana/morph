package morph.gen.module;

import java.util.*;
import morph.ma.lts.*;

/**
 * Generator module
 * 
 * todo under dev
 * todo extract things shared by Anal and Gen module
 *
 * @author  Jirka Hana
 */
public abstract class GenModule extends morph.common.Module {
    
    /**
     * 
     */
    public abstract Collection<String> generate(Lt aLt);
}
