package morph.gen.module;

import lombok.Getter;
import morph.ma.module.Paradigms;

/**
 *
 * @author  Jirka
 */
public abstract class EndingAModule extends GenModule {
    @Getter protected Paradigms paradigms;
}
