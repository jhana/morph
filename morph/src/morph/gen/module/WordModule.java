package morph.gen.module;


import java.util.*;
import lombok.Getter;
import morph.ts.Tag;
import morph.ma.lts.*;
import morph.common.UserLog;
import morph.ma.module.WordEntry;
import util.col.MultiMap;
import util.col.MappingSet;

/**
 *
 * @author  Jirka Hana
 */
public class WordModule extends GenModule {

    static class LemmaEntry {
        @Getter MultiMap<Tag,WordEntry> tag2forms = new MultiMap<>();
    }
    
    
    @Getter Map<String, LemmaEntry> lemma2entry;

    @Override
    public int compile(UserLog aUlog)  {
        return new WordModuleCompiler().init(this, moduleId, aUlog, args).compile();
    }
    

    @Override
    public Collection<String> generate(Lt aLt) {
//        System.out.printf("generating %s:%s", aLt.lemma(), aLt.tag());
        
        final LemmaEntry le = lemma2entry.get(aLt.lemma());
//        System.out.println("found lemma " + (le != null));
        if (le == null) return Collections.emptySet();
        
        final Set<WordEntry> wes = le.getTag2forms().get(aLt.tag());
//        System.out.println("found wes " + (wes != null));
        
        return (wes == null) ? Collections.<String>emptySet() : new MappingSet<>(wes, WordEntry.e2form);
    }

}
