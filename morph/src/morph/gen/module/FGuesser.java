package morph.gen.module;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import morph.common.UserLog;
import morph.ma.lts.Lt;
import morph.ma.module.LexEntry;
import morph.ma.module.prdgm.DeclParadigm;
import morph.ma.module.prdgm.Ending;
import morph.ts.Tag;

/**
 * 
 * @author  Jiri
 */
public final class FGuesser extends GenModule {
    @Getter protected morph.gen.module.GenParadigms paradigms;

    @Override
    public int compile(UserLog aUlog)  {
        FGuesserCompiler compiler = new FGuesserCompiler();
        compiler.init(this, moduleId, aUlog, args);
        compiler.compile();
        if (compiler.getUlog().getErrors() > 0) return compiler.getUlog().getErrors();
        
        paradigms   = compiler.getParadigms();

                
        return 0;
    }

    @Override
    public Collection<String> generate(final Lt aLt) {
        String lemma = aLt.lemma();
        
        final Collection<String> forms = new HashSet<>();
        for (DeclParadigm prdgm : paradigms.getParadigms().values()) {
            String[] stems = prdgm.allStems(lemma);       // epen???
        // get stem+prdgm
            for (String stem : stems) {
                generate(stem,  prdgm, aLt.tag(), forms);
            }
        }
        return forms;
    }
    
    private void generate(final String stem,  DeclParadigm aPrdgm, final Tag aTag, final Collection<String> aResults) {
        final Set<Ending> endings = getEndings(aPrdgm, aTag);
        
        for (Ending ending : endings) {
            String form = genderateForm(stem, ending);
            aResults.add(form);
        }
        
    }
    
    private Set<Ending> getEndings(final DeclParadigm aPrdgm, final Tag aTag) {
        final Set<Ending> es = new HashSet<>();
        for (Ending e : aPrdgm.getEndings()) {  // todo precompute
            if (e.getTag().equals(aTag)) es.add(e);
        }
        return es;
        
    }
    

    private String genderateForm(String stem, Ending ending) {
        return stem + ending.getEnding();
    }
}
