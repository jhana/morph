package morph.gen.module;

import java.io.*;
import java.util.regex.Pattern;
import lombok.Cleanup;
import lombok.Getter;
import morph.common.ModuleCompiler;
import morph.io.*;
import morph.ma.module.LexEntry;
import util.col.MultiMap;
import util.io.*;

/**
 *
 * @author  Jiri
 */
public class LexModuleCompiler extends ModuleCompiler<LexGenModule> {
    @Getter final protected GenParadigms paradigms = new GenParadigms();
    @Getter final protected MultiMap<String,LexEntry> lemma2entry = new MultiMap<>();     //  lemma -> entry
    
    @Override
    protected void compileI() throws IOException {
        System.out.println("ab 1 ");
        if (!initParadigms()) return;
        System.out.println("ab 2 ");
        readInLexicon(getMFile("file"));
        System.out.println("ab 3 ");
    }

    private boolean initParadigms() throws java.io.IOException {
        try {
            System.out.println("ac 1 ");
            paradigms.init(mModuleId, args);
            System.out.println("ac 2 ");
            if (paradigms.compile(ulog) > 0) return false;
            System.out.println("ac 3 ");
            paradigms.configure();
            System.out.println("ac 4 ");
        }
        catch(Throwable e) {
            ulog.handleError(e, "Cannot load paradigms submodule");
            System.err.println(e);
            e.printStackTrace();
        }
        return true;
    }
    
    
    /**
     * Reads in the lexicon from the specified file 
     * Required format: lemma stem paradigmId
     */
    private void readInLexicon(XFile aFileName) throws java.io.IOException {
        @Cleanup ReaderParser r = new ReaderParser(aFileName, inFilter, "lexicon"); 

        for(;;) {
            if (r.nextLine() == null) break;
            
            try {
                readLexEntry(r);
            }
            catch (java.util.NoSuchElementException e) {
                ulog.handleError(e);
            }
        }
    }

    // todo share with paradigms compiler
    private final static Pattern cStemIdPattern = Pattern.compile("\\@\\d+");

    private void readLexEntry(ReaderParser aR)  {
        final LexEntry le = new LexEntry();

        le.lemma = aR.getStringF("lemma");
        le.stem  = aR.getStringF("stem");
        lemma2entry.add(le.stem, le);

        while (aR.nextTokenMatches(cStemIdPattern)) {
            int stemId = Integer.valueOf(aR.getString("stemId").substring(1)) - 1;
            String stem = aR.getStringF("stem" + stemId);
            // todo check it has not been specified yet
            le.addStem(stemId, stem);
            lemma2entry.add(stem, le);
        }

        String paradigmId = aR.getString("paradigmId");
        le.paradigm = paradigms.getParadigm(paradigmId);
        // @todo/TODO handle the errors 
        ulog.assertTrue(le.paradigm != null, aR, "the paradigm id %s is not defined", paradigmId);
        // todo assertTrue(le.paradigm.nrOfStems() == le.stems.size(), aR, "Stems problems @todo");

        // --- if additional stems not specified, default them to the stem 1 --- ?? todo
    }


}
