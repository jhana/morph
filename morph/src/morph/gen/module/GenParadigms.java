package morph.gen.module;

import java.io.IOException;
import morph.ma.module.prdgm.ParadigmsCompiler;
import java.util.*;
import lombok.Getter;
import morph.common.Module;
import morph.common.UserLog;
import morph.ma.module.prdgm.DeclParadigm;
import morph.ma.module.prdgm.Ending;
import morph.ma.module.prdgm.Paradigm;
import morph.ts.Tag;
import util.col.Multi2Map;
import util.col.MultiMap;
import util.col.pred.Predicate;

/**
 * 
 * todo move relevant parts to lexicon/guesser??
 * @author  Jirka
 */
public class GenParadigms extends Module {
    
    @Getter private Map<String,DeclParadigm> paradigms;
    @Getter private MultiMap<String,String> classes;

    // lexicon needs -- todo should this all be in a single module??
    @Getter private Multi2Map<Paradigm,Tag,Ending> pt2endings = new Multi2Map<>();       // prdgm -> tag -> ending
    
    // guesser needs
    @Getter private MultiMap<Tag,Ending> tag2ending;    // tag -> ending
    @Getter private MultiMap<String,Ending> str2lemEnding;    // string -> lemmatizing endings
    
    
// -----------------------------------------------------------------------------
// Setup
// -----------------------------------------------------------------------------


    @Override
    public int compile(UserLog aUlog) throws IOException  {
        return compile(aUlog, null, null);
    }

    public int compile(UserLog aUlog, final Predicate<DeclParadigm> aPrdgmFilter, final Predicate<Ending> aEndingFilter) throws java.io.IOException {
        final ParadigmsCompiler compiler = new ParadigmsCompiler();
        final morph.ma.module.Paradigms dummy = new morph.ma.module.Paradigms();
        compiler.init(dummy, moduleId, aUlog, args);

        if (aPrdgmFilter  != null) compiler.setPrdgmFilter(aPrdgmFilter);
//      if (aEndingFilter != null) compiler.setEndingFilter(aEndingFilter);

        if ( compiler.compile() == 0) {
            extract(compiler);
        }

        return aUlog.getErrors();
    }

    private void extract(ParadigmsCompiler compiler) {
        for (Ending e : compiler.getEndings().allValues()) {
            pt2endings.add(e.getPrdgm(), e.getTag(), e);
        }

        paradigms = compiler.getParadigms();
        classes   = compiler.getStemTails();
    }
    
    @Override public int configure()  {
        //createSuperParadigms();

        return 0;
    }


// -----------------------------------------------------------------------------
// Analysis
// -----------------------------------------------------------------------------
    

    public DeclParadigm getParadigm(String aPrdgmId) {
        return getParadigms().get(aPrdgmId);
    }

}
