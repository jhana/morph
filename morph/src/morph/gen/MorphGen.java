package morph.gen;

import java.util.*;
import lombok.Getter;
import morph.gen.module.GenModule;
import morph.ma.lts.Lt;
import util.col.Cols;

/**
 * 
 * @todo under development
 * 
 * todo prefixes, capitalization, add info to generation
 * @author j
 */
public class MorphGen implements java.io.Serializable {

    /**
     * Modules in the order as used for generation.
     */
    @Getter List<GenModule> modulesList;

    @Getter Map<String, GenModule> modules;

    Map<String, Integer> moduleNameToIdx;



// -----------------------------------------------------------------------------
// Generation
// -----------------------------------------------------------------------------

    /**
     * 
     * 
     * @return all possible corresponding forms
     */
    public Collection<String> generate(Lt aLt) {
        final List<String> allForms = new ArrayList<>();
        
        
        for (GenModule mam : modulesList ) {
            // todo mam.src()
            Collection<String> forms = mam.generate(aLt);
            allForms.addAll(forms);
            if (!allForms.isEmpty() && mam.isStopWhenNotEmpty() ) break;
        }

        System.out.printf("Gen %s - %s > %s\n", aLt.lemma(), aLt.tag(), Cols.toString(allForms));

        return allForms;
    }
}
