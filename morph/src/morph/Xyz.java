
package morph;

import java.io.IOException;
import java.io.PrintWriter;
import morph.util.MUtilityBase;
import util.io.IO;
import util.io.LineReader;
import util.io.XFile;

/**
 *
 * @author Administrator
 */
public class Xyz extends MUtilityBase {
    public static void main(String[] args) throws IOException {
        new Xyz().go(args);
    }
    
    public void go(String[] aArgs) throws java.io.IOException {
        argumentBasics(aArgs,2);
        XFile in  = getArgs().getDirectFile(0);
        XFile out = getArgs().getDirectFile(1);
        
        LineReader r = IO.openLineReader(in);
        PrintWriter w = IO.openPrintWriter(out);
        
        for (;;) {
            String line = r.readLine();
            if (line == null) break;
            
            if (line.startsWith("---")) {
                line = line.substring(3);
            }
            else if (line.startsWith("--")) {
                line = line.substring(2);
            }

            if (line.endsWith("---")) {
                line = line.substring(0, line.length()-3);
            }
            else if (line.endsWith("--")) {
                line = line.substring(0, line.length()-2);
            }
            
            if (line.startsWith("l'")) {
                w.println("l'");
                line = line.substring(2);
            }
            else if (line.startsWith("L'")) {
                w.println("L'");
                line = line.substring(2);
            }

            if (line.startsWith("d'")) {
                w.println("d'");
                line = line.substring(2);
            }
            else if (line.startsWith("D'")) {
                w.println("D'");
                line = line.substring(2);
            }
            
            
            w.println(line);
        }
        
        
        IO.close(r);
        IO.close(w);
        
    }
    
}
