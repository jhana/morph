package morph;

import java.io.*;
import java.util.*;


/**
 * Converts files from any encoding to any encoding.
 * See http://www.devsphere.com/mapping/docs/guide/encodings.html for possible encodings.
 * <p>
 *
 * To run it: 
 * <br>java Conv <InEncoding> <InFileName> <OutEncoding> <OutFileName>
 * <p> 
 *
 */
public class ConvX {

    public static void main(String[] args) throws IOException {
        String iEnc;
        String oEnc;
        String iFile;
        String oFile;

        if (args.length == 4) {
            iEnc = translateEncAbbr(args[0]);
            iFile = args[1];
            oEnc = translateEncAbbr(args[2]);
            oFile = args[2];
        }
        else if (args.length == 3) {
            iEnc = translateEncAbbr(args[0]);
            iFile = args[1];
            oEnc = translateEncAbbr(args[2]);
            oFile = autoName(iFile, oEnc);
        }
        else if (args.length == 2) {     // ISO cyr -> arg[1]
            iEnc = "ISO8859_5";
            iFile = args[0];
            oEnc = translateEncAbbr(args[1]);
            oFile = autoName(iFile, oEnc);
        }
        else {
            printHelp();
            return;
        }
            
        conv(iEnc, iFile, oEnc, oFile);            
    }

    static void printHelp() {
        System.out.println("Encoding conversion utility\n");
        System.out.println("arguments: InEncoding InFile OutEncoding OutFile");
        System.out.println("See http://www.devsphere.com/mapping/docs/guide/encodings.html for possible encodings");
        System.out.println("\nabbreviations:");
        System.out.println("   Arguments: InEncoding InFile OutEncoding");
        System.out.println("      abbreviation for: InEncoding InFile OutEncoding auto-name");
        System.out.println("   Arguments: InFile OutEncoding");
        System.out.println("      abbreviation for: ISO8859_5 InFile t auto-name");
        System.out.println("\nencoding aliases:");
        System.out.println("   win -> Cp1250, iso -> ISO8859_2 (Latin-2)");
        System.out.println("   winCyr -> Cp1251, isoCyr -> ISO8859_5 (Latin-5)");
        System.out.print("\nauto-name is created from InFile by adding .iso/.iso/.win/.win/{enc} as the extension ");
        System.out.println("depending on the output encoding (ISO8859_2/ISO8859_5/Cp1250/Cp1251/{enc}). ");
        System.out.println("\n(c) Jirka Hana");
    }
    
    static String translateEncAbbr(String aEnc) {   
        if (aEnc.equals("win"))         return "Cp1250";
        else if (aEnc.equals("iso"))    return "ISO8859_2";
        else if (aEnc.equals("winCyr")) return "Cp1251";
        else if (aEnc.equals("isoCyr")) return "ISO8859_5";
        else                            return aEnc;
    }

    static String autoName(String aIFileName, String aOEnc) {
        String ext = aOEnc;
        if (aOEnc.equals("Cp1250") || aOEnc.equals("Cp1251"))
            ext = "win";
        else if (aOEnc.equals("ISO8859_2") || aOEnc.equals("ISO8859_5"))
            ext = "iso";
        
        return aIFileName + '.' + ext; 
    }

    static void conv(String aInEncoding, String aInFileName, String aOutEncoding, String aOutFileName)  throws java.io.IOException {
        Reader r = new BufferedReader(new InputStreamReader(  new FileInputStream(aInFileName), aInEncoding ));
        Writer w = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(aOutFileName), aOutEncoding )) ;

        for(;;) {
            int c = r.read();
            if (c == -1) break;
            
            w.write(c);
        }
        
        r.close();
        w.close();
    }
}
