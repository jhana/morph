/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package morph.corpus;

import java.io.IOException;
import java.io.StringReader;
import junit.framework.TestCase;
import morph.corpus.SgmlTokenizer.TokenType;
import util.sgml.SgmlTag;

/**
 *
 * @author Administrator
 */
public class SgmlTokenizerTest extends TestCase {
    
    public SgmlTokenizerTest(String testName) {
        super(testName);
    }

    public void testBasic() throws IOException {
        SgmlTokenizer t = tokenizer("<t>ab<u><vxz>cd");

        assertEquals(t.tokenType(), TokenType.bof);

        checkStep(t, TokenType.tag, "<t>");
        checkStep(t, TokenType.text, "ab");
        checkStep(t, TokenType.tag, "<u>");
        checkStep(t, TokenType.tag, "<vxz>");
        checkStep(t, TokenType.text, "cd");
        checkStep(t, TokenType.eof, null);
        checkStep(t, TokenType.eof, null);
    }

    public void testPushBack() throws IOException {
        SgmlTokenizer t = tokenizer("<t>ab<u><vxz>cd");

        try {
            t.pushBack();
            fail("Should be in illegal state");
        }
        catch(IllegalStateException e) {}

        checkStep(t, TokenType.tag, "<t>");
        t.pushBack();
        checkStep(t, TokenType.tag, "<t>");
        t.pushBack();                           // second pushback does nothing
        checkStep(t, TokenType.tag, "<t>");

        checkStep(t, TokenType.text, "ab");
        t.pushBack();
        checkStep(t, TokenType.text, "ab");

        checkStep(t, TokenType.tag, "<u>");
        checkStep(t, TokenType.tag, "<vxz>");
        checkStep(t, TokenType.text, "cd");
        checkStep(t, TokenType.eof, null);
        t.pushBack();                           // does nothing pushback does nothing
        checkStep(t, TokenType.eof, null);
    }

    public void testTag() throws IOException {
        SgmlTokenizer t = tokenizer("<t>ab<u><vxz>cd");

        t.next();
        SgmlTag tag = t.tag();
        assertEquals(tag.getCore(), "t");
        assertEquals(tag.toString(), "<t>");

        t.next();
        try {
            t.tag();
            fail("Should be in illegal state");
        }
        catch(IllegalStateException e) {}


    }

    public void testLineCounting() throws IOException {
        checkLineCounting("<t>ab<u>\n<vxz>cd\n\n<u>");          // unix
        checkLineCounting("<t>ab<u>\r\n<vxz>cd\r\n\r\n<u>");    // win
        checkLineCounting("<t>ab<u>\r<vxz>cd\r\r<u>");          // mac
        checkLineCounting("<t>ab<u>\n<vxz>cd\r\n\n<u>");        // mixed
        checkLineCounting("<t>ab<u>\n<vxz>cd\n\r<u>");          // mixed
    }


    protected SgmlTokenizer tokenizer(String aString) {
        return new SgmlTokenizer(new StringReader(aString));
    }

    protected void checkStep(SgmlTokenizer aT, TokenType aType, String token) throws IOException {
        assertEquals(aT.next(), aType);
        assertEquals(aT.tokenType(), aType);
        assertEquals(aT.token(), token);
    }

    protected void checkLineCounting(String aString) throws IOException {
        SgmlTokenizer t = new SgmlTokenizer(new StringReader(aString) );

        assertEquals(t.tokenType(), TokenType.bof);
        assertTrue(t.getLineNo() == 0);

        checkStep(t, TokenType.tag, "<t>");
        checkStep(t, TokenType.text, "ab");
        checkStep(t, TokenType.tag, "<u>");
        checkStep(t, TokenType.eol, null);
        assertTrue(t.getLineNo() == 1);

        checkStep(t, TokenType.tag, "<vxz>");
        checkStep(t, TokenType.text, "cd");
        checkStep(t, TokenType.eol, null);
        assertTrue(t.getLineNo() == 2);

        checkStep(t, TokenType.eol, null);
        assertTrue(t.getLineNo() == 3);
        checkStep(t, TokenType.tag, "<u>");
        checkStep(t, TokenType.eof, null);
    }




}
