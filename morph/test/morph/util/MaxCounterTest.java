
package morph.util;

import java.util.Arrays;
import junit.framework.TestCase;

/**
 *
 * @author Jirka Hana
 */
public class MaxCounterTest extends TestCase {
    
    public MaxCounterTest(String testName) {
        super(testName);
    }

    /**
     * Test of put method, of class MaxCounter.
     */
    public void test() {
        NMaxs<String,Integer> c = new NMaxs<String,Integer>(3);
        c.put("A", 10);
        assertTrue( c.contains("A") );

        c.put("B",  0);
        assertTrue( c.contains("A") );
        assertTrue( c.contains("B") );

        c.put("C", 10);
        assertTrue( c.contains("A") );
        assertTrue( c.contains("B") );
        assertTrue( c.contains("C") );

        c.put("D", 100);
        assertTrue( c.contains("A") );
        assertFalse( c.contains("B") );
        assertTrue( c.contains("C") );
        assertTrue( c.contains("D") );

        c.put("E", 200);
        c.put("F", 100);
        c.put("A", 10);

        assertFalse( c.contains("A") );
        assertTrue( c.contains("D") );
        assertTrue( c.contains("E") );
        assertTrue( c.contains("F") );
        assertEquals(Arrays.asList("F", "D", "E"), c.getIds());
    }

    /**
     * Test of contains method, of class MaxCounter.
     */
    public void testContains() {
    }

    /**
     * Test of getList method, of class MaxCounter.
     */
    public void testGetList() {
    }

}
