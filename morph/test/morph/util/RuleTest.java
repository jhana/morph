/*
 * RuleTest.java
 * JUnit based test
 *
 * Created on October 30, 2004, 3:33 PM
 */

package morph.util;

import junit.framework.TestCase;
import junit.framework.*;
import java.util.*;

/**
 *
 * @author Jiri
 */
public class RuleTest extends TestCase {
    
    public RuleTest(String testName) {
        super(testName);
    }

    protected void setUp() throws java.lang.Exception {
    }

    protected void tearDown() throws java.lang.Exception {
    }

    public static junit.framework.Test suite() {

        junit.framework.TestSuite suite = new junit.framework.TestSuite(RuleTest.class);
        
        return suite;
    }

    public void testGetPoints() {
        Rule rule = new Rule("name", "-1 2 -2 5 -4 10 -6");
        assertEquals(rule.getPoints(0), -1);
        assertEquals(rule.getPoints(2), -2);
        assertEquals(rule.getPoints(3), -2);
        assertEquals(rule.getPoints(5), -4);
        assertEquals(rule.getPoints(9), -4);
        assertEquals(rule.getPoints(50), -6);
    }

    public void testToString()  {
        Rule rule = new Rule("name", "-1 2 -2 5 -4 10 -6");
        assertEquals(rule.toString(), "-1 2 -2 5 -4 10 -6");
    }
    
    // TODO add test methods here. The name must begin with 'test'. For example:
    // public void testHello() {}
    
}
