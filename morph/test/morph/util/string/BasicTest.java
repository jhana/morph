/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph.util.string;

import junit.framework.TestCase;

/**
 *
 * @author j
 */
public class BasicTest extends TestCase {

    public BasicTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    public void testGetSubruleNr() {
    }

    public void testGetSubruleSize() {
    }

    public void testTuples() {
    }

    public void testToString() {
    }

    public void testTranslateTail() {
        Basic rule = new Basic("a/b:b/c:c/d");

        assertEquals("aa", rule.translateTail("aa", 1, 0));
        assertEquals("aa", rule.translateTail("ab", 1, 0));
        assertEquals("ab", rule.translateTail("ac", 1, 0));
        assertEquals("ac", rule.translateTail("ad", 1, 0));
        assertEquals("ae", rule.translateTail("ae", 1, 0));

        assertEquals("ab", rule.translateTail("aa", 0, 1));
        assertEquals("ac", rule.translateTail("ab", 0, 1));
        assertEquals("ad", rule.translateTail("ac", 0, 1));
        assertEquals("ad", rule.translateTail("ad", 0, 1));
        assertEquals("ae", rule.translateTail("ae", 0, 1));

        assertEquals("aa", rule.translateTail("aa", 0, 0));
        assertEquals("ab", rule.translateTail("ab", 0, 0));
        assertEquals("ac", rule.translateTail("ac", 0, 0));
        assertEquals("ad", rule.translateTail("ad", 0, 0));
        assertEquals("ae", rule.translateTail("ae", 0, 0));

        rule = new Basic("2/bb:1/b:b/ccc:c/d");

        assertEquals("a2", rule.translateTail("abb", 1, 0));
        assertEquals("a1", rule.translateTail("ab", 1, 0));
        assertEquals("cc", rule.translateTail("cc", 1, 0));
        assertEquals("ac", rule.translateTail("ad", 1, 0));

        assertEquals("abb", rule.translateTail("a2", 0, 1));
        assertEquals("ab", rule.translateTail("a1", 0, 1));
        assertEquals("accc", rule.translateTail("ab", 0, 1));
        assertEquals("ad", rule.translateTail("ac", 0, 1));
    }

    public void testTranslateHead() {
    }

    public void testMerge() {
    }

    public void testInstantiateRule() {
    }

    public void testFilterByPossibleTails() {
    }
}
