
package morph.util.string;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import junit.framework.TestCase;
import util.Triple;

/**
 *
 * @author Jirka Hana
 */
public class VariableEdTest extends TestCase {
    
    public VariableEdTest(String testName) {
        super(testName);
    }

    /**
     * Test of distance method, of class VariableEd.
     */
    public void testStdDistance() {
        VariableEd ved = new VariableEd(Collections.<Triple<Character,Character,Double>>emptyList()  );
        BasicEd ed = new BasicEd();

        assertEquals(0, ved.distance("abc", "abc") );

        assertEquals(ed.distance("abc", "ABC"), ved.distance("abc", "ABC")/1000 );
        assertEquals(ed.distance("abc", "HFG"), ved.distance("abc", "HFG")/1000 );
        assertEquals(ed.distance("abc", "ab"), ved.distance("abc", "ab")/1000 );
        assertEquals(ed.distance("abc", "abcd"), ved.distance("abc", "abcd")/1000 );
    }

    public void testVarDistance() {
        BasicEd ed = new BasicEd();

        List<Triple<Character,Character,Double>> ccss = new ArrayList<Triple<Character,Character,Double>>();
        ccss.add(new Triple('a', 'A', 0.0));
        ccss.add(new Triple('A', 'a', 0.0));
        ccss.add(new Triple('b', 'B', 0.0));
        ccss.add(new Triple('B', 'b', 0.0));
        ccss.add(new Triple('c', 'C', 0.0));
        ccss.add(new Triple('C', 'c', 0.0));
        VariableEd ved  = new VariableEd( ccss );

        // everything without (A,B,C) should be as before
        assertEquals(0, ved.distance("abc", "abc") );
        assertEquals(ed.distance("abc", "HFG"), ved.distance("abc", "HFG")/1000 );
        assertEquals(ed.distance("abc", "ab"), ved.distance("abc", "ab")/1000 );
        assertEquals(ed.distance("abc", "abcd"), ved.distance("abc", "abcd")/1000 );

        assertEquals(0, ved.distance("abc", "ABC") );

    }

}
