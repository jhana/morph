
package morph.util.string;

import junit.framework.TestCase;
import util.col.Table;

/**
 *
 * @author Administrator
 */
public class EditDistanceTest extends TestCase {
    
    public EditDistanceTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of EditDistance method, of class EditDistance.
     */
    public void testEditDistance() {
    }

    /**
     * Test of basic method, of class EditDistance.
     */
    public void testBasic() {
        EditDistance ed = new EditDistance(null);
        assertEquals(ed.basic("", ""), 0);
        assertEquals(ed.basic("aa", "aa"), 0);
    }

    /**
     * Test of annas method, of class EditDistance.
     */
    public void testAnnas() {
        EditDistance ed = new EditDistance(null);
        xTestAnnas(ed, "", "");
        xTestAnnas(ed, "aa", "aa");
        xTestAnnas(ed, "a", "b");
        xTestAnnas(ed, "aa", "bb");
        xTestAnnas(ed, "aax", "bbx");

        xTestAnnas(ed, "", "bbx");
        xTestAnnas(ed, "bbx", "");

        xTestAnnas(ed, "aabb", "azb");
    }

    private void xTestAnnas(EditDistance aEd, String a, String b) {
        assertEquals(1.0*aEd.basic(a,b), aEd.annas(a,b));
    }
    
    /**
     * Test of getSubsCost method, of class EditDistance.
     */
    public void testGetSubsCost() {
    }

    /**
     * Test of normalize method, of class EditDistance.
     */
    public void testNormalize() {
    }

}
