/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph.util;

import java.util.Arrays;
import junit.framework.TestCase;

/**
 *
 * @author jirka
 */
public class ColsTest extends TestCase {
    
    public ColsTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testFirstUnused() {
        assertEquals(0, Cols.firstUnused(Arrays.asList(1,2,3,4), 0));
        assertEquals(1, Cols.firstUnused(Arrays.asList(0,2,3,4), 0));
        assertEquals(2, Cols.firstUnused(Arrays.asList(0,1,3,4), 0));
        assertEquals(3, Cols.firstUnused(Arrays.asList(0,1,2,4), 0));
        assertEquals(4, Cols.firstUnused(Arrays.asList(0,1,2,3), 0));

        assertEquals(1, Cols.firstUnused(Arrays.asList(2,3,4), 1));
        assertEquals(2, Cols.firstUnused(Arrays.asList(1,3,4), 1));
        assertEquals(3, Cols.firstUnused(Arrays.asList(1,2,4), 1));
        assertEquals(4, Cols.firstUnused(Arrays.asList(1,2,3), 1));
    }
}
