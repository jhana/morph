/*
 * EvalMATest.java
 * JUnit based test
 *
 * Created on September 17, 2004, 7:23 PM
 */

package morph.eval;

import junit.framework.TestCase;
import junit.framework.*;
import morph.*;
import morph.util.*;
import morph.readers.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 *
 * @author Jiri
 */
public class EvalMATest extends TestCase {
    
    public EvalMATest(String testName) {
        super(testName);
    }


    protected void setUp() throws java.lang.Exception {
    }

    protected void tearDown() throws java.lang.Exception {
    }

    public static junit.framework.Test suite() {

        junit.framework.TestSuite suite = new junit.framework.TestSuite(EvalMATest.class);
        
        return suite;
    }

    /**
     * Test of main method, of class morph.eval.EvalMA.
     */
    public void testMain() {

        
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of go method, of class morph.eval.EvalMA.
     */
    public void testGo() {

        
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of helpString method, of class morph.eval.EvalMA.
     */
    public void testHelpString() {

        
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of checkFile method, of class morph.eval.EvalMA.
     */
    public void testCheckFile() {

        
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of checkLemma method, of class morph.eval.EvalMA.
     */
    public void testCheckLemma() {
           assertEquals(Pattern.compile("\\-").split("abc-1def")[0], "abc" );
           assertEquals(Pattern.compile("\\_").split("abc_2def")[0], "abc" );
           assertEquals(Pattern.compile("[\\_\\-]").split("abc_3def")[0], "abc" );
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of error method, of class morph.eval.EvalMA.
     */
    public void testError() {

        
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of prinPrec method, of class morph.eval.EvalMA.
     */
    public void testPrinPrec() {

        
        // TODO add your test code below by replacing the default call to fail.
    }
    
    // TODO add test methods here. The name must begin with 'test'. For example:
    // public void testHello() {}
    
}
