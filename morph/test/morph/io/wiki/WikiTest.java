
package morph.io.wiki;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;
import morph.io.wiki.WikiArticle;
import util.col.Cols;

/**
 *
 * @author Jirka Hana
 */
public class WikiTest extends TestCase {
    
    public WikiTest(String testName) {
        super(testName);
    }

    /**
     * Test of close method, of class Wiki.
     */
    public void testClose() throws Exception {
    }

    /**
     * Test of next method, of class Wiki.
     */
    public void testNext() throws Exception {
    }

    /**
     * Test of main method, of class Wiki.
     */
    public void testMain() throws Exception {
    }

    public void testArticle() {
        articleEq("abc     def", "abc[XYZ]def");
        articleEq("abc          def", "abc[[ja:XYZ]]def");
        articleEq("abc  XYZ   def g", "abc[[XYZ]] def g");
        articleEq("abc  XYZdef   g", "abc[[XYZ]]def g");
        articleEq("abc      XYZdef   g", "abc[[xyz|XYZ]]def g");
        articleEq("abc      XYZ  ", "abc[[xyz|XYZ]]");
    }

    private void articleEq(String aExpected, String aInput) {
        assertEquals(aExpected, new WikiArticle(aInput).toString());
    }

    public static <T> List<T> toList(Iterable<T> aCol) {
        List<T> list = new ArrayList<T>();
        for (T e : aCol) {
            list.add(e);
        }
        return list;
    }

    public void testTokenizing() {
        ct();

        ct("abc", " ", "cde", "!", "Abc", " ", "a-a", " ", "bc", "?", "de");

        ct("abc", " ", "cde", " ", " ", "Abc", "\t", "a-a", "\n", "\t", " ", "bc", "?", "de", "\n", "\n", "t" );
        ct(" ");

        ct("abc", "!", "a-a", "!", "bc", "?", "de");
        ct("abc", "!", "aa-ab", "12", "!", "12a", " ", "-12-dx", " ", "bc", "?", "de");
        ct("1930", "�", "  ", "1941", "  ");
//                1930�[[1941]]
//        ct(Arrays.asList("abc", " ", "aaaa", "(", "a-a", "12", "!", " ", "zz", ")", "bc", "$", "12", "de"), "abc (a-a 12!) bc $12 de" );
    }

    private void ct(String ... aStrs) {
        List<String> tokens = new ArrayList<String>(Arrays.asList(aStrs));
        tokens.removeAll(Arrays.asList(" ", "  ", "\t", "\n"));

        List<String> textStrs = Arrays.asList(aStrs);

        String text = Cols.toString(textStrs, "", "", "", "");
        ct(tokens, text);

    }
    private void ct(List<String> aTokens, String aText) {
        WikiArticle a = new WikiArticle(aText);
        assertEquals(aTokens, toList(a));
    }


}
