/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph.io;

import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author j
 */
public class PushBackTokenizerTest extends TestCase {
    
    public PushBackTokenizerTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testX() {
        testX("ab cd ef gh", "ab", "cd", "ef", "gh");
        testX(" ab cd  ef\tgh  ", "ab", "cd", "ef", "gh");
        testX("ab  cd ef gh", "ab", "cd", "ef", "gh");  // non breaking space
        testX("  й     S", "й", "S");                   // initial non-breaking space, cyrillics
        
    }
    private void testX(String input, String ... aExpected) {
        final PushBackTokenizer tokenizer = new PushBackTokenizer(input);
        
        for (String str : aExpected) {
            assertEquals(str, tokenizer.nextToken());
        }
        assertTrue(!tokenizer.hasMoreTokens());
    }

    /**
     * Test of string method, of class PushBackTokenizer.
     */
    public void testString() {
    }

    /**
     * Test of hasMoreTokens method, of class PushBackTokenizer.
     */
    public void testHasMoreTokens() {
    }

    /**
     * Test of nextToken method, of class PushBackTokenizer.
     */
    public void testNextToken_0args() {
    }

    /**
     * Test of nextToken method, of class PushBackTokenizer.
     */
    public void testNextToken_String() {
    }

    /**
     * Test of nextTokenIf method, of class PushBackTokenizer.
     */
    public void testNextTokenIf() {
    }

    /**
     * Test of pushBack method, of class PushBackTokenizer.
     */
    public void testPushBack() {
    }

    /**
     * Test of eat method, of class PushBackTokenizer.
     */
    public void testEat() {
    }

    /**
     * Test of eatIf method, of class PushBackTokenizer.
     */
    public void testEatIf() {
    }

    /**
     * Test of isNextToken method, of class PushBackTokenizer.
     */
    public void testIsNextToken() {
    }

    /**
     * Test of nextTokenStartsWith method, of class PushBackTokenizer.
     */
    public void testNextTokenStartsWith() {
    }

    /**
     * Test of nextTokenMatches method, of class PushBackTokenizer.
     */
    public void testNextTokenMatches() {
    }

    /**
     * Test of hasMoreElements method, of class PushBackTokenizer.
     */
    public void testHasMoreElements() {
    }

    /**
     * Test of nextElement method, of class PushBackTokenizer.
     */
    public void testNextElement() {
    }

    /**
     * Test of countTokens method, of class PushBackTokenizer.
     */
    public void testCountTokens() {
    }

    /**
     * Test of getString method, of class PushBackTokenizer.
     */
    public void testGetString() {
    }

    /**
     * Test of getTag method, of class PushBackTokenizer.
     */
    public void testGetTag() {
    }

    /**
     * Test of getTagFill method, of class PushBackTokenizer.
     */
    public void testGetTagFill() {
    }

    /**
     * Test of getInt method, of class PushBackTokenizer.
     */
    public void testGetInt() {
    }

    /**
     * Test of getChar method, of class PushBackTokenizer.
     */
    public void testGetChar() {
    }

    /**
     * Test of msg method, of class PushBackTokenizer.
     */
    public void testMsg() {
    }

    /**
     * Test of warning method, of class PushBackTokenizer.
     */
    public void testWarning() {
    }

    /**
     * Test of error method, of class PushBackTokenizer.
     */
    public void testError() {
    }

    /**
     * Test of tokenError method, of class PushBackTokenizer.
     */
    public void testTokenError() {
    }

    /**
     * Test of typeError method, of class PushBackTokenizer.
     */
    public void testTypeError() {
    }

    /**
     * Test of reportError method, of class PushBackTokenizer.
     */
    public void testReportError() {
    }

    /**
     * Test of reportTokenError method, of class PushBackTokenizer.
     */
    public void testReportTokenError() {
    }

    /**
     * Test of reportTypeError method, of class PushBackTokenizer.
     */
    public void testReportTypeError() {
    }

    /**
     * Test of getLastPosition method, of class PushBackTokenizer.
     */
    public void testGetLastPosition() {
    }

    /**
     * Test of lastToken method, of class PushBackTokenizer.
     */
    public void testLastToken() {
    }

    /**
     * Test of state method, of class PushBackTokenizer.
     */
    public void testState() {
    }
}
