/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph.io;

import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;
import util.io.LineReader;

/**
 *
 * @author j
 */
public class ReaderParserTest extends TestCase {
    
    public ReaderParserTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testX() {
        testX("ab cd ef gh", "ab", "cd", "ef", "gh");
        testX(" ab cd  ef\tgh  ", "ab", "cd", "ef", "gh");
        testX("ab  cd ef gh", "ab", "cd", "ef", "gh");  // non breaking space
        testX("  й     S", "й", "S");                   // initial non-breaking space, cyrillics
        
    }
    
    
    private void testX(String input, String ... aExpected)  {
        try {
            final ReaderParser reader = new ReaderParser(new LineReader(new StringReader(input)), null, "test");
            
            reader.nextLine();
            
            for (String str : aExpected) {
                String tok = reader.nextToken();
                System.out.println("tok=" + tok);
                assertEquals(str, tok);
            }
            assertTrue(!reader.hasMoreTokens());
        } catch (IOException ex) {
            Logger.getLogger(ReaderParserTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /**
     * Test of close method, of class ReaderParser.
     */
    public void testClose() throws Exception {
    }

    /**
     * Test of nextLine method, of class ReaderParser.
     */
    public void testNextLine() throws Exception {
    }

    /**
     * Test of removeComments method, of class ReaderParser.
     */
    public void testRemoveComments() {
    }

    /**
     * Test of hasMoreTokens method, of class ReaderParser.
     */
    public void testHasMoreTokens() {
    }

    /**
     * Test of skipUntil method, of class ReaderParser.
     */
    public void testSkipUntil_String() throws Exception {
    }

    /**
     * Test of skipUntil method, of class ReaderParser.
     */
    public void testSkipUntil_Pattern() throws Exception {
    }

    /**
     * Test of line method, of class ReaderParser.
     */
    public void testLine() {
    }

    /**
     * Test of getLineNumber method, of class ReaderParser.
     */
    public void testGetLineNumber() {
    }

    /**
     * Test of getString method, of class ReaderParser.
     */
    public void testGetString() {
    }

    /**
     * Test of getStringF method, of class ReaderParser.
     */
    public void testGetStringF() {
    }

    /**
     * Test of getTag method, of class ReaderParser.
     */
    public void testGetTag_String() {
    }

    /**
     * Test of getTag method, of class ReaderParser.
     */
    public void testGetTag_String_String() {
    }

    /**
     * Test of getTag method, of class ReaderParser.
     */
    public void testGetTag_3args() {
    }

    /**
     * Test of getTagFill method, of class ReaderParser.
     */
    public void testGetTagFill() {
    }

    /**
     * Test of getInt method, of class ReaderParser.
     */
    public void testGetInt() {
    }

    /**
     * Test of getChar method, of class ReaderParser.
     */
    public void testGetChar() {
    }

    /**
     * Test of getBoolean method, of class ReaderParser.
     */
    public void testGetBoolean() {
    }

    /**
     * Test of eat method, of class ReaderParser.
     */
    public void testEat() {
    }

    /**
     * Test of eatIf method, of class ReaderParser.
     */
    public void testEatIf() {
    }

    /**
     * Test of isNextToken method, of class ReaderParser.
     */
    public void testIsNextToken() {
    }

    /**
     * Test of nextToken method, of class ReaderParser.
     */
    public void testNextToken() {
    }

    /**
     * Test of nextTokenStartsWith method, of class ReaderParser.
     */
    public void testNextTokenStartsWith() {
    }

    /**
     * Test of nextTokenMatches method, of class ReaderParser.
     */
    public void testNextTokenMatches() {
    }

    /**
     * Test of msg method, of class ReaderParser.
     */
    public void testMsg() {
    }

    /**
     * Test of warning method, of class ReaderParser.
     */
    public void testWarning() {
    }

    /**
     * Test of error method, of class ReaderParser.
     */
    public void testError() {
    }

    /**
     * Test of getReader method, of class ReaderParser.
     */
    public void testGetReader() {
    }

    /**
     * Test of getTokenizer method, of class ReaderParser.
     */
    public void testGetTokenizer() {
    }

    /**
     * Test of getFileId method, of class ReaderParser.
     */
    public void testGetFileId() {
    }

    /**
     * Test of getFilter method, of class ReaderParser.
     */
    public void testGetFilter() {
    }
}
