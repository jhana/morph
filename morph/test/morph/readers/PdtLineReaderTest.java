/*
 * PdtLineReaderTest.java
 * JUnit based test
 *
 * Created on October 15, 2004, 12:04 PM
 */

package morph.readers;

import junit.framework.TestCase;
import junit.framework.*;
import morph.readers.*;
import morph.util.*;
import java.io.*;
import java.util.*;
import morph.io.PdtLineReader;

/**
 *
 * @author Jiri
 */
public class PdtLineReaderTest extends TestCase {
    File mTestFile;
    
    public PdtLineReaderTest(String testName) {
        super(testName);
    }

    protected void setUp() throws java.lang.Exception {
        //mTestFile = new File(ClassLoader.getSystemResource("morph/test.ma").getFile());
    }

    protected void tearDown() throws java.lang.Exception {
    }

    public static junit.framework.Test suite() {

        junit.framework.TestSuite suite = new junit.framework.TestSuite(PdtLineReaderTest.class);
        
        return suite;
    }

    public void testSetOnlyFDLines() {
    }

    public void testOnlyFDLines() {
    }

    public void testLineType() {
    }

    public void testIsFDLineType() {
    }

    public void testReadLine() {
    }

    public void testExtractByCore() {
    }

    public void testExtract() {
    }

    public void testExtractMMTagsS() {
    }

    public void testExtractMMTags() {
    }

    public void testExtractMMLemmas() {
    }

    public void testExtractTags() {
    }

    public void testExtractValues() throws java.io.IOException {
//        PdtLineReader r = new PdtLineReader(new morph.io.XFile(mTestFile, "Cp1250"));
//        r.testSetLine("xxx<MMt>A<MMt abc>AA<MMt zr>BB<MMt>BBB<MMtr abc>XX<MMt yy>CC");
//        assertEquals(r.extractValues("MMt").toString(), "[A, AA, BB, BBB, CC]");
//        r.testSetLine("xxx<t>A<t abc>AA<t zr>BB<t>BBB<tr abc>XX<t yy>CC");
//        assertEquals(r.extractValues("t").toString(), "[A, AA, BB, BBB, CC]");
    }

    public void testNextVal() {
    }
    
    // TODO add test methods here. The name must begin with 'test'. For example:
    // public void testHello() {}
    
}
