/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph;

import java.util.regex.Pattern;
import junit.framework.TestCase;
import morph.CreateTsMap.Condition;
import morph.CreateTsMap.SlotRule;
import morph.ts.TagsetTest;

/**
 *
 * @author j
 */
public class CreateTsMapTest extends TestCase {

    public CreateTsMapTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    public void testMain() throws Exception {
    }

    public void testSlotRule() throws Exception {
        slotRule("PBCDF", "ABCDE", "ABCDF", ".*", "AB.*", 0, 'P');
        slotRule("ABPDF", "ABCDE", "ABCDF", ".*", "AB.*", 2, 'P');
        slotRule("ABCDP", "ABCDE", "ABCDF", ".*", "AB.*", 4, 'P');

        slotRule("PBCDF", "ABCDE", "ABCDF", "AB.*", "AB.*", 0, 'P');
        slotRule("ABPDF", "ABCDE", "ABCDF", "AB.*", "AB.*", 2, 'P');
        slotRule("ABCDP", "ABCDE", "ABCDF", "AB.*", "AB.*", 4, 'P');

        slotRule("ABCDF", "ABCDE", "ABCDF", "A.*", "X.*", 2, 'P');
        slotRule("ABCDF", "ABCDE", "ABCDF", "X.*", "A.*", 2, 'P');
        slotRule("ABCDF", "ABCDE", "ABCDF", "X.*", "X.*", 2, 'P');

        slotStr("P4XP1", "P4FP1", "P4FP1", "slot .* [^N]..[PD].* c=X");

    }
        static class CreateTsMapX extends CreateTsMap {

            public void args() {
                tagset = TagsetTest.buildSampleTagset();
                String slotRulePatternStr = String.format("slot\\s+(\\S+)\\s+(\\S+)\\s+([%s])\\s*=\\s*(.)", tagset.cCodeString);
                slotRulePattern = Pattern.compile(slotRulePatternStr);
            }

        }

    public void slotStr(String aExpected, String aOrigTag, String aCurTag, String aRuleSpec) throws Exception {
        CreateTsMapX x = new CreateTsMapX(); x.args();
        SlotRule rule = x.parseSlotRule(null, aRuleSpec);

        assertEquals(aExpected, rule.apply(aOrigTag, aCurTag));
    }

    private void slotRule(String aExpected, String aOrigTag, String aCurTag, String aOrigCond, String aCurCond, int slot, char val) throws Exception {
        CreateTsMapX x = new CreateTsMapX(); x.args();
        Condition orig = x.compile(aOrigCond, null, "1st");
        Condition cur  = x.compile(aCurCond,  null, "2st");

        SlotRule rule = x.new SlotRule(orig,cur, slot, val);

        assertEquals(aExpected, rule.apply(aOrigTag, aCurTag));

    }


    public void testMinus() {
    }
}
