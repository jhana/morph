package morph.ma.module.prdgm;

import junit.framework.TestCase;

/**
 *
 * @author jirka
 */
public class StrChangeTest extends TestCase {
    
    public StrChangeTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testFromColonSuffixStr() {
        StrChange change;
                
        change = StrChange.fromColonSuffixStr("z/SUFF1:SUFF2");
        assertEquals("textSUFF2", change.change("text"));
        assertEquals("texSUFF1", change.change("texz"));
    }

    public void testChange() {
        StrChange change;
        
        change = StrChange.fromColonPrefixStr("t/NEW1:dex/NEW2:Text/NEW3:q/NEW4:[gk]/NEW5:0/NEW6:NEW7");
        assertEquals("NEW1ext", change.change("text"));
        assertEquals("NEW2t",   change.change("dext"));
        assertEquals("NEW3",    change.change("Text"));
        assertEquals("NEW5ext", change.change("gext"));
        assertEquals("NEW6Zext",change.change("Zext"));

        change = StrChange.fromColonPrefixStr("t/NEW1:NEW2");
        assertEquals("NEW1ext",    change.change("text"));
        assertEquals("NEW2dext",   change.change("dext"));
    
        change = StrChange.fromColonPrefixStr("NEW1");
        assertEquals("NEW1text",    change.change("text"));
        assertEquals("NEW1dext",   change.change("dext"));

        change = StrChange.fromColonPrefixStr(".*/NEW1");
        assertEquals("NEW1",    change.change("text"));

        change = StrChange.fromColonPrefixStr("[td].*/NEW1");
        assertEquals("NEW1",    change.change("text"));
        assertEquals("zext",    change.change("zext"));
    }
}
