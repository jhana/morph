/*
 * VariousTest.java
 * JUnit based test
 *
 * Created on October 6, 2004, 5:09 PM
 */

package morph.ma.module;

import junit.framework.TestCase;
import junit.framework.*;
import java.util.*;
import morph.util.*;
import morph.ma.lts.*;
import morph.ma.*;

/**
 *
 * @author Jiri
 */
public class VariousTest extends TestCase {
    
    public VariousTest(String testName) {
        super(testName);
    }

    protected void setUp() throws java.lang.Exception {
    }

    protected void tearDown() throws java.lang.Exception {
    }

    public static junit.framework.Test suite() {

        junit.framework.TestSuite suite = new junit.framework.TestSuite(VariousTest.class);
        
        return suite;
    }

    public void testInit() {
    }

    public void testAnalyze() {
    }

    public void testParseNumber() {
        Various var = new Various();
        var.cNumberFormat = java.text.NumberFormat.getInstance(new Locale("CZE", "CZ"));
        
        assertEquals(var.parseNumber("1"), new Long(1));
        assertEquals(var.parseNumber("1.0"), new Long(1));
        assertEquals(var.parseNumber("1.01"), new Double(1.01));
        assertEquals(var.parseNumber("200"), new Long(200));
        assertEquals(var.parseNumber(""), null);
        assertEquals(var.parseNumber("abc"), null);
    }
    
    // TODO add test methods here. The name must begin with 'test'. For example:
    // public void testHello() {}
    
}
