/*
 * StringChangeRuleTest.java
 * JUnit based test
 *
 * Created on August 18, 2006, 2:36 PM
 */

package morph.ma.module;

import junit.framework.*;
import java.util.*;
import util.str.Strings;
import util.col.Cols;
import util.err.Err;

/**
 *
 * @author Jirka
 */
public class StringChangeRuleTest extends TestCase {
    
    public StringChangeRuleTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
    }

    protected void tearDown() throws Exception {
    }

    /**
     * Test of getSubruleNr method, of class morph.ma.module.Basic.
     */
    public void testGetSubruleNr() {
        assertEquals(new Basic("a/b/c").getSubruleNr(), 1);
        assertEquals(new Basic("a/b/c:e/f/g").getSubruleNr(), 2);
    }

    /**
     * Test of getSubruleSize method, of class morph.ma.module.Basic.
     */
    public void testGetSubruleSize() {
        assertEquals(new Basic("a/b/c").getSubruleSize(), 3);
        assertEquals(new Basic("a/b/c:e/f/g").getSubruleSize(), 3);
    }

    /**
     * Test of translateTail method, of class morph.ma.module.Basic.
     */
    public void testTranslateTail() {
        Basic r = new Basic("a/b/c");
        assertEquals(r.translateTail("stringa",0,2), "stringc");
        assertEquals(r.translateTail("stringa",0,0), "stringa");
        assertEquals(r.translateTail("string",0,2),  "string");
        assertEquals(r.translateTail("stringb",0,2), "stringb");
        assertEquals(r.translateTail("stringa",2,0), "stringa");

        r = new Basic("aa/b/c:a/x/y");
        assertEquals(r.translateTail("stringaa",0,2), "stringc");
        assertEquals(r.translateTail("stringa",0,2),  "stringy");
    }

    /**
     * Test of translateHead method, of class morph.ma.module.Basic.
     */
    public void testTranslateHead() {
        Basic r = new Basic("a/b/c");
        assertEquals(r.translateHead("astring",0,2), "cstring");
        assertEquals(r.translateHead("astring",0,0), "astring");
        assertEquals(r.translateHead("string",0,2),  "string");
        assertEquals(r.translateHead("bstring",0,2), "bstring");
        assertEquals(r.translateHead("astring",2,0), "astring");

        r = new Basic("aa/b/c:a/x/y");
        assertEquals(r.translateHead("aastring",0,2), "cstring");
        assertEquals(r.translateHead("astring",0,2),  "ystring");
    }

    /**
     * Test of toString method, of class morph.ma.module.Basic.
     */
    public void testToString() {
        assertEquals("a/b/c:d/e/f", new Basic("a/b/c:d/e/f").toString());
        assertEquals("a/b/c",       new Basic("a/b/c").toString());
        assertEquals("a/b",         new Basic("a/b").toString());
        assertEquals("a:c",         new Basic("a:c").toString());
    }

    /**
     * Test of merge method, of class morph.ma.module.Basic.
     */
    public void testMerge() {
        Basic b = new Basic("aax/r/s:ax/t/u:x/w/z");
        Basic a1 = new Basic("bx/i");
        Basic a2 = new Basic("x/i");
        Basic a3 = new Basic("ax/i");

        System.out.println(b.merge(a2, 0, 0).toString());
        assertEquals(b.merge(a2, 0, 0).toString(), "aax/r/s/aai:ax/t/u/ai:x/w/z/i");
        System.out.println(b.merge(a1, 0, 0).toString());
        assertEquals(b.merge(a1, 0, 0).toString(), "aax/r/s/aax:ax/t/u/ax:bx/bw/bz/i:x/w/z/x");
    }
    
    public void filterByPossibleTails() {
        Basic r = new Basic("aax/r/s:ax/t/u:b/bw/bz/i:x/w/z");
        
        assertEquals(r.filterByPossibleTails(Arrays.asList("x", "b"),0).toString(), r.toString());
        assertEquals(r.filterByPossibleTails(Arrays.asList("ax", "y"),0).toString(), "aax/r/s:ax/t/u");
        assertEquals(r.filterByPossibleTails(Arrays.asList("aaax", "y"),0).toString(), "aax/r/s");
    }

    public void testInstantiateRule() {
        Basic r = new Basic("aax/r/s:ax/t/u:x/w/z");
        
        assertEquals(Cols.toString(r.instantiateRule("bx", 0)), "[bx, bw, bz]");
        assertEquals(Cols.toString(r.instantiateRule("x", 0)), "[x, w, z]");
        assertEquals(Cols.toString(r.instantiateRule("aax", 0)), "[aax, r, s]");
        assertEquals(Cols.toString(r.instantiateRule("ax", 0)), "[ax, t, u]");
        assertEquals(Cols.toString(r.instantiateRule("aaax", 0)), "[aaax, ar, as]");
        assertEquals(Cols.toString(r.instantiateRule("y", 0)), "[y, y, y]");
    }
    
    
    /**
     * Test of tuples method, of class morph.ma.module.Basic.
     */
    public void testTuples() {
    }
    
}
