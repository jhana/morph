/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph.ma.module;

import junit.framework.TestCase;
import morph.ts.TagsetTest;

/**
 *
 * @author j
 */
public class WordEntryTest extends TestCase {

    public WordEntryTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    public void testGetForm() {
        WordEntry we = new WordEntry("A", "Lemma", TagsetTest.buildSampleTagset().fromString("NN---"));
        System.out.println(we);
    }

    public void testGetLemma() {
    }

    public void testGetTag() {
    }

    public void testHash() {
        WordEntry we1 = new WordEntry("A", "Lemma", TagsetTest.buildSampleTagset().fromString("NN---"));
        WordEntry we2 = new WordEntry("A", "Lemma", TagsetTest.buildSampleTagset().fromString("NN---"));

        assertTrue(we1.hashCode() == we2.hashCode());
        assertTrue(we1.equals(we2));

    }
}
