/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package morph.ma;

import java.io.IOException;
import junit.framework.TestCase;
import morph.io.WordReader;
import morph.io.WordReaderList;
import morph.ma.lts.Lts;
import util.str.Cap;

/**
 *
 * @author Administrator
 */
public class ContextTest extends TestCase {

    public ContextTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    protected WordReader words() {
        return new WordReaderList("a", "b", "c", "d", "e", "f", "g");
    }



    public void testX() throws Exception {
        // aR.word() is one word ahead, ctx.curForm() is the current form
        Context ctx = new Context(words(), 2, 2);
        ctx.prepare();

        assertEquals("a", ctx.getCur());
        assertEquals(Cap.lower, ctx.getCap());

        assertEquals(null, ctx.getPrevWord(0));
        assertEquals(null, ctx.getPrevCap(0));
        assertEquals(null, ctx.getPrevAnals(0));
        assertEquals(null, ctx.getPrevWord(1));
        assertEquals("b", ctx.getNextWord(0));
        assertEquals("c", ctx.getNextWord(1));

        Lts lts = new Lts();
        ctx.update(lts);

        assertEquals("b", ctx.getCur());
        assertEquals(Cap.lower, ctx.getCap());

        assertEquals("a", ctx.getPrevWord(0));
        assertEquals(Cap.lower, ctx.getPrevCap(0));
        assertEquals(lts, ctx.getPrevAnals(0));
        assertEquals(null, ctx.getPrevWord(1));

        assertEquals("c", ctx.getNextWord(0));
        assertEquals("d", ctx.getNextWord(1));



    }


    public void testEmptyContext() throws Exception {
        Context ctx = Context.emptyContext("aa");
        ctx.prepare();

        assertEquals("aa", ctx.getCur());
        assertEquals(Cap.lower, ctx.getCap());

        assertEquals(null, ctx.getPrevWord(0));
        assertEquals(null, ctx.getPrevCap(0));
        assertEquals(null, ctx.getPrevAnals(0));
        assertEquals(null, ctx.getPrevWord(1));
        assertEquals(null, ctx.getNextWord(0));
        assertEquals(null, ctx.getNextWord(1));

        assertFalse(ctx.hasNext());

        Lts lts = new Lts();
        try {
            ctx.update(lts);
            fail();
        }
        catch (IOException e) {}
    }

}
