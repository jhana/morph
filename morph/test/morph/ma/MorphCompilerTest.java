
package morph.ma;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;
import util.Pair;
import util.Pairs;
import util.err.FormatError;

/**
 *
 * @author Administrator
 */
public class MorphCompilerTest extends TestCase {
    
    public MorphCompilerTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of factory method, of class MorphCompiler.
     */
    public void testFactory() {
//        System.out.println("factory");
//        MorphCompiler expResult = null;
//        MorphCompiler result = MorphCompiler.factory();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of compile method, of class MorphCompiler.
     */
    public void testCompile() {
//        System.out.println("compile");
//        MArgs aArgs = null;
//        MorphCompiler instance = new MorphCompiler();
//        Morph expResult = null;
//        Morph result = instance.compile(aArgs);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    
    
    /**
     * Test of getModuleOrder method, of class MorphCompiler.
     */
    public void testGetModuleOrder() {
        checkModuleOrder( asList("a", "b", "c"), asList("a", "b", "c"), asList(false, false, false) );
        checkModuleOrder( asList("a", "!", "b", "c"), asList("a", "b", "c"), asList(true, false, false) );
        checkModuleOrder( asList("a", "!", "!", "b", "c"), asList("a", "b", "c"), asList(true, false, false) );
        checkModuleOrder( asList("!", "a", "!", "!", "b", "c"), asList("a", "b", "c"), asList(true, false, false) );
        checkModuleOrder( asList("a", "!", "b", "!", "c", "!", "!"), asList("a", "b", "c"), asList(true, true, true) );
    }

//    private void checkModuleOrderFails(List<String> aSpec) {
//        try {
//            checkModuleOrder(aSpec, null, null);
//            fail("Exception not thrown!");
//        }
//        catch(FormatError e) {
//        }
//    }
    
    private void checkModuleOrder(List<String> aSpec, List<String> aModules, List<Boolean> aStops) {
        List<Pair<String,Boolean>> lss = new MorphCompiler().getModuleOrder( aSpec );
        assertEquals( aModules, Pairs.firsts(lss) );
        assertEquals( aStops, Pairs.seconds(lss) );
    }

    private static <T> List<T> asList(T... a) {
	return Arrays.asList(a);
    }

    private void checkFirsts(List<Pair<String,Boolean>> aLss, String ... aVals) {
        assertEquals( aVals, Pairs.firsts(aLss) );
    }

    private void checkSeconds(List<Pair<String,Boolean>> aLss, Boolean ... aVals) {
        assertEquals( aVals, Pairs.seconds(aLss) );
    }

}
