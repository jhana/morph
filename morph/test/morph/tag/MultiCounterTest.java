
package morph.tag;

import java.util.Map;
import junit.framework.TestCase;

/**
 *
 * @author Jirka Hana
 */
public class MultiCounterTest extends TestCase {
    
    public MultiCounterTest(String testName) {
        super(testName);
    }


    /**
     * Test of put method, of class MultiCounter.
     */
    public void testPut() {
        MultiCounter<String,String> m = new MultiCounter<String,String>();

        m.put("aa", "AA", 0.0);
        Map<String,Double> r = m.get("aa");
        assertEquals(r.get("AA"), 0.0);
        assertEquals(r.get("AAA"), null);

        m.put("aa", "AAA", 1.0);
        r = m.get("aa");
        assertEquals(r.get("AA"), 0.0);
        assertEquals(r.get("AAA"), 1.0);

        m.put("bb", "BB", 2.0);
        r = m.get("bb");
        assertEquals(r.get("BB"), 2.0);
        r = m.get("aa");
        assertEquals(r.get("AA"), 0.0);
        assertEquals(r.get("AAA"), 1.0);
    }


}
