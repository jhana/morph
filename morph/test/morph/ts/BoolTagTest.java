package morph.ts;

import java.util.Arrays;
import junit.framework.TestCase;

/**
 *
 * @author jirka
 */
public class BoolTagTest extends TestCase {
    
    public BoolTagTest(String testName) {
        super(testName);
    }

    Tagset ts;
    BoolTag all, none;
    
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ts = new Tagset.TagsetBuilder().setTagLen(5).setSlotCodes("abcde").build();
        all = ts.codestr2booltag("all");
        none = ts.codestr2booltag("none");
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCreation() {
        assertEquals("+++++", ts.codestr2booltag("all").toString());
        assertEquals("-----", ts.codestr2booltag("none").toString());
        assertEquals("+-+-+", ts.codestr2booltag("ace").toString());
        assertEquals("+-+++", ts.codestr2booltag("acde").toString());
        assertEquals("--+--", ts.codestr2booltag("c").toString());
    }
    
    
    
    public void testFill() {
    }

    public void testToCodeStrings() {
    }

    public void testGetLen() {
        assertEquals(5, all.getLen());
    }

    public void testToSlotsIdxs() {
        assertEquals(Arrays.asList(0,1,2,3,4), all.toSlotsIdxs());
        assertEquals(Arrays.asList(), none.toSlotsIdxs());

        assertEquals(Arrays.asList(0,2,4), ts.codestr2booltag("ace").toSlotsIdxs());
        assertEquals(Arrays.asList(2), ts.codestr2booltag("c").toSlotsIdxs());
    }

    public void testToCodeStringHF() {
        assertEquals("all", all.toCodeStringHF());
        assertEquals("none", none.toCodeStringHF());
    }

    public void testToCodeString() {
        assertEquals("abcde", all.toCodeString());
        assertEquals("", none.toCodeString());
    }

    public void testGet() {
        assertTrue( ts.codestr2booltag("all").get(0));
        assertTrue( ts.codestr2booltag("all").get(1));
        assertTrue( ts.codestr2booltag("all").get(2));
        assertTrue( ts.codestr2booltag("all").get(3));
        assertTrue( ts.codestr2booltag("all").get(4));

        assertTrue( ts.codestr2booltag("abcde").get(0));
        assertTrue( ts.codestr2booltag("abcde").get(1));
        assertTrue( ts.codestr2booltag("abcde").get(2));
        assertTrue( ts.codestr2booltag("abcde").get(3));
        assertTrue( ts.codestr2booltag("abcde").get(4));

        assertFalse( ts.codestr2booltag("c").get(0));
        assertFalse( ts.codestr2booltag("c").get(1));
        assertTrue(  ts.codestr2booltag("c").get(2));
        assertFalse( ts.codestr2booltag("c").get(3));
        assertFalse( ts.codestr2booltag("c").get(4));
        
        assertTrue(  ts.codestr2booltag("aed").get(0));
        assertFalse( ts.codestr2booltag("aed").get(1));
        assertFalse( ts.codestr2booltag("aed").get(2));
        assertTrue(  ts.codestr2booltag("aed").get(3));
        assertTrue(  ts.codestr2booltag("aed").get(4));

        assertFalse( ts.codestr2booltag("none").get(0));
        assertFalse( ts.codestr2booltag("none").get(1));
        assertFalse( ts.codestr2booltag("none").get(2));
        assertFalse( ts.codestr2booltag("none").get(3));
        assertFalse( ts.codestr2booltag("none").get(4));
    }

    public void testSet_int_boolean() {
    }

    public void testSet_int() {
    }

    public void testClear() {
    }

    public void testNot() {
    }

    public void testAnd() {
    }

    public void testNand() {
    }

    public void testOr() {
    }

    public void testXor() {
    }

    public void testIsSubsetOf() {
    }

    public void testHasAllSet() {
        assertTrue( ts.codestr2booltag("all").hasAllSet());
        assertTrue( ts.codestr2booltag("abcde").hasAllSet());

        assertFalse( ts.codestr2booltag("abcd").hasAllSet());
        assertFalse( ts.codestr2booltag("c").hasAllSet());
        assertFalse( ts.codestr2booltag("aed").hasAllSet());
        assertFalse( ts.codestr2booltag("").hasAllSet());
    }

    public void testHasAllCleared_() {
        assertTrue( ts.codestr2booltag("").hasAllCleared());
        assertTrue( ts.codestr2booltag("none").hasAllCleared());

        assertFalse( ts.codestr2booltag("all").hasAllCleared());
        assertFalse( ts.codestr2booltag("abcde").hasAllCleared());
        assertFalse( ts.codestr2booltag("abcd").hasAllCleared());
        assertFalse( ts.codestr2booltag("c").hasAllCleared());
        assertFalse( ts.codestr2booltag("aed").hasAllCleared());
    }

    public void testHasSomeSet() {
    }

    public void testHasAllCleared_int_int() {
    }

    public void testNrOfSet() {
    }

    public void testHashCode() {
    }

    public void testEquals() {
    }

    public void testToString() {
    }
}
