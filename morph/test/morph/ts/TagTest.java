/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph.ts;

import junit.framework.TestCase;

/**
 *
 * @author jirka
 */
public class TagTest extends TestCase {
    
    public TagTest(String testName) {
        super(testName);
    }
    
    Tagset ts;
    
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ts = new Tagset.TagsetBuilder().setTagLen(5).setSlotCodes("abcde").build();
    }

    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testGetLen() {
        assertEquals(5, ts.fromString("NNMS1").getLen());
    }

    public void testToString() {
        assertEquals("NNMS1", ts.fromString("NNMS1").toString());
    }

    public void testHashCode() {
        assertEquals(ts.fromString("NNMS1").toString(), ts.fromString("NNMS1").toString());
    }

    public void testEquals_Object() {
        assertTrue(ts.fromString("NNMS1").equals( ts.fromString("NNMS1") ) );
        assertFalse(ts.fromString("NNMS1").equals( ts.fromString("NNMS2") ) );
        assertFalse(ts.fromString("NNMS1").equals( ts.fromString("ANMS1") ) );
    }

    public void testEquals_String() {
        assertTrue(ts.fromString("NNMS1").eq( "NNMS1") );
        assertFalse(ts.fromString("NNMS1").eq( "NNMS2") );
        assertFalse(ts.fromString("NNMS1").eq( "ANMS1") );
    }

    public void testCompareTo() {
    }

    public void testGetSlot() {
    }

    public void testSetSlot() {
    }

    public void testGetPOS() {
    }

    public void testGetSubPOS() {
    }

    public void testGetNegation() {
    }

    public void testGetVariant() {
    }

    public void testSetPOS() {
    }

    public void testSetSubPOS() {
    }

    public void testSetNegation() {
    }

    public void testSetVariant() {
    }

    public void testMatches() {
    }

    public void testSlotValueIn() {
    }

    public void testPOSIn() {
    }

    public void testSubPOSIn() {
    }

    public void testIsPunct() {
    }

    public void testIsOpenClass() {
    }

    public void testCombineTags() {
    }

    public void testEquals_Tag_BoolTag() {
    }

    public void testStartsWith() {
    }

    public void testFilterTag() {
    }

    public void testTagAbbr() {
    }
}
