/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morph.ts;

import junit.framework.TestCase;

/**
 *
 * @author jirka
 */
public class TagUtilTest extends TestCase {
    
    public TagUtilTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCreatePattern() {
        test("AAFXS1------1A--", "AAFXS1------1A--");
        test("AA\\E.\\QXS1------\\E.\\QA--", "AA?XS1------?A--");
        test("AA\\E..\\QS1------\\E.\\QA--", "AA??S1------?A--");
        test("AA\\E...\\Q1------\\E.\\QA--", "AA???1------?A--");
        
    }
    
    private void test(String aExpectedPatternStr, String aTmpl) {
        assertEquals("\\Q" + aExpectedPatternStr + "\\E", TagUtil.createPattern(aTmpl).toString());
    }
}
