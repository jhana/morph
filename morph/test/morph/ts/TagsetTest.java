package morph.ts;

import java.util.Arrays;
import junit.framework.TestCase;

/**
 *
 * @author jirka
 */
public class TagsetTest extends TestCase {

    public TagsetTest(String testName) {
        super(testName);
    }

    Tagset ts;

    /**
     * Bulds a tagset with 5 slots (slot codes abcde).
     *
     * @return
     */
    public static Tagset buildSampleTagset() {
        return new Tagset.TagsetBuilder()
            .setTagLen(5)
            .setSlotCodes("abcde")
            .setOption("negation.slot", 'd')
            .setOption("negation.affirmative", 'A')
            .setOption("negation.negated", 'N')
            .setOption("variant.slot", 'e')
            .setOption("openClass", "[NAVD].*")
            .setOption("punct", "Z.*")
            .setOption("nrs", "C[=}]---")
            .setOption("slotName", Arrays.asList("POS", "SubPOS", "Gender", "Animacy", "Number"))
            .setOption("sorting", Arrays.asList("aNAPCVDRJTI", "dSDP"))
            .build();
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ts = buildSampleTagset();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testGetDef() {
    }

    public void testSetDef() {
    }

    public void testFromString() {
    }

    public void testFromStringNoCheck() {
    }

    public void testFromStringFill() {
    }

    public void testFromAbbr_3args() {
    }

    public void testFromAbbr_String_String() {
    }

    public void testFromAbbrI() {
    }

    public void testSetAbbrTail() {
    }

    public void testGetLen() {
    }

    public void testIsPunct() {
        assertTrue(ts.isPunct(ts.fromString("Z:---")));
        assertTrue(ts.isPunct(ts.fromString("Z$---")));
        assertFalse(ts.isPunct(ts.fromString("NN---")));
    }

    public void testSlotName() {
    }

    public void testSlotNamePadded() {
    }

    public void testSlotCode() {
    }

    public void testSlot2code() {
        assertEquals('a', ts.slot2code(0));
        assertEquals('b', ts.slot2code(1));
        assertEquals('c', ts.slot2code(2));
        assertEquals('d', ts.slot2code(3));
        assertEquals('e', ts.slot2code(4));
    }

    public void testCode2slot() {
        assertEquals(0, ts.code2slot('a'));
        assertEquals(1, ts.code2slot('b'));
        assertEquals(2, ts.code2slot('c'));
        assertEquals(3, ts.code2slot('d'));
        assertEquals(4, ts.code2slot('e'));
    }

    public void testStr2booltag() {
        assertEquals("+++++", ts.str2booltag("all").toString());
        assertEquals("+++++", ts.str2booltag("abcde").toString());
        assertEquals("+++++", ts.str2booltag("eabdcd").toString());
        assertEquals("-----", ts.str2booltag("none").toString());
        assertEquals("+-+-+", ts.str2booltag("ace").toString());
        assertEquals("+-+-+", ts.str2booltag("cea").toString());
        assertEquals("+-+++", ts.str2booltag("acde").toString());
        assertEquals("--+--", ts.str2booltag("c").toString());

        assertEquals("+++++", ts.str2booltag("+++++").toString());
        assertEquals("-----", ts.str2booltag("-----").toString());
        assertEquals("+-+-+", ts.str2booltag("+-+-+").toString());
        assertEquals("+-+++", ts.str2booltag("+-+++").toString());
        assertEquals("--+--", ts.str2booltag("--+--").toString());
    }

    public void testStr2booltag_List() {
    }

    public void testCodestr2booltag() {
        assertEquals("+++++", ts.codestr2booltag("all").toString());
        assertEquals("+++++", ts.codestr2booltag("abcde").toString());
        assertEquals("+++++", ts.codestr2booltag("eabdcd").toString());
        assertEquals("-----", ts.codestr2booltag("none").toString());
        assertEquals("+-+-+", ts.codestr2booltag("ace").toString());
        assertEquals("+-+-+", ts.codestr2booltag("cea").toString());
        assertEquals("+-+++", ts.codestr2booltag("acde").toString());
        assertEquals("--+--", ts.codestr2booltag("c").toString());
    }

    public void testPlusStr2booltag() {
        assertEquals("abcde", ts.plusStr2booltag("+++++").toCodeString());
        assertEquals("", ts.plusStr2booltag("-----").toCodeString());
        assertEquals("ace", ts.plusStr2booltag("+-+-+").toCodeString());
        assertEquals("acde", ts.plusStr2booltag("+-+++").toCodeString());
        assertEquals("c", ts.plusStr2booltag("--+--").toCodeString());
    }

    public void testCodestr2booltag_List() {
    }

    public void testPlusStr2booltag_List() {
    }

    public void testCheckTag() {
    }

    public void testAssertTag_String() {
    }

    public void testAssertTag_String_String() {
    }

    public void testGetNegationSlot() {
    }

    public void testGetNegationAffirmative() {
    }

    public void testGetNegationNegated() {
    }

    public void testHasVariantSlot() {
    }

    public void testGetVariantSlot() {
    }

    public void testCompare() {
        less("NNMS1", "J,---");
        less("NNMS1", "AAMS1");
        less("AAMS1", "CXMS1");
        less("NNMS1", "NNMP1");
    }

    private void less(String a, String b) {
        assertTrue( ts.compare(ts.fromString(a), ts.fromString(b)) < 0);
    }


}
