
package morph.ant;

import java.io.File;
import java.util.regex.Pattern;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

/**
 *
 * @author Jirka Hana
 */
public class Util {
//    public static Object getReqProperty(final Project aProj, final String aKey, final String aErrMsg) {
//        Object val  = aProj.getProperties().get(aKey);
//        if (val == null || val.equals("")) {
//            throw new BuildException("The property '" + aKey + "' must be specified" + aErrMsg, aProj.getLocation());
//        }
//        return val;
//    }

    /**
     * Retrieves value of a variable from a default, a property or a task attribute.
     * Precedence of value specs: default < property < attribute
     *
     * @param aVarValue value of the value as returned by a getter
     * @param aISet flag set when the value of the variable was set via a setter (the setter must set this flag)
     * @param aPropertyKey key of the property used to set default
     * @return value that should be set to the variable
     */
    public static String getValue(final Project aProject, final String aVarValue, final boolean aISet, final String aPropertyKey) {
        // use a possible property value only if the var was not specified via task's argument
        if (aISet) return aVarValue;

        String val = (String) aProject.getProperties().get(aPropertyKey);
        return val == null ? aVarValue : val;
    }


    /**
     * Retrieves value of a variable from a default, a property or a task attribute.
     * Precedence of value specs: default < property < attribute
     *
     * @param aVarValue value of the value as returned by a getter (must be initialized to null)
     * @param aPropertyKey key of the property used to set default
     * @param aDefault value used if both the getter and the property are nulls
     * @return value that should be set to the variable
     */
    public static String getValue(final Project aProject, final String aVarValue, final String aPropertyKey, final String aDefault) {
        // use a possible property value only if the var was not specified via task's argument
        if (aVarValue != null) return aVarValue;

        String val = (String) aProject.getProperties().get(aPropertyKey);
        return val == null ? aDefault : val;
    }

// =============================================================================
// Copied from util.io.Files

    public final static Pattern cDotSplitter = Pattern.compile("\\.");

    /**
     * Removes an extension (if present) from a file name.
     * @param aFileName file to remove an extension from
     * @return file name without an extension "file.x.txt" -> "file.x", "file" -> "file".
     */
    public static String removeExtension(String aFileName) {
        int lastDotIdx = aFileName.lastIndexOf('.');

        if (lastDotIdx != -1)
            return aFileName.substring(0,lastDotIdx);
        else
            return aFileName;
    }


    /**
     * @return (the last) extension; or an empty string if there is none extension
     */
    public static String getExtension(String aFileName) {
        int lastDotIdx = aFileName.lastIndexOf('.');

        return (lastDotIdx == -1) ? "" : aFileName.substring(lastDotIdx+1 );
    }

    public static String addBeforeExtension(File aFile, String aInfix) {
        String fileStr = aFile.toString();
        return removeExtension(fileStr) + '.' + aInfix + '.' + getExtension(fileStr);
    }

    public static String addBeforeExtension(String aFile, String aInfix) {
        return removeExtension(aFile) + '.' + aInfix + '.' + getExtension(aFile);
    }

    public static File file(File aDir, String aFile) {
        return file(aDir, new File(aFile));
    }

    public static File file(File aDir, File aFile) {
        System.out.println("dir:" + aDir + ", file:" + aFile);
        return aFile.isAbsolute() ? aFile : new File(aDir, aFile.toString());
    }

}
