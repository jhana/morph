package morph.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * Runs morphological analysis.
 * 
 * @author Administrator
 */
public class MaTask extends MorphTask {
    /**
     * Normal constructor
     */
    public MaTask() {
    }

    /**
     * create a bound task
     * @param owner owner
     */
    public MaTask(Task owner) {
        super(owner);
    }

    @Override
    public void execute() throws BuildException {
        setClassname("morph.ma.MA");
        super.execute();
    }

}
