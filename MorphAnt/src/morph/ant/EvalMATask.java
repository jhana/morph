package morph.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * Evaluates a morphologically analyzed file against the golden standard.
 *
 * Setting enabled.eval property to false disables all evals.
 *
 * (EvalTask evaluates tagged files)
 */
public class EvalMATask extends MorphTask {
    /**
     * Normal constructor
     */
    public EvalMATask() {
    }

    /**
     * create a bound task
     * @param owner owner
     */
    public EvalMATask(Task owner) {
        super(owner);
    }


    @Override
    public void execute() throws BuildException {
        final Object enabled  = getProject().getProperties().get("enabled.eval");
        if ("false".equals(enabled)) return;

        setClassname("morph.eval.EvalMA");
        super.execute();
    }


}
