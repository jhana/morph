package morph.ant;


import lombok.Getter;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;


/**
 * Task calling tnt-train binary.
 *
 * precedence of value specs: default < property < attribute
 *
 * iX boolean variables show wheher the property was set or a default should be
 * used
 *
 * @author Jirka Hana
 */
public class TntTrainTask extends MorphExecTask {

    /** Name of the model files (modulo extension) */
    @Getter String model = "model";
    boolean iModelSet = false;

    /** Name of the input corpus  */
    @Getter String corpus;
    boolean iCorpusSet = false;

    /** Filters specifying set of tag-filters that are also used as pre-extensions
     * of input corpora: forall tf in tagFilter . exists (corpus minus ext).tf.(corpus' ext) */
    @Getter String tagFilters = null;
    boolean iTagFilterSet = false;

    /**
     * Create an instance.
     * Needs to be configured by binding to a project.
     */
    public TntTrainTask() {
        super();
    }

    /**
     * create an instance that is helping another task.
     * Project, OwningTarget, TaskName and description are all
     * pulled out
     * @param owner task that we belong to
     */
    public TntTrainTask(Task owner) {
        super(owner);
    }


    public void setModel(String a) {
        iModelSet = true;
        model = a;
    }

    public void setCorpus(String aCorpus) {
        iCorpusSet = true;
        corpus = aCorpus;
    }

    public void setTagFilters(String tagFilter) {
        iTagFilterSet = true;
        this.tagFilters = tagFilter;
    }


    @Override
    public void execute() throws BuildException {
        setFailonerror(true);

        getProps("tnt-para");

//        if (!considerWords) {
//            String origCorpus = corpus;
//            corpus = Util.addBeforeExtension(corpus, "nowords");
//
//            // remove words from the corpus
//            final MorphTask task = new MorphTask(this);
//            task.setClassname("morph.corpus.RemoveWords");
//            task.setArgs(origCorpus + " " + corpus);
//            task.execute();
//        }

        // run training several times, each time considering different tag slots
        if (tagFilters != null) {
            /* Input: set of corpora differing by slots filled in tags.
             * Output: corresponding set of models.
             *
             * The extension of the corpora and of the models are preceded by 
             * slot-strings specifying which slots are filled (e.g. all, pgc, psgnc,...).
             */
            for (String filter : tagFilters.split("[;:\\s]")) {
                final TntTrainTask task = new TntTrainTask(this);
                task.setModel(model + "." + filter);
                task.setCorpus(Util.addBeforeExtension(corpus, filter));
                task.execute();
            }
        }
        // run standard training
        else {
            fillCmdLine(bin, model, corpus);

            super.execute();
        }

    }


}
