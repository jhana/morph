package morph.ant;


import java.io.File;
import lombok.Getter;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Copy;

/**
 * Voting - combines results of several taggers into a single tagged file.
 *
 * Note: This inherited from ExecTask. I see no reason why it could not inherit from MorphTask.
 *
 * precedence of value specs: default < property < attribute
 * @author Jirka Hana
 */
public class VoteTag extends MorphTask { // ExecTask {
    private @Getter String source = null;
    boolean iSourceSet = false;

    private @Getter String transBase;
    private @Getter String lex;

    private @Getter String filters;
    boolean iFiltersSet = false;

    private @Getter String votedFilters;

    private @Getter String ttResult = "x.tt.tm"; // TODO configurable default via property
    boolean iTtResultSet = false;

    private @Getter String result   = "x.tm";      // TODO configurable default via property
    boolean iResultSet = false;

    private @Getter boolean btt = true;
    private boolean iBttSet = false;

    private @Getter String ma = "x.pmm";
    private boolean iMaSet = false;

    private @Getter boolean noTag = false;

    /**
     * Create an instance.
     * Needs to be configured by binding to a project.
     */
    public VoteTag() {
        super();
    }

    /**
     * create an instance that is helping another task.
     * Project, OwningTarget, TaskName and description are all
     * pulled out
     * @param owner task that we belong to
     */
    public VoteTag(Task owner) {
        super(owner);
    }

    public void setBtt(boolean btt) {
        iBttSet = true;
        this.btt = btt;
    }

    public void setFilters(String filters) {
        iFiltersSet = true;
        this.filters = filters;
    }

    public void setLex(String lex) {
        this.lex = lex;
    }

    public void setMa(String ma) {
        iMaSet = true;
        this.ma = ma;
    }

    public void setResult(String result) {
        iResultSet = true;
        this.result = result;
    }

    public void setSource(String source) {
        iSourceSet = true;
        this.source = source;
    }

    public void setTransBase(String transBase) {
        this.transBase = transBase;
    }

    public void setTtResult(String ttResult) {
        iTtResultSet = true;
        this.ttResult = ttResult;
    }

    public void setVotedFilters(String votedFilters) {
        this.votedFilters = votedFilters;
    }

    public void setNoTag(boolean noTag) {
        this.noTag = noTag;
    }




    @Override
    public void execute() throws BuildException {
        setFailonerror(true);

        ma = Util.getValue(getProject(), ma, iMaSet, "tnt.ma");

        String[] filterList = filters.split("[:;\\s]");

        File tntResultBase = Util.file(expDir, btt ? ttResult : result);
        System.out.println("tntResultBase: " + tntResultBase);
        File resultBase    = Util.file(expDir, result);

        if (!noTag) {
            // --- Create filtered lexicons ---
            // <morph classname="morph.tag.FilterLexBySlots"  args="${e.build}/ma/model.lex all;pgc;snc;psgnc ${e.build}/voting"/>
            final MorphTask filterLex = new MorphTask(this);
            filterLex.setClassname("morph.tag.FilterLexBySlots");
            filterLex.setArgs(lex + " " + filters + " " + expDir);
            filterLex.execute();

            // --- copy transitions, tag, btt ---

            String src = Util.getValue(getProject(), source, iSourceSet, "tnt.src");

            Object bttProp  = getProject().getProperties().get("btt");
            if ("false".equals(bttProp)) btt = false;


            for (String filter : filterList) {
                // -- copy transitions --
                // <copy file="${s.build}/model.${slg}-tt.all.123"    tofile="${e.build}/voting/model.all.123"/>
                final Copy copy = new Copy();
                copy.setFile(  new File(transBase + "." + filter + ".123") );
                copy.setTofile( new File(expDir, "model." + filter + ".123") );
                copy.execute();

                // -- tag --
                // <tnt model="model.all"   result="x.all.tm" btt="false"/>
                final TntTagTask tag = new TntTagTask(this);
                tag.setModel("model." + filter);
                tag.setSource(src);
                String fullTntResult = Util.addBeforeExtension(tntResultBase, filter);
                System.out.println("fullTntResult: " + fullTntResult);
                tag.setResult(fullTntResult);  // todo should be <filter>.tt not tt.<filter>
                tag.setBtt(false);
                tag.execute();

                // -- back translation --
                if (btt) {
                    //System.out.println("Translating");
                    String fullResult = Util.addBeforeExtension(resultBase, filter);
                    String ttMemFile = ma + ".ttmem ";
                    System.out.println("ttMemFile: " + ttMemFile);

                    //<morph classname="morph.TranslBack"  args="${e.build}/voting/x.tt.tm ${e.build}/ma/x.l2.leo.pmm.ttmem ${e.build}/voting/x.tm"/>
                    MorphTask morphTask = new MorphTask(this);
                    morphTask.setClassname("morph.TranslBack")  ;
                    String args = "-tfs " + filter + " " +  fullTntResult + " " + ttMemFile + " " + fullResult;
                    morphTask.setArgs(args);
                    System.out.println("morph.TranslBack:" + args);
                    morphTask.execute();
                }

            }
        }
        // <morph classname="morph.tag.Voting"  args="-tcs all;pgc;snc;psgnc -vcs  nfmetdav12i;c;nc;psgnc ${e.build}/voting/x.tm ${e.build}/ma/x.l2.leo.pmm@csts ${e.build}/voting/x.tt.tm"/>
        //String fullTntResult = exDir + (btt ? ttResult : result);

        final MorphTask tag = new MorphTask(this);
        tag.setClassname("morph.tag.Voting");
        //ma = "d:/morph/proj/rus/build/dev/ma/x.l2.leo.nt.pmm"; // TODO !!!!!!!!!!!!!!!!!!!!!!! FIX
        String args = "-tcs " + filters + " -vcs " + votedFilters + " " + resultBase + " " + ma + " " + resultBase;
        tag.setArgs(args);
        System.out.println("args:" + args);
        tag.execute();

    }
}
