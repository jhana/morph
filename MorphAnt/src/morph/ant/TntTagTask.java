package morph.ant;


import java.io.File;
import lombok.Getter;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;


/**
 * Task calling tnt binary, possibly translating tags back to the original tagset.
 * 
 * precedence of value specs: default < property < attribute
 * @author Jirka Hana
 */
public class TntTagTask extends MorphExecTask {
    /** Name of the model files (modulo extension) */
    @Getter String model = "model";
    boolean iModelSet = false;

    /** Input file */
    @Getter String source;
    boolean iSourceSet = false;

    /** The file containing result of tagging before tags are translated back (see {@link #btt}) */
    @Getter String ttResult = "x.tt.tm"; // TODO configurable default via property, if not set keep analogous with result
    boolean iTtResultSet = false;

    /** Name of the output file */
    @Getter String result   = "x.tm";     
    boolean iResultSet = false;

    /** Result of lemmatization (merge with result?) */
    @Getter String lemResult   = "x.pm";     
    boolean iLemResultSet = false;
    
    /** Should tags be translated back to the original tagset? */
    @Getter boolean btt = true;
    private boolean iBttSet = false;

    /** Should the ma be disambiguated by the tnt result? */
    @Getter boolean lemmatize = true;
    private boolean iLemmatizeSet = false;

    /**
     * File containing the result of morphological analysis - it is used
     * to find the memory file for tag back-translation. (see {@link #btt}) 
     */
    @Getter String ma = "x.pmm";
    private boolean iMaSet = false;

    /**
     * Create an instance.
     * Needs to be configured by binding to a project.
     */
    public TntTagTask() {
    }

    /**
     * create an instance that is helping another task.
     * Project, OwningTarget, TaskName and description are all
     * pulled out
     * @param owner task that we belong to
     */
    public TntTagTask(Task owner) {
        super(owner);
    }


    /** Name of the model files (modulo extension) */
    public void setModel(String a) {
        iModelSet = true;
        model = a;
    }

    /** The file containing result of tagging before tags are translated back (see {@link #btt}) */
    public void setTtResult(String result) {
        iTtResultSet = true;
        this.ttResult = result;
    }

    /** Name of the output file */
    public void setResult(String result) {
        iResultSet = true;
        this.result = result;
    }

    /** Name of the output file */
    public void setLemResult(String result) {
        iLemResultSet = true;
        this.lemResult = result;
    }
    
    /** Input file */
    public void setSource(String source) {
        iSourceSet = true;
        this.source = source;
    }

    /** Should tags be translated back to the original tagset? */
    public void setBtt(boolean aDoBackTranslation) {
        iBttSet = true;
        btt = aDoBackTranslation;
    }

    /** Should the ma be disambiguated by the tnt result? */
    public void setLemmatize(boolean aLemmatize) {
        System.out.println("Setting lemmatize to " + aLemmatize);
        iLemmatizeSet = true;
        lemmatize = aLemmatize;
    }
    
    /**
     * File containing the result of morphological analysis - it is used
     * to find the memory file for tag back-translation.
     */
    public void setMa(String maFile) {
        iMaSet = true;
        ma = maFile;
    }



    //<exec command="cmd /c ${tnt.bin} ${s.build}/direct/model.${slg}.a ${d.tnt} &gt; ${d.build}/direct/xa.tm" failonerror="true"/>
    //<exec command="cmd /c ${tnt-bat.bin} ${d.build}/cog/model ${d.tnt} ${d.build}/cog/x.tt.tm" failonerror="true"/>
    @Override
    public void execute() throws BuildException {
        setFailonerror(true);

        getProps("tnt");
        getExDir();

        final Object bttProp  = getProject().getProperties().get("btt");
        if ("false".equals(bttProp)) btt = false;

        final File fullModel;
        if (new File(model).isAbsolute()) {
            fullModel = new File(model);
        }
        else {
            fullModel     = model.startsWith("/") ? new File(model.substring(1)) : new File(expDir, model);
        }

        source             = Util.getValue(getProject(), source, iSourceSet, "tnt.src");
        File fullTntResult = Util.file(expDir, btt ? ttResult : result);
        getProject().log("tag: fullTntResult=" + fullTntResult);

        fillCmdLine(bin, fullModel, source, fullTntResult);

        super.execute();

        ma = Util.getValue(getProject(), ma, iMaSet, "tnt.ma");

        // translate tags back using a "memory file" (produced by MA)
        if (btt) {
            String ttMemFile = ma + ".ttmem ";
            final File fullResult  = Util.file(expDir, result);

            // args="${d.build}/cog/x.tt.tm ${d.build}/ma/x.l2.pmm.ttmem ${d.build}/cog/x.tm"  />
            final MorphTask morphTask = new MorphTask(this);
            morphTask.setClassname("morph.TranslBack")  ;
            String args = fullTntResult + " " + ttMemFile + " " + fullResult;
            morphTask.setArgs(args);
            morphTask.execute();
        }
        
        // use tags to disambiguate lemmas
        final Object lemmatizeProp  = getProject().getProperties().get("lemmatize");
        if ("false".equals(lemmatizeProp)) lemmatize = false;
        if (lemmatize) {
            final File out  = Util.file(expDir, lemResult);
//
            // args="${d.build}/cog/x.tt.tm ${d.build}/ma/x.l2.pmm.ttmem ${d.build}/cog/x.tm"  />
            final MorphTask morphTask = new MorphTask(this);
            morphTask.setClassname("morph.tag.Lemmatize")  ;
            String args = ma + " " + fullTntResult + " " + out;
            morphTask.setArgs(args);
            morphTask.execute();
            
        }
    }

}
