package morph.ant;


import java.io.File;
import lombok.Getter;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * Evaluates a tagged file against the golden standard.
 * 
 * Setting enabled.eval property to false disables all evals.
 *
 * (EvalMATask evaluates morph. analyzed files)
 * @todo add support for various levels of defaults (Util.getValue(..))
 */
public class EvalTask extends MorphTask {
    /** Files to check */
    private @Getter String files = "x.tm";

    /** Golden standard */
    private @Getter String gs = null;

    /** File with results */
    // todo configurable via property
    private @Getter String log = "results.txt";

    /** Should overview files be created File with results */
    private @Getter boolean of = true;

    private @Getter String filters;

 

    /**
     * Normal constructor
     */
    public EvalTask() {
    }

    /**
     * create a bound task
     * @param owner owner
     */
    public EvalTask(Task owner) {
        super(owner);
    }


    public void setFiles(String files) {
        this.files = files;
    }

    public void setGs(String gs) {
        this.gs = gs;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public void setOf(boolean of) {
        this.of = of;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }




    @Override
    protected void configure() throws BuildException {
        setClassname("morph.eval.Eval");

        if (isOf()) {
            addArgs("-of");
        }

        if (!getLog().equals("")) {
            addArgs("-log", getLog());
        }

        if (getFilters() != null) {
            addArgs("-a", "-cs", getFilters());
        }

        // --- Files ---
        gs = Util.getValue(getProject(), getGs(), "eval.gs", null);
        //assertTrue(getGs() != null && !getGs().equals(""), "gs attribute or eval.gs property must specify a corpus to check against.");

        getCommandLine().getJavaCommand().createArgument().setValue(getGs());

        logn("Exp: " + expDir);

        String newFiles = "";
        for (String file : files.trim().split("\\s*;\\s*") ) { // TODO probably smth else on unix
            //file = file.trim();
            if (newFiles.length() > 0) newFiles += ";";
            if (new File(file).isAbsolute()) {
                newFiles += file;
            }
            else {
                newFiles += new File(expDir,file).getPath();
            }
        }

        logn("files: " + files);
        logn("newFiles: " + newFiles);
        files = newFiles;
        getCommandLine().getJavaCommand().createArgument().setValue(files);

        super.configure();
    }


    @Override
    public void execute() throws BuildException {
        final Object enabled  = getProject().getProperties().get("enabled.eval");
        if ("false".equals(enabled)) return;

        super.execute();
    }

}
