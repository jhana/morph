package morph.ant;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.Path;


/**
 * A general task for calling morph's tools. 
 * It also serves as a superclass for tasks simplifying calls of frequently
 * used tools.
 *
 * The usual call is as follows:
 *  <code>
 *     <morph classname="className" args="standard arguments"/>
 * </code>
 * However, keep in mind that ant deprecated this way of argument passing and 
 * it causes problems when a file name contains a space.
 * <p>
 * Additional parameters can be specified using the extra attribute. They are
 * inserted immediately after the classname in the resulting java call:
 * <code>
 *     <morph classname="className" args="standard arguments" extra="extra-arguments"/>
 * </code>
 * 
 * In addition, the property <code>classpath</code> must contain a path object
 * used as a classpath used when invoking the morph tools.
 *
 * The following ant properties can be used to configure the task:
 *
 * <table>
 * <tr><td>ant property<td>values<td>default<td>meaning
 *
 * <tr><td>morph.verbose    <td>boolean       <td>false   <td>Should detailed messages be produced?
 * <tr><td>morph.fork       <td>boolean       <td>false   <td>Should the tool be executed in a new VM (see ant's doc)
 * <tr><td>morph.maxMemory  <td><i>int</i>[mg]<td>none    <td>Adds java'ss -Xmx switch with the specified value.
 * <tr><td>morph.failonerror<td>boolean       <td>true    <td>Should the whole script be stopped when an error occurs?
 * <tr><td>morph.confD      <td>directory     <td>none    <td>passed to morph's -confD switch (directory with configuration files)
 * <tr><td>morph.confE      <td>file extension<td>none    <td>passed to morph's -confE switch (extension of configuration files)
 * <tr><td>morph.dp         <td>directory     <tr>required<td>passed to morph's -confE -dp switch (directory with language data)
 * </table>
 *
 * @author Jirka Hana (http://purl.org/NET/jh)
 */
public class MorphTask extends org.apache.tools.ant.taskdefs.Java {
    private boolean verbose = false;

    /** Experiment's directory, use PrepTask to set it */
    protected File expDir = null;

    /**
     * Additional parameters.
     */
    private @Getter @Setter String extra = null;

    /**
     * Normal constructor
     */
    public MorphTask() {
    }


    /**
     * create a bound task
     * @param owner owner
     */
    public MorphTask(Task owner) {
        super(owner);
    }



    @Override
    public void execute() throws BuildException {
        String expDirStr  = (String) getProject().getProperties().get("exp.dir");
        expDir = (expDirStr == null) ? null : new File(expDirStr);


        final Map props = getProject().getProperties();

        String verboseStr  = (String)props.get("morph.verbose");
        verbose = verboseStr != null && verboseStr.equals("yes") ;

        String fork = (String)props.get("morph.fork");
        if (fork != null && fork.equals("yes")) setFork(true);

        String maxMemory = (String)props.get("morph.maxMemory");
        if (maxMemory == null) maxMemory = "1000m";
        setMaxmemory(maxMemory);

        String failOnError = (String)props.get("morph.failonerror");
        if (failOnError == null) failOnError = "true";
        setFailonerror("true".equals(failOnError));

        configure();

        if (extra != null) {
            addArgs(extra.split("\\s+"));
        }

        String confD = (String)props.get("morph.confD");
        if (confD != null) {
            addArgs("-confD", confD);
        }

        String confE = (String)props.get("morph.confE");
        if (confE != null) {
            addArgs("-confE", confE);
        }

        // --- setting data path ---
        String morphDp = (String)props.get("morph.dp");
        if (morphDp != null && !getArgumentsL().contains("-dp")) {  // add -dp if it is not already there
            addArgs("-dp", morphDp);
        }

        Object classPath = getReqReference("classpath", "");
        assertTrue(classPath instanceof Path, "classpath reference must refer to a path object.");

        setClasspath((Path)classPath);

        logn("");
        logn(getCommandLine().getJavaCommand());
        logn("Classpath = " + classPath);

        executeImpl();
    }

    protected void configure() throws BuildException {
    }

    protected void executeImpl() throws BuildException {
        super.execute();
    }

    /** No warning about deprecation */
    @Override
    public void setArgs(String s) {
        getCommandLine().createArgument().setLine(s);
    }

    public List<String> getArgumentsL() {
        List<String> result = new ArrayList<String>();
        getCommandLine().getJavaCommand().addArgumentsToList(result.listIterator());
        return result;
    }

    /**
     * Inserts a list of arguments at the beginning of the command line
     * @param aArgs
     */
    protected <T> void  addArgs(T ... aArgs) {
        for (int i=aArgs.length-1; i >= 0; i--) {
            getCommandLine().getJavaCommand().createArgument(true).setValue(aArgs[i].toString());
        }
    }



    public void assertTrue(boolean aTest, String aErrMsg, Object ... aParams) {
        if (aTest) return;

        throw new BuildException("ERROR: " + String.format(aErrMsg, aParams), getLocation());
    }

    public Object getReqReference(String aKey, String aErrMsg) {
        final Object val  = getProject().getReference(aKey);
        if (val == null || val.equals("")) {
            throw new BuildException("The reference '" + aKey + "' must be specified" + aErrMsg, getLocation());
        }
        return val;
    }

    public Object getReqProperty(String aKey, String aErrMsg) {
        final Object val  = getProject().getProperties().get(aKey);
        if (val == null || val.equals("")) {
            throw new BuildException("The property '" + aKey + "' must be specified" + aErrMsg, getLocation());
        }
        return val;
    }

    protected void log(String aFormat, Object ... aParams) {
        if (verbose) {
            getProject().log(String.format(aFormat, aParams));
        }
    }

    protected void logn(String aFormat, Object ... aParams) {
        log(aFormat + '\n', aParams);
    }

    protected void logn(Object aParam) {
        log(String.valueOf(aParam) + '\n');
    }

}
