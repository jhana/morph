package morph.ant;


import java.io.File;
import lombok.Getter;
import lombok.Setter;
import org.apache.tools.ant.BuildException;


/**
 * Prepares an experiments (creates the experiment directory and sets it to the exp.dir property).
 *
 * @author Administrator
 */
public class PrepTask extends org.apache.tools.ant.Task {
    private boolean verbose = false;
    /**
     * Normal constructor
     */
    public PrepTask() {
    }

//    /**
//     * create a bound task
//     * @param owner owner
//     */
//    public PrepTask(Task owner) {
//        super(owner);
//    }

    /** Experiment's directory */
    private @Getter @Setter File exp;

    @Override
    public void execute() throws BuildException {
        // rewrite exp property (should be used only in this target)
        //getProject().setProperty("morph.exp", expDir.toString());  // deprecated (replace by exp.dir)
        getProject().setProperty("exp.dir",   exp.toString());
        //getProject().setProperty("e.build",   exp.toString());

        // create the experiment's directory
        org.apache.tools.ant.taskdefs.Mkdir mkdir = new org.apache.tools.ant.taskdefs.Mkdir();
        mkdir.setDir(exp);
        mkdir.execute();

        super.execute();
    }

}
