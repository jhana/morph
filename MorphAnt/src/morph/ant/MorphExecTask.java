package morph.ant;

import java.io.File;
import java.util.IllegalFormatException;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline;

/**
 * Superclass of tasks executing a command-line command.
 *
 * The usual use: override the {@link #execute} method. Before doing any actual work in execute, call 
 * the {@link #getProps} function and (if the task is part of an experiment) the {@link #getExDir} function.
 *
 *
 * @author Jirka
 */
public abstract class MorphExecTask extends ExecTask {
    /**
     * Command pattern (placeholder for the binary and its parameters)
     * in the format of  String.format.	 
     * <p>
     * For example, the value can be <code>cmd /c %s %s %s > %s</code>,
     * the subclass is responsible for replacing <code>%s</code> with the actual parameters
     * and
     *
     */
    protected String cmdPattern;

    /** Path to the binary filled into the pattern */
    protected String bin;

    /**
     * Directory of the experiment (may but need not be used by subclasses).
     */
    protected File expDir;


    public MorphExecTask() {
        super();
    }

    public MorphExecTask(Task owner) {
        super(owner);
    }

    protected void fillCmdLine(Object... aParams) {
        final String command;
        try {
            command = String.format(cmdPattern, aParams);
        }
        catch (IllegalFormatException ex) {
            throw new BuildException("The cmd property '" + cmdPattern + "' has a wrong format.", ex, getLocation());
        }
        
        getProject().log("Command: " + command);
        cmdl = new Commandline(command);
    }

    /**
     * Loads parameters specified as properties.
     * Intended to be called early in {@link #execute}.
     *
     * @param aPropPrefix Prefix of the properties used by a particular task. E.g. properties
     * tnt-para.bin and tnt-para.cmd, have a common prefix tnt-para. The prefix
     * does not contain the final dot.
     */
    protected void getProps(final String aPropPrefix) {
        cmdPattern  = (String)getReqProperty(aPropPrefix + ".cmd", " and contain the " + aPropPrefix + " command pattern");
        bin         = (String)getReqProperty(aPropPrefix + ".bin", " and point to the " + aPropPrefix + " executable");
    }

    /**
     * Loads the directory of the experiment as specified by the <code>exp.dir</code>
     * property.
     *
     * This method is intended to be optionally called early in {@link #execute}.
     */
    protected void getExDir() {
        expDir = new File( (String)getReqProperty("exp.dir", " (use prep task to do that)") );
    }


    /**
     * Load value of a property; throwing an exception if the property is not specified.
     *
     * @param aKey name of the property
     * @param aErrMsg property specific part of the error message reported when
     *   the property is not specified.
     * @return
     */
    protected Object getReqProperty(String aKey, String aErrMsg) {
        final Object val = getProject().getProperties().get(aKey);
        if (val == null || val.equals("")) {
            throw new BuildException("The property '" + aKey + "' must be specified" + aErrMsg, getLocation());
        }
        return val;
    }

}
