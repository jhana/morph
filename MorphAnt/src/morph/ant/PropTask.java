package morph.ant;


import org.apache.tools.ant.BuildException;


/**
 * Overrides a property
 * @author Administrator
 */
public class PropTask extends org.apache.tools.ant.Task {
    private String name;
    private String value;

    /**
     * Normal constructor
     */
    public PropTask() {
    }

//    /**
//     * create a bound task
//     * @param owner owner
//     */
//    public PrepTask(Task owner) {
//        super(owner);
//    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void execute() throws BuildException {
        getProject().setProperty(getName(), getValue());

        super.execute();
    }

}
